package com.dao;

import java.util.List;

import com.model.LibrarianVO;
import com.model.LibraryIssueVO;
import com.model.LibraryReqVO;
import com.model.LibraryVO;

public interface LibraryDao
{
	public LibraryVO insertBooksDetails(LibraryVO libraryVO) throws Exception;
	//insertReq
	public LibraryIssueVO insertReq(LibraryIssueVO libraryVO) throws Exception;
	//updateReq
	public LibraryIssueVO updateReq(LibraryIssueVO libraryVO) throws Exception;
	//updateReturn
	public LibraryIssueVO updateReturn(LibraryIssueVO libraryVO) throws Exception;
	
	
	//updateReqMaster
	public LibraryVO updateReqMaster(LibraryVO libraryVO) throws Exception;
	//updateReturnMaster
	public LibraryVO updateReturnMaster(LibraryVO libraryVO) throws Exception;
	//updateRequestMasterCount
	public LibraryVO updateRequestMasterCount(LibraryVO libraryVO) throws Exception;
	
	
	public  List<Object> getBook_ID() throws Exception ;
	
	public LibraryReqVO requestBooksDetails(LibraryReqVO libraryreqVO) throws Exception;
	//reqBookbyStudent
	 
	public List<LibraryVO> getLtrReferenceDataall()throws Exception;
	//getBooktitle
	public List<LibraryVO> getBooktitle(String cate)throws Exception;
	//getBookDetails
	public List<LibraryVO> getBookDetails(String title,String cate)throws Exception;
	
	public List<LibraryVO> reqBookbyStudent(String sclass)throws Exception;
	//getLibDetails
	public List<LibrarianVO> getLibDetails(String sclass)throws Exception;
	//getRequestbyDate
	public List<LibraryIssueVO> getRequestbyDate(String sclass,String btitle)throws Exception;
	//getIssedAll
	public List<LibraryIssueVO> getIssedAll(String title,String subj)throws Exception;
	//getBookTitle
	public List<LibraryIssueVO> getBookTitle()throws Exception;
	
	//getRequestbyID
	public List<LibraryVO> getRequestbyID(String id)throws Exception;
	//getRequestbyStudent
	public List<LibraryIssueVO> getRequestbyStudent(String id)throws Exception;
	//getIssedBooks
	public List<LibraryIssueVO> getIssedBooks(String sclass,String sec)throws Exception;
	//getDueDateBooks
	public List<LibraryIssueVO> getDueDateBooks(String sclass,String sec)throws Exception;
	//getDueDateBooksAll
	public List<LibraryIssueVO> getDueDateBooksAll()throws Exception;
	//getDueDateBooksAllAdmin
	public List<LibraryIssueVO> getDueDateBooksAllAdmin(String title,String subj)throws Exception;
	//showRequest
	public List<LibraryIssueVO> showRequest()throws Exception;
	
	public List<LibraryReqVO> getLtrRequestData()throws Exception;
	//getBookRequestDetails
	public List<LibraryIssueVO> getBookRequestDetails(String sclass)throws Exception;
	//getBookRequestbyClass
	public List<LibraryIssueVO> getBookRequestbyClass(String sclass,String btitle)throws Exception;
	//getBookReturnDetails
	public List<LibraryIssueVO> getBookReturnDetails(String title,String cate)throws Exception;
	
	//countTotalBooks
	public int countTotalBooks()throws Exception;
	
	//countRequest
	public int countRequest()throws Exception;
	//countIssued
	public int countIssued()throws Exception;
	//countDueDate
	public int countDueDate(String btitle,String cate)throws Exception;
	
	public int countReturnBooks(String btitle,String cate)throws Exception;

	}
	

