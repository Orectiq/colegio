package com.dao;

import java.util.List;

import com.model.AbsenceRequestVO;
import com.model.AbsenceStuRequestVO;
import com.model.LibraryIssueVO;
import com.model.TeacherVO;


public interface AbsenceRequestDao 
{
	public AbsenceRequestVO insertAbsenceRequestDetails(AbsenceRequestVO absenceRequestVO)throws Exception;
	//insertStuAbsenceRequest
	public AbsenceStuRequestVO insertStuAbsenceRequest(AbsenceStuRequestVO absenceRequestVO)throws Exception;
	public List<AbsenceRequestVO> getLtrReferenceData()throws Exception;
	//getTeacherLeaveRequestbyDate
	public List<AbsenceRequestVO> getTeacherLeaveRequestbyDate(String rdate)throws Exception;
	//getTeacherLeaveRequestbyDateNotify
	public List<AbsenceRequestVO> getTeacherLeaveRequestbyDateNotify(String rdate,String tid)throws Exception;
	public List<AbsenceRequestVO> getTeacherLeaveRequestbyDateNotify1(String rdate,String tid,String tim)throws Exception;
	//getStudentLeaveRequestbyDate
	public List<AbsenceStuRequestVO> getStudentLeaveRequestbyDate(String rdate)throws Exception;
	
	//getLeaveData
	public List<AbsenceRequestVO> getLeaveData(String tid)throws Exception;
	//getLeaveGrantbyStudent
	public List<AbsenceStuRequestVO> getLeaveGrantbyStudent(String sid)throws Exception;
	
	//updateLeaveReq
	public AbsenceRequestVO updateLeaveReq(AbsenceRequestVO absenceRequestVO) throws Exception;
	//updateStudentLeaveReq
	public AbsenceStuRequestVO updateStudentLeaveReq(AbsenceStuRequestVO absenceRequestVO) throws Exception;
	
	//updateTeacherMaster
	public TeacherVO updateTeacherMaster(TeacherVO teacherVO) throws Exception;

}
