package com.dao;

import java.util.List;

import com.model.HomeworkStatusVO;
import com.model.HomeworkVO;


public interface HomeworkDao 
{
	public HomeworkVO insertHomeworkDetails(HomeworkVO homeworkVO)throws Exception;
	//insertHomeworkStatus
	public HomeworkStatusVO insertHomeworkStatus(HomeworkStatusVO homeworkVO)throws Exception;
	
	public List<HomeworkVO> getLtrReferenceData()throws Exception;
	//getHomeworkdata
	public List<HomeworkVO> getHomeworkdata(String sclass,String sec,String ttype,String tid,String dt)throws Exception;
	
	public List<HomeworkVO> getHomeworkStudata(String sclass,String sec,String thedate)throws Exception;
	//getHomeworkStatus
	public List<HomeworkStatusVO> getHomeworkStatus(String tid,String thedate,String sid)throws Exception;
	
	//getHomeWorkStuStatus
	public List<HomeworkStatusVO> getHomeWorkStuStatus(String sclass,String sec,String mobile,String subj)throws Exception;
}
