package com.dao;

import java.util.List;

import com.model.EchallanStudentVO;
import com.model.EchallanVO;
import com.model.StuVO;

public interface EchallanDao 
{
	public  List<Object> getChallanId(String ref) throws Exception ;
	
	public EchallanVO insertEchallanDetails(EchallanVO echallanVO)throws Exception;
	public EchallanVO sendEchallanDetails(EchallanVO echallanVO)throws Exception;
	
	public List<EchallanVO> getFeeDraft(String sclass)throws Exception;
	public List<EchallanVO> getFee(String sclass)throws Exception;
	//getStudentFee
	public List<EchallanStudentVO> getStudentFee(String sclass)throws Exception;
	
	//getChallanFeeID
	public List<EchallanVO> getChallanFeeID(String sclass)throws Exception;
	//getChallanDraftID
	public List<EchallanVO> getChallanDraftID(String sclass)throws Exception;
}
