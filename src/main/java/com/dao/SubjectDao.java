package com.dao;

import java.util.List;

import com.model.MenuItemVO;
import com.model.SectionVO;
import com.model.SubjectVO;
import com.model.TeacherVO;

public interface SubjectDao {

	public SubjectVO insertSubjectDetails(SubjectVO subjectVO) throws Exception;
	//insertClass
	public SubjectVO insertClass(String[] myArray) throws Exception;
	
	//deleteSubject
	public SubjectVO deleteSubject(SubjectVO subjectVO) throws Exception;
	
	//insertSection
	public SectionVO insertSection(SectionVO sectionVO) throws Exception;
	
	public List<TeacherVO> getSubject(String sclass,String section,String ttype,String tid)throws Exception;
	//getTeacherSubject
	public List<SubjectVO> getTeacherSubject(String sclass,String section)throws Exception;
	//
	public List<SubjectVO> getSClassMaster()throws Exception;
	
	
	//getSClass
	public List<SectionVO> getSClass()throws Exception;
	//getMenuData
	public List<MenuItemVO> getMenuData(String mtype)throws Exception;
	
	
	
	//getSection
	public List<SectionVO> getSection(String sclass)throws Exception;
}
