package com.dao.constant;



public interface StudentConstant
{
	com.model.LoginVO z1=new com.model.LoginVO();
	String dbname=z1.getDbname();
	String db1="spring_login";
		
	
	String GET_STUID = "SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE 
			+ " as A WHERE A.cntModId = 100";
	String GET_PARID = "SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 101";
	String GET_ADMNID = "SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 102";
	String GET_ADMNID1 = "SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 102";
	String GET_STUROLL = "SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 103";
	String GET_CHALLANID="SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 105";
	
	
	String GET_REQID="SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 107";
	
	String GET_NEWSID = "SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE 
			+ " as A WHERE A.cntModId = 112";
	String GET_EVENTSID="SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE 
			+ " as A WHERE A.cntModId = 113";
	String GET_ASSIGNID="SELECT A.cntPrifix, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE 
			+ " as A WHERE A.cntModId = 115";
	
	
	String UPDATE_COUNTER_COUNT1="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=100";
	String UPDATE_COUNTER_COUNT2="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=101";
	String UPDATE_COUNTER_COUNT3="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=102";
	String UPDATE_COUNTER_COUNT4="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=103";
	String UPDATE_COUNTER_COUNT5="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=104";
	
	String UPDATE_COUNTER_COUNT7="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=107";
	String UPDATE_COUNTER_COUNT8="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=105";
	
	String UPDATE_COUNTER_COUNT_NEWS="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=112";
	String UPDATE_COUNTER_COUNT_EVENTS="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=113";
	
	String UPDATE_COUNTER_COUNT_ASSIGN="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=115";
	
	
	String INSERT_STUDENT="INSERT INTO studentsmaster VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_LOGIN_MASTER="INSERT INTO login_master VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	
	
	String INSERT_PARENT="INSERT INTO parentmaster VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_MEDICAL="INSERT INTO student_medical VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	//String UPDATE_STUDENT="update studentsmaster set fname=?,lname=?,sclass=?,section=?,photo=?,dob=?,bplace=?,nationality=?,mtongue=?,cgender=?,religion=?,category=?,address1=?,address2=?,city=?,state=?,pin=?,mobile=?,phone=?,cemail=?,sch_transport=?,transport_board=?,hostel=?,hostel_allot=?,Hostel_name=?,Hostel_Room_no=?,pre_school1=?,pre_qual1=?,prd_from1=?,prd_to1=?,pre_school2=?,pre_qual2=?,prd_from2=?,prd_to2=?,pre_school3=?,pre_qual3=?,prd_from3=?,prd_to3=?,parent_id=?,adm_id=?,student_roll=?,Join_date=?,School_Name=?,Academic_year=?,role=? where student_id=?";
	String UPDATE_STUDENT="update studentsmaster set fname=?,lname=?,bplace=?,nationality=?,mtongue=?,cgender=?,religion=?,category=?,address1=?,address2=?,city=?,state=?,pin=?,mobile=?,phone=?,cemail=?,pre_school1=?,pre_school2=?,pre_school3=?,Hostel_name=?,Hostel_Room_no=? where student_id=?"; 
	
	String UPDATE_PARENT="update parentmaster set prt_fname=?,prt_mname=?,prt_father_dob=?,prt_mother_dob=?,prt_off_add=?,prt_off_phn=?,prt_design=?,prt_occupa=?,prt_mobile=?,prt_email=?,prt_gname=?,prt_grd_occupa=?,prt_grd_design=?,prt_grd_off_add=?,prt_grd_home_add=?,prt_grd_relation=?,prt_grd_mobile=?,prt_grd_email=? where parent_id=?";
	//String UPDATE_STUDENT="update studentsmaster set fname=?,lname=? where student_id=?";
	String UPDATE_COUNTER="update counter_table set Cnt_Prifix=? where Cnt_Mod_Id=?";
	String EXAMTYPE_MASTER="insert into exam_type_master values(?)";
	String UPDATE_Hostel="update studentsmaster set hostel_allot='Yes',Hostel_name=? where student_id=?";
	String UPDATE_ROLLNO="update studentsmaster,"+
							"("+
							"select "+ 
							"@row_number:=ifnull(@row_number, 0)+1 as new_position, "+
							"student_id "+ 
							"from studentsmaster "+
							"where sclass=? and section=? "+
							" order by fname "+
							") as table_position, "+
							"( "+
							"select @row_number:=0 "+
							") as rowNumberInit "+
							"set student_roll=table_position.new_position "+
							"where table_position.student_id=studentsmaster.student_id";

	
	
	String INSERT_MASTER="INSERT INTO mastertable VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_LOGIN="INSERT INTO login_master VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	
	String UPDATE_MASTER_PHOTO="update mastertable set photo=? where uname=?";
	String UPDATE_LOGIN_PHOTO="update login_master set photo=? where uname=?";
	String UPDATE_TEACHER_PHOTO="update teacher_master set photo=? where mobile=?";
	String UPDATE_HOSTEL_PHOTO="update nonteachingstaff set photo=? where Phone_Number=?";
	String UPDATE_PARENTS_PHOTO="update parentmaster set prt_photo=? where prt_mobile=?";
	String UPDATE_STUDENT_PHOTO="update studentsmaster set photo=? where student_id=?";
	String UPDATE_DRIVER_PHOTO="update transport set dri_photo=? where dri_contact_number=?";
	
	String UPDATE_ADMIN_PASSWORD="update mastertable set pass=? where uname=? and pass=?";
	String UPDATE_MOBILE_PASSWORD="update login_master set lpass=? where uname=? and lpass=?";
	
	String UPDATE_ADMINGLOBALSETTING="update login_master set slogo=?,sheader=?,sfooter=? where sname=?";
	String UPDATE_ADMINGLOBALSETTING1="update mastertable set logo=?,email=?,mobile=?,address=?,sheader=?,sfooter=? where uname=? and schoolname=?";
	String UPDATE_ADMINGLOBALSETTING2="update settingadmin set logo_disp=?,language=? where sname=?";
	String INSERT_ADMINGLOBALSETTING2="insert into settingadmin values(?,?,?)";
	
	String UPDATE_ID="update counter_table set Cnt_Prifix=? where Cnt_Mod_Id=?";
	String UPDATE_PASSWORDSTATUS="update login_master set lpass=?,pass_change=? where uname=?";
	
	String INSERT_MAILSETTING="insert into mailsetting values(?,?,?,?,?)";
	String UPDATE_MAILSETTING="update mailsetting set Tem_message=?,Temp_id=?,Temp_head=?,Temp_footer=? where Tem_var=?";
	String DELETE_MAILSETTING="delete from mailsetting where Tem_var=?";
	String GET_SETTING_ADMIN="select * from settingadmin where sname=:sname";

	
	String INSERT_TERMSCONDITION="insert into terms values(?)";
	String GET_TERMS="select * from terms";
	String GET_COUNTRY="select * from countries";
	
	
	String INSERT_ATTENDANCE="INSERT INTO attendance VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_MARKS="INSERT INTO marks_entry VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_MARKS1="INSERT INTO marks_entry(exam_id,exam_name,exam_type,sclass,ssec,student_id,student_name,isActive,Inserted_By,Inserted_Date,Updated_By,Updated_Date) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
	//String UPDATE_MARKS="update marks_entry set ";
	String INSERT_MARKSALL="INSERT INTO marks_entry_all VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_MARKSALL_NEW="insert into marks_total select exam_id,student_id,student_name,sclass,ssec,sum(mark) as total,avg(mark) as average  FROM marks_entry_all where Exam_Id=? and Exam_Name=? and Exam_Type=? and sclass=? and ssec=? and student_id=?";
	
	
	String GET_ATTENDANCE_CHECK="select * from attendance where aclass=:aclass and asection=:asection and att_time=:att_time and status='Y'";
	String GET_ATTENDANCE_CHECK1="select * from attendance where asection=:asec";
	
	String GET_STUDENT_LIST="select * from studentsmaster";// where student_id=:studid";
	String GET_TRANSPORT_STUDENT_LIST="select distinct(transport_board) from studentsmaster where not transport_board='NIL'";
	
	
	String GET_STUDENT_LIST_ALL="select * from studentsmaster where student_id=:stuid";
	String GET_STUDENT_LIST_MEDICAL="select * from student_medical where student_id=:studid";
	String GET_STUDENT_EMAIL_LOGIN="select * from studentsmaster where cemail=:cemail";
	String GET_PARENT_EMAIL_LOGIN="select * from parentmaster where prt_mobile=:mobile";
	
	String GET_LIBRARIAN_LIST_ALL="select * from librarian_master where lib_id=:libid";
	
	
	String GET_TEACHER_LIST_ALL="select * from teacher_master where mobile=:userid or cemail=:userid and pass=:pwd";
	String GET_TRANS_BOARD_LIST="select * from studentsmaster where transport_board=:transport_board and sch_transport='Yes'";
	String GET_HOSTEL_STU_LIST="select * from studentsmaster where Hostel_name=:Hostel_name";
	
	String GET_PARENT_STU_LIST_ALL="select * from studentsmaster where parent_id=:parid";
	String GET_PARENT_ID="select * from studentsmaster where mobile=:mobile";
	
	String GET_STUDENT_PARENT_LIST_ALL="select * from parentmaster where parent_id=:parent_id";  ///////////////////////////////
	String GET_STUDENT_PARENT_LIST_ALL1="select * from parentmaster where prt_mobile=:parent_id";  ///////////////////////////////
	
	String GET_CLASS_LIST_ALL="select * from studentsmaster where SClass=:sclass and Section=:sec order by student_roll";
	String GET_LOGIN="select * from login_master where uname=:uname and lpass=:lpass";
	String GET_LOGIN_EMAIL="select * from login_master where email=:email";
	String GET_MAILSETTING="select * from mailsetting";
	String GET_MAILSETTING_TYPE="select * from mailsetting where tem_var=:temvar";
	String GET_IDSETTING="select * from counter_table where Cnt_Prifix=:Cnt_Prifix";
	String GET_IDALL="select * from counter_table";
	
	String GET_LOGIN_MOBILE="select * from login_master where uname=:uname";
	
	
	String GET_STUDENT_HOSTEL_GENDER="select * from studentsmaster where cgender=:cgender and hostel='Yes' and hostel_allot='No'";
	
	
	String GET_TOTAL_STU="select * from studentsmaster where SClass=:sclass and Section=:sec";
	
	
	String GET_STU_ATTEN_LIST="select * from attendance where adate between :fdate and :tdate and att_time=:atime and sid=:sid";
	String GET_STU_ATTEN_LIST_DAYALL="select * from attendance_dayStudentsTest where month=:mon and monyear=:monyear and aclass=:aclass and asection=:asection";
	String GET_TEA_ATTEN_LIST_DAYALL="select * from attendance_dayTeacher_Test where month=:month and monyear=:monyear";
	
	
	String GET_STU_ATTEN_LIST_COUNT="select * from dummy";
	String INSERT_DUMMY="INSERT INTO dummy SELECT adate,count(*) as stucount,att_status FROM attendance WHERE MONTH(adate) = 12 AND YEAR(adate) = 2016 and aclass='1st-Std' and asection='A-Section' group by att_status";
	
	
	//String GET_STU_ATTEN_LIST_ALL="select * from attendance where adate between :fdate and :tdate and aclass=:aclass and asection=:asec";
	
	String GET_STU_ATTEN_LIST_ALL="select * from attendance where adate=:fdate and aclass=:aclass and asection=:asec";
	String GET_STU_ATTEN_DATE_LIST_ALL="select * from attendance where adate=:fdate and aclass=:aclass and asection=:asec group by adate";
	String GET_STU_ATTEN_DATE_LIST_ABSENT="select * from attendance where adate=:fdate and aclass=:aclass and asection=:asec and att_status='Absent'";
	String GET_STU_ATTEN_ID_LIST_ABSENT="select * from attendance where adate=:adate and sid=:sid and att_status='Absent'";
	
	
	String GET_ATTEN_COUNT="select count(*) as count from attendance where aclass=:aclass and asection=:asection and adate=:adate and att_status='Present'";
	
	
	String GET_PARENT_LIST_ALL="select a.parent_id,a.prt_fname,a.prt_mobile,a.prt_photo,b.student_id,b.fname,b.lname,b.photo,b.mobile,b.student_roll,b.address1 from parentmaster a,studentsmaster b where a.parent_id=b.parent_id and b.sclass=:sclass and b.section=:sec";
	String GET_CLASS_TOPPER_NAME="select student_name from marks_total where sclass=:sclass and ssec=:ssec order by total desc limit 0,1";
								  //select student_name from colegio_devp.marks_entry where sclass='1st-Std' and ssec='A-Section' and  Inserted_Date in (SELECT max(Inserted_Date) from colegio_devp.marks_entry) order by total desc limit 0,1
	
	String GET_STUDENT_EDIT_LIST="select * from studentsmaster where student_id=:studid";
	/*String GET_STUDENT_DATA_PROFILE="select * from studentsmaster where student_id=stuid";*/
	String GET_STUDENT_LIST1="select * from studentsmaster";
	String DELETE_STUDENT="delete from studentsmaster where student_id=?";
	String DELETE_STUDENT_LOGIN="delete from login_master where uname=?";
	String DELETE_STUDENT_MEDICAL="delete from student_medical where student_id=?";
	
	String EDIT_STUDENT="update studentsmaster set fname=?,lname=?,photo=?,dob=?,bplace=?,nationality=?,mtongue=?,cgender=?,religion=?,category=?,address1=?,address2=? where student_id=?";
	
	String GET_ASSIGNMENT_LIST="select * from studentsmaster where sclass=:sclass and section=:section";
	
	String GET_STUDENTS_LIKE_LIST="SELECT * FROM studentsmaster where sclass=:sclass and section=:section and fname like 'S%' ";
	
	String INSERT_ASSIGNMENT="INSERT INTO assignment VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	
	String GET_PHOTO="select * from login_master where uname=:uname and lpass=:lpass";
	
	String GET_MASTER_DETAILS="select * from mastertable where uname=:uname";
	
	String GET_STUID_LIST="select fname,lname,student_id,sclass,section from studentsmaster";
	
	String GET_TIMETABLE="select * from time_table where sclass=:sclass and section=:section";
	String GET_TIMETABLE_DAY="select * from time_table where sclass=:sclass and section=:section and day=:day";
	String GET_TIMETABLE_PERIOD="select * from time_table group by time1_hr";
	String UPDATE_TIMETABLE="update time_table set time1=?,time2=?,time3=?,time4=?,time5=?,time6=?,time7=?,time8=?,time9=?,time10=?,time11=?,time12=?,time13=?,time14=? where sclass=? and section=? and day=?";
	
	String INSERT_TIMETABLE7="insert into time_table(sclass,section,day,time1,time1_hr,time2,time2_hr,time3,time3_hr,time4,time4_hr,time5,time5_hr,time6,time6_hr,time7,time7_hr) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_TIMETABLE8="insert into time_table(sclass,section,day,time1,time1_hr,time2,time2_hr,time3,time3_hr,time4,time4_hr,time5,time5_hr,time6,time6_hr,time7,time7_hr,time8,time8_hr) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	String INSERT_TIMETABLE9="insert into time_table(sclass,section,day,time1,time1_hr,time2,time2_hr,time3,time3_hr,time4,time4_hr,time5,time5_hr,time6,time6_hr,time7,time7_hr,time8,time8_hr,time9,time9_hr) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_TIMETABLE10="insert into time_table(sclass,section,day,time1,time1_hr,time2,time2_hr,time3,time3_hr,time4,time4_hr,time5,time5_hr,time6,time6_hr,time7,time7_hr,time8,time8_hr,time9,time9_hr,time10,time10_hr) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_TIMETABLE11="insert into time_table(sclass,section,day,time1,time1_hr,time2,time2_hr,time3,time3_hr,time4,time4_hr,time5,time5_hr,time6,time6_hr,time7,time7_hr,time8,time8_hr,time9,time9_hr,time10,time10_hr,time11,time11_hr) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";																																																														
	String INSERT_TIMETABLE12="insert into time_table(sclass,section,day,time1,time1_hr,time2,time2_hr,time3,time3_hr,time4,time4_hr,time5,time5_hr,time6,time6_hr,time7,time7_hr,time8,time8_hr,time9,time9_hr,time10,time10_hr,time11,time11_hr,time12,time12_hr) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";		
	String INSERT_TIMETABLE13="insert into time_table(sclass,section,day,time1,time1_hr,time2,time2_hr,time3,time3_hr,time4,time4_hr,time5,time5_hr,time6,time6_hr,time7,time7_hr,time8,time8_hr,time9,time9_hr,time10,time10_hr,time11,time11_hr,time12,time12_hr,time13,time13_hr) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_TIMETABLE14="insert into time_table(sclass,section,day,time1,time1_hr,time2,time2_hr,time3,time3_hr,time4,time4_hr,time5,time5_hr,time6,time6_hr,time7,time7_hr,time8,time8_hr,time9,time9_hr,time10,time10_hr,time11,time11_hr,time12,time12_hr,time13,time13_hr,time14,time14_hr) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	
	
	String SELECT="select a.student_id,a.fname,a.lname,a.photo,a.sclass,a.section,a.dob,a.bplace,a.nationality,a.mtongue,a.cgender,a.religion,a.category,a.address1,a.address2,a.city,a.state,a.country,a.pin,a.mobile,a.phone,a.cemail,a.adm_id,a.parent_id,a.Join_date,a.Academic_year,a.School_Name,a.student_roll,a.sch_transport,a.transport_board,a.hostel,a.hostel_allot,a.pre_school1,a.pre_qual1,a.prd_from1,a.prd_to1,a.pre_school2,a.pre_qual2,a.prd_from2,a.prd_to2,a.pre_school3,a.pre_qual3,a.prd_from3,a.prd_to3,b.blood_group from studentsmaster a,student_medical b where a.student_id=b.student_id and a.student_id=:student_id";
	String GET_PARSTU="select a.parent_id,a.prt_fname,a.prt_mname,a.prt_father_dob,a.prt_mother_dob,a.prt_occupa,a.prt_design,a.prt_off_add,a.prt_off_phn,a.prt_mobile,a.prt_photo,a.prt_email,a.prt_gname,a.prt_grd_occupa,a.prt_grd_design,a.prt_grd_off_add,a.prt_grd_home_add,a.prt_grd_mobile,a.prt_grd_email,a.prt_grd_relation,b.student_id,b.fname,b.lname,b.student_roll from parentmaster a,studentsmaster b where a.parent_id=b.parent_id and a.parent_id =:parent_id";
	String GET_PARENT="select * from parentmaster where parent_id=:parent_id";
}
