package com.dao.constant;

public interface LibraryConstant
{
	String INSERT_BOOKS="INSERT INTO library_addbook VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_REQ="insert into library_requestbook values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String INSERT_REQUEST="INSERT INTO requestbook_count (Book_Title,count) select Book_Title,count(*) as count from library_requestbook where status='request' group by Book_Title;";
	String DELETE_REQUEST="delete from requestbook_count";
	String GET_REQUEST_BOOKS_LIST="select * from requestbook_count";
	
	
	String UPDATE_REQ="update library_requestbook set status=?,approved_by=? where breq_id=?";
	String UPDATE_RETURN="update library_requestbook set status=?,fineamt=? where breq_id=?";
	
	
	String UPDATE_REQ_MASTER="update library_addbook set req_books=req_books+1,avail_books=avail_books-1 where Book_ID=?";
	String UPDATE_RETURN_MASTER="update library_addbook set req_books=req_books-1,avail_books=avail_books+1,request=request-1 where Book_ID=?";
	
	String UPDATE_REQUEST_COUNT="update library_addbook set request=request+1 where Book_ID=?";
	
	
	String UPDATE_REQ_MASTER_STATUS="update library_addbook set isActive='N' where Book_ID=? and avail_books=1";
	String UPDATE_RETURN_MASTER_STATUS="update library_addbook set isActive='Y' where Book_ID=?";
	
	
	String REQUEST_BOOKS="INSERT INTO library_requestbook VALUES (?,?,?,?,?,?,?,?,?,?)";
	String GET_BOOKID = "SELECT A.cntNum, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE 
			+ " as A WHERE A.cntModId = 106";
	
	String UPDATE_COUNTER_BOOK="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=106";
	
	String GET_LIBRARY_LIST="select * from library_addbook";
	String GET_LIBRARY_LIST_CATE="select * from library_addbook where Subject=:Subject";
	String GET_LIBRARY_LIST_TITLE="select * from library_addbook where Book_Title=:Book_Title and Subject=:Category";
	
	String GET_LIBRARY_STU_LIST="select * from library_addbook where SClass=:SClass and isActive='Y'";
	String GET_LIBRARIAN_LIST="select * from librarian_master where lib_id=:libid";
	String GET_REQUEST_BYDATE="select * from library_requestbook where req_date=:rdate and book_title=:btitle and status='request'"; //-------------------->request
	String GET_ISSUED_BOOK_ALL="select * from library_requestbook where status='received' and Book_Title=:book_title and Subject=:subject";
	String GET_BOOK_TITLE_ALL="select book_title from library_requestbook group by Book_Title";
	
	String GET_REQUEST_BYID="select * from library_addbook where Book_ID=:id";
	String GET_REQUEST_BY_STUDENT="select * from library_requestbook where student_id=:id and status='received'";
	String GET_DUEDATE_BOOKS="SELECT * FROM library_requestbook WHERE Due_Date < CURDATE() and status='received' and sclass=:sclass and sec=:sec";
	String GET_DUEDATE_BOOKS_ALL="SELECT * FROM library_requestbook WHERE Due_Date < CURDATE() and status='received'";
	String GET_DUEDATE_BOOKS_ALL_ADMIN="SELECT * FROM library_requestbook WHERE Due_Date < CURDATE() and status='received' and Book_Title=:book_title and Subject=:subject ";
	
	String GET_ISSUED_BOOKS="SELECT * FROM colegio_devp.library_requestbook WHERE status='received' and sclass=:sclass and sec=:sec";


	String GET_LIBRARY_REQUEST_BOOKS="select * from library_requestbook";
	String GET_LIBRARY_REQUEST_CLASS="select * from library_requestbook where sclass=:sclass and  status='request'";
	String GET_LIBRARY_REQUEST_CLASS_TITLE="select * from library_requestbook where sclass=:sclass and book_title=:book_title and  status='request'";
	
	String GET_LIBRARY_REQUEST_BOOKS_TITLE="select * from library_requestbook where Book_Title=:Book_Title and Subject=:Category";


	
}

