package com.dao.constant;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="counter_table")
public class CounterTableVO implements Serializable, Cloneable {
	
	public static final String TYPE = CounterTableVO.class.getName();
	
	@Id
	@Column(name = "Cnt_Mod_Id")
	private int cntModId = 0;

	@Column(name = "Cnt_Prifix")
	private String cntPrifix = null;

	@Column(name = "Cnt_Count")
	private int cntCount = 0;
	
	@Column(name = "Cnt_Num")
	private String cntNum = null;

	public int getCntModId() {
		return cntModId;
	}

	public void setCntModId(int cntModId) {
		this.cntModId = cntModId;
	}

	public String getCntPrifix() {
		return cntPrifix;
	}

	public void setCntPrifix(String cntPrifix) {
		this.cntPrifix = cntPrifix;
	}

	public int getCntCount() {
		return cntCount;
	}

	public void setCntCount(int cntCount) {
		this.cntCount = cntCount;
	}

	public static String getType() {
		return TYPE;
	}

	public String getCntNum() {
		return cntNum;
	}

	public void setCntNum(String cntNum) {
		this.cntNum = cntNum;
	}

	
	
	
}
