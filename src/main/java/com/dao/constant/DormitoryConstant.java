package com.dao.constant;

public interface DormitoryConstant 
{
	
 String INSERT_Dormitory="INSERT INTO dormitory VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
 String EDIT_Dormitory="update dormitory set Location=?,fee=?,cgender=?,description=?,Warden_Photo=?,name=?,Contact_Number=?,Alt_Contact_Number=?,address=?,DeputyWarden_Photo=?,name1=?,Contact_Number1=?,Alt_Contact_Number1=?,address1=? where Dormitory_name=?";
 
 String DELETE_Dormitory="delete from dormitory where Dormitory_name=?";
 
 String Update_Dormitory="update dormitory set allot_beds=allot_beds+?,remain_beds=remain_beds-? where dormitory_name=?";
 
 						//  dormitoryVO.getAllot_beds(), dormitoryVO.getAllot_beds(),dormitoryVO.getDormitory_name()});
 
 
 String INSERT_Dormitory_Allot="INSERT INTO hostel_allot VALUES (?,?,?,?,?,?)";
 String GET_DORMITORY_STUDENT_LIST="select * from dormitory";
 String GET_HOSTEL_GENDER="select * from dormitory where cgender=:cgender";
 String GET_NON_HOSTEL_LIST="select * from studentsmaster where hostel='No'";
 String GET_DORMITORY_LIST="select * from dormitory where dormitory_name=:dormitory_name";
 String GET_DORMITORY_ALLOT_LIST="select * from hostel_allot where dormitory_name=:dormitory_name";
 String UPDATE_HOSTEL="update studentsmaster set hostel='Yes' where student_id=?";
	
}
