package com.dao.constant;

public interface HomeworkConstant 
{
	//// data: {"sClass1":sclass,"Section1":sec,"theDate":today},
 String INSERT_Homework="INSERT INTO home_work VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
 String INSERT_HomeworkStatus="INSERT INTO home_work_status VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
 String GET_Homework_LIST_ALL="select * from home_work";
 //String sclass,String sec,String ttype,String tid,String dt
 String GET_Homework_LIST="select * from home_work where sClass1=:sclass and Section1=:sec and Teacher_Id=:tid and ttype=:ttype and theDate=:dt";
 String GET_Homework_Stu_LIST="select * from home_work where sClass1=:sClass1 and Section1=:Section1 and theDate=:theDate";
 String GET_Homework_Status_LIST="select * from home_work_status where teacher_id=:teacher_id and  thedate=:thedate and student_id=:student_id";
 String GET_Homework_Status_LIST_STU="select * from home_work_status where sClass1=:sClass1 and  Section1=:Section1 and mobile=:mobile and Subject=:Subject";
 
	
}
