package com.dao.constant;

public interface SubjectConstant {

String INSERT_SUBJECT = "INSERT INTO subject_master VALUES (?,?,?)";;
String DELETE_SUBJECT="delete from subject_master where subject=?";
String INSERT_CLASS="insert into classmaster values(?)";
String DELETE_CLASS="delete from classmaster";

String INSERT_SECTION="INSERT INTO section_master VALUES (?,?)";	
String DELETE_SECTION="delete from section_master where sclass=? order by section DESC limit ?";


String GET_SUBJECT_LIST = "select * from teacher_master where sclass=:sclass and section=:section and ttype=:ttype and Tid=:Tid";
String GET_SUBJECT_TEACHER_LIST="select * from subject_master where sclass=:sclass and section=:section";
String GET_CLASSMASTER="select * from classmaster";

String GET_CLASS_LIST="select distinct(sclass) from section_master";
String GET_MENU_LIST="select * from menu_setting where mtype=:mtype ORDER BY sno+0 asc";

String GET_SECTION_LIST="select * from section_master where sclass=:sclass";

}
