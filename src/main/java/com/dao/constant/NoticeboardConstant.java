package com.dao.constant;

public class NoticeboardConstant {
 ///Nivetha 10-3-17
 public static  String UPDATE_EVENT ="update nboard_event set event_tittle=?,event_desc=?, event_place=? where eventsid=?";

 public static String INSERT_NOTICEBOARD="INSERT INTO nboard_event VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
 

 public static String INSERT_NB_NOTICEBOARD="INSERT INTO nboard_news VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
 public static String EDIT_NB_NOTICEBOARD="update nboard_news set news_tittle=?,news_context=? where newsid=?";

 public static String DELETE_EVENT="delete from nboard_event where eventsid=?";
 

 public static String UPDATE_NB_NOTICEBOARD="update nboard_news set news_tittle=?,news_context=? where newsid=?";
 
 public static String DELETE_NB_NOTICEBOARD="delete from nboard_news where newsid=?";
 
 ///Nivetha 10-3-17
 public static String GET_NOTICEBOARD_LIST = "select * from nboard_event where doe=:doe ORDER BY doe DESC";
 public static String GET_NOTICEBOARD_NEWS_LIST = "select * from nboard_news where dop=:dop ORDER BY dop DESC";
 
 public static String GET_NOTICEBOARD_LIST_ALL1 = "select * from colegio_devp.nboard_event WHERE doe > CURDATE() ORDER BY doe ASC";  //NEWWWWWWWWWWWWWWWWWW
 public static String GET_NOTICEBOARD_LIST_ALL = "select * from nboard_event ORDER BY doe DESC";
 public static String GET_NOTICEBOARD_NEWS_LIST_All = "select * from nboard_news ORDER BY dop DESC";
 

 
 ////current News 
 public static String GET_NOTICEBOARD_NEWS_LIST_TEACHER="select * from nboard_news where n_teacher='YES' and DATE(dop) = DATE(NOW()) ORDER BY top ASC LIMIT 5";
 public static String GET_NOTICEBOARD_NEWS_LIST_LIBRARY="select * from nboard_news where n_non_teach_staff='YES' and DATE(dop) = DATE(NOW()) ORDER BY top ASC LIMIT 5";
 public static String GET_NOTICEBOARD_NEWS_LIST_NONTECH="select * from nboard_news where n_non_teach_staff='YES' and DATE(dop) = DATE(NOW()) ORDER BY top ASC LIMIT 5";
 public static String GET_NOTICEBOARD_NEWS_LIST_STUDENTS="select * from nboard_news where n_student='YES' and DATE(dop) = DATE(NOW()) ORDER BY top ASC LIMIT 5";
 public static String GET_NOTICEBOARD_NEWS_LIST_PARENTS="select * from nboard_news where n_parent='YES' and DATE(dop) = DATE(NOW()) ORDER BY top ASC LIMIT 5";
 
 ////histrory News
 public static String GET_NOTICEBOARD_NEWS_LIST_TEACHER_DATE="select * from nboard_news  where  dop= :dop and  n_teacher='YES'";
 public static String GET_NOTICEBOARD_NEWS_LIST_LIBRARY_DATE="select * from nboard_news  where  dop= :dop and  n_non_teach_staff='YES'";
 public static String GET_NOTICEBOARD_NEWS_LIST_NONTECH_DATE="select * from nboard_news  where  dop= :dop and  n_non_teach_staff='YES'";
 public static String GET_NOTICEBOARD_NEWS_LIST_STUDENTS_DATE="select * from nboard_news  where  dop= :dop and  n_student='YES'";
 public static String GET_NOTICEBOARD_NEWS_LIST_PARENTS_DATE="select * from nboard_news  where  dop= :dop and  n_parent='YES'";
 
 ////Current event and upcomming Events
 public static String GET_NOTICEBOARD_EVENT_TEACHER_LIST_ALL="select * from nboard_event WHERE n_teacher='YES' and doe > CURDATE() ORDER BY toe ASC LIMIT 5";
 public static String GET_NOTICEBOARD_EVENT_STUDENTS_LIST_ALL="select * from nboard_event WHERE n_student='YES' and doe > CURDATE() ORDER BY toe ASC LIMIT 5";
 public static String GET_NOTICEBOARD_EVENT_PARENTS_LIST_ALL="select * from nboard_event WHERE n_parent='YES' and doe > CURDATE() ORDER BY toe ASC LIMIT 5";
 public static String GET_NOTICEBOARD_EVENT_NONTECH_LIST_ALL="select * from nboard_event WHERE n_non_teach_staff='YES' and doe > CURDATE() ORDER BY toe ASC LIMIT 5";
 
 ///only current date event
 public static String GET_NOTICEBOARD_EVENTS_LIST_TEACHER="select * from nboard_event where n_teacher='YES' and DATE(doe) = DATE(NOW())";
 public static String GET_NOTICEBOARD_EVENTS_LIST_LIBRARY="select * from nboard_event where n_non_teach_staff='YES' and DATE(doe) = DATE(NOW())";
 public static String GET_NOTICEBOARD_EVENTS_LIST_NONTECH="select * from nboard_event where n_non_teach_staff='YES' and DATE(doe) = DATE(NOW())";
 public static String GET_NOTICEBOARD_EVENTS_LIST_STUDENTS="select * from nboard_event where n_student='YES' and DATE(doe) = DATE(NOW())";
 public static String GET_NOTICEBOARD_EVENTS_LIST_PARENTS="select * from nboard_event where n_parent='YES' and DATE(doe) = DATE(NOW())";
 
 /////history Events
 public static String GET_NOTICEBOARD_EVENT_TEACHER="select * from nboard_event WHERE doe=:doe and n_teacher='YES'";
 public static String GET_NOTICEBOARD_EVENT_NONTECH="select * from nboard_event WHERE  doe=:doe and n_non_teach_staff='YES'";
 public static String GET_NOTICEBOARD_EVENT_STUDENTS="select * from nboard_event WHERE  doe=:doe and n_student='YES'";
 public static String GET_NOTICEBOARD_EVENT_PARENTS="select * from nboard_event WHERE  doe=:doe and n_parent='YES'";
 

 
 
 
 
 
 
 
 
 

 public static String GET_NEWSBOARD_TITLE="select * from nboard_news where newsid=:newsid";

 
 public static String GET_NOTICEBOARD_TITLE="select * from nboard_event where eventsid=:eventsid";
 
 
 
 
 
}