package com.dao.constant;

public interface ExamConstant {

	String INSERT_EXAM="INSERT INTO exam_schedule_new VALUES (?,?,?,?,?,?,?,?,?)";
	String UPDATE_EXAM = "update exam_schedule_new set exam_name=?,exam_type=?,e_class=?,section=? where exam_id=?";
	
	String GET_EXAM_LIST="select * from exam_schedule";
	String GET_EXAM_LIST_NEW="select * from exam_schedule_new group by exam_id";
	String GET_EXAM_LIST_ALL="select * from exam_schedule_new where exam_type=:etype and e_class=:sclass and section=:sec group by exam_id";
	String GET_EXAM_LIST_CLASS_ALL="select * from exam_schedule_new where E_Class=:sclass and Section=:sec group by exam_id";
	
	String GET_MARKS_LIST_ALL="SELECT student_id,student_name,subject,mark FROM marks_entry_all where exam_type=:exam_type and sclass=:sclass and ssec=:ssec and exam_id=:exam_id group by student_name,subject";
	String GET_MARKS_LIST_ALL_SUBJECT="SELECT subject FROM marks_entry_all where sclass=:sclass and ssec=:ssec group by subject";
	String GET_MARKS_LIST_STUDENTS="SELECT student_id,student_name FROM marks_entry_all where sclass=:sclass and ssec=:ssec group by student_name";
	String GET_MARKS_LIST_EXAMID="select * from new_mark1 where exam_id=:exam_id";
	String GET_MARKS_LIST_FINAL="select * from markentryalldumpHead";
	
	
	String GET_MARKS_SUBJECT="SELECT * FROM marks_entry_all where exam_id=:exam_id and student_id=:student_id and subject=:subject";
	 
	String GET_MARKS_LIST_ALL_STUDENTS="select * from marks_entry_all where sclass=:sclass and ssec=:ssec and exam_id=:exam_id and student_id=:student_id";
	
	
	String GET_EXAMTYPE_ALL="select * from exam_type_master";
	String GET_MARKS_STU_LIST="select * from marks_entry where student_id=:student_id";
	String GET_EXAM_ID="select * from exam_schedule_new where exam_id=:exam_id";
	String GET_EXAM_ID_SUBJECT="select * from exam_schedule_new where exam_id=:exam_id and subject=:subject";
	
	//new
	String GET_STUDENT_EXAM="select * from exam_schedule a1,studentsmaster a2 where a2.student_id=:student_id and a1.E_Class=a2.sclass";
	
	
	String DELETE_EXAM="delete from exam_schedule_new where exam_id=?";

	
}
