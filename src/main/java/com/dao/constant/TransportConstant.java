package com.dao.constant;

public class TransportConstant {
	
	public static String INSERT_TRANSPORT="INSERT INTO transport VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static String INSERT_TRANSPORT_ATTENDANCE="INSERT INTO transport_attendance VALUES(?,?,?,?,?,?,?,?,?)";
	public static String DELETE_TRANSPORT="delete from transport where transport_name=?";
	public static String DELETE_LOGIN_TRANSPORT="delete from login_master where sid=?";
	
	public static String GET_TRANSPORT_CHECK="select * from transport where uname=:userid and pass=:pwd";
	
	public static String GET_TRANSPORT_LIST = "select * from transport";
	public static String GET_TRANSPORT_LIST_ALL = "select * from transport WHERE transport_name=:transport_name";
	//GET_TRANSPORT_USER
	public static String GET_TRANSPORT_USER = "select * from transport where dri_contact_number=:uname";
	public static String GET_TRANSPORT_LOGIN_USER="select * from transport where dri_contact_number=:mobile";
	public static String GET_ATTENDANCE_HISTORY="select * from transport_attendance where duser=:duser and adate=:adate";
	public static String GET_TRAN_DETAIL="select * from transport_attendance where duser=:duser";
	public static String GET_ATTENDANCE_HISTORY_ABSENT="select * from transport_attendance where duser=:duser and adate=:adate and attendance='Absent' ";
	
	
	public static String GET_AO_STUDENTS_HISTORY_LIST="select * from transport_attendance where boarding=:boarding and adate=:adate";
	public static String GET_AO_STUDENTS_HISTORY_LIST_ABSENT="select * from transport_attendance where boarding=:boarding and adate=:adate and attendance='Absent'";
	
	
	
	public static String GET_AO_STUDENTS_LIST="select * from transport_attendance where boarding=:boarding";
	
}
