package com.dao.constant;

public interface EchallanConstant {
	
	String INSERT_echallan="INSERT INTO echallan VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String UPDATE_echallan="update echallan set sendMessage='Y' where challan_id=?";
	String UPDATE_ECHALLAN="update echallan set theDate=?,fee_name=?,acc_year=?,Fee_Break1=?,amount1=?,Fee_Break2=?,amount2=?,Fee_Break3=?,amount3=?,total=?,SClass=?,Section=?,datepicker=? where challan_id=?";
	String GET_FEE_DRAFT_ALL="select * from echallan where SClass=:sclass and sendMessage='N'";
	String GET_FEE_ALL="select * from echallan where SClass=:sclass and sendMessage='Y'";
	
	String GET_CHALLAN_FEE_ID="select * from echallan where challan_id=:challan_id and sendMessage='Y'";
	String GET_CHALLAN_DRAFT_ID="select * from echallan where challan_id=:challan_id and sendMessage='N'";
	

	//String GET_STUDENT_FEE_ALL="select * from echallan a1,studentsmaster a2 where a2.student_id=:student_id and a1.SClass=a2.sclass and a1.sendMessage='Y'";
	String GET_STUDENT_FEE_ALL="select * from studentsmaster where student_id=:student_id";
	
	String GET_CHALLANID = "SELECT A.cntNum, (A.cntCount+1) as referenceno FROM " + CounterTableVO.TYPE 
			+ " as A WHERE A.cntModId = 105";
	String DELETE_CHALLAN = "delete from echallan where challan_id=?";
	
	
	
	
}
