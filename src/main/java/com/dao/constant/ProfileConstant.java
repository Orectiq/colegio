package com.dao.constant;

public interface ProfileConstant
{
	String UPDATE_PARENTS_PASSWORD="update login_master set lpass=? where uname=? and lpass=?";
	
	String UPDATE_ADMIN_PASSWORD="update mastertable set pass=? where uname=? and pass=?";
	String UPDATE_MOBILE_PASSWORD="update login_master set lpass=? where uname=? and lpass=?";
	
	String UPDATE_TEACHER_PHOTO="update teacher_master set photo=? where mobile=?";
	String UPDATE_HOSTEL_PHOTO="update nonteachingstaff set photo=? where Phone_Number=?";
}
