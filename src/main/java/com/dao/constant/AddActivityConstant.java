package com.dao.constant;

public interface AddActivityConstant {

 String INSERT_AddActivity="INSERT INTO student_activity VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
 
 String GET_AddActivity_LIST_ALL="select * from student_activity";
 String GET_AddActivity_LIST_TEACHER="select * from student_activity where aclass=:aclass and asec=:asec";
 String GET_AddActivity_LIST_TEACHER_TYPE="select * from student_activity where aclass=:aclass and asec=:asec and Place=:Place ORDER BY ADate DESC LIMIT 1";
 String GET_AddActivity_LIST_TEACHER_TYPE2="select * from student_activity where aclass=:aclass and asec=:asec and Place=:Place ORDER BY adate DESC ";
	
}
