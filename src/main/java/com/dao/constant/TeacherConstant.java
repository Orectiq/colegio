package com.dao.constant;

public interface TeacherConstant
{
	String GET_TEACHERID="SELECT A.cntNum, (A.cntCount) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 104";
	
	String INSERT_TEACHER="INSERT INTO teacher_master VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String UPDATE_TEACHER="update teacher_master set fname=?,lname=?,dob=?,nationality=?,cgender=?,address1=?,address2=?,city=?,state=?,pin=?,mobile=?,phone=?,cemail=?,pan=? where Tid=?";
	String DELETE_TEACHER="delete from teacher_master where Tid=?";
	String DELETE_LOGIN="delete from login_master where sid=?";
	
	String GET_TEID="SELECT * FROM login_master  where ltype='library' or ltype='hostel' or ltype='Teacher'or ltype='Transport' or ltype='Transport_AO'";
	String GET_All="select * from login_master";
	
	String INSERT_TEACHER_SUBJECT_ASSIGN="INSERT INTO subject_assign VALUES (?,?,?,?,?,?,?)";
	String UPDATE_TEACHER_SUBJECT_ASSIGN="update subject_assign where subject_handle=?";
	String DELETE_TEACHER_SUBJECT_ASSIGN="delete from subject_assign where subject_handle=?";
	
	String INSERT_ATTENDANCE_TEACHER="insert into attendance_teacher(adate,checkin,tid,tname) values(?,?,?,?)";
	
	String UPDATE_ATTENDANCE_TEACHER="update attendance_staff set checkout=?,att_status=? where adate=? and tid=?";
	
	String INSERT_ATTENDANCE_STAFF="insert into attendance_staff(adate,checkin,tid,tname,ttype) values(?,?,?,?,?)";
	
	
	String UPDATE_ATTENDANCE_STAFF="update attendance_staff set checkout=?,att_status=? where adate=? and tid=?";
	
	String INSERT_COMPOSE="INSERT INTO mail_master VALUES (?,?,?,?,?,?,?,?)";
	String INSERT_GROUP="INSERT INTO group_create VALUES (?,?)";
	String INSERT_COMPOSE_BACKUP="INSERT INTO mail_master_backup VALUES (?,?,?,?,?,?,?,?)";
	String DELETE_COMPOSE="delete from mail_master where mdate=? and mtime=?";
	
	
	String UPDATE_COUNTER_COUNT="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=104";
	String GET_TEACHER_LIST="select * from teacher_master";
	String GET_TEACHER_LIST_ALL="select * from teacher_master where Tid=:techid";
	String GET_TEACHER_CLASS_LIST_ALL="select a.tid,a.tname,a.sclass,a.section,a.subject_handle,b.tid from subject_assign a teacher_master b where a.tid=b.Tid";
	
	
	String GET_TEACHER_SUBJECT="select * from subject_assign where sclass=:sclass and section=:section";
	
	
	String GET_INBOX="select * from mail_master where mail_to=:mail_to ORDER BY mdate DESC,mtime DESC LIMIT 5";
	String GET_INBOX_GROUP="select * from group_create where group_name=:group_name";
	String GET_INBOX_DATE="select * from mail_master where mail_to=:mail_to and mdate=:mdate";
	String GET_INBOX_DATE_TIME="select * from mail_master where mail_to=:mail_to and mdate=:mdate and mtime=:mtime";
	
	String GET_INBOX_SEND="select * from mail_master where mail_from=:mail_from";
	String GET_INBOX_TRASH="select * from mail_master_backup where mail_to=:mail_to";
	

	String GET_TEACHER_SUBJECT_ASSIGN="select * from subject_assign where sclass=:sclass and section=:section";
	String GET_SUBJECT_ASSIGN1="select * from subject_assign where tid=:tid and sclass=:sclass and section=:section";
	String GET_SUBJECT_ASSIGN1_ID="select * from subject_assign where tid=:tid";
	String GET_SUBJECT_ASSIGN1_TYPE="select * from subject_assign where ttype=:ttype and mobile=:mobile group by sclass,section";
	String GET_SUBJECT_ASSIGN1_CLASS="select * from subject_assign where sclass=:sclass and ttype=:ttype and mobile=:mobile group by section";
	String GET_SUBJECT_ASSIGN1_CLASS_SECTION="select * from subject_assign where sclass=:sclass and section=:section and ttype=:ttype and mobile=:mobile";
	
	String GET_ID="select * from teacher_master where mobile=:uname or cemail=:uname";
	
	String GET_TEACHER_TYPE="select * from subject_assign where mobile=:mobile and ttype=:ttype";
	
	
	String GET_TEACHER_ATTENDANCE="select * from attendance_staff where adate=:adate and tid=:tid";
	String GET_STAFF_ATTENDANCE="select * from attendance_staff where adate=:adate and tid=:tid";
	
	
	String GET_TEACHER_ATTENDANCE_HISTORY="select * FROM attendance_staff where adate between :fdate and :tdate";
	
	//String GET_TEACHER_ATTENDANCE_HISTORY="select a.adate,a.checkin,a.checkout,a.tid,a.tname,a.att_status from colegio_devp.attendance_staff a,colegio_devp.attendance_teacher where a.adate between :fdate and :tdate group by a.tid";

	String GET_DL_LIST = "select group_name from colegio_devp.group_create group by group_name";
	
	String DELETE_GROUP="delete from group_create where group_name=?";

	String UPDATE_GROUP = "update group_create set group_name=?";

	
}
