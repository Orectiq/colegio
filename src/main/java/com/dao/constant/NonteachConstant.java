package com.dao.constant;

public interface NonteachConstant {
	
	
	String GET_LIBID="SELECT A.cntNum, (A.cntCount) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 109";
	String UPDATE_COUNTER_COUNT_LIBID="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=109";
	
	String GET_DORID="SELECT A.cntNum, (A.cntCount) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 110";
	String UPDATE_COUNTER_COUNT_GET_DORID="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=110";
	
	String GET_TRAID="SELECT A.cntNum, (A.cntCount) as referenceno FROM " + CounterTableVO.TYPE
			+ " as A WHERE A.cntModId = 111";
	String UPDATE_COUNTER_COUNT_TRAID="update counter_table set Cnt_Count=Cnt_Count+1 where Cnt_Mod_Id=111";
	
	
	String INSERT_NONTEACH="INSERT INTO nonteachingstaff VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String UPDATE_NONTEACHER="update nonteachingstaff set Department=?,Name=?,Age=?,DOB=?,Gender=?,Phone_Number=?,Contact_Number=?,Email=?,DOJ=?,Work_Area=?,Current_Address=?,Permanent_Address=? where Staff_ID=?";
	String DELETE_TEACHER="delete from nonteachingstaff where Staff_ID=?";
	String DELETE_LOGIN="delete from login_master where sid=?";
	
	
	
	
	String GET_NONTEACH_LIST="select * from nonteachingstaff";

	String GET_NONTEACH_LIST_ALL="select * from nonteachingstaff where Department=:department";
	String GET_NONTEACH_LOGINID="select * from nonteachingstaff where Staff_ID=:Staff_ID";
	String GET_NONTEACH_LOGINID_PHONE="select * from nonteachingstaff where Phone_Number=:Phone_Number";
	String GET_NONTEACH_ATTENDANCE="select * FROM attendance_staff where adate between :fdate and :tdate and tid=:tid";

}
