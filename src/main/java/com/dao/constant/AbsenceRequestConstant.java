package com.dao.constant;

public interface AbsenceRequestConstant 
{
	
 String INSERT_AbsenceRequest="INSERT INTO absence_request VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
 String INSERT_AbsenceStuRequest="INSERT INTO absence_student_request VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
 
 String GET_AbsenceRequest_LIST_ALL="select * from absence_request";
 String GET_AbsenceRequest_TEACHER_LIST="select * from absence_request where theDate=:rdate and status='Apply'";
 String GET_AbsenceRequest_TEACHER_NOTIFY_LIST="select * from absence_request where theDate=:theDate and Teacher_Id=:Teacher_id and status='Apply'";
 String GET_AbsenceRequest_TEACHER_NOTIFY_LIST1="select * from absence_request where theDate=:theDate and Teacher_Id=:Teacher_id and time=:time and status='Apply'";
 
 String GET_AbsenceRequest_STUDENT_LIST="select * from absence_student_request where thedate=:rdate and status='Apply'";
 
 String GET_TEACHER_GRANT_LIST="select * from absence_request where Teacher_Id=:Teacher_Id  ORDER BY theDate ASC LIMIT 5";
 String GET_STUDENT_GRANT_LIST="select * from absence_student_request where student_id=:student_id";
 
 String UPDATE_TEACHER_LEAVE="update absence_request set status=? where Teacher_Id=? and theDate=? and time=?";
 String UPDATE_STUDENT_LEAVE="update absence_student_request set status=?,approved_by=? where student_id=? and thedate=? and time=?";
 
 String UPDATE_TEACHER_MASTER="update teacher_master set lapply=lapply+?,lbalance=lbalance-? where Tid=?";
	
}
