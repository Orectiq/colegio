package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dao.HomeworkDao;
import com.dao.constant.HomeworkConstant;
import com.model.HomeworkStatusVO;
import com.model.HomeworkVO;
import com.model.StuVO;


@Repository
public class HomeworkDaoImpl implements HomeworkDao 
{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
	
	
	
	@Autowired
	 DataSource dataSource;

	//@Override
	public HomeworkVO insertHomeworkDetails(HomeworkVO homeworkVO) throws Exception 
	{
		
		HomeworkVO Homeworkvo = new HomeworkVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		// TODO Auto-generated method stub
		
		
		System.out.println("Enter 1");
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			System.out.println("Enter 2");
			
			
			String sql =HomeworkConstant.INSERT_Homework ;
			System.out.println("Enter 2");
					  jdbcTemplate.update(sql,new Object[] {
							  
							  homeworkVO.getTheDate(),
							  homeworkVO.getsClass1(),homeworkVO.getSection1(),
							  homeworkVO.getTeacher_Name(),homeworkVO.getTeacher_Id(),	
							  homeworkVO.getTtype(),homeworkVO.getMobile(),homeworkVO.getSubject(),
							  homeworkVO.getHomeWork(),
							  homeworkVO.getIsActive(),homeworkVO.getInserted_By(),homeworkVO.getInserted_Date(),
							  homeworkVO.getUpdated_By(),homeworkVO.getUpdated_Date()
							  });
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Homeworkvo;
	}
	//insertHomeworkStatus
	public HomeworkStatusVO insertHomeworkStatus(HomeworkStatusVO homeworkVO) throws Exception 
	{
		
		HomeworkStatusVO Homeworkvo = new HomeworkStatusVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();

		System.out.println("Enter 1");
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			System.out.println("Enter 2");

			String sql =HomeworkConstant.INSERT_HomeworkStatus ;
			System.out.println("Enter 2");
					  jdbcTemplate.update(sql,new Object[] {
							  
							  homeworkVO.getTheDate(),homeworkVO.getStudent_id(),homeworkVO.getsClass1(),homeworkVO.getSection1(),
							  homeworkVO.getTeacher_Name(),homeworkVO.getTeacher_Id(),homeworkVO.getTtype(),
							  homeworkVO.getMobile(),homeworkVO.getSubject(),homeworkVO.getHomeWork(),
							  homeworkVO.getStatus(),homeworkVO.getRemark()
							 
							 });
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Homeworkvo;
	}
	
	

	@SuppressWarnings("unchecked")	
	public List<HomeworkVO> getLtrReferenceData() throws Exception 
	{
		List<HomeworkVO> homeworkServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			homeworkServiceVOList = session.createSQLQuery(HomeworkConstant.GET_Homework_LIST_ALL)
					.setResultTransformer(Transformers.aliasToBean(HomeworkVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return homeworkServiceVOList;
	}
	
	//getHomeworkdata
	@SuppressWarnings("unchecked")	
	public List<HomeworkVO> getHomeworkdata(String sclass,String sec,String ttype,String tid,String dt) throws Exception 
	{
		List<HomeworkVO> homeworkServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			homeworkServiceVOList = session.createSQLQuery(HomeworkConstant.GET_Homework_LIST)
					.setParameter("sclass", sclass)
					.setParameter("sec", sec)
					.setParameter("ttype", ttype)
					.setParameter("tid", tid)
					.setParameter("dt", dt)
					.setResultTransformer(Transformers.aliasToBean(HomeworkVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return homeworkServiceVOList;
	}
	
	// data: {"sClass1":sclass,"Section1":sec,"theDate":today},
	@SuppressWarnings("unchecked")	
	public List<HomeworkVO> getHomeworkStudata(String sclass,String sec,String thedate) throws Exception 
	{
		List<HomeworkVO> homeworkServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			homeworkServiceVOList = session.createSQLQuery(HomeworkConstant.GET_Homework_Stu_LIST)
					.setParameter("sClass1", sclass)
					.setParameter("Section1", sec)
					.setParameter("theDate", thedate)
					.setResultTransformer(Transformers.aliasToBean(HomeworkVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return homeworkServiceVOList;
	}
	
	//getHomeworkStatus
	@SuppressWarnings("unchecked")	
	public List<HomeworkStatusVO> getHomeworkStatus(String tid,String thedate,String sid) throws Exception 
	{
		List<HomeworkStatusVO> homeworkStatusServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			homeworkStatusServiceVOList = session.createSQLQuery(HomeworkConstant.GET_Homework_Status_LIST)
					.setParameter("teacher_id", tid)
					.setParameter("thedate", thedate)
					.setParameter("student_id", sid)
					.setResultTransformer(Transformers.aliasToBean(HomeworkStatusVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return homeworkStatusServiceVOList;
	}
	
	//getHomeWorkStuStatus
	@SuppressWarnings("unchecked")	
	public List<HomeworkStatusVO> getHomeWorkStuStatus(String sclass,String sec,String mobile,String subj) throws Exception 
	{
		List<HomeworkStatusVO> homeworkStatusServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			homeworkStatusServiceVOList = session.createSQLQuery(HomeworkConstant.GET_Homework_Status_LIST_STU)
					.setParameter("sClass1", sclass)
					.setParameter("Section1", sec)
					.setParameter("mobile", mobile)
					.setParameter("Subject", subj)
					.setResultTransformer(Transformers.aliasToBean(HomeworkStatusVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return homeworkStatusServiceVOList;
	}
}


	