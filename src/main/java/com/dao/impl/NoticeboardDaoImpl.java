package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dao.NoticeboardDao;
import com.dao.constant.NoticeboardConstant;
import com.dao.constant.StudentConstant;
import com.dao.constant.TeacherConstant;
import com.dao.constant.TransportConstant;
import com.model.Nb_newsVO;
import com.model.NoticeboardVO;
import com.model.StuVO;

@Repository
public class NoticeboardDaoImpl implements NoticeboardDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Autowired
	DataSource dataSource;

	public NoticeboardVO insertEventDetails(NoticeboardVO noticeboardVO) throws Exception {
		System.out.println("Enter Noticeboard DAO Impl ----------------------------->" + noticeboardVO.getDoe());
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
		Session session = sessionFactory.openSession();
		org.hibernate.Transaction trx = session.beginTransaction();
		
		
		String tran_type=noticeboardVO.getTran_typ();
		System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type);
		if(tran_type.equals("insert"))
		{
		
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

			String sql_count4=StudentConstant.UPDATE_COUNTER_COUNT_EVENTS ;
			jdbcTemplate.update(sql_count4);
			
			
			String sql = NoticeboardConstant.INSERT_NOTICEBOARD;
			jdbcTemplate.update(sql,
					new Object[] { noticeboardVO.getEvent_tittle(), noticeboardVO.getEvent_desc(),
							noticeboardVO.getEvent_place(), 
							noticeboardVO.getN_student(), noticeboardVO.getN_non_teach_staff(),	noticeboardVO.getN_parent(),noticeboardVO.getN_teacher(), 
							noticeboardVO.getDoe(), noticeboardVO.getIsActive(),
							noticeboardVO.getInserted_By(), noticeboardVO.getInserted_Date(),
							noticeboardVO.getUpdated_By(), noticeboardVO.getUpdated_Date(),noticeboardVO.getToe(),noticeboardVO.getEventsid() });

			System.out.println("Success insert................");
			trx.commit();

		} catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>" + e);
			throw new Exception(e);
		}
		}
		else if(tran_type.equals("edit"))
		{
			System.out.println("********************************* Edit  ***************************   "+noticeboardVO.getEventsid());
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql = NoticeboardConstant.UPDATE_EVENT;
						  jdbcTemplate.update(sql,new Object[] {
								  noticeboardVO.getEvent_tittle(), noticeboardVO.getEvent_desc(),
									noticeboardVO.getEvent_place(), noticeboardVO.getEventsid()
									 });
		
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			
		}
	 if(tran_type.equals("delete"))
		{
			System.out.println("********************************* delete  ***************************");
			try {
				
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =NoticeboardConstant.DELETE_EVENT ;
						  jdbcTemplate.update(sql,new Object[] {
								  noticeboardVO.getEventsid()
						 
						  });
						  
				
				System.out.println("Success Delete................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}
		}
		return Noticeboardvo;
	}

	public Nb_newsVO insertNewsDetails(Nb_newsVO nb_newsVO) throws Exception {
		Nb_newsVO Nb_newsvo = new Nb_newsVO();
		Session session = sessionFactory.openSession();
		org.hibernate.Transaction trx = session.beginTransaction();
		
		
		String tran_type=nb_newsVO.getTran_typ();
		
		System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type+"news tittle"+nb_newsVO.getNews_tittle());
		
		
		
		if(tran_type.equals("insert"))
		{
		
		try {

			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

			
			String sql_count4=StudentConstant.UPDATE_COUNTER_COUNT_NEWS ;
			jdbcTemplate.update(sql_count4);
			
			String sql = NoticeboardConstant.INSERT_NB_NOTICEBOARD;
			jdbcTemplate.update(sql,
					new Object[] { nb_newsVO.getNews_tittle(), nb_newsVO.getNews_context(), nb_newsVO.getN_student(),
							nb_newsVO.getN_parent(), nb_newsVO.getN_teacher(), nb_newsVO.getN_non_teach_staff(),
							nb_newsVO.getDop(), nb_newsVO.getIsActive(), nb_newsVO.getInserted_By(),
							nb_newsVO.getInserted_Date(), nb_newsVO.getUpdated_By(), nb_newsVO.getUpdated_Date(),nb_newsVO.getTop(),nb_newsVO.getNewsid() });
			
			
			

			System.out.println("Success insert................");
			trx.commit();
			
		} catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>" + e);
			throw new Exception(e);
		}

		}
		

		
		else if(tran_type.equals("edit"))
		{
			System.out.println("************* Edit  *********** Title  : "+nb_newsVO.getNews_tittle()+" context : "+nb_newsVO.getNews_context()+" news id   "+nb_newsVO.getNewsid());
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				
						  String sql1 =NoticeboardConstant.UPDATE_NB_NOTICEBOARD;
						  jdbcTemplate.update(sql1,new Object[] {
								  nb_newsVO.getNews_tittle(), nb_newsVO.getNews_context(),nb_newsVO.getNewsid() });
				
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			
		}
		if(tran_type.equals("delete"))
		{
			System.out.println("********************************* delete  ***************************"+nb_newsVO.getNews_tittle()+" id   "+nb_newsVO.getNewsid());
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =NoticeboardConstant.DELETE_NB_NOTICEBOARD;
						  jdbcTemplate.update(sql,new Object[] {
								  nb_newsVO.getNewsid()
						 
						  });
						  
				
				System.out.println("Success Delete................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
		}
		


		return Nb_newsvo;
	}

	public List<NoticeboardVO> geteventdetail(String doe,String ltype) throws Exception {

		List<NoticeboardVO> noticeboardServiceVOList = null;

		Session session = sessionFactory.openSession();

	  if(ltype.equals("Teacher"))
		{	
			System.out.println("teaching   "+ltype);
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_TEACHER)
						.setParameter("doe", doe)
						.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
				} catch (Exception e) {
				System.out.println("Error in DAOIMPL " + e);
			}
		}
		else if(ltype.equals("library") || ltype.equals("hostel") || ltype.equals("Transport_AO") || ltype.equals("Transport"))
		{	
			System.out.println("Nonteaching   "+ltype);
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_NONTECH)
						.setParameter("doe", doe)
						.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
				} catch (Exception e) {
				System.out.println("Error in DAOIMPL " + e);
			}
		}
		if(ltype.equals("Students") || ltype.equals("student") )
		{	
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_STUDENTS)
						.setParameter("doe", doe)
						.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
				} catch (Exception e) {
				System.out.println("Error in DAOIMPL " + e);
			}
		}
		if(ltype.equals("Parent"))
		{	
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_PARENTS)
						.setParameter("doe", doe)
						.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
				} catch (Exception e) {
				System.out.println("Error in DAOIMPL " + e);
			}
		}
		
		
		
		return noticeboardServiceVOList;
	}

	public List<Nb_newsVO> getnewsdetail(String dop) throws Exception {

		List<Nb_newsVO> newsboardServiceVOList = null;
		Session session = sessionFactory.openSession();
		
		
		
		try {
			newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST)
					.setParameter("dop", dop)
					.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL " + e);
		}
		return newsboardServiceVOList;

	}
	public List<NoticeboardVO> getevent() throws Exception {

		List<NoticeboardVO> noticeboardServiceVOList = null;

		Session session = sessionFactory.openSession();
		try {
			noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_TEACHER)
					.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL " + e);
		}
		return noticeboardServiceVOList;

	}

	
	public List<NoticeboardVO> geteventdetailAll() throws Exception {

		List<NoticeboardVO> noticeboardServiceVOList = null;

		Session session = sessionFactory.openSession();
		try {
			noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_LIST_ALL)
					.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL " + e);
		}
		return noticeboardServiceVOList;

	}

	public List<Nb_newsVO> getnewsdetailAll() throws Exception {

		List<Nb_newsVO> newsboardServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_All)
					.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL " + e);
		}
		return newsboardServiceVOList;

	}
	
	//geteventdetailNext
	public List<NoticeboardVO> geteventdetailNext() throws Exception {

		List<NoticeboardVO> noticeboardServiceVOList = null;

		Session session = sessionFactory.openSession();
		try {
			noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_LIST_ALL1)
					.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL " + e);
		}
		return noticeboardServiceVOList;

	}
	
	//geteventdetailType
	public List<NoticeboardVO> geteventdetailType(String ttype,String doe) throws Exception {

		System.out.println("DAO impl Type  "+ttype+"doe"+doe);
		
		List<NoticeboardVO> noticeboardServiceVOList = null;
		Session session = sessionFactory.openSession();
		
		if(ttype.equals("Teacher"))
		{	
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_TEACHER)
						.setParameter("doe", doe)
						.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
				} catch (Exception e) {
				System.out.println("Error in DAOIMPL " + e);
			}
		}
		else if(ttype.equals("library") || ttype.equals("hostel") || ttype.equals("Transport_AO") || ttype.equals("Transport"))
		{	
			System.out.println("Nonteaching   "+ttype);
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_NONTECH)
						.setParameter("doe", doe)
						.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
				} catch (Exception e) {
				System.out.println("Error in DAOIMPL " + e);
			}
		}
		if(ttype.equals("Students") || ttype.equals("student") )
		{	
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_STUDENTS)
						.setParameter("doe", doe)
						.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
				} catch (Exception e) {
				System.out.println("Error in DAOIMPL " + e);
			}
		}
		if(ttype.equals("Parent"))
		{	
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_PARENTS)
						.setParameter("doe", doe)
						.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
				} catch (Exception e) {
				System.out.println("Error in DAOIMPL " + e);
			}
		}
		
		
		
		return noticeboardServiceVOList;

	}
	
	//getupcommingevents
		public List<NoticeboardVO> getupcommingevents(String ttype) throws Exception {

			System.out.println("DAO impl Type  "+ttype);
			
			List<NoticeboardVO> noticeboardServiceVOList = null;
			Session session = sessionFactory.openSession();
			
			if(ttype.equals("Teacher"))
			{	
				try {
					noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_TEACHER_LIST_ALL)
							.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
					} catch (Exception e) {
					System.out.println("Error in DAOIMPL " + e);
				}
			}
			else if(ttype.equals("library") || ttype.equals("hostel") || ttype.equals("Transport_AO") || ttype.equals("Transport"))
			{	
				System.out.println("Nonteaching   "+ttype);
				try {
					noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_NONTECH_LIST_ALL)
							.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
					} catch (Exception e) {
					System.out.println("Error in DAOIMPL " + e);
				}
			}
			if(ttype.equals("Students") || ttype.equals("student") )
			{	
				try {
					noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_STUDENTS_LIST_ALL)
							.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
					} catch (Exception e) {
					System.out.println("Error in DAOIMPL " + e);
				}
			}
			if(ttype.equals("Parent"))
			{	
				try {
					noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENT_PARENTS_LIST_ALL)
							.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
					} catch (Exception e) {
					System.out.println("Error in DAOIMPL " + e);
				}
			}
			
			
			
			return noticeboardServiceVOList;

		}
		
	
	
	//getTeachernewsdetail
	public List<Nb_newsVO> getTeachernewsdetail(String ttype) throws Exception {

		System.out.println("dao  type !!!!!!!!!!! "+ttype);
		
		List<Nb_newsVO> newsboardServiceVOList = null;
		Session session = sessionFactory.openSession();
		
		
		if(ttype.equals("Teacher"))
		{	
			System.out.println("Type  "+ttype);
			try {
				newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_TEACHER)
					.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		
		else if(ttype.equals("library"))
		{	
			System.out.println("Type  "+ttype);
			try {
				newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_LIBRARY)
					.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		else if(ttype.equals("non_tech"))
		{	
			System.out.println("Type  "+ttype);
			try {
				newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH)
					.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		else if(ttype.equals("Students") || ttype.equals("student"))
		{	
			System.out.println("Type  "+ttype);
			try {
				newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_STUDENTS)
					.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		//Parents
		else if(ttype.equals("Parent"))
		{	
			System.out.println("Type  "+ttype);
			try {
				newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_PARENTS)
					.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		//hostel
				else if(ttype.trim().equals("hostel"))
				{	
					System.out.println("Type  "+ttype);
					try {
						newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH)
							//.setParameter("ltype",ttype)
							//.setParameter("dop", dop)
							.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
					} catch (Exception e) {
						System.out.println("Error in DAOIMPL "+e);
					}
				}
		//Transport_AO
				else if(ttype.equals("Transport_AO"))
				{	
					System.out.println("Type  "+ttype);
					System.out.println("%%%%%%%%%%%%%%%%%%%%%%%  Transport_AO" );
					try {
						newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH)
							//.setParameter("ltype",ttype)
							//.setParameter("dop", dop)
							.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
					} catch (Exception e) {
						System.out.println("Error in DAOIMPL "+e);
					}
				}
		//Transport_Driver
				else if(ttype.equals("Transport"))
				{	
					System.out.println("Type  "+ttype);
					System.out.println("%%%%%%%%%%%%%%%%%%%%%%%  Transport_AO" );
					try {
						newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH)
							//.setParameter("ltype",ttype)
							//.setParameter("dop", dop)
							.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
					} catch (Exception e) {
						System.out.println("Error in DAOIMPL "+e);
					}
				}
		
		return newsboardServiceVOList;
		
		

	}
	
	//getTeachernewsdetail
		public List<Nb_newsVO> getTeachernewsdetaildate(String dop,String ttype) throws Exception {

			System.out.println("dao  type !!!!!!!!!!! "+ttype+"dop"+dop);
			
			List<Nb_newsVO> newsboardServiceVOList = null;
			Session session = sessionFactory.openSession();
			
			
			if(ttype.equals("Teacher"))
			{	
				System.out.println("Type  "+ttype+"dop"+dop);
				try {
					newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_TEACHER_DATE)
							.setParameter("dop", dop)
						  /////  .setParameter("ttype",ttype)
							.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
				} catch (Exception e) {
					System.out.println("Error in DAOIMPL "+e);
				}
			}
			
			else if(ttype.equals("library"))
			{	
				System.out.println("Type  "+ttype);
				try {
					newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_LIBRARY_DATE)
							.setParameter("dop", dop)
						.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
				} catch (Exception e) {
					System.out.println("Error in DAOIMPL "+e);
				}
			}
			else if(ttype.equals("non_tech"))
			{	
				System.out.println("Type  "+ttype);
				try {
					newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH_DATE)
							.setParameter("dop", dop)
						.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
				} catch (Exception e) {
					System.out.println("Error in DAOIMPL "+e);
				}
			}
			else if(ttype.equals("Students") || ttype.equals("student"))
			{	
				System.out.println("Type  "+ttype);
				try {
					newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_STUDENTS_DATE)
							.setParameter("dop", dop)
						.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
				} catch (Exception e) {
					System.out.println("Error in DAOIMPL "+e);
				}
			}
			//Parents
			else if(ttype.equals("Parent"))
			{	
				System.out.println("Type  "+ttype);
				try {
					newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_PARENTS_DATE)
							.setParameter("dop", dop)
						.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
				} catch (Exception e) {
					System.out.println("Error in DAOIMPL "+e);
				}
			}
			//hostel
					else if(ttype.trim().equals("hostel"))
					{	
						System.out.println("Type  "+ttype);
						try {
							newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH_DATE)
									.setParameter("dop", dop)
								//.setParameter("ltype",ttype)
								//.setParameter("dop", dop)
								.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
						} catch (Exception e) {
							System.out.println("Error in DAOIMPL "+e);
						}
					}
			//Transport_AO
					else if(ttype.equals("Transport_AO"))
					{	
						System.out.println("Type  "+ttype);
						System.out.println("%%%%%%%%%%%%%%%%%%%%%%%  Transport_AO" );
						try {
							newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH_DATE)
									.setParameter("dop", dop)
								//.setParameter("ltype",ttype)
								//.setParameter("dop", dop)
								.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
						} catch (Exception e) {
							System.out.println("Error in DAOIMPL "+e);
						}
					}
			//Transport_Driver
					else if(ttype.equals("Transport"))
					{	
						System.out.println("Type  "+ttype);
						System.out.println("%%%%%%%%%%%%%%%%%%%%%%%  Transport_AO" );
						try {
							newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH_DATE)
									.setParameter("dop", dop)
								//.setParameter("ltype",ttype)
								//.setParameter("dop", dop)
								.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
						} catch (Exception e) {
							System.out.println("Error in DAOIMPL "+e);
						}
					}
			
			return newsboardServiceVOList;
			
			

		}
	
	
	
	
	//getnewsdetail_title
	public List<Nb_newsVO> getnewsdetail_title(String title) throws Exception {

		List<Nb_newsVO> newsboardServiceVOList = null;

		Session session = sessionFactory.openSession();
		try {
			newsboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NEWSBOARD_TITLE)

					.setParameter("newsid",title)

					.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL " + e);
		}
		return newsboardServiceVOList;

	}
	
	//geteventdetail_title
	public List<NoticeboardVO> geteventdetail_title(String title) throws Exception {

		List<NoticeboardVO> noticeboardServiceVOList = null;

		Session session = sessionFactory.openSession();
		try {
			noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_TITLE)
					.setParameter("eventsid",title)
					.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL " + e);
		}
		return noticeboardServiceVOList;

	}

	public List<NoticeboardVO> getevent(String ttype) throws Exception {
		// TODO Auto-generated method stub
		List<NoticeboardVO> noticeboardServiceVOList = null;
		Session session = sessionFactory.openSession();
		
		
		if(ttype.equals("Teacher"))
		{	
			System.out.println("Type  "+ttype);
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENTS_LIST_TEACHER)
					.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		
		else if(ttype.equals("library"))
		{	
			System.out.println("Type  "+ttype);
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENTS_LIST_LIBRARY)
					.setResultTransformer(Transformers.aliasToBean(Nb_newsVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		else if(ttype.equals("non_tech"))
		{	
			System.out.println("Type  "+ttype);
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENTS_LIST_NONTECH)
					.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		else if(ttype.equals("Students") || ttype.equals("student"))
		{	
			System.out.println("Type  "+ttype);
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_EVENTS_LIST_STUDENTS)
					.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		//Parents
		else if(ttype.equals("Parents"))
		{	
			System.out.println("Type  "+ttype);
			try {
				noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_PARENTS)
					.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		//hostel
				else if(ttype.trim().equals("hostel"))
				{	
					System.out.println("Type  "+ttype);
					try {
						noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH)
							//.setParameter("ltype",ttype)
							//.setParameter("dop", dop)
							.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
					} catch (Exception e) {
						System.out.println("Error in DAOIMPL "+e);
					}
				}
		//Transport_AO
				else if(ttype.equals("Transport_AO"))
				{	
					System.out.println("Type  "+ttype);
					System.out.println("%%%%%%%%%%%%%%%%%%%%%%%  Transport_AO" );
					try {
						noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH)
							//.setParameter("ltype",ttype)
							//.setParameter("dop", dop)
							.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
					} catch (Exception e) {
						System.out.println("Error in DAOIMPL "+e);
					}
				}
		//Transport_Driver
				else if(ttype.equals("Transport"))
				{	
					System.out.println("Type  "+ttype);
					System.out.println("%%%%%%%%%%%%%%%%%%%%%%%  Transport_AO" );
					try {
						noticeboardServiceVOList = session.createSQLQuery(NoticeboardConstant.GET_NOTICEBOARD_NEWS_LIST_NONTECH)
							//.setParameter("ltype",ttype)
							//.setParameter("dop", dop)
							.setResultTransformer(Transformers.aliasToBean(NoticeboardVO.class)).list();
					} catch (Exception e) {
						System.out.println("Error in DAOIMPL "+e);
					}
				}
		
		return noticeboardServiceVOList;
		
		

	}

	
	
	//getTeachernewsdetail
	
	
}
