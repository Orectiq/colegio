package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.common.ExamTypeMasterVO;
import com.dao.ExamDao;
import com.dao.constant.ExamConstant;
import com.dao.constant.StudentConstant;
import com.dao.constant.TeacherConstant;
import com.model.ExamNewVO;
import com.model.ExamStudentVO;
import com.model.ExamVO;
import com.model.MarksEntryVO;
import com.model.MarksTempVO;
import com.model.MarksdumpVO;
import com.model.MarksentryallVO;
import com.model.StuParVO;


public class ExamDaoImpl implements ExamDao{
private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Autowired
	DataSource dataSource;
	
	//@Override
	public ExamNewVO insertExamDetails(ExamNewVO examVO) throws Exception
	{
		System.out.println("Exam DAOIMPL ");
		ExamNewVO Examvo = new ExamNewVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();

	
		String tran_type=examVO.getTran_typ();

		System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type);
		if(tran_type.equals("insert"))
		{	
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			
			String sql =ExamConstant.INSERT_EXAM ;
			
					  jdbcTemplate.update(sql,new Object[] {
							  examVO.getExam_id(),examVO.getExam_name(),examVO.getExam_type(),examVO.getE_class(),
							  examVO.getSection(),examVO.getSubject(),examVO.getE_date(),examVO.getE_time(),
							  examVO.getE_mark()});
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		}
		
		else if(tran_type.equals("edit"))
		{
			System.out.println("********************************* Edit  ***************************");
			try {
JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =ExamConstant.UPDATE_EXAM ;
						  jdbcTemplate.update(sql,new Object[] {
								  examVO.getExam_name(),examVO.getExam_type(),examVO.getE_class(),examVO.getSection(),
								  examVO.getExam_id()
						  
						  });
				
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			
		}
	 if(tran_type.equals("delete"))
		{
			System.out.println("********************************* delete exammmmmmmmmmmmm  ***************************");
			try {
           JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =ExamConstant.DELETE_EXAM ;
						  jdbcTemplate.update(sql,new Object[] {
								  examVO.getExam_id()
						 
						  });
						  
			
				
				System.out.println("Success Delete Exam  ...............");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
		}
		
				 
		
		return Examvo;

		
	}
	
	
	@SuppressWarnings("unchecked")	
	public List<ExamVO> getLtrReferenceData() throws Exception 
	{
		List<ExamVO> examServiceVOList = null;
	
		
		Session session = sessionFactory.openSession();
		try {
			examServiceVOList = session.createSQLQuery(ExamConstant.GET_EXAM_LIST)
					.setResultTransformer(Transformers.aliasToBean(ExamVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return examServiceVOList;
	} 
	//getExamData
	@SuppressWarnings("unchecked")	
	public List<ExamNewVO> getExamData() throws Exception 
	{
		List<ExamNewVO> examNewServiceVOList = null;
	
		
		Session session = sessionFactory.openSession();
		try {
			examNewServiceVOList = session.createSQLQuery(ExamConstant.GET_EXAM_LIST_NEW)
					.setResultTransformer(Transformers.aliasToBean(ExamNewVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return examNewServiceVOList;
	} 
	
	@SuppressWarnings("unchecked")	
	public ExamVO getLtrReferenceData100() throws Exception 
	{
		List<ExamVO> examServiceVOList = null;
		ExamVO Examvo = new ExamVO();
		
		Session session = sessionFactory.openSession();
		try {
			Examvo = (ExamVO) session.createSQLQuery(ExamConstant.GET_EXAM_LIST)
					.setResultTransformer(Transformers.aliasToBean(ExamVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return Examvo;
	}
	
	
	@SuppressWarnings("unchecked")	
	public List<ExamVO> getLtrReferenceDataall() throws Exception 
	{
		List<ExamVO> examServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			examServiceVOList = session.createSQLQuery(ExamConstant.GET_EXAM_LIST)
					.setResultTransformer(Transformers.aliasToBean(ExamVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return examServiceVOList;
	}


	public List<ExamNewVO> getExam(String sclass, String sec,String etype) throws Exception {
		System.out.println("getExam controller"+sclass+","+sec+"etype  "+etype);
		
		List<ExamNewVO> examNewServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			examNewServiceVOList = session.createSQLQuery(ExamConstant.GET_EXAM_LIST_ALL)
					.setParameter("sclass",sclass)
					.setParameter("sec", sec)
					.setParameter("etype", etype)
					.setResultTransformer(Transformers.aliasToBean(ExamNewVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return examNewServiceVOList;
	} 
	
	//getExamClass
	public List<ExamNewVO> getExamClass(String sclass, String sec) throws Exception {
		System.out.println("getExam controller"+sclass+","+sec);
		
		List<ExamNewVO> examNewServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			examNewServiceVOList = session.createSQLQuery(ExamConstant.GET_EXAM_LIST_CLASS_ALL)
					.setParameter("sclass",sclass)
					.setParameter("sec", sec)
					.setResultTransformer(Transformers.aliasToBean(ExamNewVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return examNewServiceVOList;
	} 
	
	
	//getClassMarks
	public List<MarksentryallVO> getClassMarks(String sclass, String sec,String etype,String eid) throws Exception {
		System.out.println("getExam controller"+sclass+","+sec+"etype  "+etype);
		
		List<MarksentryallVO> marksAllServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			marksAllServiceVOList = session.createSQLQuery(ExamConstant.GET_MARKS_LIST_ALL)
					.setParameter("sclass",sclass)
					.setParameter("ssec", sec)
					.setParameter("exam_type", etype)
					.setParameter("exam_id", eid)
					.setResultTransformer(Transformers.aliasToBean(MarksentryallVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksAllServiceVOList;
	} 
	
	//MarksentryallVO getClassSubject
	public List<MarksentryallVO> getClassSubject(String sclass, String sec) throws Exception {
		System.out.println("getExam controller"+sclass+","+sec);
		
		List<MarksentryallVO> marksAllServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			marksAllServiceVOList = session.createSQLQuery(ExamConstant.GET_MARKS_LIST_ALL_SUBJECT)
					.setParameter("sclass",sclass)
					.setParameter("ssec", sec)
					.setResultTransformer(Transformers.aliasToBean(MarksentryallVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksAllServiceVOList;
	} 
	
	//getClassStudents
	public List<MarksentryallVO> getClassStudents(String sclass, String sec) throws Exception {
		System.out.println("getExam controller"+sclass+","+sec);
		
		List<MarksentryallVO> marksAllServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			marksAllServiceVOList = session.createSQLQuery(ExamConstant.GET_MARKS_LIST_STUDENTS)
					.setParameter("sclass",sclass)
					.setParameter("ssec", sec)
					.setResultTransformer(Transformers.aliasToBean(MarksentryallVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksAllServiceVOList;
	} 
	
	//getClassExamid
	
	public List<MarksdumpVO> getClassExamid(/*String eid,String sclass, String sec*//*,String[] subj*/) throws Exception {
		//System.out.println("getExam controller"+sclass+","+sec);
		
		List<MarksdumpVO> marksDumpServiceVOList = null;
		Session session = sessionFactory.openSession();
		
		
	
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		/*String sql1="create or replace view markentryalldump as "+
						"SELECT exam_id,sclass,ssec,student_id,student_name,"+
						"GROUP_CONCAT(if(`subject` = 'Tamil', `mark`, NULL)) AS 'Tamil',"+ 
						"GROUP_CONCAT(if(`subject` = 'English', `mark`, NULL)) AS 'English',"+
						"GROUP_CONCAT(if(`subject` = 'Maths', `mark`, NULL)) AS 'Maths',"+
						"GROUP_CONCAT(if(`subject` = 'Science', `mark`, NULL)) AS 'Science'"+ 
						"FROM `marks_entry_all`"+ 
						"WHERE exam_id='"+eid+"' and sclass='"+sclass+"' and ssec='"+sec+"'"+ 
						"GROUP BY RTRIM(LTRIM(student_id))";
						jdbcTemplate.execute(sql1);
						
		String sql2="create or replace view markentryalldumpHead as "+
								"SELECT student_id,student_name,"+
								"GROUP_CONCAT(if(`subject` = 'Tamil', `mark`, NULL)) AS 'Tamil',"+ 
								"GROUP_CONCAT(if(`subject` = 'English', `mark`, NULL)) AS 'English',"+
								"GROUP_CONCAT(if(`subject` = 'Maths', `mark`, NULL)) AS 'Maths',"+
								"GROUP_CONCAT(if(`subject` = 'Science', `mark`, NULL)) AS 'Science'"+ 
								"FROM `marks_entry_all`"+ 
								"WHERE exam_id='"+eid+"' and sclass='"+sclass+"' and ssec='"+sec+"'"+ 
								"GROUP BY RTRIM(LTRIM(student_id))";
								jdbcTemplate.execute(sql2);
								
		String sql3="create or replace view colu as select COLUMN_NAME as cname from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='markentryalldumphead'";
		jdbcTemplate.execute(sql3);*/
		
		String sql2="create or replace view markentrydump11 as SELECT student_id,sclass,ssec,MAX(CASE WHEN t.subject = 'tamil' THEN t.mark ELSE NULL END) AS tamil,MAX(CASE WHEN t.subject = 'english' THEN t.mark ELSE NULL END) AS english,MAX(CASE WHEN t.subject = 'maths' THEN t.mark ELSE NULL END) AS maths,sum(mark) as total FROM marks_entry_all t GROUP BY t.student_id"; 
		jdbcTemplate.execute(sql2);
		String sql3="DROP TABLE IF EXISTS new_mark1";
		jdbcTemplate.execute(sql3);
		String sql4="CREATE TABLE new_mark1 SELECT * from markentrydump11";
		jdbcTemplate.execute(sql4);
		
		
		try {
			marksDumpServiceVOList = session.createSQLQuery(ExamConstant.GET_MARKS_LIST_EXAMID)
					.setResultTransformer(Transformers.aliasToBean(MarksdumpVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksDumpServiceVOList;
	} 
	
	
	//getMarksTemp
	public List<MarksTempVO> getMarksTemp(String eid,String sclass1,String[] myArray) throws Exception {
		
		
		
		
		List<MarksTempVO> marksTempServiceVOList = null;
		Session session = sessionFactory.openSession();
		//String sql1,sql2,sql3,sql4,sql5;
		
	    
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		String arrayall="";
		String tabArray="";
		for(int i=0;i<myArray.length;i++)
		{
			System.out.println("getExam controller  eid "+eid+" sclass "+sclass1+" array  "+myArray[i]);
			
			arrayall+="MAX(CASE WHEN t.subject = '"+myArray[i]+"' THEN t.mark ELSE NULL END) AS "+myArray[i]+",";
		    tabArray+=myArray[i]+" varchar(50),";
			
		}
		
		String sql33="DROP VIEW IF EXISTS markentrydump11";
		jdbcTemplate.execute(sql33);
		
		String sql2="create or replace view markentrydump11 as "+
				" SELECT exam_id,student_id,student_name,sclass,ssec,"+arrayall+
			    " sum(mark) as total FROM marks_entry_all t where sclass='"+sclass1+"' GROUP BY t.student_id";
		
		String sql4="CREATE TABLE new_mark1(student_id varchar(50),student_name varchar(50),sclass varchar(50),ssec varchar(50),"+tabArray+" total varchar(50)) SELECT * from markentrydump11";
		
		
		System.out.println("array all  value  -------------   "+sql2);
		System.out.println("Table array all  value  -------------   "+sql4);
		
		jdbcTemplate.execute(sql2);
		
		String sql3="DROP TABLE IF EXISTS new_mark1";
		jdbcTemplate.execute(sql3);
		
		jdbcTemplate.execute(sql4);
		
		try {
			marksTempServiceVOList = session.createSQLQuery(ExamConstant.GET_MARKS_LIST_EXAMID)
					.setParameter("exam_id", eid)
					.setResultTransformer(Transformers.aliasToBean(MarksTempVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksTempServiceVOList;
	} 
	
	
	
	
	
	
	
	//getMarksFinal
	public List<MarksdumpVO> getMarksFinal() throws Exception {
		System.out.println("getExam controller");
		
		List<MarksdumpVO> marksDumpServiceVOList = null;
		Session session = sessionFactory.openSession();
		
	
		
		try {
			marksDumpServiceVOList = session.createSQLQuery(ExamConstant.GET_MARKS_LIST_FINAL)
					.setResultTransformer(Transformers.aliasToBean(MarksdumpVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksDumpServiceVOList;
	} 
	
	
	
	
	//getClassStudentsMarks
	public List<MarksentryallVO> getClassStudentsMarks(String eid, String sid,String subj) throws Exception {
		System.out.println("getExam controller "+eid+"  sid  "+sid+"  subject  "+subj);
		
		List<MarksentryallVO> marksAllServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			marksAllServiceVOList = session.createSQLQuery(ExamConstant.GET_MARKS_SUBJECT)
					.setParameter("exam_id",eid)
					.setParameter("student_id", sid)
					.setParameter("subject", subj)
					.setResultTransformer(Transformers.aliasToBean(MarksentryallVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksAllServiceVOList;
	} 
	
	
	//getClassMarksStudents
	public List<MarksEntryVO> getClassMarksStudents(String sclass, String sec,String eid,String sid) throws Exception {
		System.out.println("getExam controller"+sclass+","+sec);
		
		List<MarksEntryVO> marksServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			marksServiceVOList = session.createSQLQuery(ExamConstant.GET_MARKS_LIST_ALL_STUDENTS)
					.setParameter("sclass",sclass)
					.setParameter("ssec", sec)
					//.setParameter("exam_type", etype)
					.setParameter("exam_id", eid)
					.setParameter("student_id", sid)
					.setResultTransformer(Transformers.aliasToBean(MarksEntryVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksServiceVOList;
	} 
	
	
	//List<ExamTypeMasterVO> getExamType()
	public List<ExamTypeMasterVO> getExamType() throws Exception
	{
		List<ExamTypeMasterVO> examtypeServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			examtypeServiceVOList = session.createSQLQuery(ExamConstant.GET_EXAMTYPE_ALL)
					.setResultTransformer(Transformers.aliasToBean(ExamTypeMasterVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return examtypeServiceVOList;
	} 
	//getStudentMarks
	
	public List<MarksEntryVO> getStudentMarks(String sid) throws Exception {
		System.out.println("getExam controller"+sid);
		
		List<MarksEntryVO> marksServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			marksServiceVOList = session.createSQLQuery(ExamConstant.GET_MARKS_STU_LIST)
					.setParameter("student_id",sid)
					
					.setResultTransformer(Transformers.aliasToBean(MarksEntryVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksServiceVOList;
	} 
	//getExamID
	public List<ExamNewVO> getExamID(String eid) throws Exception {
		System.out.println("getExam controller"+eid);
		
		List<ExamNewVO> examNewServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			examNewServiceVOList = session.createSQLQuery(ExamConstant.GET_EXAM_ID)
					.setParameter("exam_id",eid)
					
					.setResultTransformer(Transformers.aliasToBean(ExamNewVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return examNewServiceVOList;
	} 
	//getExamID_Subject
	public List<ExamNewVO> getExamID_Subject(String eid,String subj) throws Exception {
		System.out.println("getExam controller"+eid);
		
		List<ExamNewVO> examNewServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			examNewServiceVOList = session.createSQLQuery(ExamConstant.GET_EXAM_ID_SUBJECT)
					.setParameter("exam_id",eid)
					.setParameter("subject",subj)
					.setResultTransformer(Transformers.aliasToBean(ExamNewVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return examNewServiceVOList;
	}
	
	//getExamDetails
	public List<ExamStudentVO> getExamDetails(String sid) throws Exception {
		System.out.println("getExam controller"+sid);
		
		List<ExamStudentVO> examServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			examServiceVOList = session.createSQLQuery(ExamConstant.GET_STUDENT_EXAM)
					.setParameter("student_id",sid)
					
					.setResultTransformer(Transformers.aliasToBean(ExamStudentVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return examServiceVOList;
	} 
	
}
