package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dao.DormitoryDao;
import com.dao.constant.DormitoryConstant;
import com.dao.constant.StudentConstant;
import com.dao.constant.SubjectConstant;
import com.model.DormitoryAllotVO;
import com.model.DormitoryVO;
import com.model.StuVO;

@Repository
public class DormitoryDaoImpl implements DormitoryDao 
{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
	
	
	
	@Autowired
	 DataSource dataSource;

	//@Override
	public DormitoryVO insertDormitoryDetails(DormitoryVO dormitoryVO) throws Exception 
	{
		
		DormitoryVO Dormitoryvo = new DormitoryVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		System.out.println("DAO IMPL dormitory");
		System.out.println(dormitoryVO.getDormitory_name());
		System.out.println(dormitoryVO.getLocation());
		System.out.println(dormitoryVO.getFee());
		System.out.println(dormitoryVO.getCgender());
		System.out.println("DAO IMPL dormitory end");
		
		
		
		// TODO Auto-generated method stub
		
		
		System.out.println("Enter 1");
		
		String ttype=dormitoryVO.getTran_typ();
		
		if(ttype.equals("insert"))
		{
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			System.out.println("Enter 2");
			
			
			String sql =DormitoryConstant.INSERT_Dormitory ;
			System.out.println("Enter 2");
					  jdbcTemplate.update(sql,new Object[] {
							  dormitoryVO.getDormitory_name(),dormitoryVO.getLocation(), dormitoryVO.getFee(),dormitoryVO.getCgender(),
							  dormitoryVO.getDescription(),dormitoryVO.getNo_of_beds(),dormitoryVO.getWarden_Photo(),dormitoryVO.getName(),dormitoryVO.getAge(),
							  dormitoryVO.getContact_Number(),dormitoryVO.getAlt_Contact_Number(),dormitoryVO.getAddress(),dormitoryVO.getDeputyWarden_Photo(),dormitoryVO.getName1(),
							  dormitoryVO.getAge1(),dormitoryVO.getContact_Number1(),dormitoryVO.getAlt_Contact_Number1(),dormitoryVO.getAddress1(),
							  dormitoryVO.getIsActive(),dormitoryVO.getInserted_By(),dormitoryVO.getInserted_Date(),
							  dormitoryVO.getUpdated_By(),dormitoryVO.getUpdated_Date(),dormitoryVO.getAllot_beds(),dormitoryVO.getRemain_beds()});
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		}
		if(ttype.equals("edit"))
		{
			System.out.println("Edit_____________________________________________>"+dormitoryVO.getDormitory_name());
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				System.out.println("Enter 2");
				
				
				
				
				String sql =DormitoryConstant.EDIT_Dormitory ;
				System.out.println("Enter 2");
						  jdbcTemplate.update(sql,new Object[] {
								  dormitoryVO.getLocation(), dormitoryVO.getFee(),dormitoryVO.getCgender(),
								  dormitoryVO.getDescription(),dormitoryVO.getNo_of_beds(),dormitoryVO.getWarden_Photo(),dormitoryVO.getName(),/*dormitoryVO.getAge(),*/
								  dormitoryVO.getContact_Number(),dormitoryVO.getAlt_Contact_Number(),dormitoryVO.getAddress(),dormitoryVO.getDeputyWarden_Photo(),dormitoryVO.getName1(),
								  dormitoryVO.getAge1(),dormitoryVO.getContact_Number1(),dormitoryVO.getAlt_Contact_Number1(),dormitoryVO.getAddress1(),dormitoryVO.getDormitory_name()
								  });
				
				System.out.println("Success EDIT................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}
		}
		else if(ttype.equals("delete"))
		{
			System.out.println("********************************* delete  ***************************");
			try {
		
	JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =DormitoryConstant.DELETE_Dormitory;
						  jdbcTemplate.update(sql,new Object[] {
								  dormitoryVO.getDormitory_name()
						 
						  });
						  
				
				
				System.out.println("Success Delete................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}
		}
		

		return Dormitoryvo;
	}
	//updateRoomAllot
	public DormitoryVO updateRoomAllot(DormitoryVO dormitoryVO) throws Exception 
	{
		
		DormitoryVO Dormitoryvo = new DormitoryVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		System.out.println("DAO IMPL dormitory");
		System.out.println(dormitoryVO.getDormitory_name());
		System.out.println(dormitoryVO.getLocation());
		System.out.println(dormitoryVO.getFee());
		System.out.println(dormitoryVO.getCgender());
		System.out.println("DAO IMPL dormitory end");
		
		
		
		// TODO Auto-generated method stub
		
		
		System.out.println("Enter 1");
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			System.out.println("Enter 2");
			
			
			String sql =DormitoryConstant.Update_Dormitory ;
			System.out.println("Enter 2");
					  jdbcTemplate.update(sql,new Object[] {
							  dormitoryVO.getAllot_beds(), dormitoryVO.getAllot_beds(),dormitoryVO.getDormitory_name()});
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Dormitoryvo;
	}
	
	
	
	//insertRoomAllot
	public DormitoryAllotVO insertRoomAllot(DormitoryAllotVO dormitoryVO) throws Exception 
	{
		
		DormitoryAllotVO Dormitoryvo = new DormitoryAllotVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		System.out.println("DAO IMPL dormitory");
		System.out.println(dormitoryVO.getDormitory_name());
		System.out.println(dormitoryVO.getLocation());
	
		System.out.println("DAO IMPL dormitory end");
		
		
		
		// TODO Auto-generated method stub
		
		
		System.out.println("Enter 1");
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			System.out.println("Enter 2");
			
			
			String sql =DormitoryConstant.INSERT_Dormitory_Allot ;
			System.out.println("Enter 2");
					  jdbcTemplate.update(sql,new Object[] {
							  dormitoryVO.getDormitory_name(),dormitoryVO.getLocation(), dormitoryVO.getStudent_id(),dormitoryVO.getStudent_name(),
							  dormitoryVO.getGender(),dormitoryVO.getRoom_number()});
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Dormitoryvo;
	}
	
	
	
	//Show Records
		@SuppressWarnings("unchecked")	
		public List<DormitoryVO> getLtrReferenceData() throws Exception 
		{
			List<DormitoryVO> dormitoryServiceVOList = null;
			
			Session session = sessionFactory.openSession();
			try {
				dormitoryServiceVOList = session.createSQLQuery(DormitoryConstant.GET_DORMITORY_STUDENT_LIST)
						.setResultTransformer(Transformers.aliasToBean(DormitoryVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
			return dormitoryServiceVOList;
		}
		
		//getHostelList
		public List<DormitoryVO> getHostelList(String gender) throws Exception
		{
			System.out.println("Enter DAOImpl  Attendance "+gender);
			
			List<DormitoryVO> dormitoryServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				dormitoryServiceVOList = session.createSQLQuery(DormitoryConstant.GET_HOSTEL_GENDER)
						.setParameter("cgender",gender)
						.setResultTransformer(Transformers.aliasToBean(DormitoryVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
			return dormitoryServiceVOList;
		}
		//getNonDormitoryStudent
		public List<StuVO> getNonDormitoryStudent() throws Exception
		{
			
			List<StuVO> studentServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				studentServiceVOList = session.createSQLQuery(DormitoryConstant.GET_NON_HOSTEL_LIST)
						.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
			return studentServiceVOList;
		}
		
		
		//getDormitoryList
		public List<DormitoryVO> getDormitoryList(String dname) throws Exception
		{
			System.out.println("Enter DAOImpl  Attendance "+dname);
			
			List<DormitoryVO> dormitoryServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				dormitoryServiceVOList = session.createSQLQuery(DormitoryConstant.GET_DORMITORY_LIST)
						.setParameter("dormitory_name",dname)
						.setResultTransformer(Transformers.aliasToBean(DormitoryVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
			return dormitoryServiceVOList;
		}
		
		//getDormitoryAllotList
		public List<DormitoryAllotVO> getDormitoryAllotList(String dname) throws Exception
		{
			System.out.println("Enter DAOImpl  Attendance "+dname);
			
			List<DormitoryAllotVO> dormitoryAllotServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				dormitoryAllotServiceVOList = session.createSQLQuery(DormitoryConstant.GET_DORMITORY_ALLOT_LIST)
						.setParameter("dormitory_name",dname)
						.setResultTransformer(Transformers.aliasToBean(DormitoryAllotVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
			return dormitoryAllotServiceVOList;
		}
		
		//editHostel
		/*public List<StuVO> editHostel(String sid) throws Exception
		{
			System.out.println("Enter DAOImpl  Attendance "+sid);
			
			List<StuVO> studentServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				studentServiceVOList = session.createSQLQuery(DormitoryConstant.EDIT_HOSTEL)
						.setParameter("student_id",sid)
						.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
			return studentServiceVOList;
		}*/
		
		
		public StuVO editHostel(StuVO stuVO) throws Exception 
		{
			
			StuVO Stuvo = new StuVO();
			Session session = sessionFactory.openSession();
			Transaction trx = session.beginTransaction();
			
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				
				String sql =DormitoryConstant.UPDATE_HOSTEL ;
						  jdbcTemplate.update(sql,new Object[] {
								  stuVO.getStudent_id()});
				System.out.println("Success update................");
				trx.commit();
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}
			
			return Stuvo;
		}
		
		
}


	