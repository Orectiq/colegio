package com.dao.impl;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dao.ProfileDao;
import com.dao.constant.ProfileConstant;
import com.dao.constant.StudentConstant;
import com.model.LoginVO;

@Repository
public class ProfileDaoImpl implements ProfileDao
{
	@Autowired

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
	
	@Autowired
	 DataSource dataSource;
	
	
	//updateParentsPassword
		public LoginVO updateParentsPassword(LoginVO loginVO) throws Exception
		{
			System.out.println("new pass  "+loginVO.getNpass()+"  uname  "+loginVO.getUname()+" old pass  "+loginVO.getLpass());
			LoginVO Loginvo = new LoginVO();
			Session session = sessionFactory.openSession();
			Transaction trx = session.beginTransaction();
			try {
			
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				String sql =ProfileConstant.UPDATE_PARENTS_PASSWORD;
						  jdbcTemplate.update(sql,new Object[] {
								  loginVO.getNpass(),loginVO.getUname(),loginVO.getLpass()
						  });
				
						  System.out.println("Success Password Update finally................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error final------------>"+e);
				throw new Exception(e);
			}
			
			System.out.println("daoooooooooooooo   imple   "+Loginvo);
			return Loginvo;
		}
		
		
		
		
		
		//photoUpdate
		
		public LoginVO photoUpdate(LoginVO loginVO) throws Exception {
				
				System.out.println("DAO IMPL  "+loginVO.getPhoto()+" and  "+loginVO.getUname());
				LoginVO Loginvo = new LoginVO();
				Session session = sessionFactory.openSession();
				Transaction trx = session.beginTransaction();
				try {
				
					 
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					
					
					System.out.println("@@@@@@@@@@@@@@@@@@@@"+loginVO.getLtype());		
					
					
					String sql2 =StudentConstant.UPDATE_LOGIN_PHOTO ;
					  jdbcTemplate.update(sql2,new Object[] {
							  loginVO.getPhoto(),loginVO.getUname()
					  });
					  
					
				    if(loginVO.getLtype().equals("Teacher"))
				    {
				    	String sql1 =StudentConstant.UPDATE_TEACHER_PHOTO ;
						  jdbcTemplate.update(sql1,new Object[] {
								  loginVO.getPhoto(),loginVO.getUname()
						  });
				    }
				    else if(loginVO.getLtype().equals("hostel"))
				    {
				    	String sql1 =StudentConstant.UPDATE_HOSTEL_PHOTO ;
						  jdbcTemplate.update(sql1,new Object[] {
								  loginVO.getPhoto(),loginVO.getUname()
						  });
				    }
				    else if(loginVO.getLtype().equals("Parents"))
				    {
				    	String sql1 =StudentConstant.UPDATE_PARENTS_PHOTO ;
						  jdbcTemplate.update(sql1,new Object[] {
								  loginVO.getPhoto(),loginVO.getUname()
						  });
				    }	
				    else if(loginVO.getLtype().equalsIgnoreCase("admin"))
				    {
				    	String sql1 =StudentConstant.UPDATE_MASTER_PHOTO ;
						  jdbcTemplate.update(sql1,new Object[] {
								  loginVO.getPhoto(),loginVO.getUname()
						  });
				    }	
				    else if(loginVO.getLtype().equalsIgnoreCase("student"))
				    {
				    	String sql1 =StudentConstant.UPDATE_STUDENT_PHOTO ;
						  jdbcTemplate.update(sql1,new Object[] {
								  loginVO.getPhoto(),loginVO.getUname()
						  });
				    }	
				    else if(loginVO.getLtype().trim().equals("library"))
				    {
				    	System.out.println("Library photo change");
				    	String sql1 =StudentConstant.UPDATE_HOSTEL_PHOTO ;
						  jdbcTemplate.update(sql1,new Object[] {
								  loginVO.getPhoto(),loginVO.getUname()
						  });
				    }	
				    else if(loginVO.getLtype().trim().equals("Transport_AO"))
				    {
				    	System.out.println("Transport_AO photo change ");
				    	String sql1 =StudentConstant.UPDATE_HOSTEL_PHOTO ;
						  jdbcTemplate.update(sql1,new Object[] {
								  loginVO.getPhoto(),loginVO.getUname()
						  });
				    }	
				    else if(loginVO.getLtype().trim().equals("Transport"))
				    {
				    	System.out.println("Transport Driver photo change ");
				    	String sql1 =StudentConstant.UPDATE_DRIVER_PHOTO ;
						  jdbcTemplate.update(sql1,new Object[] {
								  loginVO.getPhoto(),loginVO.getUname()
						  });
				    }			  
					
							  System.out.println("Success Photo Update finally................");
					trx.commit();
							
				}catch (Exception e) {
					trx.rollback();
					System.out.println("Error final------------>"+e);
					throw new Exception(e);
				}
				
				return Loginvo;
			}
		//updateMobilePassword
			public LoginVO updateMobilePassword(LoginVO loginVO) throws Exception
			{
				System.out.println("new pass  "+loginVO.getNpass()+"  uname  "+loginVO.getUname()+" old pass  "+loginVO.getLpass());
				LoginVO Loginvo = new LoginVO();
				Session session = sessionFactory.openSession();
				Transaction trx = session.beginTransaction();
				try {
				
					 
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					String sql =StudentConstant.UPDATE_MOBILE_PASSWORD;
							  jdbcTemplate.update(sql,new Object[] {
									  loginVO.getNpass(),loginVO.getUname(),loginVO.getLpass()
							  });
					
							  System.out.println("Success Password Update finally................");
					trx.commit();
							
				}catch (Exception e) {
					trx.rollback();
					System.out.println("Error final------------>"+e);
					throw new Exception(e);
				}
				
				System.out.println("daoooooooooooooo   imple   "+Loginvo);
				return Loginvo;
			}	
		
		
		
		
		
		
		
}
