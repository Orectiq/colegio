package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.dao.AssignmentDao;
import com.dao.constant.AssignmentConstant;
import com.dao.constant.StudentConstant;
import com.model.AssignApprovalVO;
import com.model.AssignmentaddVO;





public class AssignmentDaoImpl implements AssignmentDao{

private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Autowired
	DataSource dataSource;

	
	public AssignmentaddVO insertAssignmentDetails(AssignmentaddVO assignmentaddVO) throws Exception 
	{
		AssignmentaddVO Assignmentaddvo = new AssignmentaddVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql_count2=StudentConstant.UPDATE_COUNTER_COUNT_ASSIGN ;
			jdbcTemplate.update(sql_count2);
			
			
			String sql =AssignmentConstant.INSERT_ASSIGNMENT ;
					  jdbcTemplate.update(sql,new Object[] {
							  assignmentaddVO.getSclass(),assignmentaddVO.getSubject(), assignmentaddVO.getSection(),assignmentaddVO.getTitle(),
							  assignmentaddVO.getDescription(),assignmentaddVO.getDue_Date(),assignmentaddVO.getMark(),assignmentaddVO.getHDate(),
							  assignmentaddVO.getIsActive(),assignmentaddVO.getInserted_By(),assignmentaddVO.getInserted_Date(),assignmentaddVO.getUpdated_By(),
							  assignmentaddVO.getUpdated_Date(),assignmentaddVO.getTid(),assignmentaddVO.getFname(),assignmentaddVO.getTtype(),assignmentaddVO.getAssignid() });
			System.out.println("Success insert................");
			
			
			
			
			
			
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		return Assignmentaddvo;
	}
	
	//insertAssignApproval
	public AssignApprovalVO insertAssignApproval(AssignApprovalVO assignmentaddVO) throws Exception 
	{
		AssignApprovalVO Assignmentaddvo = new AssignApprovalVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =AssignmentConstant.INSERT_ASSIGNMENT_APPROVAL ;
					  jdbcTemplate.update(sql,new Object[] {
							  assignmentaddVO.getAssign_date(),assignmentaddVO.getTeacher_id(),assignmentaddVO.getTname(),assignmentaddVO.getTtype(), assignmentaddVO.getStudent_id(),assignmentaddVO.getStudent_name(),
							  assignmentaddVO.getSclass(),assignmentaddVO.getSection(),assignmentaddVO.getSubject(),assignmentaddVO.getTitle(),assignmentaddVO.getDescription(),
							  assignmentaddVO.getStatus(),assignmentaddVO.getMark() });
			System.out.println("Success insert................");
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		return Assignmentaddvo;
	}
	
	//updateAssignApproval
	public AssignApprovalVO updateAssignApproval(AssignApprovalVO assignmentaddVO) throws Exception 
	{
		System.out.println("status  "+assignmentaddVO.getStatus()+" mark  "+assignmentaddVO.getMark()+" id  "+assignmentaddVO.getTeacher_id()+" class "+assignmentaddVO.getClass()+" section "+assignmentaddVO.getSection()+" sub  "+assignmentaddVO.getSubject()+" stuid  "+assignmentaddVO.getStudent_id()+" type  "+assignmentaddVO.getTtype());
		AssignApprovalVO Assignmentaddvo = new AssignApprovalVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =AssignmentConstant.UPDATE_ASSIGNMENT_APPROVAL_STATUS ;
			//assign_date=? and teacher_id=? and sclass=? and section=? and subject=?";
					  jdbcTemplate.update(sql,new Object[] {
							  assignmentaddVO.getStatus(), assignmentaddVO.getMark(),assignmentaddVO.getTeacher_id(),
							  assignmentaddVO.getSclass(),assignmentaddVO.getSection(),assignmentaddVO.getSubject(),assignmentaddVO.getStudent_id(),assignmentaddVO.getTtype()
							 
							  });
			System.out.println("Success Update................");
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		return Assignmentaddvo;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AssignmentaddVO> getAssignment(String sclass, String section) throws Exception {
Object sec = null;
System.out.println("Enter DAOImpl  Assignment "+sclass+" and "+section);
		
		List<AssignmentaddVO> assignmentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_LIST)
					.setParameter("sclass",sclass)
					.setParameter("section",section)
					.setResultTransformer(Transformers.aliasToBean(AssignmentaddVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return assignmentServiceVOList;
	}
	
	
	
	//getSubject
	@SuppressWarnings("unchecked")
	public List<AssignApprovalVO> getSubject(String tid) throws Exception 
	{
			Object sec = null;
			System.out.println("Enter DAOImpl  Assignment "+tid);
		
		List<AssignApprovalVO> assignmentApprovalServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentApprovalServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_SUBJECT_LIST)
					.setParameter("teacher_id",tid)
					.setResultTransformer(Transformers.aliasToBean(AssignApprovalVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return assignmentApprovalServiceVOList;
	}
	//getAssignApprovalTeacher(String adate,String tid,String sclass,String sec,String subj,String sta)
	@SuppressWarnings("unchecked")
	public List<AssignApprovalVO> getAssignApprovalTeacher(String adate,String tid,String sclass,String sec,String subj,String sta) throws Exception 
	{
			Object sec1 = null;
			System.out.println("Enter DAOImpl  Assignment     "+sta);
			System.out.println("id  "+tid+" class  "+sclass+" sec  "+sec+"  status  "+sta);
		if(sta.equals("All"))
		{
			System.out.println("Enter         ALL     "+sta);
		List<AssignApprovalVO> assignmentApprovalServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentApprovalServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_APPROVAL_LIST1)
					/*.setParameter("assign_date",adate)*/
					.setParameter("teacher_id",tid)
					.setParameter("sclass",sclass)
					.setParameter("section",sec)
					.setParameter("subject",subj)
					.setResultTransformer(Transformers.aliasToBean(AssignApprovalVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return assignmentApprovalServiceVOList;
		
		}
		
		List<AssignApprovalVO> assignmentApprovalServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentApprovalServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_APPROVAL_LIST)
					/*.setParameter("assign_date",adate)*/
					.setParameter("teacher_id",tid)
					.setParameter("sclass",sclass)
					.setParameter("section",sec)
					.setParameter("subject",subj)
					.setParameter("status",sta)
					.setResultTransformer(Transformers.aliasToBean(AssignApprovalVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		
		return assignmentApprovalServiceVOList;
		
		
	}
	
	//getApprovalTType
	
	@SuppressWarnings("unchecked")
	public List<AssignApprovalVO> getApprovalTType(String ttype,String tid) throws Exception 
	{
			Object sec1 = null;
			
			System.out.println("enter daoimpl Assignment !!!!!! "+ttype+" and  "+tid);
		
		List<AssignApprovalVO> assignmentApprovalServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentApprovalServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_APPROVAL_TYPE_LIST)
					/*.setParameter("assign_date",adate)*/
					.setParameter("teacher_id",tid)
					.setParameter("ttype",ttype)
					.setResultTransformer(Transformers.aliasToBean(AssignApprovalVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		
		return assignmentApprovalServiceVOList;
	}
	//getApprovalList(String sclass,String section,String tid,String title,String ttype)
	@SuppressWarnings("unchecked")
	public List<AssignApprovalVO> getApprovalList(String sclass,String section,String tid,String title,String ttype) throws Exception 
	{
			Object sec1 = null;
			
			System.out.println("sclass  "+sclass+" section  "+section+"id  "+tid+"title  "+title+" type  "+ttype);
		
		List<AssignApprovalVO> assignmentApprovalServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentApprovalServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_REAPPROVAL_TYPE_LIST)
					.setParameter("sclass",sclass)
					.setParameter("section",section)
					.setParameter("teacher_id",tid)
					.setParameter("subject",title)
					.setParameter("ttype",ttype)
					.setResultTransformer(Transformers.aliasToBean(AssignApprovalVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		
		return assignmentApprovalServiceVOList;
	}
	
	//getAdminApprovalList
	@SuppressWarnings("unchecked")
	public List<AssignApprovalVO> getAdminApprovalList(String sclass,String section,String subject) throws Exception 
	{
			Object sec1 = null;
			
			System.out.println("sclass  "+sclass+" section  "+section+"subject"+subject);
		
		List<AssignApprovalVO> assignmentApprovalServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentApprovalServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_ADMINPAGE)
					.setParameter("sclass",sclass)
					.setParameter("section",section)
					.setParameter("subject",subject)
					.setResultTransformer(Transformers.aliasToBean(AssignApprovalVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		
		return assignmentApprovalServiceVOList;
	}
	
	
	//getAssignmentTeacher
	@SuppressWarnings("unchecked")
	public List<AssignmentaddVO> getAssignmentTeacher(String sclass, String section,String tid,String adate,String ttype) throws Exception {
Object sec = null;
System.out.println("Enter DAOImpl  Assignment "+sclass+" and "+section+" and "+tid);
		
		List<AssignmentaddVO> assignmentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_TEACHER_LIST)
					.setParameter("Sclass",sclass)
					.setParameter("Section",section)
					.setParameter("Tid",tid)
					/*.setParameter("HDate",adate)*/
					.setParameter("ttype",ttype)
					.setResultTransformer(Transformers.aliasToBean(AssignmentaddVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return assignmentServiceVOList;
	}
	
	//getAssignmentTeacherAllot
	@SuppressWarnings("unchecked")
	public List<AssignmentaddVO> getAssignmentTeacherAllot(String aid) throws Exception {
Object sec = null;
System.out.println("Enter DAOImpl  Assignment  id "+aid);
		
		List<AssignmentaddVO> assignmentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_TEACHER_LIST1)
					.setParameter("assignid",aid)
					.setResultTransformer(Transformers.aliasToBean(AssignmentaddVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return assignmentServiceVOList;
	}
	
	
	////getTitle  String hdate,String subj
	
	@SuppressWarnings("unchecked")
	public List<AssignmentaddVO> getTitle(String hdate,String subj) throws Exception {
Object sec = null;
System.out.println("Enter DAOImpl  Assignment ");
		
		List<AssignmentaddVO> assignmentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_TITLE)
					/*.setParameter("HDate",hdate)*/
					.setParameter("Subject",subj)
					.setResultTransformer(Transformers.aliasToBean(AssignmentaddVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return assignmentServiceVOList;
	}

	
	@SuppressWarnings("unchecked")
	public List<AssignmentaddVO> getTType(String ttype,String tid) throws Exception {
Object sec = null;
System.out.println("Enter DAOImpl  Assignment ");
		
		List<AssignmentaddVO> assignmentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			assignmentServiceVOList = session.createSQLQuery(AssignmentConstant.GET_ASSIGNMENT_TYPE)
					.setParameter("Tid",tid)
					.setParameter("ttype",ttype)
					.setResultTransformer(Transformers.aliasToBean(AssignmentaddVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return assignmentServiceVOList;
	}
	
	
	
}
