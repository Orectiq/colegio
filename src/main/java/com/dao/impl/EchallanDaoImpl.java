package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dao.EchallanDao;
import com.dao.constant.EchallanConstant;
import com.dao.constant.StudentConstant;
import com.dao.constant.TeacherConstant;
import com.model.EchallanStudentVO;
import com.model.EchallanVO;
import com.model.StuVO;

@Repository
public class EchallanDaoImpl implements EchallanDao 
{
	@Autowired
	private SessionFactory sessionFactory;
	

	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
	@Autowired
	DataSource dataSource;

	
	
	public List<Object> getChallanId(String ref) throws Exception {
		System.out.println("enter getstuid daiImpl  "+ref);
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(EchallanConstant.GET_CHALLANID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		System.out.println("Return value of  daiImpl  ");
		return obj;
	}

	//@Override
	public EchallanVO insertEchallanDetails(EchallanVO echallanVO) throws Exception 
	{
		System.out.println("challan daoimpl insert ");
		
		EchallanVO Echallanvo = new EchallanVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
	
		
		String tran_type=echallanVO.getTran_typ();
		System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type);
		if(tran_type.equals("insert"))
		{	
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql_count4=StudentConstant.UPDATE_COUNTER_COUNT8 ;
			jdbcTemplate.update(sql_count4);
			
			
			String sql = EchallanConstant.INSERT_echallan ;
			
					  jdbcTemplate.update(sql,new Object[] {
							  echallanVO.getChallan_id(),echallanVO.getTheDate(), echallanVO.getFee_name(),echallanVO.getAcc_year(),
							  echallanVO.getFee_Break1(),echallanVO.getAmount1(),echallanVO.getFee_Break2(),echallanVO.getAmount2(),
							  echallanVO.getFee_Break3(),echallanVO.getAmount3(),
							  echallanVO.getTotal(),echallanVO.getSClass(),echallanVO.getSection(),echallanVO.getDatepicker(),							  
							  echallanVO.getIsActive(),echallanVO.getInserted_By(),echallanVO.getInserted_Date(),
							  echallanVO.getUpdated_By(),echallanVO.getUpdated_Date(),echallanVO.getSendMessage()});
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			//trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		}
		else if(tran_type.equals("edit"))
		{
			System.out.println("********************************* Edit  ***************************"+echallanVO.getChallan_id());
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =EchallanConstant.UPDATE_ECHALLAN ;
						  jdbcTemplate.update(sql,new Object[] {
								  echallanVO.getTheDate(), echallanVO.getFee_name(),echallanVO.getAcc_year(),
								  echallanVO.getFee_Break1(),echallanVO.getAmount1(),echallanVO.getFee_Break2(),echallanVO.getAmount2(),
								  echallanVO.getFee_Break3(),echallanVO.getAmount3(),
								  echallanVO.getTotal(),echallanVO.getSClass(),echallanVO.getSection(),echallanVO.getDatepicker(),echallanVO.getChallan_id()
						  });
				
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			
		}
		 if(tran_type.equals("delete"))
		{
			System.out.println("********************************* delete challan  ***************************");
			try {
                JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =EchallanConstant.DELETE_CHALLAN ;
						  jdbcTemplate.update(sql,new Object[] {
								  echallanVO.getChallan_id()
						 
						  });
						  
				
				System.out.println("Success Delete................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
		}
		
		
		
		return Echallanvo;
		
	}
	
	public EchallanVO sendEchallanDetails(EchallanVO echallanVO) throws Exception 
	{
		System.out.println("challan daoimpl update ");
		
		EchallanVO Echallanvo = new EchallanVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
	
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql = EchallanConstant.UPDATE_echallan ;
			
					  jdbcTemplate.update(sql,new Object[] {
							  echallanVO.getChallan_id()});
			
			System.out.println("Success Update................");
			trx.commit();
					
		}catch (Exception e) {
			//trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Echallanvo;
		
	}

	public List<EchallanVO> getFeeDraft(String sclass) throws Exception {
		System.out.println("Enter DAOImpl  class "+sclass);
		List<EchallanVO> echallanServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			echallanServiceVOList = session.createSQLQuery(EchallanConstant.GET_FEE_DRAFT_ALL)
					.setParameter("sclass", sclass)
					//.setParameter("sec", sec)
					.setResultTransformer(Transformers.aliasToBean(EchallanVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return echallanServiceVOList;
	}
	
	public List<EchallanVO> getFee(String sclass) throws Exception {
		System.out.println("Enter DAOImpl  class ---> "+sclass);
		List<EchallanVO> echallanServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			echallanServiceVOList = session.createSQLQuery(EchallanConstant.GET_FEE_ALL)
					.setParameter("sclass", sclass)
					//.setParameter("sec", sec)
					.setResultTransformer(Transformers.aliasToBean(EchallanVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return echallanServiceVOList;
	}
	//getStudentFee
	
	public List<EchallanStudentVO> getStudentFee(String sclass) throws Exception {
		System.out.println("Enter DAOImpl  class "+sclass);
		List<EchallanStudentVO> echallanServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			echallanServiceVOList = session.createSQLQuery(EchallanConstant.GET_STUDENT_FEE_ALL)
					.setParameter("student_id", sclass)
					//.setParameter("sec", sec)
					.setResultTransformer(Transformers.aliasToBean(EchallanStudentVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return echallanServiceVOList;
	}
	
	//getChallanFeeID
	public List<EchallanVO> getChallanFeeID(String sclass) throws Exception {
		System.out.println("Enter DAOImpl  class "+sclass);
		List<EchallanVO> echallanServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			echallanServiceVOList = session.createSQLQuery(EchallanConstant.GET_CHALLAN_FEE_ID)
					.setParameter("challan_id", sclass)
					//.setParameter("sec", sec)
					.setResultTransformer(Transformers.aliasToBean(EchallanVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return echallanServiceVOList;
	}

	//getChallanDraftID
		public List<EchallanVO> getChallanDraftID(String sclass) throws Exception {
			System.out.println("Enter DAOImpl  class "+sclass);
			List<EchallanVO> echallanServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				echallanServiceVOList = session.createSQLQuery(EchallanConstant.GET_CHALLAN_DRAFT_ID)
						.setParameter("challan_id", sclass)
						//.setParameter("sec", sec)
						.setResultTransformer(Transformers.aliasToBean(EchallanVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
			return echallanServiceVOList;
		}
	
	
}


	