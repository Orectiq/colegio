package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.dao.MediaDao;

import com.dao.constant.MediaConstant;
import com.model.MediaVO;
import com.model.MediaaddVO;

public class MediaDaoImpl implements MediaDao
{
private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Autowired
	DataSource dataSource;
	
	//@Override
	public MediaVO insertAlbumDetails(MediaVO mediaVO) throws Exception {
		MediaVO Mediavo = new MediaVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			
			String sql =MediaConstant.INSERT_ALBUM ;
			
					  jdbcTemplate.update(sql,new Object[] {
							  mediaVO.getAlbum_Title(),mediaVO.getAlbum_Description(), mediaVO.getAlbum_Image(),mediaVO.getShare_Student(),
							  mediaVO.getShare_Teacher(),mediaVO.getShare_NonStaff(),
							  mediaVO.getIsActive(), mediaVO.getInserted_By(), mediaVO.getInserted_Date(), mediaVO.getUpdated_By(),
							  mediaVO.getUpdated_Date()});
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Mediavo;

		
	}
	//@Override
	public MediaaddVO insertMediaall(MediaaddVO mediaaddVO) throws Exception {
		MediaaddVO Mediaaddvo = new MediaaddVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			
			String sql =MediaConstant.INSERT_MEDIA ;
			
					  jdbcTemplate.update(sql,new Object[] {
							  mediaaddVO.getMedia_Title(),mediaaddVO.getMedia_Description(),mediaaddVO.getUrlType(),
							  mediaaddVO.getUrlPath(),
							  mediaaddVO.getIsActive(), mediaaddVO.getInserted_By(), mediaaddVO.getInserted_Date(), mediaaddVO.getUpdated_By(),
							  mediaaddVO.getUpdated_Date()});
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Mediaaddvo;

	}
	
	
	@SuppressWarnings("unchecked")	
	public List<MediaaddVO> getMediaData() throws Exception 
	{
		List<MediaaddVO> mediaServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			mediaServiceVOList = session.createSQLQuery(MediaConstant.GET_MEDIA_LIST)
					.setResultTransformer(Transformers.aliasToBean(MediaaddVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return mediaServiceVOList;
	} 

	@SuppressWarnings("unchecked")	
	public List<MediaVO> getAlbumData() throws Exception 
	{
		List<MediaVO> albumServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			albumServiceVOList = session.createSQLQuery(MediaConstant.GET_ALBUM_LIST)
					.setResultTransformer(Transformers.aliasToBean(MediaVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return albumServiceVOList;
	} 
	
	
	
	
}
