package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dao.AbsenceRequestDao;
import com.dao.constant.AbsenceRequestConstant;
import com.dao.constant.LibraryConstant;
import com.model.AbsenceRequestVO;
import com.model.AbsenceStuRequestVO;
import com.model.LibraryIssueVO;
import com.model.TeacherVO;



@Repository
public class AbsenceRequestDaoImpl implements AbsenceRequestDao 
{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
	
	
	
	@Autowired
	 DataSource dataSource;

	//@Override
	public AbsenceRequestVO insertAbsenceRequestDetails(AbsenceRequestVO absenceRequestVO) throws Exception 
	{
		
		AbsenceRequestVO AbsenceRequestvo = new AbsenceRequestVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		System.out.println("  ------------------------------->?  DAO IMPL absence");
		System.out.println(absenceRequestVO.getTeacher_Id());
		System.out.println(absenceRequestVO.getTeacher_Name());
		System.out.println(absenceRequestVO.getStart_Date());
		System.out.println(absenceRequestVO.getEnd_Date());
		System.out.println(absenceRequestVO.getAbsence_Name());
		System.out.println(absenceRequestVO.getComments());
		System.out.println("DAO IMPL absence END");
		
		// TODO Auto-generated method stub
		
		
		System.out.println("Enter 1");
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			System.out.println("Enter 2");
			
			
			String sql =AbsenceRequestConstant.INSERT_AbsenceRequest ;
			System.out.println("Enter 2");
					  jdbcTemplate.update(sql,new Object[] {
							 absenceRequestVO.getTheDate(),
							  absenceRequestVO.getTeacher_Id(),
							  absenceRequestVO.getTeacher_Name(),
							  absenceRequestVO.getStart_Date(),
							  absenceRequestVO.getEnd_Date(),
							  absenceRequestVO.getAbsence_Name(),
							  absenceRequestVO.getStatus(),
							  absenceRequestVO.getComments(),							 						  
							  absenceRequestVO.getIsActive(),
							  absenceRequestVO.getInserted_By(),
							  absenceRequestVO.getInserted_Date(),
							  absenceRequestVO.getUpdated_By(),
							  absenceRequestVO.getUpdated_Date(),
							  absenceRequestVO.getTime()
					  });
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return AbsenceRequestvo;
	}
	//insertStuAbsenceRequest
	public AbsenceStuRequestVO insertStuAbsenceRequest(AbsenceStuRequestVO absenceRequestVO) throws Exception 
	{
		
		AbsenceStuRequestVO AbsenceRequestvo = new AbsenceStuRequestVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		System.out.println("DAO IMPL dormitory");
		System.out.println(absenceRequestVO.getThedate());
		System.out.println(absenceRequestVO.getStudent_id());
		System.out.println(absenceRequestVO.getFname());
		System.out.println("DAO IMPL dormitory END");
		
		// TODO Auto-generated method stub
		
		
		//System.out.println("Enter 1");
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			//System.out.println("Enter 2");
			
			
			String sql =AbsenceRequestConstant.INSERT_AbsenceStuRequest ;
			//System.out.println("Enter 2");
					  jdbcTemplate.update(sql,new Object[] {
							 absenceRequestVO.getThedate(),
							  absenceRequestVO.getStudent_id(),
							  absenceRequestVO.getFname(),
							  absenceRequestVO.getSclass(),
							  absenceRequestVO.getSection(),
							  absenceRequestVO.getStart_date(),
							  absenceRequestVO.getEnd_date(),
							  absenceRequestVO.getAbsence_name(),							 						  
							  absenceRequestVO.getStatus(),
							  absenceRequestVO.getComments(),
							  absenceRequestVO.getLapply(),
							  absenceRequestVO.getApproved_by(),
							  absenceRequestVO.getTime()
							  });
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return AbsenceRequestvo;
	}

	//Show Records
			@SuppressWarnings("unchecked")	
			public List<AbsenceRequestVO> getLtrReferenceData() throws Exception 
			{
				List<AbsenceRequestVO> absenceRequestServiceVOList = null;
				
				Session session = sessionFactory.openSession();
				
				try {
					absenceRequestServiceVOList = session.createSQLQuery(AbsenceRequestConstant.GET_AbsenceRequest_LIST_ALL)
							.setResultTransformer(Transformers.aliasToBean(AbsenceRequestVO.class)).list();
				} catch (Exception e) {
					System.out.println("controller AbsenceRequest");
					
					System.out.println("controller AbsenceRequest end");
					System.out.println("Error in DAOIMPL "+e);
				}
				return absenceRequestServiceVOList;
			}
			
		//getTeacherLeaveRequestbyDate
			@SuppressWarnings("unchecked")	
			public List<AbsenceRequestVO> getTeacherLeaveRequestbyDate(String rdate) throws Exception 
			{
				List<AbsenceRequestVO> absenceRequestServiceVOList = null;
				
				Session session = sessionFactory.openSession();
				
				try {
					absenceRequestServiceVOList = session.createSQLQuery(AbsenceRequestConstant.GET_AbsenceRequest_TEACHER_LIST)
							.setParameter("rdate",rdate)
							.setResultTransformer(Transformers.aliasToBean(AbsenceRequestVO.class)).list();
				} catch (Exception e) {
					System.out.println("controller AbsenceRequest");
					
					System.out.println("controller AbsenceRequest end");
					System.out.println("Error in DAOIMPL "+e);
				}
				return absenceRequestServiceVOList;
			}
			
			//getTeacherLeaveRequestbyDateNotify
			@SuppressWarnings("unchecked")	
			public List<AbsenceRequestVO> getTeacherLeaveRequestbyDateNotify(String rdate,String tid) throws Exception 
			{
				List<AbsenceRequestVO> absenceRequestServiceVOList = null;
				
				Session session = sessionFactory.openSession();
				
				try {
					absenceRequestServiceVOList = session.createSQLQuery(AbsenceRequestConstant.GET_AbsenceRequest_TEACHER_NOTIFY_LIST)
							.setParameter("theDate",rdate)
							.setParameter("Teacher_id",tid)
							.setResultTransformer(Transformers.aliasToBean(AbsenceRequestVO.class)).list();
				} catch (Exception e) {
					System.out.println("controller AbsenceRequest");
					
					System.out.println("controller AbsenceRequest end");
					System.out.println("Error in DAOIMPL "+e);
				}
				return absenceRequestServiceVOList;
			}
			
			
			@SuppressWarnings("unchecked")	
			public List<AbsenceRequestVO> getTeacherLeaveRequestbyDateNotify1(String rdate,String tid,String tim) throws Exception 
			{
				List<AbsenceRequestVO> absenceRequestServiceVOList = null;
				
				Session session = sessionFactory.openSession();
				
				try {
					absenceRequestServiceVOList = session.createSQLQuery(AbsenceRequestConstant.GET_AbsenceRequest_TEACHER_NOTIFY_LIST1)
							.setParameter("theDate",rdate)
							.setParameter("Teacher_id",tid)
							.setParameter("time",tim)
							.setResultTransformer(Transformers.aliasToBean(AbsenceRequestVO.class)).list();
				} catch (Exception e) {
					System.out.println("controller AbsenceRequest");
					
					System.out.println("controller AbsenceRequest end");
					System.out.println("Error in DAOIMPL "+e);
				}
				return absenceRequestServiceVOList;
			}
			
			
			
			
			
			
			//getStudentLeaveRequestbyDate
			@SuppressWarnings("unchecked")	
			public List<AbsenceStuRequestVO> getStudentLeaveRequestbyDate(String rdate) throws Exception 
			{
				List<AbsenceStuRequestVO> absenceStuRequestServiceVOList = null;
				
				Session session = sessionFactory.openSession();
				
				try {
					absenceStuRequestServiceVOList = session.createSQLQuery(AbsenceRequestConstant.GET_AbsenceRequest_STUDENT_LIST)
							.setParameter("rdate",rdate)
							.setResultTransformer(Transformers.aliasToBean(AbsenceStuRequestVO.class)).list();
				} catch (Exception e) {
					System.out.println("controller AbsenceRequest");
					
					System.out.println("controller AbsenceRequest end");
					System.out.println("Error in DAOIMPL "+e);
				}
				return absenceStuRequestServiceVOList;
			}
			
			
			
			//getLeaveData
			@SuppressWarnings("unchecked")	
			public List<AbsenceRequestVO> getLeaveData(String tid) throws Exception 
			{
				System.out.println("enter dao IMPL  &&&&&&&&&&& "+tid);
				
				List<AbsenceRequestVO> absenceRequestServiceVOList = null;
				
				Session session = sessionFactory.openSession();
				
				try {
					absenceRequestServiceVOList = session.createSQLQuery(AbsenceRequestConstant.GET_TEACHER_GRANT_LIST)
							.setParameter("Teacher_Id",tid)
							.setResultTransformer(Transformers.aliasToBean(AbsenceRequestVO.class)).list();
				} catch (Exception e) {
					System.out.println("controller AbsenceRequest");
					
					System.out.println("controller AbsenceRequest end");
					System.out.println("Error in DAOIMPL "+e);
				}
				return absenceRequestServiceVOList;
			}
			//getLeaveGrantbyStudent
			@SuppressWarnings("unchecked")	
			public List<AbsenceStuRequestVO> getLeaveGrantbyStudent(String sid) throws Exception 
			{
				System.out.println("enter dao IMPL  &&&&&&&&&&& "+sid);
				
				List<AbsenceStuRequestVO> absenceStuRequestServiceVOList = null;
				
				Session session = sessionFactory.openSession();
				
				try {
					absenceStuRequestServiceVOList = session.createSQLQuery(AbsenceRequestConstant.GET_STUDENT_GRANT_LIST)
							.setParameter("student_id",sid)
							.setResultTransformer(Transformers.aliasToBean(AbsenceStuRequestVO.class)).list();
				} catch (Exception e) {
					System.out.println("controller AbsenceRequest");
					
					System.out.println("controller AbsenceRequest end");
					System.out.println("Error in DAOIMPL "+e);
				}
				return absenceStuRequestServiceVOList;
			}
			
			
			//updateLeaveReq
			
			public AbsenceRequestVO updateLeaveReq(AbsenceRequestVO absenceRequestVO) throws Exception 
			{
				
				System.out.println("Dao IMPL Library");
				//System.out.println(absenceRequestVO.getTheDate());
				System.out.println(absenceRequestVO.getTeacher_Id());
				System.out.println(absenceRequestVO.getStatus());
				System.out.println("Dao IMPL library end");
				
				AbsenceRequestVO Absencerequestvo = new AbsenceRequestVO();
				Session session = sessionFactory.openSession();
				Transaction trx = session.beginTransaction();
				try {
					 
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					
					String sql =AbsenceRequestConstant.UPDATE_TEACHER_LEAVE ;
							  jdbcTemplate.update(sql,new Object[] {
									  absenceRequestVO.getStatus(),absenceRequestVO.getTeacher_Id(),absenceRequestVO.getTheDate(),absenceRequestVO.getTime()
							  });
					
					System.out.println("Success update................");
					trx.commit();
							
				}catch (Exception e) {
					//trx.rollback();
					System.out.println("Error ------------>"+e);
					throw new Exception(e);
				}
				
				return Absencerequestvo;

				
			}
			//updateStudentLeaveReq
			public AbsenceStuRequestVO updateStudentLeaveReq(AbsenceStuRequestVO absenceRequestVO) throws Exception 
			{
				
				System.out.println("Dao IMPL Library--------------------------->");
				System.out.println(absenceRequestVO.getStatus());
				System.out.println(absenceRequestVO.getApproved_by());
				System.out.println(absenceRequestVO.getStudent_id());
				System.out.println(absenceRequestVO.getThedate());
				System.out.println(absenceRequestVO.getTime());
				
				
			
				System.out.println("Dao IMPL library end --------------------------->");
				
				AbsenceStuRequestVO Absencerequestvo = new AbsenceStuRequestVO();
				Session session = sessionFactory.openSession();
				Transaction trx = session.beginTransaction();
				try {
					 
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					
					String sql =AbsenceRequestConstant.UPDATE_STUDENT_LEAVE ;
							  jdbcTemplate.update(sql,new Object[] {
									  absenceRequestVO.getStatus(),absenceRequestVO.getApproved_by(),absenceRequestVO.getStudent_id(),
									  absenceRequestVO.getThedate(),absenceRequestVO.getTime()
							  });
					
					System.out.println("Success update................");
					trx.commit();
							
				}catch (Exception e) {
					//trx.rollback();
					System.out.println("Error ------------>"+e);
					throw new Exception(e);
				}
				
				return Absencerequestvo;

				
			}
			
			
			//updateTeacherMaster
			public TeacherVO updateTeacherMaster(TeacherVO teacherVO) throws Exception 
			{
				TeacherVO Teachervo = new TeacherVO();
				Session session = sessionFactory.openSession();
				Transaction trx = session.beginTransaction();
				try {
					 
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					
					String sql =AbsenceRequestConstant.UPDATE_TEACHER_MASTER ;
							  jdbcTemplate.update(sql,new Object[] {
									  teacherVO.getLapply(),teacherVO.getLapply(),teacherVO.getTid()
							  });
					
					System.out.println("Success update................");
					trx.commit();
							
				}catch (Exception e) {
					//trx.rollback();
					System.out.println("Error ------------>"+e);
					throw new Exception(e);
				}
				
				return Teachervo;

				
			}
	
}


	