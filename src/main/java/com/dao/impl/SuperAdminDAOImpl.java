package com.dao.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.common.StringUtils;
import com.dao.SuperAdminDAO;
import com.dao.constant.StudentConstant;
import com.dao.constant.SuperAdminConstant;
import com.model.SchoolInfoVO;

@Repository
@Transactional
public class SuperAdminDAOImpl implements SuperAdminDAO {
	@Autowired
	SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Autowired
	DataSource dataSource;

	@Autowired
	ServletContext servletContext;

	public boolean registerSchool(final SchoolInfoVO schoolInfoVO) {
		schoolInfoVO.setNullsToEmpty();
		Session session = null;
		Transaction trx = null;
		String url = "";
		try {
			url = dataSource.getConnection().getMetaData().getURL();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			session = sessionFactory.openSession();
			trx = session.beginTransaction();
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

			jdbcTemplate.update(SuperAdminConstant.INSERT_SCHOOL_INFO,
					new Object[] { schoolInfoVO.getSchoolId(), schoolInfoVO.getSchoolName(),
							schoolInfoVO.getSchoolPhonePrimary(), schoolInfoVO.getSchoolAddress(),
							schoolInfoVO.getSchoolMembership(), schoolInfoVO.getSchoolPhoneSecondary(),
							schoolInfoVO.getSchoolEmail(), schoolInfoVO.getSchoolDB(), schoolInfoVO.getSchoolDBUser(),
							schoolInfoVO.getSchoolDBPass() });
			jdbcTemplate.execute("CREATE DATABASE " + schoolInfoVO.getSchoolDB());
			jdbcTemplate
					.execute("CREATE USER " + StringUtils.singleQuotationMarkAppender(schoolInfoVO.getSchoolDBUser())
							+ "@'localhost' IDENTIFIED BY "
							+ StringUtils.singleQuotationMarkAppender(schoolInfoVO.getSchoolDBPass()));
			jdbcTemplate.execute("GRANT ALL PRIVILEGES ON " + schoolInfoVO.getSchoolDB() + ".* TO "
					+ schoolInfoVO.getSchoolDBUser() + "@'localhost'");
			jdbcTemplate.execute("FLUSH PRIVILEGES");
			trx.commit();

		} catch (Exception e) {
			trx.rollback();
			System.out.println("Error in adding school ");
			e.printStackTrace();
			return false;
		} finally {
			if (session != null) {
				session.close();
			}
		}

		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource(
				url.substring(0, url.lastIndexOf("/") + 1) + schoolInfoVO.getSchoolDB(), schoolInfoVO.getSchoolDBUser(),
				schoolInfoVO.getSchoolDBPass());
		ScriptRunner scriptRunner = null;
		try {
			scriptRunner = new ScriptRunner(driverManagerDataSource.getConnection());
			scriptRunner.setAutoCommit(true);
			scriptRunner.setStopOnError(true);
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource("DBScripts//DBScript_School.sql").getFile());
			//new FileReader("G:\\Upwork\\colegio\\src\\main\\resources\\DBScripts\\DBScript_School.sql")
			scriptRunner.runScript(new BufferedReader(new FileReader(file)));
			String sql = StudentConstant.INSERT_LOGIN_MASTER;
			JdbcTemplate jdbcTemplate = new JdbcTemplate(driverManagerDataSource);
			jdbcTemplate.update(sql,
					new Object[] { "admin", "admin", "admin", schoolInfoVO.getSchoolId(), "Photo", "Administrator",
							schoolInfoVO.getSchoolName(), "year", "Logo", schoolInfoVO.getSchoolName(),
							schoolInfoVO.getSchoolName(), "yes", schoolInfoVO.getSchoolEmail() });
		} catch (Exception e) {
			System.out.println("Error in Creating school DB");
			e.printStackTrace();
			return false;
		} finally {
			scriptRunner.closeConnection();
		}
		return true;
	}
}
