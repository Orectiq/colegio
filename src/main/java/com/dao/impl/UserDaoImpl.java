package com.dao.impl;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.common.MasVO;
import com.common.TeachVO;
import com.common.UserVO;
import com.dao.UserDao;
import com.dao.constant.CommonConstant;
import com.dao.constant.LoginConstant;
import com.dao.constant.NonteachConstant;
import com.dao.constant.StudentConstant;
import com.dao.constant.TransportConstant;
import com.model.CounterVO;
import com.model.CountryVO;
import com.model.LibrarianVO;
import com.model.LoginVO;
import com.model.MailSettingVO;
import com.model.MasterVO;
import com.model.ParentVO;
import com.model.SettingadminVO;
import com.model.StuVO;
import com.model.TeacherVO;
import com.model.TransportVO;


@Repository
@Transactional
public class UserDaoImpl implements UserDao
{
	String urlnew=CommonConstant.PATH_URL;  
	
	@Autowired
	SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Autowired
	 DataSource dataSource;
	
	@Autowired
	ServletContext servletContext;
	
	public MasVO isUserNameExist(String userId, String pwd) throws Exception {
		System.out.println("DAOIMPL Uname  "+userId+" and  password  "+pwd);
		MasVO masVO = null;
		
	
			try {
				masVO = (MasVO) sessionFactory.getCurrentSession()
					.createQuery(
							" select A from " + MasVO.TYPE + " A where A.uname =:userId AND A.pass =:pwd  ")
					.setParameter("userId", userId).setParameter("pwd", pwd).uniqueResult();
				} catch (Exception e) 
				{	
						throw new Exception(e);
				}
		
		return masVO;
	}
	//isCommonUserExist
	public LoginVO isCommonUserExist(String userId, String pwd) throws Exception {
		System.out.println("DAOIMPL Uname  "+userId+" and  password  "+pwd);
		LoginVO loginVO = null;
			List<LoginVO> productslist = null;
			Session session = sessionFactory.openSession();
			try {

				loginVO = (LoginVO) session.createSQLQuery(LoginConstant.CHECK_COMMON_LOGIN)
						.setParameter("userid", userId)
						.setParameter("pwd", pwd)
						.setResultTransformer(Transformers.aliasToBean(LoginVO.class)).uniqueResult();
				
				loginVO.setLoginServiceVOList(productslist);
				
			} catch (Exception e) {
				throw new Exception(e);
			} finally {
				if (session != null) {
					session.close();
				}
			}	
		
		return loginVO;
	}
	
	
	//isTransportExist
	public TransportVO isTransportExist(String userId, String pwd) throws Exception {
		System.out.println("Enter DAOImpl  "+userId+" and password  "+pwd);
		TransportVO transVO=null;
		
		List<TransportVO> productslist = null;
		Session session = sessionFactory.openSession();
		try {

			transVO = (TransportVO) session.createSQLQuery(TransportConstant.GET_TRANSPORT_CHECK)
					.setParameter("userid", userId)
					.setParameter("pwd", pwd)
					.setResultTransformer(Transformers.aliasToBean(TransportVO.class)).uniqueResult();
			
			transVO.setTransportServiceVOList(productslist);
			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return transVO;
	}

	public TeacherVO isStaffNameExist(String userId, String pwd) throws Exception {
		System.out.println("Enter DAOImpl  "+userId+" and password  "+pwd);
		TeacherVO stuVO=null;
		
		List<TeacherVO> productslist = null;
		Session session = sessionFactory.openSession();
		try {

			stuVO = (TeacherVO) session.createSQLQuery(StudentConstant.GET_TEACHER_LIST_ALL)
					.setParameter("userid", userId)
					.setParameter("pwd", pwd)
					.setResultTransformer(Transformers.aliasToBean(TeacherVO.class)).uniqueResult();
			
			stuVO.setTeacherServiceVOList(productslist);
			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return stuVO;
	}




	public StuVO isSTUidExist(String userId, String password) throws Exception {
		System.out.println("Enter DAOImpl");
		StuVO stuVO=null;
		
		List<StuVO> productslist = null;
		Session session = sessionFactory.openSession();
		try {

			stuVO = (StuVO) session.createSQLQuery(StudentConstant.GET_STUDENT_LIST_ALL)
					.setParameter("studid", userId)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).uniqueResult();
			
			stuVO.setStudentServiceVOList(productslist);
			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return stuVO;
	}
	
	//isLIBidExist
	public LibrarianVO isLIBidExist(String userId, String password) throws Exception {
		System.out.println("Enter DAOImpl  "+userId);
		LibrarianVO libVO=null;
		
		List<LibrarianVO> productslist = null;
		Session session = sessionFactory.openSession();
		try {

			libVO = (LibrarianVO) session.createSQLQuery(StudentConstant.GET_LIBRARIAN_LIST_ALL)
					.setParameter("libid", userId)
					.setResultTransformer(Transformers.aliasToBean(LibrarianVO.class)).uniqueResult();
			
			libVO.setLibrarianServiceVOList(productslist);
			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return libVO;
	}

	public ParentVO isPARidExist(String userId, String password) throws Exception ///////////////////
	{
		System.out.println("Enter DAOImpl   "+userId);
		ParentVO parVO=null;
		
		List<ParentVO> productslist = null;
		Session session = sessionFactory.openSession();
		try {

			parVO = (ParentVO) session.createSQLQuery(StudentConstant.GET_STUDENT_PARENT_LIST_ALL1)
					.setParameter("parent_id", userId)
					.setResultTransformer(Transformers.aliasToBean(ParentVO.class)).uniqueResult();
			
			parVO.setParentServiceVOList(productslist);
			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return parVO;
	}



	public MasterVO insertMasterDetails(MasterVO masterVO) throws Exception
	{
		String desfile="http://localhost:8080/colegioui/assets/images/activity/activity1.jpg";
		try{
		File file = new File(desfile);
		if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } 
        }
	
		/*File sourceFile = new File("F:\\ReadPhotos\\"+masterVO.getPhoto());
		
        //File destinationFile = new File("http:\\localhost:8080\\colegioui\\WebContent\\assets\\images\\admin\\" + sourceFile.getName());
        //urlnew+"/Dashboard/Student_DashBoard.jsp?&mobile="+mobile+"&ltype="+ltype;
		File destinationFile = new File(desfile + sourceFile.getName());

        FileInputStream fileInputStream = new FileInputStream(sourceFile);
        FileOutputStream fileOutputStream = new FileOutputStream(
                destinationFile);

        int bufferSize;
        byte[] bufffer = new byte[512];
        while ((bufferSize = fileInputStream.read(bufffer)) > 0) {
            fileOutputStream.write(bufffer, 0, bufferSize);
        }
        fileInputStream.close();
        fileOutputStream.close();
        */
		}catch(Exception rr)
		{
			System.out.println(rr);
		}
        
		
		
		/*String webappRoot = servletContext.getRealPath("/");
	    String relativeFolder = "resources" + File.separator+ "images" + File.separator;
	    String filename = webappRoot + relativeFolder;
	    System.out.println("1st   "+webappRoot);
	    System.out.println("2nd   "+relativeFolder);
	    System.out.println("3rd   "+filename);
		
		String uploadUrl = desfile;
		String targetFileName = "F:\\ReadPhotos\\"+masterVO.getPhoto();
		URL url;
		HttpURLConnection connection = null;
		try {
		//Create connection
			url = new URL(uploadUrl);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "image/png");
			connection.setRequestProperty("Target-File-Name", desfile);
			connection.setUseCaches (false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			
			
			
			
		//Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			Image ii = ImageIO.read(new File(targetFileName));
			System.out.println("Enter 1 -------------->  "+ii);
			
			BufferedImage bimg = new BufferedImage (1000,1000, BufferedImage.TYPE_INT_ARGB);
			bimg = (BufferedImage)ii;
			System.out.println("Enter 2 -------------->  "+bimg);
			ImageIO.write(bimg, "png", wr);
			System.out.println("Enter 3 -------------->  ");
			wr.flush ();
			wr.close ();

		//Get Response 
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while((line = rd.readLine()) != null) 
			{
				response.append(line);
				response.append('\r');
			}
			rd.close();
		}catch (Exception e)
		{
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}*/
        
		
		
		
		
		MasterVO Mastervo = new MasterVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.INSERT_MASTER ;
					  jdbcTemplate.update(sql,new Object[] {
							  masterVO.getRole(),masterVO.getDesg(),masterVO.getName(),masterVO.getPhoto(),
							  masterVO.getUname(),masterVO.getPass(),masterVO.getSchoolname(),masterVO.getBranch(),
							  masterVO.getLogo(),masterVO.getAyear(),masterVO.getEmail(),masterVO.getMobile(),
							  masterVO.getPhone(),masterVO.getAddress(),masterVO.getStartdate(),masterVO.getValiddate(),
							  masterVO.getKey(),masterVO.getPack(),masterVO.getAdmid(),masterVO.getSchoolname(),masterVO.getSchoolname()
					  });
					  
			String sql1 =StudentConstant.INSERT_LOGIN_MASTER ;
					  jdbcTemplate.update(sql1,new Object[] 
							  {
								  masterVO.getUname(),masterVO.getPass(),masterVO.getLtype(),masterVO.getAdmid(),masterVO.getPhoto(),masterVO.getName(),
								  masterVO.getSchoolname(),masterVO.getAyear(),masterVO.getLogo(),masterVO.getSchoolname(),masterVO.getSchoolname(),masterVO.getPass_change(),
								  masterVO.getEmail()
					  });		  
					  
			
					  System.out.println("Success insert finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		return Mastervo;
	}
	//updateAdminPhoto
	public MasterVO updateAdminPhoto(MasterVO masterVO) throws Exception {
		
		System.out.println("DAO IMPL  "+masterVO.getPhoto()+" and  "+masterVO.getUname());
		MasterVO Mastervo = new MasterVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		try{
			
			File file = new File("F:\\PHOTOS");
			if (!file.exists()) {
	            if (file.mkdir()) {
	                System.out.println("Directory is created!");
	            } 
	        }	
			
			
		File sourceFile = new File("F:\\ReadPhotos\\"+masterVO.getPhoto());
		System.out.println("$$$$$$$$$$$$$$$$$$$$   "+sourceFile+" file name  --->"+sourceFile.getName());
		File destinationFile = new File("F:\\PHOTOS\\" + sourceFile.getName());
		FileInputStream fileInputStream = new FileInputStream(sourceFile);
        FileOutputStream fileOutputStream = new FileOutputStream(
                destinationFile);

        int bufferSize;
        byte[] bufffer = new byte[512];
        while ((bufferSize = fileInputStream.read(bufffer)) > 0) {
            fileOutputStream.write(bufffer, 0, bufferSize);
        }
        fileInputStream.close();
        fileOutputStream.close();
		}catch(Exception rr)
		{
			System.out.println(rr);
		}
		
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.UPDATE_MASTER_PHOTO ;
					  jdbcTemplate.update(sql,new Object[] {
							  masterVO.getPhoto(),masterVO.getUname()
					  });
			
					  System.out.println("Success Photo Update finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		return Mastervo;
	}
	
	//photoUpdate
	
public LoginVO photoUpdate(LoginVO loginVO) throws Exception {
		
		System.out.println("DAO IMPL  "+loginVO.getPhoto()+" and  "+loginVO.getUname());
		LoginVO Loginvo = new LoginVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.UPDATE_LOGIN_PHOTO ;
					  jdbcTemplate.update(sql,new Object[] {
							  loginVO.getPhoto(),loginVO.getUname()
					  });
			
			System.out.println("@@@@@@@@@@@@@@@@@@@@          "+loginVO.getLtype());		  
		    if(loginVO.getLtype().equals("Teacher"))
		    {
		    	String sql1 =StudentConstant.UPDATE_TEACHER_PHOTO ;
				  jdbcTemplate.update(sql1,new Object[] {
						  loginVO.getPhoto(),loginVO.getUname()
				  });
		    }
		    else if(loginVO.getLtype().equals("hostel"))
		    {
		    	String sql1 =StudentConstant.UPDATE_HOSTEL_PHOTO ;
				  jdbcTemplate.update(sql1,new Object[] {
						  loginVO.getPhoto(),loginVO.getUname()
				  });
		    }
					  
					  
			
					  System.out.println("Success Photo Update finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		return Loginvo;
	}
//updateMobilePassword
	public LoginVO updateMobilePassword(LoginVO loginVO) throws Exception
	{
		System.out.println("new pass  "+loginVO.getNpass()+"  uname  "+loginVO.getUname()+" old pass  "+loginVO.getLpass());
		LoginVO Loginvo = new LoginVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.UPDATE_MOBILE_PASSWORD;
					  jdbcTemplate.update(sql,new Object[] {
							  loginVO.getNpass(),loginVO.getUname(),loginVO.getLpass()
					  });
			
					  
					  
					  
					  System.out.println("Success Password Update finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		System.out.println("daoooooooooooooo   imple   "+Loginvo);
		return Loginvo;
	}	
	
	//updateAdminGlobalSetting
	public LoginVO updateAdminGlobalSetting(LoginVO loginVO) throws Exception
	{
		System.out.println("DAO IMPL  General Setting  ");
		System.out.println("logo "+loginVO.getSlogo()+"  email  "+loginVO.getEmail()+" mobile  "+loginVO.getMobile()+" address "+loginVO.getAddress()+" uname "+loginVO.getUname());
		System.out.println("sname  "+loginVO.getSname());
		System.out.println("Approval "+loginVO.getEn());
		System.out.println("ID  "+loginVO.getCnt_Prifix()+" and  "+loginVO.getCnt_Num());
		
		LoginVO Loginvo = new LoginVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.UPDATE_ADMINGLOBALSETTING;
					  jdbcTemplate.update(sql,new Object[] {
							  loginVO.getSlogo(),loginVO.getSheader(),loginVO.getSfooter(),loginVO.getSname()
					  });
			
			 String sql1 =StudentConstant.UPDATE_ADMINGLOBALSETTING1;
					  jdbcTemplate.update(sql1,new Object[] {
							  loginVO.getSlogo(),loginVO.getEmail(),loginVO.getMobile(),loginVO.getAddress(),loginVO.getSheader(),loginVO.getSfooter(),loginVO.getUname(),loginVO.getSname()
					  });
			
			
					 
					  
			if(loginVO.getEn().equals("Yes"))
			{	
			  String sql2 =StudentConstant.UPDATE_ADMINGLOBALSETTING2;
					  jdbcTemplate.update(sql2,new Object[] {
							  loginVO.getLogo_disp(),loginVO.getLanguage(),/*loginVO.getLanguage_allowed(),*/loginVO.getSname()
					  });
			}
			else if(loginVO.getEn().equals("No"))
			{
				String sql2 =StudentConstant.INSERT_ADMINGLOBALSETTING2;
				  jdbcTemplate.update(sql2,new Object[] {
						  loginVO.getSname(),loginVO.getLogo_disp(),loginVO.getLanguage()/*,loginVO.getLanguage_allowed()*/
				  });
			}
					  
					  System.out.println("Success Admin Setting Update finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		System.out.println("daoooooooooooooo   imple   "+Loginvo);
		return Loginvo;
	}	
	
	
	//updateMailPassword
	
	public LoginVO updateMailPassword(LoginVO loginVO) throws Exception
	{
		System.out.println("uname  "+loginVO.getUname()+" new pass "+loginVO.getConfirm_pass()+" status  "+loginVO.getPass_change());
		
		LoginVO Loginvo = new LoginVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.UPDATE_PASSWORDSTATUS;
					  jdbcTemplate.update(sql,new Object[] {
							  loginVO.getConfirm_pass(),loginVO.getPass_change(),loginVO.getUname()
					  });
			
			
					  
					  System.out.println("Success Mail password changed Update finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		
		return Loginvo;
	}	
	
	
	
	//insertMailSetting
	public MailSettingVO insertMailSetting(MailSettingVO mailsettingVO) throws Exception
	{
		
		MailSettingVO MailSettingvo = new MailSettingVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		String tran_type=mailsettingVO.getTran_typ();
		System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type);
		
		
		
		if(tran_type.equals("insert"))
		{	
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.INSERT_MAILSETTING;
					  jdbcTemplate.update(sql,new Object[] {
							  mailsettingVO.getTem_var(),mailsettingVO.getTem_message(),mailsettingVO.getTemp_id(),mailsettingVO.getTemp_head(),mailsettingVO.getTemp_footer()
					  });
			
					  
					  System.out.println("Success Admin Setting Update finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		}
		else if(tran_type.equals("edit"))
		{
			System.out.println("********************************* Edit  ***************************"+mailsettingVO.getTem_var());
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				
				String sql =StudentConstant.UPDATE_MAILSETTING;
						  jdbcTemplate.update(sql,new Object[] {
								 mailsettingVO.getTem_message(),mailsettingVO.getTemp_id(),
								  mailsettingVO.getTemp_head(),mailsettingVO.getTemp_footer(),mailsettingVO.getTem_var()
								  
						  
						  });
				
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}
			
			
		}
		if(tran_type.equals("delete"))
		{
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =StudentConstant.DELETE_MAILSETTING;
						  jdbcTemplate.update(sql,new Object[] {
								  mailsettingVO.getTem_var()
						 
						  });
						  
				
				System.out.println("Success Delete................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
		}
		
	
		
		System.out.println("daoooooooooooooo   imple   "+MailSettingvo);
		return MailSettingvo;
	}	
	//getMailSetting
	public List<MailSettingVO> getMailSetting() throws Exception
	{
		List<MailSettingVO> mailSettingServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			mailSettingServiceVOList = session.createSQLQuery(StudentConstant.GET_MAILSETTING)
					.setResultTransformer(Transformers.aliasToBean(MailSettingVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return mailSettingServiceVOList;
	}
	public List<MailSettingVO> getMailSettingType(String temvar) throws Exception
	{
		List<MailSettingVO> mailSettingServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			mailSettingServiceVOList = session.createSQLQuery(StudentConstant.GET_MAILSETTING_TYPE)
					.setParameter("temvar",temvar)
					.setResultTransformer(Transformers.aliasToBean(MailSettingVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return mailSettingServiceVOList;
	}
	
	//insertTerms
	
	public MailSettingVO insertTerms(MailSettingVO mailsettingVO) throws Exception
	{
		
		MailSettingVO MailSettingvo = new MailSettingVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.INSERT_TERMSCONDITION;
					  jdbcTemplate.update(sql,new Object[] {
							  mailsettingVO.getTerms()
					  });
			
					  
					  System.out.println("Success Terms Condition finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		System.out.println("daoooooooooooooo   imple   "+MailSettingvo);
		return MailSettingvo;
	}	
	
	//getTerms
	public List<MailSettingVO> getTerms() throws Exception
	{
		List<MailSettingVO> mailSettingServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			mailSettingServiceVOList = session.createSQLQuery(StudentConstant.GET_TERMS)
					.setResultTransformer(Transformers.aliasToBean(MailSettingVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return mailSettingServiceVOList;
	}
	
	//loadCountry
	public List<CountryVO> loadCountry() throws Exception
	{
		List<CountryVO> countryServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			countryServiceVOList = session.createSQLQuery(StudentConstant.GET_COUNTRY)
					.setResultTransformer(Transformers.aliasToBean(CountryVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return countryServiceVOList;
	}
	
	
	//getIDSetting
	public List<CounterVO> getIDSetting(String prifix) throws Exception
	{
		List<CounterVO> counterServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			counterServiceVOList = session.createSQLQuery(StudentConstant.GET_IDSETTING)
					.setParameter("Cnt_Prifix", prifix)
					.setResultTransformer(Transformers.aliasToBean(CounterVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return counterServiceVOList;
	}
	//getIDSettingAll
	public List<CounterVO> getIDSettingAll() throws Exception
	{
		List<CounterVO> counterServiceVOList=null;
		Session session=sessionFactory.openSession();
		try{
			counterServiceVOList=session.createSQLQuery(StudentConstant.GET_IDALL)
					.setResultTransformer(Transformers.aliasToBean(CounterVO.class)).list();
		}catch(Exception e)
		{
			if(session!=null)
			{
				session.close();
			}
		}
		return counterServiceVOList;
	}
	//updateIDSetting
	public CounterVO updateIDSetting(CounterVO counterVO) throws Exception
	{
		
		CounterVO Countervo = new CounterVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql10 =StudentConstant.UPDATE_ID;
			  jdbcTemplate.update(sql10,new Object[] {
					  counterVO.getCnt_Prifix(),counterVO.getCnt_Mod_Id()
			  });
			
					  
					  System.out.println("Success ID Condition finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		System.out.println("daoooooooooooooo   imple   "+Countervo);
		return Countervo;
	}	
	
	
	
	
	
	
	
	//getLoginDetails
		public List<LoginVO> getLoginDetails(String uid,String pass) throws Exception
		{
			System.out.println("Enter DAOImpl  Attendance "+uid+" pass  :  "+pass);
			
			List<LoginVO> loginServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				loginServiceVOList = session.createSQLQuery(StudentConstant.GET_LOGIN)
						.setParameter("uname",uid)
						.setParameter("lpass", pass)
						.setResultTransformer(Transformers.aliasToBean(LoginVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return loginServiceVOList;
		}	
	//getLoginEmail
		public List<LoginVO> getLoginEmail(String uid) throws Exception
		{
			System.out.println("Enter DAOImpl  Attendance "+uid);
			
			List<LoginVO> loginServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				loginServiceVOList = session.createSQLQuery(StudentConstant.GET_LOGIN_EMAIL)
						.setParameter("email",uid)
						.setResultTransformer(Transformers.aliasToBean(LoginVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return loginServiceVOList;
		}	
	//getLoginMobile
		public List<LoginVO> getLoginMobile(String uid) throws Exception
		{
			System.out.println("Enter DAOImpl  Attendance "+uid);
			
			List<LoginVO> loginServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				loginServiceVOList = session.createSQLQuery(StudentConstant.GET_LOGIN_MOBILE)
						.setParameter("uname",uid)
						
						.setResultTransformer(Transformers.aliasToBean(LoginVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return loginServiceVOList;
		}	
//getAdminSettingDetails
		public List<SettingadminVO> getAdminSettingDetails(String uid) throws Exception
		{
			System.out.println("Enter DAOImpl  Attendance "+uid);
			
			List<SettingadminVO> settingAdminServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {
				settingAdminServiceVOList = session.createSQLQuery(StudentConstant.GET_SETTING_ADMIN)
						.setParameter("sname",uid)
						.setResultTransformer(Transformers.aliasToBean(SettingadminVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return settingAdminServiceVOList;
		}	
	
	//updatePassword
	public MasterVO updatePassword(MasterVO masterVO) throws Exception {
		MasterVO Mastervo = new MasterVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.UPDATE_ADMIN_PASSWORD ;
					  jdbcTemplate.update(sql,new Object[] {
							  masterVO.getNpass(),masterVO.getUname(),masterVO.getPass()
					  });
			
					  System.out.println("Success Password Update finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		System.out.println("daoooooooooooooo   imple   "+Mastervo);
		return Mastervo;
	}
	
	
	
	public List<MasterVO> getMasterDetail(String uname)throws Exception
	{
		System.out.println("Dao   "+uname);
		
		List<MasterVO> masterServiceVOList=null;
		Session session=sessionFactory.openSession();
		try
		{
			masterServiceVOList=session.createSQLQuery(StudentConstant.GET_MASTER_DETAILS)
			.setParameter("uname",uname)
			.setResultTransformer(Transformers.aliasToBean(MasterVO.class)).list();
		}catch(Exception rr)
		{
			System.out.println(rr);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return masterServiceVOList;
	}
	
	
	
}
