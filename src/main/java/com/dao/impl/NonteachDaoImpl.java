package com.dao.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.sql.DataSource;

import org.apache.pdfbox.io.IOUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.controller.NonteachingController;
import com.dao.NonteachDao;
import com.dao.constant.NonteachConstant;
import com.dao.constant.StudentConstant;
import com.dao.constant.TeacherConstant;
import com.model.NonteachVO;
import com.model.staffAttendanceVO;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class NonteachDaoImpl implements NonteachDao{
	
	private final String SUFFIX = "/";
	
private SessionFactory sessionFactory;
@Autowired
public void setSessionFactory(SessionFactory sf){
	this.sessionFactory = sf;
}

@Autowired
DataSource dataSource;


@Autowired
private MailSender crunchifymail;
	//@Override



@SuppressWarnings("unchecked")
public List<Object> getLibID(String ref) throws Exception
{
	System.out.println("enter getstuid daiImpl  "+ref);
	List<Object> obj = null;
	Session session = sessionFactory.openSession();
	try {
		
			obj = session.createQuery(NonteachConstant.GET_LIBID).list();
	} catch (Exception e) {
		System.out.println("Could not get  Reference Number : \n" + e);
		throw new Exception(e);
	} finally {
		if (session != null) {
			session.close();
		}
		
	}
	return obj;
}


@SuppressWarnings("unchecked")
public List<Object> getDorID(String ref) throws Exception
{
	System.out.println("enter getstuid daiImpl  "+ref);
	List<Object> obj = null;
	Session session = sessionFactory.openSession();
	try {
		
			obj = session.createQuery(NonteachConstant.GET_DORID).list();
	} catch (Exception e) {
		System.out.println("Could not get  Reference Number : \n" + e);
		throw new Exception(e);
	} finally {
		if (session != null) {
			session.close();
		}
		
	}
	return obj;
}

@SuppressWarnings("unchecked")
public List<Object> getTraID(String ref) throws Exception
{
	System.out.println("enter getstuid daiImpl  "+ref);
	List<Object> obj = null;
	Session session = sessionFactory.openSession();
	try {
		
			obj = session.createQuery(NonteachConstant.GET_TRAID).list();
	} catch (Exception e) {
		System.out.println("Could not get  Reference Number : \n" + e);
		throw new Exception(e);
	} finally {
		if (session != null) {
			session.close();
		}
		
	}
	return obj;
}



public NonteachVO sendMail(NonteachVO nonteachVO) throws Exception 
{
	System.out.println("Mail DAOIMPL  -->  "+nonteachVO.getToAddr()+" from  "+nonteachVO.getFrmAddr()+" subj  "+nonteachVO.getSubj()+" body  "+nonteachVO.getBody());
	System.out.println("for SMS    "+nonteachVO.getPhone_Number());
	NonteachVO Nonteachvo = new NonteachVO();
	
	
			String postData="";
			String retval = "";
			//give all Parameters In String
			String Username ="kapsystem";
			String Password = "kap@user!123";
			String MobileNo = "919842854059";
			String Message = "Hi This is checking message";
			String SenderID = "KAPMSG";
			postData += "username=" + Username + "&password=" + Password + "&to=" +MobileNo +"&sender=" + SenderID + "&message=" + Message;
			URL url = new URL("http://instant.kapsystem.com/web2sms.php?");
			
			
	
	
	try{


	
	SimpleMailMessage crunchifyMsg = new SimpleMailMessage();
	crunchifyMsg.setFrom(nonteachVO.getFrmAddr());
	crunchifyMsg.setTo(nonteachVO.getToAddr());
	crunchifyMsg.setSubject(nonteachVO.getSubj());
	crunchifyMsg.setText(nonteachVO.getBody());
	crunchifymail.send(crunchifyMsg);
	
	
	}catch(Exception rr)
	{
		System.out.println("Error  ------------->"+rr);
	}
	
	
	try{
		
		HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
		urlconnection.setRequestMethod("POST");
		urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		urlconnection.setDoOutput(true);
		OutputStreamWriter out = new
		OutputStreamWriter(urlconnection.getOutputStream());
		out.write(postData);
		out.close();
		
		BufferedReader in = new BufferedReader( new
		InputStreamReader(urlconnection.getInputStream()));
		String decodedString;
		while ((decodedString = in.readLine()) != null) {
		retval += decodedString;
		}
		in.close();
		System.out.println("SMS           "+retval);
		
	}catch(Exception rr)
	{
		System.out.println("Error SMS ------------->"+rr);
	}
	
	return Nonteachvo;
}


	public NonteachVO insertNonteachDetails(NonteachVO nonteachVO,String path1) throws Exception 
	{
		System.out.println("DAO IMPL    "+nonteachVO.getDepartment()+" pass status  "+nonteachVO.getPass_change()+" file name   "+nonteachVO.getPhoto()+" path   "+path1);
		NonteachVO Nonteachvo = new NonteachVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		String tran_type=nonteachVO.getTran_typ();
		System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type+"  path   "+path1);
		
		
		
		if(tran_type.equals("insert"))
		{	
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			

			
			if(nonteachVO.getDepartment().equalsIgnoreCase("library"))
			{
				String sql_count4=NonteachConstant.UPDATE_COUNTER_COUNT_LIBID;
				jdbcTemplate.update(sql_count4);
			}
			if(nonteachVO.getDepartment().equalsIgnoreCase("hostel"))
			{
				String sql_count4=NonteachConstant.UPDATE_COUNTER_COUNT_GET_DORID;
				jdbcTemplate.update(sql_count4);
			}
			if(nonteachVO.getDepartment().equalsIgnoreCase("Transport_AO"))
			{
				String sql_count4=NonteachConstant.UPDATE_COUNTER_COUNT_TRAID;
				jdbcTemplate.update(sql_count4);
			}
			
			
			
			String sql =NonteachConstant.INSERT_NONTEACH ;
					  jdbcTemplate.update(sql,new Object[] {
							  nonteachVO.getStaff_ID(),nonteachVO.getDepartment(), nonteachVO.getName(),nonteachVO.getAge(),nonteachVO.getDOB(),
							  nonteachVO.getGender(),nonteachVO.getPhone_Number(),nonteachVO.getContact_Number(),nonteachVO.getEmail(),
							  nonteachVO.getDOJ(),nonteachVO.getWork_Area(),nonteachVO.getCurrent_Address(),nonteachVO.getPermanent_Address(),
							  nonteachVO.getIsActive(),
							  nonteachVO.getInserted_By(),nonteachVO.getInserted_Date(),nonteachVO.getUpdated_By(),nonteachVO.getUpdated_Date(),
							  nonteachVO.getPhoto()});
			
			String sql1 =StudentConstant.INSERT_LOGIN_MASTER ;
					  jdbcTemplate.update(sql1,new Object[] {
							  nonteachVO.getPhone_Number(),nonteachVO.getLpass(),nonteachVO.getLtype(),nonteachVO.getStaff_ID(),nonteachVO.getPhoto(),nonteachVO.getName(),
							  nonteachVO.getSname(),nonteachVO.getAyear(),nonteachVO.getSlogo(),nonteachVO.getSheader(),nonteachVO.getSfooter(),nonteachVO.getPass_change(),
							  nonteachVO.getEmail()
					  });	
			String name=nonteachVO.getPhoto();
			String file=nonteachVO.getPhoto();
					  
			//connectAmazon(path1,name,file);	  
					  
					  
					  
			System.out.println("Success insert................");
			trx.commit();
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}
		}	
		else if(tran_type.equals("edit"))
		{
			System.out.println("********************************* Edit  ***************************");
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				
				String sql =NonteachConstant.UPDATE_NONTEACHER ;
						  jdbcTemplate.update(sql,new Object[] {
								  nonteachVO.getDepartment(), nonteachVO.getName(),nonteachVO.getAge(),nonteachVO.getDOB(),
								  nonteachVO.getGender(),nonteachVO.getPhone_Number(),nonteachVO.getContact_Number(),nonteachVO.getEmail(),
								  nonteachVO.getDOJ(),nonteachVO.getWork_Area(),nonteachVO.getCurrent_Address(),nonteachVO.getPermanent_Address(),
								  nonteachVO.getStaff_ID()
						  
						  });
				
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}
			
			
		}
		if(tran_type.equals("delete"))
		{
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =NonteachConstant.DELETE_TEACHER ;
						  jdbcTemplate.update(sql,new Object[] {
								  nonteachVO.getStaff_ID()
						 
						  });
						  
				String sql1 =NonteachConstant.DELETE_LOGIN ;
						  jdbcTemplate.update(sql1,new Object[] {
								  nonteachVO.getStaff_ID()
						 
						  });
				
				System.out.println("Success Delete................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
		}
		
		
		return Nonteachvo;
	}

	@SuppressWarnings("unchecked")
	public List<NonteachVO> getLtrReferenceData() throws Exception {
List<NonteachVO> nonteachServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			nonteachServiceVOList = session.createSQLQuery(NonteachConstant.GET_NONTEACH_LIST)
					.setResultTransformer(Transformers.aliasToBean(NonteachVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return nonteachServiceVOList;

	}

	@SuppressWarnings("unchecked")
	public List<NonteachVO> getLtrReferenceDataall() throws Exception {
List<NonteachVO> nonteachServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			nonteachServiceVOList = session.createSQLQuery(NonteachConstant.GET_NONTEACH_LIST)
					.setResultTransformer(Transformers.aliasToBean(NonteachVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return nonteachServiceVOList;
	}

	@SuppressWarnings("unchecked")
	//@Override
	public List<NonteachVO> getNonteach(String department) throws Exception {
		System.out.println("getNonteach controller" +department);
		
		List<NonteachVO> nonteachServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			nonteachServiceVOList = session.createSQLQuery(NonteachConstant.GET_NONTEACH_LIST_ALL)
					.setParameter("department",department)
					
					.setResultTransformer(Transformers.aliasToBean(NonteachVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return nonteachServiceVOList;

	}
	//getNonTeachingLoginID
	@SuppressWarnings("unchecked")
	//@Override
	public List<NonteachVO> getNonTeachingLoginID(String mobile) throws Exception {
		System.out.println("getNonteach controller" +mobile);
		
		List<NonteachVO> nonteachServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			nonteachServiceVOList = session.createSQLQuery(NonteachConstant.GET_NONTEACH_LOGINID)
					.setParameter("Staff_ID",mobile)
					
					.setResultTransformer(Transformers.aliasToBean(NonteachVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return nonteachServiceVOList;

	}
	
	//getNonTeachIDWithPhone
	
	@SuppressWarnings("unchecked")
	//@Override
	public List<NonteachVO> getNonTeachIDWithPhone(String mobile) throws Exception {
		System.out.println("getNonteach controller" +mobile);
		
		List<NonteachVO> nonteachServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			nonteachServiceVOList = session.createSQLQuery(NonteachConstant.GET_NONTEACH_LOGINID_PHONE)
					.setParameter("Phone_Number",mobile)
					
					.setResultTransformer(Transformers.aliasToBean(NonteachVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return nonteachServiceVOList;

	}
	
	//getNonTeachAttendance
	
	@SuppressWarnings("unchecked")
	//@Override
	public List<staffAttendanceVO> getNonTeachAttendance(String fdate,String tdate,String tid) throws Exception {
		System.out.println("getNonteach controller" +fdate);
		
		List<staffAttendanceVO> staffAttendanceServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			staffAttendanceServiceVOList = session.createSQLQuery(NonteachConstant.GET_NONTEACH_ATTENDANCE)
					.setParameter("fdate",fdate)
					.setParameter("tdate",tdate)
					.setParameter("tid",tid)
					
					.setResultTransformer(Transformers.aliasToBean(staffAttendanceVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return staffAttendanceServiceVOList;

	}
	
	public void connectAmazon(String folderName,String name,String file1)
	{
		System.out.println("------------------->  enter 1");
		//fname="F:\\ReadPhotos\\stunew2.jpg";
		//String fname2=folderName+"\\"+fname;
		String folder="Staff";
		//System.out.println("folder name   "+folderName+" and Filename  "+fname);
		
		AWSCredentials credentials = new BasicAWSCredentials(
				"AKIAJI2JNLEDXQTXHJRQ", 
				"A8ewKyESXUcxSUUsRTLPCWZ5kuC9focHE8LW39/2");
		
		System.out.println("------------------->  enter 2");
		AmazonS3 s3client = new AmazonS3Client(credentials);
		
		try{
		String bucketName = "orectiqcolegio";
		s3client.createBucket(bucketName);
		System.out.println("------------------->  enter 3");
		File file = new File(file1);
	    FileInputStream input = new FileInputStream(file);
	    MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));
		
	    System.out.println("------------------->  enter 4");
			InputStream is=multipartFile.getInputStream();
			s3client.putObject(new PutObjectRequest(bucketName,name,is,new ObjectMetadata()).withCannedAcl(CannedAccessControlList.PublicRead));
			 System.out.println("------------------->  enter 5");
			S3Object s3object=s3client.getObject(new GetObjectRequest(bucketName,name));
			//redirectAttributes.addAttribute("picUrl",s3object.getObjectContent().getHttpRequest().getURI().toString());
			System.out.println("--------------------> final output    "+s3object.getObjectContent().getHttpRequest().getURI().toString());
		}catch(Exception rr)
		{
			System.out.println("Error ------------------------------------->"+rr);
		}
		
		
		
		// list buckets          old code
		/*for (Bucket bucket : s3client.listBuckets()) {
			System.out.println(" <----------------> " + bucket.getName());
		}
		
		// create folder into bucket
		//String folderName = "testfolder";
		createFolder(bucketName, folder, s3client);
		
		// upload file to folder and set it to public
		String fileName = folder + SUFFIX + fname;
		s3client.putObject(new PutObjectRequest(bucketName, fileName, 
				new File(fname2))
				.withCannedAcl(CannedAccessControlList.PublicRead));*/
		
		//deleteFolder(bucketName, folderName, s3client);
		// deletes bucket
		//s3client.deleteBucket(bucketName);
	}
	
	
	
public void createFolder(String bucketName, String folderName, AmazonS3 client) {
	// create meta-data for your folder and set content-length to 0
	ObjectMetadata metadata = new ObjectMetadata();
	metadata.setContentLength(0);
	// create empty content
	InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
	// create a PutObjectRequest passing the folder name suffixed by /
	PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
			folderName + SUFFIX, emptyContent, metadata);
	// send request to S3 to create folder
	client.putObject(putObjectRequest);
}
	
	
	
}




	