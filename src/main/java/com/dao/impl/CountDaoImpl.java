package com.dao.impl;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dao.CountDao;

@Repository
public class CountDaoImpl implements CountDao 
{
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

	@Autowired
	 DataSource dataSource;
	
	//countTeacherLeaveReq	
	public int countTeacherLeaveReq() throws Exception 
	{
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String sql="select count(*) from absence_request where Status='Apply'";
		return jdbcTemplate.queryForInt(sql); 
	}	
}
