package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session; 
import org.hibernate.SessionFactory; 
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.jdbc.core.JdbcTemplate; 
import org.springframework.stereotype.Repository;

import com.dao.TransportDao;
import com.dao.constant.StudentConstant;
import com.dao.constant.TransportConstant;
import com.model.DormitoryVO;
import com.model.TransportAttendanceVO;
import com.model.TransportVO;

@Repository 
public class TransportDaoImpl implements TransportDao 
{
	
	@Autowired 
	private SessionFactory sessionFactory; 

	public void setSessionFactory(SessionFactory sf){ 
	this.sessionFactory = sf; 
	} 

	private Session getCurrentSession() { 
	return sessionFactory.getCurrentSession(); 
	} 
	@Autowired 
	DataSource dataSource; 
	
	

	public TransportVO insertTransportDetails(TransportVO transportVO) throws Exception
	{
		System.out.println("Enter Transport  DAOImpl");
		System.out.println("transport_name   "+transportVO.getTransport_name());
		TransportVO Transportvo = new TransportVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		String ttype=transportVO.getTran_type();
		if(ttype.equals("insert"))
		{
		try {
			
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql =TransportConstant.INSERT_TRANSPORT ;
					jdbcTemplate.update(sql,new Object[] {
							transportVO.getTransport_name(),transportVO.getTransport_type(), transportVO.getRegistration_number(),
							transportVO.getDescription(),transportVO.getNo_seats(),transportVO.getFee(),transportVO.getArea_cover(),
							transportVO.getBoard_points(),transportVO.getDri_photo(),transportVO.getDri_name(),transportVO.getDri_age(),
							transportVO.getDri_contact_number(),transportVO.getDri_alter_contact_number(),transportVO.getDlicense(),transportVO.getDri_address(),transportVO.getEmail(),
							transportVO.getCond_photo(),transportVO.getCond_name(),transportVO.getCond_age(),transportVO.getCond_contact_number(),
							transportVO.getCond_alter_contact_number(),transportVO.getCond_address(),
							transportVO.getRoute_num(),transportVO.getUname(),transportVO.getPass(),transportVO.getRole()	
					});
			
					String sql1 =StudentConstant.INSERT_LOGIN_MASTER ;
					  jdbcTemplate.update(sql1,new Object[] {
							  transportVO.getDri_contact_number(),transportVO.getLpass(),transportVO.getLtype(),transportVO.getTransport_name(),transportVO.getDri_photo(),transportVO.getDri_name(),
							  transportVO.getSname(),transportVO.getAyear(),transportVO.getSlogo(),transportVO.getSheader(),transportVO.getSfooter(),transportVO.getPass_change(),
							  transportVO.getEmail()
					  });
					
					
			System.out.println("Success insert................");
			trx.commit();
					
		}
		catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		}// if -- end
		
		
		if(ttype.equals("delete"))
		{
		try {
			
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql =TransportConstant.DELETE_TRANSPORT ;
					jdbcTemplate.update(sql,new Object[] {
							transportVO.getTransport_name()
					});
			
			String sql1 =TransportConstant.DELETE_LOGIN_TRANSPORT ;
					jdbcTemplate.update(sql1,new Object[] {
							transportVO.getTransport_name()
					});		
					
					
			System.out.println("Success insert................");
			trx.commit();
					
		}
		catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		}// if -- end
		
		
		
		return Transportvo;
	}
	//insertAttendance
	public TransportAttendanceVO insertAttendance(TransportAttendanceVO attendanceVO)throws Exception
	{
		TransportAttendanceVO Attendancevo=new TransportAttendanceVO();
		Session session=sessionFactory.openSession();
		Transaction trx=session.beginTransaction();
		try{
			JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
			String sql=TransportConstant.INSERT_TRANSPORT_ATTENDANCE;
				jdbcTemplate.update(sql,new Object[] {
						attendanceVO.getAdate(),attendanceVO.getDuser(),attendanceVO.getBoarding(),
						attendanceVO.getRoute_num(),attendanceVO.getStudent_id(),attendanceVO.getStudent_name(),
						attendanceVO.getSclass(),attendanceVO.getSection(),attendanceVO.getAttendance()
				});
				trx.commit();
				
		}catch(Exception e)
		{
			trx.rollback();
			System.out.println("Error  "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return Attendancevo;
	}
	
	

	@SuppressWarnings("unchecked")	
	public List<TransportVO> gettransportdetail() throws Exception 
	{
		List<TransportVO>transportServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			transportServiceVOList = session.createSQLQuery(TransportConstant.GET_TRANSPORT_LIST)
					.setResultTransformer(Transformers.aliasToBean(TransportVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return transportServiceVOList;
	}
	
	@SuppressWarnings("unchecked")	
	public List<TransportVO> gettransportdetailall(String transport_name) throws Exception 
	{
		List<TransportVO>transportServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			transportServiceVOList = session.createSQLQuery(TransportConstant.GET_TRANSPORT_LIST_ALL)
					.setParameter("transport_name",transport_name)
					.setResultTransformer(Transformers.aliasToBean(TransportVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return transportServiceVOList;
	}
	
	
	
	//getDname
	@SuppressWarnings("unchecked")	
	public List<TransportVO> getDname(String uname) throws Exception 
	{
		List<TransportVO> transportServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			transportServiceVOList = session.createSQLQuery(TransportConstant.GET_TRANSPORT_USER)
					.setParameter("uname",uname)
					.setResultTransformer(Transformers.aliasToBean(TransportVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return transportServiceVOList;
	}	
	//getTransportLoginID
	@SuppressWarnings("unchecked")	
	public List<TransportVO> getTransportLoginID(String mobile) throws Exception 
	{
		List<TransportVO> transportServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			transportServiceVOList = session.createSQLQuery(TransportConstant.GET_TRANSPORT_LOGIN_USER)
					.setParameter("mobile",mobile)
					.setResultTransformer(Transformers.aliasToBean(TransportVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return transportServiceVOList;
	}	
	
//getAttendanceHistory
	public List<TransportAttendanceVO> getAttendanceHistory(String uname,String adate)throws Exception
	{
		List<TransportAttendanceVO> transportAttendanceServiceVOList=null;
		Session session=sessionFactory.openSession();
		try{
			transportAttendanceServiceVOList=session.createSQLQuery(TransportConstant.GET_ATTENDANCE_HISTORY)
					.setParameter("duser",uname)
					.setParameter("adate",adate)
					.setResultTransformer(Transformers.aliasToBean(TransportAttendanceVO.class)).list();
		}catch(Exception e)
		{
			System.out.println(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return transportAttendanceServiceVOList;
	}
	//getAttendanceHistory
		public List<TransportAttendanceVO> gettrandetail(String duser)throws Exception
		{
			List<TransportAttendanceVO> transportAttendanceServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				transportAttendanceServiceVOList=session.createSQLQuery(TransportConstant.GET_TRAN_DETAIL)
						.setParameter("duser",duser)
						
						.setResultTransformer(Transformers.aliasToBean(TransportAttendanceVO.class)).list();
			}catch(Exception e)
			{
				System.out.println(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return transportAttendanceServiceVOList;
		}
//getAttendanceHistoryAbsent	
	public List<TransportAttendanceVO> getAttendanceHistoryAbsent(String uname,String adate)throws Exception
	{
		List<TransportAttendanceVO> transportAttendanceServiceVOList=null;
		Session session=sessionFactory.openSession();
		try{
			transportAttendanceServiceVOList=session.createSQLQuery(TransportConstant.GET_ATTENDANCE_HISTORY_ABSENT)
					.setParameter("duser",uname)
					.setParameter("adate",adate)
					.setResultTransformer(Transformers.aliasToBean(TransportAttendanceVO.class)).list();
		}catch(Exception e)
		{
			System.out.println(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return transportAttendanceServiceVOList;
	}
	
	
	//getTransStudentVOList
	public List<TransportAttendanceVO> getTransStudentVOList(String board,String adate)throws Exception
	{
		List<TransportAttendanceVO> transportAttendanceServiceVOList=null;
		Session session=sessionFactory.openSession();
		try{
			transportAttendanceServiceVOList=session.createSQLQuery(TransportConstant.GET_AO_STUDENTS_HISTORY_LIST)
					.setParameter("boarding",board)
					.setParameter("adate",adate)
					.setResultTransformer(Transformers.aliasToBean(TransportAttendanceVO.class)).list();
		}catch(Exception e)
		{
			System.out.println(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return transportAttendanceServiceVOList;
	}
	
	//getTransStuListBYBoard
	public List<TransportAttendanceVO> getTransStuListBYBoard(String board,String adate)throws Exception
	{
		List<TransportAttendanceVO> transportAttendanceServiceVOList=null;
		Session session=sessionFactory.openSession();
		try{
			transportAttendanceServiceVOList=session.createSQLQuery(TransportConstant.GET_AO_STUDENTS_HISTORY_LIST_ABSENT)
					.setParameter("boarding",board)
					.setParameter("adate",adate)
					.setResultTransformer(Transformers.aliasToBean(TransportAttendanceVO.class)).list();
		}catch(Exception e)
		{
			System.out.println(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return transportAttendanceServiceVOList;
	}
	
	
	//getTransStudentVOListNew
	public List<TransportAttendanceVO> getTransStudentVOListNew(String board)throws Exception
	{
		List<TransportAttendanceVO> transportAttendanceServiceVOList=null;
		Session session=sessionFactory.openSession();
		try{
			transportAttendanceServiceVOList=session.createSQLQuery(TransportConstant.GET_AO_STUDENTS_LIST)
					.setParameter("boarding",board)
					.setResultTransformer(Transformers.aliasToBean(TransportAttendanceVO.class)).list();
		}catch(Exception e)
		{
			System.out.println(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		return transportAttendanceServiceVOList;
	}


}
	

