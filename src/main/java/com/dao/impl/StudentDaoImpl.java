package com.dao.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.model.AssignmentVO;
import com.model.AttendanceDayVO;
import com.model.AttendanceVO;
import com.model.CounterVO;
import com.model.DummyVO;
import com.model.LoginVO;
import com.model.MarksVO;
import com.model.MedicalVO;
import com.model.ParentVO;
import com.model.StuMedicalVO;
import com.model.StuParVO;
import com.model.StuVO;
import com.model.TimeTableVO;
import com.common.ExamTypeMasterVO;
import com.common.MasVO;
import com.common.StudentVO;
import com.dao.StudentDao;
import com.dao.constant.StudentConstant;

@Repository
public class StudentDaoImpl implements StudentDao
{

	@Autowired
	/*private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}*/
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
	
	
	
	@Autowired
	 DataSource dataSource;
	
	
	//getNewsid
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getNewsid() throws Exception 
	{
		System.out.println("enter getstuid daiImpl  -------------------->  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_NEWSID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}
	//getEventsId
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getEventsId() throws Exception 
	{
		System.out.println("enter getstuid daiImpl  -------------------->  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_EVENTSID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}
	//getAssignId
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getAssignId() throws Exception 
	{
		System.out.println("enter getASSIGNID daiImpl  -------------------->  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_ASSIGNID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}
	
	
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getStuId() throws Exception 
	{
		System.out.println("enter getstuid daiImpl  -------------------->  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_STUID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}

	//getReqId
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getReqId() throws Exception 
	{
		System.out.println("enter getstuid daiImpl  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_REQID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}

	
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getParId() throws Exception 
	{
		System.out.println("enter getstuid daiImpl  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_PARID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getAdmnId() throws Exception 
	{
		System.out.println("enter getstuid daiImpl  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_ADMNID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getAdmnId1() throws Exception 
	{
		System.out.println("enter getstuid daiImpl  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_ADMNID1).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getStuRoll() throws Exception 
	{
		System.out.println("enter getstuid daiImpl  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_STUROLL).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}
	
	//getChallanID
	@SuppressWarnings("unchecked")
	//@Override
	public List<Object> getChallanID() throws Exception 
	{
		System.out.println("enter getstuid daiImpl  ");
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(StudentConstant.GET_CHALLANID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}



	public StuVO insertStudentDetails(StuVO stuVO) throws Exception 
	{
		
		StuVO Stuvo = new StuVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		 BufferedImage image = null;
		
		System.out.println("******* DAOIMPL Start *******");
		System.out.println("fname  "+stuVO.getFname());
		System.out.println("lname  "+stuVO.getLname());
		System.out.println("photo  "+stuVO.getPhoto());
		System.out.println("bplace  "+stuVO.getBplace());
		System.out.println("nationality  "+stuVO.getNationality());
		System.out.println("mtongue  "+stuVO.getMtongue());
		System.out.println("cgender  "+stuVO.getCgender());
		System.out.println("religion  "+stuVO.getReligion());
		System.out.println("category  "+stuVO.getCategory());
		System.out.println("address1  "+stuVO.getAddress1());
		System.out.println("address2  "+stuVO.getAddress2());
		System.out.println("state  "+stuVO.getState());
		System.out.println("pin  "+stuVO.getPin());
		System.out.println("mobile  "+stuVO.getMobile());
		System.out.println("phone  "+stuVO.getPhone());
		System.out.println("cemail"+stuVO.getCemail());
		System.out.println("sch_transport  "+stuVO.getSch_transport());
		System.out.println("pre_school  "+stuVO.getPre_school1());
		System.out.println("prd_from  "+stuVO.getPrd_from1());
		System.out.println("prd_to  "+stuVO.getPrd_to1());
		
		System.out.println("******* DAOIMPL end *******");
		
		
		String tran_type=stuVO.getTran_typ();
		
		System.out.println("******* Type   *******"+tran_type);
		
		if(tran_type.equals("insert"))
		{
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql_count1=StudentConstant.UPDATE_COUNTER_COUNT1 ;
			jdbcTemplate.update(sql_count1);

			String sql_count3=StudentConstant.UPDATE_COUNTER_COUNT3 ;
			jdbcTemplate.update(sql_count3);

			String sql_count4=StudentConstant.UPDATE_COUNTER_COUNT4 ;
			jdbcTemplate.update(sql_count4);
			//46
			
			String sql =StudentConstant.INSERT_STUDENT ;
					  jdbcTemplate.update(sql,new Object[] {
							  stuVO.getStudent_id(),stuVO.getFname(), stuVO.getLname(),stuVO.getSclass() ,stuVO.getSection()
							  ,stuVO.getPhoto(),stuVO.getDob() ,stuVO.getBplace(),stuVO.getNationality(),stuVO.getMtongue()
							  ,stuVO.getCgender(),stuVO.getReligion(),stuVO.getCategory(),stuVO.getAddress1(),stuVO.getAddress2()
							  ,stuVO.getCity(),stuVO.getState(),stuVO.getCountry(),stuVO.getPin(),stuVO.getMobile(),stuVO.getPhone()
							  
							  ,stuVO.getCemail(),stuVO.getSch_transport(),stuVO.getTransport_board(), stuVO.getHostel()
							  
							  ,stuVO.getHostel_allot(),stuVO.getHostel_name(),stuVO.getHostel_Room_no()
							  
							  ,stuVO.getPre_school1(),stuVO.getPre_qual1(),stuVO.getPrd_from1(),stuVO.getPrd_to1()
							  ,stuVO.getPre_school2(),stuVO.getPre_qual2(),stuVO.getPrd_from2(),stuVO.getPrd_to2()
							  ,stuVO.getPre_school3(),stuVO.getPre_qual3(),stuVO.getPrd_from3(),stuVO.getPrd_to3()
							  
							  ,stuVO.getParent_id(),stuVO.getAdm_id() ,stuVO.getStudent_roll(),stuVO.getJoin_date(),stuVO.getSchool_Name()
							  ,stuVO.getAcademic_year(),stuVO.getRole()
					  
					  });
					  
					  String sql1 =StudentConstant.INSERT_LOGIN_MASTER ;
					  jdbcTemplate.update(sql1,new Object[] {
							  stuVO.getStudent_id(),stuVO.getLpass(),stuVO.getLtype(),stuVO.getStudent_id(),stuVO.getPhoto(),stuVO.getFname(),
							  stuVO.getSchool_Name(),stuVO.getAcademic_year(),stuVO.getSlogo(),stuVO.getSheader(),stuVO.getSfooter(),stuVO.getPass_change(),stuVO.getCemail()
					  
					  });			  

					try {
						String file1=stuVO.getPhoto();
						String path="F:\\ReadPhotos\\"+file1;
						System.out.println("Full path with file name   "+path);
						  File imagefile = new File(path);
						  
						  image = ImageIO.read(imagefile);
						  ImageIO.write(image, "jpg",new File("F:\\WritePhotos\\"+file1)); 
						  
						  
					} catch(IOException e) {
						System.out.println("Write error for --->  " + e.getMessage());
					  }
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		}
		else if(tran_type.equals("edit"))
		{
			System.out.println("********************************* Edit  ***************************");
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =StudentConstant.UPDATE_STUDENT ;
						  jdbcTemplate.update(sql,new Object[] {
								  stuVO.getFname(),stuVO.getLname(),/*stuVO.getDob(),*/stuVO.getBplace(),stuVO.getNationality(),
								  stuVO.getMtongue(),stuVO.getCgender(),stuVO.getReligion(),stuVO.getCategory(),stuVO.getAddress1(),
								  stuVO.getAddress2(),stuVO.getCity(),stuVO.getState(),stuVO.getPin(),stuVO.getMobile(),
								  stuVO.getPhone(),stuVO.getCemail()
								  
								  ,stuVO.getPre_school1()/*stuVO.getPre_qual1(),stuVO.getPrd_from1(),stuVO.getPrd_to1()*/
								  ,stuVO.getPre_school2()/*stuVO.getPre_qual2(),stuVO.getPrd_from2(),stuVO.getPrd_to2()*/
								  ,stuVO.getPre_school3()/*stuVO.getPre_qual3(),stuVO.getPrd_from3(),stuVO.getPrd_to3()*/
								  ,stuVO.getHostel_name(),stuVO.getHostel_Room_no(),stuVO.getStudent_id()
						  
						  });
				
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}
		}
		
		
		return Stuvo;
	}
	
	public ParentVO insertParentDetails(ParentVO parentVO) throws Exception 
	{
		
		ParentVO Parentvo = new ParentVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		String tran_type=parentVO.getTran_typ();
		
		System.out.println("******* Type   *******"+tran_type);
		
		if(tran_type.equals("insert"))
		{
		
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql_count2=StudentConstant.UPDATE_COUNTER_COUNT2 ;
			jdbcTemplate.update(sql_count2);
			
			String sql =StudentConstant.INSERT_PARENT ;
					  jdbcTemplate.update(sql,new Object[] {
							  parentVO.getParent_id(),parentVO.getPrt_fname(),parentVO.getPrt_father_dob()
							  ,parentVO.getPrt_occupa(),parentVO.getPrt_design(),parentVO.getPrt_off_add()
							  ,parentVO.getPrt_off_phn()
							  
							  , parentVO.getPrt_mname(),parentVO.getPrt_mother_dob(),parentVO.getMother_occupa(),parentVO.getMother_design(),
							  parentVO.getMother_off_add(),parentVO.getMother_off_phn()
							  
							  ,parentVO.getPrt_mobile(),parentVO.getPrt_email()
							  ,parentVO.getPrt_photo(),parentVO.getPrt_gname(),parentVO.getPrt_grd_occupa(),parentVO.getPrt_grd_design()
							  ,parentVO.getPrt_grd_off_add(),parentVO.getPrt_grd_home_add(),parentVO.getPrt_grd_mobile()
							  ,parentVO.getPrt_grd_email(),parentVO.getPrt_grd_relation(),parentVO.getIsActive(),parentVO.getInserted_by()
							  ,parentVO.getInserted_date(),parentVO.getUpdated_by(),parentVO.getUpdated_date(),parentVO.getRole()});
		   
			String sql1 =StudentConstant.INSERT_LOGIN_MASTER ;
					  jdbcTemplate.update(sql1,new Object[] {
							  parentVO.getPrt_mobile(),parentVO.getLpass(),parentVO.getLtype(),parentVO.getParent_id(),parentVO.getPrt_photo(),parentVO.getPrt_fname(),
							  parentVO.getSchool_Name(),parentVO.getAcademic_year(),parentVO.getSlogo(),parentVO.getSheader(),parentVO.getSfooter(),parentVO.getPass_change(),parentVO.getPrt_email()
							  
					  });	
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		}
		
		else if(tran_type.equals("edit"))
		{
			System.out.println("Parent ********************************* Edit  ***************************"+parentVO.getParent_id());
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =StudentConstant.UPDATE_PARENT ;
						  jdbcTemplate.update(sql,new Object[] {
								  parentVO.getPrt_fname(),parentVO.getPrt_father_dob()
								  ,parentVO.getPrt_occupa(),parentVO.getPrt_design(),parentVO.getPrt_off_add()
								  ,parentVO.getPrt_off_phn()
								  
								  , parentVO.getPrt_mname(),parentVO.getPrt_mother_dob()
								  
								  ,parentVO.getPrt_mobile(),parentVO.getPrt_email()
								  ,parentVO.getPrt_gname(),parentVO.getPrt_grd_occupa(),parentVO.getPrt_grd_design()
								  ,parentVO.getPrt_grd_off_add(),parentVO.getPrt_grd_home_add(),parentVO.getPrt_grd_mobile()
								  ,parentVO.getPrt_grd_email(),parentVO.getPrt_grd_relation(),parentVO.getParent_id()});
				
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}
		}
		
		return Parentvo;
	}
	
	public MedicalVO insertMedicalDetails(MedicalVO medicalVO) throws Exception 
	{
		
		MedicalVO Medicalvo = new MedicalVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			
			String sql =StudentConstant.INSERT_MEDICAL ;
					  jdbcTemplate.update(sql,new Object[] {
							  medicalVO.getStudent_id(),medicalVO.getBlood_group(), medicalVO.getHeight(),medicalVO.getWeights()
							  ,medicalVO.getMedical_condition(),medicalVO.getAllergy(),medicalVO.getAllergy_type(),medicalVO.getIs_Physically_Challenged(),medicalVO.getPhy_cha_type()
							  ,medicalVO.getIs_Special_Child(),medicalVO.getSpecialChild_type(),medicalVO.getIs_disease(),medicalVO.getDisease_type()
							  ,medicalVO.getIsActive(),medicalVO.getInserted_by()
							  ,medicalVO.getInserted_date(),medicalVO.getUpdated_by(),medicalVO.getUpdated_date()});
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Medicalvo;
	}
	
	
	public StuVO insertFinalDetails(StuVO stuVO) throws Exception 
	{
		
		StuVO Stuvo = new StuVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql_count1=StudentConstant.UPDATE_COUNTER_COUNT1 ;
			jdbcTemplate.update(sql_count1);

			/*String sql_count2=StudentConstant.UPDATE_COUNTER_COUNT2 ;
			jdbcTemplate.update(sql_count2);*/

			String sql_count3=StudentConstant.UPDATE_COUNTER_COUNT3 ;
			jdbcTemplate.update(sql_count3);

			String sql_count4=StudentConstant.UPDATE_COUNTER_COUNT4 ;
			jdbcTemplate.update(sql_count4);

			/*String sql_count5=StudentConstant.UPDATE_COUNTER_COUNT5 ;
			jdbcTemplate.update(sql_count5)*/;

			
			String sql =StudentConstant.UPDATE_STUDENT ;
					  jdbcTemplate.update(sql,new Object[] {
							  stuVO.getJoin_date(), stuVO.getHostel_name(),stuVO.getHostel_Room_no(),
							  stuVO.getSchool_Name(),stuVO.getAcademic_year(),stuVO.getSclass(),stuVO.getSection(),stuVO.getHostel(),stuVO.getHostel_allot(),stuVO.getTransport_board(),stuVO.getRole(),stuVO.getAdm_id()});
			
					  		  
			
					  
		System.out.println("Success insert finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		
		return Stuvo;
	}
	//updateCounter
	public CounterVO updateCounter(CounterVO counterVO) throws Exception 
	{
		
		CounterVO Countervo = new CounterVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();

		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.UPDATE_COUNTER ;
					  jdbcTemplate.update(sql,new Object[] {
							  counterVO.getCnt_Prifix(),counterVO.getCnt_Mod_Id()});
		System.out.println("Success Update................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		
		return Countervo;
	}
	//insertExamTypeMaster
	public ExamTypeMasterVO insertExamTypeMaster(ExamTypeMasterVO examtypeVO) throws Exception 
	{
		
		ExamTypeMasterVO Examtypevo = new ExamTypeMasterVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();

		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.EXAMTYPE_MASTER ;
					  jdbcTemplate.update(sql,new Object[] {
							  examtypeVO.getExam_type()});
		System.out.println("Success Insert in ExamTypeMaster................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		
		return Examtypevo;
	}
	
	//updateRoomAllot
	public StuVO updateRoomAllot(StuVO stuVO) throws Exception 
	{
		
		StuVO Stuvo = new StuVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql =StudentConstant.UPDATE_Hostel ;
					  jdbcTemplate.update(sql,new Object[] {
							  stuVO.getHostel_name(),stuVO.getStudent_id()});
			
			
					  
		System.out.println("Success update Hostel................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		
		return Stuvo;
	}
	
	
	//updateRollNumber
	public StuVO updateRollNumber(StuVO stuVO) throws Exception 
	{
		StuVO Stuvo = new StuVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =StudentConstant.UPDATE_ROLLNO;
					  jdbcTemplate.update(sql,new Object[] {
							  stuVO.getSclass(),stuVO.getSection()});
					  
		System.out.println("Success update Rollno................");
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		return Stuvo;
	}
	
	
	//insertAttendDetails
	public AttendanceVO insertAttendDetails(AttendanceVO attendVO) throws Exception 
	{
		
		AttendanceVO Attendvo = new AttendanceVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql =StudentConstant.INSERT_ATTENDANCE;
					  jdbcTemplate.update(sql,new Object[] {
							  attendVO.getAdate(), attendVO.getAclass(),attendVO.getAsection(),attendVO.getSid(),
							  attendVO.getFname(),attendVO.getLname(),attendVO.getAtt_status(),attendVO.getAtt_time(),attendVO.getStatus(),
							  attendVO.getIsActive(),attendVO.getInserted_By(),attendVO.getInserted_Date(),attendVO.getUpdated_By(),attendVO.getUpdated_Date()
					  });
			
			
					  
		System.out.println("Success insert finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		
		return Attendvo;
	}
	
	//insertMarks
	public MarksVO insertMarks(MarksVO marksVO) throws Exception 
	{
		
		MarksVO Marksvo = new MarksVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			//String sql =StudentConstant.INSERT_MARKS1;
					  /*jdbcTemplate.update(sql,new Object[] {
							  marksVO.getExam_id(),marksVO.getExam_name(),marksVO.getExam_type(),marksVO.getSclass(),
							  marksVO.getSsec(),marksVO.getStudent_id(),marksVO.getStudent_name(),
							
							  
							  marksVO.getTamil(),marksVO.getEnglish(),
							  marksVO.getMaths(),marksVO.getScience(),marksVO.getSscience(),marksVO.getTotal(),marksVO.getAvg(),marksVO.getResult(),
							  marksVO.getIsActive(),marksVO.getInserted_By(),marksVO.getInserted_Date(),marksVO.getUpdated_By(),marksVO.getUpdated_Date()
							  marksVO.getSubject(),marksVO.getMark()
					  });*/
			
			String sql =StudentConstant.INSERT_MARKSALL;
			jdbcTemplate.update(sql,new Object[] {
					  marksVO.getExam_id(),marksVO.getExam_name(),marksVO.getExam_type(),marksVO.getSclass(),
					  marksVO.getSsec(),marksVO.getStudent_id(),marksVO.getStudent_name(),
					  
					  marksVO.getSubject(),marksVO.getMark(),
					  marksVO.getIsActive(),marksVO.getInserted_By(),marksVO.getInserted_Date(),marksVO.getUpdated_By(),marksVO.getUpdated_Date()
			  });
			
			String sql1=StudentConstant.INSERT_MARKSALL_NEW;
			jdbcTemplate.update(sql1,new Object[] {
					  marksVO.getExam_id(),marksVO.getExam_name(),marksVO.getExam_type(),marksVO.getSclass(),
					  marksVO.getSsec(),marksVO.getStudent_id()
			  });
			
			
					  
		System.out.println("Success insert finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		
		return Marksvo;
	}
	
	
	public AttendanceVO checkAttendance(AttendanceVO attendVO) throws Exception 
	{
		System.out.println("Enter DAOImpl before   "+attendVO.getAsection());
		String sec=attendVO.getAsection();
		System.out.println("Enter DAOImpl After   "+sec);
		AttendanceVO stuVO=null;
		
		List<AttendanceVO> productslist = null;
		Session session = sessionFactory.openSession();
		try {
			System.out.println("Enter DAOImpl 1");
			stuVO = (AttendanceVO) session.createSQLQuery(StudentConstant.GET_ATTENDANCE_CHECK1)
					.setParameter("asec",sec)
					.setResultTransformer(Transformers.aliasToBean(AttendanceVO.class)).uniqueResult();
			System.out.println("Enter DAOImpl 2");
			stuVO.setAttendanceServiceVOList(productslist);
			System.out.println("Enter DAOImpl 3");
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return stuVO;
		
		
	}
	
	public StuVO deleteStudentsDetails(StuVO stuVO) throws Exception 
	{
		System.out.println(" delete daoimpl  "+stuVO.getStudent_id());
		StuVO Stuvo = new StuVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		try {
		
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			

			String sql =StudentConstant.DELETE_STUDENT ;
					  jdbcTemplate.update(sql,new Object[] {
							  stuVO.getStudent_id()});
					  
			String sql1 =StudentConstant.DELETE_STUDENT_LOGIN ;
					  jdbcTemplate.update(sql1,new Object[] {
							  stuVO.getStudent_id()});
			String sql2 =StudentConstant.DELETE_STUDENT_MEDICAL ;
					  jdbcTemplate.update(sql2,new Object[] {
							  stuVO.getStudent_id()});
			
		System.out.println("Success DELETE finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		
		return Stuvo;
	}
	
	//Show Records
	@SuppressWarnings("unchecked")	
	public List<StuVO> getLtrReferenceData() throws Exception 
	{
		List<StuVO> studentServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_STUDENT_LIST)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
	}
	
	//getTransportList
	@SuppressWarnings("unchecked")	
	public List<StuVO> getTransportList() throws Exception 
	{
		List<StuVO> studentServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_TRANSPORT_STUDENT_LIST)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
	}
	
	public List<StuMedicalVO> getLtrReferenceDataAll(String student_id) throws Exception
	{
		System.out.println("Enter DAOImpl  3 "+student_id);
		StuVO StuMedicalVO=null;
		
		List<StuMedicalVO> SuMedicalServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {

			SuMedicalServiceVOList = session.createSQLQuery(StudentConstant.SELECT)
					.setParameter("student_id", student_id)
					.setResultTransformer(Transformers.aliasToBean(StuMedicalVO.class)).list();
			
			//stuVO.setStudentServiceVOList(productslist);
			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return SuMedicalServiceVOList;
	}
	public List<MedicalVO> getMedical(String studid) throws Exception
	{
		System.out.println("Enter DAOImpl Medical details"+studid);
		MedicalVO MedicalVO=null;
		
		List<MedicalVO> setStudentMedicalServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {

			setStudentMedicalServiceVOList = session.createSQLQuery(StudentConstant.GET_STUDENT_LIST_MEDICAL)
					.setParameter("studid", studid)
					.setResultTransformer(Transformers.aliasToBean(MedicalVO.class)).list();
			
			//stuVO.setStudentServiceVOList(productslist);
			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return setStudentMedicalServiceVOList;
	}
	
	//getStudentLoginID
	public List<StuVO> getStudentLoginID(String email) throws Exception
	{
		System.out.println("Enter DAOImpl   "+email);
		List<StuVO> studentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_STUDENT_EMAIL_LOGIN)
					.setParameter("cemail", email)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return studentServiceVOList;
	}
	//getParentLoginID
	public List<ParentVO> getParentLoginID(String mobile) throws Exception
	{
		System.out.println("Enter DAOImpl   "+mobile);
		List<ParentVO> parentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			parentServiceVOList = session.createSQLQuery(StudentConstant.GET_PARENT_EMAIL_LOGIN)
					.setParameter("mobile", mobile)
					.setResultTransformer(Transformers.aliasToBean(ParentVO.class)).list();
			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return parentServiceVOList;
	}
	
	
	
	//Parent Students List
	
	public List<StuVO> getParentDataAll(String parid) throws Exception
	{
		System.out.println("Enter DAOImpl  PARENT   "+parid);
		
		List<StuVO> studentServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_PARENT_STU_LIST_ALL)
					.setParameter("parid", parid)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
	}
	
	//getParentid
	public List<StuVO> getParentid(String mobile) throws Exception
	{
		System.out.println("Enter DAOImpl  PARENT   "+mobile);
		
		List<StuVO> studentServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_PARENT_ID)
					.setParameter("mobile", mobile)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
	}
	
	
	
	//Students parent View
	
	public List<StuParVO> getStuParDataAll(String parent_id) throws Exception {
		System.out.println("Enter DAOImpl ---->%%%%   "+parent_id);
		
		List<StuParVO> setStudentServiceVOList = null;
	
		
		Session session = sessionFactory.openSession();
		
		try {
			setStudentServiceVOList = session.createSQLQuery(StudentConstant.GET_PARSTU)
					.setParameter("parent_id", parent_id)
					.setResultTransformer(Transformers.aliasToBean(StuParVO.class)).list();
			
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		
		
		
		return setStudentServiceVOList;
		
		
		
	}
	//parentList
	public List<ParentVO> parentList(String parent_id) throws Exception {
		System.out.println("Enter DAOImpl ---->%%%%   "+parent_id);
		
		List<ParentVO> parentServiceVOList = null;
	
		
		Session session = sessionFactory.openSession();
		
		try {
			parentServiceVOList = session.createSQLQuery(StudentConstant.GET_PARENT)
					.setParameter("parent_id", parent_id)
					.setResultTransformer(Transformers.aliasToBean(ParentVO.class)).list();
			
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		
		
		
		return parentServiceVOList;
		
		
		
	}
	
	public List<StuVO> getStudentClassAll(String sclass, String sec) throws Exception 
	{
		System.out.println("Enter DAOImpl  class "+sclass+" and "+sec);
		List<StuVO> studentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_CLASS_LIST_ALL)
					.setParameter("sclass", sclass)
					.setParameter("sec", sec)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
	}
	
	//totalStudent
	public List<StuVO> totalStudent(String sclass, String sec) throws Exception 
	{
		System.out.println("Enter DAOImpl  class "+sclass+" and "+sec);
		List<StuVO> studentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_TOTAL_STU)
					.setParameter("sclass", sclass)
					.setParameter("sec", sec)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		//System.out.println("Enter Return value "+studentServiceVOList.get(1));
		return studentServiceVOList;
	}
	
	//getStuAttendance
	public List<AttendanceVO> getStuAttendance(String fdate,String tdate,String atime,String sid) throws Exception 
	{
		System.out.println("enter dao impl  "+atime+" fdate  "+fdate+" tdate "+tdate+" studentid "+sid);
		List<AttendanceVO> attendanceServiceVOList = null;
		Session session = sessionFactory.openSession();
		
		/*String atime=attVO.getAtt_time();
		String sid=attVO.getSid();
		*/
		
		try {
			attendanceServiceVOList = session.createSQLQuery(StudentConstant.GET_STU_ATTEN_LIST)
					.setParameter("fdate", fdate)
					.setParameter("tdate", tdate)
					.setParameter("atime", atime)
					.setParameter("sid", sid)
					.setResultTransformer(Transformers.aliasToBean(AttendanceVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return attendanceServiceVOList;
	}
	
	//getStuAttendanceDayAll
	
	public List<AttendanceDayVO> getStuAttendanceDayAll(String mon,String monyear,String sclass,String sec) throws Exception 
	{
		System.out.println("enter dao impl  "+mon);
		List<AttendanceDayVO> attendanceDayServiceVOList = null;
		Session session = sessionFactory.openSession();

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String sql1="create or replace view attendance_dayStudentsTest as "+
						//"SELECT monthname(adate) as month,aclass,asection,sid,fname,"+
						"SELECT monthname(adate) as month,aclass,asection,sid,fname,"+
						"MONTH(adate) as mondate,"+
						"YEAR(adate) as monyear,"+
						"GROUP_CONCAT(if(DAY(`adate`) = 1, `att_status`, NULL)) AS 'day1',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 2, `att_status`, NULL)) AS 'day2',"+
						"GROUP_CONCAT(if(DAY(`adate`) = 3, `att_status`, NULL)) AS 'day3',"+
						"GROUP_CONCAT(if(DAY(`adate`) = 4, `att_status`, NULL)) AS 'day4',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 5, `att_status`, NULL)) AS 'day5',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 6, `att_status`, NULL)) AS 'day6',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 7, `att_status`, NULL)) AS 'day7',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 8, `att_status`, NULL)) AS 'day8',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 9, `att_status`, NULL)) AS 'day9',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 10, `att_status`, NULL)) AS 'day10',"+
						"GROUP_CONCAT(if(DAY(`adate`) = 11, `att_status`, NULL)) AS 'day11',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 12, `att_status`, NULL)) AS 'day12',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 13, `att_status`, NULL)) AS 'day13',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 14, `att_status`, NULL)) AS 'day14',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 15, `att_status`, NULL)) AS 'day15',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 16, `att_status`, NULL)) AS 'day16',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 17, `att_status`, NULL)) AS 'day17',"+
						"GROUP_CONCAT(if(DAY(`adate`) = 18, `att_status`, NULL)) AS 'day18',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 19, `att_status`, NULL)) AS 'day19',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 20, `att_status`, NULL)) AS 'day20',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 21, `att_status`, NULL)) AS 'day21',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 22, `att_status`, NULL)) AS 'day22',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 23, `att_status`, NULL)) AS 'day23',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 24, `att_status`, NULL)) AS 'day24',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 25, `att_status`, NULL)) AS 'day25',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 26, `att_status`, NULL)) AS 'day26',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 27, `att_status`, NULL)) AS 'day27',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 28, `att_status`, NULL)) AS 'day28',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 29, `att_status`, NULL)) AS 'day29',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 30, `att_status`, NULL)) AS 'day30',"+  
						"GROUP_CONCAT(if(DAY(`adate`) = 31, `att_status`, NULL)) AS 'day31'"+ 
						/*"COUNT(if(`att_status`='Present', `att_status`, NULL)) AS 'totalP'"+*/
						"FROM `attendance`"+
						//"GROUP BY RTRIM(LTRIM(sid))";
						"GROUP BY RTRIM(LTRIM(sid)) , MONTH(adate) , YEAR(adate)";
						
						jdbcTemplate.execute(sql1);
		
		
		try {
			attendanceDayServiceVOList = session.createSQLQuery(StudentConstant.GET_STU_ATTEN_LIST_DAYALL)
					.setParameter("mon", mon)
					.setParameter("monyear", monyear)
					.setParameter("aclass", sclass)
					.setParameter("asection", sec) 
					.setResultTransformer(Transformers.aliasToBean(AttendanceDayVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return attendanceDayServiceVOList;
	}
	
	//getTeacherAttendanceDayAll
	public List<AttendanceDayVO> getTeacherAttendanceDayAll(String month,String monyear) throws Exception 
	{
		System.out.println("enter dao impl ----------------->   "+month);
		List<AttendanceDayVO> attendanceDayServiceVOList = null;
		Session session = sessionFactory.openSession();

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String sql1="create or replace view attendance_dayTeacher_Test as "+
						"SELECT monthname(adate) as month,tid,tname,"+
						"MONTH(adate) as mondate,"+
						"YEAR(adate) as monyear,"+
						"GROUP_CONCAT(if(DAY(`adate`) = 1, `att_status`, NULL)) AS 'day1',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 2, `att_status`, NULL)) AS 'day2',"+
						"GROUP_CONCAT(if(DAY(`adate`) = 3, `att_status`, NULL)) AS 'day3',"+
						"GROUP_CONCAT(if(DAY(`adate`) = 4, `att_status`, NULL)) AS 'day4',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 5, `att_status`, NULL)) AS 'day5',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 6, `att_status`, NULL)) AS 'day6',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 7, `att_status`, NULL)) AS 'day7',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 8, `att_status`, NULL)) AS 'day8',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 9, `att_status`, NULL)) AS 'day9',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 10, `att_status`, NULL)) AS 'day10',"+
						"GROUP_CONCAT(if(DAY(`adate`) = 11, `att_status`, NULL)) AS 'day11',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 12, `att_status`, NULL)) AS 'day12',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 13, `att_status`, NULL)) AS 'day13',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 14, `att_status`, NULL)) AS 'day14',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 15, `att_status`, NULL)) AS 'day15',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 16, `att_status`, NULL)) AS 'day16',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 17, `att_status`, NULL)) AS 'day17',"+
						"GROUP_CONCAT(if(DAY(`adate`) = 18, `att_status`, NULL)) AS 'day18',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 19, `att_status`, NULL)) AS 'day19',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 20, `att_status`, NULL)) AS 'day20',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 21, `att_status`, NULL)) AS 'day21',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 22, `att_status`, NULL)) AS 'day22',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 23, `att_status`, NULL)) AS 'day23',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 24, `att_status`, NULL)) AS 'day24',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 25, `att_status`, NULL)) AS 'day25',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 26, `att_status`, NULL)) AS 'day26',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 27, `att_status`, NULL)) AS 'day27',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 28, `att_status`, NULL)) AS 'day28',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 29, `att_status`, NULL)) AS 'day29',"+ 
						"GROUP_CONCAT(if(DAY(`adate`) = 30, `att_status`, NULL)) AS 'day30',"+  
						"GROUP_CONCAT(if(DAY(`adate`) = 31, `att_status`, NULL)) AS 'day31'"+ 
						/*"COUNT(if(`att_status`='Present', `att_status`, NULL)) AS 'totalP'"+*/
						"FROM `attendance_teacher`"+
						//"GROUP BY RTRIM(LTRIM(tid))";
						"GROUP BY RTRIM(LTRIM(tid)) , MONTH(adate) , YEAR(adate)";
						
						jdbcTemplate.execute(sql1);
		
		
		try {
			attendanceDayServiceVOList = session.createSQLQuery(StudentConstant.GET_TEA_ATTEN_LIST_DAYALL)
					.setParameter("month", month)
					.setParameter("monyear", monyear)
					.setResultTransformer(Transformers.aliasToBean(AttendanceDayVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return attendanceDayServiceVOList;
	}
	
	
	
	//countAttendanceMonth		
	public List<AttendanceVO> countAttendanceMonth(String sclass,String sec,String amonth) throws Exception 
	{
		System.out.println("enter dao impl  "+amonth+" class  "+sclass+" sec "+sec);

		List<AttendanceVO> attendanceServiceVOList=null;
		List<DummyVO> dummyServiceVOList = null;
		Session session = sessionFactory.openSession();
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String sql1="truncate table dummy";
		jdbcTemplate.execute(sql1);
		
		
		
		//String sql="SELECT count(*) FROM attendance where adate='"+ adate +"' and aclass='"+sclass+"' and asection='"+sec+"' and att_status='Absent'";
		String sql="INSERT INTO dummy SELECT adate,count(*) as stucount,att_status FROM attendance WHERE MONTH(adate) = '"+amonth+"' AND YEAR(adate) = 2016 and aclass='"+sclass+"' and asection='"+sec+"' group by att_status";
		jdbcTemplate.execute(sql); 
		return attendanceServiceVOList;
	}	
	
	
	//showCount
	public List<DummyVO> showCount() throws Exception 
	{
		List<DummyVO> dummyServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			dummyServiceVOList = session.createSQLQuery(StudentConstant.GET_STU_ATTEN_LIST_COUNT)
					.setResultTransformer(Transformers.aliasToBean(DummyVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return dummyServiceVOList;
	}	
	
	
	
	
	
	
	
	
	//getStuAttendanceAll
	public List<AttendanceVO> getStuAttendanceAll(String fdate,String aclass,String asec) throws Exception 
	{
		System.out.println("enter dao impl  "+aclass+" fdate  "+fdate+" studentid "+asec);
		List<AttendanceVO> attendanceServiceVOList = null;
		Session session = sessionFactory.openSession();

		try {
			attendanceServiceVOList = session.createSQLQuery(StudentConstant.GET_STU_ATTEN_LIST_ALL)
					.setParameter("fdate", fdate)
					.setParameter("aclass", aclass)
					.setParameter("asec", asec)
					.setResultTransformer(Transformers.aliasToBean(AttendanceVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return attendanceServiceVOList;
	}
	//getStuAttendanceByDate
	public List<AttendanceVO> getStuAttendanceByDate(String fdate,String aclass,String asec) throws Exception 
	{
		System.out.println("enter dao impl  "+aclass+" fdate  "+fdate+" studentid "+asec);
		List<AttendanceVO> attendanceServiceVOList = null;
		Session session = sessionFactory.openSession();

		try {
			attendanceServiceVOList = session.createSQLQuery(StudentConstant.GET_STU_ATTEN_DATE_LIST_ALL)
					.setParameter("fdate", fdate)
					//.setParameter("tdate", tdate)
					.setParameter("aclass", aclass)
					.setParameter("asec", asec)
					.setResultTransformer(Transformers.aliasToBean(AttendanceVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return attendanceServiceVOList;
	}
	//getStuAttendanceByDateAbsent
	public List<AttendanceVO> getStuAttendanceByDateAbsent(String fdate,String aclass,String asec,String ttype) throws Exception 
	{
		System.out.println("enter dao impl  "+aclass+" fdate  "+fdate+" studentid "+asec);
		List<AttendanceVO> attendanceServiceVOList = null;
		Session session = sessionFactory.openSession();

		if(ttype.equals("Teacher"))
		{	
			try {
				attendanceServiceVOList = session.createSQLQuery(StudentConstant.GET_STU_ATTEN_DATE_LIST_ABSENT)
					.setParameter("fdate", fdate)
					.setParameter("aclass", aclass)
					.setParameter("asec", asec)
					.setResultTransformer(Transformers.aliasToBean(AttendanceVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		}
		return attendanceServiceVOList;
	}
	
	//getStuAttendanceByIDAbsent
	public List<AttendanceVO> getStuAttendanceByIDAbsent(String adate,String sid) throws Exception 
	{
		System.out.println("enter dao impl --->  "+adate+" sid  "+sid);
		List<AttendanceVO> attendanceServiceVOList = null;
		Session session = sessionFactory.openSession();

		
			try {
				attendanceServiceVOList = session.createSQLQuery(StudentConstant.GET_STU_ATTEN_ID_LIST_ABSENT)
					.setParameter("adate", adate)
					.setParameter("sid", sid)
					
					.setResultTransformer(Transformers.aliasToBean(AttendanceVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		
		return attendanceServiceVOList;
	}
	
	//countAttendanceToday
	public int countAttendanceToday(String fdate,String aclass,String asec) throws Exception 
	{
		System.out.println("date  "+fdate+" class  "+aclass+"  sec  "+asec);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		String sql="select count(*) as count from attendance where aclass='"+aclass+"' and asection='"+asec+"' and adate='"+fdate+"' and att_status='Present'";
		return jdbcTemplate.queryForInt(sql); 
	}
	//countTotalStudentsToday
	public int countTotalStudentsToday(String fdate,String aclass,String asec) throws Exception 
	{
		System.out.println("date  "+fdate+" class  "+aclass+"  sec  "+asec);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		String sql="select count(*) as count from studentsmaster where sclass='"+aclass+"' and section='"+asec+"'";
		return jdbcTemplate.queryForInt(sql); 
	}
	//topperClass
	public int topperClass(String fdate,String aclass,String asec) throws Exception 
	{
		System.out.println("date  "+fdate+" class  "+aclass+"  sec  "+asec);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		//String sql="select count(*) as count from attendance where aclass='"+aclass+"' and asection='"+asec+"' and adate='2016-09-17'";
		//String sql="select max(total) from marks_entry where sclass='"+aclass+"' and ssec='"+asec+"' and  Inserted_Date in (SELECT max(Inserted_Date) from marks_entry)";
		String sql="select max(total) from marks_total where sclass='"+aclass+"' and ssec='"+asec+"'";
		return jdbcTemplate.queryForInt(sql); 
	}
	//topperClassName
	public List<MarksVO> topperClassName(String fdate,String aclass,String asec) throws Exception 
	{
		System.out.println("date  "+fdate+" class  "+aclass+"  sec  "+asec);
		List<MarksVO> marksServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			marksServiceVOList = session.createSQLQuery(StudentConstant.GET_CLASS_TOPPER_NAME)
					.setParameter("sclass",aclass)
					.setParameter("ssec", asec)
					.setResultTransformer(Transformers.aliasToBean(MarksVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return marksServiceVOList;
	}
	

	//public List<StuParVO> getParentClassAll(String sclass) throws Exception 
	public List<StuParVO> getParentClassAll(String sclass,String sec) throws Exception
	{
		System.out.println("Enter DAOImpl  parent "+sclass);
		
		List<StuParVO> studentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_PARENT_LIST_ALL)
					.setParameter("sclass",sclass)
					.setParameter("sec", sec)
					.setResultTransformer(Transformers.aliasToBean(StuParVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
		
		 //Session sessionHb = session.openSession();
		 /*Session session = sessionFactory.openSession();
	     String sql = "select a.parent_id,a.prt_fname,b.student_id,b.fname,b.sclass,b.section from parentmaster a,studentsmaster b where a.parent_id=b.parent_id and b.sclass=:sclass";
	     SQLQuery query = session.createSQLQuery(sql);
	     query.setParameter("sclass", parentVO.getSclass());
	     query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
	     return query.list();*/
	}
	
	public List<StuVO> getAttendance(String sclass,String sec) throws Exception
	{
		System.out.println("Enter DAOImpl  Attendance "+sclass);
		
		List<StuVO> studentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_CLASS_LIST_ALL)
					.setParameter("sclass",sclass)
					.setParameter("sec", sec)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
	}
	//getLoginDetails
	public List<LoginVO> getLoginDetails(String uid,String pass) throws Exception
	{
		System.out.println("Enter DAOImpl  Attendance "+uid+" pass  :  "+pass);
		
		List<LoginVO> loginServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			loginServiceVOList = session.createSQLQuery(StudentConstant.GET_LOGIN)
					.setParameter("uname",uid)
					.setParameter("lpass", pass)
					.setResultTransformer(Transformers.aliasToBean(LoginVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return loginServiceVOList;
	}
	
	
	
	
	
	
	
	
	//getHostelList
	public List<StuVO> getHostelList(String gender) throws Exception
	{
		System.out.println("Enter DAOImpl  Attendance "+gender);
		
		List<StuVO> studentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_STUDENT_HOSTEL_GENDER)
					.setParameter("cgender",gender)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
	}
	//getTransStudent
	public List<StuVO> getTransStudent(String board)throws Exception
	{
		System.out.println("Transport DAO IMPL controller  "+board);
		List<StuVO> studentServiceVOList=null;
		Session session=sessionFactory.openSession();
		try
		{
			studentServiceVOList=session.createSQLQuery(StudentConstant.GET_TRANS_BOARD_LIST)
					.setParameter("transport_board",board)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
					
		}catch(Exception e)
		{
			System.out.println("Error in DAO  "+e);
		}
		return studentServiceVOList;
	}
	//getDorStudent
	public List<StuVO> getDorStudent(String hostelname)throws Exception
	{
		List<StuVO> studentServiceVOList=null;
		Session session=sessionFactory.openSession();
		try
		{
			studentServiceVOList=session.createSQLQuery(StudentConstant.GET_HOSTEL_STU_LIST)
					.setParameter("Hostel_name",hostelname)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
					
		}catch(Exception e)
		{
			System.out.println("Error in DAO  "+e);
		}
		return studentServiceVOList;
	}
	

	public StuVO getEditStudentsData(String studid) throws Exception
	{
		System.out.println("Enter DAOImpl");
		StuVO stuVO=null;
		
		List<StuVO> productslist = null;
		Session session = sessionFactory.openSession();
		try {

			stuVO = (StuVO) session.createSQLQuery(StudentConstant.GET_STUDENT_EDIT_LIST)
					.setParameter("studid", studid)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).uniqueResult();
			
			
			//stuVO.setStudentVOList(productslist);
			
		} catch (Exception e) {
			//logger.error("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}

		}
		return stuVO;
	}

	//Edit Students Details
	
	

	public StuVO editStudentDetails(StuVO stuVO) throws Exception 
	{
		
		StuVO Stuvo = new StuVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		System.out.println("*******EDIT  DAOIMPL Start *******");
		System.out.println("fname  "+stuVO.getFname());
		System.out.println("lname  "+stuVO.getLname());
		System.out.println("photo  "+stuVO.getPhoto());
		System.out.println("bplace  "+stuVO.getBplace());
		System.out.println("nationality  "+stuVO.getNationality());
		System.out.println("mtongue  "+stuVO.getMtongue());
		System.out.println("cgender  "+stuVO.getCgender());
		System.out.println("religion  "+stuVO.getReligion());
		System.out.println("category  "+stuVO.getCategory());
		System.out.println("address1  "+stuVO.getAddress1());
		System.out.println("address2  "+stuVO.getAddress2());
		System.out.println("state  "+stuVO.getState());
		System.out.println("pin  "+stuVO.getPin());
		System.out.println("mobile  "+stuVO.getMobile());
		System.out.println("phone  "+stuVO.getPhone());
		System.out.println("cemail"+stuVO.getCemail());
		System.out.println("sch_transport  "+stuVO.getSch_transport());
		
		
		System.out.println("******* DAOIMPL end *******");
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
						
			String sql =StudentConstant.EDIT_STUDENT ;
					  jdbcTemplate.update(sql,new Object[] {
							  stuVO.getFname(), stuVO.getLname(),stuVO.getPhoto()
							  ,stuVO.getDob(),stuVO.getBplace(),stuVO.getNationality(),stuVO.getMtongue(),stuVO.getCgender()
							  ,stuVO.getReligion(),stuVO.getCategory(),stuVO.getAddress1(),stuVO.getAddress2(),stuVO.getStudent_id()});
			
			
			
			
			System.out.println("Success Edit................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Stuvo;
	}
	
	@SuppressWarnings("unchecked")
	public List<StuVO> getAssignment(String sclass,String sec) throws Exception {
System.out.println("Enter DAOImpl  Assignment "+sclass+" and "+sec);
		
		List<StuVO> studentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_ASSIGNMENT_LIST)
					.setParameter("sclass",sclass)
					.setParameter("section",sec)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
	}
//getStuNameLike	
	@SuppressWarnings("unchecked")
	public List<StuVO> getStuNameLike(String sclass,String sec,String fname) throws Exception {
System.out.println("Enter DAOImpl  Assignment "+sclass+" and "+sec+" fname  "+fname);
		
		List<StuVO> studentServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			studentServiceVOList = session.createSQLQuery(StudentConstant.GET_STUDENTS_LIKE_LIST)
					.setParameter("sclass",sclass)
					.setParameter("section",sec)
					.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return studentServiceVOList;
	}
	
//getPhoto	
	@SuppressWarnings("unchecked")
	public List<LoginVO> getPhoto(String sid,String pass) throws Exception {
			//System.out.println("Enter DAOImpl  Assignment "+sclass+" and "+sec+" fname  "+fname);
		
		System.out.println("dao impl -----------------> "+sid+"  pass  "+pass);
		
		List<LoginVO> loginServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			loginServiceVOList = session.createSQLQuery(StudentConstant.GET_PHOTO)
					.setParameter("uname",sid)
					.setParameter("lpass",pass)
					.setResultTransformer(Transformers.aliasToBean(LoginVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return loginServiceVOList;
	}
	

	public AssignmentVO insertAssignmentDetails(AssignmentVO assignmentVO) throws Exception {
		AssignmentVO Assignmentvo = new AssignmentVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			

			String sql =StudentConstant.INSERT_ASSIGNMENT;
					  jdbcTemplate.update(sql,new Object[] {
							  assignmentVO.getAdate(), assignmentVO.getAclass(),assignmentVO.getAsection(),
							  assignmentVO.getFname(),assignmentVO.getAss_status(),assignmentVO.getStatus(),
							  assignmentVO.getIsActive(),assignmentVO.getInserted_By(),assignmentVO.getInserted_Date(),assignmentVO.getUpdated_By(),assignmentVO.getUpdated_Date()
					  });
			
			
					  
		System.out.println("Success insert finally................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error final------------>"+e);
			throw new Exception(e);
		}
		
		return Assignmentvo;
	}
	
	//msgCount
	public int msgCount(String mfrom) throws Exception 
	{
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String sql="select count(*) as msgcount from mail_master where mail_from='"+mfrom+"'";
		System.out.println("daoimpl  "+jdbcTemplate.queryForInt(sql));
		return jdbcTemplate.queryForInt(sql); 
	}
	
	//countStudents
		public int countStudents() throws Exception 
		{
			
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql="select count(*) as stucount from studentsmaster";
			System.out.println("daoimpl  "+jdbcTemplate.queryForInt(sql));
			return jdbcTemplate.queryForInt(sql); 
		}
	//countPresent	
		public int countPresent() throws Exception 
		{
			
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql="SELECT count(*) FROM attendance where (att_status='Present' or att_status='Permission') and DATE(adate) = DATE(NOW())";
			return jdbcTemplate.queryForInt(sql); 
		}
		//countAbsent	
				public int countAbsent() throws Exception 
				{
					
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					String sql="SELECT count(*) FROM attendance where att_status='Absent' and DATE(adate) = DATE(NOW())";
					return jdbcTemplate.queryForInt(sql); 
				}	
				
				
		//count Total,Present,Absent list based on class,section and date
				//countStudentsClass
				public int countStudentsClass(String sclass,String sec) throws Exception 
				{
					
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					String sql="select count(*) from studentsmaster where sclass='"+sclass+"' and section='"+sec+"' ";
					return jdbcTemplate.queryForInt(sql); 
				}
			//countPresentClass	
				public int countPresentClass(String sclass,String sec,String adate) throws Exception 
				{
					
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					String sql="SELECT count(*) FROM attendance where adate='"+ adate +"' and aclass='"+sclass+"' and asection='"+sec+"' and att_status='Present' or att_status='Permission'";
					return jdbcTemplate.queryForInt(sql); 
				}
				//countAbsentClass	
						public int countAbsentClass(String sclass,String sec,String adate) throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="SELECT count(*) FROM attendance where adate='"+ adate +"' and aclass='"+sclass+"' and asection='"+sec+"' and att_status='Absent'";
							return jdbcTemplate.queryForInt(sql); 
						}	
				//countAttTeacherMonthPresent		
						public int countAttTeacherMonthPresent(String amonth) throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="SELECT count(*) FROM attendance where att_status='Present' and MONTH(adate) ='"+ amonth +"' AND YEAR(adate) = 2016";
							return jdbcTemplate.queryForInt(sql); 
						}	
						
						public int countAttTeacherMonthAbsent(String amonth) throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="SELECT count(*) FROM attendance where att_status='Absent' and MONTH(adate) ='"+ amonth +"' AND YEAR(adate) = 2016";
							return jdbcTemplate.queryForInt(sql); 
						}
				
				//countPresentMonth	
						public int countPresentMonth(String sclass,String sec,String adate) throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							//String sql="SELECT count(*) FROM attendance where adate='"+ adate +"' and aclass='"+sclass+"' and asection='"+sec+"' and att_status='Present' or att_status='Permission'";
							String sql="SELECT count(*) FROM colegio_devp.attendance WHERE MONTH(adate) ='"+adate+"' AND YEAR(adate) = 2016 and aclass='"+sclass+"' and asection='"+sec+"' and (att_status='Present' or att_status='Permission')";
							return jdbcTemplate.queryForInt(sql); 
						}
				//countAbsentMonth	
								public int countAbsentMonth(String sclass,String sec,String adate) throws Exception 
								{
									
									JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
									//String sql="SELECT count(*) FROM attendance where adate='"+ adate +"' and aclass='"+sclass+"' and asection='"+sec+"' and att_status='Absent'";
									String sql="SELECT count(*) FROM colegio_devp.attendance WHERE MONTH(adate) ='"+adate+"' AND YEAR(adate) = 2016 and aclass='"+sclass+"' and asection='"+sec+"' and att_status='Absent'";
									return jdbcTemplate.queryForInt(sql); 
								}		
						
						
			
		//UserReport
						public int countParents() throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="SELECT count(*) as parcount FROM parentmaster";
							return jdbcTemplate.queryForInt(sql); 
						}			
						
						public int countTeachers() throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="SELECT count(*) FROM teacher_master";
							return jdbcTemplate.queryForInt(sql); 
						}	
						
						public int countPresentTeacher()throws Exception
						{
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="select count(*) from attendance_staff where checkin is not null and DATE(adate) = DATE(NOW()) and ttype='TeachingStaff'";
							return jdbcTemplate.queryForInt(sql); 
						}
						
						public int countAbsentTeacher()throws Exception
						{
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="select count(*) from attendance_staff where  att_status='Absent' and DATE(adate) = DATE(NOW()) and ttype='TeachingStaff'";
							return jdbcTemplate.queryForInt(sql); 
						}
						
						
						public int countLibrary() throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="SELECT count(*) FROM nonteachingstaff where Department='LIBRARIAN'";
							return jdbcTemplate.queryForInt(sql); 
						}	
						
						public int countDormitory() throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="SELECT count(*) FROM nonteachingstaff where Department='DORMITORY'";
							return jdbcTemplate.queryForInt(sql); 
						}	
						
						public int countTransport() throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="SELECT count(*) FROM transport";
							return jdbcTemplate.queryForInt(sql); 
						}	
						
						public int countTransportAO() throws Exception 
						{
							
							JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
							String sql="SELECT count(*) FROM nonteachingstaff where Department='TRANSPORT'";
							return jdbcTemplate.queryForInt(sql); 
						}	
				
						//getSClass
						@SuppressWarnings("unchecked")
						public List<StuVO> getstuid() throws Exception {
					System.out.println("Enter DAOImpl  student id ");
							
							List<StuVO> studentServiceVOList = null;
							Session session = sessionFactory.openSession();
							try {
								studentServiceVOList = session.createSQLQuery(StudentConstant.GET_STUID_LIST)
										
										.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
							} catch (Exception e) {
								System.out.println("Error in DAOIMPL "+e);
							}
							return studentServiceVOList;
						}
						
						//getTimeTable

						@SuppressWarnings("unchecked")
						public List<TimeTableVO> getTimeTable(String sclass,String sec) throws Exception {
					System.out.println("Enter DAOImpl  TimeTable   "+sclass+" and  "+sec);
							
							List<TimeTableVO> timeTableServiceVOList = null;
							Session session = sessionFactory.openSession();
							try {
								timeTableServiceVOList = session.createSQLQuery(StudentConstant.GET_TIMETABLE)
										.setParameter("sclass", sclass)
										.setParameter("section",sec)
										.setResultTransformer(Transformers.aliasToBean(TimeTableVO.class)).list();
							} catch (Exception e) {
								System.out.println("Error in DAOIMPL "+e);
							}
							return timeTableServiceVOList;
						}
					//getTimeTableDay	
						@SuppressWarnings("unchecked")
						public List<TimeTableVO> getTimeTableDay(TimeTableVO timetableVO) throws Exception {
					System.out.println("Enter DAOImpl  TimeTable  DAY  ----------------> "+timetableVO.getSclass()+" sec  "+timetableVO.getSection()+" day "+timetableVO.getDay());
							
							List<TimeTableVO> timeTableServiceVOList = null;
							Session session = sessionFactory.openSession();
							try {
								timeTableServiceVOList = session.createSQLQuery(StudentConstant.GET_TIMETABLE_DAY)
										.setParameter("sclass", timetableVO.getSclass())
										.setParameter("section",timetableVO.getSection())
										.setParameter("day", timetableVO.getDay())
										.setResultTransformer(Transformers.aliasToBean(TimeTableVO.class)).list();
							} catch (Exception e) {
								System.out.println("Error in DAOIMPL "+e);
							}
							return timeTableServiceVOList;
						}
						//getTimePeriod
						@SuppressWarnings("unchecked")
						public List<TimeTableVO> getTimePeriod() throws Exception {
					System.out.println("Enter DAOImpl  TimeTable  DAY  ----------------> ");
							
							List<TimeTableVO> timeTableServiceVOList = null;
							Session session = sessionFactory.openSession();
							try {
								timeTableServiceVOList = session.createSQLQuery(StudentConstant.GET_TIMETABLE_PERIOD)
										
										.setResultTransformer(Transformers.aliasToBean(TimeTableVO.class)).list();
							} catch (Exception e) {
								System.out.println("Error in DAOIMPL "+e);
							}
							return timeTableServiceVOList;
						}
						
						
						
				//TimeTableInsert
						public TimeTableVO TimeTableInsert(TimeTableVO timetableVO) throws Exception 
						{
							
							System.out.println("******* DAO Impl Start insert *******"+timetableVO.getTran_typ()+" period  "+timetableVO.getPeriod());
							
							
							TimeTableVO Timetablevo = new TimeTableVO();
							Session session = sessionFactory.openSession();
							Transaction trx = session.beginTransaction();
							
							String tran_type=timetableVO.getTran_typ();
							String period=timetableVO.getPeriod();
							
							System.out.println("******* Type   *******"+tran_type);
							
							if(tran_type.equals("insert"))
							{
								
							
								
							
								if(period.equals("7"))
								{
									System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%    Period 7   %%%%%%%%%%%%%%%%%%%%%%%%%%%");
									try {
										 
										JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
										String sql =StudentConstant.INSERT_TIMETABLE7 ;
												  jdbcTemplate.update(sql,new Object[] {
														  timetableVO.getSclass(),timetableVO.getSection(),timetableVO.getDay(),
														  timetableVO.getTime1(),timetableVO.getTime1_hr(),
														  timetableVO.getTime2(),timetableVO.getTime2_hr(),
														  timetableVO.getTime3(),timetableVO.getTime3_hr(),
														  timetableVO.getTime4(),timetableVO.getTime4_hr(),
														  timetableVO.getTime5(),timetableVO.getTime5_hr(),
														  timetableVO.getTime6(),timetableVO.getTime6_hr(),
														  timetableVO.getTime7(),timetableVO.getTime7_hr()
												  });
										
										System.out.println("Success Insert................");
										trx.commit();
												
									}catch (Exception e) {
										trx.rollback();
										System.out.println("Error ------------>"+e);
										throw new Exception(e);
									}
								  }		
								
								if(period.equals("8"))
								{
									System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%    Period 8   %%%%%%%%%%%%%%%%%%%%%%%%%%%");
									try {
										 
										JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
										String sql =StudentConstant.INSERT_TIMETABLE8 ;
												  jdbcTemplate.update(sql,new Object[] {
														  timetableVO.getSclass(),timetableVO.getSection(),timetableVO.getDay(),
														  timetableVO.getTime1(),timetableVO.getTime1_hr(),
														  timetableVO.getTime2(),timetableVO.getTime2_hr(),
														  timetableVO.getTime3(),timetableVO.getTime3_hr(),
														  timetableVO.getTime4(),timetableVO.getTime4_hr(),
														  timetableVO.getTime5(),timetableVO.getTime5_hr(),
														  timetableVO.getTime6(),timetableVO.getTime6_hr(),
														  timetableVO.getTime7(),timetableVO.getTime7_hr(),
														  timetableVO.getTime8(),timetableVO.getTime8_hr()
												  });
										
										System.out.println("Success Insert................");
										trx.commit();
												
									}catch (Exception e) {
										trx.rollback();
										System.out.println("Error ------------>"+e);
										throw new Exception(e);
									}
								  }		
								if(period.equals("9"))
								{
									System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%    Period 8   %%%%%%%%%%%%%%%%%%%%%%%%%%%");
									try {
										 
										JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
										String sql =StudentConstant.INSERT_TIMETABLE9 ;
												  jdbcTemplate.update(sql,new Object[] {
														  timetableVO.getSclass(),timetableVO.getSection(),timetableVO.getDay(),
														  timetableVO.getTime1(),timetableVO.getTime1_hr(),
														  timetableVO.getTime2(),timetableVO.getTime2_hr(),
														  timetableVO.getTime3(),timetableVO.getTime3_hr(),
														  timetableVO.getTime4(),timetableVO.getTime4_hr(),
														  timetableVO.getTime5(),timetableVO.getTime5_hr(),
														  timetableVO.getTime6(),timetableVO.getTime6_hr(),
														  timetableVO.getTime7(),timetableVO.getTime7_hr(),
														  timetableVO.getTime8(),timetableVO.getTime8_hr(),
														  timetableVO.getTime9(),timetableVO.getTime9_hr()
												  });
										
										System.out.println("Success Insert................");
										trx.commit();
												
									}catch (Exception e) {
										trx.rollback();
										System.out.println("Error ------------>"+e);
										throw new Exception(e);
									}
								  }		  
								
								if(period.equals("10"))
								{
									System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%    Period 8   %%%%%%%%%%%%%%%%%%%%%%%%%%%");
									try {
										 
										JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
										String sql =StudentConstant.INSERT_TIMETABLE10 ;
												  jdbcTemplate.update(sql,new Object[] {
														  timetableVO.getSclass(),timetableVO.getSection(),timetableVO.getDay(),
														  timetableVO.getTime1(),timetableVO.getTime1_hr(),
														  timetableVO.getTime2(),timetableVO.getTime2_hr(),
														  timetableVO.getTime3(),timetableVO.getTime3_hr(),
														  timetableVO.getTime4(),timetableVO.getTime4_hr(),
														  timetableVO.getTime5(),timetableVO.getTime5_hr(),
														  timetableVO.getTime6(),timetableVO.getTime6_hr(),
														  timetableVO.getTime7(),timetableVO.getTime7_hr(),
														  timetableVO.getTime8(),timetableVO.getTime8_hr(),
														  timetableVO.getTime9(),timetableVO.getTime9_hr(),
														  timetableVO.getTime10(),timetableVO.getTime10_hr()
														 
												  });
										
										System.out.println("Success Insert................");
										trx.commit();
												
									}catch (Exception e) {
										trx.rollback();
										System.out.println("Error ------------>"+e);
										throw new Exception(e);
									}
								  }		  
								if(period.equals("11"))
								{
									System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%    Period 8   %%%%%%%%%%%%%%%%%%%%%%%%%%%");
									try {
										 
										JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
										String sql =StudentConstant.INSERT_TIMETABLE11 ;
												  jdbcTemplate.update(sql,new Object[] {
														  timetableVO.getSclass(),timetableVO.getSection(),timetableVO.getDay(),
														  timetableVO.getTime1(),timetableVO.getTime1_hr(),
														  timetableVO.getTime2(),timetableVO.getTime2_hr(),
														  timetableVO.getTime3(),timetableVO.getTime3_hr(),
														  timetableVO.getTime4(),timetableVO.getTime4_hr(),
														  timetableVO.getTime5(),timetableVO.getTime5_hr(),
														  timetableVO.getTime6(),timetableVO.getTime6_hr(),
														  timetableVO.getTime7(),timetableVO.getTime7_hr(),
														  timetableVO.getTime8(),timetableVO.getTime8_hr(),
														  timetableVO.getTime9(),timetableVO.getTime9_hr(),
														  timetableVO.getTime10(),timetableVO.getTime10_hr(),
														  timetableVO.getTime11(),timetableVO.getTime11_hr()
														 
												  });
										
										System.out.println("Success Insert................");
										trx.commit();
												
									}catch (Exception e) {
										trx.rollback();
										System.out.println("Error ------------>"+e);
										throw new Exception(e);
									}
								  }		  
								if(period.equals("12"))
								{
									System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%    Period 8   %%%%%%%%%%%%%%%%%%%%%%%%%%%");
									try {
										 
										JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
										String sql =StudentConstant.INSERT_TIMETABLE12 ;
												  jdbcTemplate.update(sql,new Object[] {
														  timetableVO.getSclass(),timetableVO.getSection(),timetableVO.getDay(),
														  timetableVO.getTime1(),timetableVO.getTime1_hr(),
														  timetableVO.getTime2(),timetableVO.getTime2_hr(),
														  timetableVO.getTime3(),timetableVO.getTime3_hr(),
														  timetableVO.getTime4(),timetableVO.getTime4_hr(),
														  timetableVO.getTime5(),timetableVO.getTime5_hr(),
														  timetableVO.getTime6(),timetableVO.getTime6_hr(),
														  timetableVO.getTime7(),timetableVO.getTime7_hr(),
														  timetableVO.getTime8(),timetableVO.getTime8_hr(),
														  timetableVO.getTime9(),timetableVO.getTime9_hr(),
														  timetableVO.getTime10(),timetableVO.getTime10_hr(),
														  timetableVO.getTime11(),timetableVO.getTime11_hr(),
														  timetableVO.getTime12(),timetableVO.getTime12_hr()
														 
												  });
										
										System.out.println("Success Insert................");
										trx.commit();
												
									}catch (Exception e) {
										trx.rollback();
										System.out.println("Error ------------>"+e);
										throw new Exception(e);
									}
								  }		  
							
								if(period.equals("13"))
								{
									System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%    Period 8   %%%%%%%%%%%%%%%%%%%%%%%%%%%");
									try {
										 
										JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
										String sql =StudentConstant.INSERT_TIMETABLE13 ;
												  jdbcTemplate.update(sql,new Object[] {
														  timetableVO.getSclass(),timetableVO.getSection(),timetableVO.getDay(),
														  timetableVO.getTime1(),timetableVO.getTime1_hr(),
														  timetableVO.getTime2(),timetableVO.getTime2_hr(),
														  timetableVO.getTime3(),timetableVO.getTime3_hr(),
														  timetableVO.getTime4(),timetableVO.getTime4_hr(),
														  timetableVO.getTime5(),timetableVO.getTime5_hr(),
														  timetableVO.getTime6(),timetableVO.getTime6_hr(),
														  timetableVO.getTime7(),timetableVO.getTime7_hr(),
														  timetableVO.getTime8(),timetableVO.getTime8_hr(),
														  timetableVO.getTime9(),timetableVO.getTime9_hr(),
														  timetableVO.getTime10(),timetableVO.getTime10_hr(),
														  timetableVO.getTime11(),timetableVO.getTime11_hr(),
														  timetableVO.getTime12(),timetableVO.getTime12_hr(),
														  timetableVO.getTime13(),timetableVO.getTime13_hr()
														 
												  });
										
										System.out.println("Success Insert................");
										trx.commit();
												
									}catch (Exception e) {
										trx.rollback();
										System.out.println("Error ------------>"+e);
										throw new Exception(e);
									}
								  }		
								if(period.equals("14"))
								{
									System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%    Period 8   %%%%%%%%%%%%%%%%%%%%%%%%%%%");
									try {
										 
										JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
										String sql =StudentConstant.INSERT_TIMETABLE14 ;
												  jdbcTemplate.update(sql,new Object[] {
														  timetableVO.getSclass(),timetableVO.getSection(),timetableVO.getDay(),
														  timetableVO.getTime1(),timetableVO.getTime1_hr(),
														  timetableVO.getTime2(),timetableVO.getTime2_hr(),
														  timetableVO.getTime3(),timetableVO.getTime3_hr(),
														  timetableVO.getTime4(),timetableVO.getTime4_hr(),
														  timetableVO.getTime5(),timetableVO.getTime5_hr(),
														  timetableVO.getTime6(),timetableVO.getTime6_hr(),
														  timetableVO.getTime7(),timetableVO.getTime7_hr(),
														  timetableVO.getTime8(),timetableVO.getTime8_hr(),
														  timetableVO.getTime9(),timetableVO.getTime9_hr(),
														  timetableVO.getTime10(),timetableVO.getTime10_hr(),
														  timetableVO.getTime11(),timetableVO.getTime11_hr(),
														  timetableVO.getTime12(),timetableVO.getTime12_hr(),
														  timetableVO.getTime13(),timetableVO.getTime13_hr(),
														  timetableVO.getTime14(),timetableVO.getTime14_hr()
														 
												  });
										
										System.out.println("Success Insert................");
										trx.commit();
												
									}catch (Exception e) {
										trx.rollback();
										System.out.println("Error ------------>"+e);
										throw new Exception(e);
									}
								  }		
								
								
							}
							
							else if(tran_type.equals("edit"))
							{
								System.out.println("TimeTable ********************************* Edit  ***************************"+timetableVO.getDay()+" class  "+timetableVO.getSclass()+" sec  "+timetableVO.getSection());
								try {
									 
									JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
									

									String sql =StudentConstant.UPDATE_TIMETABLE ;
											  jdbcTemplate.update(sql,new Object[] {
													  timetableVO.getTime1(),timetableVO.getTime2(),timetableVO.getTime3(),
													  timetableVO.getTime4(),timetableVO.getTime5(),timetableVO.getTime6(),
													  timetableVO.getTime7(),timetableVO.getTime8(),timetableVO.getTime9(),
													  timetableVO.getTime10(),timetableVO.getTime11(),timetableVO.getTime12(),
													  timetableVO.getTime13(),timetableVO.getTime14(),
													  timetableVO.getSclass(),timetableVO.getSection(),timetableVO.getDay()
											  });
									
									System.out.println("Success update................");
									trx.commit();
											
								}catch (Exception e) {
									trx.rollback();
									System.out.println("Error ------------>"+e);
									throw new Exception(e);
								}
							}
							
							return Timetablevo;
						}

						public List<StuParVO> getStuParDataAll(String parent_id, String student_id) throws Exception {
							// TODO Auto-generated method stub
							return null;
						}		
						
						//getSClass
						
						public List<StuVO> getstudentdata(String stuid) throws Exception {
					System.out.println("Enter DAOImpl  student id "+stuid);
							
							List<StuVO> studentServiceVOList = null;
							Session session = sessionFactory.openSession();
							try {
								studentServiceVOList = session.createSQLQuery(StudentConstant.GET_STUDENT_LIST_ALL)
										.setParameter("stuid", stuid)
										.setResultTransformer(Transformers.aliasToBean(StuVO.class)).list();
							} catch (Exception e) {
								System.out.println("Error in DAOIMPL "+e);
							}
							return studentServiceVOList;
						}
						
	
}
