package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;


import com.dao.SubjectDao;
import com.dao.constant.SubjectConstant;
import com.dao.constant.TeacherConstant;
import com.model.MenuItemVO;
import com.model.SectionVO;
import com.model.SubjectVO;
import com.model.TeacherVO;


public class SubjectDaoImpl implements SubjectDao{

private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Autowired
	DataSource dataSource;

	

	///Nivetha(9-3-17)modification
	public SubjectVO insertSubjectDetails(SubjectVO subjectVO) throws Exception {
		SubjectVO Subjectvo = new SubjectVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		System.out.println("Teacher Insert DaoImpl ############################################################");
		String tran_type=subjectVO.getTran_typ();
		System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type);
		
		if(tran_type.equals("insert"))
		{	
			
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =SubjectConstant.INSERT_SUBJECT ;
					  jdbcTemplate.update(sql,new Object[] {
							  subjectVO.getSclass(), subjectVO.getSection(),subjectVO.getSubject() });
			System.out.println("Success insert................");
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		}
		
		else if(tran_type.equals("delete"))
		{
			System.out.println("********************************* delete  ***************************");
			try {
		
	JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =SubjectConstant.DELETE_SUBJECT;
						  jdbcTemplate.update(sql,new Object[] {
								  subjectVO.getSubject()
						 
						  });
						  
				
				
				System.out.println("Success Delete................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}
		}
		
		return Subjectvo;

	}
	
	
	//insertClass
	public SubjectVO insertClass(String[] myArray) throws Exception {
		
		for(int i=0;i<myArray.length;i++)
		{
			System.out.println(" array  "+myArray[i]);
		}
		
		SubjectVO Subjectvo = new SubjectVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql1 =SubjectConstant.DELETE_CLASS ;
			  jdbcTemplate.update(sql1);
			
			
			for(int i=0;i<myArray.length;i++)
			{
			String sql =SubjectConstant.INSERT_CLASS ;
					  jdbcTemplate.update(sql,new Object[] {
							  myArray[i] });
			}
			System.out.println("Success insert................");
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		
		return Subjectvo;

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	///Nivetha(9-3-17)modification
	
	
	//deleteSubject
	/*public SubjectVO deleteSubject(SubjectVO subjectVO) throws Exception {
		SubjectVO Subjectvo = new SubjectVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =SubjectConstant.DELETE_SUBJECT ;
					  jdbcTemplate.update(sql,new Object[] {
							  subjectVO.getSclass(), subjectVO.getSection(),subjectVO.getSubject() });
			System.out.println("Success Delete................");
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return Subjectvo;

	}
	*/
	//insertSection
	public SectionVO insertSection(SectionVO sectionVO) throws Exception
	{
		System.out.println("DAO Impl");
		SectionVO Sectionvo = new SectionVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		String ttype=sectionVO.getTran_type();
		
		System.out.println("Transaction Type ----------------------------------->   "+ttype);
		
		if(ttype.equalsIgnoreCase("insert"))
				{
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =SubjectConstant.INSERT_SECTION ;
					  jdbcTemplate.update(sql,new Object[] {
							  sectionVO.getSclass(), sectionVO.getSection() });
			System.out.println("Success insert Section................");
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
				}
		if(ttype.equals("delete"))
		{
			System.out.println("classssssssssssss ------------------------------->   "+sectionVO.getSclass());
			System.out.println("limit delete ------------------------------->    "+sectionVO.getDele());
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				String sql =SubjectConstant.DELETE_SECTION ;
						  jdbcTemplate.update(sql,new Object[] {
								  sectionVO.getSclass(),sectionVO.getDele() });
				System.out.println("Success insert Section................");
				trx.commit();
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}
		}
				
			
		
		return Sectionvo;

	}

	@SuppressWarnings("unchecked")
	public List<TeacherVO> getSubject(String sclass, String section,String ttype,String tid) throws Exception {
System.out.println("Enter DAOImpl  Subject "+sclass+" and "+section);
		
		List<TeacherVO> teacherServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			teacherServiceVOList = session.createSQLQuery(SubjectConstant.GET_SUBJECT_LIST)
					.setParameter("sclass",sclass)
					.setParameter("section",section)
					.setParameter("ttype",ttype)
					.setParameter("Tid",tid)
					.setResultTransformer(Transformers.aliasToBean(TeacherVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return teacherServiceVOList;
	}
//getTeacherSubject	
	@SuppressWarnings("unchecked")
	public List<SubjectVO> getTeacherSubject(String sclass, String section) throws Exception {
System.out.println("Enter DAOImpl  Subject -----------> "+sclass+" and "+section);
		
		List<SubjectVO> subjectServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			subjectServiceVOList = session.createSQLQuery(SubjectConstant.GET_SUBJECT_TEACHER_LIST)
					.setParameter("sclass",sclass)
					.setParameter("section",section)
					.setResultTransformer(Transformers.aliasToBean(SubjectVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return subjectServiceVOList;
	}
//getSClassMaster	
	@SuppressWarnings("unchecked")
	public List<SubjectVO> getSClassMaster() throws Exception {
System.out.println("Enter DAOImpl  Subject -----------> ");
		
		List<SubjectVO> subjectServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			subjectServiceVOList = session.createSQLQuery(SubjectConstant.GET_CLASSMASTER)
					
					.setResultTransformer(Transformers.aliasToBean(SubjectVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return subjectServiceVOList;
	}
	
//getSClass
	@SuppressWarnings("unchecked")
	public List<SectionVO> getSClass() throws Exception {
System.out.println("Enter DAOImpl  Subject ");
		
		List<SectionVO> sectionServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			sectionServiceVOList = session.createSQLQuery(SubjectConstant.GET_CLASS_LIST)
					
					.setResultTransformer(Transformers.aliasToBean(SectionVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return sectionServiceVOList;
	}
//getMenuData	
	@SuppressWarnings("unchecked")
	public List<MenuItemVO> getMenuData(String mtype) throws Exception {
System.out.println("Enter DAOImpl  Subject ");
		
		List<MenuItemVO> menuitemServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			menuitemServiceVOList = session.createSQLQuery(SubjectConstant.GET_MENU_LIST)
					.setParameter("mtype", mtype)
					.setResultTransformer(Transformers.aliasToBean(MenuItemVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return menuitemServiceVOList;
	}
	
//getSection
	@SuppressWarnings("unchecked")
	public List<SectionVO> getSection(String sclass) throws Exception {
System.out.println("Enter DAOImpl  Subject ");
		
		List<SectionVO> sectionServiceVOList = null;
		Session session = sessionFactory.openSession();
		try {
			sectionServiceVOList = session.createSQLQuery(SubjectConstant.GET_SECTION_LIST)
					.setParameter("sclass",sclass)
					.setResultTransformer(Transformers.aliasToBean(SectionVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return sectionServiceVOList;
	}
	public SubjectVO deleteSubject(SubjectVO subjectVO) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
