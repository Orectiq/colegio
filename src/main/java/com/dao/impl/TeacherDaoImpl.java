package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.dao.TeacherDao;
import com.dao.constant.StudentConstant;
import com.dao.constant.TeacherConstant;
import com.model.AbsenceRequestVO;
import com.model.ClassSectionVO;
import com.model.GroupVO;
import com.model.LoginVO;
import com.model.MailBackupVO;
import com.model.MailVO;
import com.model.ParentVO;
import com.model.StuVO;
import com.model.SubjectAssignVO;
import com.model.TeacherVO;
import com.model.staffAttendanceVO;
import com.model.teacherAttendanceVO;

public class TeacherDaoImpl implements TeacherDao
{
	@Autowired
	/*private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}*/
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
	
	
	@Autowired
	 DataSource dataSource;
	
	@SuppressWarnings("unchecked")
	public List<Object> getTeacId(String ref) throws Exception
	{
		System.out.println("enter getstuid daiImpl  "+ref);
		List<Object> obj = null;
		Session session = sessionFactory.openSession();
		try {
			
				obj = session.createQuery(TeacherConstant.GET_TEACHERID).list();
		} catch (Exception e) {
			System.out.println("Could not get  Reference Number : \n" + e);
			throw new Exception(e);
		} finally {
			if (session != null) {
				session.close();
			}
			
		}
		return obj;
	}
	
	public TeacherVO insertTeacherDetails(TeacherVO teacherVO) throws Exception 
	{
		System.out.println("Teacher Insert DaoImpl ############################################################");
		TeacherVO Teachervo = new TeacherVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		String tran_type=teacherVO.getTran_typ();
		System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type);
		if(tran_type.equals("insert"))
		{	
			
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			
			
			String sql_count4=TeacherConstant.UPDATE_COUNTER_COUNT;
			jdbcTemplate.update(sql_count4);
			
			String sql =TeacherConstant.INSERT_TEACHER ;
					  jdbcTemplate.update(sql,new Object[] {
							  teacherVO.getTid(),teacherVO.getFname(), teacherVO.getLname()
							  ,teacherVO.getPhoto(),teacherVO.getDob(),teacherVO.getNationality()
							  ,teacherVO.getCgender(),teacherVO.getAddress1(),teacherVO.getAddress2()
							  ,teacherVO.getCity(),teacherVO.getState(),teacherVO.getPin()
							  ,teacherVO.getMobile(),teacherVO.getPhone(),teacherVO.getCemail()
							  ,teacherVO.getPan() ,teacherVO.getLallowed(),teacherVO.getLapply()
							  ,teacherVO.getLbalance()
							  
							  ,teacherVO.getTech_school1_name(),teacherVO.getTech_per1(),teacherVO.getAyear1()
							  ,teacherVO.getTech_school2_name(),teacherVO.getTech_per2(),teacherVO.getAyear2()
							  ,teacherVO.getTech_school3_name(),teacherVO.getTech_per3(),teacherVO.getAyear3()
							  ,teacherVO.getTech_school4_name(),teacherVO.getTech_per4(),teacherVO.getAyear4()
							  
							  ,teacherVO.getTech_prev_emp1(),teacherVO.getTech_prev_emp_desg1(),teacherVO.getTech_year_of_exp1()
							  ,teacherVO.getTech_prev_emp2(),teacherVO.getTech_prev_emp_desg2(),teacherVO.getTech_year_of_exp2()
							  ,teacherVO.getTech_prev_emp3(),teacherVO.getTech_prev_emp_desg3(),teacherVO.getTech_year_of_exp3()
							  
							  ,teacherVO.getIsActive(),teacherVO.getCreated_by(),teacherVO.getCreated_date(),teacherVO.getUpdated_by(),teacherVO.getUpdated_date()
							  ,teacherVO.getRole()
							  
							  
							  });
					  
					  String sql1 =StudentConstant.INSERT_LOGIN_MASTER ;
					  jdbcTemplate.update(sql1,new Object[] {
							  teacherVO.getMobile(),teacherVO.getLpass(),teacherVO.getLtype(),teacherVO.getTid(),teacherVO.getPhoto(),teacherVO.getFname(),
							  teacherVO.getSname(),teacherVO.getAyear(),teacherVO.getSlogo(),teacherVO.getSheader(),teacherVO.getSfooter(),
							  teacherVO.getPass_change(),teacherVO.getCemail()
					  });
					  
					  
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		}
		else if(tran_type.equals("edit"))
		{
			System.out.println("********************************* Edit  ***************************");
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =TeacherConstant.UPDATE_TEACHER ;
						  jdbcTemplate.update(sql,new Object[] {
								 teacherVO.getFname(), teacherVO.getLname()
								  ,teacherVO.getDob(),teacherVO.getNationality()
								  ,teacherVO.getCgender(),teacherVO.getAddress1(),teacherVO.getAddress2()
								  ,teacherVO.getCity(),teacherVO.getState(),teacherVO.getPin()
								  ,teacherVO.getMobile(),teacherVO.getPhone(),teacherVO.getCemail()
								  ,teacherVO.getPan(), teacherVO.getTid()
						  
						  });
				
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			
		}
		if(tran_type.equals("delete"))
		{
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =TeacherConstant.DELETE_TEACHER ;
						  jdbcTemplate.update(sql,new Object[] {
								  teacherVO.getTid()
						 
						  });
						  
				String sql1 =TeacherConstant.DELETE_LOGIN ;
						  jdbcTemplate.update(sql1,new Object[] {
								  teacherVO.getTid()
						 
						  });
				
				System.out.println("Success Delete................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
		}
		
		return Teachervo;
	}
	
	//subjectAssignInsert
	public SubjectAssignVO subjectAssignInsert(SubjectAssignVO subjectAssignVO) throws Exception 
	{
		System.out.println("Teacher Insert DaoImpl");
		SubjectAssignVO Subjectassignvo = new SubjectAssignVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		
		String tran_type=subjectAssignVO.getTran_type() ;
		
		System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type+"Group name  "+subjectAssignVO.getSubject_handle());
		if(tran_type.equals("insert"))
		{	
		
		
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql =TeacherConstant.INSERT_TEACHER_SUBJECT_ASSIGN ;
					  jdbcTemplate.update(sql,new Object[] {
							  subjectAssignVO.getTid(),subjectAssignVO.getTname(),subjectAssignVO.getSclass(),subjectAssignVO.getSection(),subjectAssignVO.getTtype(),
							  subjectAssignVO.getSubject_handle(),subjectAssignVO.getMobile()
							  
							  });
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		
		}
		else if(tran_type.equals("edit"))
		{
			System.out.println("********************************* Edit  ***************************");
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =TeacherConstant.UPDATE_TEACHER_SUBJECT_ASSIGN ;
						  jdbcTemplate.update(sql,new Object[] {
								  subjectAssignVO.getTid(),subjectAssignVO.getTname(),subjectAssignVO.getSclass(),subjectAssignVO.getSection(),subjectAssignVO.getTtype(),
								  subjectAssignVO.getSubject_handle(),subjectAssignVO.getMobile()
						  
						  });
				
				System.out.println("Success update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			
		}
		if(tran_type.equals("delete"))
		{
			System.out.println("********************************* delete  ***************************"+tran_type);
			try {
				 
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				

				String sql =TeacherConstant.DELETE_TEACHER_SUBJECT_ASSIGN ;
						  jdbcTemplate.update(sql,new Object[] {
								  subjectAssignVO.getSubject_handle()
						 
						  });
						  
				
				
				System.out.println("Success Delete................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error Delete------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
		}
		
		
		return Subjectassignvo;
	}
	
	
	
	
	
	
	
	
	//teacherAttenInsert
	public teacherAttendanceVO teacherAttenInsert(teacherAttendanceVO teacherVO) throws Exception 
	{
		System.out.println("Teacher Insert DaoImpl");
		teacherAttendanceVO Teachervo = new teacherAttendanceVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =TeacherConstant.INSERT_ATTENDANCE_TEACHER ;
					  jdbcTemplate.update(sql,new Object[] {
							  teacherVO.getAdate(),teacherVO.getCheckin(), teacherVO.getTid(),teacherVO.getTname()
							  });
			System.out.println("Success insert................");
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		return Teachervo;
	}
	
	//teacherAttenUpdate
	public teacherAttendanceVO teacherAttenUpdate(teacherAttendanceVO teacherVO) throws Exception 
	{
		System.out.println("Teacher update DaoImpl  "+teacherVO.getAdate()+"  and  "+teacherVO.getTid());
		teacherAttendanceVO Teachervo = new teacherAttendanceVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			String sql =TeacherConstant.UPDATE_ATTENDANCE_TEACHER ;
					  jdbcTemplate.update(sql,new Object[] {
							  teacherVO.getCheckout(),teacherVO.getAtt_status(),teacherVO.getAdate(), teacherVO.getTid()
							  });
			System.out.println("Success Update................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}finally {
			if (session != null) {
				session.close();
			}
		}	
		
		return Teachervo;
	}
	
	
	//staffAttenInsert
		public staffAttendanceVO staffAttenInsert(staffAttendanceVO teacherVO) throws Exception 
		{
			System.out.println("Teacher Insert DaoImpl");
			staffAttendanceVO Teachervo = new staffAttendanceVO();
			Session session = sessionFactory.openSession();
			Transaction trx = session.beginTransaction();
			try {
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				String sql =TeacherConstant.INSERT_ATTENDANCE_STAFF ;
						  jdbcTemplate.update(sql,new Object[] {
								  teacherVO.getAdate(),teacherVO.getCheckin(), teacherVO.getTid(),teacherVO.getTname(),teacherVO.getTtype()
								  });
				System.out.println("Success insert................");
				trx.commit();
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			return Teachervo;
		}
		//composeMail	
		public MailVO composeMail(MailVO mailVO) throws Exception 
		{
			System.out.println("Teacher Insert DaoImpl");
			MailVO Mailvo = new MailVO();
			Session session = sessionFactory.openSession();
			Transaction trx = session.beginTransaction();
			try {
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				String sql =TeacherConstant.INSERT_COMPOSE ;
						  jdbcTemplate.update(sql,new Object[] {
								  mailVO.getMail_from(),mailVO.getMail_to(),mailVO.getSubj(),mailVO.getDesc(),mailVO.getMessage(),mailVO.getAttach(),
								  mailVO.getMdate(),mailVO.getMtime()
								  });
				System.out.println("Success insert................");
				trx.commit();
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			return Mailvo;
		}
		
		//groupMail
		public GroupVO groupMail(GroupVO groupVO) throws Exception 
		{
			System.out.println("Group Insert DaoImpl"+groupVO.getGroup_name());
			GroupVO Groupvo = new GroupVO();
			Session session = sessionFactory.openSession();
			Transaction trx = session.beginTransaction();
			
			String tran_type=groupVO.getTran_typ();
			
			System.out.println("Trans type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+tran_type+"Group name  "+groupVO.getGroup_name());
			if(tran_type.equals("insert"))
			{	
			
			
			try {
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				String sql =TeacherConstant.INSERT_GROUP ;
						  jdbcTemplate.update(sql,new Object[] {
								  groupVO.getGroup_name(),groupVO.getGroup_mem()
								  });
				System.out.println("Success insert................");
				trx.commit();
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			}
			
			else if(tran_type.equals("edit"))
			{
				System.out.println("********************************* Edit  ***************************"+groupVO.getGroup_name());
				try {
					 
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					

					String sql =TeacherConstant.DELETE_GROUP ;
							  jdbcTemplate.update(sql,new Object[] {
									  groupVO.getGroup_name()
							  
							  });
							  
							  String sql1 =TeacherConstant.INSERT_GROUP ;
							  jdbcTemplate.update(sql1,new Object[] {
									  groupVO.getGroup_name(),groupVO.getGroup_mem()
									  });
					
					System.out.println("Success update................");
					trx.commit();
							
				}catch (Exception e) {
					trx.rollback();
					System.out.println("Error ------------>"+e);
					throw new Exception(e);
				}finally {
					if (session != null) {
						session.close();
					}
				}	
				
				
			}
			if(tran_type.equals("delete"))
			{
				try {
					 
					JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
					

					String sql =TeacherConstant.DELETE_GROUP ;
							  jdbcTemplate.update(sql,new Object[] {
									  groupVO.getGroup_name()
							 
							  });
							  
					
					System.out.println("Success Delete................");
					trx.commit();
							
				}catch (Exception e) {
					trx.rollback();
					System.out.println("Error Delete------------>"+e);
					throw new Exception(e);
				}finally {
					if (session != null) {
						session.close();
					}
				}	
			}
			
			return Groupvo;
		}
		
		//deleteMail
		public MailVO deleteMail(MailVO mailVO) throws Exception 
		{
			System.out.println("Teacher Delete DaoImpl  ---------------------------------> "+mailVO.getMdate()+" and "+mailVO.getMtime());
			MailVO Mailvo = new MailVO();
			Session session = sessionFactory.openSession();
			Transaction trx = session.beginTransaction();
			try {
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				
				String sql1 =TeacherConstant.INSERT_COMPOSE_BACKUP ;
				  jdbcTemplate.update(sql1,new Object[] {
						  mailVO.getMail_from(),mailVO.getMail_to(),mailVO.getSubj(),mailVO.getDesc(),mailVO.getMessage(),mailVO.getAttach(),
						  mailVO.getMdate(),mailVO.getMtime()
						  });
				
				
				String sql =TeacherConstant.DELETE_COMPOSE ;
						  jdbcTemplate.update(sql,new Object[] {
								  mailVO.getMdate(),mailVO.getMtime()
								  });
				System.out.println("Success Delete................");
				trx.commit();
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			return Mailvo;
		}
		
		
		//staffAttenUpdate
		public staffAttendanceVO staffAttenUpdate(staffAttendanceVO teacherVO) throws Exception 
		{
			System.out.println("Teacher update DaoImpl  "+teacherVO.getAdate()+"  and  "+teacherVO.getTid());
			staffAttendanceVO Teachervo = new staffAttendanceVO();
			Session session = sessionFactory.openSession();
			Transaction trx = session.beginTransaction();
			try {
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				String sql =TeacherConstant.UPDATE_ATTENDANCE_STAFF ;
						  jdbcTemplate.update(sql,new Object[] {
								  teacherVO.getCheckout(),teacherVO.getAtt_status(),teacherVO.getAdate(), teacherVO.getTid()
								  });
				System.out.println("Success Update................");
				trx.commit();
						
			}catch (Exception e) {
				trx.rollback();
				System.out.println("Error ------------>"+e);
				throw new Exception(e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			
			return Teachervo;
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//Show Records
		@SuppressWarnings("unchecked")	
		public List<TeacherVO> getltrreferencedata() throws Exception 
		{
			List<TeacherVO> teacherServiceVOList = null;
			
			Session session = sessionFactory.openSession();
			try {
				teacherServiceVOList = session.createSQLQuery(TeacherConstant.GET_TEACHER_LIST)
						.setResultTransformer(Transformers.aliasToBean(TeacherVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return teacherServiceVOList;
		}
		
		
		public List<TeacherVO> getLtrReferenceDataAll(String techid) throws Exception
		{
			System.out.println("Enter DAOImpl  "+techid);
			TeacherVO teacherVO=null;
			List<TeacherVO> teacherServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {

				teacherServiceVOList = session.createSQLQuery(TeacherConstant.GET_TEACHER_LIST_ALL)
						.setParameter("techid", techid)
						.setResultTransformer(Transformers.aliasToBean(TeacherVO.class)).list();
				//teacherVO.setTeacherServiceVOList(productslist);
			} catch (Exception e) {
				throw new Exception(e);
			} finally {
				if (session != null) {
					session.close();
				}
			}
			return teacherServiceVOList;
		}
		
		//getClassSectionAll
		public List<ClassSectionVO> getClassSectionAll(String techid) throws Exception
		{
			System.out.println("Enter DAOImpl  "+techid);
			TeacherVO teacherVO=null;
			List<ClassSectionVO> teacherServiceVOList = null;
			Session session = sessionFactory.openSession();
			try {

				teacherServiceVOList = session.createSQLQuery(TeacherConstant.GET_TEACHER_CLASS_LIST_ALL)
						.setParameter("tid", techid)
						.setResultTransformer(Transformers.aliasToBean(ClassSectionVO.class)).list();
				//teacherVO.setTeacherServiceVOList(productslist);
			} catch (Exception e) {
				throw new Exception(e);
			} finally {
				if (session != null) {
					session.close();
				}
			}
			return teacherServiceVOList;
		}
		
		public List<TeacherVO> getTeacherSubject(String sclass,String sec)throws Exception
		{
			TeacherVO teacherVO=null;
			List<TeacherVO> teacherServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				teacherServiceVOList=session.createSQLQuery(TeacherConstant.GET_TEACHER_SUBJECT)
						.setParameter("sclass",sclass)
						.setParameter("section",sec)
						.setResultTransformer(Transformers.aliasToBean(TeacherVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println(rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return teacherServiceVOList;
		}
		//mailInbox
		public List<MailVO> mailInbox(String mailto)throws Exception
		{
			MailVO mailVO=null;
			List<MailVO> mailServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				mailServiceVOList=session.createSQLQuery(TeacherConstant.GET_INBOX)
						.setParameter("mail_to",mailto)
						.setResultTransformer(Transformers.aliasToBean(MailVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println(rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return mailServiceVOList;
		}
		
		//getGroupMail
		public List<GroupVO> getGroupMail(String gname)throws Exception
		{
			System.out.println("daoimpl  groupmail  "+gname);
			GroupVO groupVO=null;
			List<GroupVO> groupServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				groupServiceVOList=session.createSQLQuery(TeacherConstant.GET_INBOX_GROUP)
						.setParameter("group_name",gname)
						.setResultTransformer(Transformers.aliasToBean(GroupVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println(rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return groupServiceVOList;
		}
		//mailInboxDate
		public List<MailVO> mailInboxDate(String mailto,String mdate)throws Exception
		{
			MailVO mailVO=null;
			List<MailVO> mailServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				mailServiceVOList=session.createSQLQuery(TeacherConstant.GET_INBOX_DATE)
						.setParameter("mail_to",mailto)
						.setParameter("mdate",mdate)
						.setResultTransformer(Transformers.aliasToBean(MailVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println(rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return mailServiceVOList;
		}
		//mailInboxDateTime
		public List<MailVO> mailInboxDateTime(String mailto,String mdate,String mtime)throws Exception
		{
			MailVO mailVO=null;
			List<MailVO> mailServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				mailServiceVOList=session.createSQLQuery(TeacherConstant.GET_INBOX_DATE_TIME)
						.setParameter("mail_to",mailto)
						.setParameter("mdate",mdate)
						.setParameter("mtime",mtime)
						.setResultTransformer(Transformers.aliasToBean(MailVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println(rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return mailServiceVOList;
		}
		
		
		//mailSend
		public List<MailVO> mailSend(String mailto)throws Exception
		{
			MailVO mailVO=null;
			List<MailVO> mailServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				mailServiceVOList=session.createSQLQuery(TeacherConstant.GET_INBOX_SEND)
						.setParameter("mail_from",mailto)
						.setResultTransformer(Transformers.aliasToBean(MailVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println(rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return mailServiceVOList;
		}
		
		//mailTrash
		public List<MailBackupVO> mailTrash(String mto)throws Exception
		{
			System.out.println("Trash dao impl  "+mto);
			MailVO mailVO=null;
			List<MailBackupVO> mailBackupServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				mailBackupServiceVOList=session.createSQLQuery(TeacherConstant.GET_INBOX_TRASH)
						.setParameter("mail_to",mto)
						.setResultTransformer(Transformers.aliasToBean(MailBackupVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println(rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return mailBackupServiceVOList;
		}
		
		//getSubjectAssign
		public List<SubjectAssignVO> getSubjectAssign(String sclass,String sec)throws Exception
		{
			TeacherVO teacherVO=null;
			List<SubjectAssignVO> subjectAssignServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				subjectAssignServiceVOList=session.createSQLQuery(TeacherConstant.GET_TEACHER_SUBJECT_ASSIGN)
						.setParameter("sclass",sclass)
						.setParameter("section",sec)
						.setResultTransformer(Transformers.aliasToBean(SubjectAssignVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println(rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return subjectAssignServiceVOList;
		}
		//getSubjectAssignTeacher
		public List<SubjectAssignVO> getSubjectAssignTeacher(String sclass,String sec,String tid)throws Exception
		{
			System.out.println("class  "+sclass+"  sec  "+sec+"  tid  "+tid);
			SubjectAssignVO teacherVO=null;
			List<SubjectAssignVO> subjectAssignServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				subjectAssignServiceVOList=session.createSQLQuery(TeacherConstant.GET_SUBJECT_ASSIGN1)
						.setParameter("tid",tid)
						.setParameter("sclass",sclass)
						.setParameter("section",sec)
						.setResultTransformer(Transformers.aliasToBean(SubjectAssignVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println("Error *********************       "+rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return subjectAssignServiceVOList;
		}
		//getSubjectAssignTeacherID
		public List<SubjectAssignVO> getSubjectAssignTeacherID(String tid)throws Exception
		{
			//System.out.println("class  "+sclass+"  sec  "+sec+"  tid  "+tid);
			SubjectAssignVO teacherVO=null;
			List<SubjectAssignVO> subjectAssignServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				subjectAssignServiceVOList=session.createSQLQuery(TeacherConstant.GET_SUBJECT_ASSIGN1_ID)
						.setParameter("tid",tid)
						.setResultTransformer(Transformers.aliasToBean(SubjectAssignVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println("Error *********************       "+rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return subjectAssignServiceVOList;
		}
		
		//getSubjectAssignTeacherType
		public List<SubjectAssignVO> getSubjectAssignTeacherType(String ttype,String mobile)throws Exception
		{
			//System.out.println("class  "+sclass+"  sec  "+sec+"  tid  "+tid);
			SubjectAssignVO teacherVO=null;
			List<SubjectAssignVO> subjectAssignServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				subjectAssignServiceVOList=session.createSQLQuery(TeacherConstant.GET_SUBJECT_ASSIGN1_TYPE)
						.setParameter("ttype",ttype)
						.setParameter("mobile",mobile)
						.setResultTransformer(Transformers.aliasToBean(SubjectAssignVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println("Error *********************       "+rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return subjectAssignServiceVOList;
		}
		//getSubjectAssignTeacherClass
		public List<SubjectAssignVO> getSubjectAssignTeacherClass(String sclass,String ttype,String mobile)throws Exception
		{
			//System.out.println("class  "+sclass+"  sec  "+sec+"  tid  "+tid);
			SubjectAssignVO teacherVO=null;
			List<SubjectAssignVO> subjectAssignServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				subjectAssignServiceVOList=session.createSQLQuery(TeacherConstant.GET_SUBJECT_ASSIGN1_CLASS)
						.setParameter("sclass",sclass)
						.setParameter("ttype",ttype)
						.setParameter("mobile",mobile)
						.setResultTransformer(Transformers.aliasToBean(SubjectAssignVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println("Error *********************       "+rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return subjectAssignServiceVOList;
		}
		//getSubjectAssignTeacherClassSection
		public List<SubjectAssignVO> getSubjectAssignTeacherClassSection(String sclass,String section,String ttype,String mobile)throws Exception
		{
			//System.out.println("class  "+sclass+"  sec  "+sec+"  tid  "+tid);
			SubjectAssignVO teacherVO=null;
			List<SubjectAssignVO> subjectAssignServiceVOList=null;
			Session session=sessionFactory.openSession();
			try{
				subjectAssignServiceVOList=session.createSQLQuery(TeacherConstant.GET_SUBJECT_ASSIGN1_CLASS_SECTION)
						.setParameter("sclass",sclass)
						.setParameter("section",section)
						.setParameter("ttype",ttype)
						.setParameter("mobile",mobile)
						.setResultTransformer(Transformers.aliasToBean(SubjectAssignVO.class)).list();
				
			}catch(Exception rr)
			{
				System.out.println("Error *********************       "+rr);
			}finally {
				if (session != null) {
					session.close();
				}
			}	
			return subjectAssignServiceVOList;
		}
		
		//getSClassSection
		public List<TeacherVO> getSClassSection(String ttype,String mobile)throws Exception
		{
			TeacherVO teacherVO=null;
			List<TeacherVO> teacherServiceVOList=null;
			Session session=sessionFactory.openSession();
			
			System.out.println("enter dao impl  "+ttype+"  and  "+mobile);
			
			//if(ttype.equals("Teacher"))
			//{
				try{
					teacherServiceVOList=session.createSQLQuery(TeacherConstant.GET_TEACHER_TYPE)
						.setParameter("ttype",ttype)
						.setParameter("mobile",mobile)
						.setResultTransformer(Transformers.aliasToBean(TeacherVO.class)).list();
				
				}catch(Exception rr)
				{
					System.out.println(rr);
				}finally {
					if (session != null) {
						session.close();
					}
				}	
			//}
			return teacherServiceVOList;
		}
		
		//getSClassSection
				public List<TeacherVO> getSClassSectionAll(String ttype,String mobile)throws Exception
				{
					TeacherVO teacherVO=null;
					List<TeacherVO> teacherServiceVOList=null;
					Session session=sessionFactory.openSession();
					
					System.out.println("enter dao impl  "+ttype+"  and  "+mobile);
					
					
						try{
							teacherServiceVOList=session.createSQLQuery(TeacherConstant.GET_TEACHER_TYPE)
								.setParameter("ttype",ttype)
								.setParameter("mobile",mobile)
								.setResultTransformer(Transformers.aliasToBean(TeacherVO.class)).list();
						
						}catch(Exception rr)
						{
							System.out.println(rr);
						}finally {
							if (session != null) {
								session.close();
							}
						}	
					
					return teacherServiceVOList;
				}
		//getCheckin		
				public List<teacherAttendanceVO> getCheckin(String adate,String tid)throws Exception
				{
					TeacherVO teacherVO=null;
					List<teacherAttendanceVO> teacherAttendanceServiceVOList=null;
					Session session=sessionFactory.openSession();
					
					System.out.println("enter dao impl  "+adate+"  and  "+tid);
					
					
						try{
							teacherAttendanceServiceVOList=session.createSQLQuery(TeacherConstant.GET_TEACHER_ATTENDANCE)
								.setParameter("adate",adate)
								.setParameter("tid",tid)
								.setResultTransformer(Transformers.aliasToBean(teacherAttendanceVO.class)).list();
						
						}catch(Exception rr)
						{
							System.out.println(rr);
						}finally {
							if (session != null) {
								session.close();
							}
						}	
					
					return teacherAttendanceServiceVOList;
				}
		//getStaffCheckin		
				public List<staffAttendanceVO> getStaffCheckin(String adate,String tid)throws Exception
				{
					TeacherVO teacherVO=null;
					List<staffAttendanceVO> staffAttendanceServiceVOList=null;
					Session session=sessionFactory.openSession();
					
					System.out.println("enter dao impl  "+adate+"  and  "+tid);
					
					
						try{
							staffAttendanceServiceVOList=session.createSQLQuery(TeacherConstant.GET_STAFF_ATTENDANCE)
								.setParameter("adate",adate)
								.setParameter("tid",tid)
								.setResultTransformer(Transformers.aliasToBean(staffAttendanceVO.class)).list();
						
						}catch(Exception rr)
						{
							System.out.println(rr);
						}finally {
							if (session != null) {
								session.close();
							}
						}	
					
					return staffAttendanceServiceVOList;
				}
				
				
		//getStaffAttendance		
				public List<teacherAttendanceVO> getStaffAttendance(String fdate,String tdate)throws Exception
				{
					TeacherVO teacherVO=null;
					List<teacherAttendanceVO> teacherAttendanceServiceVOList=null;
					Session session=sessionFactory.openSession();
					
					System.out.println("enter dao impl  "+fdate+tdate);
					
					
						try{
							teacherAttendanceServiceVOList=session.createSQLQuery(TeacherConstant.GET_TEACHER_ATTENDANCE_HISTORY)
								.setParameter("fdate",fdate)
								.setParameter("tdate",tdate)
								//.setParameter("tid",tid)
								.setResultTransformer(Transformers.aliasToBean(teacherAttendanceVO.class)).list();
						
						}catch(Exception rr)
						{
							System.out.println(rr);
						}finally {
							if (session != null) {
								session.close();
							}
						}	
					
					return teacherAttendanceServiceVOList;
				}
		
		public TeacherVO getId(String uname) throws Exception
		{
			System.out.println("Enter DAOImpl");
			TeacherVO teacherVO=null;
			List<TeacherVO> productslist = null;
			Session session = sessionFactory.openSession();
			try {

				teacherVO = (TeacherVO) session.createSQLQuery(TeacherConstant.GET_ID)
						.setParameter("uname", uname)
						.setResultTransformer(Transformers.aliasToBean(TeacherVO.class)).uniqueResult();
				teacherVO.setTeacherServiceVOList(productslist);
			} catch (Exception e) {
				throw new Exception("Error ---------------->  "+e);
			} finally {
				if (session != null) {
					session.close();
				}
			}
			return teacherVO;
		}

		public List<TeacherVO> getLtrReferenceData() throws Exception {
			// TODO Auto-generated method stub
			return null;
		}
		
		//Show Records
				@SuppressWarnings("unchecked")	
				public List<GroupVO> getDLlist() throws Exception 
				{
					List<GroupVO> groupServiceVOList = null;
					
					Session session = sessionFactory.openSession();
					try {
						groupServiceVOList = session.createSQLQuery(TeacherConstant.GET_DL_LIST)
								.setResultTransformer(Transformers.aliasToBean(GroupVO.class)).list();
					} catch (Exception e) {
						System.out.println("Error in DAOIMPL "+e);
					}finally {
						if (session != null) {
							session.close();
						}
					}	
					return groupServiceVOList;
				}
				
				public List<GroupVO> getgroup(String groupname)throws Exception
				{
					System.out.println("daoimpl  groupmail  "+groupname);
					GroupVO groupVO=null;
					List<GroupVO> groupServiceVOList=null;
					Session session=sessionFactory.openSession();
					try{
						groupServiceVOList=session.createSQLQuery(TeacherConstant.GET_INBOX_GROUP)
								.setParameter("group_name",groupname)
								.setResultTransformer(Transformers.aliasToBean(GroupVO.class)).list();
						
					}catch(Exception rr)
					{
						System.out.println(rr);
					}finally {
						if (session != null) {
							session.close();
						}
					}	
					return groupServiceVOList;
				}
				
				@SuppressWarnings("unchecked")
				public  List<LoginVO> getlogindetail() throws Exception {
					// TODO Auto-generated method stub
					System.out.println("Enter DAOImpl  teacher id ");
					
					List<LoginVO> loginServiceVOList = null;
					Session session = sessionFactory.openSession();
					try {
						loginServiceVOList = session.createSQLQuery(TeacherConstant.GET_TEID)
								
								.setResultTransformer(Transformers.aliasToBean(LoginVO.class)).list();
					} catch (Exception e) {
						System.out.println("Error in DAOIMPL "+e);
					}
					
					
					return loginServiceVOList;
				}
				
				@SuppressWarnings("unchecked")
				public  List<LoginVO> getlogindetailAll() throws Exception {
					// TODO Auto-generated method stub
					System.out.println("Enter DAOImpl  teacher id ");
					
					List<LoginVO> loginServiceVOList = null;
					Session session = sessionFactory.openSession();
					try {
						loginServiceVOList = session.createSQLQuery(TeacherConstant.GET_All)
								
								.setResultTransformer(Transformers.aliasToBean(LoginVO.class)).list();
					} catch (Exception e) {
						System.out.println("Error in DAOIMPL "+e);
					}
					
					
					return loginServiceVOList;
				}

	
}
