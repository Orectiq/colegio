package com.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dao.AddActivityDao;
import com.dao.constant.AddActivityConstant;
import com.model.AddActivityVO;

@Repository
public class AddActivityDaoImpl implements AddActivityDao 
{
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
	@Autowired
	DataSource dataSource;


	//@Override
	public AddActivityVO insertAddActivityDetails(com.model.AddActivityVO addactivityVO) throws Exception 
	{
		// TODO Auto-generated method stub
		System.out.println("Enter DAO IMPL Activity");
		
		AddActivityVO AddActivityvo = new AddActivityVO();
		Session session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
	
		
		
		try {
			 
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			
			String sql =AddActivityConstant.INSERT_AddActivity;
			
					  jdbcTemplate.update(sql,new Object[] {
							  addactivityVO.getADate(),addactivityVO.getTid(),addactivityVO.getTname(),addactivityVO.getTtype(),addactivityVO.getAclass(),addactivityVO.getAsec(),
							  addactivityVO.getPlace(),addactivityVO.getBrief_Description(),
							  addactivityVO.getDescription1(),addactivityVO.getDescription2(),addactivityVO.getStudents_Involved(),addactivityVO.getUpload_Photo(),
							  addactivityVO.getUpload_Document(),addactivityVO.getWidget(),addactivityVO.getIsActive(),addactivityVO.getInserted_By(),addactivityVO.getInserted_Date(),
							  addactivityVO.getUpdated_By(),addactivityVO.getUpdated_Date()});
			
			System.out.println("Success insert................");
			trx.commit();
					
		}catch (Exception e) {
			trx.rollback();
			System.out.println("Error ------------>"+e);
			throw new Exception(e);
		}
		
		return AddActivityvo;
		
	}

	
	
	@SuppressWarnings("unchecked")	
	public List<AddActivityVO> getLtrReferenceData() throws Exception 
	{
		List<AddActivityVO> addactivityServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		try {
			addactivityServiceVOList = session.createSQLQuery(AddActivityConstant.GET_AddActivity_LIST_ALL)
					.setResultTransformer(Transformers.aliasToBean(AddActivityVO.class)).list();
		} catch (Exception e) {
			System.out.println("Error in DAOIMPL "+e);
		}
		return addactivityServiceVOList;
	} 
	
	//getClassActivity(String aclass,String asec,String ttype)
	@SuppressWarnings("unchecked")	
	public List<AddActivityVO> getClassActivity(String aclass,String asec,String ttype) throws Exception 
	{
		List<AddActivityVO> addactivityServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		
		
		//if(ttype.equals("Teacher"))
		//{
			try {
				addactivityServiceVOList = session.createSQLQuery(AddActivityConstant.GET_AddActivity_LIST_TEACHER)
					.setParameter("aclass", aclass)
					.setParameter("asec", asec)
					.setResultTransformer(Transformers.aliasToBean(AddActivityVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		//}	
		return addactivityServiceVOList;
	} 

	//getActivitybyType
	@SuppressWarnings("unchecked")	
	public List<AddActivityVO> getActivitybyType(String aclass,String asec,String place) throws Exception 
	{
		List<AddActivityVO> addactivityServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		System.out.println("place &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&   "+place+" class  "+aclass+"  sec  "+asec);
		
		//if(ttype.equals("Teacher"))
		//{
			try {
				addactivityServiceVOList = session.createSQLQuery(AddActivityConstant.GET_AddActivity_LIST_TEACHER_TYPE)
					.setParameter("aclass", aclass)
					.setParameter("asec", asec)
					.setParameter("Place", place)
					.setResultTransformer(Transformers.aliasToBean(AddActivityVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		//}	
		return addactivityServiceVOList;
	} 
	
	@SuppressWarnings("unchecked")	
	public List<AddActivityVO> getActivitybyType2(String aclass,String asec,String place) throws Exception 
	{
		List<AddActivityVO> addactivityServiceVOList = null;
		
		Session session = sessionFactory.openSession();
		
		
		//if(ttype.equals("Teacher"))
		//{
			try {
				addactivityServiceVOList = session.createSQLQuery(AddActivityConstant.GET_AddActivity_LIST_TEACHER_TYPE2)
					.setParameter("aclass", aclass)
					.setParameter("asec", asec)
					.setParameter("Place", place)
					.setResultTransformer(Transformers.aliasToBean(AddActivityVO.class)).list();
			} catch (Exception e) {
				System.out.println("Error in DAOIMPL "+e);
			}
		//}	
		return addactivityServiceVOList;
	}
	
}


	