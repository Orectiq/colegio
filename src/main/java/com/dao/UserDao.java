package com.dao;

import java.util.List;

import com.common.MasVO;
import com.common.TeachVO;
import com.common.UserVO;
import com.model.CounterVO;
import com.model.CountryVO;
import com.model.LibrarianVO;
import com.model.LoginVO;
import com.model.MailSettingVO;
import com.model.MasterVO;
import com.model.ParentVO;
import com.model.SettingadminVO;
import com.model.StuVO;
import com.model.TeacherVO;
import com.model.TransportVO;


public interface UserDao 
{

	public MasVO isUserNameExist(String userId,String password) throws Exception;
	//isCommonUserExist
	public LoginVO isCommonUserExist(String userId,String password) throws Exception;
	
	public TeacherVO isStaffNameExist(String userId,String password) throws Exception;
	//isTransportExist
	public TransportVO isTransportExist(String userId,String password) throws Exception;
	
	public StuVO isSTUidExist(String userId,String password) throws Exception;
	//isLIBidExist
	public LibrarianVO isLIBidExist(String userId,String password) throws Exception;
	public ParentVO isPARidExist(String userId,String password) throws Exception;
	public MasterVO insertMasterDetails(MasterVO masterVO) throws Exception;
	//updateAdminPhoto
	public MasterVO updateAdminPhoto(MasterVO masterVO) throws Exception;
	//photoUpdate
	public LoginVO photoUpdate(LoginVO loginVO) throws Exception;
	//updateMobilePassword
	public LoginVO updateMobilePassword(LoginVO loginVO) throws Exception;
	//updateAdminGlobalSetting
	public LoginVO updateAdminGlobalSetting(LoginVO loginVO) throws Exception;
	//updateMailPassword
	public LoginVO updateMailPassword(LoginVO loginVO) throws Exception;
	//insertMailSetting
	public MailSettingVO insertMailSetting(MailSettingVO loginVO) throws Exception;
	//getMailSetting
	public List<MailSettingVO> getMailSetting()throws Exception;
	public List<MailSettingVO> getMailSettingType(String temvar)throws Exception;
	
	//insertTerms
	public MailSettingVO insertTerms(MailSettingVO loginVO) throws Exception;
	//getTerms
	public List<MailSettingVO> getTerms()throws Exception;
	//loadCountry
	public List<CountryVO> loadCountry()throws Exception;
	//getIDSetting
	public List<CounterVO> getIDSetting(String prifix)throws Exception;
	//getIDSettingAll
	public List<CounterVO> getIDSettingAll()throws Exception;
	//updateIDSetting
		public CounterVO updateIDSetting(CounterVO counterVO) throws Exception;
		
		
	//getLoginDetails
		public List<LoginVO> getLoginDetails(String sid,String pass)throws Exception;
	//getLoginMobile
		public List<LoginVO> getLoginEmail(String sid)throws Exception;
	//getLoginEmail
		public List<LoginVO> getLoginMobile(String sid)throws Exception;
	//getAdminSettingDetails	
		public List<SettingadminVO> getAdminSettingDetails(String sid)throws Exception;
	//updatePassword
	public MasterVO updatePassword(MasterVO masterVO) throws Exception;
	
	public List<MasterVO> getMasterDetail(String uname)throws Exception;
}
