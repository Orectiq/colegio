package com.dao;

import com.model.SchoolInfoVO;

public interface SuperAdminDAO 
{

	public boolean registerSchool(SchoolInfoVO schoolInfoVO);
}
