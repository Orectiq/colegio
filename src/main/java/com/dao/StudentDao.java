package com.dao;

import java.util.List;

import com.common.ExamTypeMasterVO;
import com.common.MasVO;
import com.common.StudentVO;
import com.model.AssignmentVO;
import com.model.AttendanceDayVO;
import com.model.AttendanceVO;
import com.model.CounterVO;
import com.model.DummyVO;
import com.model.LoginVO;
import com.model.MarksVO;
import com.model.MedicalVO;
import com.model.ParentVO;
import com.model.StuMedicalVO;
import com.model.StuParVO;
import com.model.StuVO;
import com.model.TimeTableVO;


public interface StudentDao 
{
	
	public  List<Object> getNewsid() throws Exception ;
	public List<Object> getEventsId() throws Exception;
	public  List<Object> getStuId() throws Exception ;
	public List<Object> getAssignId() throws Exception;
	public  List<Object> getReqId() throws Exception ;
	
	
	public  List<Object> getParId() throws Exception ;
	public  List<Object> getAdmnId() throws Exception ;
	public  List<Object> getAdmnId1() throws Exception ;
	public  List<Object> getStuRoll() throws Exception ;
	//getChallanID
	public  List<Object> getChallanID() throws Exception ;
	
	public StuVO insertStudentDetails(StuVO stuVO) throws Exception;
	public ParentVO insertParentDetails(ParentVO parentVO) throws Exception;
	public MedicalVO insertMedicalDetails(MedicalVO medicalVO) throws Exception;
	public StuVO insertFinalDetails(StuVO stuVO) throws Exception;
	//updateCounter
	public CounterVO updateCounter(CounterVO counterVO) throws Exception;
	//insertExamTypeMaster
	public ExamTypeMasterVO insertExamTypeMaster(ExamTypeMasterVO examtypeVO)throws Exception;
	
	//updateRoomAllot
	public StuVO updateRoomAllot(StuVO stuVO) throws Exception;
	//updateRollNumber
	public StuVO updateRollNumber(StuVO stuVO) throws Exception;
	
	//insertAttendDetails
	public AttendanceVO insertAttendDetails(AttendanceVO attendVO) throws Exception;
	//insertMarks
	public MarksVO insertMarks(MarksVO marksVO) throws Exception;
	
	public AttendanceVO checkAttendance(AttendanceVO attendVO) throws Exception;
	
	public StuVO deleteStudentsDetails(StuVO stuVO) throws Exception;
	
	public List<StuVO> getLtrReferenceData()throws Exception;//(String studid)throws Exception;
	//getTransportList
	public List<StuVO> getTransportList()throws Exception;//(String studid)throws Exception;
	
	public List<StuMedicalVO> getLtrReferenceDataAll(String student_id)throws Exception;
	public List<MedicalVO> getMedical(String studid)throws Exception;
	//getStudentLoginID
	public List<StuVO> getStudentLoginID(String email)throws Exception;
	//getParentLoginID
	public List<ParentVO> getParentLoginID(String mobile)throws Exception;
	
	public List<StuVO> getParentDataAll(String studid)throws Exception;
	//getParentid
	public List<StuVO> getParentid(String mobile)throws Exception;
	
	public List<StuParVO> getStuParDataAll(String parent_id)throws Exception;
	//parentList
	public List<ParentVO> parentList(String parent_id)throws Exception;
	public List<StuVO> getStudentClassAll(String sclass,String sec)throws Exception;
	//totalStudent
	public List<StuVO> totalStudent(String sclass,String sec)throws Exception;
	
	
	//getStuAttendance
	public List<AttendanceVO> getStuAttendance(String fdate,String tdate,String atime,String sid)throws Exception;
	//getStuAttendanceDayAll
	public List<AttendanceDayVO> getStuAttendanceDayAll(String fdate,String monyear,String sclass,String sec)throws Exception;
	//getTeacherAttendanceDayAll
	public List<AttendanceDayVO> getTeacherAttendanceDayAll(String fdate,String monyear)throws Exception;
	
	//countAttendanceMonth
	public List<AttendanceVO> countAttendanceMonth(String sclass,String sec,String amonth)throws Exception;
	//showCount
	public List<DummyVO> showCount()throws Exception;
	
	//getStuAttendanceAll
	public List<AttendanceVO> getStuAttendanceAll(String fdate,String aclass,String asec)throws Exception;
	//getStuAttendanceByDate
	public List<AttendanceVO> getStuAttendanceByDate(String fdate,String aclass,String asec)throws Exception;
	//getStuAttendanceByDateAbsent
	public List<AttendanceVO> getStuAttendanceByDateAbsent(String fdate,String aclass,String asec,String ttype)throws Exception;
	//getStuAttendanceByIDAbsent
	public List<AttendanceVO> getStuAttendanceByIDAbsent(String adate,String sid)throws Exception;
	
	//countAttendanceToday
	public int countAttendanceToday(String fdate,String aclass,String asec)throws Exception;
	//countTotalStudentsToday
	public int countTotalStudentsToday(String fdate,String aclass,String asec)throws Exception;
	//topperClass
	public int topperClass(String fdate,String aclass,String asec)throws Exception;
	//topperClassName
	public List<MarksVO> topperClassName(String fdate,String aclass,String asec)throws Exception;
	
	public List<StuParVO> getParentClassAll(String sclass,String sec)throws Exception;
	//getAttendance
	public List<StuVO> getAttendance(String sclass,String sec)throws Exception;
	//getLoginDetails
	public List<LoginVO> getLoginDetails(String sid,String pass)throws Exception;
	
	//getHostelList
	public List<StuVO> getHostelList(String gender)throws Exception;
	//getTransStudent
	public List<StuVO> getTransStudent(String board)throws Exception;
	//getDorStudent
	public List<StuVO> getDorStudent(String board)throws Exception;
	
	public StuVO getEditStudentsData(String studid)throws Exception;
	
	public StuVO editStudentDetails(StuVO stuVO) throws Exception;
	
public List<StuVO> getAssignment(String sclass,String sec)throws Exception;
	//getStuNameLike
	public List<StuVO> getStuNameLike(String sclass,String sec,String fname)throws Exception;
//getPhoto	
	public List<LoginVO> getPhoto(String uid,String pass)throws Exception;
	
	public AssignmentVO insertAssignmentDetails(AssignmentVO assignmentVO) throws Exception;
	
	//msgCount
	public int msgCount(String mfrom)throws Exception;
	
	//countStudents
		public int countStudents()throws Exception;
	//countPresent	
		public int countPresent()throws Exception;
	//countAbsent	
		public int countAbsent()throws Exception;
		
	//count Total,Present,Absent list based on class,section and date	
		//countStudentsClass
				public int countStudentsClass(String sclass,String sec)throws Exception;
		//countPresentClass	
				public int countPresentClass(String sclass,String sec,String adate)throws Exception;
		//countAbsentClass	
				public int countAbsentClass(String sclass,String sec,String adate)throws Exception;
		//countAttTeacherMonthPresent		
				public int countAttTeacherMonthPresent(String adate)throws Exception;
				public int countAttTeacherMonthAbsent(String adate)throws Exception;
				
		//countPresentMonth	
				public int countPresentMonth(String sclass,String sec,String adate)throws Exception;
		//countAbsentMonth	
				public int countAbsentMonth(String sclass,String sec,String adate)throws Exception;		
				
		//UserReport
				public int countParents()throws Exception;
				public int countTeachers()throws Exception;
				public int countPresentTeacher()throws Exception;
				public int countAbsentTeacher()throws Exception;
				
				
				public int countLibrary()throws Exception;
				public int countDormitory()throws Exception;
				public int countTransport()throws Exception;
				public int countTransportAO()throws Exception;
				
				//getStudentid
				public List<StuVO> getstuid()throws Exception;
				//getTimeTable
				public List<TimeTableVO> getTimeTable(String sclass,String sec)throws Exception;
				//getTimeTableDay
				public List<TimeTableVO> getTimeTableDay(TimeTableVO timetableVO)throws Exception;
				//getTimePeriod
				public List<TimeTableVO> getTimePeriod()throws Exception;
				//TimeTableInsert
				public TimeTableVO TimeTableInsert(TimeTableVO timetableVO) throws Exception;
				public List<StuVO> getstudentdata(String stuid)throws Exception;
}
