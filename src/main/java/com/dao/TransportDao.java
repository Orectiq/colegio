package com.dao;

import java.util.List;

import com.model.TransportAttendanceVO;
import com.model.TransportVO;

public interface TransportDao {
	public TransportVO insertTransportDetails(TransportVO transportVO) throws Exception;
	//insertAttendance
	public TransportAttendanceVO insertAttendance(TransportAttendanceVO attendanceVO)throws Exception;
	
	public List<TransportVO> gettransportdetail()throws Exception;
	
	public List<TransportVO> gettransportdetailall(String transport_name)throws Exception;
	//getDname
	public List<TransportVO> getDname(String dname)throws Exception;
	//getTransportLoginID
	public List<TransportVO> getTransportLoginID(String mobile)throws Exception;
	
	//getAttendanceHistory
	public List<TransportAttendanceVO> getAttendanceHistory(String uname,String adate)throws Exception;
	
	//getAttendanceHistoryAbsent
	public List<TransportAttendanceVO> getAttendanceHistoryAbsent(String uname,String adate)throws Exception;
	
	//getTransStudentVOList
	public List<TransportAttendanceVO> getTransStudentVOList(String board,String adate)throws Exception;
	//getTransStuListBYBoard
	public List<TransportAttendanceVO> getTransStuListBYBoard(String board,String adate)throws Exception;
	
	//getTransStudentVOListNew
	public List<TransportAttendanceVO> getTransStudentVOListNew(String board)throws Exception;
	public List<TransportAttendanceVO> gettrandetail(String duser)throws Exception;
	
}
