package com.dao;

import java.util.List;

import com.model.NonteachVO;
import com.model.staffAttendanceVO;



public interface NonteachDao {
	
	
	
	public  List<Object> getLibID(String ref) throws Exception ;
	public  List<Object> getDorID(String ref) throws Exception ;
	public  List<Object> getTraID(String ref) throws Exception ;
	
	public NonteachVO insertNonteachDetails(NonteachVO nonteachVO,String path1) throws Exception;
    //sendMail
	public NonteachVO sendMail(NonteachVO nonteachVO) throws Exception;
	
	public List<NonteachVO> getLtrReferenceData()throws Exception;
	
	public List<NonteachVO> getLtrReferenceDataall()throws Exception;
	
	public List<NonteachVO> getNonteach(String department)throws Exception;
	//getNonTeachingLoginID
	public List<NonteachVO> getNonTeachingLoginID(String mobile)throws Exception;
	
	//getNonTeachIDWithPhone
	public List<NonteachVO> getNonTeachIDWithPhone(String mobile)throws Exception;
	
	//getNonTeachAttendance
	public List<staffAttendanceVO> getNonTeachAttendance(String fdate,String tdate,String tid)throws Exception;
}
