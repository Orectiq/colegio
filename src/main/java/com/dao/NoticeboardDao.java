package com.dao;

import java.util.List;

import com.model.Nb_newsVO;
import com.model.NoticeboardVO;

public interface NoticeboardDao {
	public NoticeboardVO insertEventDetails(NoticeboardVO noticeboardVO) throws Exception;
	public Nb_newsVO insertNewsDetails(Nb_newsVO nb_newsVO) throws Exception;
	
	public List<NoticeboardVO>geteventdetail(String doe,String ltype)throws Exception;
	public List<NoticeboardVO>getevent(String ltype)throws Exception;
	
	public List<Nb_newsVO>getnewsdetail(String dop)throws Exception;
	
	public List<NoticeboardVO>geteventdetailAll()throws Exception;
	public List<Nb_newsVO>getnewsdetailAll()throws Exception;
	
	//geteventdetailNext
	public List<NoticeboardVO>geteventdetailNext()throws Exception;
	//geteventdetailType
	public List<NoticeboardVO>geteventdetailType(String ltype,String doe)throws Exception;
	
	//getupcommingevents
		public List<NoticeboardVO>getupcommingevents(String ltype)throws Exception;
		
	
	
	//getTeachernewsdetail
	public List<Nb_newsVO> getTeachernewsdetail(String ttype)throws Exception;
	
	//getTeachernewsdetaildate
		public List<Nb_newsVO> getTeachernewsdetaildate(String dop,String ttype)throws Exception;
		
	//geteventdetail_title
	public List<NoticeboardVO> geteventdetail_title(String title)throws Exception;
	
	//getnewsdetail_title
	public List<Nb_newsVO> getnewsdetail_title(String title)throws Exception;
}
