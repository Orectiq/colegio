package com.dao;

import java.util.List;

import com.model.MediaVO;
import com.model.MediaaddVO;

public interface MediaDao {

	
	public MediaVO insertAlbumDetails(MediaVO mediaVO) throws Exception;
	public MediaaddVO insertMediaall(MediaaddVO mediaaddVO) throws Exception;
	public List<MediaaddVO> getMediaData()throws Exception;
	public List<MediaVO> getAlbumData()throws Exception;	
}
