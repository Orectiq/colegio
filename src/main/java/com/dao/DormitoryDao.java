package com.dao;

import java.util.List;

import com.model.DormitoryAllotVO;
import com.model.DormitoryVO;
import com.model.StuVO;

public interface DormitoryDao 
{
	public DormitoryVO insertDormitoryDetails(DormitoryVO dormitoryVO)throws Exception;
	//updateRoomAllot
	public DormitoryVO updateRoomAllot(DormitoryVO dormitoryVO)throws Exception;
	
	
	//insertRoomAllot
	public DormitoryAllotVO insertRoomAllot(DormitoryAllotVO dormitoryVO)throws Exception;
	
	public List<DormitoryVO> getLtrReferenceData()throws Exception;
	//getHostelList
		public List<DormitoryVO> getHostelList(String gender)throws Exception;
	//getNonDormitoryStudent
		public List<StuVO> getNonDormitoryStudent()throws Exception;
		
	//getDormitoryList
		public List<DormitoryVO> getDormitoryList(String dname)throws Exception;
	//getDormitoryAllotList	
		public List<DormitoryAllotVO> getDormitoryAllotList(String dname)throws Exception;
	//editHostel
		public StuVO editHostel(StuVO stuvo)throws Exception;
}
