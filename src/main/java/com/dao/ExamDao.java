package com.dao;

import java.util.List;

import com.common.ExamTypeMasterVO;
import com.model.ExamNewVO;
import com.model.ExamStudentVO;
import com.model.ExamVO;
import com.model.MarksEntryVO;
import com.model.MarksTempVO;
import com.model.MarksdumpVO;
import com.model.MarksentryallVO;
import com.model.StuParVO;

public interface ExamDao {
	public ExamNewVO insertExamDetails(ExamNewVO examVO) throws Exception;
	
	public List<ExamVO> getLtrReferenceData()throws Exception;
	//getExamData
	public List<ExamNewVO> getExamData()throws Exception;
	
	public ExamVO getLtrReferenceData100()throws Exception;
	
	public List<ExamVO> getLtrReferenceDataall()throws Exception;
	public List<ExamNewVO> getExam(String sclass,String sec,String etype)throws Exception;
	//getExamClass
	public List<ExamNewVO> getExamClass(String sclass,String sec)throws Exception;
	
	//getClassMarks
	public List<MarksentryallVO> getClassMarks(String sclass,String sec,String etype,String eid)throws Exception;
	//MarksentryallVO getClassSubject
	public List<MarksentryallVO> getClassSubject(String sclass,String sec)throws Exception;
	//getClassStudents
	public List<MarksentryallVO> getClassStudents(String sclass,String sec)throws Exception;
	//getClassExamid
	public List<MarksdumpVO> getClassExamid(/*String eid,String sclass,String sec*/)throws Exception;
	//getMarksTemp
	public List<MarksTempVO> getMarksTemp(String eid,String sclass,String[] myArray)throws Exception;
	
	
	//getMarksFinal
	public List<MarksdumpVO> getMarksFinal()throws Exception;
	
	//getClassStudentsMarks
	public List<MarksentryallVO> getClassStudentsMarks(String eid,String sid,String subj)throws Exception;
	
	//getClassMarksStudents
	public List<MarksEntryVO> getClassMarksStudents(String sclass,String sec,String eid,String sid)throws Exception;
	
	//getExamType
	public List<ExamTypeMasterVO> getExamType()throws Exception;
	
	//getStudentMarks
	public List<MarksEntryVO> getStudentMarks(String sid)throws Exception;
	

	//getExamID
	public List<ExamNewVO> getExamID(String eid)throws Exception;
	//getExamID_Subject
	public List<ExamNewVO> getExamID_Subject(String eid,String subj)throws Exception;
	
	//getExamDetails
	public List<ExamStudentVO> getExamDetails(String sid)throws Exception;
}
