package com.dao;

import java.util.List;

import com.model.AbsenceRequestVO;
import com.model.ClassSectionVO;
import com.model.GroupVO;
import com.model.LoginVO;
import com.model.MailBackupVO;
import com.model.MailVO;
import com.model.ParentVO;
import com.model.StuVO;
import com.model.SubjectAssignVO;
import com.model.TeacherVO;
import com.model.staffAttendanceVO;
import com.model.teacherAttendanceVO;

public interface TeacherDao
{
	public  List<Object> getTeacId(String ref) throws Exception ;
	
	
	public TeacherVO insertTeacherDetails(TeacherVO teacherVO) throws Exception;
	//subjectAssignInsert
	public SubjectAssignVO subjectAssignInsert(SubjectAssignVO subjectAssignVO) throws Exception;
	
	//teacherAttenInsert
	public teacherAttendanceVO teacherAttenInsert(teacherAttendanceVO teacherVO) throws Exception;
	//teacherAttenUpdate
	public teacherAttendanceVO teacherAttenUpdate(teacherAttendanceVO teacherVO) throws Exception;
	
	
	//staffAttenInsert
		public staffAttendanceVO staffAttenInsert(staffAttendanceVO teacherVO) throws Exception;
	//composeMail	
		public MailVO composeMail(MailVO mailVO) throws Exception;
	//groupMail
		public GroupVO groupMail(GroupVO groupVO) throws Exception;
		
	//deleteMail	
		public MailVO deleteMail(MailVO mailVO) throws Exception;
		
		//staffAttenUpdate
		public staffAttendanceVO staffAttenUpdate(staffAttendanceVO teacherVO) throws Exception;
	
	
	
	public List<TeacherVO> getLtrReferenceDataAll(String studid)throws Exception;
	//getClassSectionAll
	public List<ClassSectionVO> getClassSectionAll(String studid)throws Exception;
	
	public List<TeacherVO> getTeacherSubject(String sclass,String sec)throws Exception;
	//mailInbox
	public List<MailVO> mailInbox(String mailto)throws Exception;
	//getGroupMail
	public List<GroupVO> getGroupMail(String gname)throws Exception;
	
	//mailInboxDate
	public List<MailVO> mailInboxDate(String mailto,String mdate)throws Exception;
	//mailInboxDateTime
	public List<MailVO> mailInboxDateTime(String mailto,String mdate,String mtime)throws Exception;
	//mailSend
	public List<MailVO> mailSend(String mailto)throws Exception;
	//mailTrash
	public List<MailBackupVO> mailTrash(String mto)throws Exception;
	
	
	//getSubjectAssign
	public List<SubjectAssignVO> getSubjectAssign(String sclass,String sec)throws Exception;
	//getSubjectAssignTeacher
	public List<SubjectAssignVO> getSubjectAssignTeacher(String sclass,String sec,String tid)throws Exception;
	//getSubjectAssignTeacherID
	public List<SubjectAssignVO> getSubjectAssignTeacherID(String tid)throws Exception;
	//getSubjectAssignTeacherType
	public List<SubjectAssignVO> getSubjectAssignTeacherType(String ttype,String mobile)throws Exception;
	//getSubjectAssignTeacherClass
	public List<SubjectAssignVO> getSubjectAssignTeacherClass(String sclass,String ttype,String mobile)throws Exception;
	//getSubjectAssignTeacherClassSection
	public List<SubjectAssignVO> getSubjectAssignTeacherClassSection(String sclass,String section,String ttype,String mobile)throws Exception;
	
	//getSClassSection
	public List<TeacherVO> getSClassSection(String ttype,String mobile)throws Exception;
	public List<TeacherVO> getSClassSectionAll(String ttype,String mobile)throws Exception;
	//getCheckin
	public List<teacherAttendanceVO> getCheckin(String adate,String tid)throws Exception;
	//getStaffCheckin
	public List<staffAttendanceVO> getStaffCheckin(String adate,String tid)throws Exception;
	
	//getStaffAttendance
	public List<teacherAttendanceVO> getStaffAttendance(String fdate,String tdate)throws Exception;
	
	public TeacherVO getId(String uname)throws Exception;


	public List<TeacherVO> getltrreferencedata()throws Exception;
	public List<GroupVO> getDLlist()throws Exception;
	//getGroupMail
		public List<GroupVO> getgroup(String groupname)throws Exception;
		public List<LoginVO> getlogindetail()throws Exception;	
		public List<LoginVO> getlogindetailAll()throws Exception;	
	
}
