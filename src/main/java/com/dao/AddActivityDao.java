package com.dao;

import com.model.AddActivityVO;

import java.util.List;



public interface AddActivityDao {

	public AddActivityVO insertAddActivityDetails(AddActivityVO addactivityVO)throws Exception;

	public List<AddActivityVO> getLtrReferenceData()throws Exception;
	
	public List<AddActivityVO> getClassActivity(String aclass,String asec,String ttype)throws Exception;
	//getActivitybyType
	public List<AddActivityVO> getActivitybyType(String aclass,String asec,String place)throws Exception;
	public List<AddActivityVO> getActivitybyType2(String aclass,String asec,String place)throws Exception;



	

}
