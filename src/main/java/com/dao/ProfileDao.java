package com.dao;

import com.model.LoginVO;

public interface ProfileDao
{
	public LoginVO updateParentsPassword(LoginVO loginVO) throws Exception;
	
	//photoUpdate
		public LoginVO photoUpdate(LoginVO loginVO) throws Exception;
		//updateMobilePassword
		public LoginVO updateMobilePassword(LoginVO loginVO) throws Exception;
}
