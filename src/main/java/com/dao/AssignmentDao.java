package com.dao;

import java.util.List;

import com.model.AssignApprovalVO;
import com.model.AssignmentaddVO;
//import com.model.SubjectVO;



public interface AssignmentDao {

	public AssignmentaddVO insertAssignmentDetails(AssignmentaddVO assignmentaddVO) throws Exception;
	//insertAssignApproval
	public AssignApprovalVO insertAssignApproval(AssignApprovalVO assignappVO) throws Exception;
	//updateAssignApproval
	public AssignApprovalVO updateAssignApproval(AssignApprovalVO assignappVO) throws Exception;

	public List<AssignmentaddVO> getAssignment(String sclass, String section)throws Exception;
	//getSubject
	public List<AssignApprovalVO> getSubject(String tid)throws Exception;
	////getAssignApprovalTeacher(String adate,String tid,String sclass,String sec,String subj,String sta)
	public List<AssignApprovalVO> getAssignApprovalTeacher(String adate,String tid,String sclass,String sec,String subj,String sta)throws Exception;
	//getApprovalTType
	public List<AssignApprovalVO> getApprovalTType(String ttype,String tid)throws Exception;
	//getApprovalList(String sclass,String section,String tid,String title,String ttype)
	public List<AssignApprovalVO> getApprovalList(String sclass,String section,String tid,String title,String ttype)throws Exception;
	//getAdminApprovalList
	public List<AssignApprovalVO> getAdminApprovalList(String sclass,String section,String subject)throws Exception;
	
	//getAssignmentTeacher
	public List<AssignmentaddVO> getAssignmentTeacher(String sclass, String section,String tid,String adate,String ttype)throws Exception;
	//getAssignmentTeacherAllot
	public List<AssignmentaddVO> getAssignmentTeacherAllot(String adi)throws Exception;
	
	//getTitle  String hdate,String subj
	public List<AssignmentaddVO> getTitle(String hdate,String subj)throws Exception;
	public List<AssignmentaddVO> getTType(String ttype,String tid)throws Exception;
}
