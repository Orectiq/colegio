package com.model;
import java.util.Date;
import java.util.List;
public class StuParVO 
{
	//ParentVO
	private String parent_id=null;
	private String prt_fname  = null;
	private String prt_mname = null;
	private String prt_father_dob = null;
	private String prt_mother_dob = null;
	private String prt_occupa = null;
	private String prt_design = null;
	private String prt_off_add = null;
	private String prt_off_phn = null;
	private String prt_mobile = null;
	private String prt_email = null;
	private String prt_photo = null;
	private String prt_gname = null;
	private String prt_grd_occupa = null;
	private String prt_grd_design = null;
	private String prt_grd_off_add = null;
	private String prt_grd_home_add = null;
	private String prt_grd_mobile = null;
	private String prt_grd_email = null;
	private String prt_grd_relation = null;
	private String isActive = null;
	private String inserted_by = null;
	private Date inserted_date = null;
	private String updated_by = null;
	private Date updated_date = null;
	
	//StudentVO
	private String student_id=null;
	private String firstname = null;
	private String lastname = null;
	private String sphoto = null;
	private String sdob = null;
	private String bplace=null;
	private String nationality=null;
	private String mtongue=null;
	private String cgender=null;
	private String religion=null;
	private String category=null;
	private String address1=null;
	private String address2=null;
	private String state=null;
	private int pin=0;
	private String mobile=null;
	private String phone=null;
	private String cemail=null;
	private String sch_transport=null;
	private String pre_school=null;
	private String prd_from=null;
	private String prd_to=null;
	private String role=null;
	
	
	
	//private String parent_id=null;
	private String adm_id=null;
	private String student_roll=null;
	private Date Join_date=null;
	private String Hostel_name=null;
	private String Hostel_Room_no=null;
	private String photo=null;
	private String School_Name=null;
	private String Academic_year=null;
	private String sclass=null;
	private String section=null;
	private List<StuParVO> studentDetailList=null;
	private List<StuParVO> studentServiceVOList = null;
	private String fname = null;
	private String lname = null;
	private String dob = null;
	private String parentid = null;
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	public String getPrt_fname() {
		return prt_fname;
	}
	public void setPrt_fname(String prt_fname) {
		this.prt_fname = prt_fname;
	}
	public String getPrt_mname() {
		return prt_mname;
	}
	public void setPrt_mname(String prt_mname) {
		this.prt_mname = prt_mname;
	}
	public String getPrt_father_dob() {
		return prt_father_dob;
	}
	public void setPrt_father_dob(String prt_father_dob) {
		this.prt_father_dob = prt_father_dob;
	}
	public String getPrt_mother_dob() {
		return prt_mother_dob;
	}
	public void setPrt_mother_dob(String prt_mother_dob) {
		this.prt_mother_dob = prt_mother_dob;
	}
	public String getPrt_occupa() {
		return prt_occupa;
	}
	public void setPrt_occupa(String prt_occupa) {
		this.prt_occupa = prt_occupa;
	}
	public String getPrt_design() {
		return prt_design;
	}
	public void setPrt_design(String prt_design) {
		this.prt_design = prt_design;
	}
	public String getPrt_off_add() {
		return prt_off_add;
	}
	public void setPrt_off_add(String prt_off_add) {
		this.prt_off_add = prt_off_add;
	}
	public String getPrt_off_phn() {
		return prt_off_phn;
	}
	public void setPrt_off_phn(String prt_off_phn) {
		this.prt_off_phn = prt_off_phn;
	}
	public String getPrt_mobile() {
		return prt_mobile;
	}
	public void setPrt_mobile(String prt_mobile) {
		this.prt_mobile = prt_mobile;
	}
	public String getPrt_email() {
		return prt_email;
	}
	public void setPrt_email(String prt_email) {
		this.prt_email = prt_email;
	}
	public String getPrt_photo() {
		return prt_photo;
	}
	public void setPrt_photo(String prt_photo) {
		this.prt_photo = prt_photo;
	}
	public String getPrt_gname() {
		return prt_gname;
	}
	public void setPrt_gname(String prt_gname) {
		this.prt_gname = prt_gname;
	}
	public String getPrt_grd_occupa() {
		return prt_grd_occupa;
	}
	public void setPrt_grd_occupa(String prt_grd_occupa) {
		this.prt_grd_occupa = prt_grd_occupa;
	}
	public String getPrt_grd_design() {
		return prt_grd_design;
	}
	public void setPrt_grd_design(String prt_grd_design) {
		this.prt_grd_design = prt_grd_design;
	}
	public String getPrt_grd_off_add() {
		return prt_grd_off_add;
	}
	public void setPrt_grd_off_add(String prt_grd_off_add) {
		this.prt_grd_off_add = prt_grd_off_add;
	}
	public String getPrt_grd_home_add() {
		return prt_grd_home_add;
	}
	public void setPrt_grd_home_add(String prt_grd_home_add) {
		this.prt_grd_home_add = prt_grd_home_add;
	}
	public String getPrt_grd_mobile() {
		return prt_grd_mobile;
	}
	public void setPrt_grd_mobile(String prt_grd_mobile) {
		this.prt_grd_mobile = prt_grd_mobile;
	}
	public String getPrt_grd_email() {
		return prt_grd_email;
	}
	public void setPrt_grd_email(String prt_grd_email) {
		this.prt_grd_email = prt_grd_email;
	}
	public String getPrt_grd_relation() {
		return prt_grd_relation;
	}
	public void setPrt_grd_relation(String prt_grd_relation) {
		this.prt_grd_relation = prt_grd_relation;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_by() {
		return inserted_by;
	}
	public void setInserted_by(String inserted_by) {
		this.inserted_by = inserted_by;
	}
	public Date getInserted_date() {
		return inserted_date;
	}
	public void setInserted_date(Date inserted_date) {
		this.inserted_date = inserted_date;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public String getStudent_id() {
		return student_id;
	}
	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getSphoto() {
		return sphoto;
	}
	public void setSphoto(String sphoto) {
		this.sphoto = sphoto;
	}
	public String getSdob() {
		return sdob;
	}
	public void setSdob(String sdob) {
		this.sdob = sdob;
	}
	public String getBplace() {
		return bplace;
	}
	public void setBplace(String bplace) {
		this.bplace = bplace;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getMtongue() {
		return mtongue;
	}
	public void setMtongue(String mtongue) {
		this.mtongue = mtongue;
	}
	public String getCgender() {
		return cgender;
	}
	public void setCgender(String cgender) {
		this.cgender = cgender;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCemail() {
		return cemail;
	}
	public void setCemail(String cemail) {
		this.cemail = cemail;
	}
	public String getSch_transport() {
		return sch_transport;
	}
	public void setSch_transport(String sch_transport) {
		this.sch_transport = sch_transport;
	}
	public String getPre_school() {
		return pre_school;
	}
	public void setPre_school(String pre_school) {
		this.pre_school = pre_school;
	}
	public String getPrd_from() {
		return prd_from;
	}
	public void setPrd_from(String prd_from) {
		this.prd_from = prd_from;
	}
	public String getPrd_to() {
		return prd_to;
	}
	public void setPrd_to(String prd_to) {
		this.prd_to = prd_to;
	}
	public String getAdm_id() {
		return adm_id;
	}
	public void setAdm_id(String adm_id) {
		this.adm_id = adm_id;
	}
	public String getStudent_roll() {
		return student_roll;
	}
	public void setStudent_roll(String student_roll) {
		this.student_roll = student_roll;
	}
	public Date getJoin_date() {
		return Join_date;
	}
	public void setJoin_date(Date join_date) {
		Join_date = join_date;
	}
	public String getHostel_name() {
		return Hostel_name;
	}
	public void setHostel_name(String hostel_name) {
		Hostel_name = hostel_name;
	}
	public String getHostel_Room_no() {
		return Hostel_Room_no;
	}
	public void setHostel_Room_no(String hostel_Room_no) {
		Hostel_Room_no = hostel_Room_no;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getSchool_Name() {
		return School_Name;
	}
	public void setSchool_Name(String school_Name) {
		School_Name = school_Name;
	}
	public String getAcademic_year() {
		return Academic_year;
	}
	public void setAcademic_year(String academic_year) {
		Academic_year = academic_year;
	}
	
	
	
	
	public String getSclass() {
		return sclass;
	}
	public void setSclass(String sclass) {
		this.sclass = sclass;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public List<StuParVO> getStudentDetailList() {
		return studentDetailList;
	}
	public void setStudentDetailList(List<StuParVO> studentDetailList) {
		this.studentDetailList = studentDetailList;
	}
	public List<StuParVO> getStudentServiceVOList() {
		return studentServiceVOList;
	}
	public void setStudentServiceVOList(List<StuParVO> studentServiceVOList) {
		this.studentServiceVOList = studentServiceVOList;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}


	
	
}
