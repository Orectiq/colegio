package com.model;

import java.util.Date;
import java.util.List;

public class LibraryIssueVO 
{
	String breq_id=null;
	String book_id=null;
	String book_title=null;
	String author=null;
	String student_id=null;
	String student_name=null;
	String sclass=null;
	String sec=null;
	Date req_date=null;
	String status=null;
	Date due_date=null;
	int fineamt=0;
	String approved_by=null;
	int count=0;
	String subject=null;
	String category=null;
	
	
	private List<LibraryIssueVO> libraryIssueServiceVOList = null;

	
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getBreq_id() {
		return breq_id;
	}

	public void setBreq_id(String breq_id) {
		this.breq_id = breq_id;
	}

	public String getBook_id() {
		return book_id;
	}

	public void setBook_id(String book_id) {
		this.book_id = book_id;
	}

	public String getBook_title() {
		return book_title;
	}

	public void setBook_title(String book_title) {
		this.book_title = book_title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getStudent_id() {
		return student_id;
	}

	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}

	public String getStudent_name() {
		return student_name;
	}

	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSec() {
		return sec;
	}

	public void setSec(String sec) {
		this.sec = sec;
	}

	public Date getReq_date() {
		return req_date;
	}

	public void setReq_date(Date req_date) {
		this.req_date = req_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDue_date() {
		return due_date;
	}

	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}

	public int getFineamt() {
		return fineamt;
	}

	public void setFineamt(int fineamt) {
		this.fineamt = fineamt;
	}

	public List<LibraryIssueVO> getLibraryIssueServiceVOList() {
		return libraryIssueServiceVOList;
	}

	public void setLibraryIssueServiceVOList(List<LibraryIssueVO> libraryIssueServiceVOList) {
		this.libraryIssueServiceVOList = libraryIssueServiceVOList;
	}

	public String getApproved_by() {
		return approved_by;
	}

	public void setApproved_by(String approved_by) {
		this.approved_by = approved_by;
	}

	
	
	
}
