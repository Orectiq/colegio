package com.model;

import java.util.Date;
import java.util.List;

public class LibraryReqVO {

	
	String Book_Title=null;
	String Author=null;
	String Edition=null;
	String Description=null;
	String Rate=null;
	String isActive=null;
	String Inserted_By=null;
	Date Inserted_Date=null;
	String Updated_By=null;
	Date Updated_Date=null;
	
	//LibraryDetailList
	private List<LibraryReqVO> libraryDetailList = null;
	
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_By() {
		return Inserted_By;
	}
	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}
	public Date getInserted_Date() {
		return Inserted_Date;
	}
	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	public Date getUpdated_Date() {
		return Updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}
	public String getBook_Title() {
		return Book_Title;
	}
	public void setBook_Title(String book_Title) {
		Book_Title = book_Title;
	}
	public String getAuthor() {
		return Author;
	}
	public void setAuthor(String author) {
		Author = author;
	}
	public String getEdition() {
		return Edition;
	}
	public void setEdition(String edition) {
		Edition = edition;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getRate() {
		return Rate;
	}
	public void setRate(String rate) {
		Rate = rate;
	}
	public List<LibraryReqVO> getLibraryDetailList() {
		return libraryDetailList;
	}
	public void setLibraryDetailList(List<LibraryReqVO> libraryDetailList) {
		this.libraryDetailList = libraryDetailList;
	}
	
	
}
