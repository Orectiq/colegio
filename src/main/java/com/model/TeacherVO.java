package com.model;

import java.util.Date;
import java.util.List;

public class TeacherVO 
{
	private String Tid=null;
	private String fname=null;
	private String lname=null;
	private String sclass=null;
	private String section=null;
	private String ttype=null;
	private String subject_handle=null;
	
	
	private String uname=null;
	private String pass=null;
	private String photo=null;
	private String dob=null;
	private String nationality=null;
	private String cgender=null;
	private String address1=null;
	private String address2=null;
	private String city=null;
	private String state=null;
	private String pin=null;
	private String mobile=null;
	private String phone=null;
	private String cemail=null;
	private String pan=null;
	private int lallowed=0;
	private int lapply=0;
	private int lbalance=0;
	
	private String tech_school1_name=null;
	private String tech_per1=null;
	private String ayear1=null;
	private String tech_school2_name=null;
	private String tech_per2=null;
	private String ayear2=null;
	private String tech_school3_name=null;
	private String tech_per3=null;
	private String ayear3=null;
	private String tech_school4_name=null;
	private String tech_per4=null;
	private String ayear4=null;

	private String tech_prev_emp1=null;
	private String tech_prev_emp_desg1=null;
	private String tech_year_of_exp1=null;
	private String tech_prev_emp2=null;
	private String tech_prev_emp_desg2=null;
	private String tech_year_of_exp2=null;
	private String tech_prev_emp3=null;
	private String tech_prev_emp_desg3=null;
	private String tech_year_of_exp3=null;
	
	private String isActive=null;
	private String created_by=null;
	private String created_date=null;
	private String updated_by=null;
	private String updated_date=null;
	
	private String role=null;
	
	private String lpass=null;
	private String ltype=null;
	private String slogo=null;
	private String sname=null;
	private String ayear=null;
	 private String sheader=null;
	 private String sfooter=null;
	 private String pass_change=null;
	 
	 private String tran_typ=null;
	
	
	private List<TeacherVO> teacherServiceVOList = null;
	
	
	
	public String getTran_typ() {
		return tran_typ;
	}
	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}
	public String getPass_change() {
		return pass_change;
	}
	public void setPass_change(String pass_change) {
		this.pass_change = pass_change;
	}
	public String getSheader() {
		return sheader;
	}
	public void setSheader(String sheader) {
		this.sheader = sheader;
	}
	public String getSfooter() {
		return sfooter;
	}
	public void setSfooter(String sfooter) {
		this.sfooter = sfooter;
	}
	public String getSlogo() {
		return slogo;
	}
	public void setSlogo(String slogo) {
		this.slogo = slogo;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getAyear() {
		return ayear;
	}
	public void setAyear(String ayear) {
		this.ayear = ayear;
	}
	public String getTid() {
		return Tid;
	}
	public void setTid(String tid) {
		Tid = tid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getSclass() {
		return sclass;
	}
	public void setSclass(String sclass) {
		this.sclass = sclass;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getTtype() {
		return ttype;
	}
	public void setTtype(String ttype) {
		this.ttype = ttype;
	}
	public String getSubject_handle() {
		return subject_handle;
	}
	public void setSubject_handle(String subject_handle) {
		this.subject_handle = subject_handle;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getCgender() {
		return cgender;
	}
	public void setCgender(String cgender) {
		this.cgender = cgender;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCemail() {
		return cemail;
	}
	public void setCemail(String cemail) {
		this.cemail = cemail;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public int getLallowed() {
		return lallowed;
	}
	public void setLallowed(int lallowed) {
		this.lallowed = lallowed;
	}
	public int getLapply() {
		return lapply;
	}
	public void setLapply(int lapply) {
		this.lapply = lapply;
	}
	public int getLbalance() {
		return lbalance;
	}
	public void setLbalance(int lbalance) {
		this.lbalance = lbalance;
	}
	public String getTech_school1_name() {
		return tech_school1_name;
	}
	public void setTech_school1_name(String tech_school1_name) {
		this.tech_school1_name = tech_school1_name;
	}
	public String getTech_per1() {
		return tech_per1;
	}
	public void setTech_per1(String tech_per1) {
		this.tech_per1 = tech_per1;
	}
	public String getAyear1() {
		return ayear1;
	}
	public void setAyear1(String ayear1) {
		this.ayear1 = ayear1;
	}
	public String getTech_school2_name() {
		return tech_school2_name;
	}
	public void setTech_school2_name(String tech_school2_name) {
		this.tech_school2_name = tech_school2_name;
	}
	public String getTech_per2() {
		return tech_per2;
	}
	public void setTech_per2(String tech_per2) {
		this.tech_per2 = tech_per2;
	}
	public String getAyear2() {
		return ayear2;
	}
	public void setAyear2(String ayear2) {
		this.ayear2 = ayear2;
	}
	public String getTech_school3_name() {
		return tech_school3_name;
	}
	public void setTech_school3_name(String tech_school3_name) {
		this.tech_school3_name = tech_school3_name;
	}
	public String getTech_per3() {
		return tech_per3;
	}
	public void setTech_per3(String tech_per3) {
		this.tech_per3 = tech_per3;
	}
	public String getAyear3() {
		return ayear3;
	}
	public void setAyear3(String ayear3) {
		this.ayear3 = ayear3;
	}
	public String getTech_school4_name() {
		return tech_school4_name;
	}
	public void setTech_school4_name(String tech_school4_name) {
		this.tech_school4_name = tech_school4_name;
	}
	public String getTech_per4() {
		return tech_per4;
	}
	public void setTech_per4(String tech_per4) {
		this.tech_per4 = tech_per4;
	}
	public String getAyear4() {
		return ayear4;
	}
	public void setAyear4(String ayear4) {
		this.ayear4 = ayear4;
	}
	public String getTech_prev_emp1() {
		return tech_prev_emp1;
	}
	public void setTech_prev_emp1(String tech_prev_emp1) {
		this.tech_prev_emp1 = tech_prev_emp1;
	}
	public String getTech_prev_emp_desg1() {
		return tech_prev_emp_desg1;
	}
	public void setTech_prev_emp_desg1(String tech_prev_emp_desg1) {
		this.tech_prev_emp_desg1 = tech_prev_emp_desg1;
	}
	public String getTech_year_of_exp1() {
		return tech_year_of_exp1;
	}
	public void setTech_year_of_exp1(String tech_year_of_exp1) {
		this.tech_year_of_exp1 = tech_year_of_exp1;
	}
	public String getTech_prev_emp2() {
		return tech_prev_emp2;
	}
	public void setTech_prev_emp2(String tech_prev_emp2) {
		this.tech_prev_emp2 = tech_prev_emp2;
	}
	public String getTech_prev_emp_desg2() {
		return tech_prev_emp_desg2;
	}
	public void setTech_prev_emp_desg2(String tech_prev_emp_desg2) {
		this.tech_prev_emp_desg2 = tech_prev_emp_desg2;
	}
	public String getTech_year_of_exp2() {
		return tech_year_of_exp2;
	}
	public void setTech_year_of_exp2(String tech_year_of_exp2) {
		this.tech_year_of_exp2 = tech_year_of_exp2;
	}
	public String getTech_prev_emp3() {
		return tech_prev_emp3;
	}
	public void setTech_prev_emp3(String tech_prev_emp3) {
		this.tech_prev_emp3 = tech_prev_emp3;
	}
	public String getTech_prev_emp_desg3() {
		return tech_prev_emp_desg3;
	}
	public void setTech_prev_emp_desg3(String tech_prev_emp_desg3) {
		this.tech_prev_emp_desg3 = tech_prev_emp_desg3;
	}
	public String getTech_year_of_exp3() {
		return tech_year_of_exp3;
	}
	public void setTech_year_of_exp3(String tech_year_of_exp3) {
		this.tech_year_of_exp3 = tech_year_of_exp3;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public List<TeacherVO> getTeacherServiceVOList() {
		return teacherServiceVOList;
	}
	public void setTeacherServiceVOList(List<TeacherVO> teacherServiceVOList) {
		this.teacherServiceVOList = teacherServiceVOList;
	}
	public String getLpass() {
		return lpass;
	}
	public void setLpass(String lpass) {
		this.lpass = lpass;
	}
	public String getLtype() {
		return ltype;
	}
	public void setLtype(String ltype) {
		this.ltype = ltype;
	}
	
	
	

	
	
}
