package com.model;

import java.util.Date;
import java.util.List;

public class EchallanStudentVO 
{

	private String student_id=null;
	private String firstname = null;
	private String lastname = null;
	private String photo = null;
	private Date sdob = null;
	private String bplace=null;
	private String nationality=null;
	private String mtongue=null;
	private String cgender=null;
	private String religion=null;
	private String category=null;
	private String address1=null;
	private String address2=null;
	private String state=null;
	private int pin=0;
	private String mobile=null;
	private String phone=null;
	private String cemail=null;
	private String sch_transport=null;
	private String pre_school=null;
	private Date prd_from=null;
	private Date prd_to=null;
	private String parent_id=null;
	private String adm_id=null;
	private String student_roll=null;
	private Date Join_date=null;
	private String Hostel_name=null;
	private String Hostel_Room_no=null;
	private String School_Name=null;
	private String Academic_year=null;
	private String sclass=null;
	private String section=null;

	private List<EchallanStudentVO> studentDetailList=null;
	private List<EchallanStudentVO> studentServiceVOList = null;
	
	private String fname = null;
	private String lname = null;
	private Date dob = null;
	private String parentid = null;
	
	//EChallan
	
	private String challan_id=null;
	private String theDate = null;
	private String fee_name = null;
	private String acc_year = null;
	private String Fee_Break1 = null;
	private String amount1 = null;
	private String Fee_Break2 = null;
	private String amount2 = null;
	private String Fee_Break3 = null;
	private String amount3 = null;	
	private String total = null;
	private String SClass = null;
	private String Section = null;
	private String datepicker = null;	
	private String isActive = null;
	private String Inserted_By = null;
	private Date Inserted_Date = null;
	private String Updated_By = null;
	private Date Updated_Date = null;
	private String sendMessage = null;
	
	private List<EchallanStudentVO> echallanServiceVOList = null;

	public String getStudent_id() {
		return student_id;
	}

	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Date getSdob() {
		return sdob;
	}

	public void setSdob(Date sdob) {
		this.sdob = sdob;
	}

	public String getBplace() {
		return bplace;
	}

	public void setBplace(String bplace) {
		this.bplace = bplace;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getMtongue() {
		return mtongue;
	}

	public void setMtongue(String mtongue) {
		this.mtongue = mtongue;
	}

	public String getCgender() {
		return cgender;
	}

	public void setCgender(String cgender) {
		this.cgender = cgender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCemail() {
		return cemail;
	}

	public void setCemail(String cemail) {
		this.cemail = cemail;
	}

	public String getSch_transport() {
		return sch_transport;
	}

	public void setSch_transport(String sch_transport) {
		this.sch_transport = sch_transport;
	}

	public String getPre_school() {
		return pre_school;
	}

	public void setPre_school(String pre_school) {
		this.pre_school = pre_school;
	}

	public Date getPrd_from() {
		return prd_from;
	}

	public void setPrd_from(Date prd_from) {
		this.prd_from = prd_from;
	}

	public Date getPrd_to() {
		return prd_to;
	}

	public void setPrd_to(Date prd_to) {
		this.prd_to = prd_to;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	public String getAdm_id() {
		return adm_id;
	}

	public void setAdm_id(String adm_id) {
		this.adm_id = adm_id;
	}

	public String getStudent_roll() {
		return student_roll;
	}

	public void setStudent_roll(String student_roll) {
		this.student_roll = student_roll;
	}

	public Date getJoin_date() {
		return Join_date;
	}

	public void setJoin_date(Date join_date) {
		Join_date = join_date;
	}

	public String getHostel_name() {
		return Hostel_name;
	}

	public void setHostel_name(String hostel_name) {
		Hostel_name = hostel_name;
	}

	public String getHostel_Room_no() {
		return Hostel_Room_no;
	}

	public void setHostel_Room_no(String hostel_Room_no) {
		Hostel_Room_no = hostel_Room_no;
	}

	public String getSchool_Name() {
		return School_Name;
	}

	public void setSchool_Name(String school_Name) {
		School_Name = school_Name;
	}

	public String getAcademic_year() {
		return Academic_year;
	}

	public void setAcademic_year(String academic_year) {
		Academic_year = academic_year;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public List<EchallanStudentVO> getStudentDetailList() {
		return studentDetailList;
	}

	public void setStudentDetailList(List<EchallanStudentVO> studentDetailList) {
		this.studentDetailList = studentDetailList;
	}

	public List<EchallanStudentVO> getStudentServiceVOList() {
		return studentServiceVOList;
	}

	public void setStudentServiceVOList(List<EchallanStudentVO> studentServiceVOList) {
		this.studentServiceVOList = studentServiceVOList;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public String getChallan_id() {
		return challan_id;
	}

	public void setChallan_id(String challan_id) {
		this.challan_id = challan_id;
	}

	public String getTheDate() {
		return theDate;
	}

	public void setTheDate(String theDate) {
		this.theDate = theDate;
	}

	public String getFee_name() {
		return fee_name;
	}

	public void setFee_name(String fee_name) {
		this.fee_name = fee_name;
	}

	public String getAcc_year() {
		return acc_year;
	}

	public void setAcc_year(String acc_year) {
		this.acc_year = acc_year;
	}

	public String getFee_Break1() {
		return Fee_Break1;
	}

	public void setFee_Break1(String fee_Break1) {
		Fee_Break1 = fee_Break1;
	}

	public String getAmount1() {
		return amount1;
	}

	public void setAmount1(String amount1) {
		this.amount1 = amount1;
	}

	public String getFee_Break2() {
		return Fee_Break2;
	}

	public void setFee_Break2(String fee_Break2) {
		Fee_Break2 = fee_Break2;
	}

	public String getAmount2() {
		return amount2;
	}

	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}

	public String getFee_Break3() {
		return Fee_Break3;
	}

	public void setFee_Break3(String fee_Break3) {
		Fee_Break3 = fee_Break3;
	}

	public String getAmount3() {
		return amount3;
	}

	public void setAmount3(String amount3) {
		this.amount3 = amount3;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getSClass() {
		return SClass;
	}

	public void setSClass(String sClass) {
		SClass = sClass;
	}

	public String getDatepicker() {
		return datepicker;
	}

	public void setDatepicker(String datepicker) {
		this.datepicker = datepicker;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getInserted_By() {
		return Inserted_By;
	}

	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}

	public Date getInserted_Date() {
		return Inserted_Date;
	}

	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}

	public String getUpdated_By() {
		return Updated_By;
	}

	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}

	public Date getUpdated_Date() {
		return Updated_Date;
	}

	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}

	public String getSendMessage() {
		return sendMessage;
	}

	public void setSendMessage(String sendMessage) {
		this.sendMessage = sendMessage;
	}

	public List<EchallanStudentVO> getEchallanServiceVOList() {
		return echallanServiceVOList;
	}

	public void setEchallanServiceVOList(List<EchallanStudentVO> echallanServiceVOList) {
		this.echallanServiceVOList = echallanServiceVOList;
	}
	
	

}
