package com.model;

import java.util.Date;
import java.util.List;

public class HomeworkStatusVO
{
	//date and calendar
	private Date theDate=null;
	
	
	//class and section
	private String student_id = null;
	private String sClass1 = null;
	private String Section1 = null;	
	
	//TeacherName and TeacherId
	private String Teacher_Name = null;
	private String Teacher_Id = null;	
	private String ttype = null;
	private String mobile = null;
	
	
	//#1.Period,Subject,Homework
	private String Subject = null;
	private String HomeWork = null;
	private String status = null;
	private String remark = null;
	
	
	private List<HomeworkStatusVO> homeworkStatusServiceVOList = null;


	


	public Date getTheDate() {
		return theDate;
	}


	public void setTheDate(Date theDate) {
		this.theDate = theDate;
	}


	public String getStudent_id() {
		return student_id;
	}


	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}


	public String getsClass1() {
		return sClass1;
	}


	public void setsClass1(String sClass1) {
		this.sClass1 = sClass1;
	}


	public String getSection1() {
		return Section1;
	}


	public void setSection1(String section1) {
		Section1 = section1;
	}


	public String getTeacher_Name() {
		return Teacher_Name;
	}


	public void setTeacher_Name(String teacher_Name) {
		Teacher_Name = teacher_Name;
	}


	public String getTeacher_Id() {
		return Teacher_Id;
	}


	public void setTeacher_Id(String teacher_Id) {
		Teacher_Id = teacher_Id;
	}


	public String getTtype() {
		return ttype;
	}


	public void setTtype(String ttype) {
		this.ttype = ttype;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getSubject() {
		return Subject;
	}


	public void setSubject(String subject) {
		Subject = subject;
	}


	public String getHomeWork() {
		return HomeWork;
	}


	public void setHomeWork(String homeWork) {
		HomeWork = homeWork;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public List<HomeworkStatusVO> getHomeworkStatusServiceVOList() {
		return homeworkStatusServiceVOList;
	}


	public void setHomeworkStatusServiceVOList(List<HomeworkStatusVO> homeworkStatusServiceVOList) {
		this.homeworkStatusServiceVOList = homeworkStatusServiceVOList;
	}
	
		
	
	

}
