package com.model;

import java.util.List;

public class CountryVO 
{
	private int id;
	private String sortname;
	private String name;
	private int phonecode;
	
	private List<CountryVO> countryServiceVOList = null;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSortname() {
		return sortname;
	}

	public void setSortname(String sortname) {
		this.sortname = sortname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPhonecode() {
		return phonecode;
	}

	public void setPhonecode(int phonecode) {
		this.phonecode = phonecode;
	}

	public List<CountryVO> getCountryServiceVOList() {
		return countryServiceVOList;
	}

	public void setCountryServiceVOList(List<CountryVO> countryServiceVOList) {
		this.countryServiceVOList = countryServiceVOList;
	}
	
	

}
