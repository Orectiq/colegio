package com.model;

import java.util.Date;
import java.util.List;

public class AddActivityVO {

	//private String Activity_ID=null;
	private Date ADate = null;
	private String tid = null;
	private String tname = null;
	private String ttype = null;
	private String aclass = null;
	private String asec = null;
	private String Place = null;
	private String Brief_Description = null;
	private String Description1 = null;
	private String Description2 = null;
	private String Students_Involved = null;
	private String Upload_Photo = null;
	private String Upload_Document = null;
	private String widget = null;
	private String isActive = null;
	private String Inserted_By = null;
	private Date Inserted_Date = null;
	private String Updated_By = null;
	private Date Updated_Date = null;
	
	private List<AddActivityVO> addActivityServiceVOList = null;
	
	
	
	
	
	
	
	public Date getADate() {
		return ADate;
	}
	public void setADate(Date aDate) {
		ADate = aDate;
	}
	public String getPlace() {
		return Place;
	}
	public void setPlace(String place) {
		Place = place;
	}
	public String getBrief_Description() {
		return Brief_Description;
	}
	public void setBrief_Description(String brief_Description) {
		Brief_Description = brief_Description;
	}
	public String getDescription1() {
		return Description1;
	}
	public void setDescription1(String description1) {
		Description1 = description1;
	}
	public String getDescription2() {
		return Description2;
	}
	public void setDescription2(String description2) {
		Description2 = description2;
	}
	public String getStudents_Involved() {
		return Students_Involved;
	}
	public void setStudents_Involved(String students_Involved) {
		Students_Involved = students_Involved;
	}
	public String getUpload_Photo() {
		return Upload_Photo;
	}
	public void setUpload_Photo(String upload_Photo) {
		Upload_Photo = upload_Photo;
	}
	public String getUpload_Document() {
		return Upload_Document;
	}
	public void setUpload_Document(String upload_Document) {
		Upload_Document = upload_Document;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_By() {
		return Inserted_By;
	}
	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}
	
	public Date getInserted_Date() {
		return Inserted_Date;
	}
	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	
	
	public Date getUpdated_Date() {
		return Updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}
	public List<AddActivityVO> getAddActivityServiceVOList() {
		return addActivityServiceVOList;
	}
	public void setAddActivityServiceVOList(List<AddActivityVO> addActivityServiceVOList) {
		this.addActivityServiceVOList = addActivityServiceVOList;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getTtype() {
		return ttype;
	}
	public void setTtype(String ttype) {
		this.ttype = ttype;
	}
	public String getAclass() {
		return aclass;
	}
	public void setAclass(String aclass) {
		this.aclass = aclass;
	}
	public String getAsec() {
		return asec;
	}
	public void setAsec(String asec) {
		this.asec = asec;
	}
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public String getWidget() {
		return widget;
	}
	public void setWidget(String widget) {
		this.widget = widget;
	}
	
}
