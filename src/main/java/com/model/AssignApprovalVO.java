package com.model;

import java.util.List;

public class AssignApprovalVO 
{
	private String assign_date = null;
	private String teacher_id=null;
	private String tname=null;
	private String ttype=null;
	private String student_id=null;
	private String student_name=null;
	private String sclass=null;
	private String section=null;
	private String subject=null;
	private String title=null;
	private String description=null;
	private String status=null;
	private String mark=null;
	private List<AssignApprovalVO> assignmentApprovalServiceVOList = null;
	public String getAssign_date() {
		return assign_date;
	}
	public void setAssign_date(String assign_date) {
		this.assign_date = assign_date;
	}
	public String getTeacher_id() {
		return teacher_id;
	}
	public void setTeacher_id(String teacher_id) {
		this.teacher_id = teacher_id;
	}
	public String getStudent_id() {
		return student_id;
	}
	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}
	public String getStudent_name() {
		return student_name;
	}
	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}
	public String getSclass() {
		return sclass;
	}
	public void setSclass(String sclass) {
		this.sclass = sclass;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public List<AssignApprovalVO> getAssignmentApprovalServiceVOList() {
		return assignmentApprovalServiceVOList;
	}
	public void setAssignmentApprovalServiceVOList(List<AssignApprovalVO> assignmentApprovalServiceVOList) {
		this.assignmentApprovalServiceVOList = assignmentApprovalServiceVOList;
	}
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public String getTtype() {
		return ttype;
	}
	public void setTtype(String ttype) {
		this.ttype = ttype;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	

}
