package com.model;

import java.util.Date;
import java.util.List;

public class MarksVO 
{
	private String exam_id = null;
	private String exam_name = null;
	private String exam_type = null;
	private String sclass=null;
	private String ssec=null;
	private String student_id=null;
	private String student_name=null;
	private int tamil=0;
	private int english=0;
	private int maths=0;
	private int science=0;
	private int sscience=0;
	private int total=0;
	private double avg=0.0;
	private String result=null;
	private String isActive=null;
	private String Inserted_By=null;
	private Date Inserted_Date=null;
	private String Updated_By=null;
	private Date Updated_Date=null;
	
	private String subject=null;
	private int mark=0;
	
	private List<MarksVO> marksServiceVOList = null;

	public String getExam_id() {
		return exam_id;
	}

	public void setExam_id(String exam_id) {
		this.exam_id = exam_id;
	}

	public String getExam_name() {
		return exam_name;
	}

	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}

	public String getExam_type() {
		return exam_type;
	}

	public void setExam_type(String exam_type) {
		this.exam_type = exam_type;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSsec() {
		return ssec;
	}

	public void setSsec(String ssec) {
		this.ssec = ssec;
	}

	public String getStudent_id() {
		return student_id;
	}

	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}

	public String getStudent_name() {
		return student_name;
	}

	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}

	public int getTamil() {
		return tamil;
	}

	public void setTamil(int tamil) {
		this.tamil = tamil;
	}

	public int getEnglish() {
		return english;
	}

	public void setEnglish(int english) {
		this.english = english;
	}

	public int getMaths() {
		return maths;
	}

	public void setMaths(int maths) {
		this.maths = maths;
	}

	public int getScience() {
		return science;
	}

	public void setScience(int science) {
		this.science = science;
	}

	public int getSscience() {
		return sscience;
	}

	public void setSscience(int sscience) {
		this.sscience = sscience;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<MarksVO> getMarksServiceVOList() {
		return marksServiceVOList;
	}

	public void setMarksServiceVOList(List<MarksVO> marksServiceVOList) {
		this.marksServiceVOList = marksServiceVOList;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getInserted_By() {
		return Inserted_By;
	}

	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}

	public Date getInserted_Date() {
		return Inserted_Date;
	}

	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}

	public String getUpdated_By() {
		return Updated_By;
	}

	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}

	public Date getUpdated_Date() {
		return Updated_Date;
	}

	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}
	
	
	
}
