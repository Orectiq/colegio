package com.model;

import java.util.Date;
import java.util.List;

public class AbsenceStuRequestVO 
{
	private String thedate = null;
	private String student_id = null;
	private String fname = null;
	private String sclass = null;
	private String section = null;
	private String start_date = null;
	private String end_date = null;
	private String absence_name=null; 
	private String status = null;
	private String comments = null;
	private int lapply = 0;
	private String approved_by=null;
	private String time=null;

	private List<AbsenceStuRequestVO> absenceStuRequestServiceVOList = null;

	public String getThedate() {
		return thedate;
	}

	public void setThedate(String thedate) {
		this.thedate = thedate;
	}

	public String getStudent_id() {
		return student_id;
	}

	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getAbsence_name() {
		return absence_name;
	}

	public void setAbsence_name(String absence_name) {
		this.absence_name = absence_name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getLapply() {
		return lapply;
	}

	public void setLapply(int lapply) {
		this.lapply = lapply;
	}

	public String getApproved_by() {
		return approved_by;
	}

	public void setApproved_by(String approved_by) {
		this.approved_by = approved_by;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<AbsenceStuRequestVO> getAbsenceStuRequestServiceVOList() {
		return absenceStuRequestServiceVOList;
	}

	public void setAbsenceStuRequestServiceVOList(List<AbsenceStuRequestVO> absenceStuRequestServiceVOList) {
		this.absenceStuRequestServiceVOList = absenceStuRequestServiceVOList;
	}

	
	

}
