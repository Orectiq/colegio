package com.model;

import java.util.Date;
import java.util.List;

public class teacherAttendanceVO
{
	private String adate=null;
	private String checkin=null;
	private String checkout=null;
	private String tid=null;
	private String tname=null;
	private String att_status=null;
	private String fdate=null;
	private String tdate=null;
	
	private String ttype=null;
	
	private List<teacherAttendanceVO> teacherAttendanceServiceVOList = null;

	

	public String getTtype() {
		return ttype;
	}

	public void setTtype(String ttype) {
		this.ttype = ttype;
	}

	public String getAdate() {
		return adate;
	}

	public void setAdate(String adate) {
		this.adate = adate;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getCheckout() {
		return checkout;
	}

	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getTname() {
		return tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	public String getAtt_status() {
		return att_status;
	}

	public void setAtt_status(String att_status) {
		this.att_status = att_status;
	}

	public List<teacherAttendanceVO> getTeacherAttendanceServiceVOList() {
		return teacherAttendanceServiceVOList;
	}

	public void setTeacherAttendanceServiceVOList(List<teacherAttendanceVO> teacherAttendanceServiceVOList) {
		this.teacherAttendanceServiceVOList = teacherAttendanceServiceVOList;
	}

	public String getFdate() {
		return fdate;
	}

	public void setFdate(String fdate) {
		this.fdate = fdate;
	}

	public String getTdate() {
		return tdate;
	}

	public void setTdate(String tdate) {
		this.tdate = tdate;
	}
	
	
	
}
