package com.model;

import java.util.List;

public class MenuItemVO 
{
	private String sno=null;
	private String main_menu=null;
	private String sub_menu=null;
	private String menu_item1 = null;
	private String menu_item2 = null;
	private String menu_item3 = null;
	private String mtype = null;
	private String pack=null;
	
	
	private List<MenuItemVO> menuitemServiceVOList = null;

	

	public String getMenu_item1() {
		return menu_item1;
	}

	public void setMenu_item1(String menu_item1) {
		this.menu_item1 = menu_item1;
	}

	public String getMenu_item2() {
		return menu_item2;
	}

	public void setMenu_item2(String menu_item2) {
		this.menu_item2 = menu_item2;
	}

	public String getMenu_item3() {
		return menu_item3;
	}

	public void setMenu_item3(String menu_item3) {
		this.menu_item3 = menu_item3;
	}

	public String getSno() {
		return sno;
	}

	public void setSno(String sno) {
		this.sno = sno;
	}

	public String getMain_menu() {
		return main_menu;
	}

	public void setMain_menu(String main_menu) {
		this.main_menu = main_menu;
	}

	public String getSub_menu() {
		return sub_menu;
	}

	public void setSub_menu(String sub_menu) {
		this.sub_menu = sub_menu;
	}

	public String getPack() {
		return pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	

	public String getMtype() {
		return mtype;
	}

	public void setMtype(String mtype) {
		this.mtype = mtype;
	}

	public List<MenuItemVO> getMenuitemServiceVOList() {
		return menuitemServiceVOList;
	}

	public void setMenuitemServiceVOList(List<MenuItemVO> menuitemServiceVOList) {
		this.menuitemServiceVOList = menuitemServiceVOList;
	}
	
	
	
	
}
