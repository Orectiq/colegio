package com.model;

import java.util.Date;
import java.util.List;

public class EchallanVO {

	private String challan_id=null;
	private String theDate = null;
	private String fee_name = null;
	private String acc_year = null;
	private String Fee_Break1 = null;
	private String amount1 = null;
	private String Fee_Break2 = null;
	private String amount2 = null;
	private String Fee_Break3 = null;
	private String amount3 = null;	
	private String total = null;
	private String SClass = null;
	private String Section = null;
	private String datepicker = null;	
	private String isActive = null;
	private String Inserted_By = null;
	private Date Inserted_Date = null;
	private String Updated_By = null;
	private Date Updated_Date = null;
	private String sendMessage = null;
	private String  tran_typ=null;
	
	
	
	private List<EchallanVO> echallanServiceVOList = null;
	
	public String getChallan_id() {
		return challan_id;
	}
	public void setChallan_id(String challan_id) {
		this.challan_id = challan_id;
	}
	
	
	
	public String getTheDate() {
		return theDate;
	}
	public void setTheDate(String theDate) {
		this.theDate = theDate;
	}
	public String getFee_name() {
		return fee_name;
	}
	public void setFee_name(String fee_name) {
		this.fee_name = fee_name;
	}
	
	public String getAcc_year() {
		return acc_year;
	}
	public void setAcc_year(String acc_year) {
		this.acc_year = acc_year;
	}
	public String getFee_Break1() {
		return Fee_Break1;
	}
	public void setFee_Break1(String fee_Break1) {
		Fee_Break1 = fee_Break1;
	}
	public String getAmount1() {
		return amount1;
	}
	public void setAmount1(String amount1) {
		this.amount1 = amount1;
	}
	public String getFee_Break2() {
		return Fee_Break2;
	}
	public void setFee_Break2(String fee_Break2) {
		Fee_Break2 = fee_Break2;
	}
	public String getAmount2() {
		return amount2;
	}
	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}
	public String getFee_Break3() {
		return Fee_Break3;
	}
	public void setFee_Break3(String fee_Break3) {
		Fee_Break3 = fee_Break3;
	}
	public String getAmount3() {
		return amount3;
	}
	public void setAmount3(String amount3) {
		this.amount3 = amount3;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getSClass() {
		return SClass;
	}
	public void setSClass(String sClass) {
		SClass = sClass;
	}
	public String getSection() {
		return Section;
	}
	public void setSection(String section) {
		Section = section;
	}
	
	
	public String getDatepicker() {
		return datepicker;
	}
	public void setDatepicker(String datepicker) {
		this.datepicker = datepicker;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_By() {
		return Inserted_By;
	}
	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}
	public Date getInserted_Date() {
		return Inserted_Date;
	}
	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	
	
	
	public Date getUpdated_Date() {
		return Updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}
	public List<EchallanVO> getEchallanServiceVOList() {
		return echallanServiceVOList;
	}
	public void setEchallanServiceVOList(List<EchallanVO> echallanServiceVOList) {
		this.echallanServiceVOList = echallanServiceVOList;
	}
	public String getSendMessage() {
		return sendMessage;
	}
	public void setSendMessage(String sendMessage) {
		this.sendMessage = sendMessage;
	}
	public String getTran_typ() {
		return tran_typ;
	}
	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}
	
	
	
	
}
