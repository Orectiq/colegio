package com.model;

public class SchoolInfoVO {

	private String schoolId;
	private String schoolName;
	private String schoolPhonePrimary;
	private String schoolAddress;
	private int schoolMembership;
	private String schoolPhoneSecondary;
	private String schoolEmail;
	private String schoolDB;
	private String schoolDBUser;
	private String schoolDBPass;

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolPhonePrimary() {
		return schoolPhonePrimary;
	}

	public void setSchoolPhonePrimary(String schoolPhonePrimary) {
		this.schoolPhonePrimary = schoolPhonePrimary;
	}

	public String getSchoolAddress() {
		return schoolAddress;
	}

	public void setSchoolAddress(String schoolAddress) {
		this.schoolAddress = schoolAddress;
	}

	public int getSchoolMembership() {
		return schoolMembership;
	}

	public void setSchoolMembership(int schoolMembership) {
		this.schoolMembership = schoolMembership;
	}

	public String getSchoolPhoneSecondary() {
		return schoolPhoneSecondary;
	}

	public void setSchoolPhoneSecondary(String schoolPhoneSecondary) {
		this.schoolPhoneSecondary = schoolPhoneSecondary;
	}

	public String getSchoolEmail() {
		return schoolEmail;
	}

	public void setSchoolEmail(String schoolEmail) {
		this.schoolEmail = schoolEmail;
	}

	public String getSchoolDB() {
		return schoolDB;
	}

	public void setSchoolDB(String schoolDB) {
		this.schoolDB = schoolDB;
	}

	public String getSchoolDBUser() {
		return schoolDBUser;
	}

	public void setSchoolDBUser(String schoolDBUser) {
		this.schoolDBUser = schoolDBUser;
	}

	public String getSchoolDBPass() {
		return schoolDBPass;
	}

	public void setSchoolDBPass(String schoolDBPass) {
		this.schoolDBPass = schoolDBPass;
	}

	public void setNullsToEmpty() {
		if (null == schoolId) {
			schoolId = "";
		}
		if (null == schoolName) {
			schoolName = "";
		}
		if (null == schoolPhonePrimary) {
			schoolPhonePrimary = "";
		}
		if (null == schoolAddress) {
			schoolAddress = "";
		}
		if (null == schoolPhoneSecondary) {
			schoolPhoneSecondary = "";
		}
		if (null == schoolEmail) {
			schoolEmail = "";
		}
		if (null == schoolDB) {
			schoolDB = "";
		}
		if (null == schoolDBUser) {
			schoolDBUser = "";
		}
		if (null == schoolDBPass) {
			schoolDBPass = "";
		}

	}
}
