package com.model;

import java.util.Date;
import java.util.List;

public class TransportAttendanceVO 
{
	private String adate=null;
	private String duser=null;
	private String boarding=null;
	private String route_num=null;
	private String student_id=null;
	private String student_name=null;
	private String sclass=null;
	private String section=null;
	private String attendance=null;
	private String atten=null;
	
	private List<TransportAttendanceVO> transportAttendanceServiceVOList = null;

	

	public String getAtten() {
		return atten;
	}

	public void setAtten(String atten) {
		this.atten = atten;
	}

	public String getAdate() {
		return adate;
	}

	public void setAdate(String adate) {
		this.adate = adate;
	}

	public String getDuser() {
		return duser;
	}

	public void setDuser(String duser) {
		this.duser = duser;
	}

	public String getBoarding() {
		return boarding;
	}

	public void setBoarding(String boarding) {
		this.boarding = boarding;
	}

	public String getRoute_num() {
		return route_num;
	}

	public void setRoute_num(String route_num) {
		this.route_num = route_num;
	}

	public String getStudent_id() {
		return student_id;
	}

	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}

	public String getStudent_name() {
		return student_name;
	}

	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getAttendance() {
		return attendance;
	}

	public void setAttendance(String attendance) {
		this.attendance = attendance;
	}

	public List<TransportAttendanceVO> getTransportAttendanceServiceVOList() {
		return transportAttendanceServiceVOList;
	}

	public void setTransportAttendanceServiceVOList(List<TransportAttendanceVO> transportAttendanceServiceVOList) {
		this.transportAttendanceServiceVOList = transportAttendanceServiceVOList;
	}
	
	

}
