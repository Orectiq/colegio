package com.model;

import java.sql.Time;
import java.util.Date;
import java.util.List;

public class NoticeboardVO {

	private String event_tittle=null;
	private String event_desc=null;
	private String event_place=null;
	private String n_student=null;
	private String n_parent=null;
	private String n_teacher=null;
	private String n_non_teach_staff=null;
	private Date doe=null;
	private String toe=null;
	String isActive=null;
	String inserted_By=null;
	Date inserted_Date=null;
	String updated_By=null;
	Date updated_Date=null;
	String eventsid=null;
	
	private String ltype=null;
	 private String tran_typ=null;
	
	//NoticeboardServiceVOList
	private List<NoticeboardVO> noticeboardServiceVOList = null;
	
	
	public String getEventsid() {
		return eventsid;
	}
	public void setEventsid(String eventsid) {
		this.eventsid = eventsid;
	}
	public String getToe() {
		return toe;
	}
	public void setToe(String toe) {
		this.toe = toe;
	}
	public String getEvent_tittle() {
		return event_tittle;
	}
	public void setEvent_tittle(String event_tittle) {
		this.event_tittle = event_tittle;
	}
	public String getEvent_desc() {
		return event_desc;
	}
	public void setEvent_desc(String event_desc) {
		this.event_desc = event_desc;
	}
	public String getEvent_place() {
		return event_place;
	}
	public void setEvent_place(String event_place) {
		this.event_place = event_place;
	}

	
	
	public String getN_student() {
		return n_student;
	}
	public void setN_student(String n_student) {
		this.n_student = n_student;
	}
	public String getN_parent() {
		return n_parent;
	}
	public void setN_parent(String n_parent) {
		this.n_parent = n_parent;
	}
	public String getN_teacher() {
		return n_teacher;
	}
	public void setN_teacher(String n_teacher) {
		this.n_teacher = n_teacher;
	}
	public String getN_non_teach_staff() {
		return n_non_teach_staff;
	}
	public void setN_non_teach_staff(String n_non_teach_staff) {
		this.n_non_teach_staff = n_non_teach_staff;
	}
	public Date getDoe() {
		return doe;
	}
	public void setDoe(Date doe) {
		this.doe = doe;
	}
	/*public void setNoticeboardServiceVOList(List<NoticeboardVO> geteventdetail) {
		// TODO Auto-generated method stub
		
	}*/
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_By() {
		return inserted_By;
	}
	public void setInserted_By(String inserted_By) {
		this.inserted_By = inserted_By;
	}
	public Date getInserted_Date() {
		return inserted_Date;
	}
	public void setInserted_Date(Date inserted_Date) {
		this.inserted_Date = inserted_Date;
	}
	public String getUpdated_By() {
		return updated_By;
	}
	public void setUpdated_By(String updated_By) {
		this.updated_By = updated_By;
	}
	public Date getUpdated_Date() {
		return updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		this.updated_Date = updated_Date;
	}
	public List<NoticeboardVO> getNoticeboardServiceVOList() {
		return noticeboardServiceVOList;
	}
	public void setNoticeboardServiceVOList(List<NoticeboardVO> noticeboardServiceVOList) {
		this.noticeboardServiceVOList = noticeboardServiceVOList;
	}
	public String getLtype() {
		return ltype;
	}
	public void setLtype(String ltype) {
		this.ltype = ltype;
	}
	public String getTran_typ() {
		return tran_typ;
	}
	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}
	
	
	
}
