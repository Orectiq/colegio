package com.model;

import java.util.Date;
import java.util.List;

public class SubjectVO {

String sclass=null;
	
	String section=null;
	
	String subject=null;
	

	 String tran_typ=null;
	String dele=null;
	String tran_type=null;
	
	
	private List<SubjectVO> subjectServiceVOList = null;



	
	public String getTran_type() {
		return tran_type;
	}

	public void setTran_type(String tran_type) {
		this.tran_type = tran_type;
	}

	public String getDele() {
		return dele;
	}

	public void setDele(String dele) {
		this.dele = dele;
	}

	public String getTran_typ() {
		return tran_typ;
	}

	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<SubjectVO> getSubjectServiceVOList() {
		return subjectServiceVOList;
	}

	public void setSubjectServiceVOList(List<SubjectVO> subjectServiceVOList) {
		this.subjectServiceVOList = subjectServiceVOList;
	}
	

	
	
	
}
