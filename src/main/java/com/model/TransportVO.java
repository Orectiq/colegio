package com.model;

import java.util.Date;
import java.util.List;

public class TransportVO {

	private String transport_name=null;
	private String transport_type=null;
	private String registration_number=null;
	private String description=null;
	private int no_seats=0;
	private int fee=0;
	private String area_cover=null;
	private String board_points=null;
	private String dri_photo=null;
	private String dri_name=null;
	private int dri_age=0;
	private String dri_contact_number=null;
	private String dri_alter_contact_number=null;
	private String dri_address=null;
	private String cond_photo=null;
	private String cond_name=null;
	private int cond_age=0;
	private String cond_contact_number=null;
	private String cond_alter_contact_number=null;
	private String cond_address=null;
	private String dlicense=null;
	private String email=null;
	
	private String route_num=null;
	private String uname=null;
	private String pass=null;
	private String role=null;
	
	 private String slogo=null;
	 private String sname=null;
	 private String ayear=null;
	 private String sheader=null;
	 private String sfooter=null;
	 private String pass_change=null;
	 private String ltype=null;
     private String lpass=null;
	
	private String tran_type=null;
	
	private List<TransportVO> transportServiceVOList = null;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLtype() {
		return ltype;
	}
	public void setLtype(String ltype) {
		this.ltype = ltype;
	}
	public String getLpass() {
		return lpass;
	}
	public void setLpass(String lpass) {
		this.lpass = lpass;
	}
	public String getSlogo() {
		return slogo;
	}
	public void setSlogo(String slogo) {
		this.slogo = slogo;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getAyear() {
		return ayear;
	}
	public void setAyear(String ayear) {
		this.ayear = ayear;
	}
	public String getSheader() {
		return sheader;
	}
	public void setSheader(String sheader) {
		this.sheader = sheader;
	}
	public String getSfooter() {
		return sfooter;
	}
	public void setSfooter(String sfooter) {
		this.sfooter = sfooter;
	}
	public String getPass_change() {
		return pass_change;
	}
	public void setPass_change(String pass_change) {
		this.pass_change = pass_change;
	}
	public String getTran_type() {
		return tran_type;
	}
	public void setTran_type(String tran_type) {
		this.tran_type = tran_type;
	}
	public String getDlicense() {
		return dlicense;
	}
	public void setDlicense(String dlicense) {
		this.dlicense = dlicense;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getTransport_name() {
		return transport_name;
	}
	public void setTransport_name(String transport_name) {
		this.transport_name = transport_name;
	}
	public String getTransport_type() {
		return transport_type;
	}
	public void setTransport_type(String transport_type) {
		this.transport_type = transport_type;
	}
	
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public String getArea_cover() {
		return area_cover;
	}
	public void setArea_cover(String area_cover) {
		this.area_cover = area_cover;
	}
	public String getBoard_points() {
		return board_points;
	}
	public void setBoard_points(String board_points) {
		this.board_points = board_points;
	}
	public String getDri_photo() {
		return dri_photo;
	}
	public void setDri_photo(String dri_photo) {
		this.dri_photo = dri_photo;
	}
	public String getDri_name() {
		return dri_name;
	}
	public void setDri_name(String dri_name) {
		this.dri_name = dri_name;
	}
	
	
	
	public String getDri_address() {
		return dri_address;
	}
	public void setDri_address(String dri_address) {
		this.dri_address = dri_address;
	}
	public String getCond_photo() {
		return cond_photo;
	}
	public void setCond_photo(String cond_photo) {
		this.cond_photo = cond_photo;
	}
	public String getCond_name() {
		return cond_name;
	}
	public void setCond_name(String cond_name) {
		this.cond_name = cond_name;
	}
	
	public String getCond_address() {
		return cond_address;
	}
	public void setCond_address(String cond_address) {
		this.cond_address = cond_address;
	}
	
	
	public List<TransportVO> getTransportServiceVOList() {
		return transportServiceVOList;
	}
	public void setTransportServiceVOList(List<TransportVO> transportServiceVOList) {
		this.transportServiceVOList = transportServiceVOList;
	}
	
	
	public String getRegistration_number() {
		return registration_number;
	}
	public void setRegistration_number(String registration_number) {
		this.registration_number = registration_number;
	}
	public int getNo_seats() {
		return no_seats;
	}
	public void setNo_seats(int no_seats) {
		this.no_seats = no_seats;
	}
	public int getFee() {
		return fee;
	}
	public void setFee(int fee) {
		this.fee = fee;
	}
	public int getDri_age() {
		return dri_age;
	}
	public void setDri_age(int dri_age) {
		this.dri_age = dri_age;
	}
	
	public int getCond_age() {
		return cond_age;
	}
	public void setCond_age(int cond_age) {
		this.cond_age = cond_age;
	}
	
	
	
	public String getDri_contact_number() {
		return dri_contact_number;
	}
	public void setDri_contact_number(String dri_contact_number) {
		this.dri_contact_number = dri_contact_number;
	}
	public String getDri_alter_contact_number() {
		return dri_alter_contact_number;
	}
	public void setDri_alter_contact_number(String dri_alter_contact_number) {
		this.dri_alter_contact_number = dri_alter_contact_number;
	}
	public String getCond_contact_number() {
		return cond_contact_number;
	}
	public void setCond_contact_number(String cond_contact_number) {
		this.cond_contact_number = cond_contact_number;
	}
	public String getCond_alter_contact_number() {
		return cond_alter_contact_number;
	}
	public void setCond_alter_contact_number(String cond_alter_contact_number) {
		this.cond_alter_contact_number = cond_alter_contact_number;
	}
	public String getRoute_num() {
		return route_num;
	}
	public void setRoute_num(String route_num) {
		this.route_num = route_num;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
	

	
	
	
}


