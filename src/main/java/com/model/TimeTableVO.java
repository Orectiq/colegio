package com.model;

import java.util.List;

public class TimeTableVO
{
	private String sclass=null;
	 private String section=null;
	 private String day=null;
	 private String time1=null;
	 private String time2=null;
	 private String time3=null;
	 private String time4=null;
	 private String time5=null;
	 private String time6=null;
	 private String time7=null;
	 private String time8=null;
	 private String time9=null;
	 private String time10=null;
	 private String time11=null;
	 private String time12=null;
	 private String time13=null;
	 private String time14=null;
	 private String time15=null;
	 
	 private String time1_hr=null;
	 private String time2_hr=null;
	 private String time3_hr=null;
	 private String time4_hr=null;
	 private String time5_hr=null;
	 private String time6_hr=null;
	 private String time7_hr=null;
	 private String time8_hr=null;
	 private String time9_hr=null;
	 private String time10_hr=null;
	 private String time11_hr=null;
	 private String time12_hr=null;
	 private String time13_hr=null;
	 private String time14_hr=null;
	 private String time15_hr=null;
	 
	 private String tran_typ=null;
	 private String period=null;
	 private String time=null;
	
	private List<TimeTableVO> timeTableServiceVOList = null;
	private List<TimeTableVO> timePeriodServiceVOList = null;

	
	
	
	
	public String getTime10() {
		return time10;
	}

	public void setTime10(String time10) {
		this.time10 = time10;
	}

	public String getTime11() {
		return time11;
	}

	public void setTime11(String time11) {
		this.time11 = time11;
	}

	public String getTime12() {
		return time12;
	}

	public void setTime12(String time12) {
		this.time12 = time12;
	}

	public String getTime13() {
		return time13;
	}

	public void setTime13(String time13) {
		this.time13 = time13;
	}

	public String getTime14() {
		return time14;
	}

	public void setTime14(String time14) {
		this.time14 = time14;
	}

	public String getTime15() {
		return time15;
	}

	public void setTime15(String time15) {
		this.time15 = time15;
	}

	public String getTime10_hr() {
		return time10_hr;
	}

	public void setTime10_hr(String time10_hr) {
		this.time10_hr = time10_hr;
	}

	public String getTime11_hr() {
		return time11_hr;
	}

	public void setTime11_hr(String time11_hr) {
		this.time11_hr = time11_hr;
	}

	public String getTime12_hr() {
		return time12_hr;
	}

	public void setTime12_hr(String time12_hr) {
		this.time12_hr = time12_hr;
	}

	public String getTime13_hr() {
		return time13_hr;
	}

	public void setTime13_hr(String time13_hr) {
		this.time13_hr = time13_hr;
	}

	public String getTime14_hr() {
		return time14_hr;
	}

	public void setTime14_hr(String time14_hr) {
		this.time14_hr = time14_hr;
	}

	public String getTime15_hr() {
		return time15_hr;
	}

	public void setTime15_hr(String time15_hr) {
		this.time15_hr = time15_hr;
	}

	public String getTime1_hr() {
		return time1_hr;
	}

	public void setTime1_hr(String time1_hr) {
		this.time1_hr = time1_hr;
	}

	public String getTime2_hr() {
		return time2_hr;
	}

	public void setTime2_hr(String time2_hr) {
		this.time2_hr = time2_hr;
	}

	public String getTime3_hr() {
		return time3_hr;
	}

	public void setTime3_hr(String time3_hr) {
		this.time3_hr = time3_hr;
	}

	public String getTime4_hr() {
		return time4_hr;
	}

	public void setTime4_hr(String time4_hr) {
		this.time4_hr = time4_hr;
	}

	public String getTime5_hr() {
		return time5_hr;
	}

	public void setTime5_hr(String time5_hr) {
		this.time5_hr = time5_hr;
	}

	public String getTime6_hr() {
		return time6_hr;
	}

	public void setTime6_hr(String time6_hr) {
		this.time6_hr = time6_hr;
	}

	public String getTime7_hr() {
		return time7_hr;
	}

	public void setTime7_hr(String time7_hr) {
		this.time7_hr = time7_hr;
	}

	public String getTime8_hr() {
		return time8_hr;
	}

	public void setTime8_hr(String time8_hr) {
		this.time8_hr = time8_hr;
	}

	public String getTime9_hr() {
		return time9_hr;
	}

	public void setTime9_hr(String time9_hr) {
		this.time9_hr = time9_hr;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<TimeTableVO> getTimePeriodServiceVOList() {
		return timePeriodServiceVOList;
	}

	public void setTimePeriodServiceVOList(List<TimeTableVO> timePeriodServiceVOList) {
		this.timePeriodServiceVOList = timePeriodServiceVOList;
	}

	public String getTran_typ() {
		return tran_typ;
	}

	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTime1() {
		return time1;
	}

	public void setTime1(String time1) {
		this.time1 = time1;
	}

	public String getTime2() {
		return time2;
	}

	public void setTime2(String time2) {
		this.time2 = time2;
	}

	public String getTime3() {
		return time3;
	}

	public void setTime3(String time3) {
		this.time3 = time3;
	}

	public String getTime4() {
		return time4;
	}

	public void setTime4(String time4) {
		this.time4 = time4;
	}

	public String getTime5() {
		return time5;
	}

	public void setTime5(String time5) {
		this.time5 = time5;
	}

	public String getTime6() {
		return time6;
	}

	public void setTime6(String time6) {
		this.time6 = time6;
	}

	public String getTime7() {
		return time7;
	}

	public void setTime7(String time7) {
		this.time7 = time7;
	}

	public String getTime8() {
		return time8;
	}

	public void setTime8(String time8) {
		this.time8 = time8;
	}

	public String getTime9() {
		return time9;
	}

	public void setTime9(String time9) {
		this.time9 = time9;
	}

	public List<TimeTableVO> getTimeTableServiceVOList() {
		return timeTableServiceVOList;
	}

	public void setTimeTableServiceVOList(List<TimeTableVO> timeTableServiceVOList) {
		this.timeTableServiceVOList = timeTableServiceVOList;
	}

	
	
	
	
}
