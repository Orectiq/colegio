package com.model;

import java.util.List;

public class GroupVO
{
	private String group_name=null;
	private String group_mem=null;
	 private String tran_typ=null;
	
	private List<GroupVO> groupServiceVOList = null;

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public String getGroup_mem() {
		return group_mem;
	}

	public void setGroup_mem(String group_mem) {
		this.group_mem = group_mem;
	}
	

	public String getTran_typ() {
		return tran_typ;
	}

	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}

	public List<GroupVO> getGroupServiceVOList() {
		return groupServiceVOList;
	}

	public void setGroupServiceVOList(List<GroupVO> groupServiceVOList) {
		this.groupServiceVOList = groupServiceVOList;
	}

	
	
	
}
