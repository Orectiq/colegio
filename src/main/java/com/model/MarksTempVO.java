package com.model;

import java.util.List;

public class MarksTempVO 
{
	String exam_id=null;
	String student_id=null;
	String student_name=null;
	String sclass=null;
	String ssec=null;
	
	private String tamil=null;
	private String english=null;
	private String maths=null;
	private String science=null;
	private String hindi=null;
	private String social=null;
	private String computer=null;
	private String sanskrit=null;
	
	private String total=null;
	private List<MarksTempVO> marksTempServiceVOList = null;
	
	
	public String getStudent_id() {
		return student_id;
	}
	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}
	public String getSclass() {
		return sclass;
	}
	public void setSclass(String sclass) {
		this.sclass = sclass;
	}
	public String getSsec() {
		return ssec;
	}
	public void setSsec(String ssec) {
		this.ssec = ssec;
	}
	
	
	
	public String getExam_id() {
		return exam_id;
	}
	public void setExam_id(String exam_id) {
		this.exam_id = exam_id;
	}
	public String getStudent_name() {
		return student_name;
	}
	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}
	public String getScience() {
		return science;
	}
	public void setScience(String science) {
		this.science = science;
	}
	public String getTamil() {
		return tamil;
	}
	public void setTamil(String tamil) {
		this.tamil = tamil;
	}
	public String getEnglish() {
		return english;
	}
	public void setEnglish(String english) {
		this.english = english;
	}
	public String getMaths() {
		return maths;
	}
	public void setMaths(String maths) {
		this.maths = maths;
	}
	
	
	
	
	
	public String getHindi() {
		return hindi;
	}
	public void setHindi(String hindi) {
		this.hindi = hindi;
	}
	public String getSocial() {
		return social;
	}
	public void setSocial(String social) {
		this.social = social;
	}
	public String getComputer() {
		return computer;
	}
	public void setComputer(String computer) {
		this.computer = computer;
	}
	public String getSanskrit() {
		return sanskrit;
	}
	public void setSanskrit(String sanskrit) {
		this.sanskrit = sanskrit;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public List<MarksTempVO> getMarksTempServiceVOList() {
		return marksTempServiceVOList;
	}
	public void setMarksTempServiceVOList(List<MarksTempVO> marksTempServiceVOList) {
		this.marksTempServiceVOList = marksTempServiceVOList;
	}
	
	
	
	
}
