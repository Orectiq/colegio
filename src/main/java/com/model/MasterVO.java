package com.model;
import java.util.Date;
import java.util.List;
public class MasterVO 
{
	private String role=null;
	private String desg=null;
	private String name=null;
	private String photo=null;
	private String uname = null;
	private String pass = null;
	private String schoolname = null;
	private String branch = null;
	private String logo = null;
	private String ayear = null;	
	private String email = null;
	private String mobile = null;
	private String phone = null;
	private String address = null;
	private Date startdate = null;
	private Date validdate = null;
	private String key = null;
	private String pack = null;
	private String admid = null;
	private String npass=null;
	private String ltype=null;
	
	private String slogo=null;
	private String sname=null;
	private String sheader=null;
	private String sfooter=null;
	 private String pass_change=null;
	 
	private List<MasterVO> masterServiceVOList = null;

	
	
	public String getPass_change() {
		return pass_change;
	}

	public void setPass_change(String pass_change) {
		this.pass_change = pass_change;
	}

	public String getSlogo() {
		return slogo;
	}

	public void setSlogo(String slogo) {
		this.slogo = slogo;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getSheader() {
		return sheader;
	}

	public void setSheader(String sheader) {
		this.sheader = sheader;
	}

	public String getSfooter() {
		return sfooter;
	}

	public void setSfooter(String sfooter) {
		this.sfooter = sfooter;
	}

	public String getLtype() {
		return ltype;
	}

	public void setLtype(String ltype) {
		this.ltype = ltype;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDesg() {
		return desg;
	}

	public void setDesg(String desg) {
		this.desg = desg;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getSchoolname() {
		return schoolname;
	}

	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getAyear() {
		return ayear;
	}

	public void setAyear(String ayear) {
		this.ayear = ayear;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	
	public Date getValiddate() {
		return validdate;
	}

	public void setValiddate(Date validdate) {
		this.validdate = validdate;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPack() {
		return pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getAdmid() {
		return admid;
	}

	public void setAdmid(String admid) {
		this.admid = admid;
	}

	public List<MasterVO> getMasterServiceVOList() {
		return masterServiceVOList;
	}

	public void setMasterServiceVOList(List<MasterVO> masterServiceVOList) {
		this.masterServiceVOList = masterServiceVOList;
	}

	public String getNpass() {
		return npass;
	}

	public void setNpass(String npass) {
		this.npass = npass;
	}
	
		
	
	
	
}
