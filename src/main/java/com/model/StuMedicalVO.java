package com.model;

import java.util.Date;
import java.util.List;

public class StuMedicalVO {

	private String student_id=null;
	private String blood_group  = null;
	private String height = null;
	private String weights = null;
	private String medical_condition = null;
	private String allergy = null;
	private String allergy_type = null;
	private String is_Physically_Challenged = null;
	private String phy_cha_type=null;
	
	private String is_Special_Child = null;
	private String specialChild_type = null;
	private String is_disease = null;
	private String disease_type = null;
	
	private String fname = null;
	private String lname = null;
	private String sclass=null;
	private String section=null;
	//@Lob
	private String photo = null;
	private Date dob = null;
	private String bplace=null;
	private String nationality=null;
	private String mtongue=null;
	private String cgender=null;
	private String religion=null;
	private String category=null;
	private String address1=null;
	private String address2=null;
	private String city=null;
	private String state=null;
	private String country=null;
	private int pin=0;
	private String mobile=null;
	private String phone=null;
	private String cemail=null;
	private String sch_transport=null;
	private String transport_board=null;
	private String hostel=null;
	private String hostel_allot=null;
	private String Hostel_name=null;
	private String Hostel_Room_no=null;
	
	private String pre_school1=null;
	private String pre_qual1=null;
	private String prd_from1=null;
	private String prd_to1=null;
	
	private String pre_school2=null;
	private String pre_qual2=null;
	private String prd_from2=null;
	private String prd_to2=null;
	
	private String pre_school3=null;
	private String pre_qual3=null;
	private String prd_from3=null;
	private String prd_to3=null;
	
	private String parent_id=null;
	private String adm_id=null;
	private String student_roll=null;
	private Date Join_date=null;
	private String School_Name=null;
	private String Academic_year=null;
	private String role=null;
	
	private int total=0;
	private String ltype=null;
	private String lpass=null;
	
	private String tran_typ=null;
	private int stucount=0;
	private String amonth=null;
	private String slogo=null;
	
	 private String sheader=null;
	 private String sfooter=null;
	 private String pass_change=null;
	 
	 private List<StuMedicalVO> StuMedicalList=null;
	 private List<StuMedicalVO> SuMedicalServiceVOList=null;
	 
	public String getStudent_id() {
		return student_id;
	}
	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}
	public String getBlood_group() {
		return blood_group;
	}
	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeights() {
		return weights;
	}
	public void setWeights(String weights) {
		this.weights = weights;
	}
	public String getMedical_condition() {
		return medical_condition;
	}
	public void setMedical_condition(String medical_condition) {
		this.medical_condition = medical_condition;
	}
	public String getAllergy() {
		return allergy;
	}
	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}
	public String getAllergy_type() {
		return allergy_type;
	}
	public void setAllergy_type(String allergy_type) {
		this.allergy_type = allergy_type;
	}
	public String getIs_Physically_Challenged() {
		return is_Physically_Challenged;
	}
	public void setIs_Physically_Challenged(String is_Physically_Challenged) {
		this.is_Physically_Challenged = is_Physically_Challenged;
	}
	public String getPhy_cha_type() {
		return phy_cha_type;
	}
	public void setPhy_cha_type(String phy_cha_type) {
		this.phy_cha_type = phy_cha_type;
	}
	public String getIs_Special_Child() {
		return is_Special_Child;
	}
	public void setIs_Special_Child(String is_Special_Child) {
		this.is_Special_Child = is_Special_Child;
	}
	public String getSpecialChild_type() {
		return specialChild_type;
	}
	public void setSpecialChild_type(String specialChild_type) {
		this.specialChild_type = specialChild_type;
	}
	public String getIs_disease() {
		return is_disease;
	}
	public void setIs_disease(String is_disease) {
		this.is_disease = is_disease;
	}
	public String getDisease_type() {
		return disease_type;
	}
	public void setDisease_type(String disease_type) {
		this.disease_type = disease_type;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getSclass() {
		return sclass;
	}
	public void setSclass(String sclass) {
		this.sclass = sclass;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getBplace() {
		return bplace;
	}
	public void setBplace(String bplace) {
		this.bplace = bplace;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getMtongue() {
		return mtongue;
	}
	public void setMtongue(String mtongue) {
		this.mtongue = mtongue;
	}
	public String getCgender() {
		return cgender;
	}
	public void setCgender(String cgender) {
		this.cgender = cgender;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCemail() {
		return cemail;
	}
	public void setCemail(String cemail) {
		this.cemail = cemail;
	}
	public String getSch_transport() {
		return sch_transport;
	}
	public void setSch_transport(String sch_transport) {
		this.sch_transport = sch_transport;
	}
	public String getTransport_board() {
		return transport_board;
	}
	public void setTransport_board(String transport_board) {
		this.transport_board = transport_board;
	}
	public String getHostel() {
		return hostel;
	}
	public void setHostel(String hostel) {
		this.hostel = hostel;
	}
	public String getHostel_allot() {
		return hostel_allot;
	}
	public void setHostel_allot(String hostel_allot) {
		this.hostel_allot = hostel_allot;
	}
	public String getHostel_name() {
		return Hostel_name;
	}
	public void setHostel_name(String hostel_name) {
		Hostel_name = hostel_name;
	}
	public String getHostel_Room_no() {
		return Hostel_Room_no;
	}
	public void setHostel_Room_no(String hostel_Room_no) {
		Hostel_Room_no = hostel_Room_no;
	}
	public String getPre_school1() {
		return pre_school1;
	}
	public void setPre_school1(String pre_school1) {
		this.pre_school1 = pre_school1;
	}
	public String getPre_qual1() {
		return pre_qual1;
	}
	public void setPre_qual1(String pre_qual1) {
		this.pre_qual1 = pre_qual1;
	}
	public String getPrd_from1() {
		return prd_from1;
	}
	public void setPrd_from1(String prd_from1) {
		this.prd_from1 = prd_from1;
	}
	public String getPrd_to1() {
		return prd_to1;
	}
	public void setPrd_to1(String prd_to1) {
		this.prd_to1 = prd_to1;
	}
	public String getPre_school2() {
		return pre_school2;
	}
	public void setPre_school2(String pre_school2) {
		this.pre_school2 = pre_school2;
	}
	public String getPre_qual2() {
		return pre_qual2;
	}
	public void setPre_qual2(String pre_qual2) {
		this.pre_qual2 = pre_qual2;
	}
	public String getPrd_from2() {
		return prd_from2;
	}
	public void setPrd_from2(String prd_from2) {
		this.prd_from2 = prd_from2;
	}
	public String getPrd_to2() {
		return prd_to2;
	}
	public void setPrd_to2(String prd_to2) {
		this.prd_to2 = prd_to2;
	}
	public String getPre_school3() {
		return pre_school3;
	}
	public void setPre_school3(String pre_school3) {
		this.pre_school3 = pre_school3;
	}
	public String getPre_qual3() {
		return pre_qual3;
	}
	public void setPre_qual3(String pre_qual3) {
		this.pre_qual3 = pre_qual3;
	}
	public String getPrd_from3() {
		return prd_from3;
	}
	public void setPrd_from3(String prd_from3) {
		this.prd_from3 = prd_from3;
	}
	public String getPrd_to3() {
		return prd_to3;
	}
	public void setPrd_to3(String prd_to3) {
		this.prd_to3 = prd_to3;
	}
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	public String getAdm_id() {
		return adm_id;
	}
	public void setAdm_id(String adm_id) {
		this.adm_id = adm_id;
	}
	public String getStudent_roll() {
		return student_roll;
	}
	public void setStudent_roll(String student_roll) {
		this.student_roll = student_roll;
	}
	public Date getJoin_date() {
		return Join_date;
	}
	public void setJoin_date(Date join_date) {
		Join_date = join_date;
	}
	public String getSchool_Name() {
		return School_Name;
	}
	public void setSchool_Name(String school_Name) {
		School_Name = school_Name;
	}
	public String getAcademic_year() {
		return Academic_year;
	}
	public void setAcademic_year(String academic_year) {
		Academic_year = academic_year;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getLtype() {
		return ltype;
	}
	public void setLtype(String ltype) {
		this.ltype = ltype;
	}
	public String getLpass() {
		return lpass;
	}
	public void setLpass(String lpass) {
		this.lpass = lpass;
	}
	public String getTran_typ() {
		return tran_typ;
	}
	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}
	public int getStucount() {
		return stucount;
	}
	public void setStucount(int stucount) {
		this.stucount = stucount;
	}
	public String getAmonth() {
		return amonth;
	}
	public void setAmonth(String amonth) {
		this.amonth = amonth;
	}
	public String getSlogo() {
		return slogo;
	}
	public void setSlogo(String slogo) {
		this.slogo = slogo;
	}
	public String getSheader() {
		return sheader;
	}
	public void setSheader(String sheader) {
		this.sheader = sheader;
	}
	public String getSfooter() {
		return sfooter;
	}
	public void setSfooter(String sfooter) {
		this.sfooter = sfooter;
	}
	public String getPass_change() {
		return pass_change;
	}
	public void setPass_change(String pass_change) {
		this.pass_change = pass_change;
	}
	public List<StuMedicalVO> getStuMedicalList() {
		return StuMedicalList;
	}
	public void setStuMedicalList(List<StuMedicalVO> stuMedicalList) {
		StuMedicalList = stuMedicalList;
	}
	public List<StuMedicalVO> getSuMedicalServiceVOList() {
		return SuMedicalServiceVOList;
	}
	public void setSuMedicalServiceVOList(List<StuMedicalVO> suMedicalServiceVOList) {
		SuMedicalServiceVOList = suMedicalServiceVOList;
	}
	
	
	
	
	
	
	
	
	
	
	
}
