package com.model;

import java.sql.Time;
import java.util.Date;
import java.util.List;

public class Nb_newsVO {
	private String news_tittle=null;
	private String news_context=null;
	private String n_student=null;
	private String n_parent=null;
	private String n_teacher=null;
	private String n_non_teach_staff=null;
	private Date dop=null;
	private String top=null;

	private String newsid=null;
	private String tran_typ=null;

	String isActive=null;
	String Inserted_By=null;
	Date Inserted_Date=null;
	String Updated_By=null;
	Date Updated_Date=null;
	
	//NoticeboardServiceVOList
	private List<Nb_newsVO> newsboardServiceVOList = null;
	
	
	
	public String getTran_typ() {
		return tran_typ;
	}
	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}
	public String getNewsid() {
		return newsid;
	}
	public void setNewsid(String newsid) {
		this.newsid = newsid;
	}
	public String getTop() {
		return top;
	}
	public void setTop(String top) {
		this.top = top;
	}
	public String getNews_tittle() {
		return news_tittle;
	}
	public void setNews_tittle(String news_tittle) {
		this.news_tittle = news_tittle;
	}
	public String getNews_context() {
		return news_context;
	}
	public void setNews_context(String news_context) {
		this.news_context = news_context;
	}
	public String getN_student() {
		return n_student;
	}
	public void setN_student(String n_student) {
		this.n_student = n_student;
	}
	public String getN_parent() {
		return n_parent;
	}
	public void setN_parent(String n_parent) {
		this.n_parent = n_parent;
	}
	public String getN_teacher() {
		return n_teacher;
	}
	public void setN_teacher(String n_teacher) {
		this.n_teacher = n_teacher;
	}
	public String getN_non_teach_staff() {
		return n_non_teach_staff;
	}
	public void setN_non_teach_staff(String n_non_teach_staff) {
		this.n_non_teach_staff = n_non_teach_staff;
	}
	
	public Date getDop() {
		return dop;
	}
	public void setDop(Date dop) {
		this.dop = dop;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	
	
	
	
	
	
	public String getInserted_By() {
		return Inserted_By;
	}
	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}
	public Date getInserted_Date() {
		return Inserted_Date;
	}
	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	public Date getUpdated_Date() {
		return Updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}
	public List<Nb_newsVO> getNewsboardServiceVOList() {
		return newsboardServiceVOList;
	}
	public void setNewsboardServiceVOList(List<Nb_newsVO> newsboardServiceVOList) {
		this.newsboardServiceVOList = newsboardServiceVOList;
	}
	
	
	
	
}
