package com.model;

import java.util.Date;
import java.util.List;

public class AbsenceRequestVO {

	private String theDate = null;
	private String Teacher_Id = null;
	private String Teacher_Name = null;
	private String Start_Date = null;
	private String End_Date = null;
	private String Absence_Name = null;
	private String Status=null; 
	private String Comments = null;
	private String isActive = null;
	private String Inserted_By = null;
	private String Inserted_Date = null;
	private String Updated_By = null;

	private String Updated_Date = null;
	private String time=null;




	private List<AbsenceRequestVO> absenceRequestServiceVOList = null;




	public String getTheDate() {
		return theDate;
	}




	public void setTheDate(String theDate) {
		this.theDate = theDate;
	}




	public String getTeacher_Id() {
		return Teacher_Id;
	}




	public void setTeacher_Id(String teacher_Id) {
		Teacher_Id = teacher_Id;
	}




	public String getTeacher_Name() {
		return Teacher_Name;
	}




	public void setTeacher_Name(String teacher_Name) {
		Teacher_Name = teacher_Name;
	}




	public String getStart_Date() {
		return Start_Date;
	}




	public void setStart_Date(String start_Date) {
		Start_Date = start_Date;
	}




	public String getEnd_Date() {
		return End_Date;
	}




	public void setEnd_Date(String end_Date) {
		End_Date = end_Date;
	}




	public String getAbsence_Name() {
		return Absence_Name;
	}




	public void setAbsence_Name(String absence_Name) {
		Absence_Name = absence_Name;
	}




	public String getStatus() {
		return Status;
	}




	public void setStatus(String status) {
		Status = status;
	}




	public String getComments() {
		return Comments;
	}




	public void setComments(String comments) {
		Comments = comments;
	}




	public String getIsActive() {
		return isActive;
	}




	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}




	public String getInserted_By() {
		return Inserted_By;
	}




	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}




	public String getInserted_Date() {
		return Inserted_Date;
	}




	public void setInserted_Date(String inserted_Date) {
		Inserted_Date = inserted_Date;
	}




	public String getUpdated_By() {
		return Updated_By;
	}




	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}




	public String getUpdated_Date() {
		return Updated_Date;
	}




	public void setUpdated_Date(String updated_Date) {
		Updated_Date = updated_Date;
	}




	public String getTime() {
		return time;
	}




	public void setTime(String time) {
		this.time = time;
	}




	public List<AbsenceRequestVO> getAbsenceRequestServiceVOList() {
		return absenceRequestServiceVOList;
	}




	public void setAbsenceRequestServiceVOList(List<AbsenceRequestVO> absenceRequestServiceVOList) {
		this.absenceRequestServiceVOList = absenceRequestServiceVOList;
	}

	

}
