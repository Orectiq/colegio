package com.model;

import java.util.Date;
import java.util.List;

public class AssignmentVO {

	
	
	private Date adate=null;
	private String aclass = null;
	private String asection = null;
	
	private String fname=null;
	
	private String ass_status=null;
	
	private String Mark=null;
	private String status=null;
	
	private String isActive=null;
	private String Inserted_By=null;
	private Date Inserted_Date=null;
	private String Updated_By=null;
	private Date Updated_Date=null;
	private List<AssignmentaddVO> assignmentServiceVOList = null;
	public Date getAdate() {
		return adate;
	}
	public void setAdate(Date adate) {
		this.adate = adate;
	}
	public String getAclass() {
		return aclass;
	}
	public void setAclass(String aclass) {
		this.aclass = aclass;
	}
	public String getAsection() {
		return asection;
	}
	public void setAsection(String asection) {
		this.asection = asection;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getAss_status() {
		return ass_status;
	}
	public void setAss_status(String ass_status) {
		this.ass_status = ass_status;
	}
	public String getMark() {
		return Mark;
	}
	public void setMark(String mark) {
		Mark = mark;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_By() {
		return Inserted_By;
	}
	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}
	public Date getInserted_Date() {
		return Inserted_Date;
	}
	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	public Date getUpdated_Date() {
		return Updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}
	public List<AssignmentaddVO> getAssignmentServiceVOList() {
		return assignmentServiceVOList;
	}
	public void setAssignmentServiceVOList(List<AssignmentaddVO> assignmentServiceVOList) {
		this.assignmentServiceVOList = assignmentServiceVOList;
	}
	
	
	
}
