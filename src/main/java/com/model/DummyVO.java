package com.model;

import java.util.Date;
import java.util.List;

public class DummyVO 
{
	private Date adate=null;
	private int count = 0;
	private String status = null;
	
	private List<DummyVO> dummyServiceVOList = null;

	public Date getAdate() {
		return adate;
	}

	public void setAdate(Date adate) {
		this.adate = adate;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<DummyVO> getDummyServiceVOList() {
		return dummyServiceVOList;
	}

	public void setDummyServiceVOList(List<DummyVO> dummyServiceVOList) {
		this.dummyServiceVOList = dummyServiceVOList;
	}
	
	
	

}
