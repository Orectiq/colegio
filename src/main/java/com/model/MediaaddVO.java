package com.model;

import java.util.Date;
import java.util.List;

public class MediaaddVO {

	String Media_Title=null;
	String Media_Description=null;
	String urlType=null;
	String urlPath=null;
	
	String isActive=null;
	String Inserted_By=null;
	Date Inserted_Date=null;
	String Updated_By=null;
	Date Updated_Date=null;
	private List<MediaaddVO> mediaServiceVOList = null;
	
	
	
	public String getMedia_Title() {
		return Media_Title;
	}
	public void setMedia_Title(String media_Title) {
		Media_Title = media_Title;
	}
	public String getMedia_Description() {
		return Media_Description;
	}
	public void setMedia_Description(String media_Description) {
		Media_Description = media_Description;
	}
	
	
	
	
	public String getUrlType() {
		return urlType;
	}
	public void setUrlType(String urlType) {
		this.urlType = urlType;
	}
	public String getUrlPath() {
		return urlPath;
	}
	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_By() {
		return Inserted_By;
	}
	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}
	public Date getInserted_Date() {
		return Inserted_Date;
	}
	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	public Date getUpdated_Date() {
		return Updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}
	public List<MediaaddVO> getMediaServiceVOList() {
		return mediaServiceVOList;
	}
	public void setMediaServiceVOList(List<MediaaddVO> mediaServiceVOList) {
		this.mediaServiceVOList = mediaServiceVOList;
	}
	
	
	
	
	
}
