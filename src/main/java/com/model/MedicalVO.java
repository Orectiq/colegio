package com.model;

import java.util.Date;
import java.util.List;

public class MedicalVO 
{
	private String student_id=null;
	private String blood_group  = null;
	private String height = null;
	private String weights = null;
	private String medical_condition = null;
	private String allergy = null;
	private String allergy_type = null;
	private String is_Physically_Challenged = null;
	private String phy_cha_type=null;
	
	private String is_Special_Child = null;
	private String specialChild_type = null;
	private String is_disease = null;
	private String disease_type = null;
	private String isActive = null;
	private String inserted_by = null;
	private Date inserted_date = null;
	private String updated_by = null;
	private Date updated_date = null;
	
	private List<MedicalVO> studentMedicalList=null;
	private List<MedicalVO> studentMedicalServiceVOList = null;
	
	
	public String getPhy_cha_type() {
		return phy_cha_type;
	}
	public void setPhy_cha_type(String phy_cha_type) {
		this.phy_cha_type = phy_cha_type;
	}
	public String getStudent_id() {
		return student_id;
	}
	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}
	public String getBlood_group() {
		return blood_group;
	}
	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}
	
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeights() {
		return weights;
	}
	public void setWeights(String weights) {
		this.weights = weights;
	}
	public String getMedical_condition() {
		return medical_condition;
	}
	public void setMedical_condition(String medical_condition) {
		this.medical_condition = medical_condition;
	}
	public String getAllergy() {
		return allergy;
	}
	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}
	public String getAllergy_type() {
		return allergy_type;
	}
	public void setAllergy_type(String allergy_type) {
		this.allergy_type = allergy_type;
	}
	public String getIs_Physically_Challenged() {
		return is_Physically_Challenged;
	}
	public void setIs_Physically_Challenged(String is_Physically_Challenged) {
		this.is_Physically_Challenged = is_Physically_Challenged;
	}
	public String getIs_Special_Child() {
		return is_Special_Child;
	}
	public void setIs_Special_Child(String is_Special_Child) {
		this.is_Special_Child = is_Special_Child;
	}
	public String getSpecialChild_type() {
		return specialChild_type;
	}
	public void setSpecialChild_type(String specialChild_type) {
		this.specialChild_type = specialChild_type;
	}
	public String getIs_disease() {
		return is_disease;
	}
	public void setIs_disease(String is_disease) {
		this.is_disease = is_disease;
	}
	public String getDisease_type() {
		return disease_type;
	}
	public void setDisease_type(String disease_type) {
		this.disease_type = disease_type;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_by() {
		return inserted_by;
	}
	public void setInserted_by(String inserted_by) {
		this.inserted_by = inserted_by;
	}
	public Date getInserted_date() {
		return inserted_date;
	}
	public void setInserted_date(Date inserted_date) {
		this.inserted_date = inserted_date;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public List<MedicalVO> getStudentMedicalList() {
		return studentMedicalList;
	}
	public void setStudentMedicalList(List<MedicalVO> studentMedicalList) {
		this.studentMedicalList = studentMedicalList;
	}
	public List<MedicalVO> getStudentMedicalServiceVOList() {
		return studentMedicalServiceVOList;
	}
	public void setStudentMedicalServiceVOList(List<MedicalVO> studentMedicalServiceVOList) {
		this.studentMedicalServiceVOList = studentMedicalServiceVOList;
	}

	
	
}
