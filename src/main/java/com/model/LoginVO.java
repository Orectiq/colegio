package com.model;

import java.util.List;

public class LoginVO 
{
	private String uname=null;
	private String ltype=null;
	private String lpass=null;
	private String sid=null; 
	private String photo=null;
	private String name=null;
	private String npass=null;
	private String sname=null;
	private String ayear=null;
	private String slogo=null;
	private String sheader=null;
	private String sfooter=null;
	
	private String address=null;
	private String email=null;
	private String mobile=null;
	
	private String logo_disp=null;
	private String language=null; 
	private String language_allowed=null;
	
	private String en=null;
	
	private int Cnt_Mod_Id=0;
	private String Cnt_Prifix=null;
	private int Cnt_Count=0;
	private String Cnt_Num=null;
	
	private String pass_change=null;
	private String confirm_pass=null;
	private String dbname=null;
	
	private List<LoginVO> loginServiceVOList = null;

	

	

	

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public String getConfirm_pass() {
		return confirm_pass;
	}

	public void setConfirm_pass(String confirm_pass) {
		this.confirm_pass = confirm_pass;
	}

	public String getPass_change() {
		return pass_change;
	}

	public void setPass_change(String pass_change) {
		this.pass_change = pass_change;
	}

	public int getCnt_Mod_Id() {
		return Cnt_Mod_Id;
	}

	public void setCnt_Mod_Id(int cnt_Mod_Id) {
		Cnt_Mod_Id = cnt_Mod_Id;
	}

	public String getCnt_Prifix() {
		return Cnt_Prifix;
	}

	public void setCnt_Prifix(String cnt_Prifix) {
		Cnt_Prifix = cnt_Prifix;
	}

	public int getCnt_Count() {
		return Cnt_Count;
	}

	public void setCnt_Count(int cnt_Count) {
		Cnt_Count = cnt_Count;
	}

	

	public String getCnt_Num() {
		return Cnt_Num;
	}

	public void setCnt_Num(String cnt_Num) {
		Cnt_Num = cnt_Num;
	}

	public String getEn() {
		return en;
	}

	public void setEn(String en) {
		this.en = en;
	}

	public String getLogo_disp() {
		return logo_disp;
	}

	public void setLogo_disp(String logo_disp) {
		this.logo_disp = logo_disp;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguage_allowed() {
		return language_allowed;
	}

	public void setLanguage_allowed(String language_allowed) {
		this.language_allowed = language_allowed;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSheader() {
		return sheader;
	}

	public void setSheader(String sheader) {
		this.sheader = sheader;
	}

	public String getSfooter() {
		return sfooter;
	}

	public void setSfooter(String sfooter) {
		this.sfooter = sfooter;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getAyear() {
		return ayear;
	}

	public void setAyear(String ayear) {
		this.ayear = ayear;
	}

	public String getSlogo() {
		return slogo;
	}

	public void setSlogo(String slogo) {
		this.slogo = slogo;
	}

	public String getNpass() {
		return npass;
	}

	public void setNpass(String npass) {
		this.npass = npass;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getLtype() {
		return ltype;
	}

	public void setLtype(String ltype) {
		this.ltype = ltype;
	}

	public String getLpass() {
		return lpass;
	}

	public void setLpass(String lpass) {
		this.lpass = lpass;
	}

	public List<LoginVO> getLoginServiceVOList() {
		return loginServiceVOList;
	}

	public void setLoginServiceVOList(List<LoginVO> loginServiceVOList) {
		this.loginServiceVOList = loginServiceVOList;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
