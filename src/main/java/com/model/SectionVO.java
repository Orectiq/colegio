package com.model;

import java.util.List;

public class SectionVO 
{
	String sclass=null;
	String section=null;
	
	int dele=0;
	String tran_type=null;
	
	private List<SectionVO> sectionServiceVOList = null;
	
	
	
	
	public int getDele() {
		return dele;
	}
	public void setDele(int dele) {
		this.dele = dele;
	}
	public String getTran_type() {
		return tran_type;
	}
	public void setTran_type(String tran_type) {
		this.tran_type = tran_type;
	}
	public String getSclass() {
		return sclass;
	}
	public void setSclass(String sclass) {
		this.sclass = sclass;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public List<SectionVO> getSectionServiceVOList() {
		return sectionServiceVOList;
	}
	public void setSectionServiceVOList(List<SectionVO> sectionServiceVOList) {
		this.sectionServiceVOList = sectionServiceVOList;
	}

	
	
}
