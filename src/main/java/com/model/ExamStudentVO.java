package com.model;

import java.util.Date;
import java.util.List;

public class ExamStudentVO
{
	//Student Table
	private String student_id=null;
	private String firstname = null;
	private String lastname = null;
	private String photo = null;
	private Date sdob = null;
	private String bplace=null;
	private String nationality=null;
	private String mtongue=null;
	private String cgender=null;
	private String religion=null;
	private String category=null;
	private String address1=null;
	private String address2=null;
	private String state=null;
	private int pin=0;
	private String mobile=null;
	private String phone=null;
	private String cemail=null;
	private String sch_transport=null;
	private String pre_school=null;
	private Date prd_from=null;
	private Date prd_to=null;
	private String parent_id=null;
	private String adm_id=null;
	private String student_roll=null;
	private Date Join_date=null;
	private String Hostel_name=null;
	private String Hostel_Room_no=null;
	private String School_Name=null;
	private String Academic_year=null;
	private String sclass=null;
	private String section=null;
	private String hostel=null;
	private String hostel_allot=null;
	private String transport_board=null;
	
	private String fname = null;
	private String lname = null;
	//private String photo = null;
	private Date dob = null;
	private String parentid = null;
	private String role=null;
	
	private List<ExamStudentVO> studentDetailList=null;
	private List<ExamStudentVO> studentServiceVOList = null;
	
	//Exam Table
	String Exam_Id=null;
	String Exam_Name=null;
	String Exam_Type=null;
	String E_Class=null;
	String Section=null;
	
	String Subject1=null;
	Date E_Date1=null;
	String E_Time1=null;
	String Subject2=null;
	Date E_Date2=null;
	String E_Time2=null;
	String Subject3=null;
	Date E_Date3=null;
	String E_Time3=null;
	String Subject4=null;
	Date E_Date4=null;
	String E_Time4=null;
	String Subject5=null;
	Date E_Date5=null;
	String E_Time5=null;
	String is_Active=null;
	String Inserted_By=null;
	Date Inserted_Date=null;
	String Updated_By=null;
	Date Updated_Date=null;
	
	private List<ExamStudentVO> examServiceVOList = null;

	
	
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStudent_id() {
		return student_id;
	}

	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Date getSdob() {
		return sdob;
	}

	public void setSdob(Date sdob) {
		this.sdob = sdob;
	}

	public String getBplace() {
		return bplace;
	}

	public void setBplace(String bplace) {
		this.bplace = bplace;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getMtongue() {
		return mtongue;
	}

	public void setMtongue(String mtongue) {
		this.mtongue = mtongue;
	}

	public String getCgender() {
		return cgender;
	}

	public void setCgender(String cgender) {
		this.cgender = cgender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCemail() {
		return cemail;
	}

	public void setCemail(String cemail) {
		this.cemail = cemail;
	}

	public String getSch_transport() {
		return sch_transport;
	}

	public void setSch_transport(String sch_transport) {
		this.sch_transport = sch_transport;
	}

	public String getPre_school() {
		return pre_school;
	}

	public void setPre_school(String pre_school) {
		this.pre_school = pre_school;
	}

	public Date getPrd_from() {
		return prd_from;
	}

	public void setPrd_from(Date prd_from) {
		this.prd_from = prd_from;
	}

	public Date getPrd_to() {
		return prd_to;
	}

	public void setPrd_to(Date prd_to) {
		this.prd_to = prd_to;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	public String getAdm_id() {
		return adm_id;
	}

	public void setAdm_id(String adm_id) {
		this.adm_id = adm_id;
	}

	public String getStudent_roll() {
		return student_roll;
	}

	public void setStudent_roll(String student_roll) {
		this.student_roll = student_roll;
	}

	public Date getJoin_date() {
		return Join_date;
	}

	public void setJoin_date(Date join_date) {
		Join_date = join_date;
	}

	public String getHostel_name() {
		return Hostel_name;
	}

	public void setHostel_name(String hostel_name) {
		Hostel_name = hostel_name;
	}

	public String getHostel_Room_no() {
		return Hostel_Room_no;
	}

	public void setHostel_Room_no(String hostel_Room_no) {
		Hostel_Room_no = hostel_Room_no;
	}

	public String getSchool_Name() {
		return School_Name;
	}

	public void setSchool_Name(String school_Name) {
		School_Name = school_Name;
	}

	public String getAcademic_year() {
		return Academic_year;
	}

	public void setAcademic_year(String academic_year) {
		Academic_year = academic_year;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	

	public String getHostel() {
		return hostel;
	}

	public void setHostel(String hostel) {
		this.hostel = hostel;
	}

	public String getHostel_allot() {
		return hostel_allot;
	}

	public void setHostel_allot(String hostel_allot) {
		this.hostel_allot = hostel_allot;
	}

	public String getTransport_board() {
		return transport_board;
	}

	public void setTransport_board(String transport_board) {
		this.transport_board = transport_board;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public List<ExamStudentVO> getStudentDetailList() {
		return studentDetailList;
	}

	public void setStudentDetailList(List<ExamStudentVO> studentDetailList) {
		this.studentDetailList = studentDetailList;
	}

	public List<ExamStudentVO> getStudentServiceVOList() {
		return studentServiceVOList;
	}

	public void setStudentServiceVOList(List<ExamStudentVO> studentServiceVOList) {
		this.studentServiceVOList = studentServiceVOList;
	}

	

	

	public String getExam_Id() {
		return Exam_Id;
	}

	public void setExam_Id(String exam_Id) {
		Exam_Id = exam_Id;
	}

	public String getExam_Name() {
		return Exam_Name;
	}

	public void setExam_Name(String exam_Name) {
		Exam_Name = exam_Name;
	}

	public String getExam_Type() {
		return Exam_Type;
	}

	public void setExam_Type(String exam_Type) {
		Exam_Type = exam_Type;
	}

	public String getE_Class() {
		return E_Class;
	}

	public void setE_Class(String e_Class) {
		E_Class = e_Class;
	}

	public String getSubject1() {
		return Subject1;
	}

	public void setSubject1(String subject1) {
		Subject1 = subject1;
	}

	public Date getE_Date1() {
		return E_Date1;
	}

	public void setE_Date1(Date e_Date1) {
		E_Date1 = e_Date1;
	}

	public String getE_Time1() {
		return E_Time1;
	}

	public void setE_Time1(String e_Time1) {
		E_Time1 = e_Time1;
	}

	public String getSubject2() {
		return Subject2;
	}

	public void setSubject2(String subject2) {
		Subject2 = subject2;
	}

	public Date getE_Date2() {
		return E_Date2;
	}

	public void setE_Date2(Date e_Date2) {
		E_Date2 = e_Date2;
	}

	public String getE_Time2() {
		return E_Time2;
	}

	public void setE_Time2(String e_Time2) {
		E_Time2 = e_Time2;
	}

	public String getSubject3() {
		return Subject3;
	}

	public void setSubject3(String subject3) {
		Subject3 = subject3;
	}

	public Date getE_Date3() {
		return E_Date3;
	}

	public void setE_Date3(Date e_Date3) {
		E_Date3 = e_Date3;
	}

	public String getE_Time3() {
		return E_Time3;
	}

	public void setE_Time3(String e_Time3) {
		E_Time3 = e_Time3;
	}

	public String getSubject4() {
		return Subject4;
	}

	public void setSubject4(String subject4) {
		Subject4 = subject4;
	}

	public Date getE_Date4() {
		return E_Date4;
	}

	public void setE_Date4(Date e_Date4) {
		E_Date4 = e_Date4;
	}

	public String getE_Time4() {
		return E_Time4;
	}

	public void setE_Time4(String e_Time4) {
		E_Time4 = e_Time4;
	}

	public String getSubject5() {
		return Subject5;
	}

	public void setSubject5(String subject5) {
		Subject5 = subject5;
	}

	public Date getE_Date5() {
		return E_Date5;
	}

	public void setE_Date5(Date e_Date5) {
		E_Date5 = e_Date5;
	}

	public String getE_Time5() {
		return E_Time5;
	}

	public void setE_Time5(String e_Time5) {
		E_Time5 = e_Time5;
	}

	public String getIs_Active() {
		return is_Active;
	}

	public void setIs_Active(String is_Active) {
		this.is_Active = is_Active;
	}

	public String getInserted_By() {
		return Inserted_By;
	}

	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}

	public Date getInserted_Date() {
		return Inserted_Date;
	}

	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}

	public String getUpdated_By() {
		return Updated_By;
	}

	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}

	public Date getUpdated_Date() {
		return Updated_Date;
	}

	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}

	public List<ExamStudentVO> getExamServiceVOList() {
		return examServiceVOList;
	}

	public void setExamServiceVOList(List<ExamStudentVO> examServiceVOList) {
		this.examServiceVOList = examServiceVOList;
	}

	
	
	
	
	
	
}
