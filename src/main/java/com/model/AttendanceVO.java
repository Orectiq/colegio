package com.model;

import java.util.Date;
import java.sql.Blob;
import java.util.Date;
import java.util.List;

public class AttendanceVO
{
	private Date adate=null;
	private String aclass = null;
	private String asection = null;
	private String sid = null;
	private String fname=null;
	private String lname=null;
	private String att_status=null;
	private String att_time=null;
	private String status=null;
	private String isActive=null;
	private String Inserted_By=null;
	private Date Inserted_Date=null;
	private String Updated_By=null;
	private Date Updated_Date=null;
	private String acount=null;
	private String amonth=null;
	
	private List<AttendanceVO> attendanceServiceVOList = null;

	

	public String getAmonth() {
		return amonth;
	}

	public void setAmonth(String amonth) {
		this.amonth = amonth;
	}

	

	

	public String getAcount() {
		return acount;
	}

	public void setAcount(String acount) {
		this.acount = acount;
	}

	public Date getAdate() {
		return adate;
	}

	public void setAdate(Date adate) {
		this.adate = adate;
	}

	public String getAclass() {
		return aclass;
	}

	public void setAclass(String aclass) {
		this.aclass = aclass;
	}

	public String getAsection() {
		return asection;
	}

	public void setAsection(String asection) {
		this.asection = asection;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getAtt_status() {
		return att_status;
	}

	public void setAtt_status(String att_status) {
		this.att_status = att_status;
	}

	public String getAtt_time() {
		return att_time;
	}

	public void setAtt_time(String att_time) {
		this.att_time = att_time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getInserted_By() {
		return Inserted_By;
	}

	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}

	public Date getInserted_Date() {
		return Inserted_Date;
	}

	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}

	public String getUpdated_By() {
		return Updated_By;
	}

	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}

	public Date getUpdated_Date() {
		return Updated_Date;
	}

	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}

	public List<AttendanceVO> getAttendanceServiceVOList() {
		return attendanceServiceVOList;
	}

	public void setAttendanceServiceVOList(List<AttendanceVO> attendanceServiceVOList) {
		this.attendanceServiceVOList = attendanceServiceVOList;
	}
	
	
	
	
}
