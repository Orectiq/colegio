package com.model;

import java.util.List;

public class DormitoryAllotVO 
{
	private String dormitory_name=null;
	private String location = null;
	private String student_id = null;
	private String student_name = null;
	private String gender=null;
	private String room_number = null;
	
	private List<DormitoryAllotVO> dormitoryAllotServiceVOList = null;

	public String getDormitory_name() {
		return dormitory_name;
	}

	public void setDormitory_name(String dormitory_name) {
		this.dormitory_name = dormitory_name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStudent_id() {
		return student_id;
	}

	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}

	public String getStudent_name() {
		return student_name;
	}

	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getRoom_number() {
		return room_number;
	}

	public void setRoom_number(String room_number) {
		this.room_number = room_number;
	}

	public List<DormitoryAllotVO> getDormitoryAllotServiceVOList() {
		return dormitoryAllotServiceVOList;
	}

	public void setDormitoryAllotServiceVOList(List<DormitoryAllotVO> dormitoryAllotServiceVOList) {
		this.dormitoryAllotServiceVOList = dormitoryAllotServiceVOList;
	}
	
	
	
}
