package com.model;

import java.util.Date;
import java.util.List;

public class MailVO
{
	private String mail_from=null;
	private String mail_to=null;
	private String subj=null;
	private String desc=null; 
	private String message=null;
	private String attach=null;
	private String mdate=null;
	private String mtime=null;
	
	
	private List<MailVO> mailServiceVOList = null;


	public String getMail_from() {
		return mail_from;
	}


	public void setMail_from(String mail_from) {
		this.mail_from = mail_from;
	}


	public String getMail_to() {
		return mail_to;
	}


	public void setMail_to(String mail_to) {
		this.mail_to = mail_to;
	}


	public String getSubj() {
		return subj;
	}


	public void setSubj(String subj) {
		this.subj = subj;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getAttach() {
		return attach;
	}


	public void setAttach(String attach) {
		this.attach = attach;
	}


	


	


	public String getMdate() {
		return mdate;
	}


	public void setMdate(String mdate) {
		this.mdate = mdate;
	}


	public String getMtime() {
		return mtime;
	}


	public void setMtime(String mtime) {
		this.mtime = mtime;
	}


	public List<MailVO> getMailServiceVOList() {
		return mailServiceVOList;
	}


	public void setMailServiceVOList(List<MailVO> mailServiceVOList) {
		this.mailServiceVOList = mailServiceVOList;
	}
	
	
	
}
