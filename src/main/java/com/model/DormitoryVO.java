package com.model;

import java.util.Date;
import java.util.List;

public class DormitoryVO {

	private String Dormitory_name=null;
	private String Location = null;
	private String fee = null;
	private String cgender = null;
	private String description = null;
	private int No_of_beds = 0;
	private String Warden_Photo = null;
	private String name = null;
	private int age=0;
	private String Contact_Number = null;
	private String Alt_Contact_Number = null;
	private String address = null;
	private String DeputyWarden_Photo = null;
	private String name1 = null;
	private int age1 = 0;
	private String Contact_Number1 = null;
	private String Alt_Contact_Number1 = null;
	private String address1 = null;
	private String isActive = null;
	private String Inserted_By = null;
	private Date Inserted_Date = null;
	private String Updated_By = null;
	private Date Updated_Date = null;
	private int allot_beds=0;
	private int remain_beds=0;
	
	private String tran_typ=null;
	
	private List<DormitoryVO> dormitoryServiceVOList = null;
	
	
	
	public String getTran_typ() {
		return tran_typ;
	}
	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}
	public String getDormitory_name() {
		return Dormitory_name;
	}
	public void setDormitory_name(String dormitory_name) {
		Dormitory_name = dormitory_name;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public String getCgender() {
		return cgender;
	}
	public void setCgender(String cgender) {
		this.cgender = cgender;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	public int getNo_of_beds() {
		return No_of_beds;
	}
	public void setNo_of_beds(int no_of_beds) {
		No_of_beds = no_of_beds;
	}
	public String getWarden_Photo() {
		return Warden_Photo;
	}
	public void setWarden_Photo(String warden_Photo) {
		Warden_Photo = warden_Photo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getContact_Number() {
		return Contact_Number;
	}
	public void setContact_Number(String contact_Number) {
		Contact_Number = contact_Number;
	}
	public String getAlt_Contact_Number() {
		return Alt_Contact_Number;
	}
	public void setAlt_Contact_Number(String alt_Contact_Number) {
		Alt_Contact_Number = alt_Contact_Number;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDeputyWarden_Photo() {
		return DeputyWarden_Photo;
	}
	public void setDeputyWarden_Photo(String deputyWarden_Photo) {
		DeputyWarden_Photo = deputyWarden_Photo;
	}
	public String getName1() {
		return name1;
	}
	public void setName1(String name1) {
		this.name1 = name1;
	}
	public int getAge1() {
		return age1;
	}
	public void setAge1(int age1) {
		this.age1 = age1;
	}
	public String getContact_Number1() {
		return Contact_Number1;
	}
	public void setContact_Number1(String contact_Number1) {
		Contact_Number1 = contact_Number1;
	}
	public String getAlt_Contact_Number1() {
		return Alt_Contact_Number1;
	}
	public void setAlt_Contact_Number1(String alt_Contact_Number1) {
		Alt_Contact_Number1 = alt_Contact_Number1;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_By() {
		return Inserted_By;
	}
	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}
	
	
	
	public Date getInserted_Date() {
		return Inserted_Date;
	}
	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	public Date getUpdated_Date() {
		return Updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}
	public List<DormitoryVO> getDormitoryServiceVOList() {
		return dormitoryServiceVOList;
	}
	public void setDormitoryServiceVOList(List<DormitoryVO> dormitoryServiceVOList) {
		this.dormitoryServiceVOList = dormitoryServiceVOList;
	}
	public int getAllot_beds() {
		return allot_beds;
	}
	public void setAllot_beds(int allot_beds) {
		this.allot_beds = allot_beds;
	}
	public int getRemain_beds() {
		return remain_beds;
	}
	public void setRemain_beds(int remain_beds) {
		this.remain_beds = remain_beds;
	}
	
	
	

	
	
	
}
