package com.model;

import java.util.List;

public class SubjectAssignVO
{
	String tid=null;
	String tname=null;
	String sclass=null;
	String section=null;
	String ttype=null;
	String subject_handle=null;
	String mobile=null;
	String tran_type=null;
	private List<SubjectAssignVO> subjectAssignServiceVOList = null;

	public String getTtype() {
		return ttype;
	}

	public void setTtype(String ttype) {
		this.ttype = ttype;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getTname() {
		return tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSubject_handle() {
		return subject_handle;
	}

	public void setSubject_handle(String subject_handle) {
		this.subject_handle = subject_handle;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getTran_type() {
		return tran_type;
	}

	public void setTran_type(String tran_type) {
		this.tran_type = tran_type;
	}

	public List<SubjectAssignVO> getSubjectAssignServiceVOList() {
		return subjectAssignServiceVOList;
	}

	public void setSubjectAssignServiceVOList(List<SubjectAssignVO> subjectAssignServiceVOList) {
		this.subjectAssignServiceVOList = subjectAssignServiceVOList;
	}
	
	
	
	
	

}
