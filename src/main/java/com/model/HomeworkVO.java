package com.model;

import java.util.Date;
import java.util.List;

public class HomeworkVO {

	//date and calendar
	private Date theDate=null;
	
	//class and section
	private String sClass1 = null;
	private String Section1 = null;	
	
	//TeacherName and TeacherId
	private String Teacher_Name = null;
	private String Teacher_Id = null;	
	
	private String ttype=null;
	private String mobile=null;
	
	//#1.Period,Subject,Homework
	private String Subject = null;
	private String HomeWork = null;
	
	
	//Default ID
	private String isActive = null;
	private String Inserted_By = null;
	private Date Inserted_Date = null;
	private String Updated_By = null;
	private Date Updated_Date = null;
	
	//List View	
	private List<HomeworkVO> homeworkServiceVOList = null;

	public Date getTheDate() {
		return theDate;
	}

	public void setTheDate(Date theDate) {
		this.theDate = theDate;
	}

	public String getsClass1() {
		return sClass1;
	}

	public void setsClass1(String sClass1) {
		this.sClass1 = sClass1;
	}

	public String getSection1() {
		return Section1;
	}

	public void setSection1(String section1) {
		Section1 = section1;
	}

	public String getTeacher_Name() {
		return Teacher_Name;
	}

	public void setTeacher_Name(String teacher_Name) {
		Teacher_Name = teacher_Name;
	}

	public String getTeacher_Id() {
		return Teacher_Id;
	}

	public void setTeacher_Id(String teacher_Id) {
		Teacher_Id = teacher_Id;
	}

	public String getTtype() {
		return ttype;
	}

	public void setTtype(String ttype) {
		this.ttype = ttype;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSubject() {
		return Subject;
	}

	public void setSubject(String subject) {
		Subject = subject;
	}

	public String getHomeWork() {
		return HomeWork;
	}

	public void setHomeWork(String homeWork) {
		HomeWork = homeWork;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getInserted_By() {
		return Inserted_By;
	}

	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}

	public Date getInserted_Date() {
		return Inserted_Date;
	}

	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}

	public String getUpdated_By() {
		return Updated_By;
	}

	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}

	public Date getUpdated_Date() {
		return Updated_Date;
	}

	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}

	public List<HomeworkVO> getHomeworkServiceVOList() {
		return homeworkServiceVOList;
	}

	public void setHomeworkServiceVOList(List<HomeworkVO> homeworkServiceVOList) {
		this.homeworkServiceVOList = homeworkServiceVOList;
	}

	
	
	
	
}
