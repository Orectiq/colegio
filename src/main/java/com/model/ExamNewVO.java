package com.model;

import java.util.Date;
import java.util.List;

public class ExamNewVO
{
	String exam_id=null;
	String exam_name=null;
	String exam_type=null;
	String e_class=null;
	String section=null;
	
	String subject=null;
	String e_date=null;
	String e_time=null;
	String e_mark=null;
	String tran_typ=null;
	
	private List<ExamNewVO> examNewServiceVOList = null;

	
	
	public String getTran_typ() {
		return tran_typ;
	}

	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}

	public String getExam_id() {
		return exam_id;
	}

	public void setExam_id(String exam_id) {
		this.exam_id = exam_id;
	}

	public String getExam_name() {
		return exam_name;
	}

	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}

	public String getExam_type() {
		return exam_type;
	}

	public void setExam_type(String exam_type) {
		this.exam_type = exam_type;
	}

	public String getE_class() {
		return e_class;
	}

	public void setE_class(String e_class) {
		this.e_class = e_class;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	

	public String getE_date() {
		return e_date;
	}

	public void setE_date(String e_date) {
		this.e_date = e_date;
	}

	public String getE_time() {
		return e_time;
	}

	public void setE_time(String e_time) {
		this.e_time = e_time;
	}

	public String getE_mark() {
		return e_mark;
	}

	public void setE_mark(String e_mark) {
		this.e_mark = e_mark;
	}

	public List<ExamNewVO> getExamNewServiceVOList() {
		return examNewServiceVOList;
	}

	public void setExamNewServiceVOList(List<ExamNewVO> examNewServiceVOList) {
		this.examNewServiceVOList = examNewServiceVOList;
	}

	
	
	

}
