package com.model;

import java.util.Date;
import java.util.List;

public class MarksentryallVO 
{
	private String exam_id = null;
	private String exam_name = null;
	private String exam_type = null;
	private String sclass=null;
	private String ssec=null;
	private String student_id=null;
	private String student_name=null;
	
	private String isActive=null;
	private String Inserted_By=null;
	private Date Inserted_Date=null;
	private String Updated_By=null;
	private Date Updated_Date=null;
	
	private String subject=null;
	private int marks=0;
	private int mark=0;
	
	private List<MarksentryallVO> marksAllServiceVOList = null;

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public String getExam_id() {
		return exam_id;
	}

	public void setExam_id(String exam_id) {
		this.exam_id = exam_id;
	}

	public String getExam_name() {
		return exam_name;
	}

	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}

	public String getExam_type() {
		return exam_type;
	}

	public void setExam_type(String exam_type) {
		this.exam_type = exam_type;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getSsec() {
		return ssec;
	}

	public void setSsec(String ssec) {
		this.ssec = ssec;
	}

	public String getStudent_id() {
		return student_id;
	}

	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}

	public String getStudent_name() {
		return student_name;
	}

	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getInserted_By() {
		return Inserted_By;
	}

	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}

	public Date getInserted_Date() {
		return Inserted_Date;
	}

	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}

	public String getUpdated_By() {
		return Updated_By;
	}

	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}

	public Date getUpdated_Date() {
		return Updated_Date;
	}

	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public List<MarksentryallVO> getMarksAllServiceVOList() {
		return marksAllServiceVOList;
	}

	public void setMarksAllServiceVOList(List<MarksentryallVO> marksAllServiceVOList) {
		this.marksAllServiceVOList = marksAllServiceVOList;
	}

	
	
	
	
}
