package com.model;

import java.util.Date;
import java.util.List;

public class ParentVO 
{
	private String parent_id=null;
	private String prt_fname  = null;
	
	private String prt_father_dob = null;
	
	private String prt_occupa = null;
	private String prt_design = null;
	private String prt_off_add = null;
	private String prt_off_phn = null;
	
	private String prt_mname = null;
	private String prt_mother_dob = null;
	private String mother_occupa = null;
	private String mother_design = null;
	private String mother_off_add = null;
	private String mother_off_phn = null;
	
	private String prt_mobile = null;
	private String prt_email = null;
	private String prt_photo = null;
	private String prt_gname = null;
	private String prt_grd_occupa = null;
	private String prt_grd_design = null;
	private String prt_grd_off_add = null;
	private String prt_grd_home_add = null;
	private String prt_grd_mobile = null;
	private String prt_grd_email = null;
	private String prt_grd_relation = null;
	private String isActive = null;
	private String inserted_by = null;
	private String inserted_date = null;
	private String updated_by = null;
	private String updated_date = null;
	
	private String tran_typ=null;
	
	private String School_Name=null;
	private String Academic_year=null;
	private String slogo=null;
	
	private String ltype=null;
	private String lpass=null;
	private String role=null;
	
	private int parcount=0;
	 private String sheader=null;
	 private String sfooter=null;
	 private String pass_change=null;
	private List<ParentVO> parentServiceVOList = null;
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	public String getPrt_fname() {
		return prt_fname;
	}
	public void setPrt_fname(String prt_fname) {
		this.prt_fname = prt_fname;
	}
	public String getPrt_father_dob() {
		return prt_father_dob;
	}
	public void setPrt_father_dob(String prt_father_dob) {
		this.prt_father_dob = prt_father_dob;
	}
	public String getPrt_occupa() {
		return prt_occupa;
	}
	public void setPrt_occupa(String prt_occupa) {
		this.prt_occupa = prt_occupa;
	}
	public String getPrt_design() {
		return prt_design;
	}
	public void setPrt_design(String prt_design) {
		this.prt_design = prt_design;
	}
	public String getPrt_off_add() {
		return prt_off_add;
	}
	public void setPrt_off_add(String prt_off_add) {
		this.prt_off_add = prt_off_add;
	}
	public String getPrt_off_phn() {
		return prt_off_phn;
	}
	public void setPrt_off_phn(String prt_off_phn) {
		this.prt_off_phn = prt_off_phn;
	}
	public String getPrt_mname() {
		return prt_mname;
	}
	public void setPrt_mname(String prt_mname) {
		this.prt_mname = prt_mname;
	}
	public String getPrt_mother_dob() {
		return prt_mother_dob;
	}
	public void setPrt_mother_dob(String prt_mother_dob) {
		this.prt_mother_dob = prt_mother_dob;
	}
	public String getMother_occupa() {
		return mother_occupa;
	}
	public void setMother_occupa(String mother_occupa) {
		this.mother_occupa = mother_occupa;
	}
	public String getMother_design() {
		return mother_design;
	}
	public void setMother_design(String mother_design) {
		this.mother_design = mother_design;
	}
	public String getMother_off_add() {
		return mother_off_add;
	}
	public void setMother_off_add(String mother_off_add) {
		this.mother_off_add = mother_off_add;
	}
	public String getMother_off_phn() {
		return mother_off_phn;
	}
	public void setMother_off_phn(String mother_off_phn) {
		this.mother_off_phn = mother_off_phn;
	}
	public String getPrt_mobile() {
		return prt_mobile;
	}
	public void setPrt_mobile(String prt_mobile) {
		this.prt_mobile = prt_mobile;
	}
	public String getPrt_email() {
		return prt_email;
	}
	public void setPrt_email(String prt_email) {
		this.prt_email = prt_email;
	}
	public String getPrt_photo() {
		return prt_photo;
	}
	public void setPrt_photo(String prt_photo) {
		this.prt_photo = prt_photo;
	}
	public String getPrt_gname() {
		return prt_gname;
	}
	public void setPrt_gname(String prt_gname) {
		this.prt_gname = prt_gname;
	}
	public String getPrt_grd_occupa() {
		return prt_grd_occupa;
	}
	public void setPrt_grd_occupa(String prt_grd_occupa) {
		this.prt_grd_occupa = prt_grd_occupa;
	}
	public String getPrt_grd_design() {
		return prt_grd_design;
	}
	public void setPrt_grd_design(String prt_grd_design) {
		this.prt_grd_design = prt_grd_design;
	}
	public String getPrt_grd_off_add() {
		return prt_grd_off_add;
	}
	public void setPrt_grd_off_add(String prt_grd_off_add) {
		this.prt_grd_off_add = prt_grd_off_add;
	}
	public String getPrt_grd_home_add() {
		return prt_grd_home_add;
	}
	public void setPrt_grd_home_add(String prt_grd_home_add) {
		this.prt_grd_home_add = prt_grd_home_add;
	}
	public String getPrt_grd_mobile() {
		return prt_grd_mobile;
	}
	public void setPrt_grd_mobile(String prt_grd_mobile) {
		this.prt_grd_mobile = prt_grd_mobile;
	}
	public String getPrt_grd_email() {
		return prt_grd_email;
	}
	public void setPrt_grd_email(String prt_grd_email) {
		this.prt_grd_email = prt_grd_email;
	}
	public String getPrt_grd_relation() {
		return prt_grd_relation;
	}
	public void setPrt_grd_relation(String prt_grd_relation) {
		this.prt_grd_relation = prt_grd_relation;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_by() {
		return inserted_by;
	}
	public void setInserted_by(String inserted_by) {
		this.inserted_by = inserted_by;
	}
	public String getInserted_date() {
		return inserted_date;
	}
	public void setInserted_date(String inserted_date) {
		this.inserted_date = inserted_date;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public String getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
	public String getTran_typ() {
		return tran_typ;
	}
	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}
	public String getSchool_Name() {
		return School_Name;
	}
	public void setSchool_Name(String school_Name) {
		School_Name = school_Name;
	}
	public String getAcademic_year() {
		return Academic_year;
	}
	public void setAcademic_year(String academic_year) {
		Academic_year = academic_year;
	}
	public String getSlogo() {
		return slogo;
	}
	public void setSlogo(String slogo) {
		this.slogo = slogo;
	}
	public String getLtype() {
		return ltype;
	}
	public void setLtype(String ltype) {
		this.ltype = ltype;
	}
	public String getLpass() {
		return lpass;
	}
	public void setLpass(String lpass) {
		this.lpass = lpass;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getParcount() {
		return parcount;
	}
	public void setParcount(int parcount) {
		this.parcount = parcount;
	}
	public String getSheader() {
		return sheader;
	}
	public void setSheader(String sheader) {
		this.sheader = sheader;
	}
	public String getSfooter() {
		return sfooter;
	}
	public void setSfooter(String sfooter) {
		this.sfooter = sfooter;
	}
	public String getPass_change() {
		return pass_change;
	}
	public void setPass_change(String pass_change) {
		this.pass_change = pass_change;
	}
	public List<ParentVO> getParentServiceVOList() {
		return parentServiceVOList;
	}
	public void setParentServiceVOList(List<ParentVO> parentServiceVOList) {
		this.parentServiceVOList = parentServiceVOList;
	}
	
	
	
	
	
	
	
}
