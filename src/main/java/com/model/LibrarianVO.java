package com.model;

import java.util.List;

public class LibrarianVO
{
	String lib_id=null;
	String lib_name=null;
	private List<LibrarianVO> librarianServiceVOList = null;
	
	
	public String getLib_id() {
		return lib_id;
	}
	public void setLib_id(String lib_id) {
		this.lib_id = lib_id;
	}
	public String getLib_name() {
		return lib_name;
	}
	public void setLib_name(String lib_name) {
		this.lib_name = lib_name;
	}
	public List<LibrarianVO> getLibrarianServiceVOList() {
		return librarianServiceVOList;
	}
	public void setLibrarianServiceVOList(List<LibrarianVO> librarianServiceVOList) {
		this.librarianServiceVOList = librarianServiceVOList;
	}
	
	
	
}
