package com.model;

import java.util.Date;
import java.util.List;

public class AssignmentaddVO {

	String Sclass=null;
	String Subject=null;
	String Section=null;
	String Title=null;
	String Description=null;
	String Due_Date=null;
	String Mark=null;
	String HDate=null;
	String isActive=null;
	String Inserted_By=null;
	String Inserted_Date=null;
	String Updated_By=null;
	String Updated_Date=null;
	String Tid=null;
	String fname=null;
	String ttype=null;
	String assignid=null;
	
	private List<AssignmentaddVO> assignmentServiceVOList = null;

	public String getAssignid() {
		return assignid;
	}

	public void setAssignid(String assignid) {
		this.assignid = assignid;
	}

	public String getDue_Date() {
		return Due_Date;
	}

	public void setDue_Date(String due_Date) {
		Due_Date = due_Date;
	}

	public String getHDate() {
		return HDate;
	}

	public void setHDate(String hDate) {
		HDate = hDate;
	}

	public String getInserted_Date() {
		return Inserted_Date;
	}

	public void setInserted_Date(String inserted_Date) {
		Inserted_Date = inserted_Date;
	}

	public String getUpdated_Date() {
		return Updated_Date;
	}

	public void setUpdated_Date(String updated_Date) {
		Updated_Date = updated_Date;
	}

	public String getSclass() {
		return Sclass;
	}

	public void setSclass(String sclass) {
		Sclass = sclass;
	}

	public String getSubject() {
		return Subject;
	}

	public void setSubject(String subject) {
		Subject = subject;
	}

	public String getSection() {
		return Section;
	}

	public void setSection(String section) {
		Section = section;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	

	public String getMark() {
		return Mark;
	}

	public void setMark(String mark) {
		Mark = mark;
	}

	

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getInserted_By() {
		return Inserted_By;
	}

	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}

	

	public String getUpdated_By() {
		return Updated_By;
	}

	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}

	

	public List<AssignmentaddVO> getAssignmentServiceVOList() {
		return assignmentServiceVOList;
	}

	public void setAssignmentServiceVOList(List<AssignmentaddVO> assignmentServiceVOList) {
		this.assignmentServiceVOList = assignmentServiceVOList;
	}

	public String getTid() {
		return Tid;
	}

	public void setTid(String tid) {
		Tid = tid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getTtype() {
		return ttype;
	}

	public void setTtype(String ttype) {
		this.ttype = ttype;
	}

	
	
	
	
	
}
