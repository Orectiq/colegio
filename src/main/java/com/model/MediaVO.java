package com.model;

import java.util.Date;
import java.util.List;

public class MediaVO {

	String Album_Title=null;
	String Album_Description=null;
	String Album_Image=null;
	String Share_Student=null;
	String Share_Teacher=null;
	String Share_NonStaff=null;
	String isActive=null;
	String Inserted_By=null;
	Date Inserted_Date=null;
	String Updated_By=null;
	Date Updated_Date=null;
	
	private List<MediaVO> albumServiceVOList = null;
	
	public String getAlbum_Title() {
		return Album_Title;
	}
	public void setAlbum_Title(String album_Title) {
		Album_Title = album_Title;
	}
	public String getAlbum_Description() {
		return Album_Description;
	}
	public void setAlbum_Description(String album_Description) {
		Album_Description = album_Description;
	}
	public String getAlbum_Image() {
		return Album_Image;
	}
	public void setAlbum_Image(String album_Image) {
		Album_Image = album_Image;
	}
	public String getShare_Student() {
		return Share_Student;
	}
	public void setShare_Student(String share_Student) {
		Share_Student = share_Student;
	}
	public String getShare_Teacher() {
		return Share_Teacher;
	}
	public void setShare_Teacher(String share_Teacher) {
		Share_Teacher = share_Teacher;
	}
	
	
	
	public String getShare_NonStaff() {
		return Share_NonStaff;
	}
	public void setShare_NonStaff(String share_NonStaff) {
		Share_NonStaff = share_NonStaff;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getInserted_By() {
		return Inserted_By;
	}
	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}
	public Date getInserted_Date() {
		return Inserted_Date;
	}
	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	public Date getUpdated_Date() {
		return Updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}
	public List<MediaVO> getAlbumServiceVOList() {
		return albumServiceVOList;
	}
	public void setAlbumServiceVOList(List<MediaVO> albumServiceVOList) {
		this.albumServiceVOList = albumServiceVOList;
	}
	
	
	
	
	
}
