package com.model;

import java.util.Date;
import java.util.List;

public class NonteachVO {
  

	private String Staff_ID=null;
	private String Department  = null;
	private String Name=null;
	private String Age = null;
	private String DOB = null;
	private String Gender = null;
	private String Phone_Number = null;
	private String Contact_Number = null;
	private String Email = null;
	private String DOJ = null;
	private String Work_Area = null;
	private String Current_Address = null;
	private String Permanent_Address = null;
	
	
	private String isActive=null;
	private String Inserted_By=null;
	private String Inserted_Date=null;
	private String Updated_By=null;
	private String Updated_Date=null;
	private String ltype=null;
	private String lpass=null;
	private String photo=null;
	
	 private String slogo=null;
	 private String sname=null;
	 private String ayear=null;
	 private String sheader=null;
	 private String sfooter=null;
	
	 private String toAddr=null;
	 private String frmAddr=null;
	 private String subj=null;
	 private String body=null;
	 private String pass_change=null;
	 private String tran_typ=null;
	private List<NonteachVO> nonteachServiceVOList = null;

	
	
	
	
	
	public String getTran_typ() {
		return tran_typ;
	}
	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}
	public String getPass_change() {
		return pass_change;
	}
	public void setPass_change(String pass_change) {
		this.pass_change = pass_change;
	}
	public String getToAddr() {
		return toAddr;
	}
	public void setToAddr(String toAddr) {
		this.toAddr = toAddr;
	}
	public String getFrmAddr() {
		return frmAddr;
	}
	public void setFrmAddr(String frmAddr) {
		this.frmAddr = frmAddr;
	}
	public String getSubj() {
		return subj;
	}
	public void setSubj(String subj) {
		this.subj = subj;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getSlogo() {
		return slogo;
	}
	public void setSlogo(String slogo) {
		this.slogo = slogo;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getAyear() {
		return ayear;
	}
	public void setAyear(String ayear) {
		this.ayear = ayear;
	}
	public String getSheader() {
		return sheader;
	}
	public void setSheader(String sheader) {
		this.sheader = sheader;
	}
	public String getSfooter() {
		return sfooter;
	}
	public void setSfooter(String sfooter) {
		this.sfooter = sfooter;
	}
	public String getLtype() {
		return ltype;
	}
	public void setLtype(String ltype) {
		this.ltype = ltype;
	}
	public String getLpass() {
		return lpass;
	}
	public void setLpass(String lpass) {
		this.lpass = lpass;
	}
	public String getStaff_ID() {
		return Staff_ID;
	}
	public void setStaff_ID(String staff_ID) {
		Staff_ID = staff_ID;
	}


	public String getDepartment() {
		return Department;
	}


	public void setDepartment(String department) {
		Department = department;
	}


	public String getName() {
		return Name;
	}


	public void setName(String name) {
		Name = name;
	}


	public String getAge() {
		return Age;
	}


	public void setAge(String age) {
		Age = age;
	}


	


	


	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public String getPhone_Number() {
		return Phone_Number;
	}


	public void setPhone_Number(String phone_Number) {
		Phone_Number = phone_Number;
	}


	public String getContact_Number() {
		return Contact_Number;
	}


	public void setContact_Number(String contact_Number) {
		Contact_Number = contact_Number;
	}


	public String getEmail() {
		return Email;
	}


	public void setEmail(String email) {
		Email = email;
	}


	


	public String getDOJ() {
		return DOJ;
	}
	public void setDOJ(String dOJ) {
		DOJ = dOJ;
	}
	public String getWork_Area() {
		return Work_Area;
	}


	public void setWork_Area(String work_Area) {
		Work_Area = work_Area;
	}


	public String getCurrent_Address() {
		return Current_Address;
	}


	public void setCurrent_Address(String current_Address) {
		Current_Address = current_Address;
	}


	public String getPermanent_Address() {
		return Permanent_Address;
	}


	public void setPermanent_Address(String permanent_Address) {
		Permanent_Address = permanent_Address;
	}


	

	public String getIsActive() {
		return isActive;
	}


	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}


	public String getInserted_By() {
		return Inserted_By;
	}


	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}


	


	public String getUpdated_By() {
		return Updated_By;
	}


	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}


	


	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}
	public String getInserted_Date() {
		return Inserted_Date;
	}
	public void setInserted_Date(String inserted_Date) {
		Inserted_Date = inserted_Date;
	}
	public String getUpdated_Date() {
		return Updated_Date;
	}
	public void setUpdated_Date(String updated_Date) {
		Updated_Date = updated_Date;
	}
	public List<NonteachVO> getNonteachServiceVOList() {
		return nonteachServiceVOList;
	}


	public void setNonteachServiceVOList(List<NonteachVO> nonteachServiceVOList) {
		this.nonteachServiceVOList = nonteachServiceVOList;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	
	
	
}
