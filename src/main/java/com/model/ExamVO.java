package com.model;

import java.util.Date;
import java.util.List;

public class ExamVO {

	String Exam_Id=null;
	String Exam_Name=null;
	String Exam_Type=null;
	String E_Class=null;
	String Section=null;
	
	String Subject1=null;
	Date E_Date1=null;
	String E_Time1=null;
	String Subject2=null;
	Date E_Date2=null;
	String E_Time2=null;
	String Subject3=null;
	Date E_Date3=null;
	String E_Time3=null;
	String Subject4=null;
	Date E_Date4=null;
	String E_Time4=null;
	String Subject5=null;
	Date E_Date5=null;
	String E_Time5=null;
	String is_Active=null;
	String Inserted_By=null;
	Date Inserted_Date=null;
	String Updated_By=null;
	Date Updated_Date=null;
	String tran_typ=null;
	
	private List<ExamVO> examServiceVOList = null;
	
	

	public String getTran_typ() {
		return tran_typ;
	}

	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}

	public String getExam_Id() {
		return Exam_Id;
	}

	public void setExam_Id(String exam_Id) {
		Exam_Id = exam_Id;
	}

	public String getExam_Name() {
		return Exam_Name;
	}

	public void setExam_Name(String exam_Name) {
		Exam_Name = exam_Name;
	}

	public String getExam_Type() {
		return Exam_Type;
	}

	public void setExam_Type(String exam_Type) {
		Exam_Type = exam_Type;
	}

	

	public String getE_Class() {
		return E_Class;
	}

	public void setE_Class(String e_Class) {
		E_Class = e_Class;
	}

	public String getSection() {
		return Section;
	}

	public void setSection(String section) {
		Section = section;
	}

	public String getSubject1() {
		return Subject1;
	}

	public void setSubject1(String subject1) {
		Subject1 = subject1;
	}

	

	public String getE_Time1() {
		return E_Time1;
	}

	public void setE_Time1(String e_Time1) {
		E_Time1 = e_Time1;
	}

	public String getSubject2() {
		return Subject2;
	}

	public void setSubject2(String subject2) {
		Subject2 = subject2;
	}

	

	

	public String getSubject3() {
		return Subject3;
	}

	public void setSubject3(String subject3) {
		Subject3 = subject3;
	}

	

	public String getE_Time3() {
		return E_Time3;
	}

	public void setE_Time3(String e_Time3) {
		E_Time3 = e_Time3;
	}

	public String getSubject4() {
		return Subject4;
	}

	public void setSubject4(String subject4) {
		Subject4 = subject4;
	}

	
	public String getE_Time4() {
		return E_Time4;
	}

	public void setE_Time4(String e_Time4) {
		E_Time4 = e_Time4;
	}

	public String getSubject5() {
		return Subject5;
	}

	public void setSubject5(String subject5) {
		Subject5 = subject5;
	}

	

	public String getE_Time5() {
		return E_Time5;
	}

	public void setE_Time5(String e_Time5) {
		E_Time5 = e_Time5;
	}

	public String getIs_Active() {
		return is_Active;
	}

	public void setIs_Active(String is_Active) {
		this.is_Active = is_Active;
	}

	public String getInserted_By() {
		return Inserted_By;
	}

	public void setInserted_By(String inserted_By) {
		Inserted_By = inserted_By;
	}

	public Date getInserted_Date() {
		return Inserted_Date;
	}

	public void setInserted_Date(Date inserted_Date) {
		Inserted_Date = inserted_Date;
	}

	public String getUpdated_By() {
		return Updated_By;
	}

	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}

	public Date getUpdated_Date() {
		return Updated_Date;
	}

	public void setUpdated_Date(Date updated_Date) {
		Updated_Date = updated_Date;
	}

	public List<ExamVO> getExamServiceVOList() {
		return examServiceVOList;
	}

	public void setExamServiceVOList(List<ExamVO> examServiceVOList) {
		this.examServiceVOList = examServiceVOList;
	}

	public Date getE_Date1() {
		return E_Date1;
	}

	public void setE_Date1(Date e_Date1) {
		E_Date1 = e_Date1;
	}

	public Date getE_Date2() {
		return E_Date2;
	}

	public void setE_Date2(Date e_Date2) {
		E_Date2 = e_Date2;
	}

	public String getE_Time2() {
		return E_Time2;
	}

	public void setE_Time2(String e_Time2) {
		E_Time2 = e_Time2;
	}

	public Date getE_Date3() {
		return E_Date3;
	}

	public void setE_Date3(Date e_Date3) {
		E_Date3 = e_Date3;
	}

	public Date getE_Date4() {
		return E_Date4;
	}

	public void setE_Date4(Date e_Date4) {
		E_Date4 = e_Date4;
	}

	public Date getE_Date5() {
		return E_Date5;
	}

	public void setE_Date5(Date e_Date5) {
		E_Date5 = e_Date5;
	}

		
	
	
	
}
