package com.model;

import java.util.Date;
import java.util.List;

public class MailSettingVO
{
	private String tem_var = null;
	private String tem_message = null;
	private String temp_id = null;
	private String temp_head=null;
	private String temp_footer=null;
	private String tran_typ = null;
	
	private String terms=null;
	
	private List<MailSettingVO> mailSettingServiceVOList = null;

	
	
	public String getTran_typ() {
		return tran_typ;
	}

	public void setTran_typ(String tran_typ) {
		this.tran_typ = tran_typ;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getTemp_footer() {
		return temp_footer;
	}

	public void setTemp_footer(String temp_footer) {
		this.temp_footer = temp_footer;
	}

	public String getTemp_head() {
		return temp_head;
	}

	public void setTemp_head(String temp_head) {
		this.temp_head = temp_head;
	}

	public String getTem_var() {
		return tem_var;
	}

	public void setTem_var(String tem_var) {
		this.tem_var = tem_var;
	}

	public String getTem_message() {
		return tem_message;
	}

	public void setTem_message(String tem_message) {
		this.tem_message = tem_message;
	}

	

	public String getTemp_id() {
		return temp_id;
	}

	public void setTemp_id(String temp_id) {
		this.temp_id = temp_id;
	}

	public List<MailSettingVO> getMailSettingServiceVOList() {
		return mailSettingServiceVOList;
	}

	public void setMailSettingServiceVOList(List<MailSettingVO> mailSettingServiceVOList) {
		this.mailSettingServiceVOList = mailSettingServiceVOList;
	}
	
	
	
}
