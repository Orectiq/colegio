package com.model;

import java.util.List;

public class SettingadminVO 
{
	private String sname=null;
	private String logo_disp=null;
	private String language=null; 
	private String language_allowed=null;
	
	
	private List<SettingadminVO> settingAdminServiceVOList = null;


	public String getSname() {
		return sname;
	}


	public void setSname(String sname) {
		this.sname = sname;
	}


	


	public String getLogo_disp() {
		return logo_disp;
	}


	public void setLogo_disp(String logo_disp) {
		this.logo_disp = logo_disp;
	}


	public String getLanguage() {
		return language;
	}


	public void setLanguage(String language) {
		this.language = language;
	}


	public String getLanguage_allowed() {
		return language_allowed;
	}


	public void setLanguage_allowed(String language_allowed) {
		this.language_allowed = language_allowed;
	}


	public List<SettingadminVO> getSettingAdminServiceVOList() {
		return settingAdminServiceVOList;
	}


	public void setSettingAdminServiceVOList(List<SettingadminVO> settingAdminServiceVOList) {
		this.settingAdminServiceVOList = settingAdminServiceVOList;
	}


	
	
	
	
}
