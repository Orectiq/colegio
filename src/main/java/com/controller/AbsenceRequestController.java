package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.model.AbsenceRequestVO;
import com.model.AbsenceStuRequestVO;
import com.model.LibraryIssueVO;
import com.model.TeacherVO;
import com.service.AbsenceRequestService;

@Controller
@RequestMapping("/AbsenceRequest")
@SessionAttributes("userName2")
public class AbsenceRequestController {
	
	@Autowired
	@Qualifier(value = "absenceRequestService")
	private AbsenceRequestService absenceRequestService;
	
	private String userName = "";
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap, @RequestParam("d") String pg, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		modelMap.addAttribute("userName2", userName);
		return new ModelAndView(pg, modelMap);
	}
	
	@RequestMapping(value = "/insertAbsenceRequest", method = RequestMethod.POST)
	@ResponseBody
	public AbsenceRequestVO insertAbsenceRequest(@ModelAttribute AbsenceRequestVO absenceRequestVO) throws Exception 
	{
		
		AbsenceRequestVO AbsenceRequestvo = new AbsenceRequestVO();
		
		System.out.println("controller AbsenceRequest ------------------------------->? ");
		System.out.println(absenceRequestVO.getTeacher_Id());
		System.out.println(absenceRequestVO.getTeacher_Name());
		System.out.println(absenceRequestVO.getStart_Date());
		System.out.println(absenceRequestVO.getEnd_Date());
		System.out.println(absenceRequestVO.getAbsence_Name());
		System.out.println(absenceRequestVO.getComments());
		System.out.println("controller AbsenceRequest end  ------------------------------->?");
		
		try {
			AbsenceRequestvo = absenceRequestService.insertAbsenceRequestDetails(absenceRequestVO);
				} catch (Exception e) {
		}
		return AbsenceRequestvo;
	}
	
	//insertStuAbsenceRequest
	@RequestMapping(value = "/insertStuAbsenceRequest", method = RequestMethod.POST)
	@ResponseBody
	public AbsenceStuRequestVO insertStuAbsenceRequest(@ModelAttribute AbsenceStuRequestVO absenceRequestVO) throws Exception 
	{
		
		AbsenceStuRequestVO AbsenceRequestvo = new AbsenceStuRequestVO();
		
		/*System.out.println("controller AbsenceRequest");
		System.out.println(absenceRequestVO.getTeacher_Id());
		System.out.println(absenceRequestVO.getTeacher_Name());
		System.out.println(absenceRequestVO.getStart_Date());
		System.out.println(absenceRequestVO.getEnd_Date());
		System.out.println(absenceRequestVO.getAbsence_Name());
		System.out.println(absenceRequestVO.getComments());
		System.out.println("controller AbsenceRequest end");
		*/
		try {
			AbsenceRequestvo = absenceRequestService.insertStuAbsenceRequest(absenceRequestVO);
				} catch (Exception e) {
		}
		return AbsenceRequestvo;
	}
	
	
	@RequestMapping(value = "/getltrreferencedata", method = RequestMethod.GET)
	@ResponseBody
	public AbsenceRequestVO getLtrReferenceData() throws Exception {
		AbsenceRequestVO AbsenceRequestvo = new AbsenceRequestVO();
		
		try{
			AbsenceRequestvo = absenceRequestService.getLtrReferenceData();
		}
		catch(Exception e){
		}
		return AbsenceRequestvo;
	}
	
	//getTeacherLeaveRequestbyDate
		@RequestMapping(value = "/getTeacherLeaveRequestbyDate", method = RequestMethod.GET)
		@ResponseBody
		public  AbsenceRequestVO getTeacherLeaveRequestbyDate(@RequestParam("req_date") String rdate) throws Exception 
		{
			System.out.println("enter controller  ");
			return absenceRequestService.getTeacherLeaveRequestbyDate(rdate);
		}
	//getTeacherLeaveRequestbyDateNotify	
		@RequestMapping(value = "/getTeacherLeaveRequestbyDateNotify", method = RequestMethod.GET)
		@ResponseBody
		public  AbsenceRequestVO getTeacherLeaveRequestbyDateNotify(@RequestParam("theDate") String rdate,@RequestParam("Teacher_Id") String tid) throws Exception 
		{
			System.out.println("enter controller  ");
			return absenceRequestService.getTeacherLeaveRequestbyDateNotify(rdate,tid);
		}
		//getTeacherLeaveRequestbyDateNotify1
				@RequestMapping(value = "/getTeacherLeaveRequestbyDateNotify1", method = RequestMethod.GET)
				@ResponseBody
				public  AbsenceRequestVO getTeacherLeaveRequestbyDateNotify1(@RequestParam("theDate") String rdate,@RequestParam("Teacher_Id") String tid,@RequestParam("time") String tim) throws Exception 
				{
					System.out.println("enter controller  ");
					return absenceRequestService.getTeacherLeaveRequestbyDateNotify1(rdate,tid,tim);
				}	
		
		
		
	//getStudentLeaveRequestbyDate
		@RequestMapping(value = "/getStudentLeaveRequestbyDate", method = RequestMethod.GET)
		@ResponseBody
		public  AbsenceStuRequestVO getStudentLeaveRequestbyDate(@RequestParam("thedate") String rdate) throws Exception 
		{
			System.out.println("enter controller  ");
			return absenceRequestService.getStudentLeaveRequestbyDate(rdate);
		}
		
	//getLeaveData	
		@RequestMapping(value = "/getLeaveData", method = RequestMethod.GET)
		@ResponseBody
		public  AbsenceRequestVO getLeaveData(@RequestParam("Teacher_Id") String tid) throws Exception 
		{
			System.out.println("enter controller  &&&&&&&&&&& "+tid);
			return absenceRequestService.getLeaveData(tid);
		}
	//getLeaveGrantbyStudent	
		@RequestMapping(value = "/getLeaveGrantbyStudent", method = RequestMethod.GET)
		@ResponseBody
		public  AbsenceStuRequestVO getLeaveGrantbyStudent(@RequestParam("student_id") String sid) throws Exception 
		{
			System.out.println("enter controller  &&&&&&&&&&& "+sid);
			return absenceRequestService.getLeaveGrantbyStudent(sid);
		}
		
	//updateLeaveReq
		@RequestMapping(value = "/updateLeaveReq", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
		@ResponseBody
		public AbsenceRequestVO updateLeaveReq(@ModelAttribute AbsenceRequestVO absencereqVO) throws Exception, Exception {
			
			System.out.println("controller Library==============>");
			//System.out.println(absencereqVO.getTheDate());
			System.out.println(absencereqVO.getTeacher_Id());
			System.out.println(absencereqVO.getStatus());
			System.out.println("controller library end ==============>");
			AbsenceRequestVO AbsenceReqvo = new AbsenceRequestVO();
			try {
				AbsenceReqvo = absenceRequestService.updateLeaveReq(absencereqVO);
					} catch (Exception e) {
			}
			return AbsenceReqvo;
		}
		//updateStudentLeaveReq
		@RequestMapping(value = "/updateStudentLeaveReq", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
		@ResponseBody
		public AbsenceStuRequestVO updateStudentLeaveReq(@ModelAttribute AbsenceStuRequestVO absencereqVO) throws Exception, Exception {
			
			System.out.println("controller Library==============>");
			System.out.println(absencereqVO.getThedate());
			System.out.println(absencereqVO.getStudent_id());
			System.out.println(absencereqVO.getStatus());
			System.out.println(absencereqVO.getLapply());
			System.out.println(absencereqVO.getApproved_by());
			System.out.println("controller library end ==============>");
			AbsenceStuRequestVO AbsenceReqvo = new AbsenceStuRequestVO();
			try {
				AbsenceReqvo = absenceRequestService.updateStudentLeaveReq(absencereqVO);
					} catch (Exception e) {
			}
			return AbsenceReqvo;
		}
		
		
		//updateTeacherMaster
		@RequestMapping(value = "/updateTeacherMaster", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
		@ResponseBody
		public TeacherVO updateTeacherMaster(@ModelAttribute TeacherVO teacherVO) throws Exception, Exception {
			
			System.out.println("**** controller Library ****");
			System.out.println(teacherVO.getTid());
			System.out.println("****controller library end ****");
			TeacherVO Teachervo = new TeacherVO();
			try {
				Teachervo = absenceRequestService.updateTeacherMaster(teacherVO);
					} catch (Exception e) {
			}
			return Teachervo;
		}

}
