package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
@Controller
@RequestMapping("/adminboard")
public class AdminboardController
{
	@RequestMapping(value = "/stumain", method = RequestMethod.POST)
	public ModelAndView editList(ModelMap modelMap,@RequestParam("sname")String sname, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				request.setAttribute("sname", sname);
				System.out.println("session value of studid      "+sname);
				
		return new ModelAndView("/student/StudentMain", modelMap);
	}	
}
