package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.common.ExamTypeMasterVO;
import com.model.ExamNewVO;
import com.model.ExamStudentVO;
import com.model.ExamVO;
import com.model.MarksEntryVO;
import com.model.MarksTempVO;
import com.model.MarksdumpVO;
import com.model.MarksentryallVO;
import com.model.StuParVO;
import com.service.ExamService;

@Controller
@RequestMapping("/exam")
@SessionAttributes("userName2")
public class ExamController {

	@Autowired
	@Qualifier(value = "examService")
	private ExamService examService;
	
	private String userName1="";
	
	
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap,@RequestParam("d") String pg, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				modelMap.addAttribute("userName2", userName1);
		return new ModelAndView(pg, modelMap);
	
	
}
	
	@RequestMapping(value = "/insertExam", method = RequestMethod.POST)
	@ResponseBody
	public ExamNewVO insertExam(@ModelAttribute ExamNewVO examVO) throws Exception, Exception 
	{
		System.out.println("Exam Controller ");
		ExamNewVO Examvo = new ExamNewVO();

		try {
			Examvo = examService.insertExamDetails(examVO);
				} catch (Exception e) {
		}
		return Examvo;
		
		
		
		
	}
	
	@RequestMapping(value = "/getltrreferencedata", method = RequestMethod.GET)
	@ResponseBody
		public ExamVO getLtrReferenceData() throws Exception 
	{
						ExamVO Examvo = new ExamVO();
						try{
							Examvo =examService.getLtrReferenceData();
						}
						catch(Exception e){
						}
						return Examvo;
			}
	
	@RequestMapping(value = "/getExamData", method = RequestMethod.GET)
	@ResponseBody
		public ExamNewVO getExamData() throws Exception 
	{
						ExamNewVO Examvo = new ExamNewVO();
						try{
							Examvo =examService.getExamData();
						}
						catch(Exception e){
						}
						return Examvo;
			}
	
	
	@RequestMapping(value = "/getltrreferencedata100", method = RequestMethod.GET)
	@ResponseBody
		public ExamVO getltrreferencedata100() throws Exception 
	{
						ExamVO Examvo = new ExamVO();
						try{
							Examvo =examService.getLtrReferenceData100();
						}
						catch(Exception e){
						}
						return Examvo;
			}
	
	
	//getExam
	@RequestMapping(value = "/getExam", method = RequestMethod.GET)
	@ResponseBody
		public ExamNewVO getExam(@RequestParam("E_Class") String sclass,@RequestParam("Section") String section,@RequestParam("Exam_Type") String etype) throws Exception 
	{
						System.out.println("getExam controller"+sclass+","+section+"etype  "+etype);
						ExamNewVO Examvo = new ExamNewVO();
						try{
							Examvo =examService.getExam(sclass,section,etype);
						}
						catch(Exception e){
						}
						return Examvo;
			}
	//getExam
		@RequestMapping(value = "/getExamClass", method = RequestMethod.GET)
		@ResponseBody
			public ExamNewVO getExamClass(@RequestParam("e_class") String sclass,@RequestParam("section") String section) throws Exception 
		{
							System.out.println("getExam controller"+sclass+","+section);
							ExamNewVO Examvo = new ExamNewVO();
							try{
								Examvo =examService.getExamClass(sclass,section);
							}
							catch(Exception e){
							}
							return Examvo;
				}
	
		
	
	
	
	//getClassMarks           data: {"sclass":sclass,"ssec":sec,"exam_id":eid,"exam_type":etype},
	@RequestMapping(value = "/getClassMarks", method = RequestMethod.GET)
	@ResponseBody
		public MarksentryallVO getClassMarks(@RequestParam("sclass") String sclass,@RequestParam("ssec") String section,@RequestParam("exam_type") String etype,@RequestParam("exam_id") String eid) throws Exception 
	{
						System.out.println("getExam controller"+sclass+","+section+"etype  "+etype);
						MarksentryallVO Marksvo = new MarksentryallVO();
						try{
							Marksvo =examService.getClassMarks(sclass,section,etype,eid);
						}
						catch(Exception e){
						}
						return Marksvo;
			}
	//getClassSubject           data: {"sclass":sclass,"ssec":sec,"exam_id":eid,"exam_type":etype},
		@RequestMapping(value = "/getClassSubject", method = RequestMethod.GET)
		@ResponseBody
			public MarksentryallVO getClassSubject(@RequestParam("sclass") String sclass,@RequestParam("ssec") String section) throws Exception 
		{
							System.out.println("getExam controller"+sclass+","+section);
							MarksentryallVO Marksvo = new MarksentryallVO();
							try{
								Marksvo =examService.getClassSubject(sclass,section);
							}
							catch(Exception e){
							}
							return Marksvo;
				}
	 //getClassStudents
		@RequestMapping(value = "/getClassStudents", method = RequestMethod.GET)
		@ResponseBody
			public MarksentryallVO getClassStudents(@RequestParam("sclass") String sclass,@RequestParam("ssec") String section) throws Exception 
		{
							System.out.println("getExam controller"+sclass+","+section);
							MarksentryallVO Marksvo = new MarksentryallVO();
							try{
								Marksvo =examService.getClassStudents(sclass,section);
							}
							catch(Exception e){
							}
							return Marksvo;
				}
		//getClassExamid
		
				@RequestMapping(value = "/getClassExamid", method = RequestMethod.GET)
				@ResponseBody
					public MarksdumpVO getClassExamid(/*@RequestParam("exam_id") String eid,@RequestParam("sclass") String sclass,@RequestParam("ssec") String sec*//*,@RequestParam("subj") String[] arr1*/) throws Exception 
				{ 
									//System.out.println("getExam controller  --->  "+eid);
									MarksdumpVO Marksvo = new MarksdumpVO();
									try{
										Marksvo =examService.getClassExamid(/*eid,sclass,sec*/);
									}
									catch(Exception e){
									}
									return Marksvo;
						}
				
				//getMarksTemp
				
				@RequestMapping(value = "/getMarksTemp", method = RequestMethod.GET)
				@ResponseBody
					public MarksTempVO getMarksTemp(@RequestParam("exam_id") String eid,@RequestParam("sclass") String sclass,@RequestParam(value="myArray[]") String[] myArray) throws Exception 
				{ 
									//System.out.println("getExam controller  --->  "+eid);
									MarksTempVO MarksTempvo = new MarksTempVO();
									try{
										MarksTempvo =examService.getMarksTemp(eid,sclass,myArray);
									}
									catch(Exception e){
									}
									return MarksTempvo;
						}
				
				
				
				
				
				//getMarksFinal
				@RequestMapping(value = "/getMarksFinal", method = RequestMethod.GET)
				@ResponseBody
					public MarksdumpVO getMarksFinal() throws Exception 
				{ 
									System.out.println("getExam controller    ");
									MarksdumpVO Marksvo = new MarksdumpVO();
									try{
										Marksvo =examService.getMarksFinal();
									}
									catch(Exception e){
									}
									return Marksvo;
						}
		
		
		//getClassStudentsMarks
		@RequestMapping(value = "/getClassStudentsMarks", method = RequestMethod.GET)
		@ResponseBody
			public MarksentryallVO getClassStudentsMarks(@RequestParam("exam_id") String eid,@RequestParam("student_id") String sid,@RequestParam("subject") String subj) throws Exception 
		{ 
							System.out.println("getExam controller    "+eid+"  sid  "+sid+"  subject  "+subj);
							MarksentryallVO Marksvo = new MarksentryallVO();
							try{
								Marksvo =examService.getClassStudentsMarks(eid,sid,subj);
							}
							catch(Exception e){
							}
							return Marksvo;
				}
		
		
		
		
	//getClassMarksStudents           data: {"sclass":sclass,"ssec":sec,"exam_id":eid,"exam_type":etype},
		@RequestMapping(value = "/getClassMarksStudents", method = RequestMethod.GET)
		@ResponseBody
			public MarksEntryVO getClassMarksStudents(@RequestParam("sclass") String sclass,@RequestParam("ssec") String section,@RequestParam("exam_id") String eid,@RequestParam("student_id") String sid) throws Exception 
		{
							System.out.println("getExam controller"+sclass+","+section);
							MarksEntryVO Marksvo = new MarksEntryVO();
							try{
								Marksvo =examService.getClassMarksStudents(sclass,section,eid,sid);
							}
							catch(Exception e){
							}
							return Marksvo;
				}
	
	
	
	//getExamType
	@RequestMapping(value = "/getExamType", method = RequestMethod.GET)
	@ResponseBody
		public ExamTypeMasterVO getExamType() throws Exception 
	{
						ExamTypeMasterVO Examtypevo = new ExamTypeMasterVO();
						try{
							Examtypevo =examService.getExamType();
						}
						catch(Exception e){
						}
						return Examtypevo;
			}
	//getStudentMarks
	@RequestMapping(value = "/getStudentMarks", method = RequestMethod.GET)
	@ResponseBody
		public MarksEntryVO getStudentMarks(@RequestParam("student_id") String sid) throws Exception 
	{
						System.out.println("getExam controller"+sid);
						MarksEntryVO Marksvo = new MarksEntryVO();
						try{
							Marksvo =examService.getStudentMarks(sid);
						}
						catch(Exception e){
						}
						return Marksvo;
			}
	
	//getExamID
		@RequestMapping(value = "/getExamID", method = RequestMethod.GET)
		@ResponseBody
			public ExamNewVO getExamID(@RequestParam("exam_id") String eid) throws Exception 
		{
							System.out.println("getExam controller"+eid);
							ExamNewVO examvo = new ExamNewVO();
							try{
								examvo =examService.getExamID(eid);
							}
							catch(Exception e){
							}
							return examvo;
				}
		//getExamID_Subject
				@RequestMapping(value = "/getExamID_Subject", method = RequestMethod.GET)
				@ResponseBody
					public ExamNewVO getExamID_Subject(@RequestParam("exam_id") String eid,@RequestParam("subject") String subj) throws Exception 
				{
									System.out.println("getExam controller"+eid);
									ExamNewVO examvo = new ExamNewVO();
									try{
										examvo =examService.getExamID_Subject(eid,subj);
									}
									catch(Exception e){
									}
									return examvo;
						}
		
		
		
	//getExamDetails
	
	@RequestMapping(value = "/getExamDetails", method = RequestMethod.GET)
	@ResponseBody
		public ExamStudentVO getExamDetails(@RequestParam("student_id") String sid) throws Exception 
	{
						System.out.println("getExam controller"+sid);
						ExamStudentVO Examstudentvo = new ExamStudentVO();
						try{
							Examstudentvo =examService.getExamDetails(sid);
						}
						catch(Exception e){
						}
						return Examstudentvo;
			}
	
	@RequestMapping(value = "/getltrreferencedataall", method = RequestMethod.GET)
	@ResponseBody
		public ExamVO getLtrReferenceDataall() throws Exception 
	{
						ExamVO Examvo = new ExamVO();
						try{
							Examvo =examService.getLtrReferenceDataall();
						}
						catch(Exception e){
						}
						return Examvo;
			}
	
}
