package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dao.constant.CommonConstant;

@Controller
@RequestMapping("/teacherMenu")
public class TeacherMenuController 
{
	String urlnew=CommonConstant.PATH_URL;
	

	
	//teacher_dashboard
		@RequestMapping(value = "/teacher_dashboard", method = RequestMethod.GET)
		public ModelAndView teacher_dashboard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
			String projectUrl=urlnew+"/Dashboard/Teacher_DashBoard.jsp?&mobile="+mobile+"&ltype="+ltype;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
		}
	

	//teacher_Dashboard
		@RequestMapping(value = "/teacherDashboard", method = RequestMethod.GET)
		public ModelAndView teacher_Dashboard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					//request.setAttribute("lid", sid);
					//System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
					
			//return new ModelAndView("/library/DuedateList", modelMap);
					String projectUrl=urlnew+"/Dashboard/Teacher_DashBoard.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
		}
	
	
	

	//teacher_StudentsList
	@RequestMapping(value = "/teacher_StudentsList", method = RequestMethod.GET)
	public ModelAndView teacher_StudentsList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("sname")String ltype, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				//request.setAttribute("lid", sid);
				//System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
				
		//return new ModelAndView("/library/DuedateList", modelMap);
				String projectUrl=urlnew+"/Teachers/Teacher_StudentsList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
				return new ModelAndView("redirect:"+projectUrl,modelMap);
	}
	
	//assignmentApprovalPage
		@RequestMapping(value = "/assignmentApprovalPage", method = RequestMethod.GET)
		public ModelAndView assignmentApprovalPage(ModelMap modelMap,@RequestParam("techid")String techid,@RequestParam("subject")String subject,@RequestParam("ttype")String ttype,@RequestParam("sclass")String sclass,@RequestParam("sec")String sec,@RequestParam("title")String title,@RequestParam("assignid")String assignid,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					System.out.println("######################  "+title);
					String projectUrl=urlnew+"/Assignment/AssinmentApprovalPage.jsp?&techid="+techid+"&subject="+subject+"&title="+title+"&ttype="+ttype+"&sclass="+sclass+"&sec="+sec+"&assignid="+assignid+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
		}
	
		//assignmentReApprovalPage
				@RequestMapping(value = "/assignmentReApprovalPage", method = RequestMethod.GET)
				public ModelAndView assignmentReApprovalPage(ModelMap modelMap,@RequestParam("techid")String techid,@RequestParam("ttype")String ttype,@RequestParam("sclass")String sclass,@RequestParam("sec")String sec,@RequestParam("title")String title,@RequestParam("subject")String subject,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,@RequestParam("assignid")String assignid, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							System.out.println("TITLE %%%%%%%%%%%%%%%%%%%%%%  "+title+" classs   "+sclass+"slogo"+slogo+"sheader"+sheader+"ttype"+ttype);
							
							String projectUrl=urlnew+"/Assignment/AssignmentReApprovalPage.jsp?&techid="+techid+"&ttype="+ttype+"&sclass="+sclass+"&sec="+sec+"&title="+title+"&subject="+subject+"&slogo="+slogo+"&sheader="+sheader+"&assignid="+assignid;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
	
	
	//teacher_NoticeBoardList
		@RequestMapping(value = "/teacher_NoticeBoardList", method = RequestMethod.GET)
		public ModelAndView teacher_NoticeBoardList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					//request.setAttribute("lid", sid);
					//System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
					
			//return new ModelAndView("/library/DuedateList", modelMap);
					String projectUrl=urlnew+"/Noticeboard/Teacher_NewsList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
		}
		
		
	//homeworkList
		@RequestMapping(value = "/homeworkList", method = RequestMethod.GET)
		public ModelAndView homeworkList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					/*request.setAttribute("techId", techId);
					System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+techId);
					return new ModelAndView("/HomeWork/Teacher_HomeworkList", modelMap);*/
			
			String projectUrl=urlnew+"/homework/Teacher_HomeworkList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
		}	
		//homeworkAdd
				@RequestMapping(value = "/homeworkAdd", method = RequestMethod.GET)
				public ModelAndView homeworkAdd(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							/*request.setAttribute("techId", techId);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+techId);
							return new ModelAndView("/HomeWork/Teacher_HomeworkList", modelMap);*/
					
					String projectUrl=urlnew+"/homework/homeworkAdd.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
			//homeworkStatus
				@RequestMapping(value = "/homeworkStatus", method = RequestMethod.GET)
				public ModelAndView homeworkStatus(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							/*request.setAttribute("techId", techId);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+techId);
							return new ModelAndView("/HomeWork/Teacher_HomeworkList", modelMap);*/
					
					String projectUrl=urlnew+"/homework/Teacher_HW_StatusList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
			//Teacher_AssignmentStatus
				@RequestMapping(value = "/Teacher_AssignmentStatus", method = RequestMethod.GET)
				public ModelAndView Teacher_AssignmentStatus(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							/*request.setAttribute("techId", techId);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+techId);
							return new ModelAndView("/HomeWork/Teacher_HomeworkList", modelMap);*/
					
					String projectUrl=urlnew+"/Assignment/Teacher_AssignmentStatus.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
			//Teacher_AssignmentAdd
				@RequestMapping(value = "/Teacher_AssignmentAdd", method = RequestMethod.GET)
				public ModelAndView Teacher_AssignmentAdd(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
									
					String projectUrl=urlnew+"/Assignment/Teacher_AssignmentAdd.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
				}			
			//Teacher_AssignmentApproval	
				@RequestMapping(value = "/Teacher_AssignmentApproval", method = RequestMethod.GET)
				public ModelAndView Teacher_AssignmentApproval(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							/*request.setAttribute("techId", techId);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+techId);
							return new ModelAndView("/HomeWork/Teacher_HomeworkList", modelMap);*/
					
					String projectUrl=urlnew+"/Assignment/Teacher_AssignmentApproval.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
				}			
				
			//viewActivity
				@RequestMapping(value = "/viewactivity", method = RequestMethod.GET)
				public ModelAndView viewactivity(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/class/Activity_List.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
						
				}
				
				
				//addActivity
				@RequestMapping(value = "/addActivity", method = RequestMethod.GET)
				public ModelAndView addActivity(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/class/Addactivity.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
		//AbsenseRequest
				@RequestMapping(value = "/AbsenseRequest", method = RequestMethod.GET)
				public ModelAndView AbsenseRequest(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//String projectUrl=urlnew+"/Teachers/Admin_TeacherAbsenseRequest.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;   
							String projectUrl=urlnew+"/Teachers/Teacher_AbsenceHistory.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader; 
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//Teacher_AbsenceHistory
				@RequestMapping(value = "/Teacher_AbsenceHistory", method = RequestMethod.GET)
				public ModelAndView Teacher_AbsenceHistory(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Teachers/Admin_TeacherAbsenseRequest.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader; 
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//StudentsLeaveApproval
				@RequestMapping(value = "/StudentsLeaveApproval", method = RequestMethod.GET)
				public ModelAndView StudentsLeaveApproval(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Leave/StuLeaveApproval.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}			
		//Examlist
				@RequestMapping(value = "/Examlist", method = RequestMethod.GET)
				public ModelAndView Examlist(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							//System.out.println("request value of admid       "+admid);
							
							String projectUrl=urlnew+"/Exam/Teacher_ExamList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);

							//return new ModelAndView("/exam/Examlist", modelMap);
				}	
		//ExamAdd
				@RequestMapping(value = "/ExamAdd", method = RequestMethod.GET)
				public ModelAndView ExamAdd(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							//System.out.println("request value of admid ---->      "+admid);
							//return new ModelAndView("/exam/Examid", modelMap);
							String projectUrl=urlnew+"/Exam/Examid.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//Teacher_StuAttendance
				@RequestMapping(value = "/Teacher_StuAttendance", method = RequestMethod.GET)
				public ModelAndView Teacher_StuAttendance(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo, @RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							//System.out.println("request value of admid ---->      "+admid);
							//return new ModelAndView("/exam/Examid", modelMap);
							String projectUrl=urlnew+"/Calendar/Teacher_StudentsAttendance.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
		//Teacher_StuAttendanceHistory
				@RequestMapping(value = "/Teacher_StuAttendanceHistory", method = RequestMethod.GET)
				public ModelAndView Teacher_StuAttendanceHistory(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Attendance/Teacher_StuAttendanceHis.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
				
		//Teacher_StuAttendanceEntry
				@RequestMapping(value = "/Teacher_StuAttendanceEntry", method = RequestMethod.GET)
				public ModelAndView Teacher_StuAttendanceEntry(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							//System.out.println("request value of admid ---->      "+admid);
							//return new ModelAndView("/exam/Examid", modelMap);
							String projectUrl=urlnew+"/Attendance/Teacher_StudentsAttendanceEntry.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
		//Teacher_StuMarkEntry
				@RequestMapping(value = "/Teacher_StuMarkEntry", method = RequestMethod.GET)
				public ModelAndView Teacher_StuMarkEntry(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/MarkSheet/SMarksheet.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//Teacher_StuMarkList
				@RequestMapping(value = "/Teacher_StuMarkList", method = RequestMethod.GET)
				public ModelAndView Teacher_StuMarkList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/MarkSheet/Teacher_Marks_ClassList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
		//Teacher_Profile
				@RequestMapping(value = "/Teacher_Profile", method = RequestMethod.GET)
				public ModelAndView Teacher_Profile(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Settings/TeacherProfile.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//MyStudents
				@RequestMapping(value = "/MyStudents", method = RequestMethod.GET)
				public ModelAndView MyStudents(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Teachers/MyStudents.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//MyStudentsStaff
				@RequestMapping(value = "/MyStudentsStaff", method = RequestMethod.GET)
				public ModelAndView MyStudentsStaff(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Teachers/MyStudentsStaff.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
		//inbox
				@RequestMapping(value = "/inbox", method = RequestMethod.GET)
				public ModelAndView inbox(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/inbox.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
		//inboxAdmin
				@RequestMapping(value = "/inboxAdmin", method = RequestMethod.GET)
				public ModelAndView inboxAdmin(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/inboxAdmin.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
				//DL
				@RequestMapping(value = "/DL", method = RequestMethod.GET)
				public ModelAndView DL(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype, @RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/mail_dl.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
				
				
				//DL
				@RequestMapping(value = "/DLList", method = RequestMethod.GET)
				public ModelAndView DLList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype, @RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/mail_dllist.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
				

				
		//inboxParents		
				@RequestMapping(value = "/inboxParents", method = RequestMethod.GET)
				public ModelAndView inboxParents(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/inboxParents.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//inboxStudents		
				@RequestMapping(value = "/inboxStudents", method = RequestMethod.GET)
				public ModelAndView inboxStudents(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/inboxStudents.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//inboxDormitory		
				@RequestMapping(value = "/inboxDormitory", method = RequestMethod.GET)
				public ModelAndView inboxDormitory(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/inboxDormitory.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//inboxLibrary		
				@RequestMapping(value = "/inboxLibrary", method = RequestMethod.GET)
				public ModelAndView inboxLibrary(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/inboxLibrary.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
		//inboxTransportAo		
				@RequestMapping(value = "/inboxTransportAo", method = RequestMethod.GET)
				public ModelAndView inboxTransportAo(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/inboxTransportAO.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//inboxTransport		
				@RequestMapping(value = "/inboxTransport", method = RequestMethod.GET)
				public ModelAndView inboxTransport(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("mtype")String mtype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/inboxTransport.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&mtype="+mtype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
				
		//compose
				@RequestMapping(value = "/compose", method = RequestMethod.GET)
				public ModelAndView compose(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/Compose.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//composeAll
				@RequestMapping(value = "/composeAll", method = RequestMethod.GET)
				public ModelAndView composeAll(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/ComposeAll.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//composeTeacher		
				@RequestMapping(value = "/composeTeacher", method = RequestMethod.GET)
				public ModelAndView composeTeacher(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/ComposeTeacher.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
		//composeAdmin		
				@RequestMapping(value = "/composeAdmin", method = RequestMethod.GET)
				public ModelAndView composeAdmin(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/ComposeAdmin.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//composeParents		
				@RequestMapping(value = "/composeParents", method = RequestMethod.GET)
				public ModelAndView composeParents(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/ComposeParents.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
		//composeStudents		
				@RequestMapping(value = "/composeStudents", method = RequestMethod.GET)
				public ModelAndView composeStudents(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/ComposeStudents.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}				
		//composeDormitory		
				@RequestMapping(value = "/composeDormitory", method = RequestMethod.GET)
				public ModelAndView composeDormitory(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/ComposeDormitory.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//composeLibrary		
				@RequestMapping(value = "/composeLibrary", method = RequestMethod.GET)
				public ModelAndView composeLibrary(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/ComposeLibrary.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//composeTransportAo		
				public ModelAndView composeTransportAo(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/ComposeTransportAO.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
		//composeTransport		
				@RequestMapping(value = "/composeTransport", method = RequestMethod.GET)
				public ModelAndView composeTransport(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/ComposeTransport.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
				//composeTransport		
				@RequestMapping(value = "/editdllist", method = RequestMethod.GET)
				public ModelAndView editdllist(ModelMap modelMap,@RequestParam("group_name")String groupname,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Mail/editdllist.jsp?&groupname="+groupname+"&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
}
