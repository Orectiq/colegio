package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.model.SchoolInfoVO;
import com.service.SuperAdminService;
@Controller
@RequestMapping(value="/SAdmin")
public class SuperAdminController
{
	@Autowired
	public SuperAdminService superAdminService;
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/registerSchool")
	public ResponseEntity registerSchool(@ModelAttribute SchoolInfoVO schoolInfoVO) {
	 superAdminService.registerSchool(schoolInfoVO);
	 return new ResponseEntity(HttpStatus.OK);
	}

	
	
	
}
