package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

//import com.model.ActlistVO;

import com.model.AddActivityVO;
import com.service.AddActivityService;

@Controller
@RequestMapping("/AddActivity")
@SessionAttributes("userName")
public class AddActivityController {
	@Autowired
	// @Qualifier(value = "addactivityService")
	private AddActivityService addactivityService;

	private String userName = "";

	@RequestMapping(value = "/loadpage", method = RequestMethod.POST)
	public ModelAndView loadPage(ModelMap modelMap, @RequestParam("d") String pg, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		modelMap.addAttribute("userName", userName);
		return new ModelAndView(pg, modelMap);
	}

	@RequestMapping(value = "/insertAddActivity", method = RequestMethod.POST)
	@ResponseBody
	public AddActivityVO insertAddActivity(@ModelAttribute AddActivityVO addactivityVO) throws Exception {
		System.out.println("Enter Controller Activity");
		//System.out.println("value 1  " + addactivityVO.getActivity_ID() + " value  2  " + addactivityVO.getPlace());
		AddActivityVO AddActivityvo = new AddActivityVO();
		try {
			AddActivityvo = addactivityService.insertAddActivityDetails(addactivityVO);
		} catch (Exception e) {
			System.out.println("Error  ------------->  " + e);
		}
		return AddActivityvo;
	}

	@RequestMapping(value = "/getltrreferencedata", method = RequestMethod.GET)
	@ResponseBody
	public AddActivityVO getLtrReferenceData() throws Exception {
		AddActivityVO Actlistvo = new AddActivityVO();
		try {
			Actlistvo = addactivityService.getLtrReferenceData();
		} catch (Exception e) {
		}
		return Actlistvo;
	}
	
	@RequestMapping(value = "/getClassActivity", method = RequestMethod.GET)
	@ResponseBody
	public AddActivityVO getClassActivity(@RequestParam("aclass") String aclass,@RequestParam("asec") String asec,@RequestParam("ttype") String ttype) throws Exception {
		AddActivityVO Actlistvo = new AddActivityVO();
		try {
			Actlistvo = addactivityService.getClassActivity(aclass,asec,ttype);
		} catch (Exception e) {
		}
		return Actlistvo;
	}
	
	@RequestMapping(value = "/getActivitybyType", method = RequestMethod.GET)
	@ResponseBody
	public AddActivityVO getActivitybyType(@RequestParam("aclass") String aclass,@RequestParam("asec") String asec,@RequestParam("Place") String place) throws Exception {
		AddActivityVO Actlistvo = new AddActivityVO();
		try {
			Actlistvo = addactivityService.getActivitybyType(aclass,asec,place);
		} catch (Exception e) {
		}
		return Actlistvo;
	}
	@RequestMapping(value = "/getActivitybyType2", method = RequestMethod.GET)
	@ResponseBody
	public AddActivityVO getActivitybyType2(@RequestParam("aclass") String aclass,@RequestParam("asec") String asec,@RequestParam("Place") String place) throws Exception {
		AddActivityVO Actlistvo = new AddActivityVO();
		try {
			Actlistvo = addactivityService.getActivitybyType2(aclass,asec,place);
		} catch (Exception e) {
		}
		return Actlistvo;
	}
	
	

}
