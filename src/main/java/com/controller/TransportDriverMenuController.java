package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dao.constant.CommonConstant;

@Controller
@RequestMapping("/transportDriverMenu")
public class TransportDriverMenuController
{
	String urlnew=CommonConstant.PATH_URL;
	
	//Dashboard
    @RequestMapping(value = "/DriverDashboard", method = RequestMethod.GET)
    public ModelAndView Driver_Dashboard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
    throws Exception
    {
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Dashboard/TransportDriver_Dashboard.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
    }

    
  //Transport List
    @RequestMapping(value = "/TransportList", method = RequestMethod.GET)
    public ModelAndView TransportList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
    throws Exception
    {
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Transport/TransportList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
    }
    

	//StudentList
	@RequestMapping(value = "/StudentDetails", method = RequestMethod.GET)
	public ModelAndView StudentDetails(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				
		//return new ModelAndView("/library/DuedateList", modelMap);
				String projectUrl=urlnew+"/Transport/StudentDetails.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				return new ModelAndView("redirect:"+projectUrl,modelMap);
	}
	
	//StudentAttendance
		@RequestMapping(value = "/StudentAttendance", method = RequestMethod.GET)
		public ModelAndView StudentAttendance(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					
			//return new ModelAndView("/library/DuedateList", modelMap);
					String projectUrl=urlnew+"/Transport/StudentAttendance.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
		}
		
		//Attendance History
				@RequestMapping(value = "/AttendanceHistory", method = RequestMethod.GET)
				public ModelAndView AttendanceHistorye(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Transport/AttendanceHistory.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//noticeboard
				@RequestMapping(value = "/noticeboard", method = RequestMethod.GET)
				public ModelAndView noticeboard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Noticeboard/TransDriver_NewsList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				//noticeboard
				@RequestMapping(value = "/AOnoticeboard", method = RequestMethod.GET)
				public ModelAndView AOnoticeboard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,  HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Noticeboard/TransportNewsList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
		//calendar
				@RequestMapping(value = "/calendar", method = RequestMethod.GET)
				public ModelAndView calendar(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Calendar/TransDriver_StuAttendance.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//NewRoute		
				@RequestMapping(value = "/NewRoute", method = RequestMethod.GET)
				public ModelAndView NewRoute(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							String projectUrl=urlnew+"/Transport/TransportDriverAdd.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
				
				
	//Tranport AO MENU
				
				//AO Attendance History
				@RequestMapping(value = "/AOAttendanceHistory", method = RequestMethod.GET)
				public ModelAndView AOAttendanceHistory(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Transport/AOAttendanceHistory.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
				
				//AOStudentList
					@RequestMapping(value = "/AOStudentList", method = RequestMethod.GET)
					public ModelAndView AOStudentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/Transport/AOStudentList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
				
				
					//Dashboard
							@RequestMapping(value = "/Dashboard", method = RequestMethod.GET)
							public ModelAndView Dashboard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										
								//return new ModelAndView("/library/DuedateList", modelMap);
										String projectUrl=urlnew+"/Dashboard/TransportMain_DashBoard.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}
					//AOCalendar
							@RequestMapping(value = "/AOCalendar", method = RequestMethod.GET)
							public ModelAndView AOCalendar(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										
								//return new ModelAndView("/library/DuedateList", modelMap);
										String projectUrl=urlnew+"/Calendar/TransAO_StuAttendance.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}
				
			//Dormitory Page
							
							
							
							//Dashboard_Dormitory
							@RequestMapping(value = "/Dashboard_Dormitory", method = RequestMethod.GET)
							public ModelAndView Dashboard_Dormitory(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										
								//return new ModelAndView("/library/DuedateList", modelMap);
										String projectUrl=urlnew+"/Dashboard/Dormitory_Dashboard.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}
							
							
							//AODormitoryStuList
							@RequestMapping(value = "/AODormitoryStuList", method = RequestMethod.GET)
							public ModelAndView AODormitoryStuList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										String projectUrl=urlnew+"/Dormitory/Dor_list.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}	
							
							
							//AODormitoryStuList
							@RequestMapping(value = "/AODormitoryAdd", method = RequestMethod.GET)
							public ModelAndView AODormitoryAdd(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										String projectUrl=urlnew+"/Dormitory/dormitory_add.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}	
							//AODormitoryEdit
							@RequestMapping(value = "/AODormitoryEdit", method = RequestMethod.GET)
							public ModelAndView AODormitoryEdit(ModelMap modelMap,@RequestParam("dname")String dname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										String projectUrl=urlnew+"/Dormitory/Dormitory_Edit.jsp?&dname="+dname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}	
							
							
							
							
							
							
							
							//AODormitoryAddStu
							@RequestMapping(value = "/AODormitoryAddStu", method = RequestMethod.GET)
							public ModelAndView AODormitoryAddStu(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										
								//return new ModelAndView("/library/DuedateList", modelMap);
										String projectUrl=urlnew+"/Dormitory/AO_AddStudent.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}		
							
							//AODormitoryAllotStu
							@RequestMapping(value = "/AODormitoryAllotStu", method = RequestMethod.GET)
							public ModelAndView AODormitoryAllotStu(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										
								//return new ModelAndView("/library/DuedateList", modelMap);
										String projectUrl=urlnew+"/Dormitory/AO_Allotment.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}		
							
							//noticeboard
							@RequestMapping(value = "/Dormitorynoticeboard", method = RequestMethod.GET)
							public ModelAndView Dormitorynoticeboard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										
								//return new ModelAndView("/library/DuedateList", modelMap);
										String projectUrl=urlnew+"/Noticeboard/Dormitory_NewsList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}		
							
							//Dormitory_Calendar
							@RequestMapping(value = "/Dormitory_Calendar", method = RequestMethod.GET)
							public ModelAndView Dormitory_Calendar(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
									throws Exception
							{
										
								//return new ModelAndView("/library/DuedateList", modelMap);
										String projectUrl=urlnew+"/Calendar/Dromitory_Calender.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
										return new ModelAndView("redirect:"+projectUrl,modelMap);
							}
				
				
}
