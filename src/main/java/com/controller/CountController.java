package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.service.CountService;
import com.service.StudentService;

@Controller
@RequestMapping("/count")

public class CountController
{
	@Autowired
	@Qualifier(value = "countService")
	private CountService countService;
	
	//countTeacherLeaveReq
	@RequestMapping(value = "/countTeacherLeaveReq", method = RequestMethod.GET)
	@ResponseBody
	public  int countAbsent() throws Exception 
	{
		int count=countService.countTeacherLeaveReq();
		return count;
	}	
	
}
