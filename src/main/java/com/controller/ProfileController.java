package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.dao.constant.CommonConstant;
import com.model.LoginVO;
import com.service.ProfileService;
import com.service.StudentService;

@Controller
@RequestMapping("/profile")
public class ProfileController 
{
	@Autowired
	@Qualifier(value = "profileService")
	private ProfileService profileService;
	
	private String userName = "";
	private String student_id="";
	private String stuid="";
	String urlnew=CommonConstant.PATH_URL;
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap, @RequestParam("d") String pg, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		modelMap.addAttribute("userName", userName);
		return new ModelAndView(pg, modelMap);
	}
	
	//Parents_Profile					PARENTS PROFILE
	@RequestMapping(value = "/Parents_Profile", method = RequestMethod.GET)
	public ModelAndView Parents_Profile(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				String projectUrl=urlnew+"/Settings/ParentsProfile.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
		 		return new ModelAndView("redirect:"+projectUrl,modelMap);
	}	
	//Students_Profile
	@RequestMapping(value = "/Students_Profile", method = RequestMethod.GET)
	public ModelAndView Students_Profile(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				String projectUrl=urlnew+"/Settings/StudentProfile.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
		 		return new ModelAndView("redirect:"+projectUrl,modelMap);
	}
	//Library_Profile
	@RequestMapping(value = "/Library_Profile", method = RequestMethod.GET)
	public ModelAndView Library_Profile(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				String projectUrl=urlnew+"/Settings/LibraryProfile.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
		 		return new ModelAndView("redirect:"+projectUrl,modelMap);
	}
	
	//Teacher_Profile
		@RequestMapping(value = "/Teacher_Profile", method = RequestMethod.GET)
		public ModelAndView Teacher_Profile(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
					String projectUrl=urlnew+"/Settings/TeacherProfile.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			 		return new ModelAndView("redirect:"+projectUrl,modelMap);
		}
		//Dormitory_Profile
				@RequestMapping(value = "/Dormitory_Profile", method = RequestMethod.GET)
				public ModelAndView Dormitory_Profile(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Settings/DormitoryProfile.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//TransportAO_Profile
				@RequestMapping(value = "/TransportAO_Profile", method = RequestMethod.GET)
				public ModelAndView TransportAO_Profile(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Settings/TransportAOProfile.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//TransportDriver_Profile
				@RequestMapping(value = "/TransportDriver_Profile", method = RequestMethod.GET)
				public ModelAndView TransportDriver_Profile(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Settings/TransportProfile.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
	//photoUpdate					
	@RequestMapping(value = "/photoUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String photoUpdate(@ModelAttribute LoginVO loginVO) throws Exception, Exception 
	{
		System.out.println("controller  "+loginVO.getPhoto()+" and  "+loginVO.getUname());
		LoginVO Loginvo = new LoginVO();

		
		String result = "";
		if (( profileService.photoUpdate(loginVO)) != null) 
		{
			result = "match";
			System.out.println("Controller Result  ok  "+result);
		} else
		{
			result = "Failed...";
		}
		return result;
		
	}	
	
	
/*	// updateParentsPassword
	@RequestMapping(value = "/updateParentsPassword", method = RequestMethod.POST)
	@ResponseBody
	public String updateParentsPassword(@ModelAttribute LoginVO loginVO) throws Exception, Exception {
		LoginVO Mastervo = new LoginVO();
		
		System.out.println("UPDATE   "+(Mastervo));
		 	String result = "";
			if ((Mastervo = profileService.updateParentsPassword(loginVO)) != null) 
			{
				result = "match";
				System.out.println("Controller Result  ok  "+result);
			} else
			{
				result = "Failed...";
			}
			return result;
	}		*/
	
	// updatePassword
	@RequestMapping(value = "/updateMobilePassword", method = RequestMethod.POST)
	@ResponseBody
	public String updateMobilePassword(@ModelAttribute LoginVO loginVO) throws Exception, Exception {
		LoginVO Mastervo = new LoginVO();
		
		System.out.println("UPDATE   "+(Mastervo));
		 	String result = "";
			if ((Mastervo = profileService.updateMobilePassword(loginVO)) != null) 
			{
				result = "match";
				System.out.println("Controller Result  ok  "+result);
			} else
			{
				result = "Failed...";
			}
			return result;
	}		
	

	
	//Dormitory_Profile					DORMITORY PROFILE
	@RequestMapping(value = "/Dormitory_Profile1", method = RequestMethod.GET)
	public ModelAndView Dormitory_Profile1(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				String projectUrl=urlnew+"/Settings/DormitoryProfile.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
		 		return new ModelAndView("redirect:"+projectUrl,modelMap);
	}	
	
	// updateDormitoryPassword
	@RequestMapping(value = "/updateDormitoryPassword", method = RequestMethod.POST)
	@ResponseBody
	public String updateDormitoryPassword(@ModelAttribute LoginVO loginVO) throws Exception, Exception {
		LoginVO Mastervo = new LoginVO();
		
		System.out.println("UPDATE   "+(Mastervo));
		 	String result = "";
			if ((Mastervo = profileService.updateParentsPassword(loginVO)) != null) 
			{
				result = "match";
				System.out.println("Controller Result  ok  "+result);
			} else
			{
				result = "Failed...";
			}
			return result;
	}			

	
	
	
}
