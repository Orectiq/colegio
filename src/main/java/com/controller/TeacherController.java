package com.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.common.StudentVO;
import com.model.AbsenceRequestVO;
import com.model.ClassSectionVO;
import com.model.ExamNewVO;
import com.model.GroupVO;
import com.model.LoginVO;
import com.model.MailBackupVO;
import com.model.MailVO;
import com.model.MedicalVO;
import com.model.ParentVO;
import com.model.StuVO;
import com.model.SubjectAssignVO;
import com.model.TeacherVO;
import com.model.staffAttendanceVO;
import com.model.teacherAttendanceVO;
import com.service.StudentService;
import com.service.TeacherService;

@Controller
@RequestMapping("/teacher")
@SessionAttributes("userName2")
public class TeacherController 
{
	@Autowired
	@Qualifier(value = "teacherService")
	private TeacherService teacherService;
	
	private String userName = "";
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap, @RequestParam("d") String pg, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		modelMap.addAttribute("userName2", userName);
		return new ModelAndView(pg, modelMap);
	}
	
	
	@RequestMapping(value = "/absenceRequest", method = RequestMethod.GET)
	public ModelAndView homeworkListStatus(ModelMap modelMap,@RequestParam("id")String tid, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("tid", tid);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+tid);
				
		return new ModelAndView("/teachers/AbsenceRequest", modelMap);
	}
	//absenceStuApproval
	@RequestMapping(value = "/absenceStuApproval", method = RequestMethod.GET)
	public ModelAndView absenceStuApproval(ModelMap modelMap,@RequestParam("id")String tid, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("tid", tid);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+tid);
				
		return new ModelAndView("/Leave/StuLeaveApproval", modelMap);
	}
	
	
	
	
	//absenceTeaHistory
	@RequestMapping(value = "/absenceTeaHistory", method = RequestMethod.GET)
	public ModelAndView absenceTeaHistory(ModelMap modelMap,@RequestParam("id")String tid, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("tid", tid);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+tid);
				
		return new ModelAndView("/teachers/AbsenceHistory", modelMap);
	}
	
	@RequestMapping(value = "/getTeacherId", method = RequestMethod.GET)
	@ResponseBody
	public TeacherVO getTeacherId() throws Exception {

		System.out.println("************** enter getTeacherid controller 1");
		String ref = "TEA";
		
		return teacherService.getTeachId(ref);
	}
	
	@RequestMapping(value = "/teacherInsert", method = RequestMethod.GET)
	@ResponseBody
	public TeacherVO teacherInsert(@ModelAttribute TeacherVO teacherVO) throws Exception, Exception {
		TeacherVO Teachervo = new TeacherVO();
		System.out.println("Teacher Insert conroller############################################################");
		
		try {
			Teachervo = teacherService.insertTeacherDetails(teacherVO);
		
		} catch (Exception e) {
			System.out.println("Controller error "+e);
		}
		return Teachervo;
	}
	
	
	@RequestMapping(value = "/subjectAssignInsert", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public SubjectAssignVO subjectAssignInsert(@ModelAttribute SubjectAssignVO subjectAssignVO) throws Exception, Exception {
		SubjectAssignVO SubjectAssignvo = new SubjectAssignVO();
		System.out.println("Teacher Insert conroller");
		
		try {
			SubjectAssignvo = teacherService.subjectAssignInsert(subjectAssignVO);
		
		} catch (Exception e) {
			System.out.println("Controller error "+e);
		}
		return SubjectAssignvo;
	}
	
	
	//teacherAttenInsert
	@RequestMapping(value = "/teacherAttenInsert", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public teacherAttendanceVO teacherAttenInsert(@ModelAttribute teacherAttendanceVO teacherVO) throws Exception, Exception {
		teacherAttendanceVO Teachervo = new teacherAttendanceVO();
		System.out.println("Teacher Insert conroller");
		
		try {
			Teachervo = teacherService.teacherAttenInsert(teacherVO);
		
		} catch (Exception e) {
			System.out.println("Controller error "+e);
		}
		return Teachervo;
	}
	
	//teacherAttenUpdate
	@RequestMapping(value = "/teacherAttenUpdate", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public teacherAttendanceVO teacherAttenUpdate(@ModelAttribute teacherAttendanceVO teacherVO) throws Exception, Exception {
		teacherAttendanceVO Teachervo = new teacherAttendanceVO();
		System.out.println("Teacher Insert conroller");
		
		try {
			Teachervo = teacherService.teacherAttenUpdate(teacherVO);
		
		} catch (Exception e) {
			System.out.println("Controller error "+e);
		}
		return Teachervo;
	}
	
	
	//staffAttenInsert
		@RequestMapping(value = "/staffAttenInsert", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
		@ResponseBody
		public staffAttendanceVO staffAttenInsert(@ModelAttribute staffAttendanceVO teacherVO) throws Exception, Exception {
			staffAttendanceVO Teachervo = new staffAttendanceVO();
			System.out.println("Teacher Insert conroller");
			
			try {
				Teachervo = teacherService.staffAttenInsert(teacherVO);
			
			} catch (Exception e) {
				System.out.println("Controller error "+e);
			}
			return Teachervo;
		}
	//composeMail
				@RequestMapping(value = "/composeMail", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
				@ResponseBody
				public MailVO compose(@ModelAttribute MailVO mailVO) throws Exception, Exception {
					MailVO Mailvo = new MailVO();
					System.out.println("Teacher Insert conroller");
					
					try {
						Mailvo = teacherService.composeMail(mailVO);
					
					} catch (Exception e) {
						System.out.println("Controller error "+e);
					}
					return Mailvo;
				}
		//groupMail
				@RequestMapping(value = "/groupMail", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
				@ResponseBody
				public GroupVO groupMail(@ModelAttribute GroupVO groupVO) throws Exception, Exception {
					GroupVO Groupvo = new GroupVO();
					System.out.println("Group Insert conroller");
					
					try {
						Groupvo = teacherService.groupMail(groupVO);
					
					} catch (Exception e) {
						System.out.println("Controller error "+e);
					}
					return Groupvo;
				}		
				
				
		//deleteMail
				@RequestMapping(value = "/deleteMail", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
				@ResponseBody
				public MailVO deleteMail(@ModelAttribute MailVO mailVO) throws Exception, Exception {
					MailVO Mailvo = new MailVO();
					System.out.println("Teacher delete conroller --------------------------------->   "+mailVO.getMdate()+" and "+mailVO.getMtime());
					
					try {
						Mailvo = teacherService.deleteMail(mailVO);
					
					} catch (Exception e) {
						System.out.println("Controller error "+e);
					}
					return Mailvo;
				}
				
				
				
		//staffAttenUpdate
		@RequestMapping(value = "/staffAttenUpdate", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
		@ResponseBody
		public staffAttendanceVO staffAttenUpdate(@ModelAttribute staffAttendanceVO teacherVO) throws Exception, Exception {
			staffAttendanceVO Teachervo = new staffAttendanceVO();
			System.out.println("Teacher Insert conroller");
			
			try {
				Teachervo = teacherService.staffAttenUpdate(teacherVO);
			
			} catch (Exception e) {
				System.out.println("Controller error "+e);
			}
			return Teachervo;
		}
	
	
	
		
	
	
	//Show Records
	@RequestMapping(value = "/getltrreferencedataAll", method = RequestMethod.GET)
	@ResponseBody
	public  TeacherVO getltrreferencedataAll(@RequestParam("Tid") String techid) throws Exception 
	{
		System.out.println("enter controller  "+techid);
		return teacherService.getLtrReferenceDataAll(techid);
	}
	
	@RequestMapping(value = "/getClassSectionAll", method = RequestMethod.GET)
	@ResponseBody
	public  ClassSectionVO getClassSectionAll(@RequestParam("Tid") String techid) throws Exception 
	{
		System.out.println("enter controller  "+techid);
		return teacherService.getClassSectionAll(techid);
	}
	
	
	
	
	//getId
	@RequestMapping(value = "/getId", method = RequestMethod.GET)
	@ResponseBody
	public  TeacherVO getId(@RequestParam("uname") String uname) throws Exception 
	{
		System.out.println("enter controller  ");
		return teacherService.getId(uname);
	}
	
	
	//getSClassSection
	@RequestMapping(value = "/getSClassSection", method = RequestMethod.GET)
	@ResponseBody
	public  TeacherVO getSClassSection(@RequestParam("ttype") String ttype,@RequestParam("mobile") String mobile) throws Exception 
	{
		System.out.println("enter controller  "+ttype+"  and  "+mobile);
		return teacherService.getSClassSection(ttype,mobile);
	}
	
	//getSClassSectionAll
		@RequestMapping(value = "/getSClassSectionAll", method = RequestMethod.GET)
		@ResponseBody
		public  TeacherVO getSClassSectionAll(@RequestParam("ttype") String ttype,@RequestParam("mobile") String mobile) throws Exception 
		{
			System.out.println("enter controller  "+ttype+"  and  "+mobile);
			return teacherService.getSClassSectionAll(ttype,mobile);
		}
	//getCheckin
		@RequestMapping(value = "/getCheckin", method = RequestMethod.GET)
		@ResponseBody
		public  teacherAttendanceVO getCheckin(@RequestParam("adate") String adate,@RequestParam("tid") String tid) throws Exception 
		{
			System.out.println("enter controller  "+adate+"  and  "+tid);
			return teacherService.getCheckin(adate,tid);
		}
		
		//getStaffCheckin
				@RequestMapping(value = "/getStaffCheckin", method = RequestMethod.GET)
				@ResponseBody
				public  staffAttendanceVO getStaffCheckin(@RequestParam("adate") String adate,@RequestParam("tid") String tid) throws Exception 
				{
					System.out.println("enter controller  "+adate+"  and  "+tid);
					return teacherService.getStaffCheckin(adate,tid);
				}
		
		
		
	//getStaffAttendance	
		@RequestMapping(value = "/getStaffAttendance", method = RequestMethod.GET)
		@ResponseBody
		public  teacherAttendanceVO getStaffAttendance(@RequestParam("fdate") String fdate,@RequestParam("tdate") String tdate) throws Exception 
		{
			System.out.println("enter controller  "+fdate+tdate);
			return teacherService.getStaffAttendance(fdate,tdate);
		}
	
	
	//getTeacherSubject
	@RequestMapping(value="/getTeacherSubject",method=RequestMethod.GET)
	@ResponseBody
	public TeacherVO getTeacherSubject(@RequestParam("sclass")String sclass,@RequestParam("section")String sec)throws Exception
	{
		return teacherService.getTeacherSubject(sclass,sec);
	}
	
	//mailInbox
		@RequestMapping(value="/mailInbox",method=RequestMethod.GET)
		@ResponseBody
		public MailVO mailInbox(@RequestParam("mail_to")String mailto)throws Exception
		{
			return teacherService.mailInbox(mailto);
		}
		
	//getGroupMail
		@RequestMapping(value="/getGroupMail",method=RequestMethod.GET)
		@ResponseBody
		public GroupVO getGroupMail(@RequestParam("group_name")String gname)throws Exception
		{
			System.out.println("controller  groupmail  "+gname);
			
			return teacherService.getGroupMail(gname);
		}		
		
		
		//mailInboxDate
				@RequestMapping(value="/mailInboxDate",method=RequestMethod.GET)
				@ResponseBody
				public MailVO mailInboxDate(@RequestParam("mail_to")String mailto,@RequestParam("mdate")String mdate)throws Exception
				{
					return teacherService.mailInboxDate(mailto,mdate);
				}	
		//mailInboxDateTime
				@RequestMapping(value="/mailInboxDateTime",method=RequestMethod.GET)
				@ResponseBody
				public MailVO mailInboxDateTime(@RequestParam("mail_to")String mailto,@RequestParam("mdate")String mdate,@RequestParam("mtime")String mtime)throws Exception
				{
					return teacherService.mailInboxDateTime(mailto,mdate,mtime);
				}	
		
		//mailSend
				@RequestMapping(value="/mailSend",method=RequestMethod.GET)
				@ResponseBody
				public MailVO mailSend(@RequestParam("mail_from")String mailto)throws Exception
				{
					return teacherService.mailSend(mailto);
					
				}
		//mailTrash
				@RequestMapping(value="/mailTrash",method=RequestMethod.GET)
				@ResponseBody
				public MailBackupVO mailTrash(@RequestParam("mail_to")String mto)throws Exception
				{
					System.out.println("Trash controller  "+mto);
					return teacherService.mailTrash(mto);
				}		
	
	
	
	
	//getSubjectAssign
		@RequestMapping(value="/getSubjectAssign",method=RequestMethod.GET)
		@ResponseBody
		public SubjectAssignVO getSubjectAssign(@RequestParam("sclass")String sclass,@RequestParam("section")String sec)throws Exception
		{
			return teacherService.getSubjectAssign(sclass,sec);
		}
		
		//getSubjectAssignTeacher
				@RequestMapping(value="/getSubjectAssignTeacher",method=RequestMethod.GET)
				@ResponseBody
				public SubjectAssignVO getSubjectAssignTeacher(@RequestParam("sclass")String sclass,@RequestParam("section")String sec,@RequestParam("tid")String tid)throws Exception
				{
					return teacherService.getSubjectAssignTeacher(sclass,sec,tid);
				}	
		//getSubjectAssignTeacherID
				@RequestMapping(value="/getSubjectAssignTeacherID",method=RequestMethod.GET)
				@ResponseBody
				public SubjectAssignVO getSubjectAssignTeacherID(@RequestParam("tid")String tid)throws Exception
				{
					return teacherService.getSubjectAssignTeacherID(tid);
				}
				
	
				//getSubjectAssignTeacherID
				@RequestMapping(value="/getSubjectAssignTeacherType",method=RequestMethod.GET)
				@ResponseBody
				public SubjectAssignVO getSubjectAssignTeacherType(@RequestParam("ttype")String ttype,@RequestParam("mobile")String mobile)throws Exception
				{
					return teacherService.getSubjectAssignTeacherType(ttype,mobile);
				}
				
				//getSubjectAssignTeacherClass
				@RequestMapping(value="/getSubjectAssignTeacherClass",method=RequestMethod.GET)
				@ResponseBody
				public SubjectAssignVO getSubjectAssignTeacherClass(@RequestParam("sclass")String sclass,@RequestParam("ttype")String ttype,@RequestParam("mobile")String mobile)throws Exception
				{
					return teacherService.getSubjectAssignTeacherClass(sclass,ttype,mobile);
				}		
		
				//getSubjectAssignTeacherClassSection
				@RequestMapping(value="/getSubjectAssignTeacherClassSection",method=RequestMethod.GET)
				@ResponseBody
				public SubjectAssignVO getSubjectAssignTeacherClassSection(@RequestParam("sclass")String sclass,@RequestParam("section")String section,@RequestParam("ttype")String ttype,@RequestParam("mobile")String mobile)throws Exception
				{
					return teacherService.getSubjectAssignTeacherClassSection(sclass,section,ttype,mobile);
				}		
				
				
				
				
				
				
				
	
	@RequestMapping(value = "/getltrreferencedata", method = RequestMethod.GET)
	@ResponseBody
	public TeacherVO getLtrReferenceData() throws Exception {
		TeacherVO Teachervo = new TeacherVO();
		try{
			Teachervo =teacherService.getltrreferencedata();
		}
		catch(Exception e){
		}
		return Teachervo;
	}
	

	@RequestMapping(value = "/getDLlist", method = RequestMethod.GET)
	@ResponseBody
		public GroupVO getDLlist() throws Exception 
	{
		GroupVO Grpvo = new GroupVO();
						try{
							Grpvo =teacherService.getDLlist();
						}
						catch(Exception e){
						}
						return Grpvo;
			}
	//getgrplist
	@RequestMapping(value="/getgroup",method=RequestMethod.GET)
	@ResponseBody
	public GroupVO getgroup(@RequestParam("group_namename")String groupname)throws Exception
	{
		return teacherService.getgroup(groupname);
	}		
	
	// getstudentid
	@RequestMapping(value = "/getlogindetail", method = RequestMethod.GET,headers="Accept=application/json")@ResponseBody
    public LoginVO getlogindetail() throws Exception
		{
			System.out.println("enter controllerStudent ID  ");
			return teacherService.getlogindetail();
		}
	
	// getlogindetailAll

		@RequestMapping(value = "/getlogindetailAll", method = RequestMethod.GET,headers="Accept=application/json")@ResponseBody
	    public LoginVO getlogindetailAll() throws Exception
			{
				System.out.println("enter controllerStudent ID  ");
				return teacherService.getlogindetailAll();

			}
		
	
}
