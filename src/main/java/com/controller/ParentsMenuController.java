package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dao.constant.CommonConstant;

@Controller
@RequestMapping("/parentMenu")
public class ParentsMenuController
{
	String urlnew=CommonConstant.PATH_URL;
	
	//parentHomeworkApproval
		@RequestMapping(value = "/parentHomeworkApproval", method = RequestMethod.GET)
		public ModelAndView parentHomeworkApproval(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					
			//return new ModelAndView("/library/DuedateList", modelMap);
					String projectUrl=urlnew+"/homework/ParentHomeworkApproval.jsp?&mobile="+mobile+"&url="+url+"&ltype="+slogo+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
		}

		
		//feesparentlist
			@RequestMapping(value = "/FeesParentList", method = RequestMethod.GET)
			public ModelAndView FeesParentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
					throws Exception
			{
						
				//return new ModelAndView("/library/DuedateList", modelMap);
						String projectUrl=urlnew+"/Accounts/FeesParentList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
						return new ModelAndView("redirect:"+projectUrl,modelMap);
			}
			
			//Exam schedule
					@RequestMapping(value = "/ExamparentList", method = RequestMethod.GET)
					public ModelAndView ExamParentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/Exam/ExamparentList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
					
					//AttendanceParent
					@RequestMapping(value = "/AttendanceParents", method = RequestMethod.GET)
					public ModelAndView AttendanceParents(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/Attendance/AttendanceParents.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
		        
					//marks_parentlist
					@RequestMapping(value = "/Marks_ParentListNew", method = RequestMethod.GET)
					public ModelAndView Marks_ParentListNew(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/MarkSheet/Marks_ParentListNew.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
					
					
					//Main Dashboard
					@RequestMapping(value = "/Dashboard", method = RequestMethod.GET)
					public ModelAndView Main_Dashboard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/Dashboard/Parents_DashBoard.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
					
					//ParentsNoticeboard
					@RequestMapping(value = "/ParentsNoticeboard", method = RequestMethod.GET)
					public ModelAndView ParentsNoticeboard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								String projectUrl=urlnew+"/Noticeboard/Parents_NewsList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
					//CalendarStudents
					@RequestMapping(value = "/CalendarStudents", method = RequestMethod.GET)
					public ModelAndView CalendarStudents(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								String projectUrl=urlnew+"/Calendar/Parent_StudentAttendance.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
					
					//ParentsAssignment
					@RequestMapping(value = "/ParentsAssignment", method = RequestMethod.GET)
					public ModelAndView ParentsAssignment(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								String projectUrl=urlnew+"/Assignment/Parents_Assignment.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
				
					
					//STUDENT MENU CONTROLLER
					
					//Main Dashboard
					@RequestMapping(value = "/StudentDashBoard", method = RequestMethod.GET)
					public ModelAndView StudentDashBoard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/Dashboard/Student_DashBoard.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
				
					//Marks
					@RequestMapping(value = "/MarksStudentList", method = RequestMethod.GET)
					public ModelAndView MarksStudentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/MarkSheet/Marks_StudentListNew.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
					//AttendanceStudent
					@RequestMapping(value = "/AttendanceStudent", method = RequestMethod.GET)
					public ModelAndView AttendanceStudent(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/Attendance/AttendanceStudent.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
					
					//ExamStudentList
					@RequestMapping(value = "/ExamStudentList", method = RequestMethod.GET)
					public ModelAndView ExamStudentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/Exam/ExamStudentList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
					//FeesStudentList
					@RequestMapping(value = "/FeesStudentList", method = RequestMethod.GET)
					public ModelAndView FeesStudentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"/Accounts/FeesStudentList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
					
					//HomeworkStudentList
					@RequestMapping(value = "/HomeworkStudentList", method = RequestMethod.GET)
					public ModelAndView HomeworkStudentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype, HttpServletRequest request, HttpServletResponse response)
							throws Exception
					{
								
						//return new ModelAndView("/library/DuedateList", modelMap);
								String projectUrl=urlnew+"homework/HomworStuHisList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype;
								return new ModelAndView("redirect:"+projectUrl,modelMap);
					}
}
