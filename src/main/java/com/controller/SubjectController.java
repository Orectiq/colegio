package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.model.MenuItemVO;
import com.model.SectionVO;
import com.model.StuVO;
import com.model.SubjectVO;
import com.model.TeacherVO;
import com.service.SubjectService;


//@Controller
//@RestController
@RequestMapping("/subject")
@SessionAttributes("userName2")
public class SubjectController {

	@Autowired
	@Qualifier(value = "subjectService")
	private SubjectService subjectService;
	
	private String userName1="";
	
	
	

	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap,@RequestParam("d") String pg, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				modelMap.addAttribute("userName2", userName1);
		return new ModelAndView(pg, modelMap);
	}
	
	

	@RequestMapping(value = "/insertSubject", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public SubjectVO insertAssignment(@ModelAttribute SubjectVO subjectVO) throws Exception, Exception {
		SubjectVO Subjectvo = new SubjectVO();
		try {
			Subjectvo = subjectService.insertSubjectDetails(subjectVO);
				} catch (Exception e) {
		}
		return Subjectvo;
	}
	//
	
	
	@RequestMapping(value = "/insertClass", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public SubjectVO insertClass(@RequestParam(value="myArray[]") String[] myArray) throws Exception, Exception {
		SubjectVO Subjectvo = new SubjectVO();
		try {
			Subjectvo = subjectService.insertClass(myArray);
				} catch (Exception e) {
		}
		return Subjectvo;
	}
	
	
	
	//deleteSubject
	@RequestMapping(value = "/deleteSubject", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
			@ResponseBody
			public SubjectVO deleteSubject(@ModelAttribute SubjectVO subjectVO) throws Exception
			{
				return subjectService.deleteSubject(subjectVO);
			}
	
	
	
	
	//insertSection
	@RequestMapping(value="/insertSection", method=RequestMethod.POST,headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public SectionVO insertSection(@ModelAttribute SectionVO sectionVO)throws Exception
	{
		System.out.println("Controller");
		SectionVO Sectionvo=new SectionVO();
		try{
			Sectionvo=subjectService.insertSection(sectionVO);
		}catch (Exception e) {
		}
		return Sectionvo;
	}
	
	
	
	
	@RequestMapping(value = "/getSubject", method = RequestMethod.GET)
	@ResponseBody
	public TeacherVO getSubject(@RequestParam("sclass") String sclass,@RequestParam("section") String section,@RequestParam("ttype") String ttype,@RequestParam("Tid") String tid) throws Exception
	{
		
		System.out.println("enter controller Assignment  "+sclass+" and "+section);
		return subjectService.getSubject(sclass,section,ttype,tid);
	}
	
	@RequestMapping(value = "/getSClassMaster", method = RequestMethod.GET,headers="Accept=application/json")
	@ResponseBody
	public SubjectVO getSClassMaster() throws Exception
	{
		
		System.out.println("enter controller Assignment  ");
		return subjectService.getSClassMaster();
	}
	
	
	//getTeacherSubject
	@RequestMapping(value = "/getTeacherSubject", method = RequestMethod.GET)
	@ResponseBody
	public SubjectVO getTeacherSubject(@RequestParam("sclass") String sclass,@RequestParam("section") String section) throws Exception
	{
		
		System.out.println("enter controller Assignment ---> "+sclass+" and "+section);
		return subjectService.getTeacherSubject(sclass,section);
	}
	
	
	
	
	@RequestMapping(value = "/getSClass", method = RequestMethod.GET,headers="Accept=application/json")
	@ResponseBody
	public SectionVO getSClass() throws Exception
	{
		
		System.out.println("enter controller Assignment  ");
		return subjectService.getSClass();
	}
	
	//getMenuData
	@RequestMapping(value = "/getMenuData", method = RequestMethod.GET,headers="Accept=application/json")
	@ResponseBody
	public MenuItemVO getMenuData(@RequestParam("mtype") String mtype) throws Exception
	{
		
		System.out.println("enter controller Assignment  ");
		return subjectService.getMenuData(mtype);
	}
	
	
	//getSection
	@RequestMapping(value = "/getSection", method = RequestMethod.GET)
	@ResponseBody
	public SectionVO getSection(@RequestParam("sclass") String sclass) throws Exception
	{
		
		System.out.println("enter controller Assignment  ");
		return subjectService.getSection(sclass);
	}
	
	
	
}
