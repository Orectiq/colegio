package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.dao.constant.CommonConstant;
import com.model.EchallanStudentVO;
import com.model.EchallanVO;
import com.model.StuVO;
import com.service.EchallanService;

@Controller
@RequestMapping("/Echallan")
@SessionAttributes("userName")
public class EchallanController {
	
	String urlnew=CommonConstant.PATH_URL;
	
	@Autowired
	@Qualifier(value = "echallanService")
	private EchallanService echallanService;
	
	private String userName = "";
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.POST)
	public ModelAndView loadPage(ModelMap modelMap, @RequestParam("d") String pg, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		modelMap.addAttribute("userName", userName);
		return new ModelAndView(pg, modelMap);
	}
	
	@RequestMapping(value = "/insertEchallan", method = RequestMethod.POST)
	@ResponseBody
	public String insertEchallan(@ModelAttribute EchallanVO echallanVO) throws Exception 
	{
		System.out.println("challan controller insert ");
		
		EchallanVO Echallanvo = new EchallanVO();
		

		try {
			Echallanvo = echallanService.insertEchallanDetails(echallanVO);
				} catch (Exception e) {
		}
		return "success";
	}
	//sendEchallan
	@RequestMapping(value = "/sendEchallan", method = RequestMethod.POST)
	@ResponseBody
	public String sendEchallan(@ModelAttribute EchallanVO echallanVO) throws Exception 
	{
		System.out.println("challan controller update ");
		
		EchallanVO Echallanvo = new EchallanVO();
		

		try {
			Echallanvo = echallanService.sendEchallanDetails(echallanVO);
				} catch (Exception e) {
		}
		return "success";
	}
	
	@RequestMapping(value = "/getChallanId", method = RequestMethod.GET)
	@ResponseBody
	public EchallanVO getChallanId() throws Exception {

		System.out.println("enter getstuid controller ");
		String ref = "CHA";
		return echallanService.getChallanId(ref);
	}
	
	@RequestMapping(value = "/getFeeDraft", method = RequestMethod.GET)
	@ResponseBody
	public  EchallanVO getFeeDraft(@RequestParam("sclass") String sclass) throws Exception 
	{
		System.out.println("enter controller  "+sclass);
		return echallanService.getFeeDraft(sclass);
	}
	
	@RequestMapping(value = "/getFeeDraftpage", method = RequestMethod.POST)
	public ModelAndView markEntry(ModelMap modelMap,@RequestParam("obj1")String sclass,@RequestParam("obj2")String admid,@RequestParam("obj3")String url,@RequestParam("obj4")String ltype,@RequestParam("obj5")String slogo,@RequestParam("obj6")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				
				System.out.println("request value of        "+sclass+"url"+url+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
				
				String projectUrl=urlnew+"/Accounts/Accounting_FeeDraft.jsp?&admid="+admid+"&sclass="+sclass+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				return new ModelAndView("redirect:"+projectUrl,modelMap);
		//return new ModelAndView("/Accounting/Accounting_FeeDraft", modelMap);
	}
	
	
	
	
	@RequestMapping(value = "/getFeepage", method = RequestMethod.POST)
	public ModelAndView getFeepage(ModelMap modelMap,@RequestParam("obj1")String sclass,@RequestParam("obj2")String admid,@RequestParam("obj3")String url,@RequestParam("obj4")String ltype,@RequestParam("obj5")String slogo,@RequestParam("obj6")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				/*request.setAttribute("Sclass", sclass);
				request.setAttribute("admid", admid);*/
				System.out.println("request value of        "+sclass+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader+"url"+url);

				String projectUrl=urlnew+"/Accounts/Accounting_FeesList.jsp?&admid="+admid+"&sclass="+sclass+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				return new ModelAndView("redirect:"+projectUrl,modelMap);
		//return new ModelAndView("/Accounting/Accounting_FeesList", modelMap);
	}
	
	
	@RequestMapping(value = "/getFee", method = RequestMethod.GET)
	@ResponseBody
	public  EchallanVO getFee(@RequestParam("sclass") String sclass) throws Exception 
	{
		System.out.println("enter controller ---> "+sclass);
		return echallanService.getFee(sclass);
	}

	//getStudentFee
	@RequestMapping(value = "/getStudentFee", method = RequestMethod.GET)
	@ResponseBody
	public  EchallanStudentVO getStudentFee(@RequestParam("student_id") String sclass) throws Exception 
	{
		System.out.println("enter controller in student FEES  "+sclass);
		return echallanService.getStudentFee(sclass);
	}
	
	//getChallanFeeID
		@RequestMapping(value = "/getChallanFeeID", method = RequestMethod.GET)
		@ResponseBody
		public  EchallanVO getChallanFeeID(@RequestParam("challan_id") String cid) throws Exception 
		{
			System.out.println("enter controller in student FEES  "+cid);
			return echallanService.getChallanFeeID(cid);
		}
	
		//getChallanDraftID
				@RequestMapping(value = "/getChallanDraftID", method = RequestMethod.GET)
				@ResponseBody
				public  EchallanVO getChallanDraftID(@RequestParam("challan_id") String cid) throws Exception 
				{
					System.out.println("enter controller in student FEES  "+cid);
					return echallanService.getChallanDraftID(cid);
				}
				
				
	
}
