package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.model.HomeworkStatusVO;
import com.model.HomeworkVO;
import com.model.StuVO;
import com.service.HomeworkService;



@Controller
@RequestMapping("/Homework")
@SessionAttributes("userName2")
public class HomeworkController {
	
	@Autowired
	@Qualifier(value = "homeworkService")
	private HomeworkService homeworkService;
	
	private String userName = "";
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap, @RequestParam("d") String pg, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		modelMap.addAttribute("userName2", userName);
		return new ModelAndView(pg, modelMap);
	}
	
	
	@RequestMapping(value = "/homeworkAdd", method = RequestMethod.GET)
	public ModelAndView homeworkList(ModelMap modelMap,@RequestParam("id")String uname, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("uname", uname);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+uname);
				
		return new ModelAndView("/HomeWork/HomeWorkAdd", modelMap);
	}
	//homeworkStuList
	@RequestMapping(value = "/homeworkStuList", method = RequestMethod.GET)
	public ModelAndView homeworkStuList(ModelMap modelMap,@RequestParam("id")String sid, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("sid", sid);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
				
		return new ModelAndView("/HomeWork/HomeWorkStudentList", modelMap);
	}
	
	//homeworkListStatus
	@RequestMapping(value = "/homeworkListStatus", method = RequestMethod.GET)
	public ModelAndView homeworkListStatus(ModelMap modelMap,@RequestParam("id")String tid, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("tid", tid);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+tid);
				
		return new ModelAndView("/HomeWork/Teacher_HW_StatusList", modelMap);
	}
	
	@RequestMapping(value = "/insertHomework", method = RequestMethod.POST)
	@ResponseBody
	public HomeworkVO insertHomework(@ModelAttribute HomeworkVO homeworkVO) throws Exception 
	{
		
		HomeworkVO Homeworkvo = new HomeworkVO();
		
		System.out.println("controller homework");
		
		System.out.println(homeworkVO.getTheDate());
		System.out.println("controller homework end");
		
		
		try {
			Homeworkvo = homeworkService.insertHomeworkDetails(homeworkVO);
				} catch (Exception e) {
		}
		return Homeworkvo;
	}
	
	
	@RequestMapping(value = "/insertHomeworkStatus", method = RequestMethod.POST)
	@ResponseBody
	public HomeworkStatusVO insertHomeworkStatus(@ModelAttribute HomeworkStatusVO homeworkstatusVO) throws Exception, Exception {
		HomeworkStatusVO HomeWorkvo = new HomeworkStatusVO();
		System.out.println("******* Controller Start *******");
		//System.out.println("Home work date  " + homeworkstatusVO.getTheDate());
		System.out.println("******* Controller end *******");
		
		try {
			HomeWorkvo = homeworkService.insertHomeworkStatus(homeworkstatusVO);
		} catch (Exception e) {
		}
		return HomeWorkvo;
	}
	
	//insertHomeworkStatus
	/*@RequestMapping(value = "/insertHomeworkStatus", method = RequestMethod.POST)
	@ResponseBody
	public HomeworkStatusVO insertHomeworkStatus(@ModelAttribute HomeworkStatusVO homeworkVO) throws Exception 
	{
		
		HomeworkStatusVO Homeworkvo = new HomeworkStatusVO();
		
		System.out.println("controller homework");
		
		System.out.println(homeworkVO.getThedate());
		
		System.out.println("controller homework end");
		
		
		try {
			Homeworkvo = homeworkService.insertHomeworkStatus(homeworkVO);
				} catch (Exception e) {
		}
		return Homeworkvo;
	}*/
	
	@RequestMapping(value = "/getltrreferencedata", method = RequestMethod.GET)
	@ResponseBody
	public HomeworkVO getLtrReferenceData() throws Exception
	{
		HomeworkVO Homeworkvo = new HomeworkVO();
		
		
		try{
			Homeworkvo =homeworkService.getLtrReferenceData();
		}
		catch(Exception e){
		}
		return Homeworkvo;
	}
	
	//getHomeworkdata  // data: {"SClass1":sclass,"Section1":section,"ttype":ttype,"Teacher_Id":tid},
	@RequestMapping(value = "/getHomeworkdata", method = RequestMethod.GET)
	@ResponseBody
	public HomeworkVO getHomeworkdata(@RequestParam("SClass1") String sclass,@RequestParam("Section1") String sec,@RequestParam("ttype") String ttype,@RequestParam("Teacher_Id") String tid,@RequestParam("theDate") String dt) throws Exception
	{
		HomeworkVO Homeworkvo = new HomeworkVO();
		
		
		try{
			Homeworkvo =homeworkService.getHomeworkdata(sclass,sec,ttype,tid,dt);
		}
		catch(Exception e){
		}
		return Homeworkvo;
	}
	//getHomeworkStatus  student_id
	@RequestMapping(value = "/getHomeworkStatus", method = RequestMethod.GET)
	@ResponseBody
	public HomeworkStatusVO getHomeworkStatus(@RequestParam("teacher_id") String tid,@RequestParam("theDate") String thedate,@RequestParam("student_id") String sid) throws Exception
	{
		HomeworkStatusVO Homeworkvo = new HomeworkStatusVO();
		
		
		try{
			Homeworkvo =homeworkService.getHomeworkStatus(tid,thedate,sid);
		}
		catch(Exception e){
		}
		return Homeworkvo;
	}
	
	
	//getHomeworStukdata   data: {"sClass1":sclass,"Section1":sec,"theDate":today},
	@RequestMapping(value = "/getHomeworStudata", method = RequestMethod.GET)
	@ResponseBody
	public HomeworkVO getHomeworStudata(@RequestParam("sClass1") String sclass,@RequestParam("Section1") String sec,@RequestParam("theDate") String thedate) throws Exception
	{
		HomeworkVO Homeworkvo = new HomeworkVO();
		
		
		try{
			Homeworkvo =homeworkService.getHomeworStudata(sclass,sec,thedate);
		}
		catch(Exception e){
		}
		return Homeworkvo;
	}
	
	//getHomeWorkStuStatus
	@RequestMapping(value = "/getHomeWorkStuStatus", method = RequestMethod.GET)
	@ResponseBody
	public HomeworkStatusVO getHomeWorkStuStatus(@RequestParam("sClass1") String sclass,@RequestParam("Section1") String sec,@RequestParam("mobile") String mobile,@RequestParam("Subject") String subj) throws Exception
	{
		HomeworkStatusVO Homeworkvo = new HomeworkStatusVO();
		
		
		try{
			Homeworkvo =homeworkService.getHomeWorkStuStatus(sclass,sec,mobile,subj);
		}
		catch(Exception e){
		}
		return Homeworkvo;
	}
	

}
