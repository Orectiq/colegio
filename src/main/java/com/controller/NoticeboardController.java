package com.controller;
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.beans.factory.annotation.Qualifier; 
import org.springframework.stereotype.Controller; 
import org.springframework.ui.ModelMap; 
import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.ResponseBody; 
import org.springframework.web.bind.annotation.SessionAttributes; 
import org.springframework.web.servlet.ModelAndView; 

import com.model.LibraryVO;
import com.model.NoticeboardVO;
import com.model.Nb_newsVO;
import com.service.LibraryService;
import com.service.NoticeboardService;
import com.service.StudentService; 
import com.service.UserService; 
@Controller 
@RequestMapping("/noticeboard") 
@SessionAttributes("userName2") 
public class NoticeboardController {

	@Autowired  
	//@Qualifier(value = "noticeboardService") 
	private NoticeboardService noticeboardService; 

	private String userName1=""; 



	@RequestMapping(value = "/loadpage", method = RequestMethod.POST) 
	public ModelAndView loadPage(ModelMap modelMap,@RequestParam("d") String pg, HttpServletRequest request, HttpServletResponse response) 
	throws Exception { 
	modelMap.addAttribute("userName2", userName1); 
	return new ModelAndView(pg, modelMap); 
	} 
 
	@RequestMapping(value = "/insertEvent", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded") 
	@ResponseBody 
	public NoticeboardVO insertEvent(@ModelAttribute NoticeboardVO noticeboardVO) throws Exception, Exception 
	{
		System.out.println("Enter Noticeboard controller----------------------------->");
		NoticeboardVO NoticeboardVO = new NoticeboardVO(); 
	try { 
		NoticeboardVO = noticeboardService.insertEventDetails(noticeboardVO); 
	} catch (Exception e) { 
	} 
	return NoticeboardVO; 
	
	}
	
	@RequestMapping(value = "/insertNews", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded") 
	@ResponseBody 
	public String insertNews(@ModelAttribute Nb_newsVO nb_newsVO) throws Exception, Exception { 
		Nb_newsVO Nb_newsVO = new Nb_newsVO(); 


		String result = "";
		if (( noticeboardService.insertNewsDetails(nb_newsVO)) != null) 
		{
			result = "match";
			System.out.println("Controller Result  ok  "+result);
		} else
		{
			result = "Failed...";
		}
		
		
	
	return result; 
	}


	@RequestMapping(value = "/geteventdetail", method = RequestMethod.GET)
	@ResponseBody
		public NoticeboardVO geteventdetail(@RequestParam("doe")String doe,@RequestParam("ltype")String ltype) throws Exception 
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
						try{
							Noticeboardvo =noticeboardService.geteventdetail(doe,ltype);
						}
						catch(Exception e){
						}
						return Noticeboardvo;
			}
	
	@RequestMapping(value = "/getevent", method = RequestMethod.GET)
	@ResponseBody
		public NoticeboardVO getevent(@RequestParam("ltype")String ltype) throws Exception 
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
						try{
							Noticeboardvo =noticeboardService.getevent(ltype);
						}
						catch(Exception e){
						}
						return Noticeboardvo;
			}


	@RequestMapping(value = "/getnewsdetail", method = RequestMethod.GET)
	@ResponseBody
		public Nb_newsVO getnewsdetail(@RequestParam("dop") String dop) throws Exception 
	{
		Nb_newsVO Nb_newsvo = new Nb_newsVO();
						try{
							Nb_newsvo =noticeboardService.getnewsdetail(dop);
						}
						catch(Exception e){
						}
						return Nb_newsvo;
	}
	
	@RequestMapping(value = "/geteventdetailAll", method = RequestMethod.GET)
	@ResponseBody
		public NoticeboardVO geteventdetailAll() throws Exception 
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
						try{
							Noticeboardvo =noticeboardService.geteventdetailAll();
						}
						catch(Exception e){
						}
						return Noticeboardvo;
			}

	
	@RequestMapping(value = "/getnewsdetailAll", method = RequestMethod.GET)
	@ResponseBody
		public Nb_newsVO getnewsdetailAll() throws Exception 
	{
		Nb_newsVO Nb_newsvo = new Nb_newsVO();
						try{
							Nb_newsvo =noticeboardService.getnewsdetailAll();
						}
						catch(Exception e){
						}
						return Nb_newsvo;
	}
	
	//geteventdetailNext
	@RequestMapping(value = "/geteventdetailNext", method = RequestMethod.GET)
	@ResponseBody
	public NoticeboardVO geteventdetailNext() throws Exception 
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
						try{
							Noticeboardvo =noticeboardService.geteventdetailNext();
						}
						catch(Exception e){
						}
						return Noticeboardvo;
			}
	//geteventdetailType
	@RequestMapping(value = "/geteventdetailType", method = RequestMethod.GET)
	@ResponseBody
	public NoticeboardVO geteventdetailType(@RequestParam("ltype") String ttype,@RequestParam("doe")String doe) throws Exception 
	{
		System.out.println("Controller Type  "+ttype);
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
						try{
							Noticeboardvo =noticeboardService.geteventdetailType(ttype,doe);
						}
						catch(Exception e){
						}
						return Noticeboardvo;
			}
	
	
	//geteventdetailType
		@RequestMapping(value = "/getupcommingevents", method = RequestMethod.GET)
		@ResponseBody
		public NoticeboardVO getupcommingevents(@RequestParam("ltype") String ttype) throws Exception 
		{
			System.out.println("Controller Type  "+ttype);
			NoticeboardVO Noticeboardvo = new NoticeboardVO();
							try{
								Noticeboardvo =noticeboardService.getupcommingevents(ttype);
							}
							catch(Exception e){
							}
							return Noticeboardvo;
				}
		
	
	
	//getTeachernewsdetail
	@RequestMapping(value = "/getTeachernewsdetail", method = RequestMethod.GET)
	@ResponseBody
		public Nb_newsVO getTeachernewsdetail(@RequestParam("ltype") String ttype) throws Exception 
	{
		System.out.println("controller  type  "+ttype);
		
		Nb_newsVO Nb_newsvo = new Nb_newsVO();
						try{
							Nb_newsvo =noticeboardService.getTeachernewsdetail(ttype);
						}
						catch(Exception e){
						}
						return Nb_newsvo;
	}
	
	//getTeachernewsdetail
		@RequestMapping(value = "/getTeachernewsdetaildate", method = RequestMethod.GET)
		@ResponseBody
			public Nb_newsVO getTeachernewsdetaildate(@RequestParam("dop")String dop,@RequestParam("ltype") String ttype) throws Exception 
		{
			System.out.println("controller  type  "+ttype);
			
			Nb_newsVO Nb_newsvo = new Nb_newsVO();
							try{
								Nb_newsvo =noticeboardService.getTeachernewsdetaildate(dop,ttype);
							}
							catch(Exception e){
							}
							return Nb_newsvo;
		}
	
	
	
	@RequestMapping(value = "/geteventdetail_title", method = RequestMethod.GET)
	@ResponseBody
		public NoticeboardVO geteventdetail_title(@RequestParam("eventsid") String title) throws Exception 
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
						try{
							Noticeboardvo =noticeboardService.geteventdetail_title(title);
						}
						catch(Exception e){
						}
						return Noticeboardvo;
			}

	@RequestMapping(value = "/getnewsdetail_title", method = RequestMethod.GET)
	@ResponseBody

	public Nb_newsVO getnewsdetail_title(@RequestParam("newsid") String title) throws Exception
{
		Nb_newsVO Nb_newsvo = new Nb_newsVO();
		try {
			Nb_newsvo = noticeboardService.getnewsdetail_title(title);
		} catch (Exception e) 
			{
			}
		return Nb_newsvo;
	}
	
	
	

}

	