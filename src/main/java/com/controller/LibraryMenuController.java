package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dao.constant.CommonConstant;

@Controller
@RequestMapping("/libraryMenu")
public class LibraryMenuController
{
	String urlnew=CommonConstant.PATH_URL;
	
	//addBooks
			@RequestMapping(value = "/addBooks", method = RequestMethod.GET)
			public ModelAndView addBooks(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception
			{
					String projectUrl=urlnew+"/Library/AddBooks.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			 		return new ModelAndView("redirect:"+projectUrl,modelMap);
			}		
	
	//LibraryNewsBoard
		@RequestMapping(value = "/LibraryNewsBoard", method = RequestMethod.GET)
		public ModelAndView LibraryNewsBoard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception
		{
				String projectUrl=urlnew+"/Noticeboard/Library_NewsList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
		 		return new ModelAndView("redirect:"+projectUrl,modelMap);
		}		
	//LibraryStuReqRec
				@RequestMapping(value = "/LibraryStuReqRec", method = RequestMethod.GET)
				public ModelAndView LibraryStuReqRec(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
					throws Exception
				{
						String projectUrl=urlnew+"/Library/Library_StuReqRec.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
	//LibraryStuReturnBook
				@RequestMapping(value = "/LibraryStuReturnBook", method = RequestMethod.GET)
				public ModelAndView LibraryStuReturnBook(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
					throws Exception
				{
						String projectUrl=urlnew+"/Library/Library_StuReturnBookjsp.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
	//LibraryStuIssedlist
				@RequestMapping(value = "/LibraryStuIssedlist", method = RequestMethod.GET)
				public ModelAndView LibraryStuIssedlist(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
					throws Exception
				{
						String projectUrl=urlnew+"/Library/Library_IssuedList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
	//LibraryStuDuedatelist
				@RequestMapping(value = "/LibraryStuDuedatelist", method = RequestMethod.GET)
				public ModelAndView LibraryStuDuedatelist(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
					throws Exception
				{
						String projectUrl=urlnew+"/Library/Library_DuedateList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	

	//LibraryCalendar
				@RequestMapping(value = "/LibraryCalendar", method = RequestMethod.GET)
				public ModelAndView LibraryCalendar(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
					throws Exception
				{
						String projectUrl=urlnew+"/Calendar/Library_StuDuedate.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
				//Librarian_DashBoard
				@RequestMapping(value = "/Librarian_DashBoard", method = RequestMethod.GET)
				public ModelAndView Librarian_DashBoard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
					throws Exception
				{
						String projectUrl=urlnew+"/Dashboard/Librarian_DashBoard.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
				//Book_List
				@RequestMapping(value = "/Book_List", method = RequestMethod.GET)
				public ModelAndView Book_List(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype, HttpServletRequest request,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletResponse response)
					throws Exception
				{
						String projectUrl=urlnew+"/Library/Book_List.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
				 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
				
}
