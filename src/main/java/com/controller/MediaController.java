package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.model.MediaVO;
import com.model.MediaaddVO;
import com.service.MediaService;





@Controller
@RequestMapping("/media")
@SessionAttributes("userName2")
public class MediaController {


	@Autowired
	@Qualifier(value = "mediaService")
	private MediaService mediaService;
	
	private String userName1="";
	
	
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap,@RequestParam("d") String pg, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				modelMap.addAttribute("userName2", userName1);
		return new ModelAndView(pg, modelMap);
	}
	
	
	@RequestMapping(value = "/insertAlbum", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public MediaVO insertAlbum(@ModelAttribute MediaVO mediaVO) throws Exception, Exception {
		MediaVO Mediavo = new MediaVO();
		try {
			Mediavo = mediaService.insertAlbumDetails(mediaVO);
				} catch (Exception e) {
		}
		return Mediavo;
	}
	
	
	@RequestMapping(value = "/insertMedia", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public MediaaddVO insertMedia(@ModelAttribute MediaaddVO mediaaddVO) throws Exception, Exception {
		
		
		
		MediaaddVO Mediaaddvo = new MediaaddVO();
		

		try {
			Mediaaddvo = mediaService.insertMediaall(mediaaddVO);
				} catch (Exception e) {
		}
		return Mediaaddvo;
	}
	
	@RequestMapping(value = "/getmediadata", method = RequestMethod.GET)
	@ResponseBody
		public MediaaddVO getmediadata() throws Exception 
	{
		MediaaddVO Mediavo = new MediaaddVO();
						try{
							Mediavo =mediaService.getMediaData();
						}
						catch(Exception e){
						}
						return Mediavo;
			}

	
	@RequestMapping(value = "/getAlbumData", method = RequestMethod.GET)
	@ResponseBody
		public MediaVO getAlbumData() throws Exception 
	{
		MediaVO Mediaaddvo = new MediaVO();
						try{
							Mediaaddvo =mediaService.getAlbumData();
						}
						catch(Exception e){
						}
						return Mediaaddvo;
			}
	
	
}
