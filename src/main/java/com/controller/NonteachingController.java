package com.controller;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.filechooser.FileSystemView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.model.NonteachVO;
import com.model.TeacherVO;
import com.model.staffAttendanceVO;
import com.service.NonteachService;

@Controller
@RequestMapping("/nonteach")
@SessionAttributes("userName2")
//@Controller("crunchifyEmail")
public class NonteachingController {

	
	
	
	@Autowired
	@Qualifier(value = "nonteachService")
	private NonteachService nonteachService;
	
	private String userName1="";
	

	//sendMail
	@RequestMapping(value = "/sendMail", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public NonteachVO sendMail(@ModelAttribute NonteachVO nonteachVO) throws Exception, Exception 
	{
		System.out.println("Mail  -->  "+nonteachVO.getToAddr()+" from  "+nonteachVO.getFrmAddr()+" subj  "+nonteachVO.getSubj()+" body  "+nonteachVO.getBody());
		NonteachVO Nonteachvo = new NonteachVO();
		try {
			Nonteachvo = nonteachService.sendMail(nonteachVO);
		} catch (Exception e) {
		}
		return Nonteachvo;
	}
	
	/*public void crunchifyReadyToSendEmail(String toAddress, String fromAddress, String subject, String msgBody) {
		 
		SimpleMailMessage crunchifyMsg = new SimpleMailMessage();
		crunchifyMsg.setFrom(fromAddress);
		crunchifyMsg.setTo(toAddress);
		crunchifyMsg.setSubject(subject);
		crunchifyMsg.setText(msgBody);
		crunchifymail.send(crunchifyMsg);
	}*/
	
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap,@RequestParam("d") String pg, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				modelMap.addAttribute("userName2", userName1);
		return new ModelAndView(pg, modelMap);
	}


	//nonteach insert
	@RequestMapping(value = "/insertNonteach", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
		@ResponseBody
		public NonteachVO insertNonteach(@ModelAttribute NonteachVO nonteachVO,HttpServletRequest request) throws Exception, Exception 
		{
			System.out.println("Controller  ltype   "+nonteachVO.getLtype());
			
			File home = FileSystemView.getFileSystemView().getHomeDirectory();
			String path1=home.getAbsolutePath();
			//System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&  %%%%%%%%%%%%%%   "+path1);
			
		/*	String property = "java.io.tmpdir";
		    String path1 = System.getProperty(property);*/
		    
		    String path2=path1+"\\.Colegio";
		    
		    
		    System.out.println("OS current temporary directory is &&&&&&&&&&&&&&&&&&&&&&&&&&&  %%%%%%%%%%%%%%   " + path2);
		    
		    
		    
		    System.out.println(nonteachVO.getStaff_ID());
			System.out.println(nonteachVO.getDepartment());
			System.out.println(nonteachVO.getName());
			System.out.println(nonteachVO.getAge());
			System.out.println(nonteachVO.getDOB());
			System.out.println(nonteachVO.getGender());
			System.out.println(nonteachVO.getPhone_Number());
			System.out.println(nonteachVO.getContact_Number());
			System.out.println(nonteachVO.getEmail());
			System.out.println(nonteachVO.getDOJ());
			System.out.println(nonteachVO.getWork_Area());
			System.out.println(nonteachVO.getCurrent_Address());
			System.out.println(nonteachVO.getPermanent_Address());
			System.out.println(nonteachVO.getIsActive());
			System.out.println(nonteachVO.getInserted_By());
			System.out.println(nonteachVO.getInserted_Date());
			System.out.println(nonteachVO.getUpdated_By());
			System.out.println(nonteachVO.getUpdated_Date());
			System.out.println(nonteachVO.getPhoto());
	
			NonteachVO Nonteachvo = new NonteachVO();
			

			try {
				Nonteachvo = nonteachService.insertNonteachDetails(nonteachVO,path2);
					} catch (Exception e) 
			{
						System.out.println("Controller Error  "+e);
			}
			return Nonteachvo;
		}

	
	
	@RequestMapping(value = "/getltrreferencedata", method = RequestMethod.GET)
	@ResponseBody
	public NonteachVO getLtrReferenceData() throws Exception 
	{
		NonteachVO Nonteachvo = new NonteachVO();
		try {
			Nonteachvo = nonteachService.getLtrReferenceData();
		} catch (Exception e) {
		}
		return Nonteachvo;
	}


	
	@RequestMapping(value = "/getltrreferencedataall", method = RequestMethod.GET)
	@ResponseBody
		public NonteachVO getLtrReferenceDataall() throws Exception 
	{
						NonteachVO Nonteachvo = new NonteachVO();
						try{
							Nonteachvo =nonteachService.getLtrReferenceDataall();
						}
						catch(Exception e){
						}
						return Nonteachvo;
			}

	
	@RequestMapping(value = "/getNonteach", method = RequestMethod.GET)
	@ResponseBody
		public NonteachVO getExam(@RequestParam("Department") String departments) throws Exception 
	{
						String department = null;
						System.out.println("getNonteach controller" +department);
						NonteachVO Nonteachvo = new NonteachVO();
						try{
							Nonteachvo =nonteachService.getNonteach(department);
						}
						catch(Exception e){
						}
						return Nonteachvo;
	}
	//getNonTeachingLoginID
	@RequestMapping(value = "/getNonTeachingLoginID", method = RequestMethod.GET)
	@ResponseBody
		public NonteachVO getNonTeachingLoginID(@RequestParam("Staff_ID") String mobile) throws Exception 
	{
						
						System.out.println("getNonteach controller" +mobile);
						NonteachVO Nonteachvo = new NonteachVO();
						try{
							Nonteachvo =nonteachService.getNonTeachingLoginID(mobile);
						}
						catch(Exception e){
						}
						return Nonteachvo;
	}
	
	//getNonTeachIDWithPhone
		@RequestMapping(value = "/getNonTeachIDWithPhone", method = RequestMethod.GET)
		@ResponseBody
			public NonteachVO getNonTeachIDWithPhone(@RequestParam("Phone_Number") String mobile) throws Exception 
		{
							
							System.out.println("getNonteach controller" +mobile);
							NonteachVO Nonteachvo = new NonteachVO();
							try{
								Nonteachvo =nonteachService.getNonTeachIDWithPhone(mobile);
							}
							catch(Exception e){
							}
							return Nonteachvo;
		}
		
		//getNonTeachAttendance
				@RequestMapping(value = "/getNonTeachAttendance", method = RequestMethod.GET)
				@ResponseBody
					public staffAttendanceVO getNonTeachAttendance(@RequestParam("fdate") String fdate,@RequestParam("tdate") String tdate,@RequestParam("tid") String tid) throws Exception 
				{
									
									System.out.println("getNonteach controller" +fdate);
									staffAttendanceVO Nonteachvo = new staffAttendanceVO();
									try{
										Nonteachvo =nonteachService.getNonTeachAttendance(fdate,tdate,tid);
									}
									catch(Exception e){
									}
									return Nonteachvo;
				}
				
				@RequestMapping(value = "/getLibID", method = RequestMethod.GET)
				@ResponseBody
				public NonteachVO getLibID() throws Exception {

					System.out.println("************** enter getTeacherid controller 1");
					String ref = "LIB";
					
					return nonteachService.getLibID(ref);
				}		
				
				@RequestMapping(value = "/getDorID", method = RequestMethod.GET)
				@ResponseBody
				public NonteachVO getDorID() throws Exception {

					System.out.println("************** enter getTeacherid controller 1");
					String ref = "DOR";
					
					return nonteachService.getDorID(ref);
				}
				
				@RequestMapping(value = "/getTraID", method = RequestMethod.GET)
				@ResponseBody
				public NonteachVO getTraID() throws Exception {

					System.out.println("************** enter getTeacherid controller 1");
					String ref = "TRA";
					
					return nonteachService.getTraID(ref);
				}
				
	
	
}
