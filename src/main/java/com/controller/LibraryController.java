package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.model.LibrarianVO;
import com.model.LibraryIssueVO;
import com.model.LibraryReqVO;
import com.model.LibraryVO;
import com.model.StuVO;
import com.service.LibraryService;

@Controller
@RequestMapping("/book")
@SessionAttributes("userName2")
public class LibraryController
{
	
	
	//private static Library2VO Library2vo = null;

	@Autowired
	@Qualifier(value = "libraryService")
	private LibraryService libraryService;
	
	private String userName1="";
	
	
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap,@RequestParam("d") String pg, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				modelMap.addAttribute("userName2", userName1);
		return new ModelAndView(pg, modelMap);
	}
	
	@RequestMapping(value = "/getBookId", method = RequestMethod.GET)
	@ResponseBody
	public LibraryVO getBookId() throws Exception {

		System.out.println("enter getstuid controller");
		String ref = "BID";
		return libraryService.getBookId();
	}
	
	//
		@RequestMapping(value = "/libStureqbooklist", method = RequestMethod.GET)
		public ModelAndView libStureqbooklist(ModelMap modelMap,@RequestParam("id")String sid, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					request.setAttribute("sid", sid);
					System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
					
			return new ModelAndView("/library/libraryreqbooklist", modelMap);
		}
		
		
		
		
		//libReqbooklistREC
		@RequestMapping(value = "/libReqbooklistREC", method = RequestMethod.GET)
		public ModelAndView libReqbooklistREC(ModelMap modelMap,@RequestParam("id")String sid, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					request.setAttribute("lid", sid);
					System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
					
			return new ModelAndView("/library/RequestReceived", modelMap);
		}
		//libIssuedbooklist
		@RequestMapping(value = "/libIssuedbooklist", method = RequestMethod.GET)
		public ModelAndView libIssuedbooklist(ModelMap modelMap,@RequestParam("id")String sid, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					request.setAttribute("lid", sid);
					System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
					
			return new ModelAndView("/library/IssedList", modelMap);
		}
		//libDueDatebooklist
		@RequestMapping(value = "/libDueDatebooklist", method = RequestMethod.GET)
		public ModelAndView libDueDatebooklist(ModelMap modelMap,@RequestParam("id")String sid, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					request.setAttribute("lid", sid);
					System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
					
			return new ModelAndView("/library/DuedateList", modelMap);
		}
		
		//libReturnbooklist
		@RequestMapping(value = "/libReturnbooklist", method = RequestMethod.GET)
		public ModelAndView libReturnbooklist(ModelMap modelMap,@RequestParam("id")String sid, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{
					request.setAttribute("lid", sid);
					System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
					
			return new ModelAndView("/library/ReturnBooks", modelMap);
		}
		

	@RequestMapping(value = "/insertBook", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public LibraryVO insertBook(@ModelAttribute LibraryVO libraryVO) throws Exception, Exception {
		
		System.out.println("controller Library ------------------->");
		System.out.println(libraryVO.getBook_ID());
		System.out.println(libraryVO.getBook_Title());
		System.out.println(libraryVO.getAuthor());
		System.out.println(libraryVO.getDescription());
		System.out.println("controller library end ------------------->");
		
		LibraryVO Libraryvo = new LibraryVO();
		

		try {
			Libraryvo = libraryService.insertBooksDetails(libraryVO);
				} catch (Exception e) {
		}
		return Libraryvo;
	}
	
	@RequestMapping(value = "/insertReq", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public LibraryIssueVO insertReq(@ModelAttribute LibraryIssueVO libraryissueVO) throws Exception, Exception {
		
		System.out.println("controller Library");
		System.out.println(libraryissueVO.getBook_id());
		System.out.println("controller library end");
		LibraryIssueVO Libraryvo = new LibraryIssueVO();
		try {
			Libraryvo = libraryService.insertReq(libraryissueVO);
				} catch (Exception e) {
		}
		return Libraryvo;
	}
	
	//updateReq
	@RequestMapping(value = "/updateReq", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public LibraryIssueVO updateReq(@ModelAttribute LibraryIssueVO libraryissueVO) throws Exception, Exception {
		
		System.out.println("controller Library");
		System.out.println(libraryissueVO.getBook_id());
		System.out.println("controller library end");
		LibraryIssueVO Libraryvo = new LibraryIssueVO();
		try {
			Libraryvo = libraryService.updateReq(libraryissueVO);
				} catch (Exception e) {
		}
		return Libraryvo;
	}
	//updateReturn
	@RequestMapping(value = "/updateReturn", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public LibraryIssueVO updateReturn(@ModelAttribute LibraryIssueVO libraryissueVO) throws Exception, Exception {
		
		System.out.println("controller Library");
		System.out.println(libraryissueVO.getBook_id());
		System.out.println("controller library end");
		LibraryIssueVO Libraryvo = new LibraryIssueVO();
		try {
			Libraryvo = libraryService.updateReturn(libraryissueVO);
				} catch (Exception e) {
		}
		return Libraryvo;
	}
	
	
	
	//updateReqMaster
	@RequestMapping(value = "/updateReqMaster", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public LibraryVO updateReqMaster(@ModelAttribute LibraryVO libraryVO) throws Exception, Exception {
		
		System.out.println("controller Library  ---  ");
		System.out.println(libraryVO.getBook_ID());
		System.out.println(libraryVO.getBook_Title());
		System.out.println(libraryVO.getAuthor());
		System.out.println(libraryVO.getDescription());
		System.out.println("controller library end ---- ");
		LibraryVO Libraryvo = new LibraryVO();
		try {
			Libraryvo = libraryService.updateReqMaster(libraryVO);
				} catch (Exception e) {
		}
		return Libraryvo;
	}
	//updateReturnMaster
	@RequestMapping(value = "/updateReturnMaster", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public LibraryVO updateReturnMaster(@ModelAttribute LibraryVO libraryVO) throws Exception, Exception {
		
		System.out.println("controller Library  ---  ");
		System.out.println(libraryVO.getBook_ID());
		System.out.println(libraryVO.getBook_Title());
		System.out.println(libraryVO.getAuthor());
		System.out.println(libraryVO.getDescription());
		System.out.println("controller library end ---- ");
		LibraryVO Libraryvo = new LibraryVO();
		try {
			Libraryvo = libraryService.updateReturnMaster(libraryVO);
				} catch (Exception e) {
		}
		return Libraryvo;
	}
	
	//updateRequestMasterCount
	@RequestMapping(value = "/updateRequestMasterCount", method = RequestMethod.POST)
	@ResponseBody
	public LibraryVO updateRequestMasterCount(@ModelAttribute LibraryVO libraryVO) throws Exception, Exception {
		
		System.out.println("controller Library  ---  ");
		System.out.println(libraryVO.getBook_ID());
		System.out.println(libraryVO.getBook_Title());
		
		System.out.println("controller library end ---- ");
		LibraryVO Libraryvo = new LibraryVO();
		try {
			Libraryvo = libraryService.updateRequestMasterCount(libraryVO);
				} catch (Exception e) {
		}
		return Libraryvo;
	}
	
	
	
	
	
	
	//updateReqMasterStatus
	/*@RequestMapping(value = "/updateReqMaster", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public LibraryVO updateReqMasterStatus(@ModelAttribute LibraryVO libraryVO) throws Exception, Exception {
		
		System.out.println("controller Library  ---  ");
		System.out.println(libraryVO.getBook_ID());
		System.out.println(libraryVO.getBook_Title());
		System.out.println(libraryVO.getAuthor());
		System.out.println(libraryVO.getDescription());
		System.out.println("controller library end ---- ");
		LibraryVO Libraryvo = new LibraryVO();
		try {
			Libraryvo = libraryService.updateReqMasterStatus(libraryVO);
				} catch (Exception e) {
		}
		return Libraryvo;
	}*/
	
	
	
	@RequestMapping(value= "/requestBook", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public LibraryReqVO requestBook(@ModelAttribute LibraryReqVO libraryreqVO) throws Exception, Exception {
		
		
		LibraryReqVO LibraryReqvo = new LibraryReqVO();
		

		try {
			LibraryReqvo = libraryService.requestBooksDetails(libraryreqVO);
				} catch (Exception e) {
		}
		return LibraryReqvo;
	}
	
	@RequestMapping(value= "/reqBookbyStudent", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public LibraryVO reqBookbyStudent(@RequestParam("SClass") String sclass) throws Exception, Exception {
		
		
		LibraryVO LibraryReqvo = new LibraryVO();
		

		try {
			LibraryReqvo = libraryService.reqBookbyStudent(sclass);
				} catch (Exception e) {
		}
		return LibraryReqvo;
	}
	
	
	
	
	
	//getLibDetails
	@RequestMapping(value = "/getLibDetails", method = RequestMethod.GET)
	@ResponseBody
	public  LibrarianVO getLibDetails(@RequestParam("lib_id") String studid) throws Exception 
	{
		System.out.println("enter controller  "+studid);
		return libraryService.getLibDetails(studid);
	}
	
	//getRequestbyDate
	@RequestMapping(value = "/getRequestbyDate", method = RequestMethod.GET)
	@ResponseBody
	public  LibraryIssueVO getRequestbyDate(@RequestParam("req_date") String rdate,@RequestParam("book_title") String btitle) throws Exception 
	{
		System.out.println("enter controller  "+rdate);
		return libraryService.getRequestbyDate(rdate,btitle);
	}
	
	//getIssedAll
	@RequestMapping(value = "/getIssedAll", method = RequestMethod.GET)
	@ResponseBody
	public  LibraryIssueVO getIssedAll(@RequestParam("book_title") String title,@RequestParam("subject") String subject) throws Exception 
	{
		return libraryService.getIssedAll(title,subject);
	}
	//getBookTitle
	@RequestMapping(value = "/getBookTitle", method = RequestMethod.GET)
	@ResponseBody
	public  LibraryIssueVO getBookTitle() throws Exception 
	{
		return libraryService.getBookTitle();
	}
	
	
	//getRequestbyID
	@RequestMapping(value = "/getRequestbyID", method = RequestMethod.GET)
	@ResponseBody
	public  LibraryVO getRequestbyID(@RequestParam("Book_ID") String rdate) throws Exception 
	{
		System.out.println("enter controller  "+rdate);
		return libraryService.getRequestbyID(rdate);
	}
	
	//getRequestbyStudent
	@RequestMapping(value = "/getRequestbyStudent", method = RequestMethod.GET)
	@ResponseBody
	public  LibraryIssueVO getRequestbyStudent(@RequestParam("student_id") String sid) throws Exception 
	{
		System.out.println("enter controller  "+sid);
		return libraryService.getRequestbyStudent(sid);
	}
	//getIssedBooks
	@RequestMapping(value = "/getIssedBooks", method = RequestMethod.GET)
	@ResponseBody
	public  LibraryIssueVO getIssedBooks(@RequestParam("sclass") String sclass,@RequestParam("sec") String sec) throws Exception 
	{
		System.out.println("enter controller  "+sclass);
		return libraryService.getIssedBooks(sclass,sec);
	}
	
	
	//getDueDateBooks
	@RequestMapping(value = "/getDueDateBooks", method = RequestMethod.GET)
	@ResponseBody
	public  LibraryIssueVO getDueDateBooks(@RequestParam("sclass") String sclass,@RequestParam("sec") String sec) throws Exception 
	{
		System.out.println("enter controller  "+sclass);
		return libraryService.getDueDateBooks(sclass,sec);
	}
	
	//getDueDateBooksAll
		@RequestMapping(value = "/getDueDateBooksAll", method = RequestMethod.GET)
		@ResponseBody
		public  LibraryIssueVO getDueDateBooksAll() throws Exception 
		{
			System.out.println("enter controller  ");
			return libraryService.getDueDateBooksAll();
		}
	
		//getDueDateBooksAll
				@RequestMapping(value = "/getDueDateBooksAllAdmin", method = RequestMethod.GET)
				@ResponseBody
				public  LibraryIssueVO getDueDateBooksAllAdmin(@RequestParam("book_title") String title,@RequestParam("subject") String subj) throws Exception 
				{
					System.out.println("enter controller  ");
					return libraryService.getDueDateBooksAllAdmin(title,subj);
				}
	
	
	//showRequest
	@RequestMapping(value = "/showRequest", method = RequestMethod.GET)
	@ResponseBody
	public  LibraryIssueVO showRequest() throws Exception 
	{
		return libraryService.showRequest();
	}
	
	
	
	@RequestMapping(value = "/getltrreferencedataall", method = RequestMethod.GET)
	@ResponseBody
		public LibraryVO getLtrReferenceData() throws Exception 
	{
						LibraryVO Libraryvo = new LibraryVO();
						try{
							Libraryvo =libraryService.getLtrReferenceDataall();
						}
						catch(Exception e){
						}
						return Libraryvo;
			}
	@RequestMapping(value = "/getBooktitle", method = RequestMethod.GET)
	@ResponseBody
		public LibraryVO getBooktitle(@RequestParam("Subject") String cate) throws Exception 
	{
						LibraryVO Libraryvo = new LibraryVO();
						try{
							Libraryvo =libraryService.getBooktitle(cate);
						}
						catch(Exception e){
						}
						return Libraryvo;
			}
	
	@RequestMapping(value = "/getBookDetails", method = RequestMethod.GET)
	@ResponseBody
		public LibraryVO getBookDetails(@RequestParam("Book_Title") String title,@RequestParam("Category") String cate) throws Exception 
	{
						System.out.println("controller ");
						LibraryVO Libraryvo = new LibraryVO();
						try{
							Libraryvo =libraryService.getBookDetails(title,cate);
						}
						catch(Exception e){
						}
						return Libraryvo;
			}
	
	//getBookReturnDetails
	


	@RequestMapping(value = "/getltrrequestdata", method = RequestMethod.GET)
	@ResponseBody
		public LibraryReqVO getLtrRequestData() throws Exception 
	{
						LibraryReqVO LibraryReqvo = new LibraryReqVO();
						try{
							LibraryReqvo =libraryService.getLtrRequestData();
						}
						catch(Exception e){
						}
						return LibraryReqvo;
			}
	//getBookRequestDetails
			@RequestMapping(value = "/getBookRequestDetails", method = RequestMethod.GET)
			@ResponseBody
				public LibraryIssueVO getBookRequestDetails(@RequestParam("sclass") String sclass) throws Exception 
			{
								System.out.println("sclass controller  "+sclass);
								LibraryIssueVO LibraryReqvo = new LibraryIssueVO();
								try{
									LibraryReqvo =libraryService.getBookRequestDetails(sclass);
								}
								catch(Exception e){
								}
								return LibraryReqvo;
					}
			//getBookRequestbyClass
			@RequestMapping(value = "/getBookRequestbyClass", method = RequestMethod.GET)
			@ResponseBody
				public LibraryIssueVO getBookRequestbyClass(@RequestParam("sclass") String sclass,@RequestParam("book_title") String btitle) throws Exception 
			{
								System.out.println("sclass controller  "+sclass);
								LibraryIssueVO LibraryReqvo = new LibraryIssueVO();
								try{
									LibraryReqvo =libraryService.getBookRequestbyClass(sclass,btitle);
								}
								catch(Exception e){
								}
								return LibraryReqvo;
					}
	
	//getBookReturnDetails
	@RequestMapping(value = "/getBookReturnDetails", method = RequestMethod.GET)
	@ResponseBody
		public LibraryIssueVO getBookReturnDetails(@RequestParam("Book_Title") String title,@RequestParam("Category") String cate) throws Exception 
	{
					LibraryIssueVO LibraryReqvo = new LibraryIssueVO();
						try{
							LibraryReqvo =libraryService.getBookReturnDetails(title,cate);
						}
						catch(Exception e){
						}
						return LibraryReqvo;
			}
	
	
	
	
	//countTotalBooks
		@RequestMapping(value = "/countTotalBooks", method = RequestMethod.GET)
		@ResponseBody
		public  int countTotalBooks() throws Exception 
		{
			int count=libraryService.countTotalBooks();
			return count;
		}
	
	//countRequest
	@RequestMapping(value = "/countRequest", method = RequestMethod.GET)
	@ResponseBody
	public  int countRequest() throws Exception 
	{
		int count=libraryService.countRequest();
		return count;
	}
	
	//countIssued
	@RequestMapping(value = "/countIssued", method = RequestMethod.GET)
	@ResponseBody
	public  int countIssued() throws Exception 
	{
		int count=libraryService.countIssued();
		return count;
	}
	//countDueDate
	@RequestMapping(value = "/countDueDate", method = RequestMethod.GET)
	@ResponseBody
	public  int countDueDate(@RequestParam("Book_Title") String btitle,@RequestParam("Category") String cate) throws Exception 
	{
		int count=libraryService.countDueDate(btitle,cate);
		return count;
	}
	
	//countReturnBooks
		@RequestMapping(value = "/countReturnBooks", method = RequestMethod.GET)
		@ResponseBody
		public  int countReturnBooks(@RequestParam("Book_Title") String btitle,@RequestParam("Category") String cate) throws Exception 
		{
			int count=libraryService.countReturnBooks(btitle,cate);
			return count;
		}
	
	
}