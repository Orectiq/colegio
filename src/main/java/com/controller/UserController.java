package com.controller;
import java.io.File;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.filechooser.FileSystemView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.model.CounterVO;
import com.model.CountryVO;
import com.model.LibrarianVO;
import com.model.LoginVO;
import com.model.MailSettingVO;
import com.model.MasterVO;
import com.model.NonteachVO;
import com.model.ParentVO;
import com.model.SettingadminVO;
import com.model.StuVO;
import com.model.TeacherVO;
import com.model.TransportVO;
import com.common.MasVO;
import com.common.TeachVO;
import com.common.UserVO;
import com.dao.constant.CommonConstant;
import com.service.StudentService;
import com.service.UserService;
@Controller
@RequestMapping("/user")
@SessionAttributes("userName2,StuVO")
public class UserController
{
	String urlnew=CommonConstant.PATH_URL;  
	String changePassUrl=CommonConstant.CHANGEPASS_PATH_URL;  
	
	@Autowired
	private UserService userService;
	
	@Autowired
	@Qualifier(value = "studentService")
	private StudentService studentService;
	
	private String userName1="";
	
	
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap,@RequestParam("d") String pg, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				modelMap.addAttribute("userName2", userName1);
		return new ModelAndView(pg, modelMap);
	}
	
	/**
	 * 
	 * @param userId
	 * @param request
	 * @return String
	 * @throws RsUnifyDAOException
	 */
	
	//@RequestMapping(value="/user/checkusername",method = RequestMethod.POST)
	@RequestMapping(value = "/checkusername", method = RequestMethod.POST)
	public @ResponseBody String isUserNameExist(@RequestParam("userId") String userId,@RequestParam("password") String password, HttpServletRequest request)
			throws Exception {
		
		System.out.println("Controller Uname  "+userId+" and  password  "+password);
		String result = "unmatch";
		String typ="";
		String sname="";
		MasVO masVO = null;
		if ((masVO = userService.isUserNameExist(userId,password)) != null) 
		{
			sname=masVO.getSchoolname();
			result = "match"+sname;
			userName1=userId;
			System.out.println("Controller Result  ok  "+result+" and school name   "+masVO.getSchoolname());
		} else {
			result = "Invalid username";
		}
		return result;
	}
	//checkCommonUser
	@RequestMapping(value = "/checkCommonUser", method = RequestMethod.POST)
	public @ResponseBody String isCommonUserExist(@RequestParam("userId") String userId,@RequestParam("password") String password, HttpServletRequest request)
			throws Exception {
		
		
		File home = FileSystemView.getFileSystemView().getHomeDirectory();
		String path1=home.getAbsolutePath();
		//System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&  %%%%%%%%%%%%%%   "+path1);
		
		/*String property = "java.io.tmpdir";
	    String path1 = System.getProperty(property);*/
	    
	    
	    String path2=path1+"\\.Colegio";
	    
	    try{
			File file = new File(path2);
			if (!file.exists()) {
	            if (file.mkdir()) {
	                System.out.println("Directory is created!");
	            } else {
	                System.out.println("Failed to create directory!");
	            }
	        }}catch(Exception rr)
		{
				System.out.println(rr);
			}
	    System.out.println("OS current temporary directory is &&&&&&&&&&&&&&&&&&&&&&&&&&&  %%%%%%%%%%%%%%   " + path2);
		
		
		
		
		
		
		System.out.println("Controller Uname  "+userId+" and  password  "+password);
		String result = "unmatch";
		String typ="";
		String sname="";
		LoginVO loginVO = null;
		if ((loginVO = userService.isCommonUserExist(userId,password)) != null) 
		{
			typ=loginVO.getLtype();
			result = "match"+typ;
			userName1=userId;
			System.out.println("Controller Result  ok  "+result);
		} else {
			result = "Invalid username";
		}
		return result;
	}
	
	@RequestMapping(value = "/checkstaffname", method = RequestMethod.POST)
	public @ResponseBody String isStaffNameExist(@RequestParam("userId") String userId,@RequestParam("password") String password, HttpServletRequest request)
			throws Exception {
		
		System.out.println("Controller Uname  "+userId+" and  password  "+password);
		String result = "unmatch";
		String typ="";
		TeacherVO techVO = null;
		if ((techVO = userService.isStaffNameExist(userId,password)) != null) 
		{
			result = "match";
			userName1=userId;
			System.out.println("Controller Result  ok  "+result);
		} else {
			result = "Invalid username";
		}
		return result;
	}
	//checkTransport
	@RequestMapping(value = "/checkTransport", method = RequestMethod.POST)
	public @ResponseBody String checkTransport(@RequestParam("userId") String userId,@RequestParam("password") String password, HttpServletRequest request)
			throws Exception {
		
		System.out.println("Controller Uname  "+userId+" and  password  "+password);
		String result = "unmatch";
		String typ="";
		TransportVO transVO = null;
		if ((transVO = userService.isTransportExist(userId,password)) != null) 
		{
			result = "match";
			userName1=userId;
			System.out.println("Controller Result  ok  "+result);
		} else {
			result = "Invalid username";
		}
		return result;
	}
	
	@RequestMapping(value = "/checkSTUid", method = RequestMethod.POST)
	public @ResponseBody String isSTUidExist(@RequestParam("userId") String userId,@RequestParam("password") String password, HttpServletRequest request)
			throws Exception {
		
		System.out.println("Controller Uname  "+userId+" and  password  "+password);
		String result = "unmatch";
		String typ="";
		StuVO stuVO = null;
		if ((stuVO = userService.isSTUidExist(userId,password)) != null) 
		{
			result = "match";
			userName1=userId;
			System.out.println("Controller Result  ok  "+result);
		} else
		{
			result = "Invalid username";
		}
		return result;
	}
	//checkLIBid
	@RequestMapping(value = "/checkLIBid", method = RequestMethod.POST)
	public @ResponseBody String isLIBidExist(@RequestParam("userId") String userId,@RequestParam("password") String password, HttpServletRequest request)
			throws Exception {
		
		System.out.println("Controller Librarian Uname  "+userId+" and  password  "+password);
		String result = "unmatch";
		String typ="";
		LibrarianVO libVO = null;
		if ((libVO = userService.isLIBidExist(userId,password)) != null) 
		{
			result = "match";
			userName1=userId;
			System.out.println("Controller Result  ok  "+result);
		} else
		{
			result = "Invalid username";
		}
		return result;
	}
	
	
	@RequestMapping(value = "/checkPARid", method = RequestMethod.POST)
	public @ResponseBody String isPARidExist(@RequestParam("userId") String userId,@RequestParam("password") String password, HttpServletRequest request)
			throws Exception {
		
		System.out.println("Controller Uname  "+userId+" and  password  "+password);
		String result = "unmatch";
		String typ="";
		ParentVO parentVO = null;
		
		userName1=userId;
		
		if ((parentVO = userService.isPARidExist(userId,password)) != null) 
		{
			//result = "match";
			typ=parentVO.getRole();
			result = "match"+typ;
			
			userName1=userId;
			System.out.println("Controller Result  ok  "+result);
		} else
		{
			result = "Invalid username";
		}
		return result;
	}
	
	
	/**
	 * 
	 * @param modelMap
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/logoutpageurl", method = RequestMethod.GET)
	public ModelAndView logoutpageurl(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,RedirectAttributes redir)
			throws Exception {
				//String projectUrl=changePassUrl+"?admid="+admId;
				return new ModelAndView("redirect:http://localhost:8080/colegioui/logout.jsp",modelMap);
	}
	
	@RequestMapping(value = "/changePassSuccess", method = RequestMethod.POST)
	public ModelAndView changePassSuccess(ModelMap modelMap,@RequestParam("admid")String admId, HttpServletRequest request, HttpServletResponse response,RedirectAttributes redir)
			throws Exception {
				String projectUrl=changePassUrl+"?admid="+admId;
				return new ModelAndView("redirect:"+projectUrl,modelMap);
	}
	
	@RequestMapping(value = "/authenSuccess", method = RequestMethod.POST)
	public ModelAndView showapp(ModelMap modelMap,@RequestParam("admid")String admId,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sname")String sname,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response,RedirectAttributes redir)
			throws Exception {
				String projectUrl=urlnew+"/Dashboard/Admin_Dashboard.jsp?admid="+admId+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sname="+sname+"&sheader="+sheader;
				String proj=projectUrl.replaceAll(" ", "%20");
				String projectUrl1=URLEncoder.encode(proj, "UTF-8");
				return new ModelAndView("redirect:"+projectUrl,modelMap);
	}
	
	
	
	@RequestMapping(value = "/authenSuccessStu", method = RequestMethod.POST)
	public ModelAndView showappstu(ModelMap modelMap,@RequestParam("uid")String mobile,@RequestParam("ltype")String ltype,@RequestParam("url")String url,@RequestParam("slogo")String slogo,@RequestParam("sname")String sname,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				String projectUrl=urlnew+"/Dashboard/Student_DashBoard.jsp?mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sname="+sname+"&sheader=" +sheader;
				return new ModelAndView("redirect:"+projectUrl,modelMap);
		
	}
	
	@RequestMapping(value = "/authenSuccessLib", method = RequestMethod.POST)
	public ModelAndView showapplib(ModelMap modelMap,@RequestParam("uid")String mobile,@RequestParam("ltype")String ltype,@RequestParam("url")String url,@RequestParam("slogo")String slogo,@RequestParam("sname")String sname, HttpServletResponse response)
			throws Exception {
		
		String projectUrl=urlnew+"/Dashboard/Librarian_DashBoard.jsp?mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sname="+sname;
		return new ModelAndView("redirect:"+projectUrl,modelMap);
	}
	
	
	@RequestMapping(value = "/authenSuccessPar", method = RequestMethod.POST)
	public ModelAndView showapppar(ModelMap modelMap,@RequestParam("uid")String mobile,@RequestParam("ltype")String ltype,@RequestParam("url")String url,@RequestParam("slogo")String slogo,@RequestParam("sname")String sname,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! authun Parent    sname  "+sname+" logo  "+slogo+ "sheader"+sheader+"url"+url);
				String projectUrl=urlnew+"/Dashboard/Parents_DashBoard.jsp?mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sname="+sname+"&sheader="+sheader;
				return new ModelAndView("redirect:"+projectUrl,modelMap);
	}
	//authenSuccessStaff                 
	@RequestMapping(value = "/authenSuccessStaff", method = RequestMethod.POST)
	public ModelAndView showapppstaff(ModelMap modelMap,@RequestParam("uid")String mobile,@RequestParam("ltype")String ltype,@RequestParam("url")String url,@RequestParam("slogo")String slogo,@RequestParam("sname")String sname,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! authun staff      sname  "+sname+" logo  "+slogo+ "sheader"+sheader);
		String projectUrl=urlnew+"/Dashboard/Teacher_DashBoard.jsp?mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sname="+sname+"&sheader="+sheader;
		return new ModelAndView("redirect:"+projectUrl,modelMap);
	}
	//authenSuccessTrans
	@RequestMapping(value = "/authenSuccessTrans", method = RequestMethod.POST)
	public ModelAndView showappptrans(ModelMap modelMap,@RequestParam("uid")String mobile,@RequestParam("ltype")String ltype,@RequestParam("url")String url,@RequestParam("slogo")String slogo,@RequestParam("sname")String sname,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! authun tran driver    sname  "+sname+" logo  "+slogo+ "sheader"+sheader);
				String projectUrl=urlnew+"/Dashboard/TransportDriver_Dashboard.jsp?mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sname="+sname+"&sheader="+sheader;
				return new ModelAndView("redirect:"+projectUrl,modelMap);
				
	}
	//authenSuccessTransMain
	@RequestMapping(value = "/authenSuccessTransMain", method = RequestMethod.POST)
	public ModelAndView authenSuccessTransMain(ModelMap modelMap,@RequestParam("uid")String mobile,@RequestParam("ltype")String ltype,@RequestParam("url")String url,@RequestParam("slogo")String slogo,@RequestParam("sname")String sname,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String projectUrl=urlnew+"/Dashboard/TransportMain_DashBoard.jsp?mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sname="+sname+"&sheader="+sheader;
		return new ModelAndView("redirect:"+projectUrl,modelMap);		
		
	}
	//authenSuccessDormitory
	@RequestMapping(value = "/authenSuccessDormitory", method = RequestMethod.POST)
	public ModelAndView authenSuccessDormitory(ModelMap modelMap,@RequestParam("uid")String mobile,@RequestParam("ltype")String ltype,@RequestParam("url")String url,@RequestParam("slogo")String slogo,@RequestParam("sname")String sname,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String projectUrl=urlnew+"/Dashboard/Dormitory_Dashboard.jsp?mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sname="+sname+"&sheader="+sheader;
		return new ModelAndView("redirect:"+projectUrl,modelMap);	
			
	}
	
	@RequestMapping(value = "/insertStu1", method = RequestMethod.POST)
	public @ResponseBody String ins1(@RequestParam("fName") String userId,@RequestParam("lname") String password, HttpServletRequest request)
			throws Exception {
		StuVO Stuvo = new StuVO();
		String result = "unmatch";
		System.out.println("******* Controller Start *******");
		System.out.println("fname  "+userId);
		System.out.println("lname  "+password);
		
		System.out.println("******* Controller end *******");
		
		try{
			//Stuvo = studentService.insertStudentDetails(stuVO);
			//LocationTransfervo.setValid(true);
		}
		catch(Exception e){
			//LocationTransfervo.setValid(false);
			//LocationTransfervo.setErrorString(e.getMessage());
		}		
		return result;
	}
	
	
	// Master Register
			@RequestMapping(value = "/insertMaster", method = RequestMethod.POST)
			@ResponseBody
			public MasterVO insertMaster(@ModelAttribute MasterVO masterVO) throws Exception, Exception {
				MasterVO Mastervo = new MasterVO();

				try {
					Mastervo = userService.insertMasterDetails(masterVO);
				} catch (Exception e) {
				}
				return Mastervo;
			}
			
			
			// updateAdminPhoto
						@RequestMapping(value = "/updateAdminPhoto", method = RequestMethod.POST)
						@ResponseBody
						public String updateAdminPhoto(@ModelAttribute MasterVO masterVO) throws Exception, Exception 
						{
							System.out.println("controller  "+masterVO.getPhoto()+" and  "+masterVO.getUname());
							MasterVO Mastervo = new MasterVO();

							/*try {
								Mastervo = userService.updateAdminPhoto(masterVO);
							} catch (Exception e) {
							}
							return Mastervo;*/
							
							String result = "";
							if (( userService.updateAdminPhoto(masterVO)) != null) 
							{
								result = "match";
								System.out.println("Controller Result  ok  "+result);
							} else
							{
								result = "Failed...";
							}
							return result;
							
						}
			// updatePassword
						@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
						@ResponseBody
						public String updatePassword(@ModelAttribute MasterVO masterVO) throws Exception, Exception {
							MasterVO Mastervo = new MasterVO();
							
							System.out.println("UPDATE   "+(Mastervo));
							 	String result = "";
								if ((Mastervo = userService.updatePassword(masterVO)) != null) 
								{
									result = "match";
									System.out.println("Controller Result  ok  "+result);
								} else
								{
									result = "Failed...";
								}
								return result;
						}			
		
			
			
	// getMasterDetail
						@RequestMapping(value = "/getMasterDetail", method = RequestMethod.POST)
						@ResponseBody
						public MasterVO getMasterDetail(@RequestParam("uname") String uname) throws Exception, Exception 
						{
							System.out.println("Controller   "+uname);
							return userService.getMasterDetail(uname);
						}	
	//photoUpdate					
						@RequestMapping(value = "/photoUpdate", method = RequestMethod.POST)
						@ResponseBody
						public String photoUpdate(@ModelAttribute LoginVO loginVO) throws Exception, Exception 
						{
							System.out.println("controller  "+loginVO.getPhoto()+" and  "+loginVO.getUname());
							LoginVO Loginvo = new LoginVO();

							
							String result = "";
							if (( userService.photoUpdate(loginVO)) != null) 
							{
								result = "match";
								System.out.println("Controller Result  ok  "+result);
							} else
							{
								result = "Failed...";
							}
							return result;
							
						}	
						
				// updatePassword
						@RequestMapping(value = "/updateMobilePassword", method = RequestMethod.POST)
						@ResponseBody
						public String updateMobilePassword(@ModelAttribute LoginVO loginVO) throws Exception, Exception {
							LoginVO Mastervo = new LoginVO();
							
							System.out.println("UPDATE   "+(Mastervo));
							 	String result = "";
								if ((Mastervo = userService.updateMobilePassword(loginVO)) != null) 
								{
									result = "match";
									System.out.println("Controller Result  ok  "+result);
								} else
								{
									result = "Failed...";
								}
								return result;
						}	
				// updateAdminGlobalSetting
						@RequestMapping(value = "/updateAdminGlobalSetting", method = RequestMethod.POST)
						@ResponseBody
						public LoginVO updateAdminGlobalSetting(@ModelAttribute LoginVO loginVO) throws Exception, Exception {
							System.out.println("Controller  General Setting  ");
							LoginVO loginvo = new LoginVO();

								try {
									loginvo = userService.updateAdminGlobalSetting(loginVO);
										} catch (Exception e) 
								{
											System.out.println("Controller Error  "+e);
								}
								return loginvo;
						}					
						
						// updateMailPassword
						@RequestMapping(value = "/updateMailPassword", method = RequestMethod.POST)
						@ResponseBody
						public LoginVO updateMailPassword(@ModelAttribute LoginVO loginVO) throws Exception, Exception {
							LoginVO Mastervo = new LoginVO();

								
								try {
									Mastervo = userService.updateMailPassword(loginVO);
										} catch (Exception e) 
								{
											System.out.println("Controller Error  "+e);
								}
								return Mastervo;
						}				
						
						
			//getLoginDetails
						@RequestMapping(value = "/getLoginDetails", method = RequestMethod.GET)
						@ResponseBody
						public LoginVO getLoginDetails(@RequestParam("uname") String uid,@RequestParam("lpass") String pass) throws Exception
						{
							
							System.out.println("enter controller photo  "+uid+" pass  : "+pass);
							return userService.getLoginDetails(uid,pass);
						}
			//getLoginMobile
						@RequestMapping(value = "/getLoginMobile", method = RequestMethod.GET)
						@ResponseBody
						public LoginVO getLoginMobile(@RequestParam("uname") String uid) throws Exception
						{
							
							System.out.println("enter controller photo  "+uid);
							return userService.getLoginMobile(uid);
						}	
				//getLoginEmail
						@RequestMapping(value = "/getLoginEmail", method = RequestMethod.GET)
						@ResponseBody
						public LoginVO getLoginEmail(@RequestParam("email") String uid) throws Exception
						{
							
							System.out.println("enter controller photo  "+uid);
							return userService.getLoginEmail(uid);
						}			
				
			//getAdminSettingDetails
						@RequestMapping(value = "/getAdminSettingDetails", method = RequestMethod.GET)
						@ResponseBody
						public SettingadminVO getAdminSettingDetails(@RequestParam("sname") String sname) throws Exception
						{
							
							System.out.println("enter controller photo  "+sname);
							return userService.getAdminSettingDetails(sname);
						}		
					
						
						
				//insertMailSetting
						@RequestMapping(value = "/insertMailSetting", method = RequestMethod.POST)
						@ResponseBody
						public MailSettingVO insertMailSetting(@ModelAttribute MailSettingVO loginVO) throws Exception, Exception {
							MailSettingVO Mastervo = new MailSettingVO();
								try {
									Mastervo = userService.insertMailSetting(loginVO);
										} catch (Exception e) 
								{
											System.out.println("Controller Error  "+e);
								}
								return Mastervo;
						}				
						
				//getMailSetting
						@RequestMapping(value = "/getMailSetting", method = RequestMethod.GET)
						@ResponseBody
						public MailSettingVO getMailSetting() throws Exception
						{
							return userService.getMailSetting();
						}	
				//getMailSettingType
						@RequestMapping(value = "/getMailSettingType", method = RequestMethod.GET)
						@ResponseBody
						public MailSettingVO getMailSettingType(@RequestParam("tem_var") String temvar) throws Exception
						{
							return userService.getMailSettingType(temvar);
						}	
				//getIDSetting
						//getMailSetting
						@RequestMapping(value = "/getIDSetting", method = RequestMethod.GET)
						@ResponseBody
						public CounterVO getIDSetting(@RequestParam("Cnt_Prifix") String prifix) throws Exception
						{
							return userService.getIDSetting(prifix);
						}	
				//getIDSettingAll
						@RequestMapping(value = "/getIDSettingAll", method = RequestMethod.GET)
						@ResponseBody
						public CounterVO getIDSettingAll() throws Exception
						{
							return userService.getIDSettingAll();
						}	
				//updateIDSetting
						@RequestMapping(value = "/updateIDSetting", method = RequestMethod.POST)
						@ResponseBody
						public CounterVO updateIDSetting(@ModelAttribute CounterVO counterVO) throws Exception, Exception {
							CounterVO Countervo = new CounterVO();
								try {
									Countervo = userService.updateIDSetting(counterVO);
										} catch (Exception e) 
								{
											System.out.println("Controller Error  "+e);
								}
								return Countervo;
						}			
						
			//insertTerms
						@RequestMapping(value = "/insertTerms", method = RequestMethod.POST)
						@ResponseBody
						public MailSettingVO insertTerms(@ModelAttribute MailSettingVO loginVO) throws Exception, Exception {
							MailSettingVO Mastervo = new MailSettingVO();
								try {
									Mastervo = userService.insertTerms(loginVO);
										} catch (Exception e) 
								{
											System.out.println("Controller Error  "+e);
								}
								return Mastervo;
						}			
				//getTerms		
						@RequestMapping(value = "/getTerms", method = RequestMethod.GET)
						@ResponseBody
						public MailSettingVO getTerms() throws Exception
						{
							return userService.getTerms();
						}	
				//loadCountry
						@RequestMapping(value = "/loadCountry", method = RequestMethod.GET)
						@ResponseBody
						public CountryVO loadCountry() throws Exception
						{
							return userService.loadCountry();
						}	
}
