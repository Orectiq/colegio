package com.controller;
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.beans.factory.annotation.Qualifier; 
import org.springframework.stereotype.Controller; 
import org.springframework.ui.ModelMap; 
import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.ResponseBody; 
import org.springframework.web.bind.annotation.SessionAttributes; 
import org.springframework.web.servlet.ModelAndView; 

import com.model.LibraryVO;
import com.model.ParentVO;
import com.model.TeacherVO;
import com.model.TransportAttendanceVO;
import com.model.TransportVO;
import com.service.LibraryService; 
import com.service.StudentService;
import com.service.TransportService;
import com.service.UserService; 



@Controller 
@RequestMapping("/transport") 
@SessionAttributes("userName3") 
public class TransportController
{

	@Autowired 
	//@Qualifier(value = "transportService") 
	private TransportService transportService; 

	private String userName=""; 



	@RequestMapping(value = "/loadpage", method = RequestMethod.POST) 
	public ModelAndView loadPage(ModelMap modelMap,@RequestParam("d") String pg, HttpServletRequest request, HttpServletResponse response) 
	throws Exception
	{ 
	modelMap.addAttribute("userName3", userName); 
	return new ModelAndView(pg, modelMap); 
	} 
	//stuDetails
	@RequestMapping(value = "/stuDetails", method = RequestMethod.GET)
	public ModelAndView stuDetails(ModelMap modelMap,@RequestParam("id")String uname, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("uname", uname);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+uname);
				
		return new ModelAndView("/transport/StudentDetails", modelMap);
	}
	//stuAODetails
	@RequestMapping(value = "/stuAODetails", method = RequestMethod.GET)
	public ModelAndView stuAODetails(ModelMap modelMap,@RequestParam("id")String uname, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("uname", uname);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+uname);
				
		return new ModelAndView("/transport/AOStudentList", modelMap);
	}
	
	
	//stuAttendance
	@RequestMapping(value = "/stuAttendance", method = RequestMethod.GET)
	public ModelAndView stuAttendance(ModelMap modelMap,@RequestParam("id")String uname, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("uname", uname);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+uname);
				
		return new ModelAndView("/transport/StudentAttendance", modelMap);
	}
	//stuAttendanceHistory
	@RequestMapping(value = "/stuAttendanceHistory", method = RequestMethod.GET)
	public ModelAndView stuAttendanceHistory(ModelMap modelMap,@RequestParam("id")String uname, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("uname", uname);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+uname);
				
		return new ModelAndView("/transport/AttendanceHistory", modelMap);
	}
	//stuAOAttendanceHistory
	@RequestMapping(value = "/stuAOAttendanceHistory", method = RequestMethod.GET)
	public ModelAndView stuAOAttendanceHistory(ModelMap modelMap,@RequestParam("id")String uname, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("uname", uname);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+uname);
				
		return new ModelAndView("/transport/AOAttendanceHistory", modelMap);
	}
	//transSTULIST
	@RequestMapping(value = "/transSTULIST", method = RequestMethod.POST)
	public ModelAndView transSTULIST(ModelMap modelMap,@RequestParam("id")String board, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("board", board);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+board);
				
		return new ModelAndView("/transport/AOStudentListAll", modelMap);
	}
	

	@RequestMapping(value = "/insertTransport", method = RequestMethod.POST, headers = "content-type=application/x-www-form-urlencoded") 
	@ResponseBody 
	public TransportVO insertTransport(@ModelAttribute TransportVO transportVO) throws Exception, Exception 
	{
		System.out.println("Enter Transport Controller");
		System.out.println("transport_name   "+transportVO.getTransport_name());
		
		TransportVO TransportVO = new TransportVO(); 
	try { 
			TransportVO = transportService.insertTransportDetails(transportVO); 
		} catch (Exception e)
		{ 
		} 
	return TransportVO; 
	} 
	
	//insertAttendance
	@RequestMapping(value="/insertAttendance",method=RequestMethod.POST,headers = "content-type=application/x-www-form-urlencoded")
	@ResponseBody
	public TransportAttendanceVO insertAttendance(@ModelAttribute TransportAttendanceVO transVO)throws Exception
	{
		TransportAttendanceVO Transvo=new TransportAttendanceVO();
		try{
			Transvo=transportService.insertAttendance(transVO);
			
		}catch(Exception e)
		{
			System.out.println(e);
		}
		return Transvo;
	}
	

	@RequestMapping(value = "/gettransportdetail", method = RequestMethod.GET)
	@ResponseBody
		public TransportVO gettransportdetail() throws Exception 
	{
					TransportVO Transvo = new TransportVO();
						try{
							Transvo =transportService.gettransportdetail();
						}
						catch(Exception e){
						}
						return Transvo;
	}
	
	
	@RequestMapping(value = "/gettransportdetailall", method = RequestMethod.GET)
	@ResponseBody
		public TransportVO gettransportdetailall(@RequestParam("transport_name")String tname) throws Exception 
	{
					TransportVO Transvo = new TransportVO();
						try{
							Transvo =transportService.gettransportdetailall(tname);
						}
						catch(Exception e){
						}
						return Transvo;
	}
	
	
	
	

	
	//getDname
	@RequestMapping(value = "/getDname", method = RequestMethod.GET)
	@ResponseBody
	public  TransportVO getDname(@RequestParam("uname") String uname) throws Exception 
	{
		System.out.println("enter controller  ");
		return transportService.getDname(uname);
	}
	
	//getAttendanceHistory
	@RequestMapping(value= "/getAttendanceHistory", method = RequestMethod.GET)
	@ResponseBody
	public TransportAttendanceVO getAttendanceHistory(@RequestParam("uname") String uname,@RequestParam("adate") String adate) throws Exception
	{
		return transportService.getAttendanceHistory(uname,adate);
	}
	//getAttendanceHistory
		@RequestMapping(value= "/gettrandetail", method = RequestMethod.GET)
		@ResponseBody
		public TransportAttendanceVO gettrandetail(@RequestParam("duser") String duser)throws Exception
		{
			return transportService.gettrandetail(duser);
		}
		
	//getAttendanceHistoryAbsent
	@RequestMapping(value= "/getAttendanceHistoryAbsent", method = RequestMethod.GET)
	@ResponseBody
	public TransportAttendanceVO getAttendanceHistoryAbsent(@RequestParam("uname") String uname,@RequestParam("adate") String adate) throws Exception
	{
		return transportService.getAttendanceHistoryAbsent(uname,adate);
	}
	
	
	//getTransStudentVOList
	@RequestMapping(value= "/getTransStudentVOList", method = RequestMethod.GET)
	@ResponseBody
	public TransportAttendanceVO getTransStudentVOList(@RequestParam("boarding") String board,@RequestParam("adate") String adate) throws Exception
	{
		return transportService.getTransStudentVOList(board,adate);
	}
	
	//getTransStuListBYBoard
		@RequestMapping(value= "/getTransStuListBYBoard", method = RequestMethod.GET)
		@ResponseBody
		public TransportAttendanceVO getTransStuListBYBoard(@RequestParam("boarding") String board,@RequestParam("adate") String adate) throws Exception
		{
			return transportService.getTransStuListBYBoard(board,adate);
		}
	
	
	//getTransStudentVOListNew
	@RequestMapping(value= "/getTransStudentVOListNew", method = RequestMethod.GET)
	@ResponseBody
	public TransportAttendanceVO getTransStudentVOListNew(@RequestParam("boarding") String board) throws Exception
	{
		return transportService.getTransStudentVOListNew(board);
	}
	
	
	
	//getTransportLoginID		
	@RequestMapping(value = "/getTransportLoginID", method = RequestMethod.GET)
	@ResponseBody
	public  TransportVO getTransportLoginID(@RequestParam("mobile") String mobile) throws Exception 
	{
		System.out.println("enter controller  "+mobile);
		return transportService.getTransportLoginID(mobile);
	}
	
	

	}
	

	

