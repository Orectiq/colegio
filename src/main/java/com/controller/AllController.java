package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.dao.constant.CommonConstant; 

@Controller
@RequestMapping("/all")
public class AllController 
{
	
	String urlnew=CommonConstant.PATH_URL;
	
	//schoolName
		@RequestMapping(value = "/schoolName", method = RequestMethod.GET)
		public ModelAndView schoolName(ModelMap modelMap,@RequestParam("sname")String sname,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
					System.out.println("request value of school name       "+sname+" admid  "+admid+"  url  "+url+" ltype" +ltype+ "slogo" +slogo+ "sheader"+sheader);
					String projectUrl=urlnew+"/admission/MobilePage.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
					
					
			//return new ModelAndView("/admission/Admission", modelMap);
			//return new ModelAndView("/admission/MobilePage", modelMap);
		}
	
	
		//index
				@RequestMapping(value = "/index", method = RequestMethod.POST)
				public ModelAndView index(ModelMap modelMap,@RequestParam("sclass")String sclass,@RequestParam("section")String section,@RequestParam("sid")String sid,@RequestParam("sname")String sname,@RequestParam("url")String url, @RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							System.out.println("request value of school name       "+sclass+" admid  "+section+"  url  "+url+"  sid  "+sid+"   sname  "+sname+"slogo"+slogo+"sheader"+sheader);
							
							String projectUrl="http://localhost:8080/colegioui";
																		   
							return new ModelAndView("redirect:"+projectUrl,modelMap);
							
							
					//return new ModelAndView("/admission/Admission", modelMap);
					//return new ModelAndView("/admission/MobilePage", modelMap);
				}	
		
		
		//class_widget
		@RequestMapping(value = "/class_widget", method = RequestMethod.POST)
		public ModelAndView class_widget(ModelMap modelMap,@RequestParam("sclass")String sclass,@RequestParam("section")String section,@RequestParam("sid")String sid,@RequestParam("sname")String sname,@RequestParam("url")String url, @RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
				throws Exception {
					System.out.println("request value of school name       "+sclass+" admid  "+section+"  url  "+url+"  sid  "+sid+"   sname  "+sname+"slogo"+slogo+"sheader"+sheader);
					
					String projectUrl=urlnew+"/class/Class_Main.jsp?&sclass="+sclass+"&sec="+section+"&sid="+sid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
																   
					return new ModelAndView("redirect:"+projectUrl,modelMap);
					
					
			//return new ModelAndView("/admission/Admission", modelMap);
			//return new ModelAndView("/admission/MobilePage", modelMap);
		}	
		
		@RequestMapping(value = "/ClassSection", method = RequestMethod.GET)
		public ModelAndView ClassSection(ModelMap modelMap,@RequestParam("sname")String sname,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
					String projectUrl=urlnew+"/class/ClassSection.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
		}	
		//GeneralSetting
		@RequestMapping(value = "/GeneralSetting", method = RequestMethod.GET)
		public ModelAndView GeneralSetting(ModelMap modelMap,@RequestParam("sname")String sname,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
					String projectUrl=urlnew+"/Settings/adminSetting.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
		}
		
		//TermsConditions
		@RequestMapping(value = "/TermsConditions", method = RequestMethod.GET)
		public ModelAndView TermsConditions(ModelMap modelMap,@RequestParam("sname")String sname,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
					String projectUrl=urlnew+"/Settings/TermsConditions.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					return new ModelAndView("redirect:"+projectUrl,modelMap);
		}
		
		//TermsConditions
				@RequestMapping(value = "/Timetable_Form", method = RequestMethod.GET)
				public ModelAndView Timetable_Form(ModelMap modelMap,@RequestParam("sname")String sname,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Settings/Timetable_Form.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		
		
		//changePassword
		@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
		public ModelAndView changePassword(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
					String projectUrl=urlnew+"changepassword.jsp";
					return new ModelAndView("redirect:"+projectUrl,modelMap);
		}
		
		
		//mobileCheck
		@RequestMapping(value = "/mobileCheck", method = RequestMethod.GET)
		public ModelAndView mobileCheck(ModelMap modelMap,@RequestParam("obj1")String sch,@RequestParam("obj2")String mobile,@RequestParam("obj3")String admid, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
					System.out.println("*****************************");
					request.setAttribute("SchName", sch);
					request.setAttribute("mobile", mobile);
					request.setAttribute("admid", admid);
					
					System.out.println("request value of exam       "+sch+","+mobile+" admid  "+admid);
					
			//return new ModelAndView("/MarkSheet/SMarksheet", modelMap);
					return new ModelAndView("/admission/Admission", modelMap);
		}	
		//mobileCheck1
				@RequestMapping(value = "/mobileCheck1", method = RequestMethod.POST)
				public ModelAndView mobileCheck1(ModelMap modelMap,@RequestParam("obj1")String sch,@RequestParam("obj2")String mobile,@RequestParam("obj3")String admid,@RequestParam("obj4")String lurl,@RequestParam("obj5")String slogo,@RequestParam("obj6")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							System.out.println("*****************************");
							/*request.setAttribute("SchName", sch);
							request.setAttribute("mobile", mobile);
							request.setAttribute("admid", admid);*/
							System.out.println("request value of mobilecheck     "+sch+","+mobile+" admid  "+admid+" url  "+lurl+"slogo"+slogo+"sheader"+sheader);
							
							String projectUrl=urlnew+"/admission/Admission.jsp?&sname="+sch+"&mobile="+mobile+"&admid="+admid+"&url="+lurl+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
							
					//return new ModelAndView("/MarkSheet/SMarksheet", modelMap);
							//return new ModelAndView("/admission/Admission", modelMap);
				}	
		
		//libAdminDueDatebooklist
				@RequestMapping(value = "/libAdminDueDatebooklist", method = RequestMethod.GET)
				public ModelAndView libDueDatebooklist(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							//request.setAttribute("lid", sid);
							//System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Library/Admin_DuedateList.jsp?&admid="+admid+"&url="+url+"&sname="+sname;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//adminDormitorylistAll
				@RequestMapping(value = "/adminDormitorylistAll", method = RequestMethod.GET)
				public ModelAndView adminDormitorylistAll(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							//request.setAttribute("lid", sid);
							//System.out.println("^^^^^^^^^^^^^^^^^^ session value of studid  ^^^^^^^^^^    "+sid);
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Dormitory/Admin_Dormitory_ListAll.jsp?&admid="+admid+"&url="+url+"&sname="+sname;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
		//adminFeeAllotAll
				@RequestMapping(value = "/adminFeeAllotAll", method = RequestMethod.GET)
				public ModelAndView adminFeeAllotAll(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname, @RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							//request.setAttribute("lid", sid);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url fess allot  ^^^^^^^^^^    "+url+" ltype" +ltype+ "slogo" +slogo+ "sheader"+sheader);
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Accounts/FeeAllotMainpage.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
			//adminCalendar
				@RequestMapping(value = "/adminCalendar", method = RequestMethod.GET)
				public ModelAndView adminCalendar(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							//request.setAttribute("lid", sid);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url+" ltype" +ltype+ "slogo" +slogo+ "sheader"+sheader);
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Calendar/Admin_Calendar.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
			//adminDashboard
				@RequestMapping(value = "/adminDashboard", method = RequestMethod.GET)
				public ModelAndView adminDashboard(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							//request.setAttribute("lid", sid);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url admin dashboard  ^^^^^^^^^^   "+url+" ltype" +ltype+ "slogo" +slogo+ "sheader"+sheader);
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Dashboard/Admin_Dashboard.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}			
				//adminTeacherAttendanceHistory
				/*@RequestMapping(value = "/adminTeacherAttendanceHistory", method = RequestMethod.GET)
				public ModelAndView adminTeacherAttendanceHistory(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							//request.setAttribute("lid", sid);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url);
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Attendance/Admin_Teacher_Attendance_History.jsp?&admid="+admid+"&url="+url+"&sname="+sname;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		*/	
				
			//adminAssignmentStatus
				@RequestMapping(value = "/adminAssignmentStatus", method = RequestMethod.GET)
				public ModelAndView adminAssignmentStatus(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname, @RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							//request.setAttribute("lid", sid);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url);
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Assignment/Admin_AssignmentStatus.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				//adminTransportStatus
				@RequestMapping(value = "/adminTransportStatus", method = RequestMethod.GET)
				public ModelAndView adminTransportStatus(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							//request.setAttribute("lid", sid);
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url);
							
					//return new ModelAndView("/library/DuedateList", modelMap);
							String projectUrl=urlnew+"/Transport/Admin_TransportPage.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	

				//adminDormitoryStatus
				@RequestMapping(value = "/adminDormitoryStatus", method = RequestMethod.GET)
				public ModelAndView adminDormitoryStatus(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url);
							String projectUrl=urlnew+"/Dormitory/Admin_DormitoryPage.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
			//mediaCenter	
				@RequestMapping(value = "/mediaCenter", method = RequestMethod.GET)
				public ModelAndView mediaCenter(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url);
							String projectUrl=urlnew+"/Media/Media_Center.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
				//Galleryupload	
				@RequestMapping(value = "/Galleryupload", method = RequestMethod.GET)
				public ModelAndView Galleryupload(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url);
							String projectUrl=urlnew+"/Media/Gallery_View.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
			//adminDormitoryStatus
				@RequestMapping(value = "/adminLibraryStatus", method = RequestMethod.GET)
				public ModelAndView adminLibraryStatus(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url+ "ltype"+ltype+ "slogo"+slogo+ "sheader"+sheader);
							String projectUrl=urlnew+"/Library/Admin_LibraryPage.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
			//adminTeacherLeaveRequest
				@RequestMapping(value = "/adminTeacherLeaveRequest", method = RequestMethod.GET)
				public ModelAndView adminTeacherLeaveRequest(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url+ "ltype"+ltype+ "slogo"+slogo+ "sheader"+sheader);
							String projectUrl=urlnew+"/Leave/TeachLeaveApproval.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
				
			//adminTeacherAdd
				@RequestMapping(value = "/adminTeacherAdd", method = RequestMethod.GET)
				public ModelAndView adminTeacherAdd(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname, @RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    "+url);
							String projectUrl=urlnew+"/Teachers/Admin_TeacherAdd.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
			//adminTeacherAssign	
				@RequestMapping(value = "/adminTeacherAssign1", method = RequestMethod.GET)
				public ModelAndView adminTeacherAssign1(ModelMap modelMap,@RequestParam("techid")String tid,@RequestParam("techname")String tname,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception
				{
							System.out.println("^^^^^^^^^^^^^^^^^^ session value of url 11111  ^^^^^^^^^^    ");
							String projectUrl=urlnew+"/Teachers/Teacher_SubjectAssign.jsp?&tid="+tid+"&tname="+tname+"&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
		
		//stuList
				@RequestMapping(value = "/stuList", method = RequestMethod.GET)
				public ModelAndView stuList(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							
							System.out.println("request value of admid  &&&&&&&&&&&&&&      "+admid+" and "+sname+"  and  "+url);
							String projectUrl=urlnew+"/Students/StudentMain.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&slogo="+slogo+"&ltype="+ltype+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
							
					//return new ModelAndView("/MarkSheet/SMarksheet", modelMap);
							//return new ModelAndView("/student/StudentMain", modelMap);
				}	
		//parent
				@RequestMapping(value = "/parent", method = RequestMethod.GET)
				public ModelAndView parent(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+ "ltype"+ltype+ "slogo"+slogo+ "sheader"+sheader);
							String projectUrl=urlnew+"/Parents/Parent_Main.jsp?&admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);	
					//return new ModelAndView("/MarkSheet/SMarksheet", modelMap);
							//return new ModelAndView("/Parents/Parent_Main", modelMap);
				}		
		//parentView
				@RequestMapping(value = "/parentView", method = RequestMethod.GET)
				public ModelAndView parentView(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							String projectUrl=urlnew+"/Parents/ParentsView.jsp?&admid="+admid+"&url="+url+"&sname="+sname;
							return new ModelAndView("redirect:"+projectUrl,modelMap);	
							
					//return new ModelAndView("/MarkSheet/SMarksheet", modelMap);
							//return new ModelAndView("/parents/ParentsView", modelMap);
				}		
		//activityMain
				@RequestMapping(value = "/activityMain", method = RequestMethod.GET)
				public ModelAndView activityMain(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
					System.out.println("request value of admid class main////       "+admid+ "ltype"+ltype+ "slogo"+slogo+ "sheader"+sheader);
					String projectUrl=urlnew+"/class/ClassMain.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);	
					//return new ModelAndView("/MarkSheet/SMarksheet", modelMap);
							//return new ModelAndView("/class/Class_Main", modelMap);
				}	
		//addActivity
				@RequestMapping(value = "/addActivity", method = RequestMethod.GET)
				public ModelAndView addActivity(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							
					//return new ModelAndView("/MarkSheet/SMarksheet", modelMap);
							//return new ModelAndView("/class/AddActivity", modelMap);
							String projectUrl=urlnew+"/Class/AddActivity.jsp?&admid="+admid+"&sname="+sname+"&url="+url;
							return new ModelAndView("redirect:"+projectUrl,modelMap);	
				}	
		//teacherList
				@RequestMapping(value = "/teacherList", method = RequestMethod.GET)
				public ModelAndView teacherList(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("slogo")String slogo,@RequestParam("ltype")String ltype,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid );
							System.out.println("request value of admid teacher////       "+admid+ "ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
							//return new ModelAndView("/teachers/TeachersMain", modelMap);
							
							String projectUrl=urlnew+"/Teachers/TeachersMain.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
				
				
				
		//teacherSubject		
				@RequestMapping(value = "/teacherSubject", method = RequestMethod.GET)
				public ModelAndView teacherSubject(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
							
					//return new ModelAndView("/MarkSheet/SMarksheet", modelMap);
							//return new ModelAndView("/teachers/Teacher_Subject", modelMap);
							
							String projectUrl=urlnew+"/Teachers/Teacher_Subject.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);		
							
							
				}
				
		//teacherAdd
				@RequestMapping(value = "/teacherAdd", method = RequestMethod.GET)
				public ModelAndView teacherAdd(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							
					//return new ModelAndView("/MarkSheet/SMarksheet", modelMap);
							return new ModelAndView("/teachers/Teachers", modelMap);
				}	
		//NonTeacherList
				@RequestMapping(value = "/NonTeacherList", method = RequestMethod.GET)
				public ModelAndView NonTeacherList(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid Nonteach////////  "+admid+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
							//return new ModelAndView("/NonTeachingStaff/Nonteachinglist", modelMap);
							
							String projectUrl=urlnew+"/Nonteachingstaff/Admin_Nonteachinglist.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//NonTeacherAdd
				@RequestMapping(value = "/NonTeacherAdd", method = RequestMethod.GET)
				public ModelAndView NonTeacherAdd(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url, @RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
							//return new ModelAndView("/NonTeachingStaff/Nonteachingview", modelMap);
							
							String projectUrl=urlnew+"/Nonteachingstaff/Admin_NonteachingAdd.jsp?&admid="+admid+"&sname="+sname+"&url="+url+" &ltype="+ltype+" &slogo="+slogo+"&sheader="+sheader;
							return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
		//attendance
				@RequestMapping(value = "/attendance", method = RequestMethod.GET)
				public ModelAndView attendance(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/attendance/Attendance", modelMap);
				}	
		//attendanceHistory
				@RequestMapping(value = "/attendanceHistory", method = RequestMethod.GET)
				public ModelAndView attendanceHistory(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/attendance/AttendanceListAll", modelMap);
				}	
		//Examlist
				@RequestMapping(value = "/Examlist", method = RequestMethod.GET)
				public ModelAndView Examlist(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							
							String projectUrl=urlnew+"/Exam/ExamList.jsp?&admid="+admid+"&sname="+sname+"&url="+url+ "&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);

							//return new ModelAndView("/exam/Examlist", modelMap);
				}	
		//ExamAdd
				@RequestMapping(value = "/ExamAdd", method = RequestMethod.GET)
				public ModelAndView ExamAdd(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							System.out.println("request value of admid ---->      "+admid);
							//return new ModelAndView("/exam/Examid", modelMap);
							String projectUrl=urlnew+"/Exam/Examid.jsp?&admid="+admid+"&sname="+sname+"&url="+url+ "&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//MarksAdd
				@RequestMapping(value = "/MarksAdd", method = RequestMethod.GET)
				public ModelAndView MarksAdd(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							
							String projectUrl=urlnew+"/Exam/Exam.jsp?&admid="+admid+"&sname="+sname+"&url="+url;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
							//return new ModelAndView("/exam/Exam", modelMap);
				}
				
		//MarkSheet
				@RequestMapping(value = "/MarkSheet", method = RequestMethod.GET)
				public ModelAndView MarkSheet(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader );
							String projectUrl=urlnew+"/MarkSheet/Marks_ClassList.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
							//return new ModelAndView("/MarkSheet/Marks_ClassList", modelMap);
				}	
		//FeeAllot
				@RequestMapping(value = "/FeeAllot", method = RequestMethod.GET)
				public ModelAndView FeeAllot(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/Accounts/FeeAllotMainpage", modelMap);
				}
		//Account_EChallan
				@RequestMapping(value = "/Account_EChallan", method = RequestMethod.GET)
				public ModelAndView Account_EChallan(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							
					System.out.println("request value of admid       "+admid+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
					String projectUrl=urlnew+"/Accounts/Accounting_EChallan.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//events_table
				@RequestMapping(value = "/events_table", method = RequestMethod.GET)
				public ModelAndView events_table(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
							String projectUrl=urlnew+"/Noticeboard/events_table.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
							//return new ModelAndView("/NoticeBoard/events_table", modelMap);
				}	
		//events_table
				@RequestMapping(value = "/news_table", method = RequestMethod.GET)
				public ModelAndView news_table(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
							
							String projectUrl=urlnew+"/Noticeboard/news_table.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
							//return new ModelAndView("/NoticeBoard/news_table", modelMap);
				}	
		//addevent
				@RequestMapping(value = "/addevent", method = RequestMethod.GET)
				public ModelAndView addevent(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
							
							
							String projectUrl=urlnew+"/Noticeboard/addevents.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
					 		
							//return new ModelAndView("/NoticeBoard/addevent", modelMap);
				}
		//NoticeBoard/addnews
				@RequestMapping(value = "/addnews", method = RequestMethod.GET)
				public ModelAndView addnews(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url, @RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+"ltype"+ltype+"slogo"+slogo+"sheader"+sheader);
							
							String projectUrl=urlnew+"/Noticeboard/addnews.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
							
							//return new ModelAndView("/NoticeBoard/addnews", modelMap);
				}
				
		//*******************************  PRO-Dashboard *************************************
		//Dormitory/DormitoryMainPage
				@RequestMapping(value = "/DormitoryMain", method = RequestMethod.GET)
				public ModelAndView DormitoryMain(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/Dormitory/DormitoryMainPage", modelMap);
				}	
		//Dormitory/Dormitory_AddView
				@RequestMapping(value = "/DormitoryAdd", method = RequestMethod.GET)
				public ModelAndView DormitoryAdd(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/Dormitory/Dormitory_AddView", modelMap);
				}	
		//Dormitory/Allotment
				@RequestMapping(value = "/DormitoryAllot", method = RequestMethod.GET)
				public ModelAndView DormitoryAllot(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/Dormitory/Allotment", modelMap);
				}
		//transport/transport_table
				@RequestMapping(value = "/TransportList", method = RequestMethod.GET)
				public ModelAndView TransportList(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/transport/transport_table", modelMap);
				}
		//transport/addtransport
				@RequestMapping(value = "/TransportAdd", method = RequestMethod.GET)
				public ModelAndView TransportAdd(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/transport/addtransport", modelMap);
				}
		//Leave/leaveapproval
				@RequestMapping(value = "/leaveapproval", method = RequestMethod.GET)
				public ModelAndView leaveapproval(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/Leave/leaveapproval", modelMap);
				}
		//StudyMaterials/add_study_material
				@RequestMapping(value = "/StudyMaterialsAdd", method = RequestMethod.GET)
				public ModelAndView StudyMaterialsAdd(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/StudyMaterials/add_study_material", modelMap);
				}
		//class/ClassSection
				@RequestMapping(value = "/ClassSection1", method = RequestMethod.GET)
				public ModelAndView ClassSection1(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/class/ClassSection1", modelMap);
				}
		//subject/subjectlist
				@RequestMapping(value = "/subjectlist", method = RequestMethod.GET)
				public ModelAndView subjectlist(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+ "ltype"+ltype+ "slogo"+slogo+"sheader"+sheader);
							//return new ModelAndView("/subject/subjectlist", modelMap);
							String projectUrl=urlnew+"/Subject/subjectlist.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
		//subject/subject		
				@RequestMapping(value = "/subjectAdd", method = RequestMethod.GET)
				public ModelAndView subjectAdd(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("sname")String sname,@RequestParam("url")String url, @RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid+ "ltype"+ltype+ "slogo"+slogo+"sheader"+sheader);
							//return new ModelAndView("/subject/subject", modelMap);
							String projectUrl=urlnew+"/Subject/subject.jsp?&admid="+admid+"&sname="+sname+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);

				}
				
		//Setting/AdminSetting
				@RequestMapping(value = "/AdminSetting", method = RequestMethod.GET)
				public ModelAndView AdminSetting(ModelMap modelMap,@RequestParam("admid")String admid, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							request.setAttribute("admid", admid);
							System.out.println("request value of admid       "+admid);
							return new ModelAndView("/Setting/AdminSetting", modelMap);
				}
				
				//IdcardMainPage
				@RequestMapping(value = "/IdcardMainPage", method = RequestMethod.GET)
				public ModelAndView IdcardMainPage(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							//System.out.println("request value of admid       "+admid);
							//return new ModelAndView("/Settings/Admin_StuIDCard", modelMap);
							
							String projectUrl=urlnew+"/Settings/IdCardMainPage.jsp?admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}			
				
			//Admin_StuIDCard
				@RequestMapping(value = "/Admin_StuIDCard", method = RequestMethod.POST)
				public ModelAndView Admin_StuIDCard(ModelMap modelMap,@RequestParam("sname")String sname,@RequestParam("sclass")String sclass,@RequestParam("section")String sec, @RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,@RequestParam("url")String url,HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							
					System.out.println("request value of.......... ltype"+ltype+"slogo"+slogo+"sheader"+sheader+"url"+url);
				
							String projectUrl=urlnew+"/Settings/StudentIdCard.jsp?&sclass="+sclass+"&sec="+sec+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader+"&url="+url;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
				//Admin_Profile
				@RequestMapping(value = "/Admin_Profile", method = RequestMethod.GET)
				public ModelAndView Admin_Profile(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							//System.out.println("request value of admid       "+admid);
							//return new ModelAndView("/Settings/Admin_StuIDCard", modelMap);
							
							String projectUrl=urlnew+"/Settings/AdminProfile.jsp?admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}
				
				
				
				
				//Stu_AttendanceReport
				@RequestMapping(value = "/Stu_AttendanceReport", method = RequestMethod.GET)
				public ModelAndView Stu_AttendanceReport(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							//System.out.println("request value of admid       "+admid);
							//return new ModelAndView("/Settings/Admin_StuIDCard", modelMap);
							
							String projectUrl=urlnew+"/Report/Stu_AttendanceReport.jsp?admid="+admid+"&url="+url+"&sname="+sname;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
				//UserReport
				@RequestMapping(value = "/UserReport", method = RequestMethod.GET)
				public ModelAndView UserReport(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							//System.out.println("request value of admid       "+admid);
							//return new ModelAndView("/Settings/Admin_StuIDCard", modelMap);
							
							String projectUrl=urlnew+"/Report/UserReport.jsp?admid="+admid+"&url="+url+"&sname="+sname;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}	
				
				//StudentsMarksReport
				@RequestMapping(value = "/StudentsMarksReport", method = RequestMethod.GET)
				public ModelAndView StudentsMarksReport(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							//request.setAttribute("admid", admid);
							//System.out.println("request value of admid       "+admid);
							//return new ModelAndView("/Settings/Admin_StuIDCard", modelMap);
							
							String projectUrl=urlnew+"/Report/StudentsMarksReport.jsp?admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
				//TeacherAttendanceReport
				@RequestMapping(value = "/TeacherAttendanceReport", method = RequestMethod.GET)
				public ModelAndView TeacherAttendanceReport(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader,HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Report/Teacher_AttendanceReport.jsp?admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
				
				//Admin_TeacherAttendanceHistory
				@RequestMapping(value = "/Admin_TeacherAttendanceHistory", method = RequestMethod.GET)
				public ModelAndView Admin_TeacherAttendanceHistory(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Teachers/Admin_TeacherAttendanceHistory.jsp?admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
				//Admin_TeacherAttendanceHistoryAll
				@RequestMapping(value = "/Admin_TeacherAttendanceHistoryAll", method = RequestMethod.GET)
				public ModelAndView Admin_TeacherAttendanceHistoryAll(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Teachers/Admin_TeacherAttendanceHistoryAll.jsp?admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				//MailTemplateAdd
				@RequestMapping(value = "/MailTemplateAdd", method = RequestMethod.GET)
				public ModelAndView MailTemplateAdd(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Settings/MailTemplateAdd.jsp?admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				//MailTemplateList
				@RequestMapping(value = "/MailTemplateList", method = RequestMethod.GET)
				public ModelAndView MailTemplateList(ModelMap modelMap,@RequestParam("admid")String admid,@RequestParam("url")String url,@RequestParam("sname")String sname,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							String projectUrl=urlnew+"/Settings/MailTemplateList.jsp?admid="+admid+"&url="+url+"&sname="+sname+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				//mailRead        &mobile='+unameLogin+'&url='+loginurl+'&ltype='+ttype+'&slogo='+slogo+'&sheader='+sheader+'&mailto='+mto+'&mdate='+mdate+'&mtime='+mtime+'
				@RequestMapping(value = "/mailRead", method = RequestMethod.GET)
				public ModelAndView mailRead(ModelMap modelMap,@RequestParam("mailto")String mailto,@RequestParam("mdate")String mdate,@RequestParam("mtime")String mtime,@RequestParam("mailfrom")String mfrom, @RequestParam("subj")String subj,HttpServletRequest request, HttpServletResponse response)
						throws Exception {
							System.out.println("subject  "+subj+" mail from  "+mfrom);
							String projectUrl=urlnew+"/Mail/ReadMail.jsp?&mailto="+mailto+"&mdate="+mdate+"&mtime="+mtime+"&mailfrom="+mfrom+"&subj="+subj;
					 		return new ModelAndView("redirect:"+projectUrl,modelMap);
				}		
				
}
