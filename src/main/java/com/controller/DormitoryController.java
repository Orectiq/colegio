package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.model.DormitoryAllotAllVO;
import com.model.DormitoryAllotVO;
import com.model.DormitoryVO;
import com.model.StuVO;
import com.service.DormitoryService;

@Controller
@RequestMapping("/Dormitory")
@SessionAttributes("userName2")
public class DormitoryController {
	
	@Autowired
	@Qualifier(value = "dormitoryService")
	private DormitoryService dormitoryService;
	
	private String userName = "";
	
	@RequestMapping(value = "/loadpage", method = RequestMethod.GET)
	public ModelAndView loadPage(ModelMap modelMap, @RequestParam("d") String pg, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		modelMap.addAttribute("userName2", userName);
		return new ModelAndView(pg, modelMap);
	}
	
	//dormitoryLIST
	@RequestMapping(value = "/dormitoryLIST", method = RequestMethod.POST)
	public ModelAndView examScheList(ModelMap modelMap,@RequestParam("id")String dname, HttpServletRequest request, HttpServletResponse response)
			throws Exception
	{
				request.setAttribute("dname", dname);
				System.out.println("^^^^^^^^^^^^^^^^^^ session value of Dormitory  ^^^^^^^^^^    "+dname);
				
		return new ModelAndView("/Dormitory/AO_Dormitory_ListAll", modelMap);
	}
	//DormitoryAllot
		@RequestMapping(value = "DormitoryAllot", method = RequestMethod.GET)
		public ModelAndView DormitoryAllot(ModelMap modelMap,@RequestParam("id")String dname, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{			//String dname="NIL";
					request.setAttribute("dname", dname);
					System.out.println("^^^^^^^^^^^^^^^^^^ session value of Dormitory  ^^^^^^^^^^    "+dname);
					
			return new ModelAndView("/Dormitory/AO_Allotment", modelMap);
		}
	//addStuAO
		@RequestMapping(value = "addStuAO", method = RequestMethod.GET)
		public ModelAndView addStuAO(ModelMap modelMap,@RequestParam("id")String dname, HttpServletRequest request, HttpServletResponse response)
				throws Exception
		{			//String dname="NIL";
					request.setAttribute("dname", dname);
					System.out.println("^^^^^^^^^^^^^^^^^^ session value of Dormitory  ^^^^^^^^^^    "+dname);
					
			return new ModelAndView("/Dormitory/AO_AddStudent", modelMap);
		}
	
	@RequestMapping(value = "/insertDormitory", method = RequestMethod.POST)
	@ResponseBody
	public DormitoryVO insertDormitory(@ModelAttribute DormitoryVO dormitoryVO) throws Exception 
	{
		
		DormitoryVO Dormitoryvo = new DormitoryVO();
		
		System.out.println("controller dormitory");
		System.out.println(dormitoryVO.getDormitory_name());
		System.out.println(dormitoryVO.getLocation());
		System.out.println(dormitoryVO.getFee());
		System.out.println(dormitoryVO.getCgender());
		System.out.println("controller dormitory end");
		
		try {
			Dormitoryvo = dormitoryService.insertDormitoryDetails(dormitoryVO);
				} catch (Exception e) {
		}
		return Dormitoryvo;
	}
	//updateRoomAllot
		@RequestMapping(value = "/updateRoomAllot", method = RequestMethod.POST)
		@ResponseBody
		public DormitoryVO updateRoomAllot(@ModelAttribute DormitoryVO dormitoryVO) throws Exception 
		{
			
			DormitoryVO Dormitoryvo = new DormitoryVO();
			
			System.out.println("controller dormitory");
			System.out.println(dormitoryVO.getDormitory_name());
			System.out.println(dormitoryVO.getLocation());
			System.out.println("controller dormitory end");
			
			try {
				Dormitoryvo = dormitoryService.updateRoomAllot(dormitoryVO);
					} catch (Exception e) {
			}
			return Dormitoryvo;
		}
	
	
	
	//insertRoomAllot
	@RequestMapping(value = "/insertRoomAllot", method = RequestMethod.POST)
	@ResponseBody
	public DormitoryAllotVO insertRoomAllot(@ModelAttribute DormitoryAllotVO dormitoryVO) throws Exception 
	{
		
		DormitoryAllotVO Dormitoryvo = new DormitoryAllotVO();
		
		System.out.println("controller dormitory");
		System.out.println(dormitoryVO.getDormitory_name());
		System.out.println(dormitoryVO.getLocation());
		System.out.println("controller dormitory end");
		
		try {
			Dormitoryvo = dormitoryService.insertRoomAllot(dormitoryVO);
				} catch (Exception e) {
		}
		return Dormitoryvo;
	}
	
	
	@RequestMapping(value = "/getltrreferencedata", method = RequestMethod.GET)
	@ResponseBody
	public DormitoryVO getLtrReferenceData() throws Exception {
		DormitoryVO Dormitoryvo = new DormitoryVO();
		try{
			Dormitoryvo =dormitoryService.getLtrReferenceData();
		}
		catch(Exception e){
		}
		return Dormitoryvo;
	}
	//getHostelList
	@RequestMapping(value = "/getHostelList", method = RequestMethod.GET)
	@ResponseBody
	public DormitoryVO getHostelList(@RequestParam("cgender") String gender) throws Exception
	{
		
		System.out.println("enter controller Hostel  "+gender);
		return dormitoryService.getHostelList(gender);
	}
	
	//getNonDormitoryStudent
	@RequestMapping(value = "/getNonDormitoryStudent", method = RequestMethod.GET)
	@ResponseBody
	public StuVO getNonDormitoryStudent() throws Exception
	{
		
		//System.out.println("enter controller Hostel  "+sclass+"sec  "+sec);
		return dormitoryService.getNonDormitoryStudent();
	}
	
	
	//dormitoryLIST
	/*@RequestMapping(value = "/dormitoryLIST", method = RequestMethod.GET)
	@ResponseBody
	public DormitoryAllotAllVO dormitoryLIST(@RequestParam("cgender") String gender) throws Exception
	{
		
		System.out.println("enter controller Hostel  "+gender);
		return dormitoryService.dormitoryLIST(gender);
	}*/
	
	
	//getDormitoryList
	@RequestMapping(value = "/getDormitoryList", method = RequestMethod.GET)
	@ResponseBody
	public DormitoryVO getDormitoryList(@RequestParam("dormitory_name") String dname) throws Exception
	{
		
		System.out.println("enter controller Hostel  "+dname);
		return dormitoryService.getDormitoryList(dname);
	}
	
	//getDormitoryAllotList
		@RequestMapping(value = "/getDormitoryAllotList", method = RequestMethod.GET)
		@ResponseBody
		public DormitoryAllotVO getDormitoryAllotList(@RequestParam("dormitory_name") String dname) throws Exception
		{
			
			System.out.println("enter controller Hostel  "+dname);
			return dormitoryService.getDormitoryAllotList(dname);
		}
	
	
	
	
	//editHostel
	@RequestMapping(value = "/editHostel", method = RequestMethod.POST)
	@ResponseBody
	public StuVO editHostel(@ModelAttribute StuVO stuVO) throws Exception
	{
		StuVO Stuvo = new StuVO();
		try {
			Stuvo = dormitoryService.editHostel(stuVO);
				} catch (Exception e) {
		}
		return Stuvo;	
	}
	
	

}
