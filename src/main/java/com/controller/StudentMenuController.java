package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dao.constant.CommonConstant;
@Controller
@RequestMapping("/studentMenu")
public class StudentMenuController
{
String urlnew=CommonConstant.PATH_URL;
	

//Main Dashboard
@RequestMapping(value = "/StudentDashBoard", method = RequestMethod.GET)
public ModelAndView StudentDashBoard(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Dashboard/Student_DashBoard.jsp?&mobile="+mobile+"&url="+url+"&ltype="+slogo+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}


//Marks
@RequestMapping(value = "/MarksStudentList", method = RequestMethod.GET)
public ModelAndView MarksStudentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/MarkSheet/Students_MarksList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}

//AttendanceStudent
@RequestMapping(value = "/AttendanceStudent", method = RequestMethod.GET)
public ModelAndView AttendanceStudent(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Attendance/AttendanceStudent.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}


//ExamStudentList
@RequestMapping(value = "/ExamStudentList", method = RequestMethod.GET)
public ModelAndView ExamStudentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Exam/Students_ExamList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}

//FeesStudentList
@RequestMapping(value = "/FeesStudentList", method = RequestMethod.GET)
public ModelAndView FeesStudentList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Accounts/Students_FeeList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}


//HomeworkStudent
@RequestMapping(value = "/HomeworkStudent", method = RequestMethod.GET)
public ModelAndView HomeworkStudent(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/homework/HomworStuHisList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}
//NoticeboardStudent
@RequestMapping(value = "/NoticeboardStudent", method = RequestMethod.GET)
public ModelAndView NoticeboardStudent(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Noticeboard/Students_NewsList.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}


//LibraryRequestList
@RequestMapping(value = "/LibraryRequestList", method = RequestMethod.GET)
public ModelAndView LibraryRequestList(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Library/StudentLibraryBookReq.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}

//AbsenceHistory
@RequestMapping(value = "/AbsenceHistory", method = RequestMethod.GET)
public ModelAndView AbsenceHistory(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Students/Absence_History.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}

//AbsenceRequest
@RequestMapping(value = "/AbsenceRequest", method = RequestMethod.GET)
public ModelAndView AbsenceRequest(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Students/AbsenceRequest.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}


//Assignment
@RequestMapping(value = "/Assignment", method = RequestMethod.GET)
public ModelAndView Assignment(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Assignment/Students_Assignment.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}
//Calendar
@RequestMapping(value = "/Calendar", method = RequestMethod.GET)
public ModelAndView Calendar(ModelMap modelMap,@RequestParam("mobile")String mobile,@RequestParam("url")String url,@RequestParam("ltype")String ltype,@RequestParam("slogo")String slogo,@RequestParam("sheader")String sheader, HttpServletRequest request, HttpServletResponse response)
		throws Exception
{
			
	//return new ModelAndView("/library/DuedateList", modelMap);
			String projectUrl=urlnew+"/Calendar/Students_Calendar.jsp?&mobile="+mobile+"&url="+url+"&ltype="+ltype+"&slogo="+slogo+"&sheader="+sheader;
			return new ModelAndView("redirect:"+projectUrl,modelMap);
}



}
