package com.common;

import java.util.List;

import com.model.StuVO;

public class ExamTypeMasterVO
{
	String exam_type=null;
	private List<ExamTypeMasterVO> examtypeServiceVOList = null;
	public String getExam_type() {
		return exam_type;
	}
	public void setExam_type(String exam_type) {
		this.exam_type = exam_type;
	}
	public List<ExamTypeMasterVO> getExamtypeServiceVOList() {
		return examtypeServiceVOList;
	}
	public void setExamtypeServiceVOList(List<ExamTypeMasterVO> examtypeServiceVOList) {
		this.examtypeServiceVOList = examtypeServiceVOList;
	}
	
	

}
