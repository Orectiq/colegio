package com.common;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.controller.NonteachingController;

public class Mailjava
{
	public void sendNewMail()
	{
		String crunchifyConfFile = "dispatcher-servlet.xml";
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(crunchifyConfFile);

		// @Service("crunchifyEmail") <-- same annotation you specified in CrunchifyEmailAPI.java
		NonteachingController crunchifyEmailAPI = (NonteachingController) context.getBean("crunchifyEmail");
		String toAddr = "jminfotech.mdu@gmail.com";
		String fromAddr = "jaiganesh.praveenram@gmail.com";

		// email subject
		String subject = "Hey.. This email sent by Crunchify's Spring MVC Tutorial";

		// email body
		String body = "There you go.. You got an email.. Let's understand details on how Spring MVC works -- By Crunchify Admin";
		//crunchifyEmailAPI.crunchifyReadyToSendEmail(toAddr, fromAddr, subject, body);
	}
}
