package com.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="teacher_master")
public class TeachVO 
{
public static final String TYPE = MasVO.class.getName();
	
	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Tid")
	private String tid = null;
		
	@Column(name = "uname")
	private String uname = null;
	
	@Column(name = "pass")
	private String pass = null;

	

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public static String getType() {
		return TYPE;
	}
	
	
	
	
}
