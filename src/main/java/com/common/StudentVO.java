package com.common;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Lob;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="studentsmaster")
public class StudentVO implements Serializable, Cloneable
{
	public static final String TYPE = StudentVO.class.getName();
	
	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "student_id")
	private String stuid = null;
		
	@Column(name = "fname")
	private String firstname = null;
	
	@Column(name = "lname")
	private String lastname = null;
	@Lob
	@Column(name = "photo")
	private String sphoto = null;
	
	@Column(name = "dob")
	private Date sdob = null;
	
	@Column(name = "bplace")
	private String bplace = null;
	
	@Column(name = "nationality")
	private String nationality = null;
	
	@Column(name = "mtongue")
	private String mtongue = null;
	
	@Column(name = "cgender")
	private String cgender = null;
	
	@Column(name = "religion")
	private String religion = null;
	
	@Column(name = "category")
	private String category = null;
	
	@Column(name = "address1")
	private String address1 = null;
	
	@Column(name = "address2")
	private String address2 = null;
	
	@Column(name = "state")
	private String state = null;
	
	@Column(name = "pin")
	private int pin = 0;
	
	@Column(name = "mobile")
	private String mobile = null;
	
	@Column(name = "phone")
	private String phone = null;
	
	@Column(name = "cemail")
	private String cemail = null;
	
	@Column(name = "sch_transport")
	private String sch_transport = null;

	@Column(name = "prd_from")
	private Date prd_from = null;
	
	@Column(name = "prd_to")
	private Date prd_to = null;
	
	@Column(name = "parent_id")
	private String parentid = null;

	//private List<StudentVO> studentVOList=null;
	

	public String getStuid() {
		return stuid;
	}

	public void setStuid(String stuid) {
		this.stuid = stuid;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSphoto() {
		return sphoto;
	}

	public void setSphoto(String sphoto) {
		this.sphoto = sphoto;
	}

	public Date getSdob() {
		return sdob;
	}

	public void setSdob(Date sdob) {
		this.sdob = sdob;
	}

	public String getBplace() {
		return bplace;
	}

	public void setBplace(String bplace) {
		this.bplace = bplace;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getMtongue() {
		return mtongue;
	}

	public void setMtongue(String mtongue) {
		this.mtongue = mtongue;
	}

	public String getCgender() {
		return cgender;
	}

	public void setCgender(String cgender) {
		this.cgender = cgender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCemail() {
		return cemail;
	}

	public void setCemail(String cemail) {
		this.cemail = cemail;
	}

	public String getSch_transport() {
		return sch_transport;
	}

	public void setSch_transport(String sch_transport) {
		this.sch_transport = sch_transport;
	}

	public Date getPrd_from() {
		return prd_from;
	}

	public void setPrd_from(Date prd_from) {
		this.prd_from = prd_from;
	}

	public Date getPrd_to() {
		return prd_to;
	}

	public void setPrd_to(Date prd_to) {
		this.prd_to = prd_to;
	}

	public static String getType() {
		return TYPE;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	

	
	
	
	
	
}


