package com.common;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="user_login_details")
public class UserVO  implements Serializable, Cloneable
{
	public static final String TYPE = UserVO.class.getName();
	
	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "UserId")
	private String userId = null;
		
	@Column(name = "UserName")
	private String userName = null;
	
	@Column(name = "Password")
	private String password = null;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static String getType() {
		return TYPE;
	}
	
	
	
	
}
