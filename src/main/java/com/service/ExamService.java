package com.service;

import com.common.ExamTypeMasterVO;
import com.model.ExamNewVO;
import com.model.ExamStudentVO;
import com.model.ExamVO;
import com.model.MarksEntryVO;
import com.model.MarksTempVO;
import com.model.MarksdumpVO;
import com.model.MarksentryallVO;
import com.model.StuParVO;

public interface ExamService {

	
	public ExamNewVO insertExamDetails(ExamNewVO examVO) throws Exception;
	
	public ExamVO getLtrReferenceData()throws Exception;
	//getExamData
	public ExamNewVO getExamData()throws Exception;
	
	public ExamVO getLtrReferenceData100()throws Exception;
	
	public ExamVO getLtrReferenceDataall()throws Exception; 
	public ExamNewVO getExam(String sclass,String section,String stype)throws Exception;
	//getExamClass
	public ExamNewVO getExamClass(String sclass,String section)throws Exception;
	
	//getClassMarks
	public MarksentryallVO getClassMarks(String sclass,String section,String stype,String sid)throws Exception;
	//getClassSubject
	public MarksentryallVO getClassSubject(String sclass,String section)throws Exception;
	//getClassStudents
	public MarksentryallVO getClassStudents(String sclass,String section)throws Exception;
	//getClassExamid
	public MarksdumpVO getClassExamid(/*String eid,String sclass,String section*//*,String[] subj*/)throws Exception;
	
	//getMarksTemp
	public MarksTempVO getMarksTemp(String eid,String sclass,String[] myArray)throws Exception;
	
	//getMarksFinal
	public MarksdumpVO getMarksFinal()throws Exception;
	
	//getClassStudentsMarks
	public MarksentryallVO getClassStudentsMarks(String eid,String sid,String subj)throws Exception;
	
	
	//getClassMarksStudents
	public MarksEntryVO getClassMarksStudents(String sclass,String section,String eid,String sid)throws Exception;
	
	
	//getExamType
	public ExamTypeMasterVO getExamType()throws Exception;
	
	//getStudentMarks
	public MarksEntryVO getStudentMarks(String sid)throws Exception;
	//getExamID
	public ExamNewVO getExamID(String eid)throws Exception;
	//getExamID_Subject
	public ExamNewVO getExamID_Subject(String eid,String subj)throws Exception;
	
	//getExamDetails
	public ExamStudentVO getExamDetails(String sid)throws Exception;
}
