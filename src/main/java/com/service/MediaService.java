package com.service;

import com.model.MediaVO;
import com.model.MediaaddVO;

public interface MediaService {

	
	public MediaVO insertAlbumDetails(MediaVO mediaVO) throws Exception;
	public MediaaddVO insertMediaall(MediaaddVO mediaaddVO) throws Exception;
	public MediaaddVO getMediaData()throws Exception; 
	public MediaVO getAlbumData()throws Exception; 
}
