package com.service;

import java.util.List;

import com.common.MasVO;
import com.common.TeachVO;
import com.common.UserVO;
import com.model.CounterVO;
import com.model.CountryVO;
import com.model.LibrarianVO;
import com.model.LoginVO;
import com.model.MailSettingVO;
import com.model.MasterVO;
import com.model.ParentVO;
import com.model.SettingadminVO;
import com.model.StuVO;
import com.model.TeacherVO;
import com.model.TransportVO;


public interface UserService 
{
	
	public MasVO isUserNameExist(String userId,String password) throws Exception;
	//isCommonUserExist
	public LoginVO isCommonUserExist(String userId,String password) throws Exception;
	
	public TeacherVO isStaffNameExist(String userId,String password) throws Exception;
	//isTransportExist
	public TransportVO isTransportExist(String userId,String password) throws Exception;
	
	public StuVO isSTUidExist(String userId,String password) throws Exception;
	//isLIBidExist
	public LibrarianVO isLIBidExist(String userId,String password) throws Exception;
	public ParentVO isPARidExist(String userId,String password) throws Exception;
	public MasterVO insertMasterDetails(MasterVO masterVO) throws Exception;
	//updateAdminPhoto
	public MasterVO updateAdminPhoto(MasterVO masterVO) throws Exception;
	//photoUpdate
	public LoginVO photoUpdate(LoginVO loginVO) throws Exception;
	public LoginVO updateMobilePassword(LoginVO loginVO)throws Exception;
	//updateAdminGlobalSetting
	public LoginVO updateAdminGlobalSetting(LoginVO loginVO)throws Exception;
	//updateMailPassword
	public LoginVO updateMailPassword(LoginVO loginVO)throws Exception;
	//insertMailSetting
	public MailSettingVO insertMailSetting(MailSettingVO mailSettingVO)throws Exception;
	//getMailSetting
	public MailSettingVO getMailSetting()throws Exception;
	public MailSettingVO getMailSettingType(String temvar)throws Exception;
	
	//getIDSetting
	public CounterVO getIDSetting(String prifix)throws Exception;
	//getIDSettingAll
	public CounterVO getIDSettingAll()throws Exception;
	//updateIDSetting
	public CounterVO updateIDSetting(CounterVO counterVO)throws Exception;
	
	//insertTerms
	public MailSettingVO insertTerms(MailSettingVO mailSettingVO)throws Exception;
	//getTerms
	public MailSettingVO getTerms()throws Exception;
	//loadCountry
	public CountryVO loadCountry()throws Exception;
	
	//getLoginDetails
		public LoginVO getLoginDetails(String uid,String pass)throws Exception;
	//getLoginMobile
		public LoginVO getLoginMobile(String uid)throws Exception;
	//getLoginEmail
		public LoginVO getLoginEmail(String uid)throws Exception;
		
	//getAdminSettingDetails	
		public SettingadminVO getAdminSettingDetails(String uid)throws Exception;
		
	//updatePassword
	public MasterVO updatePassword(MasterVO masterVO) throws Exception;
	
	public MasterVO getMasterDetail(String uname)throws Exception;
	
}
