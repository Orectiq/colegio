package com.service;


import com.model.LibrarianVO;
import com.model.LibraryIssueVO;
import com.model.LibraryReqVO;
import com.model.LibraryVO;
import com.model.StuVO;

public interface LibraryService 
{
	public LibraryVO insertBooksDetails(LibraryVO libraryVO) throws Exception;
	//updateReq
	public LibraryIssueVO insertReq(LibraryIssueVO libraryissueVO) throws Exception;
	//updateReq
	public LibraryIssueVO updateReq(LibraryIssueVO libraryissueVO) throws Exception;
	//updateReturn
	public LibraryIssueVO updateReturn(LibraryIssueVO libraryissueVO) throws Exception;
	
	
	
	//updateReqMaster
	public LibraryVO updateReqMaster(LibraryVO libraryVO) throws Exception;
	//updateReturnMaster
	public LibraryVO updateReturnMaster(LibraryVO libraryVO) throws Exception;
	//updateRequestMasterCount
	public LibraryVO updateRequestMasterCount(LibraryVO libraryVO) throws Exception;
	
	public LibraryVO getBookId() throws Exception;
	public LibraryReqVO requestBooksDetails(LibraryReqVO libraryreqVO) throws Exception;
	public LibraryVO reqBookbyStudent(String sclass) throws Exception;
	//getLibDetails
	public LibrarianVO getLibDetails(String sclass) throws Exception;
	//getRequestbyDate
	public LibraryIssueVO getRequestbyDate(String rdate,String btitle) throws Exception;
	//getIssedAll
	public LibraryIssueVO getIssedAll(String title,String subj) throws Exception;
	//getBookTitle
	public LibraryIssueVO getBookTitle() throws Exception;
	
	//getRequestbyID
	public LibraryVO getRequestbyID(String id) throws Exception;
	//getRequestbyStudent
	public LibraryIssueVO getRequestbyStudent(String id) throws Exception;
	//getIssedBooks
	public LibraryIssueVO getIssedBooks(String sclass,String sec) throws Exception;
	//getDueDateBooks
	public LibraryIssueVO getDueDateBooks(String sclass,String sec) throws Exception;
	//getDueDateBooksAll
	public LibraryIssueVO getDueDateBooksAll() throws Exception;
	public LibraryIssueVO getDueDateBooksAllAdmin(String title,String subj) throws Exception;
	//showRequest
	public LibraryIssueVO showRequest() throws Exception;
	
	public LibraryVO getLtrReferenceDataall()throws Exception;
	//getBooktitle
	public LibraryVO getBooktitle(String cate)throws Exception;
	//getBookDetails
	public LibraryVO getBookDetails(String title,String cate)throws Exception;
	
	public LibraryReqVO getLtrRequestData()throws Exception;
	//getBookRequestDetails
	public LibraryIssueVO getBookRequestDetails(String sclass)throws Exception;
	//getBookRequestbyClass
	public LibraryIssueVO getBookRequestbyClass(String sclass,String btitle)throws Exception;
	//getBookReturnDetails
	public LibraryIssueVO getBookReturnDetails(String title,String cate)throws Exception;
	
	//countTotalBooks
	public int countTotalBooks()throws Exception;
	//countRequest
	public int countRequest()throws Exception;
	//countIssued
	public int countIssued()throws Exception;
	//countDueDate
	public int countDueDate(String btitle,String cate)throws Exception;
	
	public int countReturnBooks(String btitle,String cate)throws Exception;
	
}
