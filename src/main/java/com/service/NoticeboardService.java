package com.service;

import com.model.Nb_newsVO;
import com.model.NoticeboardVO;

public interface NoticeboardService {
	
	
	public NoticeboardVO insertEventDetails(NoticeboardVO noticeboardVO) throws Exception;
	public Nb_newsVO insertNewsDetails(Nb_newsVO nb_newsVO) throws Exception;
	public NoticeboardVO geteventdetail(String doe,String ltype)throws Exception;
	public NoticeboardVO getevent(String ltype)throws Exception;
	public Nb_newsVO getnewsdetail(String dop)throws Exception;
	
	//geteventdetailAll
	public NoticeboardVO geteventdetailAll()throws Exception;
	public Nb_newsVO getnewsdetailAll()throws Exception;
	
	//geteventdetailNext
	public NoticeboardVO geteventdetailNext()throws Exception;
	//geteventdetailType
	public NoticeboardVO geteventdetailType(String ltype,String doe)throws Exception;
	
	//getupcommingevents
		public NoticeboardVO getupcommingevents(String ltype)throws Exception;
	
	public Nb_newsVO getTeachernewsdetail(String ttype)throws Exception;
	public Nb_newsVO getTeachernewsdetaildate(String dop,String ttype)throws Exception;
	
	
	//geteventdetail_title
	public NoticeboardVO geteventdetail_title(String title)throws Exception; //(String studid)throws Exception;
	
	//geteventdetail_title
		public Nb_newsVO getnewsdetail_title(String title)throws Exception; //(String studid)throws Exception;
}
