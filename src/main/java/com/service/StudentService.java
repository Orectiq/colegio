package com.service;

import java.util.List;

import com.common.ExamTypeMasterVO;
import com.common.MasVO;
import com.common.StudentVO;
import com.model.AssignmentVO;
import com.model.AttendanceDayVO;
import com.model.AttendanceVO;
import com.model.CounterVO;
import com.model.DummyVO;
import com.model.EchallanVO;
import com.model.LibraryIssueVO;
import com.model.LoginVO;
import com.model.MarksVO;
import com.model.MedicalVO;
import com.model.Nb_newsVO;
import com.model.NoticeboardVO;
import com.model.ParentVO;
import com.model.StuMedicalVO;
import com.model.StuParVO;
import com.model.StuVO;
import com.model.TimeTableVO;


public interface StudentService
{
	public Nb_newsVO getNewsId() throws Exception;
	public NoticeboardVO getEventsId()throws Exception;
	public StuVO getStudentId() throws Exception;
	//getAssignId
	public StuVO getAssignId() throws Exception;
	//getReqId
	public LibraryIssueVO getReqId() throws Exception;
	public StuVO getParentId() throws Exception;
	public StuVO getAdmId() throws Exception;
	public StuVO getAdmId1() throws Exception;
	public StuVO getStudentRoll() throws Exception;
	//getChallanID
	public EchallanVO getChallanID() throws Exception;
	
	
	public StuVO insertStudentDetails(StuVO stuVO) throws Exception;
	public ParentVO insertParentDetails(ParentVO stuVO) throws Exception;
	public MedicalVO insertMedicalDetails(MedicalVO medicalVO) throws Exception;
	public StuVO insertFinalDetails(StuVO stuVO) throws Exception;
	//updateCounter
	public CounterVO updateCounter(CounterVO counerVO) throws Exception;
	//insertExamTypeMaster
	public ExamTypeMasterVO insertExamTypeMaster(ExamTypeMasterVO examtypeVO)throws Exception;
	
	//updateRoomAllot
	public StuVO updateRoomAllot(StuVO stuVO) throws Exception;
	//updateRollNumber
	public StuVO updateRollNumber(StuVO stuVO) throws Exception;
	
	
	public AttendanceVO insertAttendDetails(AttendanceVO attendVO) throws Exception;
	//insertMarks
	public MarksVO insertMarks(MarksVO marksVO) throws Exception;
	public AttendanceVO checkAttendance(AttendanceVO attendVO) throws Exception;
	
	
	public StuVO getLtrReferenceData()throws Exception; //(String studid)throws Exception;
	//getTransportList
	public StuVO getTransportList()throws Exception; //(String studid)throws Exception;
	
	public StuMedicalVO getLtrReferenceDataAll(String student_id)throws Exception;
	public MedicalVO getMedical(String studid)throws Exception;
	//getStudentLoginID
	public StuVO getStudentLoginID(String email)throws Exception;
	//getParentLoginID
	public ParentVO getParentLoginID(String mobile)throws Exception;
	
	//getParentid
	public StuVO getParentid(String mobile)throws Exception;
	
	public StuVO getParentDataAll(String parid)throws Exception;
	
	public StuParVO getStuParDataAll(String parent_id)throws Exception;
	//StuParListNew
	public ParentVO parentList(String parent_id)throws Exception;
	public StuVO getStudentClassAll(String sclass,String sec)throws Exception;
	
	
	//totalStudent
	public StuVO totalStudent(String sclass,String sec)throws Exception;
	
	//getStuAttendance
	public AttendanceVO getStuAttendance(String fdate,String tdate,String atime,String sid)throws Exception;
	//getStuAttendanceDayAll
	public AttendanceDayVO getStuAttendanceDayAll(String fdate,String monyear,String sclass,String sec)throws Exception;
	//getTeacherAttendanceDayAll
	public AttendanceDayVO getTeacherAttendanceDayAll(String fdate,String monyear)throws Exception;
	
	public AttendanceVO countAttendanceMonth(String sclass,String sec,String amonth)throws Exception;
	//showCount
	public DummyVO showCount()throws Exception;
	
	//getStuAttendanceAll
	public AttendanceVO getStuAttendanceAll(String fdate,String aclass,String asec)throws Exception;
	
	
	//getStuAttendanceByDate
	public AttendanceVO getStuAttendanceByDate(String fdate,String aclass,String asec)throws Exception;
	//getStuAttendanceByDateAbsent
	public AttendanceVO getStuAttendanceByDateAbsent(String fdate,String aclass,String asec,String ttype)throws Exception;
	//getStuAttendanceByIDAbsent
	public AttendanceVO getStuAttendanceByIDAbsent(String adate,String sid)throws Exception;
	
	//countAttendanceToday
	public int countAttendanceToday(String fdate,String aclass,String asec)throws Exception;
	public int countTotalStudentsToday(String fdate,String aclass,String asec)throws Exception;
	//topperClass
	public int topperClass(String fdate,String aclass,String asec)throws Exception;
	//topperClassName
	public MarksVO topperClassName(String fdate,String aclass,String asec)throws Exception;
	
	public StuParVO getParentClassAll(String sclass,String section)throws Exception;
	
	public StuVO getAttendance(String sclass,String section)throws Exception;
	
	//getLoginDetails
	public LoginVO getLoginDetails(String uid,String pass)throws Exception;
	
	
	//getHostelList
	public StuVO getHostelList(String gender)throws Exception;
	//getTransStudent
	public StuVO getTransStudent(String board)throws Exception;
	//getDorStudent
	public StuVO getDorStudent(String board)throws Exception;
	
	public StuVO getEditStudentsData(String studid)throws Exception;
	
	public StuVO deleteStudentsDetails(StuVO stuVO) throws Exception;
	public StuVO editStudentDetails(StuVO stuVO) throws Exception;
	
	public StuVO getAssignment(String sclass,String section)throws Exception;
	
	//getStuNameLike
	public StuVO getStuNameLike(String sclass,String section,String fname)throws Exception;
	//getPhoto
	public LoginVO getPhoto(String uid,String pass)throws Exception;
	
	//public List<StudentVO> getCategories();
	
	public AssignmentVO insertAssignmentDetails(AssignmentVO assignmentVO) throws Exception;
	
	
	//public List<StudentVO> getCategories();
	
	//msgCount
	public int msgCount(String mfrom)throws Exception;
	
	//countStudents
		public int countStudents()throws Exception;
	//countPresent
				public int countPresent()throws Exception;	
	//countAbsent
				public int countAbsent()throws Exception;	
				
	//count Total,Present,Absent list based on class,section and date			
		
		//countStudentsClass
				public int countStudentsClass(String sclass,String sec)throws Exception;
		//countPresentClass
				public int countPresentClass(String sclass,String sec,String adate)throws Exception;	
		//countAbsentClass
				public int countAbsentClass(String sclass,String sec,String amonth)throws Exception;
				
				public int countAttTeacherMonthPresent(String amonth)throws Exception;
				public int countAttTeacherMonthAbsent(String amonth)throws Exception;
				
				
		//countAttendanceMonth		
				//public int countAttendanceMonth(String sclass,String sec,String adate)throws Exception;
				
				public int countPresentMonth(String sclass,String sec,String amonth)throws Exception;
				
				
	//UserCount
				public int countParents()throws Exception;	
				public int countTeachers()throws Exception;
				public int countPresentTeacher()throws Exception;
				public int countAbsentTeacher()throws Exception;
				
				
				public int countLibrary()throws Exception;
				public int countDormitory()throws Exception;
				public int countTransport()throws Exception;
				public int countTransportAO()throws Exception;
				
				///getstudentid

				public StuVO  getstuid()throws Exception;	
				//getTimeTable
				public TimeTableVO  getTimeTable(String sclass,String sec)throws Exception;	
				//getTimeTableDay
				public TimeTableVO  getTimeTableDay(TimeTableVO timetableVO)throws Exception;
				//getTimePeriod
				public TimeTableVO  getTimePeriod()throws Exception;
				
				//TimeTableInsert
				public TimeTableVO TimeTableInsert(TimeTableVO timetableVO) throws Exception;
				public StuVO getstudentdata(String stuid)throws Exception;
	
}
