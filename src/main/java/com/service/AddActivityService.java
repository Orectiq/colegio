package com.service;

import java.util.List;


import com.model.AddActivityVO;

public interface AddActivityService {
	
	public AddActivityVO insertAddActivityDetails(AddActivityVO addactivityVO) throws Exception;

	public AddActivityVO getLtrReferenceData()throws Exception;
	//getClassActivity
	public AddActivityVO getClassActivity(String aclass,String asec,String ttype)throws Exception;
	//getActivitybyType
	public AddActivityVO getActivitybyType(String aclass,String asec,String place)throws Exception;
	public AddActivityVO getActivitybyType2(String aclass,String asec,String place)throws Exception;
	
	
	}

