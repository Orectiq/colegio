package com.service;

import com.model.MenuItemVO;
import com.model.SectionVO;
import com.model.SubjectVO;
import com.model.TeacherVO;

public interface SubjectService {

	
	public SubjectVO insertSubjectDetails(SubjectVO subjectVO) throws Exception;
	//insertClass
	public SubjectVO insertClass(String[] myArray) throws Exception;
	//deleteSubject
		public SubjectVO  deleteSubject(SubjectVO subjectVO)throws Exception;
	
	//getSClassMaster
		public SubjectVO  getSClassMaster()throws Exception;
		
	//insertSection
	public SectionVO insertSection(SectionVO subjectVO) throws Exception;

	public TeacherVO  getSubject(String sclass,String section,String ttype,String tid)throws Exception;
	//getTeacherSubject
	public SubjectVO  getTeacherSubject(String sclass,String section)throws Exception;
	
	
	//getSClass
	public SectionVO  getSClass()throws Exception;
	
	//getMenuData
	public MenuItemVO  getMenuData(String mtype)throws Exception;
	
	//getSection
	public SectionVO  getSection(String sclass)throws Exception;
}
