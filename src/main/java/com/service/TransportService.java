package com.service;
import java.util.List;

import com.model.TransportVO;
import com.model.StuVO;
import com.model.TransportAttendanceVO;



public interface TransportService {
	
	public TransportVO insertTransportDetails(TransportVO transportVO) throws Exception;
	//insertAttendance
	public TransportAttendanceVO insertAttendance(TransportAttendanceVO attendanceVO)throws Exception;
	public TransportVO gettransportdetail() throws Exception;
	
	public TransportVO gettransportdetailall(String tname) throws Exception;
	//getDname
	public TransportVO getDname(String dname) throws Exception;
	//getTransportLoginID
	public TransportVO getTransportLoginID(String dname) throws Exception;
	
	//getAttendanceHistory
	public TransportAttendanceVO getAttendanceHistory(String uname,String adate)throws Exception;
	//getAttendanceHistoryAbsent
	public TransportAttendanceVO getAttendanceHistoryAbsent(String uname,String adate)throws Exception;
	
	//getTransStudentVOList
	public TransportAttendanceVO getTransStudentVOList(String board,String adate)throws Exception;
	
	//getTransStuListBYBoard
	public TransportAttendanceVO getTransStuListBYBoard(String board,String adate)throws Exception;
	
	//getTransStudentVOListNew
	public TransportAttendanceVO getTransStudentVOListNew(String board)throws Exception;
	public TransportAttendanceVO gettrandetail(String duser) throws Exception;
}
