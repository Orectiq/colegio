package com.service;

import com.model.HomeworkStatusVO;
import com.model.HomeworkVO;


public interface HomeworkService {
	
	public HomeworkVO insertHomeworkDetails(HomeworkVO homeworkVO) throws Exception;
	//insertHomeworkStatus
	public HomeworkStatusVO insertHomeworkStatus(HomeworkStatusVO homeworkVO) throws Exception;
	
	public HomeworkVO getLtrReferenceData()throws Exception; 
	
	//getHomeworkdata
	public HomeworkVO getHomeworkdata(String sclass,String sec,String ttype,String tid,String dt)throws Exception;
	
	//sclass,sec,thedate
	public HomeworkVO getHomeworStudata(String sclass,String sec,String thedate)throws Exception;
	
	//getHomeworkStatus
	public HomeworkStatusVO getHomeworkStatus(String tid,String thedate,String sid)throws Exception;

	//getHomeWorkStuStatus
	public HomeworkStatusVO getHomeWorkStuStatus(String sclass,String sec,String mobile,String subj)throws Exception;

	
	}

