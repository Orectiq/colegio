package com.service;

import com.model.AbsenceRequestVO;
import com.model.AbsenceStuRequestVO;
import com.model.LibraryIssueVO;
import com.model.TeacherVO;

public interface AbsenceRequestService {
	
	public AbsenceRequestVO insertAbsenceRequestDetails(AbsenceRequestVO absenceRequestVO) throws Exception;
	//insertStuAbsenceRequest
	public AbsenceStuRequestVO insertStuAbsenceRequest(AbsenceStuRequestVO absenceRequestVO) throws Exception;
	
	public AbsenceRequestVO getLtrReferenceData()throws Exception; 
	//getTeacherLeaveRequestbyDate
	public AbsenceRequestVO getTeacherLeaveRequestbyDate(String rdate)throws Exception;
	//getTeacherLeaveRequestbyDateNotify
	public AbsenceRequestVO getTeacherLeaveRequestbyDateNotify(String rdate,String tid)throws Exception;
	public AbsenceRequestVO getTeacherLeaveRequestbyDateNotify1(String rdate,String tid,String tim)throws Exception;
	
	//getStudentLeaveRequestbyDate
	public AbsenceStuRequestVO getStudentLeaveRequestbyDate(String rdate)throws Exception;
	
	//getLeaveData
	public AbsenceRequestVO getLeaveData(String tid)throws Exception;
	//getLeaveGrantbyStudent
	public AbsenceStuRequestVO getLeaveGrantbyStudent(String sid)throws Exception;
	
	//updateLeaveReq
	public AbsenceRequestVO updateLeaveReq(AbsenceRequestVO absenceRequestVO) throws Exception;
	//updateStudentLeaveReq
	public AbsenceStuRequestVO updateStudentLeaveReq(AbsenceStuRequestVO absenceRequestVO) throws Exception;
	
	//updateTeacherMaster
	public TeacherVO updateTeacherMaster(TeacherVO teacherVO) throws Exception;
	
	}

