package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.AddActivityDao;

import com.model.AddActivityVO;
import com.service.AddActivityService;


@Service
public class AddActivityServiceImpl implements AddActivityService
{
	@Autowired
	//@Qualifier(value="AddActivityDao")
	private AddActivityDao addactivityDao=null;

	//@Override
	public AddActivityVO insertAddActivityDetails(AddActivityVO addactivityVO) throws Exception
	{
		System.out.println("Enter Service IMPL Activity");
		AddActivityVO AddActivityvo = new AddActivityVO();
		try{
			AddActivityvo = addactivityDao.insertAddActivityDetails(addactivityVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return AddActivityvo;

	}

	public AddActivityVO getLtrReferenceData() throws Exception //(String studid) 
	{
		 
		AddActivityVO Actlistvo = new AddActivityVO();
		try{
			Actlistvo.setAddActivityServiceVOList(addactivityDao.getLtrReferenceData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Actlistvo;
		 
	}
	
	//getClassActivity
	public AddActivityVO getClassActivity(String aclass,String asec,String ttype) throws Exception //(String studid) 
	{
		 
		AddActivityVO Actlistvo = new AddActivityVO();
		try{
			Actlistvo.setAddActivityServiceVOList(addactivityDao.getClassActivity(aclass,asec,ttype));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Actlistvo;
		 
	}
	//getActivitybyType
	public AddActivityVO getActivitybyType(String aclass,String asec,String place) throws Exception //(String studid) 
	{
		 
		AddActivityVO Actlistvo = new AddActivityVO();
		try{
			Actlistvo.setAddActivityServiceVOList(addactivityDao.getActivitybyType(aclass,asec,place));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Actlistvo;
		 
	}

	public AddActivityVO getActivitybyType2(String aclass,String asec,String place) throws Exception //(String studid) 
	{
		 
		AddActivityVO Actlistvo = new AddActivityVO();
		try{
			Actlistvo.setAddActivityServiceVOList(addactivityDao.getActivitybyType2(aclass,asec,place));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Actlistvo;
		 
	}



}
