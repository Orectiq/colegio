package com.service.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.dao.EchallanDao;
import com.model.EchallanStudentVO;
import com.model.EchallanVO;
import com.model.StuVO;
import com.service.EchallanService;

/**
 * @author admin
 *
 */
@Service
	public class EchallanServiceImpl implements EchallanService
	{
		@Autowired
		@Qualifier(value="echallanDao")
		private EchallanDao echallanDao=null;

		
		
		
		
		public EchallanVO getChallanId(String ref) throws Exception
		{
			System.out.println("enter getstuid ServiceImpl  "+ref);
			EchallanVO challanVO = new EchallanVO();
			challanVO.setChallan_id(this.getChallanIdref(ref));
			
			return challanVO;
		}

		public String getChallanIdref(String ref) throws Exception {
			List<Object> result = echallanDao.getChallanId(ref);
			String rfnNumber = "";
			String gNumber = "";
			for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
				Object[] obj = (Object[]) itr.next();
				String prefix = (String) obj[0];
				String rfnNo = String.valueOf((Integer) obj[1]);
				if (rfnNo.length() == 1) {
					rfnNumber = "0000";
					gNumber = prefix.concat(rfnNumber.concat(rfnNo));
				} else if (rfnNo.length() == 2) {
					rfnNumber = "000";
					gNumber = prefix.concat(rfnNumber.concat(rfnNo));
				} else if (rfnNo.length() == 3) {
					rfnNumber = "00";
					gNumber = prefix.concat(rfnNumber.concat(rfnNo));
				} else if (rfnNo.length() == 5) {
					rfnNumber = "0";
					gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
				} else if (rfnNo.length() == 4) {
					gNumber = prefix.concat(rfnNo);
				}
			}
			return gNumber;
		}
		
		
		
		//@Override
		public EchallanVO insertEchallanDetails(EchallanVO echallanVO) throws Exception
		{
			System.out.println("challan serviceimpl insert ");
			
			EchallanVO Echallanvo = new EchallanVO();
			try{
				Echallanvo = echallanDao.insertEchallanDetails(echallanVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Echallanvo;

		}
		
		public EchallanVO sendEchallanDetails(EchallanVO echallanVO) throws Exception
		{
			System.out.println("challan serviceimpl update ");
			
			EchallanVO Echallanvo = new EchallanVO();
			try{
				Echallanvo = echallanDao.sendEchallanDetails(echallanVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Echallanvo;

		}

		public EchallanVO getFeeDraft(String sclass) throws Exception {
			//return studentDao.getStudentClassAll(sclass,sec);
			System.out.println(" value  "+sclass);
			EchallanVO Challanvo = new EchallanVO();
			try{
				Challanvo.setEchallanServiceVOList(echallanDao.getFeeDraft(sclass));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Challanvo;
		}
		public EchallanVO getFee(String sclass) throws Exception {
			//return studentDao.getStudentClassAll(sclass,sec);
			System.out.println(" value   --->  "+sclass);
			EchallanVO Challanvo = new EchallanVO();
			try{
				Challanvo.setEchallanServiceVOList(echallanDao.getFee(sclass));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Challanvo;
		}
		
		//getStudentFee
		public EchallanStudentVO getStudentFee(String sclass) throws Exception {
			//return studentDao.getStudentClassAll(sclass,sec);
			System.out.println(" value  "+sclass);
			EchallanStudentVO Challanstudentvo = new EchallanStudentVO();
			try{
				Challanstudentvo.setEchallanServiceVOList(echallanDao.getStudentFee(sclass));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Challanstudentvo;
		}
		
		//getChallanFeeID
		public EchallanVO getChallanFeeID(String sclass) throws Exception {
			//return studentDao.getStudentClassAll(sclass,sec);
			System.out.println(" value  "+sclass);
			EchallanVO Challanstudentvo = new EchallanVO();
			try{
				Challanstudentvo.setEchallanServiceVOList(echallanDao.getChallanFeeID(sclass));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Challanstudentvo;
		}
		
		//getChallanFeeID
				public EchallanVO getChallanDraftID(String sclass) throws Exception {
					//return studentDao.getStudentClassAll(sclass,sec);
					System.out.println(" value  "+sclass);
					EchallanVO Challanstudentvo = new EchallanVO();
					try{
						Challanstudentvo.setEchallanServiceVOList(echallanDao.getChallanDraftID(sclass));
						
					}catch(Exception e) {
						System.out.println("Error  "+e);
					} 
					return Challanstudentvo;
				}
		
		
		

	}
