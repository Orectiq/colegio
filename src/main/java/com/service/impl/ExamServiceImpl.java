package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.common.ExamTypeMasterVO;
import com.dao.ExamDao;
import com.model.ExamNewVO;
import com.model.ExamStudentVO;
import com.model.ExamVO;
import com.model.MarksEntryVO;
import com.model.MarksTempVO;
import com.model.MarksdumpVO;
import com.model.MarksentryallVO;
import com.model.StuParVO;
import com.service.ExamService;


public class ExamServiceImpl implements ExamService {

	@Autowired
	@Qualifier(value="examDao")
	private ExamDao examDao=null;

	

	//@Override
	public ExamNewVO insertExamDetails(ExamNewVO examVO) throws Exception
	{
		System.out.println("Exam Service IMPL ");
		ExamNewVO Examvo = new ExamNewVO();
		try{
			Examvo = examDao.insertExamDetails(examVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Examvo;
	}


	
	public ExamVO getLtrReferenceData() throws Exception //(String studid) 
	{
		 
		ExamVO Examvo = new ExamVO();
		try{
			Examvo.setExamServiceVOList(examDao.getLtrReferenceData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Examvo;
	}
	//getExamData
	public ExamNewVO getExamData() throws Exception //(String studid) 
	{
		 
		ExamNewVO Examvo = new ExamNewVO();
		try{
			Examvo.setExamNewServiceVOList(examDao.getExamData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Examvo;
	}
	
	
	public ExamVO getLtrReferenceData100() throws Exception //(String studid) 
	{
		 
		ExamVO Examvo = new ExamVO();
		try{
			//Examvo.setExamServiceVOList(examDao.getLtrReferenceData100());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return examDao.getLtrReferenceData100();
	}
	

	public ExamVO getLtrReferenceDataall() throws Exception //(String studid) 
	{
		 
		ExamVO Examvo = new ExamVO();
		try{
			Examvo.setExamServiceVOList(examDao.getLtrReferenceDataall());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Examvo;
		 
	}



	public ExamNewVO getExam(String sclass, String section, String stype) throws Exception {
		System.out.println("getExam Servie IMPL  "+sclass+","+section+"etype  "+stype);
		ExamNewVO Examvo = new ExamNewVO();
		try{
			Examvo.setExamNewServiceVOList(examDao.getExam(sclass,section,stype));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Examvo;
	}
	//getExamClass
	public ExamNewVO getExamClass(String sclass, String section) throws Exception {
		System.out.println("getExam Servie IMPL  "+sclass+","+section);
		ExamNewVO Examvo = new ExamNewVO();
		try{
			Examvo.setExamNewServiceVOList(examDao.getExamClass(sclass,section));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Examvo;
	}
	
	
	public MarksentryallVO getClassMarks(String sclass, String section, String stype,String eid) throws Exception {
		System.out.println("getExam Servie IMPL  "+sclass+","+section+"etype  "+stype);
		MarksentryallVO Marksvo = new MarksentryallVO();
		try{
			Marksvo.setMarksAllServiceVOList(examDao.getClassMarks(sclass,section,stype,eid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Marksvo;
	}
	
	//MarksentryallVO getClassSubject
	public MarksentryallVO getClassSubject(String sclass, String section) throws Exception {
		System.out.println("getExam Servie IMPL  "+sclass+","+section);
		MarksentryallVO Marksvo = new MarksentryallVO();
		try{
			Marksvo.setMarksAllServiceVOList(examDao.getClassSubject(sclass,section));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Marksvo;
	}
	
	//getClassStudents
	public MarksentryallVO getClassStudents(String sclass, String section) throws Exception {
		System.out.println("getExam Servie IMPL  "+sclass+","+section);
		MarksentryallVO Marksvo = new MarksentryallVO();
		try{
			Marksvo.setMarksAllServiceVOList(examDao.getClassStudents(sclass,section));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Marksvo;
	}
	//getClassExamid
	public MarksdumpVO getClassExamid(/*String eid,String sclass, String section*//*,String[] subj*/) throws Exception {
		//System.out.println("getExam Servie IMPL  "+sclass+","+section);
		MarksdumpVO Marksvo = new MarksdumpVO();
		try{
			Marksvo.setMarksDumpServiceVOList(examDao.getClassExamid(/*eid,sclass,section*/));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Marksvo;
	}
	
	//getMarksTemp
	public MarksTempVO getMarksTemp(String eid,String sclass,String[] myArray) throws Exception {
		//System.out.println("getExam Servie IMPL  "+sclass+","+section);
		MarksTempVO MarksTempvo = new MarksTempVO();
		try{
			MarksTempvo.setMarksTempServiceVOList(examDao.getMarksTemp(eid,sclass,myArray));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return MarksTempvo;
	}
	
	
	//getMarksFinal
	public MarksdumpVO getMarksFinal() throws Exception {
		System.out.println("getExam Servie IMPL  ");
		MarksdumpVO Marksvo = new MarksdumpVO();
		try{
			Marksvo.setMarksDumpServiceVOList(examDao.getMarksFinal());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Marksvo;
	}
	
	
	//getClassStudentsMarks
	public MarksentryallVO getClassStudentsMarks(String eid, String sid,String subj) throws Exception {
		System.out.println("getExam Servie IMPL  "+eid+"  sid  "+sid+"  subject  "+subj);
		MarksentryallVO Marksvo = new MarksentryallVO();
		try{
			Marksvo.setMarksAllServiceVOList(examDao.getClassStudentsMarks(eid,sid,subj));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Marksvo;
	}
	
	
	
	//getClassMarksStudents
	public MarksEntryVO getClassMarksStudents(String sclass, String section, String eid,String sid) throws Exception {
		System.out.println("getExam Servie IMPL  "+sclass+","+section);
		MarksEntryVO Marksvo = new MarksEntryVO();
		try{
			Marksvo.setMarksServiceVOList(examDao.getClassMarksStudents(sclass,section,eid,sid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Marksvo;
	}
	
	//ExamTypeMasterVO getExamType
	public ExamTypeMasterVO getExamType() throws Exception {
		
		ExamTypeMasterVO ExamTypevo = new ExamTypeMasterVO();
		try{
			ExamTypevo.setExamtypeServiceVOList(examDao.getExamType());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return ExamTypevo;
	}
	
	//getStudentMarks
	public MarksEntryVO getStudentMarks(String sid) throws Exception {
		System.out.println("getExam Servie IMPL  "+sid);
		MarksEntryVO Marksvo = new MarksEntryVO();
		try{
			Marksvo.setMarksServiceVOList(examDao.getStudentMarks(sid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Marksvo;
	}
	//getExamID
	public ExamNewVO getExamID(String eid) throws Exception {
		System.out.println("getExam Servie IMPL  "+eid);
		ExamNewVO examvo = new ExamNewVO();
		try{
			examvo.setExamNewServiceVOList(examDao.getExamID(eid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return examvo;
	}
	//getExamID_Subject
	public ExamNewVO getExamID_Subject(String eid,String subj) throws Exception {
		System.out.println("getExam Servie IMPL  "+eid);
		ExamNewVO examvo = new ExamNewVO();
		try{
			examvo.setExamNewServiceVOList(examDao.getExamID_Subject(eid,subj));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return examvo;
	}
	
	//getExamDetails
	public ExamStudentVO getExamDetails(String sid) throws Exception {
		System.out.println("getExam Servie IMPL  "+sid);
		ExamStudentVO Examstudentvo = new ExamStudentVO();
		try{
			Examstudentvo.setExamServiceVOList(examDao.getExamDetails(sid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Examstudentvo;
	}
	
	
	
	
}
