package com.service.impl;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.NoticeboardDao;
import com.model.Nb_newsVO;
import com.model.NoticeboardVO;
import com.service.NoticeboardService;



@Service
public class NoticeboardServiceImpl implements NoticeboardService
{

	@Autowired
	//@Qualifier(value="noticeboardDao")
	private NoticeboardDao noticeboardDao=null;

	public NoticeboardVO insertEventDetails(NoticeboardVO noticeboardVO) throws Exception
	{
		System.out.println("Enter Noticeboard Service Impl ----------------------------->");
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
		try{
			Noticeboardvo = noticeboardDao.insertEventDetails(noticeboardVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Noticeboardvo;
	}
	
	
	
	

	public Nb_newsVO insertNewsDetails(Nb_newsVO nb_newsVO) throws Exception{

		Nb_newsVO nb_newsvo = new Nb_newsVO();
		try{
			nb_newsvo = noticeboardDao.insertNewsDetails(nb_newsVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return nb_newsvo;

		
		
	}

	
	public NoticeboardVO geteventdetail(String doe,String ltype) throws Exception
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
		try{
			Noticeboardvo.setNoticeboardServiceVOList(noticeboardDao.geteventdetail(doe,ltype));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Noticeboardvo;
		 
		
	}
	public NoticeboardVO getevent(String ltype) throws Exception
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
		try{
			Noticeboardvo.setNoticeboardServiceVOList(noticeboardDao.getevent(ltype));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Noticeboardvo;
		 
		
	}


	public Nb_newsVO getnewsdetail(String dop) throws Exception
	{
		Nb_newsVO Nb_newsvo = new Nb_newsVO();
		try{
			Nb_newsvo.setNewsboardServiceVOList(noticeboardDao.getnewsdetail(dop));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nb_newsvo;
	}
	
	
	public NoticeboardVO geteventdetailAll() throws Exception
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
		try{
			Noticeboardvo.setNoticeboardServiceVOList(noticeboardDao.geteventdetailAll());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Noticeboardvo;
		 
		
	}


	public Nb_newsVO getnewsdetailAll() throws Exception
	{
		Nb_newsVO Nb_newsvo = new Nb_newsVO();
		try{
			Nb_newsvo.setNewsboardServiceVOList(noticeboardDao.getnewsdetailAll());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nb_newsvo;
	}
	//geteventdetailNext
	public NoticeboardVO geteventdetailNext() throws Exception
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
		try{
			Noticeboardvo.setNoticeboardServiceVOList(noticeboardDao.geteventdetailNext());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Noticeboardvo;
	}
	//geteventdetailType
	public NoticeboardVO geteventdetailType(String ltype,String doe) throws Exception
	{
		System.out.println("serviceimple Type  "+ltype);
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
		try{
			Noticeboardvo.setNoticeboardServiceVOList(noticeboardDao.geteventdetailType(ltype,doe));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Noticeboardvo;
	}
	
	//getupcommingevents
		public NoticeboardVO getupcommingevents(String ltype) throws Exception
		{
			System.out.println("serviceimple Type  "+ltype);
			NoticeboardVO Noticeboardvo = new NoticeboardVO();
			try{
				Noticeboardvo.setNoticeboardServiceVOList(noticeboardDao.getupcommingevents(ltype));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Noticeboardvo;
		}
		
	//getTeachernewsdetail
	public Nb_newsVO getTeachernewsdetail(String ttype) throws Exception
	{
		System.out.println("service  type  "+ttype);
		Nb_newsVO Nb_newsvo = new Nb_newsVO();
		try{
			Nb_newsvo.setNewsboardServiceVOList(noticeboardDao.getTeachernewsdetail(ttype));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nb_newsvo;
	}
	
	//getTeachernewsdetail
		public Nb_newsVO getTeachernewsdetaildate(String dop,String ttype) throws Exception
		{
			System.out.println("service  type  "+ttype);
			Nb_newsVO Nb_newsvo = new Nb_newsVO();
			try{
				Nb_newsvo.setNewsboardServiceVOList(noticeboardDao.getTeachernewsdetaildate(dop,ttype));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Nb_newsvo;
		}

	//geteventdetail_title
	public NoticeboardVO geteventdetail_title(String title) throws Exception
	{
		NoticeboardVO Noticeboardvo = new NoticeboardVO();
		try{
			Noticeboardvo.setNoticeboardServiceVOList(noticeboardDao.geteventdetail_title(title));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Noticeboardvo;
	}
	
	//getnewsdetail_title
	public Nb_newsVO getnewsdetail_title(String title) throws Exception
	{
		Nb_newsVO Nb_newsvo = new Nb_newsVO();
		try{
			Nb_newsvo.setNewsboardServiceVOList(noticeboardDao.getnewsdetail_title(title));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nb_newsvo;
	}





	
}


