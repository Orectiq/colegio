package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.DormitoryDao;
import com.model.DormitoryAllotVO;
import com.model.DormitoryVO;
import com.model.StuVO;
import com.service.DormitoryService;

@Service
public class DormitoryServiceImpl implements DormitoryService
{
	@Autowired
	@Qualifier(value="dormitoryDao")
	private DormitoryDao dormitoryDao=null;

	//@Override
	public DormitoryVO insertDormitoryDetails(DormitoryVO dormitoryVO) throws Exception
	{
		
		System.out.println("Service IMPL dormitory");
		System.out.println(dormitoryVO.getDormitory_name());
		System.out.println(dormitoryVO.getLocation());
		System.out.println(dormitoryVO.getFee());
		System.out.println(dormitoryVO.getCgender());
		System.out.println("Service IMPL dormitory end");
		
		DormitoryVO Dormitoryvo = new DormitoryVO();
		try{
			Dormitoryvo = dormitoryDao.insertDormitoryDetails(dormitoryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Dormitoryvo;

	}
	//updateRoomAllot
	public DormitoryVO updateRoomAllot(DormitoryVO dormitoryVO) throws Exception
	{
		
		System.out.println("Service IMPL dormitory");
		System.out.println(dormitoryVO.getDormitory_name());
		System.out.println(dormitoryVO.getLocation());
		System.out.println(dormitoryVO.getFee());
		System.out.println(dormitoryVO.getCgender());
		System.out.println("Service IMPL dormitory end");
		
		DormitoryVO Dormitoryvo = new DormitoryVO();
		try{
			Dormitoryvo = dormitoryDao.updateRoomAllot(dormitoryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Dormitoryvo;

	}
	
	
	
	//insertRoomAllot
	public DormitoryAllotVO insertRoomAllot(DormitoryAllotVO dormitoryVO) throws Exception
	{
		
		System.out.println("Service IMPL dormitory");
		System.out.println(dormitoryVO.getDormitory_name());
		System.out.println(dormitoryVO.getLocation());
		
		System.out.println("Service IMPL dormitory end");
		
		DormitoryAllotVO Dormitoryvo = new DormitoryAllotVO();
		try{
			Dormitoryvo = dormitoryDao.insertRoomAllot(dormitoryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Dormitoryvo;

	}
	
	public DormitoryVO getLtrReferenceData() throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		DormitoryVO Dormitoryvo = new DormitoryVO();
		try{
			Dormitoryvo.setDormitoryServiceVOList(dormitoryDao.getLtrReferenceData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Dormitoryvo;
		 
	}
	//getHostelList
		public DormitoryVO getHostelList(String gender) throws Exception 
		{
			System.out.println("Enter service impl  Parent sclass  "+gender);
			DormitoryVO Dormitoryvo = new DormitoryVO();
			try{
				Dormitoryvo.setDormitoryServiceVOList(dormitoryDao.getHostelList(gender));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Dormitoryvo;
		}
	//getNonDormitoryStudent	
		public StuVO getNonDormitoryStudent() throws Exception 
		{
			
			StuVO Stuvo = new StuVO();
			try{
				Stuvo.setStudentServiceVOList(dormitoryDao.getNonDormitoryStudent());
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Stuvo;
		}
	//getDormitoryList
		public DormitoryVO getDormitoryList(String dname) throws Exception 
		{
			System.out.println("Enter service impl  Parent sclass  "+dname);
			DormitoryVO Dormitoryvo = new DormitoryVO();
			try{
				Dormitoryvo.setDormitoryServiceVOList(dormitoryDao.getDormitoryList(dname));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Dormitoryvo;
		}	
	//getDormitoryAllotList	
		public DormitoryAllotVO getDormitoryAllotList(String dname) throws Exception 
		{
			System.out.println("Enter service impl  Parent sclass  "+dname);
			DormitoryAllotVO Dormitoryvo = new DormitoryAllotVO();
			try{
				Dormitoryvo.setDormitoryAllotServiceVOList(dormitoryDao.getDormitoryAllotList(dname));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Dormitoryvo;
		}	
		
	//editHostel
		public StuVO editHostel(StuVO stuVO) throws Exception 
		{
			StuVO Stuvo = new StuVO();
			try{
				Stuvo = dormitoryDao.editHostel(stuVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Stuvo;
			
		}	
	

}
