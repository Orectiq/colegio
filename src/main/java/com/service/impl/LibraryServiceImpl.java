package com.service.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.LibraryDao;
import com.model.HomeworkVO;
import com.model.LibrarianVO;
import com.model.LibraryIssueVO;
import com.model.LibraryReqVO;
import com.model.LibraryVO;
import com.model.StuVO;
import com.service.LibraryService;

@Service
public class LibraryServiceImpl implements LibraryService
{
	@Autowired
	@Qualifier(value="libraryDao")
	private LibraryDao libraryDao=null;

	
	//getBookId
	public LibraryVO getBookId() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  ");
		LibraryVO libVO = new LibraryVO();
		libVO.setBook_ID(this.getBookIdRef());
		return libVO;
	}
	public String getBookIdRef() throws Exception {
		List<Object> result = libraryDao.getBook_ID();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			/*if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}*/
			gNumber = prefix.concat(rfnNo);
		}
		return gNumber;
	}
	
	//@Override
	public LibraryVO insertBooksDetails(LibraryVO libraryVO) throws Exception
	{
		System.out.println("ServiceIMPL Library");
		System.out.println(libraryVO.getBook_ID());
		System.out.println(libraryVO.getBook_Title());
		System.out.println(libraryVO.getAuthor());
		System.out.println(libraryVO.getDescription());
		System.out.println("controller ServiceIMPL end");
		
		LibraryVO Libraryvo = new LibraryVO();
		try{
			Libraryvo = libraryDao.insertBooksDetails(libraryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Libraryvo;
	}
	
	public LibraryIssueVO insertReq(LibraryIssueVO libraryVO) throws Exception
	{
		System.out.println("ServiceIMPL Library");
		System.out.println(libraryVO.getBook_id());
		System.out.println("controller ServiceIMPL end");
		LibraryIssueVO Libraryvo = new LibraryIssueVO();
		try{
			Libraryvo = libraryDao.insertReq(libraryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Libraryvo;
	}
	
	//updateReq
	public LibraryIssueVO updateReq(LibraryIssueVO libraryVO) throws Exception
	{
		System.out.println("ServiceIMPL Library");
		System.out.println(libraryVO.getBook_id());
		System.out.println("controller ServiceIMPL end");
		LibraryIssueVO Libraryvo = new LibraryIssueVO();
		try{
			Libraryvo = libraryDao.updateReq(libraryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Libraryvo;
	}
	//updateReturn
	public LibraryIssueVO updateReturn(LibraryIssueVO libraryVO) throws Exception
	{
		System.out.println("ServiceIMPL Library");
		System.out.println(libraryVO.getBook_id());
		System.out.println("controller ServiceIMPL end");
		LibraryIssueVO Libraryvo = new LibraryIssueVO();
		try{
			Libraryvo = libraryDao.updateReturn(libraryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Libraryvo;
	}
	
	//updateReqMaster
	public LibraryVO updateReqMaster(LibraryVO libraryVO) throws Exception
	{
		System.out.println("ServiceIMPL Library");
		System.out.println(libraryVO.getBook_ID());
		System.out.println("controller ServiceIMPL end");
		LibraryVO Libraryvo = new LibraryVO();
		try{
			Libraryvo = libraryDao.updateReqMaster(libraryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Libraryvo;
	}
	//updateReturnMaster
	public LibraryVO updateReturnMaster(LibraryVO libraryVO) throws Exception
	{
		System.out.println("ServiceIMPL Library");
		System.out.println(libraryVO.getBook_ID());
		System.out.println("controller ServiceIMPL end");
		LibraryVO Libraryvo = new LibraryVO();
		try{
			Libraryvo = libraryDao.updateReturnMaster(libraryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Libraryvo;
	}
	//updateRequestMasterCount
	public LibraryVO updateRequestMasterCount(LibraryVO libraryVO) throws Exception
	{
		System.out.println("ServiceIMPL Library");
		System.out.println(libraryVO.getBook_ID());
		System.out.println("controller ServiceIMPL end");
		LibraryVO Libraryvo = new LibraryVO();
		try{
			Libraryvo = libraryDao.updateRequestMasterCount(libraryVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Libraryvo;
	}
	
	public LibraryReqVO requestBooksDetails(LibraryReqVO libraryreqVO) throws Exception
	{  
		
		
		LibraryReqVO LibraryReqvo = new LibraryReqVO();
		try{
			LibraryReqvo = libraryDao.requestBooksDetails(libraryreqVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return LibraryReqvo;

	}
	//reqBookbyStudent
	public LibraryVO reqBookbyStudent(String sclass) throws Exception
	{  
		LibraryVO libvo = new LibraryVO();
		try{
			libvo.setLibraryServiceVOList(libraryDao.reqBookbyStudent(sclass));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;

	}
	//getLibDetails
	public LibrarianVO getLibDetails(String sclass) throws Exception
	{  
		System.out.println("Librarian  service IMP   "+sclass);
		LibrarianVO libvo = new LibrarianVO();
		try{
			libvo.setLibrarianServiceVOList(libraryDao.getLibDetails(sclass));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;

	}
	
	//getRequestbyDate
	public LibraryIssueVO getRequestbyDate(String rdate,String btitle) throws Exception
	{  
		System.out.println("Librarian  service IMP   "+rdate);
		LibraryIssueVO libvo = new LibraryIssueVO();
		try{
			libvo.setLibraryIssueServiceVOList(libraryDao.getRequestbyDate(rdate,btitle));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;
	}
	//getIssedAll
	public LibraryIssueVO getIssedAll(String title,String subj) throws Exception
	{  
		//System.out.println("Librarian  service IMP   "+rdate);
		LibraryIssueVO libvo = new LibraryIssueVO();
		try{
			libvo.setLibraryIssueServiceVOList(libraryDao.getIssedAll(title,subj));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;
	}
	//getBookTitle
	public LibraryIssueVO getBookTitle() throws Exception
	{  
		//System.out.println("Librarian  service IMP   "+rdate);
		LibraryIssueVO libvo = new LibraryIssueVO();
		try{
			libvo.setLibraryIssueServiceVOList(libraryDao.getBookTitle());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;
	}
	
	//getRequestbyID
	public LibraryVO getRequestbyID(String id) throws Exception
	{  
		System.out.println("Librarian  service IMP   "+id);
		LibraryVO libvo = new LibraryVO();
		try{
			libvo.setLibraryServiceVOList(libraryDao.getRequestbyID(id));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;
	}
	//getRequestbyStudent
	public LibraryIssueVO getRequestbyStudent(String id) throws Exception
	{  
		System.out.println("Librarian  service IMP   "+id);
		LibraryIssueVO libvo = new LibraryIssueVO();
		try{
			libvo.setLibraryIssueServiceVOList(libraryDao.getRequestbyStudent(id));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;
	}
	//getIssedBooks
	public LibraryIssueVO getIssedBooks(String sclass,String sec) throws Exception
	{  
		System.out.println("Librarian  service IMP   "+sclass);
		LibraryIssueVO libvo = new LibraryIssueVO();
		try{
			libvo.setLibraryIssueServiceVOList(libraryDao.getIssedBooks(sclass,sec));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;
	}
	//getDueDateBooks
	public LibraryIssueVO getDueDateBooks(String sclass,String sec) throws Exception
	{  
		System.out.println("Librarian  service IMP   "+sclass);
		LibraryIssueVO libvo = new LibraryIssueVO();
		try{
			libvo.setLibraryIssueServiceVOList(libraryDao.getDueDateBooks(sclass,sec));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;
	}
	//getDueDateBooksAll
		public LibraryIssueVO getDueDateBooksAll() throws Exception
		{  
			System.out.println("Librarian  service IMP   ");
			LibraryIssueVO libvo = new LibraryIssueVO();
			try{
				libvo.setLibraryIssueServiceVOList(libraryDao.getDueDateBooksAll());
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return libvo;
		}
	//getDueDateBooksAllAdmin
		public LibraryIssueVO getDueDateBooksAllAdmin(String title,String subj) throws Exception
		{  
			System.out.println("Librarian  service IMP   ");
			LibraryIssueVO libvo = new LibraryIssueVO();
			try{
				libvo.setLibraryIssueServiceVOList(libraryDao.getDueDateBooksAllAdmin(title,subj));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return libvo;
		}
		
		
	//showRequest
	public LibraryIssueVO showRequest() throws Exception
	{  
		
		LibraryIssueVO libvo = new LibraryIssueVO();
		try{
			libvo.setLibraryIssueServiceVOList(libraryDao.showRequest());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return libvo;
	}
	
	public LibraryVO getLtrReferenceDataall() throws Exception //(String studid) 
	{
		 
		LibraryVO Libraryvo = new LibraryVO();
		try{
			Libraryvo.setLibraryServiceVOList(libraryDao.getLtrReferenceDataall());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Libraryvo;
		 
	}
	public LibraryVO getBooktitle(String cate) throws Exception //(String studid) 
	{
		 
		LibraryVO Libraryvo = new LibraryVO();
		try{
			Libraryvo.setLibraryServiceVOList(libraryDao.getBooktitle(cate));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Libraryvo;
		 
	}
	//getBookDetails
	public LibraryVO getBookDetails(String title,String cate) throws Exception //(String studid) 
	{
		System.out.println("service impl ");
		 
		LibraryVO Libraryvo = new LibraryVO();
		try{
			Libraryvo.setLibraryServiceVOList(libraryDao.getBookDetails(title,cate));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Libraryvo;
		 
	}
	
	
	//@Override
	public LibraryReqVO getLtrRequestData() throws Exception {
		LibraryReqVO LibraryReqvo = new LibraryReqVO();
		try{
			LibraryReqvo.setLibraryDetailList(libraryDao.getLtrRequestData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return LibraryReqvo;

	}
	
	//getBookRequestDetails
	public LibraryIssueVO getBookRequestDetails(String sclass) throws Exception {
		System.out.println("sclass service impl  "+sclass);
		LibraryIssueVO LibraryReqvo = new LibraryIssueVO();
		try{
			LibraryReqvo.setLibraryIssueServiceVOList(libraryDao.getBookRequestDetails(sclass));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return LibraryReqvo;

	}
	//getBookRequestbyClass
	public LibraryIssueVO getBookRequestbyClass(String sclass,String btitle) throws Exception {
		System.out.println("sclass service impl  "+sclass);
		LibraryIssueVO LibraryReqvo = new LibraryIssueVO();
		try{
			LibraryReqvo.setLibraryIssueServiceVOList(libraryDao.getBookRequestbyClass(sclass,btitle));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return LibraryReqvo;

	}
	
	
	//getBookReturnDetails
	public LibraryIssueVO getBookReturnDetails(String title,String cate) throws Exception {
		LibraryIssueVO LibraryReqvo = new LibraryIssueVO();
		try{
			LibraryReqvo.setLibraryIssueServiceVOList(libraryDao.getBookReturnDetails(title,cate));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return LibraryReqvo;

	}
	
	//countTotalBooks
	public int countTotalBooks() throws Exception 
	{
		return libraryDao.countTotalBooks();
	}
	
	//countRequest
	public int countRequest() throws Exception 
	{
		return libraryDao.countRequest();
	}
	//countIssued
	public int countIssued() throws Exception 
	{
		return libraryDao.countIssued();
	}
	
	//countDueDate
	public int countDueDate(String btitle,String cate) throws Exception 
	{
		return libraryDao.countDueDate(btitle,cate);
	}
	public int countReturnBooks(String btitle,String cate)throws Exception
	{
		return libraryDao.countReturnBooks(btitle,cate);
	}
	
}
