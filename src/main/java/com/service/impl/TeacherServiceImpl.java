package com.service.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.dao.StudentDao;
import com.dao.TeacherDao;
import com.model.AbsenceRequestVO;
import com.model.ClassSectionVO;
import com.model.GroupVO;
import com.model.LoginVO;
import com.model.MailBackupVO;
import com.model.MailVO;
import com.model.ParentVO;
import com.model.StuVO;
import com.model.SubjectAssignVO;
import com.model.TeacherVO;
import com.model.staffAttendanceVO;
import com.model.teacherAttendanceVO;
import com.service.TeacherService;

public class TeacherServiceImpl implements TeacherService
{
	@Autowired
	@Qualifier(value="teacherDao")
	private TeacherDao teacherDao=null;
	
	
	public TeacherVO getTeachId(String ref) throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  "+ref);
		TeacherVO teacherVO = new TeacherVO();
		teacherVO.setTid(this.getTeacId(ref));
		return teacherVO;
	}
	
	public String getTeacId(String ref) throws Exception {
		List<Object> result = teacherDao.getTeacId(ref);
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}
		}
		return gNumber;
	}

	public TeacherVO insertTeacherDetails(TeacherVO teacherVO) throws Exception
	{
		System.out.println("Teacher Insert serviceImpl ############################################################");
		TeacherVO Teachervo = new TeacherVO();
		try{
			Teachervo = teacherDao.insertTeacherDetails(teacherVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Teachervo;
	}
	//subjectAssignInsert
	public SubjectAssignVO subjectAssignInsert(SubjectAssignVO subjectAssignVO) throws Exception
	{
		System.out.println("Teacher Insert serviceImpl");
		SubjectAssignVO SubjectAssignvo = new SubjectAssignVO();
		try{
			SubjectAssignvo = teacherDao.subjectAssignInsert(subjectAssignVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return SubjectAssignvo;
	}
	
	
	//teacherAttenInsert
	public teacherAttendanceVO teacherAttenInsert(teacherAttendanceVO teacherVO) throws Exception
	{
		System.out.println("Teacher Insert serviceImpl");
		teacherAttendanceVO Teachervo = new teacherAttendanceVO();
		try{
			Teachervo = teacherDao.teacherAttenInsert(teacherVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Teachervo;
	}
	//teacherAttenUpdate
	public teacherAttendanceVO teacherAttenUpdate(teacherAttendanceVO teacherVO) throws Exception
	{
		System.out.println("Teacher Insert serviceImpl");
		teacherAttendanceVO Teachervo = new teacherAttendanceVO();
		try{
			Teachervo = teacherDao.teacherAttenUpdate(teacherVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Teachervo;
	}
	
	
	
	
	//staffAttenInsert
		public staffAttendanceVO staffAttenInsert(staffAttendanceVO teacherVO) throws Exception
		{
			System.out.println("Teacher Insert serviceImpl");
			staffAttendanceVO Teachervo = new staffAttendanceVO();
			try{
				Teachervo = teacherDao.staffAttenInsert(teacherVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Teachervo;
		}
	//composeMail	
		public MailVO composeMail(MailVO mailVO) throws Exception
		{
			System.out.println("Teacher Insert serviceImpl");
			MailVO Mailvo = new MailVO();
			try{
				Mailvo = teacherDao.composeMail(mailVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Mailvo;
		}
	//groupMail	
		public GroupVO groupMail(GroupVO groupVO) throws Exception
		{
			System.out.println("Group Insert serviceImpl");
			GroupVO Groupvo = new GroupVO();
			try{
				Groupvo = teacherDao.groupMail(groupVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Groupvo;
		}
		
	//deleteMail	
		public MailVO deleteMail(MailVO mailVO) throws Exception
		{
			System.out.println("Teacher delete serviceImpl ---------------------------------> " +mailVO.getMdate()+" and "+mailVO.getMtime());
			MailVO Mailvo = new MailVO();
			try{
				Mailvo = teacherDao.deleteMail(mailVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Mailvo;
		}
		
		//teacherAttenUpdate
		public staffAttendanceVO staffAttenUpdate(staffAttendanceVO teacherVO) throws Exception
		{
			System.out.println("Teacher Insert serviceImpl");
			staffAttendanceVO Teachervo = new staffAttendanceVO();
			try{
				Teachervo = teacherDao.staffAttenUpdate(teacherVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Teachervo;
		}
	
	
	
	
	public TeacherVO getltrreferencedata() throws Exception //(String studid) 
	{

		TeacherVO Teachervo = new TeacherVO();
		try{
			Teachervo.setTeacherServiceVOList(teacherDao.getltrreferencedata());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Teachervo;
	}
	
	public TeacherVO getLtrReferenceDataAll(String techid) throws Exception  
	{
		System.out.println("enter Serviceimpl  "+techid);
		// return teacherDao.getLtrReferenceDataAll(techid);
		TeacherVO Teachervo = new TeacherVO();
		try{
			Teachervo.setTeacherServiceVOList(teacherDao.getLtrReferenceDataAll(techid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Teachervo;
		
	}	
	//getClassSectionAll
	public ClassSectionVO getClassSectionAll(String techid) throws Exception  
	{
		System.out.println("enter Serviceimpl  "+techid);
		// return teacherDao.getLtrReferenceDataAll(techid);
		ClassSectionVO Teachervo = new ClassSectionVO();
		try{
			Teachervo.setTeacherServiceVOList(teacherDao.getClassSectionAll(techid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Teachervo;
		
	}	
	
	public TeacherVO getId(String uname) throws Exception  
	{
		System.out.println("enter Serviceimpl  ");
		 return teacherDao.getId(uname);
	}	
	public TeacherVO getTeacherSubject(String sclass,String sec)throws Exception
	{
		TeacherVO Teachervo=new TeacherVO();
		try{
			Teachervo.setTeacherServiceVOList(teacherDao.getTeacherSubject(sclass,sec));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Teachervo;
	}
	//mailInbox
	public MailVO mailInbox(String mailto)throws Exception
	{
		MailVO Mailvo=new MailVO();
		try{
			Mailvo.setMailServiceVOList(teacherDao.mailInbox(mailto));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Mailvo;
	}
	// getGroupMail
	public GroupVO getGroupMail(String gname)throws Exception
	{
		System.out.println("serviceimpl  groupmail  "+gname);
		
		GroupVO Groupvo=new GroupVO();
		try{
			Groupvo.setGroupServiceVOList(teacherDao.getGroupMail(gname));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Groupvo;
	}
	
	//mailInboxDate
	public MailVO mailInboxDate(String mailto,String mdate)throws Exception
	{
		MailVO Mailvo=new MailVO();
		try{
			Mailvo.setMailServiceVOList(teacherDao.mailInboxDate(mailto,mdate));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Mailvo;
	}
	//mailInboxDateTime
	public MailVO mailInboxDateTime(String mailto,String mdate,String mtime)throws Exception
	{
		MailVO Mailvo=new MailVO();
		try{
			Mailvo.setMailServiceVOList(teacherDao.mailInboxDateTime(mailto,mdate,mtime));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Mailvo;
	}
	//mailSend
	public MailVO mailSend(String mailto)throws Exception
	{
		MailVO Mailvo=new MailVO();
		try{
			Mailvo.setMailServiceVOList(teacherDao.mailSend(mailto));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Mailvo;
	}
	
	//mailTrash
	public MailBackupVO mailTrash(String mto)throws Exception
	{
		System.out.println("Trash serviceimpl  "+mto);
		MailBackupVO Mailvo=new MailBackupVO();
		try{
			Mailvo.setMailBackupServiceVOList(teacherDao.mailTrash(mto));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Mailvo;
	}
	
	//getSubjectAssign
	public SubjectAssignVO getSubjectAssign(String sclass,String sec)throws Exception
	{
		SubjectAssignVO Subjectassignvo=new SubjectAssignVO();
		try{
			Subjectassignvo.setSubjectAssignServiceVOList(teacherDao.getSubjectAssign(sclass,sec));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Subjectassignvo;
	}
	
	//getSubjectAssignTeacher
	public SubjectAssignVO getSubjectAssignTeacher(String sclass,String sec,String tid)throws Exception
	{
		SubjectAssignVO Subjectassignvo=new SubjectAssignVO();
		try{
			Subjectassignvo.setSubjectAssignServiceVOList(teacherDao.getSubjectAssignTeacher(sclass,sec,tid));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Subjectassignvo;
	}
	//getSubjectAssignTeacherID
	public SubjectAssignVO getSubjectAssignTeacherID(String tid)throws Exception
	{
		SubjectAssignVO Subjectassignvo=new SubjectAssignVO();
		try{
			Subjectassignvo.setSubjectAssignServiceVOList(teacherDao.getSubjectAssignTeacherID(tid));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Subjectassignvo;
	}
	
	
	
	
	//getSubjectAssignTeacherType
	public SubjectAssignVO getSubjectAssignTeacherType(String ttype,String mobile)throws Exception
	{
		SubjectAssignVO Subjectassignvo=new SubjectAssignVO();
		try{
			Subjectassignvo.setSubjectAssignServiceVOList(teacherDao.getSubjectAssignTeacherType(ttype,mobile));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Subjectassignvo;
	}
	//getSubjectAssignTeacherClass
	public SubjectAssignVO getSubjectAssignTeacherClass(String sclass,String ttype,String mobile)throws Exception
	{
		SubjectAssignVO Subjectassignvo=new SubjectAssignVO();
		try{
			Subjectassignvo.setSubjectAssignServiceVOList(teacherDao.getSubjectAssignTeacherClass(sclass,ttype,mobile));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Subjectassignvo;
	}
	//getSubjectAssignTeacherClassSection
	public SubjectAssignVO getSubjectAssignTeacherClassSection(String sclass,String section,String ttype,String mobile)throws Exception
	{
		SubjectAssignVO Subjectassignvo=new SubjectAssignVO();
		try{
			Subjectassignvo.setSubjectAssignServiceVOList(teacherDao.getSubjectAssignTeacherClassSection(sclass,section,ttype,mobile));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Subjectassignvo;
	}
	
	
	//getSClassSection
	public TeacherVO getSClassSection(String ttype,String mobile)throws Exception
	{
		System.out.println("enter service   "+ttype+"  and  "+mobile);
		
		TeacherVO Teachervo=new TeacherVO();
		try{
			Teachervo.setTeacherServiceVOList(teacherDao.getSClassSection(ttype,mobile));
		}catch(Exception rr)
		{
				System.out.println(rr);
		}
		return Teachervo;
	}
	//getSClassSectionAll
		public TeacherVO getSClassSectionAll(String ttype,String mobile)throws Exception
		{
			System.out.println("enter service   "+ttype+"  and  "+mobile);
			
			TeacherVO Teachervo=new TeacherVO();
			try{
				Teachervo.setTeacherServiceVOList(teacherDao.getSClassSectionAll(ttype,mobile));
			}catch(Exception rr)
			{
					System.out.println(rr);
			}
			return Teachervo;
		}
	//getCheckin
		public teacherAttendanceVO getCheckin(String adate,String tid)throws Exception
		{
			System.out.println("enter service   "+adate+"  and  "+tid);
			
			teacherAttendanceVO Teachervo=new teacherAttendanceVO();
			try{
				Teachervo.setTeacherAttendanceServiceVOList(teacherDao.getCheckin(adate,tid));
			}catch(Exception rr)
			{
					System.out.println(rr);
			}
			return Teachervo;
		}
		
		//getStaffCheckin
				public staffAttendanceVO getStaffCheckin(String adate,String tid)throws Exception
				{
					System.out.println("enter service   "+adate+"  and  "+tid);
					
					staffAttendanceVO Teachervo=new staffAttendanceVO();
					try{
						Teachervo.setStaffAttendanceServiceVOList(teacherDao.getStaffCheckin(adate,tid));
					}catch(Exception rr)
					{
							System.out.println(rr);
					}
					return Teachervo;
				}	
		
		
		//getStaffAttendance
		public teacherAttendanceVO getStaffAttendance(String fdate,String tdate)throws Exception
		{
			System.out.println("enter service   "+fdate+tdate);
			
			teacherAttendanceVO Teachervo=new teacherAttendanceVO();
			try{
				Teachervo.setTeacherAttendanceServiceVOList(teacherDao.getStaffAttendance(fdate,tdate));
			}catch(Exception rr)
			{
					System.out.println(rr);
			}
			return Teachervo;
		}
		
		
		public GroupVO getDLlist() throws Exception //(String studid) 
		{

			GroupVO Grpvo = new GroupVO();
			try{
				Grpvo.setGroupServiceVOList(teacherDao.getDLlist());
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Grpvo;
		}
		
		
		
		//getgroup
		public GroupVO getgroup(String groupname)throws Exception
		{
			GroupVO grpname=new GroupVO();
			try{
				grpname.setGroupServiceVOList(teacherDao.getgroup(groupname));
			}catch(Exception rr)
			{
					System.out.println(rr);
			}
			return grpname;
		}
		
		public LoginVO getlogindetail() throws Exception
		{
					System.out.println("Enter service impl student id");
					LoginVO tidvo = new LoginVO();
					try{
						String tid = null;
						tidvo.setLoginServiceVOList(teacherDao.getlogindetail());
						}
					catch(Exception e) 
					{
					System.out.println("Error  "+e);
					} 
					return tidvo;
				}	
      
		public LoginVO getlogindetailAll() throws Exception
		{
					System.out.println("Enter service impl student id");
					LoginVO tidvo = new LoginVO();
					try{
						String tid = null;
						tidvo.setLoginServiceVOList(teacherDao.getlogindetailAll());
						}
					catch(Exception e) 
					{
					System.out.println("Error  "+e);
					} 
					return tidvo;
				}	
		
}
