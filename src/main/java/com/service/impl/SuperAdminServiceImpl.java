package com.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.SuperAdminDAO;
import com.model.SchoolInfoVO;
import com.service.SuperAdminService;

@Service
public class SuperAdminServiceImpl implements SuperAdminService 
{
	@Autowired
	public SuperAdminDAO superAdminDAO;

	
	public boolean registerSchool(SchoolInfoVO schoolInfoVO) 
	{
		return superAdminDAO.registerSchool(schoolInfoVO);
	}
	
}
