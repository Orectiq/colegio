package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.AssignmentDao;
import com.model.AssignApprovalVO;
import com.model.AssignmentaddVO;
import com.model.StuVO;
//import com.model.SubjectVO;
import com.service.AssignmentService;


@Service
public class AssignmentServiceImpl implements AssignmentService {

	@Autowired
	@Qualifier(value="assignmentDao")
	private AssignmentDao assignmentDao=null;

	
	public AssignmentaddVO insertAssignmentDetails(AssignmentaddVO assignmentaddVO) throws Exception
	{
		System.out.println("dao impl Assignment");
		
		AssignmentaddVO Assignmentaddvo = new AssignmentaddVO();
		try{
			Assignmentaddvo = assignmentDao.insertAssignmentDetails(assignmentaddVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Assignmentaddvo;
	}
	
	//insertAssignApproval
	public AssignApprovalVO insertAssignApproval(AssignApprovalVO assignappVO) throws Exception
	{
		System.out.println("dao impl Assignment");
		
		AssignApprovalVO Assignappvo = new AssignApprovalVO();
		try{
			Assignappvo = assignmentDao.insertAssignApproval(assignappVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Assignappvo;
	}
	//updateAssignApproval
	public AssignApprovalVO updateAssignApproval(AssignApprovalVO assignappVO) throws Exception
	{
		System.out.println("dao impl Assignment");
		
		AssignApprovalVO Assignappvo = new AssignApprovalVO();
		try{
			Assignappvo = assignmentDao.updateAssignApproval(assignappVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Assignappvo;
	}

		

	




	








	public AssignmentaddVO getAssignment(String sclass, String section) throws Exception {
		System.out.println("Enter service impl  Parent sclass  "+sclass+" and "+section);
		AssignmentaddVO Assignmentaddvo = new AssignmentaddVO();
		try{
			String sec = null;
			Assignmentaddvo.setAssignmentServiceVOList(assignmentDao.getAssignment(sclass,section));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}
	
	//getSubject
	public AssignApprovalVO getSubject(String tid) throws Exception {
		System.out.println("Enter service impl  Parent sclass  "+tid);
		AssignApprovalVO Assignmentaddvo = new AssignApprovalVO();
		try{
			String sec = null;
			Assignmentaddvo.setAssignmentApprovalServiceVOList(assignmentDao.getSubject(tid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}	
	//getAssignApprovalTeacher(String adate,String tid,String sclass,String sec,String subj,String sta)
	public AssignApprovalVO getAssignApprovalTeacher(String adate,String tid,String sclass,String sec,String subj,String sta) throws Exception {
		System.out.println("Enter service impl  Parent sclass  "+tid);
		AssignApprovalVO Assignmentaddvo = new AssignApprovalVO();
		try{
			String sec1 = null;
			Assignmentaddvo.setAssignmentApprovalServiceVOList(assignmentDao.getAssignApprovalTeacher(adate,tid,sclass,sec,subj,sta));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}	
	//getApprovalTType
	public AssignApprovalVO getApprovalTType(String ttype,String tid) throws Exception {
		System.out.println("enter serviceimpl Assignment !!!!!! "+ttype+" and  "+tid);
		AssignApprovalVO Assignmentaddvo = new AssignApprovalVO();
		try{
			String sec1 = null;
			Assignmentaddvo.setAssignmentApprovalServiceVOList(assignmentDao.getApprovalTType(ttype,tid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}	
	//getApprovalList(String sclass,String section,String tid,String title,String ttype)
	public AssignApprovalVO getApprovalList(String sclass,String section,String tid,String title,String ttype) throws Exception {
		System.out.println("Enter service impl  Parent sclass  "+tid);
		AssignApprovalVO Assignmentaddvo = new AssignApprovalVO();
		try{
			String sec1 = null;
			Assignmentaddvo.setAssignmentApprovalServiceVOList(assignmentDao.getApprovalList(sclass,section,tid,title,ttype));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}	
	//getAdminApprovalList
	public AssignApprovalVO getAdminApprovalList(String sclass,String section,String subject) throws Exception {
		//System.out.println("Enter service impl  Parent sclass  "+tid);
		AssignApprovalVO Assignmentaddvo = new AssignApprovalVO();
		try{
			String sec1 = null;
			Assignmentaddvo.setAssignmentApprovalServiceVOList(assignmentDao.getAdminApprovalList(sclass,section,subject));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}	
	
	//getAssignmentTeacher
	public AssignmentaddVO getAssignmentTeacher(String sclass, String section,String tid,String adate,String ttype) throws Exception {
		System.out.println("Enter service impl  Parent sclass  "+sclass+" and "+section);
		AssignmentaddVO Assignmentaddvo = new AssignmentaddVO();
		try{
			String sec = null;
			Assignmentaddvo.setAssignmentServiceVOList(assignmentDao.getAssignmentTeacher(sclass,section,tid,adate,ttype));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}
	//getAssignmentTeacherAllot
	public AssignmentaddVO getAssignmentTeacherAllot(String aid) throws Exception {
		System.out.println("Enter service impl  Assignment id  "+aid);
		AssignmentaddVO Assignmentaddvo = new AssignmentaddVO();
		try{
			String sec = null;
			Assignmentaddvo.setAssignmentServiceVOList(assignmentDao.getAssignmentTeacherAllot(aid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}
	
	
	//getTitle  String hdate,String subj
	public AssignmentaddVO getTitle(String hdate,String subj) throws Exception {
		System.out.println("Enter service impl  Parent sclass  ");
		AssignmentaddVO Assignmentaddvo = new AssignmentaddVO();
		try{
			String sec = null;
			Assignmentaddvo.setAssignmentServiceVOList(assignmentDao.getTitle(hdate,subj));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}
	
	
	public AssignmentaddVO getTType(String ttype,String tid) throws Exception {
		System.out.println("Enter service impl  Parent sclass  ");
		AssignmentaddVO Assignmentaddvo = new AssignmentaddVO();
		try{
			String sec = null;
			Assignmentaddvo.setAssignmentServiceVOList(assignmentDao.getTType(ttype,tid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Assignmentaddvo;
	}
	
}

