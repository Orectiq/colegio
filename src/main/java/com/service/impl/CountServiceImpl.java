package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.CountDao;
import com.dao.StudentDao;
import com.service.CountService;

@Service
public class CountServiceImpl implements CountService 
{
	@Autowired
	@Qualifier(value="countDao")
	private CountDao countDao=null;
	
	//countTeacherLeaveReq
	public int countTeacherLeaveReq() throws Exception 
	{
		return countDao.countTeacherLeaveReq();
	}
	
}
