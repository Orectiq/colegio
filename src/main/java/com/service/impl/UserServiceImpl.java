package com.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.common.MasVO;
import com.common.TeachVO;
import com.common.UserVO;
import com.dao.UserDao;
import com.model.CounterVO;
import com.model.CountryVO;
import com.model.LibrarianVO;
import com.model.LoginVO;
import com.model.MailSettingVO;
import com.model.MasterVO;
import com.model.ParentVO;
import com.model.SettingadminVO;
import com.model.StuVO;
import com.model.TeacherVO;
import com.model.TransportVO;
import com.service.UserService;

@Service
public class UserServiceImpl implements UserService 
{
	@Autowired
	@Qualifier(value="userDao")
	private UserDao userDao=null;

	public MasVO isUserNameExist(String userId, String pwd) throws Exception {	
		System.out.println("ServiceIMPL Uname  "+userId+" and  password  "+pwd);
		MasVO masVO = null;
		try {			
			masVO =  userDao.isUserNameExist(userId, pwd);
		} catch (Exception e) {
		}		
		return masVO;	
	}
	//isCommonUserExist
	public LoginVO isCommonUserExist(String userId, String pwd) throws Exception {	
		System.out.println("ServiceIMPL Uname  "+userId+" and  password  "+pwd);
		LoginVO loginVO = null;
		try {			
			loginVO =  userDao.isCommonUserExist(userId, pwd);
		} catch (Exception e) {
		}		
		return loginVO;	
	}
	
	public TeacherVO isStaffNameExist(String userId, String pwd) throws Exception {	
		System.out.println("ServiceIMPL Uname  "+userId+" and  password  "+pwd);
		TeacherVO tecVO = null;
		try {			
			tecVO =  userDao.isStaffNameExist(userId, pwd);
		} catch (Exception e) {
		}		
		return tecVO;	
	}
	//isTransportExist
	public TransportVO isTransportExist(String userId, String pwd) throws Exception {	
		System.out.println("ServiceIMPL Uname  "+userId+" and  password  "+pwd);
		TransportVO transVO = null;
		try {			
			transVO =  userDao.isTransportExist(userId, pwd);
		} catch (Exception e) {
		}		
		return transVO;	
	}
	
	public StuVO isSTUidExist(String userId, String password) throws Exception {
		
			System.out.println("ServiceIMPL Uname  "+userId+" and  password  "+password);
			StuVO stuVO = null;
			try {			
				stuVO =  userDao.isSTUidExist(userId, password);
			} catch (Exception e) {
			}		
			return stuVO;	
	}
	//isLIBidExist
	public LibrarianVO isLIBidExist(String userId, String password) throws Exception {
		
		System.out.println("ServiceIMPL Uname  "+userId+" and  password  "+password);
		LibrarianVO libVO = null;
		try {			
			libVO =  userDao.isLIBidExist(userId, password);
		} catch (Exception e) {
		}		
		return libVO;	
}
	
	
	public ParentVO isPARidExist(String userId, String password) throws Exception {
		
		System.out.println("ServiceIMPL Uname  "+userId+" and  password  "+password);
		ParentVO parVO = null;
		try {			
			parVO =  userDao.isPARidExist(userId, password);
		} catch (Exception e) {
		}		
		return parVO;	
}


	public MasterVO insertMasterDetails(MasterVO masterVO) throws Exception
	{
		MasterVO Mastervo = new MasterVO();
		try{
			Mastervo = userDao.insertMasterDetails(masterVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Mastervo;
	}
	//updateAdminPhoto
	public MasterVO updateAdminPhoto(MasterVO masterVO) throws Exception
	{
		System.out.println("ServiceIMPL  "+masterVO.getPhoto()+" and  "+masterVO.getUname());
		MasterVO Mastervo = new MasterVO();
		try{
			Mastervo = userDao.updateAdminPhoto(masterVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Mastervo;
	}
	//photoUpdate
	public LoginVO photoUpdate(LoginVO loginVO) throws Exception
	{
		System.out.println("ServiceIMPL  "+loginVO.getPhoto()+" and  "+loginVO.getUname());
		LoginVO Loginvo = new LoginVO();
		try{
			Loginvo = userDao.photoUpdate(loginVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Loginvo;
	}
	//updateMobilePassword
	public LoginVO updateMobilePassword(LoginVO loginVO) throws Exception
	{
		System.out.println("ServiceIMPL  "+loginVO.getPhoto()+" and  "+loginVO.getUname());
		LoginVO Loginvo = new LoginVO();
		try{
			Loginvo = userDao.updateMobilePassword(loginVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Loginvo;
	}
	//updateAdminGlobalSetting
	public LoginVO updateAdminGlobalSetting(LoginVO loginVO) throws Exception
	{
		System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		System.out.println("SERVICE IMPL  General Setting  ");
		LoginVO Loginvo = new LoginVO();
		try{
			Loginvo = userDao.updateAdminGlobalSetting(loginVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Loginvo;
	}
	//updateMailPassword
	public LoginVO updateMailPassword(LoginVO loginVO) throws Exception
	{
		System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		LoginVO Loginvo = new LoginVO();
		try{
			Loginvo = userDao.updateMailPassword(loginVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Loginvo;
	}
	
	//insertMailSetting
	public MailSettingVO insertMailSetting(MailSettingVO mailsettingVO) throws Exception
	{
		//System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		MailSettingVO Loginvo = new MailSettingVO();
		try{
			Loginvo = userDao.insertMailSetting(mailsettingVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Loginvo;
	}
	//getMailSetting
	public MailSettingVO getMailSetting() throws Exception
	{
		//System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		MailSettingVO mailsettingvo = new MailSettingVO();
	
		try{
			mailsettingvo.setMailSettingServiceVOList(userDao.getMailSetting());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return mailsettingvo;
	}
	public MailSettingVO getMailSettingType(String temvar) throws Exception
	{
		//System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		MailSettingVO mailsettingvo = new MailSettingVO();
	
		try{
			mailsettingvo.setMailSettingServiceVOList(userDao.getMailSettingType(temvar));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return mailsettingvo;
	}
	
	
	//getIDSetting
	public CounterVO getIDSetting(String prifix) throws Exception
	{
		//System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		CounterVO countervo = new CounterVO();
	
		try{
			countervo.setCounterServiceVOList(userDao.getIDSetting(prifix));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return countervo;
	}
	//getIDSettingAll
	public CounterVO getIDSettingAll()throws Exception
	{
		CounterVO countervo=new CounterVO();
		try{
			countervo.setCounterServiceVOList(userDao.getIDSettingAll());
		}catch(Exception e)
		{
			
		}
		return countervo;
	}
	//updateIDSetting
	public CounterVO updateIDSetting(CounterVO counterVO) throws Exception
	{
		//System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		CounterVO Countervo = new CounterVO();
		try{
			Countervo = userDao.updateIDSetting(counterVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Countervo;
	}
	
	
	//insertTerms
	public MailSettingVO insertTerms(MailSettingVO mailsettingVO) throws Exception
	{
		//System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		MailSettingVO Loginvo = new MailSettingVO();
		try{
			Loginvo = userDao.insertTerms(mailsettingVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Loginvo;
	}
	
	
	
	
	
	
	//getTerms
	public MailSettingVO getTerms() throws Exception
	{
		//System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		MailSettingVO mailsettingvo = new MailSettingVO();
	
		try{
			mailsettingvo.setMailSettingServiceVOList(userDao.getTerms());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return mailsettingvo;
	}
	
	//loadCountry
	public CountryVO loadCountry() throws Exception
	{
		//System.out.println("ServiceIMPL  "+loginVO.getUname()+" and  "+loginVO.getSlogo());
		CountryVO countryvo = new CountryVO();
	
		try{
			countryvo.setCountryServiceVOList(userDao.loadCountry());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return countryvo;
	}
	
	
	//getLoginDetails
	
		public LoginVO getLoginDetails(String uid, String pass) throws Exception 
		{
			System.out.println("Enter service impl  Parent sclass  "+uid+" pass  :  "+pass);
			LoginVO Stuvo = new LoginVO();
			try{
				Stuvo.setLoginServiceVOList(userDao.getLoginDetails(uid,pass));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Stuvo;
		}
	//getLoginMobile
		public LoginVO getLoginMobile(String uid) throws Exception 
		{
			System.out.println("Enter service impl  Parent sclass  "+uid);
			LoginVO Stuvo = new LoginVO();
			try{
				Stuvo.setLoginServiceVOList(userDao.getLoginMobile(uid));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Stuvo;
		}
	//getLoginEmail
		public LoginVO getLoginEmail(String uid) throws Exception 
		{
			System.out.println("Enter service impl  Parent sclass  "+uid);
			LoginVO Stuvo = new LoginVO();
			try{
				Stuvo.setLoginServiceVOList(userDao.getLoginEmail(uid));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Stuvo;
		}
		
	//getAdminSettingDetails	
		public SettingadminVO getAdminSettingDetails(String uid) throws Exception 
		{
			System.out.println("Enter service impl  Parent sclass  "+uid);
			SettingadminVO Stuvo = new SettingadminVO();
			try{
				Stuvo.setSettingAdminServiceVOList(userDao.getAdminSettingDetails(uid));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Stuvo;
		}
		
	//updatePassword
	public MasterVO updatePassword(MasterVO masterVO) throws Exception
	{
		MasterVO Mastervo = new MasterVO();
		try{
			Mastervo = userDao.updatePassword(masterVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Mastervo;
	}
	
	public MasterVO getMasterDetail(String uname)throws Exception
	{
		System.out.println("Service   "+uname);
		MasterVO mastervo=new MasterVO();
		try{
			mastervo.setMasterServiceVOList(userDao.getMasterDetail(uname));
		}
		catch(Exception rr)
		{
			
		}
		return mastervo;
	}
	
	
}
