package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.HomeworkDao;
import com.model.HomeworkStatusVO;
import com.model.HomeworkVO;

import com.service.HomeworkService;

@Service
public class HomeworkServiceImpl implements HomeworkService
{
	@Autowired
	@Qualifier(value="homeworkDao")
	private HomeworkDao homeworkDao=null;

	//@Override
	public HomeworkVO insertHomeworkDetails(HomeworkVO homeworkVO) throws Exception
	{
		
		HomeworkVO Homeworkvo = new HomeworkVO();
		try{
			Homeworkvo = homeworkDao.insertHomeworkDetails(homeworkVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Homeworkvo;

	}
	//insertHomeworkStatus
	public HomeworkStatusVO insertHomeworkStatus(HomeworkStatusVO homeworkVO) throws Exception
	{
		
		
		HomeworkStatusVO Homeworkvo = new HomeworkStatusVO();
		
		System.out.println("Home work date service " + homeworkVO.getTheDate());
		
		try{
			Homeworkvo = homeworkDao.insertHomeworkStatus(homeworkVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Homeworkvo;

	}
	
	
	public HomeworkVO getLtrReferenceData() throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		HomeworkVO Homeworkvo = new HomeworkVO();
		try{
			Homeworkvo.setHomeworkServiceVOList(homeworkDao.getLtrReferenceData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Homeworkvo;
	}
	
	//getHomeworkdata
	public HomeworkVO getHomeworkdata(String sclass,String sec,String ttype,String tid,String dt) throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		HomeworkVO Homeworkvo = new HomeworkVO();
		try{
			Homeworkvo.setHomeworkServiceVOList(homeworkDao.getHomeworkdata(sclass,sec,ttype,tid,dt));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Homeworkvo;
	}
	
	//String sclass,String sec,String thedate
	public HomeworkVO getHomeworStudata(String sclass,String sec,String thedate) throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		HomeworkVO Homeworkvo = new HomeworkVO();
		try{
			Homeworkvo.setHomeworkServiceVOList(homeworkDao.getHomeworkStudata(sclass,sec,thedate));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Homeworkvo;
	}
	
	//getHomeworkStatus
	public HomeworkStatusVO getHomeworkStatus(String tid,String thedate,String sid) throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		HomeworkStatusVO Homeworkvo = new HomeworkStatusVO();
		try{
			Homeworkvo.setHomeworkStatusServiceVOList(homeworkDao.getHomeworkStatus(tid,thedate,sid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Homeworkvo;
	}
	
	//getHomeWorkStuStatus
	public HomeworkStatusVO getHomeWorkStuStatus(String sclass,String sec,String mobile,String subj) throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		HomeworkStatusVO Homeworkvo = new HomeworkStatusVO();
		try{
			Homeworkvo.setHomeworkStatusServiceVOList(homeworkDao.getHomeWorkStuStatus(sclass,sec,mobile,subj));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Homeworkvo;
	}

}
