package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.SubjectDao;
import com.model.MenuItemVO;
import com.model.SectionVO;
import com.model.StuVO;
import com.model.SubjectVO;
import com.model.TeacherVO;
import com.service.SubjectService;

@Service
public class SubjectServiceImpl  implements SubjectService {

	@Autowired
	@Qualifier(value="subjectDao")
	private SubjectDao subjectDao=null;

public com.model.SubjectVO insertSubjectDetails(com.model.SubjectVO subjectVO) throws Exception 
{
		SubjectVO Subjectvo = new SubjectVO();
		try{
			Subjectvo = subjectDao.insertSubjectDetails(subjectVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Subjectvo;
	}
//insertClass
public com.model.SubjectVO insertClass(String[] myArray) throws Exception 
{
		SubjectVO Subjectvo = new SubjectVO();
		try{
			Subjectvo = subjectDao.insertClass(myArray);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Subjectvo;
	}

//deleteSubject
public com.model.SubjectVO deleteSubject(com.model.SubjectVO subjectVO) throws Exception 
{
		SubjectVO Subjectvo = new SubjectVO();
		try{
			Subjectvo = subjectDao.deleteSubject(subjectVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Subjectvo;
	}


//insertSection
public SectionVO insertSection(SectionVO sectionVO)throws Exception
{
	System.out.println("ServiceIMPL");
	SectionVO  Sectionvo=new SectionVO();
	try{
		Sectionvo = subjectDao.insertSection(sectionVO);
		
	}catch(Exception e) {
		throw new Exception("DB Exception: "+e.getMessage());
	} 
	return Sectionvo;
}


public TeacherVO getSubject(String sclass, String section,String ttype,String tid) throws Exception {
	System.out.println("Enter service impl  subject sclass  "+sclass+" and "+section);
	TeacherVO Subjectvo = new TeacherVO();
	try{
		String sec = null;
		Subjectvo.setTeacherServiceVOList(subjectDao.getSubject(sclass,section,ttype,tid));
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Subjectvo;
}

//getTeacherSubject
public SubjectVO getTeacherSubject(String sclass, String section) throws Exception {
	System.out.println("Enter service impl  subject sclass ---------> "+sclass+" and "+section);
	SubjectVO Subjectvo = new SubjectVO();
	try{
		String sec = null;
		Subjectvo.setSubjectServiceVOList(subjectDao.getTeacherSubject(sclass,section));
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Subjectvo;
}
////getSClassMaster
public SubjectVO getSClassMaster() throws Exception {
	System.out.println("Enter service impl  subject sclass ---------> ");
	SubjectVO Subjectvo = new SubjectVO();
	try{
		String sec = null;
		Subjectvo.setSubjectServiceVOList(subjectDao.getSClassMaster());
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Subjectvo;
}


//getSClass	

public SectionVO getSClass() throws Exception {
	System.out.println("Enter service impl  subject sclass  ");
	SectionVO Sectionvo = new SectionVO();
	try{
		String sec = null;
		Sectionvo.setSectionServiceVOList(subjectDao.getSClass());
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Sectionvo;
}	
//getMenuData
public MenuItemVO getMenuData(String mtype) throws Exception {
	System.out.println("Enter service impl  subject sclass  ");
	MenuItemVO Menuitemvo = new MenuItemVO();
	try{
		String sec = null;
		Menuitemvo.setMenuitemServiceVOList(subjectDao.getMenuData(mtype));
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Menuitemvo;
	
	
}	



//getSection
public SectionVO getSection(String sclass) throws Exception {
	System.out.println("Enter service impl  subject sclass  ");
	SectionVO Sectionvo = new SectionVO();
	try{
		String sec = null;
		Sectionvo.setSectionServiceVOList(subjectDao.getSection(sclass));
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Sectionvo;
}	
	
}
