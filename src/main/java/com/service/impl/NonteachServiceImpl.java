package com.service.impl;


import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


import com.dao.NonteachDao;
import com.model.NonteachVO;
import com.model.TeacherVO;
import com.model.staffAttendanceVO;
import com.service.NonteachService;

public class NonteachServiceImpl implements NonteachService
{
	
	@Autowired
	@Qualifier(value="nonteachDao")
	private NonteachDao nonteachDao=null;
	
	
	
	
	public NonteachVO getLibID(String ref) throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  "+ref);
		NonteachVO nonteacherVO = new NonteachVO();
		nonteacherVO.setStaff_ID(this.getLibID1(ref));
		return nonteacherVO;
	}
	public NonteachVO getDorID(String ref) throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  "+ref);
		NonteachVO nonteacherVO = new NonteachVO();
		nonteacherVO.setStaff_ID(this.getDorID1(ref));
		return nonteacherVO;
	}
	public NonteachVO getTraID(String ref) throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  "+ref);
		NonteachVO nonteacherVO = new NonteachVO();
		nonteacherVO.setStaff_ID(this.getTraID1(ref));
		return nonteacherVO;
	}
	
	public String getLibID1(String ref) throws Exception {
		List<Object> result = nonteachDao.getLibID(ref);
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}
		}
		return gNumber;
	}
	
	
	public String getDorID1(String ref) throws Exception {
		List<Object> result = nonteachDao.getDorID(ref);
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}
		}
		return gNumber;
	}
	
	public String getTraID1(String ref) throws Exception {
		List<Object> result = nonteachDao.getTraID(ref);
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}
		}
		return gNumber;
	}
	
	
	
	public NonteachVO insertNonteachDetails(NonteachVO nonteachVO,String path1) throws Exception 
	{
		System.out.println("Service IMPL  ");
		NonteachVO Nonteachvo = new NonteachVO();
		try{
			Nonteachvo = nonteachDao.insertNonteachDetails(nonteachVO,path1);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Nonteachvo;
	}


	public NonteachVO getLtrReferenceData() throws Exception //(String studid) 
	{
		 
		NonteachVO Nonteachvo = new NonteachVO();
		try{
			Nonteachvo.setNonteachServiceVOList(nonteachDao.getLtrReferenceData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nonteachvo;
		 
	}

	
	public NonteachVO getLtrReferenceDataall() throws Exception //(String studid) 
	{
		 
		NonteachVO Nonteachvo = new NonteachVO();
		try{
			Nonteachvo.setNonteachServiceVOList(nonteachDao.getLtrReferenceData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nonteachvo;
		 
	}
	
	public NonteachVO getNonteach(String department) throws Exception {
		System.out.println("getNonteach Servie IMPL " +department);
		NonteachVO Nonteachvo = new NonteachVO();
		try{
			Nonteachvo.setNonteachServiceVOList(nonteachDao.getNonteach(department));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nonteachvo;
	}
	//getNonTeachingLoginID
	public NonteachVO getNonTeachingLoginID(String mobile) throws Exception {
		System.out.println("getNonteach Servie IMPL " +mobile);
		NonteachVO Nonteachvo = new NonteachVO();
		try{
			Nonteachvo.setNonteachServiceVOList(nonteachDao.getNonTeachingLoginID(mobile));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nonteachvo;
	}
	
	//getNonTeachIDWithPhone
	public NonteachVO getNonTeachIDWithPhone(String mobile) throws Exception {
		System.out.println("getNonteach Servie IMPL " +mobile);
		NonteachVO Nonteachvo = new NonteachVO();
		try{
			Nonteachvo.setNonteachServiceVOList(nonteachDao.getNonTeachIDWithPhone(mobile));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nonteachvo;
	}
	
	//getNonTeachAttendance
	public staffAttendanceVO getNonTeachAttendance(String fdate,String tdate,String tid) throws Exception {
		System.out.println("getNonteach Servie IMPL " +fdate);
		staffAttendanceVO Nonteachvo = new staffAttendanceVO();
		try{
			Nonteachvo.setStaffAttendanceServiceVOList(nonteachDao.getNonTeachAttendance(fdate,tdate,tid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Nonteachvo;
	}
	
	//sendMail
	public NonteachVO sendMail(NonteachVO nonteachVO) throws Exception {
		System.out.println("Mail ServiceIMPL  -->  "+nonteachVO.getToAddr()+" from  "+nonteachVO.getFrmAddr()+" subj  "+nonteachVO.getSubj()+" body  "+nonteachVO.getBody());
		NonteachVO Nonteachvo = new NonteachVO();
		try{
			Nonteachvo = nonteachDao.sendMail(nonteachVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Nonteachvo;
	}
	
	
}
