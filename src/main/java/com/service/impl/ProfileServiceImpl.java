package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.ProfileDao;
import com.model.LoginVO;
import com.service.ProfileService;

@Service
public class ProfileServiceImpl implements ProfileService 
{
	@Autowired
	@Qualifier(value="profileDao")
	private ProfileDao profileDao=null;
	
	//updateMobilePassword
		public LoginVO updateParentsPassword(LoginVO loginVO) throws Exception
		{
			System.out.println("ServiceIMPL  "+loginVO.getPhoto()+" and  "+loginVO.getUname());
			LoginVO Loginvo = new LoginVO();
			try{
				Loginvo = profileDao.updateParentsPassword(loginVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Loginvo;
		}
		
		//photoUpdate
		public LoginVO photoUpdate(LoginVO loginVO) throws Exception
		{
			System.out.println("ServiceIMPL  "+loginVO.getPhoto()+" and  "+loginVO.getUname());
			LoginVO Loginvo = new LoginVO();
			try{
				Loginvo = profileDao.photoUpdate(loginVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Loginvo;
		}
		//updateMobilePassword
		public LoginVO updateMobilePassword(LoginVO loginVO) throws Exception
		{
			System.out.println("ServiceIMPL  "+loginVO.getPhoto()+" and  "+loginVO.getUname());
			LoginVO Loginvo = new LoginVO();
			try{
				Loginvo = profileDao.updateMobilePassword(loginVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Loginvo;
		}
		
}
