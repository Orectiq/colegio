package com.service.impl;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.model.AssignmentVO;
import com.model.AttendanceDayVO;
import com.model.AttendanceVO;
import com.model.CounterVO;
import com.model.DummyVO;
import com.model.EchallanVO;
import com.model.LibraryIssueVO;
import com.model.LoginVO;
import com.model.MarksVO;
import com.model.MedicalVO;
import com.model.Nb_newsVO;
import com.model.NoticeboardVO;
import com.model.ParentVO;
import com.model.StuMedicalVO;
import com.model.StuParVO;
import com.model.StuVO;
import com.model.TimeTableVO;
import com.common.ExamTypeMasterVO;
import com.common.MasVO;
import com.common.StudentVO;
import com.dao.StudentDao;
import com.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService
{
	@Autowired
	@Qualifier(value="studentDao")
	private StudentDao studentDao=null;
	
	
	//Nb_newsVO getNewsId()
	public Nb_newsVO getNewsId() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl --------------------> ");
		Nb_newsVO newsVO = new Nb_newsVO();
		newsVO.setNewsid(this.getNewsID1());
		return newsVO;
	}
	//NoticeboardVO getEventsId
	public NoticeboardVO getEventsId() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl --------------------> ");
		NoticeboardVO eventsVO = new NoticeboardVO();
		eventsVO.setEventsid(this.getEventsId1());
		return eventsVO;
	}
	public StuVO getStudentId() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl --------------------> ");
		StuVO stuVO = new StuVO();
		stuVO.setStudent_id(this.getStuId());
		return stuVO;
	}
	//getAssignId
	public StuVO getAssignId() throws Exception 
	{
		System.out.println("enter getASSIGNID ServiceImpl --------------------> ");
		StuVO stuVO = new StuVO();
		stuVO.setAssignid(this.getAssId());
		return stuVO;
	}
	
	//getReqId
	public LibraryIssueVO getReqId() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  ");
		LibraryIssueVO libVO = new LibraryIssueVO();
		//stuVO.setStudent_id(this.getStuId(ref));
		libVO.setBreq_id(this.getRequId());
		return libVO;
	}
	
	public StuVO getParentId() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  ");
		StuVO stuVO = new StuVO();
		stuVO.setParent_id(this.getParId());
		return stuVO;
	}
	
	public StuVO getAdmId() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  ");
		StuVO stuVO = new StuVO();
		stuVO.setAdm_id(this.getAdmnId());
		return stuVO;
	}
	public StuVO getAdmId1() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  ");
		StuVO stuVO = new StuVO();
		stuVO.setAdm_id(this.getAdmnId1());
		return stuVO;
	}
	public StuVO getStudentRoll() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  ");
		StuVO stuVO = new StuVO();
		stuVO.setStudent_roll(this.getStuRoll());
		return stuVO;
	}
	//getChallanID
	public EchallanVO getChallanID() throws Exception 
	{
		System.out.println("enter getstuid ServiceImpl  ");
		EchallanVO stuVO = new EchallanVO();
		stuVO.setChallan_id(this.getChallanID1());
		return stuVO;
	}
	
	
	
	//getNewsID1
	public String getNewsID1() throws Exception {
		System.out.println("Serviceimpl  prefix -------------------->1 ");
		List<Object> result = studentDao.getNewsid();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			
			
			String rfnNo = String.valueOf((Integer) obj[1]);
			
			System.out.println("Serviceimpl  prefix --------------------> "+prefix+" no.  "+rfnNo);
			
			
			gNumber = prefix.concat(rfnNo);
		}
		return gNumber;
	}
	//getEventsId1
	public String getEventsId1() throws Exception {
		System.out.println("Serviceimpl  prefix -------------------->1 ");
		List<Object> result = studentDao.getEventsId();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			
			
			String rfnNo = String.valueOf((Integer) obj[1]);
			
			System.out.println("Serviceimpl  prefix --------------------> "+prefix+" no.  "+rfnNo);
			
			
			gNumber = prefix.concat(rfnNo);
		}
		return gNumber;
	}
	
	public String getStuId() throws Exception {
		System.out.println("Serviceimpl  prefix -------------------->1 ");
		List<Object> result = studentDao.getStuId();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			
			
			String rfnNo = String.valueOf((Integer) obj[1]);
			
			System.out.println("Serviceimpl  prefix --------------------> "+prefix+" no.  "+rfnNo);
			

			gNumber = prefix.concat(rfnNo);
		}
		return gNumber;
	}
	
	
	public String getAssId() throws Exception {
		System.out.println("Serviceimpl  getASSIGNID  prefix -------------------->1 ");
		List<Object> result = studentDao.getAssignId();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			
			
			String rfnNo = String.valueOf((Integer) obj[1]);
			
			System.out.println("Serviceimpl  prefix --------------------> "+prefix+" no.  "+rfnNo);
			

			gNumber = prefix.concat(rfnNo);
		}
		return gNumber;
	}
	
	
	
	
	//Library Req 
	
	public String getRequId() throws Exception {
		List<Object> result = studentDao.getReqId();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			/*if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}*/
			gNumber = prefix.concat(rfnNo);
		}
		return gNumber;
	}
	
	
	
	
	
	
	public String getParId() throws Exception {
		List<Object> result = studentDao.getParId();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			/*if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}*/
			gNumber = prefix.concat(rfnNo);
		}
		return gNumber;
	}
	public String getAdmnId() throws Exception {
		List<Object> result = studentDao.getAdmnId();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			/*if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}*/
			gNumber = prefix.concat(rfnNo);
		}
		
		
		return gNumber;
	}
	
	public String getAdmnId1() throws Exception {
		List<Object> result = studentDao.getAdmnId1();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			/*if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}*/
			gNumber = prefix.concat(rfnNo);
		}
		
		
		return gNumber;
	}
	public String getStuRoll() throws Exception {
		List<Object> result = studentDao.getStuRoll();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			/*if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}*/
			gNumber = prefix.concat(rfnNo);
		}
		return gNumber;
	}



	public String getChallanID1() throws Exception {
		List<Object> result = studentDao.getChallanID();
		String rfnNumber = "";
		String gNumber = "";
		for (Iterator<Object> itr = result.iterator(); itr.hasNext();) {
			Object[] obj = (Object[]) itr.next();
			String prefix = (String) obj[0];
			String rfnNo = String.valueOf((Integer) obj[1]);
			/*if (rfnNo.length() == 1) {
				rfnNumber = "0000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 2) {
				rfnNumber = "000";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 3) {
				rfnNumber = "00";
				gNumber = prefix.concat(rfnNumber.concat(rfnNo));
			} else if (rfnNo.length() == 5) {
				rfnNumber = "0";
				gNumber = prefix.concat(rfnNumber.concat((rfnNo)));
			} else if (rfnNo.length() == 4) {
				gNumber = prefix.concat(rfnNo);
			}*/
			gNumber = prefix.concat(rfnNo);
		}
		return gNumber;
	}

	public StuVO insertStudentDetails(StuVO stuVO) throws Exception 
	{
		StuVO Stuvo = new StuVO();
		
		System.out.println("******* ServiceIMPL Start *******");
		System.out.println("fname  "+stuVO.getFname());
		System.out.println("lname  "+stuVO.getLname());
		System.out.println("photo  "+stuVO.getPhoto());
		System.out.println("dob  "+stuVO.getDob());
		System.out.println("******* ServiceIMPL end *******");
		
		try{
			Stuvo = studentDao.insertStudentDetails(stuVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Stuvo;
	}
	
	public ParentVO insertParentDetails(ParentVO parentVO) throws Exception 
	{
		ParentVO Parentvo = new ParentVO();
		
		System.out.println("******* ServiceIMPL Start *******");
		System.out.println("fname  "+parentVO.getPrt_fname());
		System.out.println("mname  "+parentVO.getPrt_mname());
		System.out.println("fdob  "+parentVO.getPrt_father_dob());
		System.out.println("mdob  "+parentVO.getPrt_mother_dob());
		System.out.println("******* ServiceIMPL end *******");
		
		try{
			Parentvo = studentDao.insertParentDetails(parentVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Parentvo;
	}
	
	public MedicalVO insertMedicalDetails(MedicalVO medicalVO) throws Exception 
	{
		MedicalVO Medicalvo = new MedicalVO();
		
		/*System.out.println("******* ServiceIMPL Start *******");
		System.out.println("fname  "+Parentvo.getPrt_fname());
		System.out.println("mname  "+Parentvo.getPrt_mname());
		System.out.println("fdob  "+Parentvo.getPrt_father_dob());
		System.out.println("mdob  "+Parentvo.getPrt_mother_dob());
		System.out.println("******* ServiceIMPL end *******");*/
		
		try{
			Medicalvo = studentDao.insertMedicalDetails(medicalVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Medicalvo;
	}
	
	public StuVO insertFinalDetails(StuVO stuVO) throws Exception 
	{
		StuVO Stuvo = new StuVO();
		try{
			Stuvo = studentDao.insertFinalDetails(stuVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Stuvo;
	}
	
	//updateCounter
	public CounterVO updateCounter(CounterVO counterVO) throws Exception 
	{
		CounterVO Countervo = new CounterVO();
		try{
			Countervo = studentDao.updateCounter(counterVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Countervo;
	}
	//insertExamTypeMaster
	public ExamTypeMasterVO insertExamTypeMaster(ExamTypeMasterVO examtypeVO)
	{
		ExamTypeMasterVO Examtypevo=new ExamTypeMasterVO();
		try{
			Examtypevo=studentDao.insertExamTypeMaster(examtypeVO);
		}catch(Exception e) {
			System.out.println("DB Exception: "+e.getMessage());
		} 
		return Examtypevo;
	}
	
	
	//updateRoomAllot
	public StuVO updateRoomAllot(StuVO stuVO) throws Exception 
	{
		StuVO Stuvo = new StuVO();
		
		
		
		try{
			Stuvo = studentDao.updateRoomAllot(stuVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Stuvo;
	}
	//updateRollNumber
	public StuVO updateRollNumber(StuVO stuVO) throws Exception 
	{
		StuVO Stuvo = new StuVO();
		try{
			Stuvo = studentDao.updateRollNumber(stuVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Stuvo;
	}
	
	//insertAttendDetails
	public AttendanceVO insertAttendDetails(AttendanceVO attendVO) throws Exception 
	{
		AttendanceVO Attendvo = new AttendanceVO();
		try{
			Attendvo = studentDao.insertAttendDetails(attendVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Attendvo;
	}
	//insertMarks
	public MarksVO insertMarks(MarksVO marksVO) throws Exception 
	{
		MarksVO Marksvo = new MarksVO();
		try{
			Marksvo = studentDao.insertMarks(marksVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Marksvo;
	}
	
	
	public AttendanceVO checkAttendance(AttendanceVO attendVO) throws Exception {	
		System.out.println("ServiceImpl "+attendVO.getAclass()+"----"+attendVO.getAsection()+"----"+attendVO.getAtt_time()+"----");
		AttendanceVO Attendvo = null;
		try {			
			Attendvo =  studentDao.checkAttendance(attendVO);
		} catch (Exception e) {
		}		
		return Attendvo;	
	}
	
	public StuVO deleteStudentsDetails(StuVO stuVO) throws Exception 
	{
		System.out.println("delete service impl  "+stuVO.getStudent_id());
		StuVO Stuvo = new StuVO();
		try{
			Stuvo = studentDao.deleteStudentsDetails(stuVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Stuvo;
	}
	
	public StuVO getLtrReferenceData() throws Exception //(String studid) 
	{
		StuVO Stuvo = new StuVO();
		try{
			Stuvo.setStudentServiceVOList(studentDao.getLtrReferenceData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}
	//getTransportList
	public StuVO getTransportList() throws Exception //(String studid) 
	{
		StuVO Stuvo = new StuVO();
		try{
			Stuvo.setStudentServiceVOList(studentDao.getTransportList());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}
	
	//@Override
	public StuMedicalVO getLtrReferenceDataAll(String student_id) throws Exception  
	{
		System.out.println("enter Serviceimpl 2 "+student_id);
		 //return studentDao.getLtrReferenceDataAll(studid);
		StuMedicalVO StuMedicalVO = new StuMedicalVO();
			try{
				StuMedicalVO.setSuMedicalServiceVOList(studentDao.getLtrReferenceDataAll(student_id));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return StuMedicalVO;
		
	}	
	
	
	//@Override
		public StuVO getstudentdata(String stuid) throws Exception  
		{
			System.out.println("enter Serviceimpl 2 "+stuid);
			 //return studentDao.getLtrReferenceDataAll(studid);
			StuVO StuVO = new StuVO();
				try{
					StuVO.setStudentServiceVOList(studentDao.getstudentdata(stuid));
					
				}catch(Exception e) {
					System.out.println("Error  "+e);
				} 
				return StuVO;
			
		}	
	
	
	//@Override
	public MedicalVO getMedical(String studid) throws Exception  
	{
		System.out.println("enter Serviceimpl 2 "+studid);
		 //return studentDao.getLtrReferenceDataAll(studid);
		 MedicalVO MedicalVO = new MedicalVO();
			try{
				MedicalVO.setStudentMedicalList(studentDao.getMedical(studid));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return MedicalVO;
		
	}	 
	//getStudentLoginID
	public StuVO getStudentLoginID(String email) throws Exception  
	{
		 StuVO Stuvo = new StuVO();
			try{
				Stuvo.setStudentServiceVOList(studentDao.getStudentLoginID(email));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Stuvo;
	}
	//getParentLoginID
	public ParentVO getParentLoginID(String mobile) throws Exception  
	{
		ParentVO Stuvo = new ParentVO();
			try{
				Stuvo.setParentServiceVOList(studentDao.getParentLoginID(mobile));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Stuvo;
	}
	
	public StuVO getParentDataAll(String parid) throws Exception  
	{
		/*System.out.println("enter Serviceimpl  "+parid);
		 return studentDao.getParentDataAll(parid);*/
		System.out.println("enter Serviceimpl PARENT  "+parid);
		
		StuVO Stuvo = new StuVO();
		try{
			Stuvo.setStudentServiceVOList(studentDao.getParentDataAll(parid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}	 
	//getParentid
	public StuVO getParentid(String mobile) throws Exception  
	{
		/*System.out.println("enter Serviceimpl  "+parid);
		 return studentDao.getParentDataAll(parid);*/
		System.out.println("enter Serviceimpl PARENT  "+mobile);
		
		StuVO Stuvo = new StuVO();
		try{
			Stuvo.setStudentServiceVOList(studentDao.getParentid(mobile));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}	 
	
	
	
	public StuParVO getStuParDataAll(String parent_id) throws Exception 
	{
		System.out.println("enter Serviceimpl ---->%%%% "+parent_id);
		StuParVO StuParVO = new StuParVO();
		try{
			StuParVO.setStudentServiceVOList(studentDao.getStuParDataAll(parent_id));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		//System.out.println("enter Serviceimpl RETURN  "+Parvo.getParent_id());
		return StuParVO;
		
		
		//System.out.println("enter Serviceimpl  "+studid);
		 //return studentDao.getStuParDataAll(studid);
	}
	
	public ParentVO parentList(String parent_id) throws Exception 
	{
		System.out.println("enter Serviceimpl ---->%%%% "+parent_id);
		ParentVO parentVO = new ParentVO();
		try{
			parentVO.setParentServiceVOList(studentDao.parentList(parent_id));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		//System.out.println("enter Serviceimpl RETURN  "+Parvo.getParent_id());
		return parentVO;
		
		
		//System.out.println("enter Serviceimpl  "+studid);
		 //return studentDao.getStuParDataAll(studid);
	}

	public StuVO getStudentClassAll(String sclass, String sec) throws Exception 
	{
		//return studentDao.getStudentClassAll(sclass,sec);
		StuVO Stuvo = new StuVO();
		try{
			Stuvo.setStudentServiceVOList(studentDao.getStudentClassAll(sclass,sec));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}
	
	//totalStudent
	public StuVO totalStudent(String sclass, String sec) throws Exception 
	{
		//return studentDao.getStudentClassAll(sclass,sec);
		StuVO Stuvo = new StuVO();
		try{
			Stuvo.setStudentServiceVOList(studentDao.totalStudent(sclass,sec));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		
		System.out.println("Total  "+Stuvo.getTotal());
		return Stuvo;
	}
	
	//getStuAttendance
	public AttendanceVO getStuAttendance(String fdate,String tdate,String atime,String sid) throws Exception 
	{
		System.out.println("enter service impl  "+atime+" fdate  "+fdate+" tdate "+tdate+" studentid "+sid);
		AttendanceVO Attvo = new AttendanceVO();
		try{
			Attvo.setAttendanceServiceVOList(studentDao.getStuAttendance(fdate,tdate,atime,sid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Attvo;
	}
	
	//getStuAttendanceDayAll
		public AttendanceDayVO getStuAttendanceDayAll(String mon,String monyear,String sclass,String sec) throws Exception 
		{
			System.out.println("enter service impl  "+mon);
			AttendanceDayVO Attvo = new AttendanceDayVO();
			try{
				Attvo.setAttendanceDayServiceVOList(studentDao.getStuAttendanceDayAll(mon,monyear,sclass,sec));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Attvo;
		}
	//getTeacherAttendanceDayAll	
		public AttendanceDayVO getTeacherAttendanceDayAll(String fdate,String monyear) throws Exception 
		{
			System.out.println("enter service impl  ");
			AttendanceDayVO Attvo = new AttendanceDayVO();
			try{
				Attvo.setAttendanceDayServiceVOList(studentDao.getTeacherAttendanceDayAll(fdate,monyear));
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Attvo;
		}
	
	//countAttendanceMonth	
	public AttendanceVO countAttendanceMonth(String sclass,String sec,String adate) throws Exception 
	{
		AttendanceVO Attvo = new AttendanceVO();
		try{
			Attvo.setAttendanceServiceVOList(studentDao.countAttendanceMonth(sclass,sec,adate));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Attvo;
	}
	//showCount
	public DummyVO showCount() throws Exception 
	{
		DummyVO Attvo = new DummyVO();
		try{
			Attvo.setDummyServiceVOList(studentDao.showCount());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Attvo;
	}
	
	
	//getStuAttendanceAll
	public AttendanceVO getStuAttendanceAll(String fdate,String aclass,String asec) throws Exception 
	{
		System.out.println("enter service impl  "+aclass+" fdate  "+fdate+" studentid "+asec);
		AttendanceVO Attvo = new AttendanceVO();
		try{
			Attvo.setAttendanceServiceVOList(studentDao.getStuAttendanceAll(fdate,aclass,asec));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Attvo;
	}
	
	//getStuAttendanceByDate
	public AttendanceVO getStuAttendanceByDate(String fdate,String aclass,String asec) throws Exception 
	{
		System.out.println("enter service impl  "+aclass+" fdate  "+fdate+" studentid "+asec);
		AttendanceVO Attvo = new AttendanceVO();
		try{
			Attvo.setAttendanceServiceVOList(studentDao.getStuAttendanceByDate(fdate,aclass,asec));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Attvo;
	}
	//getStuAttendanceByDateAbsent
	public AttendanceVO getStuAttendanceByDateAbsent(String fdate,String aclass,String asec,String ttype) throws Exception 
	{
		System.out.println("enter service impl  "+aclass+" fdate  "+fdate+" studentid "+asec);
		AttendanceVO Attvo = new AttendanceVO();
		try{
			Attvo.setAttendanceServiceVOList(studentDao.getStuAttendanceByDateAbsent(fdate,aclass,asec,ttype));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Attvo;
	}
	//getStuAttendanceByIDAbsent
	public AttendanceVO getStuAttendanceByIDAbsent(String adate,String sid) throws Exception 
	{
		System.out.println("enter service impl --->   "+adate+" sid  "+sid);
		AttendanceVO Attvo = new AttendanceVO();
		try{
			Attvo.setAttendanceServiceVOList(studentDao.getStuAttendanceByIDAbsent(adate,sid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Attvo;
	}
	
	
	
	//countAttendanceToday
	public int countAttendanceToday(String fdate,String aclass,String asec) throws Exception 
	{
		return studentDao.countAttendanceToday(fdate,aclass,asec);
	}
	//countTotalStudentsToday
	public int countTotalStudentsToday(String fdate,String aclass,String asec) throws Exception 
	{
		return studentDao.countTotalStudentsToday(fdate,aclass,asec);
	}
	//topperClass
	public int topperClass(String fdate,String aclass,String asec) throws Exception 
	{
		return studentDao.topperClass(fdate,aclass,asec);
	}
	//topperClassName
	public MarksVO topperClassName(String fdate,String aclass,String asec) throws Exception 
	{
		MarksVO Marksvo = new MarksVO();
		try{
			Marksvo.setMarksServiceVOList(studentDao.topperClassName(fdate,aclass,asec));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Marksvo;
		//return studentDao.topperClassName(fdate,aclass,asec);
	}
	public StuParVO getParentClassAll(String sclass, String sec) throws Exception 
	{
		System.out.println("Enter service impl  Parent sclass  "+sclass);
		StuParVO StuParvo = new StuParVO();
		try{
			StuParvo.setStudentServiceVOList(studentDao.getParentClassAll(sclass,sec));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return StuParvo;
	}

	public StuVO getAttendance(String sclass, String sec) throws Exception 
	{
		System.out.println("Enter service impl  Parent sclass  "+sclass);
		StuVO Stuvo = new StuVO();
		try{
			Stuvo.setStudentServiceVOList(studentDao.getAttendance(sclass,sec));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}
	
	//getLoginDetails
	
	public LoginVO getLoginDetails(String uid, String pass) throws Exception 
	{
		System.out.println("Enter service impl  Parent sclass  "+uid+" pass  :  "+pass);
		LoginVO Stuvo = new LoginVO();
		try{
			Stuvo.setLoginServiceVOList(studentDao.getLoginDetails(uid,pass));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}
	
	
	
	
	
	
	//getHostelList
	public StuVO getHostelList(String gender) throws Exception 
	{
		System.out.println("Enter service impl  Parent sclass  "+gender);
		StuVO Stuvo = new StuVO();
		try{
			Stuvo.setStudentServiceVOList(studentDao.getHostelList(gender));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}
	
	//getTransStudent
	public StuVO getTransStudent(String board)throws Exception
	{
		System.out.println("transport service impl  "+board);
		StuVO Stuvo=new StuVO();
		try
		{
			Stuvo.setStudentServiceVOList(studentDao.getTransStudent(board));
		}catch(Exception e)
		{
			System.out.println("Error  "+e);
		}
		return Stuvo;
	}
	//getDorStudent
	public StuVO getDorStudent(String board)throws Exception
	{
		StuVO Stuvo=new StuVO();
		try
		{
			Stuvo.setStudentServiceVOList(studentDao.getDorStudent(board));
		}catch(Exception e)
		{
			System.out.println("Error  "+e);
		}
		return Stuvo;
	}
	
	public StuVO getEditStudentsData(String studid) throws Exception 
	{
		System.out.println("enter Serviceimpl  ");
		 return studentDao.getEditStudentsData(studid);
	}

	//@Override
	/*public List<StudentVO> getCategories() {
		return studentDao.getCategories();
	}*/
	
	//Edit Students Details
	public StuVO editStudentDetails(StuVO stuVO) throws Exception 
	{
		StuVO Stuvo = new StuVO();
		
		System.out.println("******* EDIT  ServiceIMPL Start *******");
		System.out.println("fname  "+stuVO.getFname());
		System.out.println("lname  "+stuVO.getLname());
		System.out.println("photo  "+stuVO.getPhoto());
		System.out.println("dob  "+stuVO.getDob());
		System.out.println("******* ServiceIMPL end *******");
		
		try{
			Stuvo = studentDao.editStudentDetails(stuVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Stuvo;
	}
	
	public StuVO getAssignment(String sclass, String section) throws Exception {
		System.out.println("Enter service impl  Parent sclass  "+sclass+" and "+section);
		StuVO Stuvo = new StuVO();
		try{
			String sec = null;
			Stuvo.setStudentServiceVOList(studentDao.getAssignment(sclass,section));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}
	
	//getStuNameLike
	public StuVO getStuNameLike(String sclass, String section,String fname) throws Exception {
		System.out.println("Enter service impl  Parent sclass  "+sclass+" and "+section);
		StuVO Stuvo = new StuVO();
		try{
			String sec = null;
			Stuvo.setStudentServiceVOList(studentDao.getStuNameLike(sclass,section,fname));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}
	//getPhoto
	public LoginVO getPhoto(String uid, String pass) throws Exception {
		//System.out.println("Enter service impl  Parent sclass  "+sclass+" and "+section);
		System.out.println("serviceimpl ----------------->   "+uid+"  pass  "+pass);
		LoginVO Stuvo = new LoginVO();
		try{
			String sec = null;
			Stuvo.setLoginServiceVOList(studentDao.getPhoto(uid,pass));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return Stuvo;
	}

	public AssignmentVO insertAssignmentDetails(AssignmentVO assignmentVO) throws Exception 
	{
		AssignmentVO Assignmentvo = new AssignmentVO();
		System.out.println("enter Serviceimpl  ");
		try{
			Assignmentvo = studentDao.insertAssignmentDetails(assignmentVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Assignmentvo;
	}
	//msgCount
	public int msgCount(String mfrom) throws Exception 
	{
		System.out.println("service imple  ");
		return studentDao.msgCount(mfrom);
	}
	
	//countStudents
		public int countStudents() throws Exception 
		{
			System.out.println("service imple  "+studentDao.countStudents());
			return studentDao.countStudents();
		}
	//countPresent
		public int countPresent() throws Exception 
		{
			return studentDao.countPresent();
		}
	//countAbsent
				public int countAbsent() throws Exception 
				{
					return studentDao.countAbsent();
				}
				
	//count Total,Present,Absent list based on class,section and date
				
				//countStudentsClass
				public int countStudentsClass(String sclass,String sec) throws Exception 
				{
					return studentDao.countStudentsClass(sclass,sec);
				}
			//countPresentClass
				public int countPresentClass(String sclass,String sec,String adate) throws Exception 
				{
					return studentDao.countPresentClass(sclass,sec,adate);
				}
			//countAbsentClass
				public int countAbsentClass(String sclass,String sec,String amonth) throws Exception 
				{
					return studentDao.countAbsentClass(sclass,sec,amonth);
				}
			//countAttTeacherMonthPresent	
				public int countAttTeacherMonthPresent(String amonth) throws Exception 
				{
					return studentDao.countAttTeacherMonthPresent(amonth);
				}
				
				public int countAttTeacherMonthAbsent(String amonth) throws Exception 
				{
					return studentDao.countAttTeacherMonthAbsent(amonth);
				}
				
				
			//countPresentMonth
				public int countPresentMonth(String sclass,String sec,String adate) throws Exception 
				{
					return studentDao.countPresentMonth(sclass,sec,adate);
				}
			
				
				
				
			//UserReport
				public int countParents() throws Exception 
				{ 	return studentDao.countParents();  }	
				
				public int countTeachers() throws Exception 
				{ 	return studentDao.countTeachers();  }
				
				public int countPresentTeacher()throws Exception
				{   return studentDao.countPresentTeacher(); }
				
				public int countAbsentTeacher()throws Exception
				{   return studentDao.countAbsentTeacher(); }
				
				public int countLibrary() throws Exception 
				{ 	return studentDao.countLibrary();  }
				
				public int countDormitory() throws Exception 
				{ 	return studentDao.countDormitory();  }
				
				public int countTransport() throws Exception 
				{ 	return studentDao.countTransport();  }
				
				public int countTransportAO() throws Exception 
				{ 	return studentDao.countTransportAO();  }
				
				
				public StuVO getstuid() throws Exception {
					System.out.println("Enter service impl  student id");
					StuVO stidvo = new StuVO();
					try{
						String stid = null;
						stidvo.setStudentServiceVOList(studentDao.getstuid());
						
					}catch(Exception e) {
						System.out.println("Error  "+e);
					} 
					return stidvo;
				}	

				//getTimeTable(String sclass,String sec)
				public TimeTableVO getTimeTable(String sclass,String sec) throws Exception {
					System.out.println("Enter service impl  TimeTable   "+sclass+" and  "+sec);
					TimeTableVO timetablevo = new TimeTableVO();
					try{
						String stid = null;
						timetablevo.setTimeTableServiceVOList(studentDao.getTimeTable(sclass,sec));
						
					}catch(Exception e) {
						System.out.println("Error  "+e);
					} 
					return timetablevo;
				}	
				//getTimeTableDay
				
				public TimeTableVO getTimeTableDay(TimeTableVO timetableVO) throws Exception {
					System.out.println("Enter service impl  TimeTable  DAY ");
					TimeTableVO timetablevo = new TimeTableVO();
					try{
						String stid = null;
						timetablevo.setTimeTableServiceVOList(studentDao.getTimeTableDay(timetableVO));
						
					}catch(Exception e) {
						System.out.println("Error  "+e);
					} 
					return timetablevo;
				}	
				
				//getTimePeriod
				public TimeTableVO getTimePeriod() throws Exception {
					System.out.println("Enter service impl  TimeTable  DAY ");
					TimeTableVO timetablevo = new TimeTableVO();
					try{
						String stid = null;
						timetablevo.setTimeTableServiceVOList(studentDao.getTimePeriod());
						
					}catch(Exception e) {
						System.out.println("Error  "+e);
					} 
					return timetablevo;
				}	
	
				//TimeTableInsert
				public TimeTableVO TimeTableInsert(TimeTableVO timetableVO) throws Exception 
				{
					System.out.println("******* Service impl Start insert *******"+timetableVO.getSclass()+" and  "+timetableVO.getSection());
					TimeTableVO Timetablevo = new TimeTableVO();
					
						
					try{
						Timetablevo = studentDao.TimeTableInsert(timetableVO);
						
					}catch(Exception e) {
						throw new Exception("DB Exception: "+e.getMessage());
					} 
					return Timetablevo;
				}
				public StuParVO getStuParDataAll(String parent_id, String student_id) throws Exception {
					// TODO Auto-generated method stub
					return null;
				}
}
