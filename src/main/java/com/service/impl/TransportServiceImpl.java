package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.StudentDao;
import com.dao.TransportDao;
import com.model.TransportAttendanceVO;
import com.model.TransportVO;
import com.service.TransportService;


@Service
public class TransportServiceImpl implements TransportService
{

	@Autowired
	//@Qualifier(value="transportDao")
	private TransportDao transportDao=null;

	public TransportVO insertTransportDetails(TransportVO transportVO) throws Exception
	{
		System.out.println("Enter Transport Service IMPL  ");
		System.out.println("transport_name   "+transportVO.getTransport_name());
		TransportVO Transportvo = new TransportVO();
		try{
			Transportvo = transportDao.insertTransportDetails(transportVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Transportvo;
	}
	
	//insertAttendance
	public TransportAttendanceVO insertAttendance(TransportAttendanceVO attendanceVO)throws Exception
	{
		TransportAttendanceVO Transvo=new TransportAttendanceVO();
		try{
			Transvo=transportDao.insertAttendance(attendanceVO);
		}catch(Exception e)
		{
			System.out.println(e);
		}
		return Transvo;
	}
	
	

public TransportVO gettransportdetail() throws Exception //(String studid) 
{
	//System.out.println("enter Serviceimpl  ");
	 //return studentDao.getLtrReferenceData(studid);
	 
	TransportVO Transvo = new TransportVO();
	try{
		Transvo.setTransportServiceVOList(transportDao.gettransportdetail());
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Transvo;
}

//gettransportall
public TransportVO gettransportdetailall(String transport_name) throws Exception //(String studid) 
{
	//System.out.println("enter Serviceimpl  ");
	 //return studentDao.getLtrReferenceData(studid);
	 
	TransportVO Transvo = new TransportVO();
	try{
		Transvo.setTransportServiceVOList(transportDao.gettransportdetailall(transport_name));
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Transvo;
}


//getDname
public TransportVO getDname(String dname) throws Exception //(String studid) 
{
	//System.out.println("enter Serviceimpl  ");
	 //return studentDao.getLtrReferenceData(studid);
	 
	TransportVO Transvo = new TransportVO();
	try{
		Transvo.setTransportServiceVOList(transportDao.getDname(dname));
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Transvo;
}
//getTransportLoginID
public TransportVO getTransportLoginID(String mobile) throws Exception //(String studid) 
{
	//System.out.println("enter Serviceimpl  ");
	 //return studentDao.getLtrReferenceData(studid);
	 
	TransportVO Transvo = new TransportVO();
	try{
		Transvo.setTransportServiceVOList(transportDao.getTransportLoginID(mobile));
		
	}catch(Exception e) {
		System.out.println("Error  "+e);
	} 
	return Transvo;
}



//getAttendanceHistory
public TransportAttendanceVO getAttendanceHistory(String uname,String adate)throws Exception
{
	TransportAttendanceVO Transvo=new TransportAttendanceVO();
	try{
		Transvo.setTransportAttendanceServiceVOList(transportDao.getAttendanceHistory(uname,adate));
	}catch(Exception e)
	{
		System.out.println(e);
	}
	return Transvo;
}
//getAttendanceHistory
public TransportAttendanceVO gettrandetail(String duser)throws Exception
{
	TransportAttendanceVO Transvo=new TransportAttendanceVO();
	try{
		Transvo.setTransportAttendanceServiceVOList(transportDao.gettrandetail(duser));
	}catch(Exception e)
	{
		System.out.println(e);
	}
	return Transvo;
}
//getAttendanceHistoryAbsent
public TransportAttendanceVO getAttendanceHistoryAbsent(String uname,String adate)throws Exception
{
	TransportAttendanceVO Transvo=new TransportAttendanceVO();
	try{
		Transvo.setTransportAttendanceServiceVOList(transportDao.getAttendanceHistoryAbsent(uname,adate));
	}catch(Exception e)
	{
		System.out.println(e);
	}
	return Transvo;
}



//getTransStudentVOList
public TransportAttendanceVO getTransStudentVOList(String board,String adate)throws Exception
{
	TransportAttendanceVO Transvo=new TransportAttendanceVO();
	try{
		Transvo.setTransportAttendanceServiceVOList(transportDao.getTransStudentVOList(board,adate));
	}catch(Exception e)
	{
		System.out.println(e);
	}
	return Transvo;
}

//getTransStuListBYBoard
public TransportAttendanceVO getTransStuListBYBoard(String board,String adate)throws Exception
{
	TransportAttendanceVO Transvo=new TransportAttendanceVO();
	try{
		Transvo.setTransportAttendanceServiceVOList(transportDao.getTransStuListBYBoard(board,adate));
	}catch(Exception e)
	{
		System.out.println(e);
	}
	return Transvo;
}

//getTransStudentVOListNew
public TransportAttendanceVO getTransStudentVOListNew(String board)throws Exception
{
	TransportAttendanceVO Transvo=new TransportAttendanceVO();
	try{
		Transvo.setTransportAttendanceServiceVOList(transportDao.getTransStudentVOListNew(board));
	}catch(Exception e)
	{
		System.out.println(e);
	}
	return Transvo;
}

public TransportVO gettransportdetailall() throws Exception {
	// TODO Auto-generated method stub
	return null;
}
}
