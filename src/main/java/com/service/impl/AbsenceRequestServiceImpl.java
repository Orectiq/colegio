package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.AbsenceRequestDao;
import com.model.AbsenceRequestVO;
import com.model.AbsenceStuRequestVO;
import com.model.LibraryIssueVO;
import com.model.TeacherVO;
import com.service.AbsenceRequestService;

@Service
public class AbsenceRequestServiceImpl implements AbsenceRequestService
{
	@Autowired
	@Qualifier(value="absenceRequestDao")
	private AbsenceRequestDao absenceRequestDao=null;

	//@Override
	public AbsenceRequestVO insertAbsenceRequestDetails(AbsenceRequestVO absenceRequestVO) throws Exception
	{
		System.out.println(" Service IMPL ------------------------------->?");
		AbsenceRequestVO AbsenceRequestvo = new AbsenceRequestVO();
		try{
			AbsenceRequestvo = absenceRequestDao.insertAbsenceRequestDetails(absenceRequestVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return AbsenceRequestvo;

	}
	//insertStuAbsenceRequest
	public AbsenceStuRequestVO insertStuAbsenceRequest(AbsenceStuRequestVO absenceRequestVO) throws Exception
	{
		
		AbsenceStuRequestVO AbsenceRequestvo = new AbsenceStuRequestVO();
		try{
			AbsenceRequestvo = absenceRequestDao.insertStuAbsenceRequest(absenceRequestVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return AbsenceRequestvo;

	}
	
	public AbsenceRequestVO getLtrReferenceData() throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		AbsenceRequestVO AbsenceRequestvo = new AbsenceRequestVO();
		try{
			AbsenceRequestvo.setAbsenceRequestServiceVOList(absenceRequestDao.getLtrReferenceData());
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return AbsenceRequestvo;
		 
	}
	//getTeacherLeaveRequestbyDate
	public AbsenceRequestVO getTeacherLeaveRequestbyDate(String rdate) throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		AbsenceRequestVO AbsenceRequestvo = new AbsenceRequestVO();
		try{
			AbsenceRequestvo.setAbsenceRequestServiceVOList(absenceRequestDao.getTeacherLeaveRequestbyDate(rdate));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return AbsenceRequestvo;
		 
	}
	//getTeacherLeaveRequestbyDateNotify
	public AbsenceRequestVO getTeacherLeaveRequestbyDateNotify(String rdate,String tid) throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		AbsenceRequestVO AbsenceRequestvo = new AbsenceRequestVO();
		try{
			AbsenceRequestvo.setAbsenceRequestServiceVOList(absenceRequestDao.getTeacherLeaveRequestbyDateNotify(rdate,tid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return AbsenceRequestvo;
		 
	}
	public AbsenceRequestVO getTeacherLeaveRequestbyDateNotify1(String rdate,String tid,String tim) throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		AbsenceRequestVO AbsenceRequestvo = new AbsenceRequestVO();
		try{
			AbsenceRequestvo.setAbsenceRequestServiceVOList(absenceRequestDao.getTeacherLeaveRequestbyDateNotify1(rdate,tid,tim));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return AbsenceRequestvo;
		 
	}
	
	//getStudentLeaveRequestbyDate
	public AbsenceStuRequestVO getStudentLeaveRequestbyDate(String rdate) throws Exception //(String studid) 
	{
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		AbsenceStuRequestVO AbsenceRequestvo = new AbsenceStuRequestVO();
		try{
			AbsenceRequestvo.setAbsenceStuRequestServiceVOList(absenceRequestDao.getStudentLeaveRequestbyDate(rdate));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return AbsenceRequestvo;
		 
	}
	
	//getLeaveData
	public AbsenceRequestVO getLeaveData(String tid) throws Exception //(String studid) 
	{
		System.out.println("enter service IMPL  &&&&&&&&&&& "+tid);
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		AbsenceRequestVO AbsenceRequestvo = new AbsenceRequestVO();
		try{
			AbsenceRequestvo.setAbsenceRequestServiceVOList(absenceRequestDao.getLeaveData(tid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return AbsenceRequestvo;
		 
	}
	//getLeaveGrantbyStudent
	public AbsenceStuRequestVO getLeaveGrantbyStudent(String tid) throws Exception //(String studid) 
	{
		System.out.println("enter service IMPL  &&&&&&&&&&& "+tid);
		//System.out.println("enter Serviceimpl  ");
		 //return studentDao.getLtrReferenceData(studid);
		 
		AbsenceStuRequestVO AbsenceRequestvo = new AbsenceStuRequestVO();
		try{
			AbsenceRequestvo.setAbsenceStuRequestServiceVOList(absenceRequestDao.getLeaveGrantbyStudent(tid));
			
		}catch(Exception e) {
			System.out.println("Error  "+e);
		} 
		return AbsenceRequestvo;
		 
	}
	
	
	//updateLeaveReq
	public AbsenceRequestVO updateLeaveReq(AbsenceRequestVO absenceRequestVO) throws Exception
	{
		System.out.println("Service IMPL Library");
		//System.out.println(absenceRequestVO.getTheDate());
		System.out.println(absenceRequestVO.getTeacher_Id());
		System.out.println(absenceRequestVO.getStatus());
		System.out.println("Service IMPL library end");
		
		
		AbsenceRequestVO Absencevo = new AbsenceRequestVO();
		try{
			Absencevo = absenceRequestDao.updateLeaveReq(absenceRequestVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Absencevo;
	}
	//updateStudentLeaveReq
	public AbsenceStuRequestVO updateStudentLeaveReq(AbsenceStuRequestVO absenceRequestVO) throws Exception
	{
		System.out.println("Service IMPL Library");
		System.out.println(absenceRequestVO.getThedate());
		System.out.println(absenceRequestVO.getStudent_id());
		System.out.println(absenceRequestVO.getStatus());
		System.out.println(absenceRequestVO.getLapply());
		System.out.println(absenceRequestVO.getApproved_by());
		System.out.println("Service IMPL library end");
		
		
		AbsenceStuRequestVO Absencevo = new AbsenceStuRequestVO();
		try{
			Absencevo = absenceRequestDao.updateStudentLeaveReq(absenceRequestVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Absencevo;
	}
	
	
	//updateTeacherMaster
	public TeacherVO updateTeacherMaster(TeacherVO teacherVO) throws Exception
	{
		System.out.println("ServiceIMPL Library");
		System.out.println(teacherVO.getTid());
		System.out.println("controller ServiceIMPL end");
		TeacherVO Teachervo = new TeacherVO();
		try{
			Teachervo = absenceRequestDao.updateTeacherMaster(teacherVO);
			
		}catch(Exception e) {
			throw new Exception("DB Exception: "+e.getMessage());
		} 
		return Teachervo;
	}

}
