package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dao.MediaDao;
import com.model.MediaVO;
import com.model.MediaaddVO;
import com.service.MediaService;





	
	
	@Service
	public class MediaServiceImpl implements MediaService
	{
		@Autowired
		@Qualifier(value="mediaDao")
		private MediaDao mediaDao=null;

		

		//@Override
		public MediaVO insertAlbumDetails(MediaVO mediaVO) throws Exception {
			
			
			MediaVO Mediavo = new MediaVO();
			try{
				Mediavo = mediaDao.insertAlbumDetails(mediaVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Mediavo;
		}



		//@Override
		public MediaaddVO insertMediaall(MediaaddVO mediaaddVO) throws Exception {
			MediaaddVO Mediaaddvo = new MediaaddVO();
			try{
				Mediaaddvo = mediaDao.insertMediaall(mediaaddVO);
				
			}catch(Exception e) {
				throw new Exception("DB Exception: "+e.getMessage());
			} 
			return Mediaaddvo;
		}


		public MediaaddVO getMediaData() throws Exception //(String studid) 
		{
			 
			MediaaddVO Mediavo = new MediaaddVO();
			try{
				Mediavo.setMediaServiceVOList(mediaDao.getMediaData());
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Mediavo;
			 
		}


		public MediaVO getAlbumData() throws Exception //(String studid) 
		{
			 
			MediaVO Mediaaddvo = new MediaVO();
			try{
				Mediaaddvo.setAlbumServiceVOList(mediaDao.getAlbumData());
				
			}catch(Exception e) {
				System.out.println("Error  "+e);
			} 
			return Mediaaddvo;
			 
		}
		
		

	}
