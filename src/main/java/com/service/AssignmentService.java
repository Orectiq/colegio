package com.service;

import com.model.AssignApprovalVO;
import com.model.AssignmentaddVO;

public interface AssignmentService {

	public AssignmentaddVO insertAssignmentDetails(AssignmentaddVO assignmentaddVO) throws Exception;
	//insertAssignApproval
	public AssignApprovalVO insertAssignApproval(AssignApprovalVO assignappVO) throws Exception;
	//updateAssignApproval
	public AssignApprovalVO updateAssignApproval(AssignApprovalVO assignappVO) throws Exception;
	
	public AssignmentaddVO getAssignment(String sclass, String section)throws Exception;
	//getSubject
	public AssignApprovalVO getSubject(String tid)throws Exception;
	//getAssignApprovalTeacher
	public AssignApprovalVO getAssignApprovalTeacher(String adate,String tid,String sclass,String sec,String subj,String sta)throws Exception;
	//getApprovalTType
	public AssignApprovalVO getApprovalTType(String ttype,String tid)throws Exception;
	//getApprovalList   sclass,section,tid,title,ttype
	public AssignApprovalVO getApprovalList(String sclass,String section,String tid,String title,String ttype)throws Exception;
	//getAdminApprovalList
	public AssignApprovalVO getAdminApprovalList(String sclass,String section,String subject)throws Exception;
	
	//getAssignmentTeacher
	public AssignmentaddVO getAssignmentTeacher(String sclass, String section,String tid,String adate,String ttype)throws Exception;
	//getAssignmentTeacherAllot
	public AssignmentaddVO getAssignmentTeacherAllot(String aid)throws Exception;
	
	//getTitle
	public AssignmentaddVO getTitle(String hdate,String subj)throws Exception;
	public AssignmentaddVO getTType(String ttype,String tid)throws Exception;
}
