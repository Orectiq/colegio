package com.service;

import com.model.EchallanStudentVO;
import com.model.EchallanVO;
import com.model.StuVO;

public interface EchallanService {
	
	
	public EchallanVO getChallanId(String ref) throws Exception;
	
	public EchallanVO insertEchallanDetails(EchallanVO echallanVO) throws Exception;
	public EchallanVO sendEchallanDetails(EchallanVO echallanVO) throws Exception;
	
	public EchallanVO getFeeDraft(String sclass)throws Exception;
	public EchallanVO getFee(String sclass)throws Exception;
	//getStudentFee
	public EchallanStudentVO getStudentFee(String sclass)throws Exception;
	
	//getChallanFeeID
	public EchallanVO getChallanFeeID(String cid)throws Exception;
	
	//getChallanFeeID
		public EchallanVO getChallanDraftID(String cid)throws Exception;

}






