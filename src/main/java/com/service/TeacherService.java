package com.service;

import com.model.AbsenceRequestVO;
import com.model.ClassSectionVO;
import com.model.ExamNewVO;
import com.model.GroupVO;
import com.model.LoginVO;
import com.model.MailBackupVO;
import com.model.MailVO;
import com.model.ParentVO;
import com.model.StuVO;
import com.model.SubjectAssignVO;
import com.model.TeacherVO;
import com.model.staffAttendanceVO;
import com.model.teacherAttendanceVO;

public interface TeacherService 
{
	public TeacherVO getTeachId(String ref) throws Exception;
	
	
	public TeacherVO insertTeacherDetails(TeacherVO teacherVO) throws Exception;
	//subjectAssignInsert
	public SubjectAssignVO subjectAssignInsert(SubjectAssignVO subjectassignVO) throws Exception;
	
	//teacherAttenInsert
	public teacherAttendanceVO teacherAttenInsert(teacherAttendanceVO teacherVO) throws Exception;
	//teacherAttenUpdate
	public teacherAttendanceVO teacherAttenUpdate(teacherAttendanceVO teacherVO) throws Exception;
	
	
	//staffAttenInsert
	public staffAttendanceVO staffAttenInsert(staffAttendanceVO teacherVO) throws Exception;
	//composeMail
	public MailVO composeMail(MailVO mailVO) throws Exception;
	//groupMail
	public GroupVO groupMail(GroupVO groupVO) throws Exception;
	
	//deleteMail
	public MailVO deleteMail(MailVO mailVO) throws Exception;
	
	
	//staffAttenUpdate
	public staffAttendanceVO staffAttenUpdate(staffAttendanceVO teacherVO) throws Exception;
	
	
	
	
	
	public TeacherVO getLtrReferenceDataAll(String techid)throws Exception;
	//getClassSectionAll
	public ClassSectionVO getClassSectionAll(String techid)throws Exception;
	
	public TeacherVO getId(String uname)throws Exception;
	//getSClassSection
	public TeacherVO getSClassSection(String ttype,String mobile)throws Exception;
	public TeacherVO getSClassSectionAll(String ttype,String mobile)throws Exception;
	//getCheckin
	public teacherAttendanceVO getCheckin(String adate,String tid)throws Exception;
	//getStaffCheckin
	public staffAttendanceVO getStaffCheckin(String adate,String tid)throws Exception;
	
	//getStaffAttendance
	public teacherAttendanceVO getStaffAttendance(String fdate,String tdate)throws Exception;
	
	public TeacherVO getTeacherSubject(String sclass,String sec)throws Exception;
	//mailInbox
	public MailVO mailInbox(String mailto)throws Exception;
	//getGroupMail
	public GroupVO getGroupMail(String gname)throws Exception;
	
	//mailInboxDate
	public MailVO mailInboxDate(String mailto,String mdate)throws Exception;
	//mailInboxDateTime
	public MailVO mailInboxDateTime(String mailto,String mdate,String mtime)throws Exception;
	//mailSend
	public MailVO mailSend(String mailto)throws Exception;
	//mailTrash
	public MailBackupVO mailTrash(String mto)throws Exception;
	
	
	//getSubjectAssign
	public SubjectAssignVO getSubjectAssign(String sclass,String sec)throws Exception;
	//getSubjectAssignTeacher
	public SubjectAssignVO getSubjectAssignTeacher(String sclass,String sec,String tid)throws Exception;
	//getSubjectAssignTeacherID
	public SubjectAssignVO getSubjectAssignTeacherID(String tid)throws Exception;
	
	
	//getSubjectAssignTeacherType
	public SubjectAssignVO getSubjectAssignTeacherType(String ttype,String mobile)throws Exception;
	//getSubjectAssignTeacherClass
	public SubjectAssignVO getSubjectAssignTeacherClass(String sclass,String ttype,String mobile)throws Exception;
	//getSubjectAssignTeacherClassSection
	public SubjectAssignVO getSubjectAssignTeacherClassSection(String sclass,String section,String ttype,String mobile)throws Exception;


	public TeacherVO getltrreferencedata()throws Exception;


	//getExamData
		public GroupVO getDLlist()throws Exception;
		//getgrplist
		public GroupVO getgroup(String groupname)throws Exception;

		public LoginVO  getlogindetail()throws Exception;
		public LoginVO  getlogindetailAll()throws Exception;
	//getLeaveRequestbyDate
	//public TeacherVO getLeaveRequestbyDate(String uname)throws Exception;
	
	/*public TeacherVO getEditStudentsData(String studid)throws Exception;
	public TeacherVO deleteStudentsDetails(StuVO stuVO) throws Exception;
	public TeacherVO editStudentDetails(StuVO stuVO) throws Exception;*/
}
