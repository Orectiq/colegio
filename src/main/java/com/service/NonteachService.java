package com.service;

import com.model.NonteachVO;
import com.model.TeacherVO;
import com.model.staffAttendanceVO;

public interface NonteachService {
	
	
	//getLibID
	public NonteachVO getLibID(String ref) throws Exception;
	public NonteachVO getDorID(String ref) throws Exception;
	public NonteachVO getTraID(String ref) throws Exception;
	
	//sendMail
	public NonteachVO sendMail(NonteachVO nonteachVO) throws Exception;
	
	public NonteachVO insertNonteachDetails(NonteachVO nonteachVO,String path1) throws Exception;


	public NonteachVO getLtrReferenceData()throws Exception; //(String studid)
	
	public NonteachVO getLtrReferenceDataall()throws Exception;
	
	public NonteachVO getNonteach(String department)throws Exception;
	//getNonTeachingLoginID
	public NonteachVO getNonTeachingLoginID(String mobile)throws Exception;
	
	//getNonTeachIDWithPhone
	public NonteachVO getNonTeachIDWithPhone(String mobile)throws Exception;
	
	//getNonTeachAttendance
	public staffAttendanceVO getNonTeachAttendance(String fdate,String tdate,String tid)throws Exception;
}
