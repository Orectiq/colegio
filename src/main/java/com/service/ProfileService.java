package com.service;

import com.model.LoginVO;

public interface ProfileService 
{
	public LoginVO updateParentsPassword(LoginVO loginVO)throws Exception;
	
	//photoUpdate
		public LoginVO photoUpdate(LoginVO loginVO) throws Exception;
		public LoginVO updateMobilePassword(LoginVO loginVO)throws Exception;
		
}
