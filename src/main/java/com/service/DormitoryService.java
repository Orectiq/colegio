package com.service;

import com.model.DormitoryAllotVO;
import com.model.DormitoryVO;
import com.model.StuVO;

public interface DormitoryService {
	
	public DormitoryVO insertDormitoryDetails(DormitoryVO dormitoryVO) throws Exception;
	//updateRoomAllot
	public DormitoryVO updateRoomAllot(DormitoryVO dormitoryVO) throws Exception;
	
	//insertRoomAllot
	public DormitoryAllotVO insertRoomAllot(DormitoryAllotVO dormitoryVO) throws Exception;
	
	public DormitoryVO getLtrReferenceData()throws Exception; //(String studid)throws Exception;
	//getHostelList
		public DormitoryVO getHostelList(String gender)throws Exception;
	//getNonDormitoryStudent
		public StuVO getNonDormitoryStudent()throws Exception;
		
	//getDormitoryList
		public DormitoryVO getDormitoryList(String dname)throws Exception;
	//getDormitoryAllotList	
		public DormitoryAllotVO getDormitoryAllotList(String dname)throws Exception;
		
	//editHostel
		public StuVO editHostel(StuVO stuvo)throws Exception;
	
	}

