package com.service;

import com.model.SchoolInfoVO;

public interface SuperAdminService 
{
	
	public boolean registerSchool(SchoolInfoVO schoolInfoVO);
	
}
