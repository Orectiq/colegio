
/*==========================================================
	NUMBERS ONLY FOR VALIDATION
============================================================*/

	$('.numbersOnly').keyup(function () {  
		this.value = this.value.replace(/[^0-9\.]/g,''); 
	});
	
/*==========================================================
	TEXT ONLY FOR VALIDATION
============================================================*/

	$('.textOnly').keyup(function () {  
		this.value = this.value.replace(/[^a-zA-Z]/g,''); 
	});
	
/*==========================================================
	EMAIL ONLY FOR VALIDATION
============================================================*/

	function ValidateEmail(email) {
		// Validate email format
		var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		return expr.test(email);
	};
	
/*==========================================================
	EMAIL ONLY FOR VALIDATION
============================================================*/

	/*==========================================================
		STUDENT TAB CHECK
	============================================================*/

	function student_tab() {
		
	////////// TEXT BOX //////////
				
		$(":text.stu_require").each(function(){
			if($(this).val() === "")
			{
				$(this).parent().find('.error').text($(this).attr('data-error'));
				$(this).parent().addClass('err_yes');
			}
			else{
				$(this).parent().find('.error').text('');
				$(this).parent().removeClass('err_yes');
			}
		});
	
	////////// RADIO BUTTON //////////	
	
		$(":radio").each(function(){
			if($(this).hasClass('stu_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text($(':radio[name=' + group + ']').attr('data-error'));
					$(':radio[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':radio[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});
		
	////////// CHECK BOX //////////	
	
		$(":checkbox").each(function(){
			if($(this).hasClass('stu_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text($(':checkbox[name=' + group + ']').attr('data-error'));
					$(':checkbox[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':checkbox[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});	
		
	////////// SELECT OPTION //////////	
	
		$("select").each(function(){
			if($(this).hasClass('stu_require'))
			{
				var group = $(this).attr('name');
				if ($('select[name='+ group +']').val() == 0) {
					$(this).parent().find('.error').text($(this).attr('data-error'));
					$(this).parent().addClass('err_yes');
				}
				else{
					$(this).parent().find('.error').text('');
					$(this).parent().removeClass('err_yes');
				}
			}
		});	
		
	////////// EMAIL //////////	
		
		if (!ValidateEmail($("#cemail").val())) {
            $('#cemail').parent().find('.error').text($('#cemail').attr('data-error'));
			$('#cemail').parent().addClass('err_yes');
            return false;
        }
        else {
			 $('#cemail').parent().find('.error').text('');
			 $('#cemail').parent().removeClass('err_yes');
        }
		
	};
	
	/*==========================================================
		PARENT TAB CHECK
	============================================================*/
	
	function parent_tab() {
		////////// TEXT BOX //////////
				
		$(":text.prt_require").each(function(){
			if($(this).val() === "")
			{
				$(this).parent().find('.error').text($(this).attr('data-error'));
				$(this).parent().addClass('err_yes');
			}
			else{
					
				$(this).parent().find('.error').text('');
				$(this).parent().removeClass('err_yes');
			}
		});
		
	
	////////// RADIO BUTTON //////////	
	
		$(":radio").each(function(){
			if($(this).hasClass('prt_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text($(':radio[name=' + group + ']').attr('data-error'));
					$(':radio[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':radio[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});
		
	////////// CHECK BOX //////////	
	
		$(":checkbox").each(function(){
			if($(this).hasClass('prt_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text($(':checkbox[name=' + group + ']').attr('data-error'));
					$(':checkbox[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':checkbox[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});	
		
	////////// SELECT OPTION //////////	
	
		$("select").each(function(){
			if($(this).hasClass('prt_require'))
			{
				var group = $(this).attr('name');
				if ($('select[name='+ group +']').val() == 0) {
					$(this).parent().find('.error').text($(this).attr('data-error'));
					$(this).parent().addClass('err_yes');
				}
				else{
					$(this).parent().find('.error').text('');
					$(this).parent().removeClass('err_yes');
				}
			}
		});

	////////// EMAIL //////////	
		
		if (!ValidateEmail($("#prt_email").val())) {
            $('#prt_email').parent().find('.error').text($('#prt_email').attr('data-error'));
			$('#prt_email').parent().addClass('err_yes');
            return false;
        }
        else {
			 $('#prt_email').parent().find('.error').text('');
			 $('#prt_email').parent().removeClass('err_yes');
        }

	
	};
	
	
	
	
	
	
	
	/*==========================================================
		STUDENT MEDICAL TAB CHECK
	============================================================*/
	
	function student_medical_tab() {
	
	////////// TEXT BOX //////////
				
		$(":text.med_require").each(function() {
			if($(this).val() === "")
			{
				$(this).parent().find('.error').text($(this).attr('data-error'));
				$(this).parent().addClass('err_yes');
			}
			else{
				$(this).parent().find('.error').text('');
				$(this).parent().removeClass('err_yes');
			}
		});
	
	////////// RADIO BUTTON //////////	
	
		$(":radio").each(function(){
			if($(this).hasClass('med_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text($(':radio[name=' + group + ']').attr('data-error'));
					$(':radio[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':radio[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});
		
	////////// CHECK BOX //////////	
	
		$(":checkbox").each(function(){
			if($(this).hasClass('med_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text($(':checkbox[name=' + group + ']').attr('data-error'));
					$(':checkbox[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':checkbox[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});	
		
	////////// SELECT OPTION //////////	
	
		$("select").each(function(){
			if($(this).hasClass('med_require'))
			{
				var group = $(this).attr('name');
				if ($('select[name='+ group +']').val() == 0) {
					$(this).parent().find('.error').text($(this).attr('data-error'));
					$(this).parent().addClass('err_yes');
				}
				else{
					$(this).parent().find('.error').text('');
					$(this).parent().removeClass('err_yes');
				}
			}
		});

	};

/*==========================================================
	ONCLICK DATE LABEL ANIMATION
============================================================*/

	$( document ).ready(function() {
		
		$('#dob, #dob_root').click(function(){
			$(this).parent().find('label').addClass('active');
			if($('#dob').val() == ""){
				$(this).parent().find('label').removeClass('active');
				$('#dob.ad_date').css("border-color","#9e9e9e");
				$('#dob.ad_date').css("box-shadow", "none");
			}
		});
		
		$('#prd_from, #prd_from_root').click(function(){
			$(this).parent().find('label').addClass('active');
			if($('#prd_from').val() == ""){
				$(this).parent().find('label').removeClass('active');
				$('#prd_from.ad_date').css("border-color","#9e9e9e");
				$('#prd_from.ad_date').css("box-shadow", "none");
			}
		});
		
		$('#prd_to, #prd_to_root').click(function(){
			$(this).parent().find('label').addClass('active');
			if($('#prd_to').val() == ""){
				$(this).parent().find('label').removeClass('active');
				$('#prd_to.ad_date').css("border-color","#9e9e9e");
				$('#prd_to.ad_date').css("box-shadow", "none");
			}
		});
		
		$('.allergy_box').hide();
		$('input:radio[name="allergy_select"]').change(function() {
			$(':radio[name="allergy_select"]').parent().next('.input-field').find('.error').text('');
			$(':radio[name="allergy_select"]').parent().next('.input-field').removeClass('err_yes');
			
			if ($(this).val() == 'Yes') {
				$('.allergy_box').show(function(){
					if($('#type_alg').val() === ""){
						$(this).parent().find('.input-field .error2').text($('#type_alg').attr('data-error'));
						$(this).parent().addClass('err_yes');
					}
					else{
						$(this).parent().find('.input-field .error2').text('');
						$(this).parent().removeClass('err_yes');
					}
				});
			} else {
				$('.allergy_box').parent().find('.input-field .error2').text('');
				$('.allergy_box').parent().removeClass('err_yes');
				$('.allergy_box').hide();
			}
		});
		
		$('.special_child_box').hide();
		$('input:radio[name="sp_child_select"]').change(function() {
			$(':radio[name="sp_child_select"]').parent().next('.input-field').find('.error').text('');
			$(':radio[name="sp_child_select"]').parent().next('.input-field').removeClass('err_yes');
			
			if ($(this).val() == 'Yes') {
				$('.special_child_box').show(function(){
					if($('#type_sp_child').val() === ""){
						$(this).parent().find('.input-field .error2').text($('#type_sp_child').attr('data-error'));
						$(this).parent().addClass('err_yes');
					}
					else{
						$(this).parent().find('.input-field .error2').text('');
						$(this).parent().removeClass('err_yes');
					}
				});
			} else {
				$('.special_child_box').parent().find('.input-field .error2').text('');
				$('.special_child_box').parent().removeClass('err_yes');
				$('.special_child_box').hide();
			}
			
		});
		
		$('.disease_box').hide();
		$('input:radio[name="disease_select"]').change(function() {
			$(':radio[name="disease_select"]').parent().next('.input-field').find('.error').text('');
			$(':radio[name="disease_select"]').parent().next('.input-field').removeClass('err_yes');
			if ($(this).val() == 'Yes') {
				$('.disease_box').show(function(){
					if($('#type_disease').val() === ""){
						//alert($('#type_disease').attr('data-error'));
						$(this).parent().find('.input-field .error2').text($('#type_disease').attr('data-error'));
						$(this).parent().addClass('err_yes');
					}
					else{
						$(this).parent().find('.input-field .error2').text('');
						$(this).parent().removeClass('err_yes');
					}
				});
			} else {
				$('.disease_box').parent().find('.input-field .error2').text('');
				$('.disease_box').parent().removeClass('err_yes');
				$('.disease_box').hide();
			}
			
		});
		
/*==========================================================
	FORM VALIDATION
============================================================*/

	//////////////////// NEXT1 VALIDATION CHECKING ////////////////////	

	$('#Next1').click( function() { 
	
		student_tab();
		
		if ($('.err_yes').length > 0) { 
			return false;
		}
		else{
			$('#ad-tab2 a').click();
			return true;
		}
	
	});
	
	//////////////////// NEXT2 VALIDATION CHECKING ////////////////////	
	
	$('#Next2').click( function() { 

		parent_tab();
		
		if ($('.err_yes').length > 0) { 
			return false;
		}
		else{
			$('#ad-tab3 a').click();
			return true;
		}
	});
	
	$('#Prev2').click( function() { 
		$('#ad-tab1 a').click();
		return true;
	});
	
	//////////////////// NEXT3 VALIDATION CHECKING ////////////////////	
	
	$('#Next3').click( function() { 
		
		student_medical_tab();
		
		if ($('.err_yes').length > 0) { 
			return false;
		}
		else{
			$('#ad-tab4 a').click();
			return true;
		}
	});
	
	$('#Prev3').click( function() { 
		$('#ad-tab2 a').click();
		return true;
	});
	
});
////////script for parrent tab second + button///////
function parent_tab2() {
		////////// TEXT BOX //////////
				
		$(":text.validate").each(function(){
			if($(this).val() === "")
			{
				$(this).parent().find('.error').text($(this).attr('data-error'));
				$(this).parent().addClass('err_yes');
			}
			else{
					
				$(this).parent().find('.error').text('');
				$(this).parent().removeClass('err_yes');
			}
		});
	};
	
	 $("student_prof_pop_01").click(function(){
        alert("The paragraph was clicked.");
    });
	
	
 



