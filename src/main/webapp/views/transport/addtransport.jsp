<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Teachers Main Page - Colegio - A Orectiq Product</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
    <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<style>
  .class_active{display:block!important;opacity:1!important;}
  .teacher_class{display:none;}
  .teacher_class_section{display:none;}
  .teacher_subject{display:none;}
  .student_class_section_sbt{display:none;}
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
				        <li class="bold"><a href='<%=request.getContextPath()%>/app/all/DormitoryMain?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Hostel</a>
                        </li>
                        
                        
                      <li class="bold"><a href='<%=request.getContextPath()%>/app/all/TransportList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Transport</a>
                        </li>
                        
                      <li class="bold"><a href='<%=request.getContextPath()%>/app/all/leaveapproval?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Leave Approval</a>
                        </li>
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/all/StudyMaterialsAdd?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Study Materials</a>
                        </li>
                        
                        <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> HR</a>
                        </li>  
                       <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Notes</a>
                        </li>           
                            
<%--                         <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Chart/PieChart' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Pie Chart</a>
                        </li>
 --%>                                            
                     <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Analyties/Report</a>
                        </li>
                        
                          <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Admin Setting </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/ClassSection' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> New Section</a></li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=subject/subjectlist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> New Subject</a></li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Setting/AdminSetting' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Admin Setting</a></li>
                                </ul>
                            </div>
                 		</li>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Transport-Add Transport</h5>
               
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

        <!--start container-->
        <div class="container">
          <div class="section">
          <div class="col s12 m8 l9 right">
                      
                    </div>
          <!-- Form Start -->
         <!--  <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4 z-depth-6 std_cls_sec_selection">
              <div class="card-panel">
                <!<h4 class="header2">Form Advance</h4>
                <div class="row">
                  <form class="col s12">
                    <div class="row">
                      <div class="input-field col s12 student_class">
                        <select>
                          <option value="" disabled selected>Choose your Class</option>
                          <option value="1">1st Standard</option>
                          <option value="2">2nd Standard</option>
                          <option value="3">3rd Standard</option>
                        </select>
                        <label>Select Your Class</label>
                      </div>         
                      <div class="input-field col s12 student_class_section">
                        <select>
                          <option value="" disabled selected>Choose your Section</option>
                          <option value="1">Section 1</option>
                          <option value="2">Section 2</option>
                          <option value="3">Section 3</option>
                        </select>
                        <label>Select Your Section</label>
                      </div>
                      <div class="input-field col s12 student_class_section_sbt">
                        <button class="btn waves-effect waves-light right submit" type="submit" name="action">Submit
                          <i class="mdi-content-send right"></i>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>-->
              <div class="row">
               
 <div class="container">                  
            
<formclass="formValidate"id="formValidate"method="post">

 
                  
                  <div class="input-field col m12">
					 <label>Transport Name:</label>
                     <input type="text" name="transport_name"  id="transport_name" class="input-field col m12">
                  </div>
  
  </div>
  </div>
   </div>
   <div class="row">
                
                <div class="col m6">
                   <div class="input-field col m6" >
                    <select name ="transport_type" id="transport_type">
                      <option value="" disabled selected>Transport Type</option>
                      <option value="Car">Car</option>
                      <option value="Bus">Bus</option>
                      <option value="Van">Van</option>
                    </select>
                    
					</div>
					 </div>
					 
					 <div class="input-field col m6 left">
					 <label>Registration Number:</label>
                     <input type="text" name="registration_number" id="registration_number" class="input-field col m6">
                     </div>
                
             
			   </div>
			   
            
 
                   <div class="input-field col m12">
					 <label>Description:</label>
                     <input type="text" name="description" id="description" class="input-field col m6">
                     </div>
  
  
 
   
  
                
              <div class="input-field col m12">
					 <label>No Of Seats</label>
                     <input type="text" name="no_seats" id="no_seats" class="input-field col m6">
                     </div>
					 
				<div class="input-field col m12">
					 <label>Fee</label>
                     <input type="text" name="fee" id="fee" class="input-field col m6">
                     </div>
				
				  
				  
  
  
             <div class="input-field col m12">
					 <label for="Section1"></label>
										<select class="browser-default stu_require" id="board" name="board" data-error="Please select your Section" onChange="showList()">
											<option value="NIL">Please select your Boarding Point</option>
											<option value="Vellachery">Vellachery</option>
											<option value="Thuraipakkam">Thuraipakkam</option>
											<option value="TNagar">TNagar</option>
											<option value="Anna_Nagar">Anna Nagar</option>
											<option value="Thambaram">Thambaram</option>
											
										</select>
                     </div>
                     
             <div class="input-field col m12">
					 <!-- <label>Route Number</label> -->
                     Route Number : <input type="text" name="route" id="route" class="input-field col m6">
                     </div>
                     
             <div class="input-field col m12">
					 <label>Area Covering</label>
                     <input type="text" name="area_cover" id="area_cover" class="input-field col m6">
                     </div>         
  
  </div>
  <div></div><h6 class="breadcrumbs-title">Driver</h5></h6><div>
  </br>
  <div class="container">   
		<form class="col m4">
		<img class="materialboxed"  src="Photo.jpg" name="dri_photo" id="dri_photo">
		    <label for="deputy_warden">Driver</label>
		
		<div class="row">
        <div class="input-field col m6">
          <input id="dri_name" type="text" class="validate">
          <label for="dri_name">Driver Name</label>
        </div>
        <div class="input-field col m6">
          <input id="dri_age" type="text" class="validate">
          <label for="dri_age">age</label>
        
		</div>
		<div class="input-field col m4">
          <input id="dri_contact_number" type="text" class="validate">
          <label for="dri_contact_number">Contact Number</label>
        
		</div>
		<div class="input-field col m4">
          <input id="dri_alter_contact_number" type="text" class="validate">
          <label for="dri_alter_contact_number">Alternate Contact Number</label>
        </div>
		<div class="input-field col m4">
          <input id="dri_address" type="text" class="validate">
          <label for="dri_address">Address</label>
        </div>
		</div>
	    </div>

	<div></div>	<h6 class="breadcrumbs-title">Conductor</h5></h6><div>
		<div class="container">   
		<form class="col m4">
		<img class="materialboxed"  src="Photo.jpg" name="cond_photo" id="cond_photo">
		    <label for="deputy_warden">conductor</label>
		
		<div class="row">
        <div class="input-field col m6">
          <input id="cond_name" type="text" class="validate">
          <label for="cond_name"> conductor Name</label>
        </div>
        <div class="input-field col m6">
          <input id="cond_age" type="text" class="validate">
          <label for="cond_age">age</label>
        
		</div>
		<div class="input-field col m4">
          <input id="cond_contact_number" type="text" class="validate">
          <label for="cond_contact_number">Contact Number</label>
        
		</div>
		<div class="input-field col m4">
          <input id="cond_alter_contact_number" type="text" class="validate">
          <label for="cond_alter_contact_number">Alternate Contact Number</label>
        </div>
		<div class="input-field col m4">
          <input id="cond_address" type="text" class="validate">
          <label for="cond_address">Address</label>
          
      
          
        <br><br>
        </div>
       
		</div>


								<div class="row">
									<div class="input-field col m6">
										<input id="uname" type="text" class="validate"> <label
											for="uname">User Name</label>
									</div>
										<div class="input-field col m6">
											<input id="pass" type="text" class="validate"> <label
												for="pass">Password</label>
										</div>
								</div>
								<a href="javascript:void(0)"
											class="btn waves-effect waves-light right"
											onClick="transportInsert()" name="transport" id="transport">Save</a>
								<br>
<!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://geekslabs.com/">Orectiq</a></span>
        </div>
    </div>
  </footer>
  <!-- END FOOTER -->
</div> 
</div>
</div>
</form>

				
				
				</div>
				</div>
				</div>
          <!-- Form End -->
          <!-- Floating Action Button -->
            <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                <a class="btn-floating btn-large">
                  <i class="mdi-action-stars"></i>
                </a>
                <ul>
                  <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                  <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                  <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                  <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
                </ul>
            </div>
            <!-- Floating Action Button -->
        </div>
        <!--end container-->
      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START RIGHT SIDEBAR NAV-->
      <aside id="right-sidebar-nav">
        <ul id="chat-out" class="side-nav rightside-navigation">
            <li class="li-hover">
            <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
            <div id="right-search" class="row">
                <form class="col s12">
                    <div class="input-field">
                        <i class="mdi-action-search prefix"></i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Search</label>
                    </div>
                </form>
            </div>
            </li>
            <li class="li-hover">
                <ul class="chat-collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                    <div class="collapsible-body recent-activity">
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">just now</a>
                                <p>Jim Doe Purchased new equipments for zonal office.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Yesterday</a>
                                <p>Your Next flight for USA will be on 15th August 2015.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Last Week</a>
                                <p>Jessy Jay open a new store at S.G Road.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                    <div class="collapsible-body sales-repoart">
                        <div class="sales-repoart-list  chat-out-list row">
                            <div class="col s8">Target Salse</div>
                            <div class="col s4"><span id="sales-line-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Payment Due</div>
                            <div class="col s4"><span id="sales-bar-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Delivery</div>
                            <div class="col s4"><span id="sales-line-2"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Progress</div>
                            <div class="col s4"><span id="sales-bar-2"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                    <div class="collapsible-body favorite-associates">
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Eileen Sideways</p>
                                <p class="place">Los Angeles, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Zaham Sindil</p>
                                <p class="place">San Francisco, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Renov Leongal</p>
                                <p class="place">Cebu City, Philippines</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Weno Carasbong</p>
                                <p>Tokyo, Japan</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Nusja Nawancali</p>
                                <p class="place">Bangkok, Thailand</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </li>
        </ul>
      </aside>
      <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->



  <!-- //////////////////////////////////////////////////////////////////////////// -->

  


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism
    <script type="text/javascript" src="js/prism/prism.js"></script>-->
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
   <script> var ctxPath = "<%=request.getContextPath() %>";</script>
  
   
   
   
   
   
 <script type="text/javascript">
      $(document).ready(function(event){

        if($(window).width()>1024){
          $('section#content').css('height', ($(window).height()) - ($('#breadcrumbs-wrapper .container').height()) - 36);
        }

        $('.student_class .select-dropdown').click(function(event){
            event.preventDefault();
            $(this).parent().find('ul').addClass("class_active");
        });

        $('html, .student_class ul.select-dropdown li').click(function(e) {
            var a = e.target;
              $('.student_class ul.select-dropdown').removeClass("class_active"); 
              if ($(a).parents('.student_class ul.select-dropdown').length === 0)
              {
                $('.student_class ul.select-dropdown').removeClass("class_active"); 
              }
        });

        $('.student_class ul.select-dropdown li').click(function(event){
            event.preventDefault();
             if($('.student_class ul.select-dropdown li').hasClass('active')){
              $('.student_class_section').show();
             }
        });

        $('.student_class_section .select-dropdown').click(function(event){
            event.preventDefault();
            $(this).parent().find('ul').addClass("class_active");
        });

        $('html, .student_class_section ul.select-dropdown li').click(function(e) {
            var a = e.target;
              $('.student_class_section ul.select-dropdown').removeClass("class_active"); 
              if ($(a).parents('.student_class_section ul.select-dropdown').length === 0)
              {
                $('.student_class_section ul.select-dropdown').removeClass("class_active"); 
              }
        });

        $('.student_class_section ul.select-dropdown li').click(function(event){
            event.preventDefault();
             if($('.student_class_section ul.select-dropdown li').hasClass('active')){
              $('.student_class_section_sbt').show();
             }
        });

      });
    </script>
    
     <script>
     
     function showList()
     {
    	var board=$("#board").val();
    	if(board=="Vellachery")
    		{
    			$("#route").val("Route-1");
    		}
    	else if(board=="Thuraipakkam")
    		{
    		$("#route").val("Route-2");
    		}
    	else if(board=="TNagar")
		{
    		$("#route").val("Route-3");
		}
    	else if(board=="Anna_Nagar")
		{
    		$("#route").val("Route-4");
		}
    	else if(board=="Thambaram")
		{
    		$("#route").val("Route-5");
		}
     }
     
	function transportInsert()
	{
		//alert("transport Insert entered111111111111111");
		var dt=new Date();
		
		/* var nat = document.getElementsByName("transport_type");
	    if (nat[0].checked == true) {
	   	 nation=nat[0].value;
	    } else if(nat[1].checked == true) {
	   	 nation=nat[1].value;
	    } */
		var role="Transport";
		$.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/transport/insertTransport.do?',
	        data: {
	        	   "transport_name":$("#transport_name").val(),"transport_type":$("#transport_type").val(),
	        	  "registration_number":$("#registration_number").val(),"description":$("#description").val(),
	        	  "no_seats":$("#no_seats").val(),"fee":$("#fee").val(),"area_cover":$("#area_cover").val(),
	        	  "board_points":$("#board").val(),"dri_photo":$("#dri_photo").val(),
	        	  "dri_name":$("#dri_name").val(),"dri_age":$("#dri_age").val(),
	        	  "dri_contact_number":$("#dri_contact_number").val(),"dri_alter_contact_number":$("#dri_alter_contact_number").val(),
	        	  "dri_address":$("#dri_address").val(),"cond_photo":$("#cond_photo").val(),"cond_name":$("#cond_name").val(),
	        	  "cond_age":$("#cond_age").val(),"cond_contact_number":$("#cond_contact_number").val(),
	        	  "cond_alter_contact_number":$("#cond_alter_contact_number").val(),"cond_address":$("#cond_address").val(), 
	        	  "route_num":$("#route").val(),"uname":$("#uname").val(),"pass":$("#pass").val(),"role":role		  
	        },
	        success: function(data) 
	        {
				if(data=='success')
		 		{
					Materialize.toast("Success",4000);	
		 		}
				else
		 		{
					Materialize.toast(data,4000);	
		 	}  
	        }
		 });
	}

   
   </script>
	
</body>

</html>