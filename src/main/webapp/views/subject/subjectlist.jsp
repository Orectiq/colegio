<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Subject</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
				        <li class="bold"><a href='<%=request.getContextPath()%>/app/all/DormitoryMain?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Hostel</a>
                        </li>
                        
                        
                      <li class="bold"><a href='<%=request.getContextPath()%>/app/all/TransportList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Transport</a>
                        </li>
                        
                      <li class="bold"><a href='<%=request.getContextPath()%>/app/all/leaveapproval?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Leave Approval</a>
                        </li>
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/all/StudyMaterialsAdd?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Study Materials</a>
                        </li>
                        
                        <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> HR</a>
                        </li>  
                       <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Notes</a>
                        </li>           
                            
<%--                         <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Chart/PieChart' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Pie Chart</a>
                        </li>
 --%>                                            
                     <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Analyties/Report</a>
                        </li>
                        
                          <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Admin Setting </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/all/ClassSection?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> New Section</a></li>
                                    <li><a href='<%=request.getContextPath()%>/app/all/subjectlist?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> New Subject</a></li>
                                    <li><a href='<%=request.getContextPath()%>/app/all/AdminSetting?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Admin Setting</a></li>
                                </ul>
                            </div>
                 		</li>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
                    <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Subject List</h5>
               </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

        <!--start container-->
			             
       					                               		   		  
    
	
	  <div class="row"> 






<div class="col m6  "> 
<div class="input-field col m6"> 

<select class="browser-default stu_require" id="Sclass" name="Sclass" data-error="Please select your Class" onChange="getSection()">
											<option value="NIL">Please select your Class</option>
											
										</select>
<!-- <label>Select Class</label>  -->
<label for="SClass"></label>
</div> 
</div> 
<div class="col m6  "> 
<div class="input-field col m6" > 
<select class="browser-default stu_require" id="Section" name="Section" data-error="Please select your Section">
											<option value="NIL">Please select your Section</option>
											
										</select>
<label for="Section"></label>
<!-- <label>Select Section</label>  -->






</div> 
</div> 





<div class="row">

	

<div class="input-field col m4">	
<a a href="javascript:void(0)" class="waves-effect waves-light  btn" onClick="showLoad()" name="search" id="Search">Subject List</a>
 </div>                              

	<div class="input-field col m4">	
<a href='<%=request.getContextPath()%>/app/all/subjectAdd?&admid=<%= sid %>' class="btn waves-effect waves-light"  name="search" id="Search">Add New Subject	</a>
 </div> 									   
</div>



  <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
                  <div class="col s12">
                   
                  </div>
                 <div class="col s12"> 
<div id="stu_list_all" class="col s12  cyan lighten-4"> 
 
		 <div class="container">
          <div class="section">	
<div class="row"> 
<!--DataTables example--> 
<div id="#student-datatables_1"> 
<!--<h4 class="header">DataTables example</h4>--> 
<div class="row"> 
<div class="col s12 m12 l12"> 
<table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0"> 
<thead> 
<tr> 
<th>S.NO</th>
<th>SUBJECT</th> 

</tr> 
</thead> 


<tbody> 

</tbody> 
</table> 
</div> 
</div> 
</div> 
<br> 
<div class="divider"></div> 
      
	</div>
</div>					
	
</div>
</div>	
	</div>					


	</div>
</div>					


</div>
</div>

</section>
</div>
</div>

<!--start container-->
				     
       
			  
  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright Â© 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
     <!-- <script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/jquery-ui.min.js"></script> 
    -->
  <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    
     <script type="text/javascript">
	
	

        /*Show entries on click hide*/

        $(document).ready(function(){
        	var d = new Date();
        	var month = d.getMonth() + 1;
        	var day = d.getDate();
        	var year = d.getYear();
        	var today = (day<10?'0':'')+ day + '/' +(month<10?'0':'')+ month + '/' + year;
        	$('#dt').val(d);
        	
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });                                                                               

        });
		
	/* $('.datepicker').pickadate({ 
selectMonths: true, // Creates a dropdown to control month 
selectYears: 15 // Creates a dropdown of 15 years to control year 
}); 
 */

function selectCheckBox() 
{ 
  var e = document.getElementById("ddlView"); 
var optionSelIndex = e.options[e.selectedIndex].value; 
var optionSelectedText = e.options[e.selectedIndex].text; 
if (optionSelIndex == 0) { 
//alert("Please select a Class"); 
} 

var e = document.getElementById("ddlView1"); 
var optionSelIndex = e.options[e.selectedIndex].value; 
var optionSelectedText = e.options[e.selectedIndex].text; 
if (optionSelIndex == 0) { 
//alert("Please select a Section"); 
} 
if ($("#test21:checked").length > 0) {
    $("#test25, #test29, #test33, #test37").prop('checked', true);
} else if ($("#DRequest-1:checked").length > 0) {
    $("#DRequest-2, #DRequest-3, #DRequest-4").prop('checked', true);
}
}




	</script>	
	
	 
 
<script>
	
//*****   Start   Class and Section Code   ******// 
retrieve();
function retrieve()
{
	 // //alert("enter 1");
	   $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/subject/getSClass.do?',
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						loadDataIntoClass(responseJson);
				});
}
function loadDataIntoClass(responseJson)
{ ////alert("enter 2");
	var tblHtml = "";
	var sclass;
	 var sel = $("#Sclass").empty();
	 $.each(responseJson.sectionServiceVOList, function(index, val)
	 {
		 ////alert("enter 3");
		sclass=val.sclass;
		sel.append('<option value="' + sclass + '">' + sclass + '</option>');
	 }); 
}

function getSection()
{
	 var sclass=$("#Sclass").val();
	   $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/subject/getSection.do?',
			  data: {"sclass":sclass},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						loadDataIntoSection(responseJson);
				});
}
function loadDataIntoSection(responseJson)
{
	var tblHtml = "";
	var sec;
	 var sel = $("#Section").empty();
	 $.each(responseJson.sectionServiceVOList, function(index, val)
	 {
		sec=val.section;
		sel.append('<option value="' + sec + '">' + sec + '</option>');
	 }); 
}

//*****   End   Class and Section Code   ******//
    
    function showLoad()
	{	
    	////alert("enter showload");
    	var sclass=$('#Sclass').val();
		var section=$('#Section').val();
		////alert("class  "+sclass+"  section "+section);
		 $.ajax({
 			  type: "GET",
 			  url: ctxPath+'/app/subject/getSubject.do?',
 			 data: {"sclass":sclass,"section":section},
 			  dataType: 'json',
			}).done(function( responseJson )
					{	
						////alert("before "+responseJson.sclass+" and section  "+responseJson.section);
					    loadDataIntoTable(responseJson);
				});	
	}
	
	
	function loadDataIntoTable(responseJson)
	  {
	 var tblHtml = "";
		
	 	  	 $.each(responseJson.subjectServiceVOList, function(index, val)
	  		{
				var subject=val.subject;
  				tblHtml += '<tr><td style="text-align: left">'+(index+1)+'</td> <td style="text-align: left" id='+subject+' onClick="onClick="getStudentsListAll(id)">'+subject+'</a></td></tr>';
		 });  
	  	 $("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
	  }

   
 </script>


	
									
			
</body>

</html>
													