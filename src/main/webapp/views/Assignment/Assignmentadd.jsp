<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Messages</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->
<%
                        	String tusername=(String)request.getAttribute("techId");
                    		//System.out.println("Value passssssssssssssssssssssss  "+uname1);
                        
                        %>
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                          <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"  data-activates="profile-dropdown"><%= tusername %><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Teacher</p>
                    </div>
                </div>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/homeworkList?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Homework</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/Homework/homeworkListStatus?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Homework Status</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/teacher/absenceRequest?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Absence Request</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/teacher/absenceStuApproval?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Students Leave Approval</a>
                        </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/assignment/assignment?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Assignment</a>
                        </li>        
                 <li class="bold"><a href='<%=request.getContextPath()%>/app/assignment/assignmentApproval?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Assignment Approval</a>
                        </li>       
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
                    <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Add Assignment</h5>
               </div>
            </div>
            
            
          </div>
        </div>
        <!--breadcrumbs end-->
        
 <div id="submit-button" class="section">
                  					<div class="row right">
                    					<div class="col s12 m12 l12">
                      						<a href='<%=request.getContextPath()%>/app/assignment/assignStatus?id=<%= tusername %>' class="btn waves-effect waves-light"  name="search" id="Search">Assignment Status</a> 
                    					</div>
                  					</div>
                				</div>
                				
                				
                				
        <!--start container-->
		 
		 <div class="container">
          <div class="section">		             
   
<form class="formValidate" id="formValidate" method="post">

   <input id="uname" name="uname" type="hidden" size="45" tabindex="5" disabled value=<%= tusername %> />
   <input id="tid" name="tid" type="text" size="45" tabindex="5" disabled />

				

							<div class="row">
								<div class="input-field col s6">
									
								</div>

								<div class="input-field col s6">
									<!-- <div id="submit-button" class="section">
										<div class="row">
											<div class="col s12 m12 l12">
												<a href="javascript:void(0)" class="btn waves-effect waves-light" onClick="showStatus()" name="search" id="Search">Assignment Status</a>
											</div>
										</div>
									</div> -->
								</div>
							</div>


							<div class="row"> 

<div class="col m6  "> 
<div class="input-field col m6"> 

<select class="browser-default stu_require" id="Sclass" name="Sclass" data-error="Please select your Class" onChange="getSection()">
											<option value="NIL">Please select your Class</option>
											
										</select>
<!-- <label>Select Class</label>  -->
<label for="Sclass"></label>
</div> 
</div> 
<div class="col m6  "> 
<div class="input-field col m6" > 
<select class="browser-default stu_require" id="Subject" name="Subject" data-error="Please select your Section">
											<option value="NIL">Please select your Subject</option>
											
											
											
										</select>
<label for="Section"></label>
<!-- <label>Select Section</label>  -->






</div> 
</div> 


</div>

<div class="row">
<div class="col m6  "> 
<div class="input-field col m6" > 
<select class="browser-default stu_require" id="Section" name="Section" data-error="Please select your Section" onChange="showList()">
											<option value="NIL">Please select your Section</option>
											
										</select>
<label for="Section"></label>
<!-- <label>Select Section</label>  -->






</div> 
</div> 


</div>


 
 
                                      <div class="input-field col s12">
										<label for="Title">Title</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="Title" name="Title" type="text" maxlength="20" class="textOnly ass_require" data-error="Please enter your w">
										<div class="error errorTxt1"></div>
									</div>
									
									<div class="input-field col s12">
										<label for="Description">Description</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="Description" name="Description" type="text" maxlength="20" class="textOnly ass_require" data-error="Please enter your w">
										<div class="error errorTxt2"></div>
									</div>
									
									<div class="input-field col m6">
				<label for="Due_Date"></label>Due Date<input
				id="Due_Date" name="Due_Date" type="text"
				maxlength="10" class="datepicker"
				data-error="Please pick your Date">
												
			</div>

              <div class="input-field col s12">
										<label for="Mark">Mark</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="Mark" name="Mark" type="text" maxlength="20" class="textOnly ass_require" data-error="Please enter your w">
										<div class="error errorTxt3"></div>
									</div>
									
									
										<div class="input-field col m6">
				<label for="HDate"></label> Date<input
				id="HDate" name="HDate" type="text"
				maxlength="10" class="datepicker"
				data-error="Please pick your Date">
												
			</div>
 
                        <center><a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="AssignmentInsert()" name="assignment" id="assignment">Save</a>
</center>
						
						</form>
						</div> 
						</div> 
</section>
</div> 
						</div> 
<!--start container-->
				     
       
			  
  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
    <script> var ctxPath = "<%=request.getContextPath() %>";</script>
 
	
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });

        });
		
		
       /* 
        var date = new Date(); 

        var day = date.getDate(); 
        var month = date.getMonth() + 1; 
        var year = date.getFullYear(); 

        if (month < 10) month = "0" + month; 
        if (day < 10) day = "0" + day; 

        var today = year + "-" + month + "-" + day; 


        document.getElementById('theDate').value = today;
		 */
        
		
    </script>
    
    <script>
		var date = new Date();

		var day = date.getDate();
		var month = date.getMonth() + 1;
		var year = date.getFullYear();

		if (month < 10) month = "0" + month;
		if (day < 10) day = "0" + day;

		var today = year + "-" + month + "-" + day;


		document.getElementById('HDate').value = date;	


</script>	
<script>

retrieve();

function loadDataIntoClass(responseJson)
{
	var tblHtml = "";
	var sclass;
	 var sel = $("#Sclass").empty();
	 $.each(responseJson.sectionServiceVOList, function(index, val)
	 {
		sclass=val.sclass;
		sel.append('<option value="' + sclass + '">' + sclass + '</option>');
	 }); 
}

function getSection()
{
	 var sclass=$("#Sclass").val();
	   $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/subject/getSection.do?',
			  data: {"sclass":sclass},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						loadDataIntoSection(responseJson);
				});
}
function loadDataIntoSection(responseJson)
{
	var tblHtml = "";
	var sec;
	 var sel = $("#Section").empty();
	 $.each(responseJson.sectionServiceVOList, function(index, val)
	 {
		sec=val.section;
		sel.append('<option value="' + sec + '">' + sec + '</option>');
	 }); 
}

//*****   End   Class and Section Code   ******//


function showList()
	{	
   	////alert("enter showload");
   	var sclass=$('#Sclass').val();
		var section=$('#Section').val();
		////alert("class  "+sclass+"  section "+section);
		 $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/subject/getSubject.do?',
			 data: {"sclass":sclass,"section":section},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						////alert("before "+responseJson.sclass+" and section  "+responseJson.section);
					    loadDataIntoTable(responseJson);
				});	
	}
	
	
	function loadDataIntoTable(responseJson)
	  {
	 var tblHtml = "";
		//Subject1
			var sub1;
		 	var sel1 = $("#Subject").empty();
		 	
		 	
	 	  	 $.each(responseJson.subjectServiceVOList, function(index, val)
	  		{
	 	  		sub1=val.subject;
		 		sel1.append('<option value="' + sub1 + '">' + sub1 + '</option>');
		 	
		 });  
	  	 
	  }


	
	function retrieve()
	{
		var id=$('#uname').val();
    	////alert("teacher uname in retrieve  "+id);
 	     
 		 $.ajax({
 	  			  type: "GET",
 	  			  url: ctxPath+'/app/teacher/getId.do?',
 	  			  data: {"uname":id},
 	  			  dataType: 'json',
 	  			}).done(function( responseJson )
 	  					{	
 	  						////alert("before   "+responseJson.uname);
 	  						loadDataIntoID(responseJson);
 	  						////alert("after   "+responseJson.uname);
 	  				});
 		 
 		 $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/subject/getSClass.do?',
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						loadDataIntoClass(responseJson);
				});
 		 
	}
	function loadDataIntoID(responseJson)
	   {
		   var id=responseJson.tid;
		   //alert("tercher id  "+id);
		  // loadID(id);
		   $('#tid').val(id);
	   }
	

	function AssignmentInsert()
	{
		//alert("Welcome to assignment insert function");
		
		var dt=new Date();
		var tid= $('#tid').val();
		var tname=$('#uname').val();
		
		$.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/assignment/insertAssignment.do?',
	        data: {"Sclass":$("#Sclass").val(),"Subject":$("#Subject").val(),"Section":$("#Section").val(),"Title":$("#Title").val(),"Description":$("#Description").val(),
	        	   "Due_Date":$("#Due_Date").val(),"Mark":$("#Mark").val(),"HDate":$("#HDate").val(),
	    	       "isActive":'Y',"Inserted_By":$("#loguser1").val(),"Inserted_Date":dt,"Updated_By":$("#loguser1").val(),
	        	   "Updated_Date":dt,"Tid":tid,"fname":tname},
	        	   
	        success: function(data) 
		        {
		       	 if(data=='success')
		       	 {
		       		Materialize.toast("Success",4000);	 
		       	 }
		       	 else 
		       	 {
		       		 Materialize.toast(data,4000);	 
		       	 }  
		        }
			 });
	
	}
	</script>	
							
</body>

</html>
													