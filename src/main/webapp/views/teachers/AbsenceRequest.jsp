
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Student List - Colegio - A Orectiq Product</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
	
	
fieldset, divider {
    border: 1px solid black;
    text-align: left;
}

fieldset, divider{
    border-collapse: collapse;
    width: 100%;
}

fieldset, divider {
    padding: 15px;
}

	
	
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
<%
                        	String sid=(String)request.getAttribute("tid");
                    		System.out.println("Value passssssssssssssssssssssss 2222222222  "+sid);
                        
                        %>
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                          <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"  data-activates="profile-dropdown"><%= sid %><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Teacher</p>
                    </div>
                </div>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/homeworkList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Homework</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/Homework/homeworkListStatus?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Homework Status</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/teacher/absenceRequest?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Absence Request</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/teacher/absenceStuApproval?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Students Leave Approval</a>
                        </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/assignment/assignment?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Assignment</a>
                        </li>        
                 <li class="bold"><a href='<%=request.getContextPath()%>/app/assignment/assignmentApproval?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Assignment Approval</a>
                        </li>       
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->



      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
         
            
            
			
			
			
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Absence Request</h5>
                <ol class="breadcrumbs">
                    <li><a href="index.html">Dashboard</a></li>
					<li class="active">Teachers Login</li>
                    <li class="active">Absence Request</li>
					
				
                </ol>
              </div>
            </div>
          </div>
		
        </div>
        <!--breadcrumbs end-->
		<div class="container">
		<!-- <div id="floatThead" class="cyan lighten-4"> -->
		
				<div class="container">
		<!--<div id="floatThead" class="cyan lighten-4">-->
	
 <!--Preselecting a tab-->
          <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
				
                  <!-- <div class="col s12">
                    <ul class="tabs tab-demo-active z-depth-1 cyan">
                      
						<li class="tab col s4"><a class="white-text waves-effect waves-light active" href="#">Class Name</a>
						</li>					  
					  <li class="tab col s4"><a class="white-text waves-effect waves-light active" href="#">Exam Name</a>
					  </li>
					  <li class="tab col s4"><a class="white-text waves-effect waves-light active" href="#">Date Period</a>
					  </li>	
						
						
						<a class="btn-floating blue btn tooltipped right" data-position="top" data-delay="0" data-tooltip="Edit" href="" data-tooltip-id="cbac35b5-9143-558e-d30f-33697894f2c5"><i class="large mdi-editor-border-color"></i></a>                        
						
						<a class="btn-floating yellow darken-1 btn tooltipped right" data-position="top" data-delay="0" data-tooltip="print" href="" data-tooltip-id="f6c6c8f3-0350-70b2-efc0-795582ec9370"><i class="large mdi-action-print"></i></a>
						
						<a class="btn-floating green btn tooltipped right" data-position="top" data-delay="0" data-tooltip="Export" href="" data-tooltip-id="14a91577-0e9f-2910-cc52-3fe66f537e4b"><i class="large mdi-action-description"></i></a>				                        
                   
				   </ul>
                  </div> -->
				  
				<div class="col m6 right">
				  <a class="waves-effect waves-light  btn">Holiday List</a>
				  <a class="waves-effect waves-light  btn"></i>Leave Policy</a>	
				  <a href='<%=request.getContextPath()%>/app/teacher/absenceTeaHistory?id=<%= sid %>' class="waves-effect waves-light  btn"></i>Leave History</a>			 
				</div>
				
				
				
				<p> <b>Absence Request</b> </p>
				
				
				
				<br>
				
				
				<div class="card-panel">
				<fieldset>
				
				<p> <b>Absence Detail</b> </p>
				
				<div class="divider"></div>
				
  
				<form class="formValidate" id="formValidate" method="post">
				
				
				<input id="techuname" name="techuname" type="hidden" size="45" tabindex="5" disabled value=<%= sid %> />
				
				<div class="row">
				
				<div class="input-field col m6 ">	
				Date <input type="date" id="theDate">
				<div class="error errorTxt1"></div>
				</div>  
				
				
				<div class="input-field col m6">	
				<label for="Teacher_Id"></label>Teacher Id<input
				id="Teacher_Id" name="Teacher_Id" type="text"
				maxlength="20" class="stu_require"
				data-error="Please enter your Id">
				<div class="error errorTxt1"></div>	
				</div>
				
				
				<div class="input-field col m6">	
				<label for="Teacher_Name"></label>Teacher Name<input
				id="Teacher_Name" name="Teacher_Name" type="text"
				maxlength="10" class="stu_require"
				data-error="Please enter your Name">
				<div class="error errorTxt2"></div>	
				</div>
				
				
				<div class="input-field col m6">	
				<label for="Start_Date"></label>Start Date<input
				id="Start_Date" name="Start_Date" type="text"
				maxlength="10" class="datepicker"
				data-error="Please pick your Date">
				<div class="error errorTxt3"></div>	
				</div>
				
				<div class="input-field col m6">
				<label for="End_Date"></label>End Date<input
				id="End_Date" name="End_Date" type="text"
				maxlength="10" class="datepicker"
				data-error="Please pick your Date">
				<div class="error errorTxt4"></div>			
				</div>
				
  
					
				<div class="input-field col m6">
												Absence Name<label for="Absence_Name"></label>
												
																	<select class="browser-default stu_require" id="Absence_Name" name="Absence_Name" data-error="Please select your Leave">
																		<option value="NIL">Please select your Option</option>
																		<option value="Sick Leave">Sick Leave</option>
																		<option value="Personal Leave">Personal Leave</option>
																		<option value="Casual Leave">Casual Leave</option>
																		<option value="Loss Of Pay">Loss Of Pay</option>
																		<option value="Social Science">Social Science</option>
																		
																	</select>
												<div class="error errorTxt5"></div>
				</div>	
				</div>	
				
				
				<br>
				
				<div class="divider"></div>
				
				 <br/>
				 <div class="row">
               <div class="input-field col s12">
                  Comments<textarea id="Comments" width="500" class="Comments" length="500"></textarea>
                  <label for="Comments"></label>
				  <div class="error errorTxt6"></div>
               </div>          
            </div>   
				
				</form>
				</fieldset>
				</div>
				
				<!-- <div class="col m2 right">
				  <a class="waves-effect waves-light  btn">Apply</a>				  
				</div> -->
				<a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="absencerequestInsert()" name="absencerequest" id="absencerequest">Apply</a>
				
				</br>
				
				<div class="row">
				<div class="input-field col m6">
				
				
				<a href="file:///D:/Tasks/new%20project/new-project/views/ABSENCE%20REQUEST/ABSRequestHistory.html" class="brand-logo">View Absence Request History</a><br>
				<a href="file:///D:/Tasks/new%20project/new-project/views/ABSENCE%20REQUEST/ViewABSBalances.html" class="brand-logo">View Absence Balance</a>
				
				</div>	
				
				
				 </div>
			
            </div>
          </div>

        </div>


             
          <!-- Floating Action Button -->
           
            <!-- Floating Action Button -->
        </div>
		  
		  </div>
		  </div>
        <!--end container-->
</div>
      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START RIGHT SIDEBAR NAV-->
      <aside id="right-sidebar-nav">
        <ul id="chat-out" class="side-nav rightside-navigation">
            <li class="li-hover">
            <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
            <div id="right-search" class="row">
                <form class="col s12">
                    <div class="input-field">
                        <i class="mdi-action-search prefix"></i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Search</label>
                    </div>
                </form>
            </div>
            </li>
            <li class="li-hover">
                <ul class="chat-collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                    <div class="collapsible-body recent-activity">
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">just now</a>
                                <p>Jim Doe Purchased new equipments for zonal office.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Yesterday</a>
                                <p>Your Next flight for USA will be on 15th August 2015.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Last Week</a>
                                <p>Jessy Jay open a new store at S.G Road.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                    <div class="collapsible-body sales-repoart">
                        <div class="sales-repoart-list  chat-out-list row">
                            <div class="col s8">Target Salse</div>
                            <div class="col s4"><span id="sales-line-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Payment Due</div>
                            <div class="col s4"><span id="sales-bar-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Delivery</div>
                            <div class="col s4"><span id="sales-line-2"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Progress</div>
                            <div class="col s4"><span id="sales-bar-2"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                    <div class="collapsible-body favorite-associates">
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Eileen Sideways</p>
                                <p class="place">Los Angeles, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Zaham Sindil</p>
                                <p class="place">San Francisco, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Renov Leongal</p>
                                <p class="place">Cebu City, Philippines</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Weno Carasbong</p>
                                <p>Tokyo, Japan</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Nusja Nawancali</p>
                                <p class="place">Bangkok, Thailand</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </li>
        </ul>
      </aside>
      <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->
  <div id="modal4" class="modal modal-fixed-footer green white-text stu_list_pop_box">
            <div class="modal-content">
            <div id="bordered-table">
            
              <h4 class="header left">Student Name</h4>
              <h4 class="header right">Academic Year</h4>
              <hr style="clear:both;"/>
              
              <div class="row">
              
                <div class="col s12 m4 l3">
                  <img title="Avatar" src="../../resources/images/avatar.jpg">
                </div>
                
                <div class="col s12 m8 l9">
                  <table class="bordered">
                        <th class="stu_details center" data-field="id">Student Details</th>
                        <tr>
                        <td>Class:</td>
                      </tr>
                      <tr>
                        <td>Roll Number:</td>
                      </tr>
                </table>
                </div>
                </div>
                      
                      <div class="col s12 m4 l3">
                  <table class="bordered">
                        <tr>
                        <td>Birth Date:</td>
                      </tr>
                      <tr>
                        <td>Gender:</td>
                      </tr>
                      <tr>
                        <td>Nationality:</td>
                      </tr>
                      <tr>
                        <td>Religion:</td>
                      </tr>
                      <tr>
                        <td>Community:</td>
                      </tr>
                      <tr>
                        <td>Place of Birth:</td>
                      </tr>
                      <tr>
                        <td>Mother Tongue:</td>
                      </tr>
                      <tr>
                        <td>Date of Joining:</td>
                      </tr>
                      <tr>
                        <td>Permanent Address:</td>
                      <tr>
                        <td>Communication Address:</td>
                      </tr>
                      <tr>
                        <td>Phone Number:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Previous Schools</th>
                      <tr>
                        <td>School Name:</td>
                      </tr>
                      <tr>
                        <td>Years:</td>
                      </tr>
                      <tr>
                        <td>Standard:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Parents Details</th>
                      <tr>
                        <td>Father Name:</td>
                      </tr>
                      <tr>
                        <td>Mother Name:</td>
                      </tr>
                      <tr>
                        <td>Father Occupation:</td>
                      </tr>
                      <tr>
                        <td>Father Office Address:</td>
                      <tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                        <td>Mother Occupation:</td>
                      </tr>
                      <tr>
                        <td>Mother office Address</td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Guardian Details</th>
                      <tr>
                        <td>Guardian Name:</td>
                      </tr>
                      <tr>
                        <td>Guardian Relation:</td>
                      </tr>
                      <tr>
                        <td>Guardian Address:</td>
                      <tr>
                        <td>Guardian Occupation:</td>
                      </tr>
                      <tr>
                        <td>Guardian Office address:</td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                      </tr>
                      <th class="stu_details center" data-field="id">Medical Details</th>
                      <tr>
                        <td>Blood Group:</td>
                      </tr>
                      <tr>
                        <td>Height:</td>
                      </tr>
                      <tr>
                        <td>Weight:</td>
                      </tr>
                      <tr>
                        <td>Allergy:</td>
                      </tr>
                      <tr>
                        <td>Diseases:</td>
                      </tr>
                      <tr>
                        <td>Special Child:</td>
                      </tr>
                      <tr>
                        <td>Physically Challenged:</td>
                      </tr>
                      <tr>
                        <td>Medical Condition:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Transport Detials</th>
                      <tr>
                        <td>Transport Name:</td>
                      </tr>
                      <tr>
                        <td>Boarding Point:</td>
                        </tr>
                        <th class="stu_details center" data-field="id">Hostel Detials</th>
                      <tr>
                        <td>Hostel Name:</td>
                      </tr>
                      <tr>
                        <td>Room Number:</td>
                        </tr>
                  </table>
                </div>
            </div>
            </div>
            </div>
            </div>


  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });

        });
		
		
      
        var date = new Date(); 

        var day = date.getDate(); 
        var month = date.getMonth() + 1; 
        var year = date.getFullYear(); 

        if (month < 10) month = "0" + month; 
        if (day < 10) day = "0" + day; 

        var today = year + "-" + month + "-" + day; 


        document.getElementById('theDate').value = date;
		
        
		
    </script>
	
	<script> var ctxPath = "<%=request.getContextPath() %>";</script>
	
	
	
	
	
    <script>
    
    
    retrieve();
    function retrieve()
    {
    	var id=$('#techuname').val();
    	//alert("teacher uname in retrieve  "+id);
 	     
 		 $.ajax({
 	  			  type: "GET",
 	  			  url: ctxPath+'/app/teacher/getId.do?',
 	  			  data: {"uname":id},
 	  			  dataType: 'json',
 	  			}).done(function( responseJson )
 	  					{	
 	  						////alert("before   "+responseJson.uname);
 	  						loadDataIntoID1(responseJson);
 	  						////alert("after   "+responseJson.uname);
 	  				});
   }
   
   
   
   function loadDataIntoID1(responseJson)
   {
	   var id=responseJson.tid;
	   var fname=responseJson.fname;
	   var lname=responseJson.lname;
	   
	   $('#Teacher_Id').val(id);
	   $('#Teacher_Name').val(fname);
   }
    
    
    
    function absencerequestInsert()
	{
		 //alert("absencerequest Insert entered111111111111111");
		 var dt=new Date();
		 
		 var status="Apply";
		 
		 
		 $.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/AbsenceRequest/insertAbsenceRequest.do?',
	        data: {
	        		"theDate":$("#theDate").val(),
		       		"Teacher_Id":$("#Teacher_Id").val(),
		       		"Teacher_Name":$("#Teacher_Name").val(),    		
		       		"Start_Date":$("#Start_Date").val(),
		       		"End_Date":$("#End_Date").val(),		       		
	        		"Absence_Name":$("#Absence_Name").val(),
	        		"Status":status,
	        		"Comments":$("#Comments").val(),	        		
	        		"isActive":'Y',
	        		"Inserted_By":$("#loguser1").val(),
	        		"Inserted_Date":dt,
	        		"Updated_By":$("#loguser1").val(), 
	        		"Updated_Date":dt 
	        		},
	        success: function(data) 
	        {
	       	 if(data=='success')
	       	 {
	       		Materialize.toast("Success",4000);	 
	       	 }
	       	 else 
	       	 {
	       		 Materialize.toast(data,4000);	 
	       	 }  
	        }
		 });
	}

    
    </script>
</body>

</html>