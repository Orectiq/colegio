
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Student List - Colegio - A Orectiq Product</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
	
	
table, td, th {
    border: 1px solid black;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}

	
	
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->


<%
                        	String sid=(String)request.getAttribute("parId");
                    		System.out.println("Value passssssssssssssssssssssss 2222222222  "+sid);
                        
                        %>
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"  data-activates="profile-dropdown"><%= sid %><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Parents</p>
                    </div>
                </div>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/markParList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Marks</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/attendanceParList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Attendance</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/examParScheList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Exam Schedule</a>
                </li>
                
                 <li class="bold"><a href='<%=request.getContextPath()%>/app/student/feesParList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Fees Details</a>
                </li>
              <li class="bold"><a href='<%=request.getContextPath()%>/app/student/homeworkApproval?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Homework Approval</a>
                </li>
                
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
         
            
            
			
			
			
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Home Work</h5>
                <ol class="breadcrumbs">
                    <li><a href="index.html">Dashboard</a></li>
                    <li class="active">Home Work</li>
					
                </ol>
              </div>
            </div>
          </div>
		
        </div>
        <!--breadcrumbs end-->
        
        
         
        
		<div class="container">
		<!-- <div id="floatThead" class="cyan lighten-4"> -->
		
				<div class="container">
		<!--<div id="floatThead" class="cyan lighten-4">-->
	
 <!--Preselecting a tab-->
          <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
				
                  <!-- <div class="col s12">
                    <ul class="tabs tab-demo-active z-depth-1 cyan">
                      
						<li class="tab col s4"><a class="white-text waves-effect waves-light active" href="#">Class Name</a>
						</li>					  
					  <li class="tab col s4"><a class="white-text waves-effect waves-light active" href="#">Exam Name</a>
					  </li>
					  <li class="tab col s4"><a class="white-text waves-effect waves-light active" href="#">Date Period</a>
					  </li>	
						
						
						<a class="btn-floating blue btn tooltipped right" data-position="top" data-delay="0" data-tooltip="Edit" href="" data-tooltip-id="cbac35b5-9143-558e-d30f-33697894f2c5"><i class="large mdi-editor-border-color"></i></a>                        
						
						<a class="btn-floating yellow darken-1 btn tooltipped right" data-position="top" data-delay="0" data-tooltip="print" href="" data-tooltip-id="f6c6c8f3-0350-70b2-efc0-795582ec9370"><i class="large mdi-action-print"></i></a>
						
						<a class="btn-floating green btn tooltipped right" data-position="top" data-delay="0" data-tooltip="Export" href="" data-tooltip-id="14a91577-0e9f-2910-cc52-3fe66f537e4b"><i class="large mdi-action-description"></i></a>				                        
                   
				   </ul>
                  </div> -->

  					<form class="formValidate" id="formValidate" method="post">

										<div class="row">
											<div class="input-field col m6 ">
												Date <input type="date" id="theDate" readonly>
												<div class="errorTxt1"></div>
											</div>


											<div class="input-field col m6">
												Student ID<select class="browser-default stu_require" id="stuid" name="stuid" data-error="Please select your Students" onClick="getID()">
													<option value="NIL">Please select your Students ID</option>
												</select>
												<div class="errorTxt1"></div>	
											</div>
									    </div>
			
			
			
	
	</br>
		
                  <div class="col s12">
                    <div id="stu_list_all" class="col s12  cyan lighten-4">
                     		<!--DataTables example-->
				            <div id="#student-datatables_1">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">
				              <!--  <form  id="slick-login" method="get"> -->
				               
				              
				              
				              
				              
				                <div class="col s12 m12 l12">
				                
				              
				                
				                  <table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0">
				                    <thead>
				                        <tr>
											<th>Period No</th>											
											<th>Subjects</th>
											<th>Home Work</th>
											<th>Status</th>
											<th>Remarks</th>		
											<th>Select</th>	
										</tr>
				                    </thead>
				                 
				                    <tfoot>
				                      
				                    </tfoot>
				                 
				                    <tbody>
				                      
				                    </tbody>
				                  </table>
				                  
				                  <input id="parid" name="parid" type="hidden" size="45" tabindex="5" disabled value=<%= sid %> />
				                  <input id="sclass" name="sclass" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="sec" name="sec" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="tid" name="tid" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="hdate" name="hdate" type="hidden" size="45" tabindex="5" disabled  />
				                  
				                  <input id="sub1" name="sub1" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="sta1" name="sta1" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="rem1" name="rem1" type="hidden" size="45" tabindex="5" disabled  />
				                  
				                  <input id="sub2" name="sub2" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="sta2" name="sta2" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="rem2" name="rem2" type="hidden" size="45" tabindex="5" disabled  />
				                  
				                  <input id="sub3" name="sub3" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="sta3" name="sta3" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="rem3" name="rem3" type="hidden" size="45" tabindex="5" disabled  />
				                  
				                  <input id="sub4" name="sub4" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="sta4" name="sta4" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="rem4" name="rem4" type="hidden" size="45" tabindex="5" disabled  />
				                  
				                  <input id="sub5" name="sub5" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="sta4" name="sta5" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="rem5" name="rem5" type="hidden" size="45" tabindex="5" disabled  />
				                  
				                  <input id="sub6" name="sub6" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="sta6" name="sta6" type="hidden" size="45" tabindex="5" disabled  />
				                  <input id="rem6" name="rem6" type="hidden" size="45" tabindex="5" disabled  />
				                  
				                 
				                  
				                  
								  
				                </div>
				                
				                 <div class="col s6">
										<!--  <button class="btn waves-effect waves-light right" id="submit" type="submit" value="submit">Submit
										</button> -->
										<a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="homeworkStatusInsert()" name="final" id="final">Save Details</a>
										
									</div>
				                
				                
				              </div>
				            </div>  
				            <br>
				            <div class="divider"></div> 
				            <!--DataTables example Row grouping-->
                    </div>

                  </div>
				  
				  
				  
				  </br>
				  
	
	
                </div>
				
				
				</form>
				
              </div>
            </div>
          </div>

        </div>


             
          <!-- Floating Action Button -->
           
            <!-- Floating Action Button -->
        </div>
		  
		  </div>
		  </div>
        <!--end container-->
</div>
      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START RIGHT SIDEBAR NAV-->
      <aside id="right-sidebar-nav">
        <ul id="chat-out" class="side-nav rightside-navigation">
            <li class="li-hover">
            <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
            <div id="right-search" class="row">
                <form class="col s12">
                    <div class="input-field">
                        <i class="mdi-action-search prefix"></i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Search</label>
                    </div>
                </form>
            </div>
            </li>
            <li class="li-hover">
                <ul class="chat-collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                    <div class="collapsible-body recent-activity">
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">just now</a>
                                <p>Jim Doe Purchased new equipments for zonal office.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Yesterday</a>
                                <p>Your Next flight for USA will be on 15th August 2015.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Last Week</a>
                                <p>Jessy Jay open a new store at S.G Road.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                    <div class="collapsible-body sales-repoart">
                        <div class="sales-repoart-list  chat-out-list row">
                            <div class="col s8">Target Salse</div>
                            <div class="col s4"><span id="sales-line-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Payment Due</div>
                            <div class="col s4"><span id="sales-bar-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Delivery</div>
                            <div class="col s4"><span id="sales-line-2"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Progress</div>
                            <div class="col s4"><span id="sales-bar-2"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                    <div class="collapsible-body favorite-associates">
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Eileen Sideways</p>
                                <p class="place">Los Angeles, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Zaham Sindil</p>
                                <p class="place">San Francisco, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Renov Leongal</p>
                                <p class="place">Cebu City, Philippines</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Weno Carasbong</p>
                                <p>Tokyo, Japan</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Nusja Nawancali</p>
                                <p class="place">Bangkok, Thailand</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </li>
        </ul>
      </aside>
      <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->
  <div id="modal4" class="modal modal-fixed-footer green white-text stu_list_pop_box">
            <div class="modal-content">
            <div id="bordered-table">
            
              <h4 class="header left">Student Name</h4>
              <h4 class="header right">Academic Year</h4>
              <hr style="clear:both;"/>
              
              <div class="row">
              
                <div class="col s12 m4 l3">
                  <img title="Avatar" src="../../resources/images/avatar.jpg">
                </div>
                
                <div class="col s12 m8 l9">
                  <table class="bordered">
                        <th class="stu_details center" data-field="id">Student Details</th>
                        <tr>
                        <td>Class:</td>
                      </tr>
                      <tr>
                        <td>Roll Number:</td>
                      </tr>
                </table>
                </div>
                </div>
                      
                      <div class="col s12 m4 l3">
                  <table class="bordered">
                        <tr>
                        <td>Birth Date:</td>
                      </tr>
                      <tr>
                        <td>Gender:</td>
                      </tr>
                      <tr>
                        <td>Nationality:</td>
                      </tr>
                      <tr>
                        <td>Religion:</td>
                      </tr>
                      <tr>
                        <td>Community:</td>
                      </tr>
                      <tr>
                        <td>Place of Birth:</td>
                      </tr>
                      <tr>
                        <td>Mother Tongue:</td>
                      </tr>
                      <tr>
                        <td>Date of Joining:</td>
                      </tr>
                      <tr>
                        <td>Permanent Address:</td>
                      <tr>
                        <td>Communication Address:</td>
                      </tr>
                      <tr>
                        <td>Phone Number:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Previous Schools</th>
                      <tr>
                        <td>School Name:</td>
                      </tr>
                      <tr>
                        <td>Years:</td>
                      </tr>
                      <tr>
                        <td>Standard:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Parents Details</th>
                      <tr>
                        <td>Father Name:</td>
                      </tr>
                      <tr>
                        <td>Mother Name:</td>
                      </tr>
                      <tr>
                        <td>Father Occupation:</td>
                      </tr>
                      <tr>
                        <td>Father Office Address:</td>
                      <tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                        <td>Mother Occupation:</td>
                      </tr>
                      <tr>
                        <td>Mother office Address</td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Guardian Details</th>
                      <tr>
                        <td>Guardian Name:</td>
                      </tr>
                      <tr>
                        <td>Guardian Relation:</td>
                      </tr>
                      <tr>
                        <td>Guardian Address:</td>
                      <tr>
                        <td>Guardian Occupation:</td>
                      </tr>
                      <tr>
                        <td>Guardian Office address:</td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                      </tr>
                      <th class="stu_details center" data-field="id">Medical Details</th>
                      <tr>
                        <td>Blood Group:</td>
                      </tr>
                      <tr>
                        <td>Height:</td>
                      </tr>
                      <tr>
                        <td>Weight:</td>
                      </tr>
                      <tr>
                        <td>Allergy:</td>
                      </tr>
                      <tr>
                        <td>Diseases:</td>
                      </tr>
                      <tr>
                        <td>Special Child:</td>
                      </tr>
                      <tr>
                        <td>Physically Challenged:</td>
                      </tr>
                      <tr>
                        <td>Medical Condition:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Transport Detials</th>
                      <tr>
                        <td>Transport Name:</td>
                      </tr>
                      <tr>
                        <td>Boarding Point:</td>
                        </tr>
                        <th class="stu_details center" data-field="id">Hostel Detials</th>
                      <tr>
                        <td>Hostel Name:</td>
                      </tr>
                      <tr>
                        <td>Room Number:</td>
                        </tr>
                  </table>
                </div>
            </div>
            </div>
            </div>
            </div>


  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });

        });
		
		
		
		
		/*** Date Picker *****/
		
		var date = new Date();

		var day = date.getDate();
		var month = date.getMonth() + 1;
		var year = date.getFullYear();

		if (month < 10) month = "0" + month;
		if (day < 10) day = "0" + day;

		var today = year + "-" + month + "-" + day;


		document.getElementById('theDate').value = date;
		
		
    </script>
    
    <script> var ctxPath = "<%=request.getContextPath() %>";</script>
	
	<!-- <script>
    
   
    retrieve();
    
    function retrieve()
      {
      	$.ajax({
      			  type: "GET",
      			  url: ctxPath+'/app/Homework/getltrreferencedata.do?',
      			  dataType: 'json',
      			}).done(function( responseJson )
      					{	
       						loadDataIntoTable(responseJson);
      				});
      }
    
    

    function loadDataIntoTable(responseJson)
      {
      	 var tblHtml = "";
      	 $.each(responseJson.homeworkServiceVOList, function(index, val)
      	{
      		var PeriodNo1=val.PeriodNo1; 
      		var Subject1=val.Subject1;
      		var HomeWork1=val.HomeWork1;
      		
      		
      		$('#homework').val(PeriodNo1);
      		

      		tblHtml += '<tr><td style="text-align: left">'+PeriodNo1+'</a></td><td style="text-align: left">'+Subject1+'</td>'+ 
 			'<td style="text-align: left">'+HomeWork1+'</td>'+
 			
 			'<td><a class="btn-floating blue btn tooltipped" data-position="top" data-delay="0" data-tooltip="Edit" onClick="PeriodCompleted(PeriodNo1,Subject1,HomeWork1)" name="PeriodNo1" id='+PeriodNo1+'><i class="large mdi-editor-border-color"></i></a>'+
 			'<a class="btn-floating blue btn tooltipped" data-position="top" data-delay="0" data-tooltip="Edit" onClick="PeriodNotCompleted(PeriodNo1,Subject1,HomeWork1)" name="PeriodNo1" id='+PeriodNo1+'><i class="large mdi-editor-border-color"></i></a>'+
 			 '<a class="btn-floating red darken-1 btn tooltipped" data-position="top" data-delay="0" data-tooltip="Delete" onClick="NeedClarification(PeriodNo1,Subject1,HomeWork1)" name="PeriodNo3" id='+PeriodNo1+'><i class="large mdi-action-delete"></i></a></td></tr>';
	    
      	});   
	
	 $("#stu-db-data-table_1 tbody").html(tblHtml);



   /*  '<td><a class="btn-floating blue btn tooltipped" data-position="top" data-delay="0" data-tooltip="NotCompleted" onClick="HomeworkNotCompleted(HPeriod_No,HSubject,HHome_Work)" name="homework" id='+HPeriod_No+'><i class="large mdi-editor-border-color"></i></a>'+

		 '<a class="btn-floating pink btn tooltipped " data-position="top" data-delay="0" data-tooltip="Completed" onClick="HomeworkCompleted(HPeriod_No,HSubject,HHome_Work)" name="homework" id='+HPeriod_No+'><i class="large mdi-action-done"></i></a></td> </tr>'+
		 '<a class="btn-floating pink btn tooltipped " data-position="top" data-delay="0" data-tooltip="Need Clarification" onClick="HomeworkNeed Clarification(HPeriod_No,HSubject,HHome_Work)" name="homework" id='+HPeriod_No+'><i class="large mdi-action-visibility"></i></a>' ; 

								
         
	});   
	
	$("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
	
	
 */
 }
    
    </script> -->
    
    <script>
    
   
    retrieve();
    function retrieve()
    {
    	 var id=$('#parid').val();
    	 //alert("enter id "+id);
  		 $.ajax({
  	  			  type: "GET",
  	  			  url: ctxPath+'/app/student/getParentLoginID.do?',
  	  			  data: {"mobile":id},
  	  			  dataType: 'json',
  	  			}).done(function( responseJson )
  	  					{	
  	  						////alert("before   "+responseJson.parent_id);
  	  						loadDataIntoLoginID(responseJson);
  	  						////alert("after   "+responseJson.student_id);
  	  				});
    }	
    function loadDataIntoLoginID(responseJson)
    {
    	////alert("enter table");
    	 $.each(responseJson.parentServiceVOList, function(index, val)
    	    	 {
    		 		////alert("enter each");
    		 		var sid1=val.parent_id;
    	 	   		////alert("id 1 "+sid1);
    	 	  		retrieve1(sid1);	 
    	    	 });
 	   
    }
    function retrieve1(id)
    {
    	//var id=$('#parid').val();
    	////alert("Parent id  "+id);
    	$.ajax({
			  type: "GET",
			  url: ctxPath+'/app/student/getParentdataAll.do?',  					  
			  data: {"parent_id":id},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
				loadDataIntoLoop(responseJson);
	  				});
	  }
	  var sid;
	function loadDataIntoLoop(responseJson)
	{
		var tblHtml = "";
		var sid;
		 var sel = $("#stuid").empty();
		 $.each(responseJson.studentServiceVOList, function(index, val)
		 {
			sid=val.student_id;
			/* $("#sclass").val(val.sclass);
			$("#sec").val(val.section); */
			sel.append('<option value="' + sid + '">' + sid + '</option>');
		 }); 
		
	}
    
    
    
    
   
   
   
   function getID()
   {
	   var obj = $("#stuid").val();
	   ////alert("student id  "+obj);
	   $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/student/getltrreferencedataAll.do?',
			  data: {"student_id":obj},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						////alert("before   "+responseJson.student_id);
					    loadDataIntoPopup(responseJson);
				});
   }
   function  loadDataIntoPopup(responseJson)
   {
	   ////alert("enter table");
	 
		   			var sid1=responseJson.student_id;
		   			var sclass1=responseJson.sclass;
		   			var sec1=responseJson.section;
		   			$("#sclass").val(sclass1);
		   			$("#sec").val(sec1);
		   			
	   				////alert("Sid  "+sid1+"  Class   "+sclass1+"  sec  "+sec1);
	   				loadID(sid1,sclass1,sec1);
	
   }
    
    
    
    function loadID(sid1,sclass1,sec1)
    {
       //	var uname=$('#techuname').val();
       var date = new Date(); 

		var day = date.getDate(); 
		var month = date.getMonth() + 1; 
		var year = date.getFullYear(); 

if (month < 10) month = "0" + month; 
if (day < 10) day = "0" + day; 

var today = year + "-" + month + "-" + day; 
       
		////alert("Enter LOad ID  Sid  "+sid1+"  Class   "+sclass1+"  sec  "+sec1+" today date   "+today);
      	$.ajax({
      			  type: "GET",
      			  url: ctxPath+'/app/Homework/getHomeworStudata.do?',
      			  data: {"sClass1":sclass1,"Section1":sec1,"theDate":today},
      			  dataType: 'json',
      			}).done(function( responseJson )
      					{	
       						loadDataIntoTable(responseJson);
      				});
      }
    
    

    function loadDataIntoTable(responseJson)
      {
    	////alert("Table  ");
      	 var tblHtml = "";
      	 var i=1;
      	 var j=1;
      	var grop=1;
      	 $.each(responseJson.homeworkServiceVOList, function(index, val)
      	{
      		// //alert("enter each");
      		var PeriodNo1=val.periodNo1; 
      		var Subject1=val.subject1;
      		var HomeWork1=val.homeWork1;
      		
      		var PeriodNo2=val.periodNo2; 
      		var Subject2=val.subject2;
      		var HomeWork2=val.homeWork2;
      		
      		var PeriodNo3=val.periodNo3; 
      		var Subject3=val.subject3;
      		var HomeWork3=val.homeWork3;
      		
      		var PeriodNo4=val.periodNo4; 
      		var Subject4=val.subject4;
      		var HomeWork4=val.homeWork4;
      		
      		var PeriodNo5=val.periodNo5; 
      		var Subject5=val.subject5;
      		var HomeWork5=val.homeWork5;
      		
      		var eSubject=val.eSubject;
      		var eHomeWork=val.eHomeWork;
      		
      		var tid=val.teacher_Id;
      		var tname=val.teacher_Name;
      		var hdate=val.theDate;
      		
      		
      		
      		$('#tid').val(tid);
      		$('#hdate').val(hdate);

      		$('#sub1').val(Subject1);
      		$('#sub2').val(Subject2);
      		$('#sub3').val(Subject3);
      		$('#sub4').val(Subject4);
      		$('#sub5').val(Subject5);
      		$('#sub6').val(eSubject);
      		
      		
      		
      		
      		$('#homework').val(PeriodNo1);
      		var k="group"+grop;


      		tblHtml += '<tr><td style="text-align: left">'+PeriodNo1+'</td><td style="text-align: left"><input type=hidden value='+Subject1+' id=sub1 >'+Subject1+'</td><td style="text-align: left"> <input type=hidden value='+HomeWork1+' id=home1 >'+HomeWork1+'</td>'+
      					'<input type=hidden value='+tid+' id=tea1 ><input type=hidden value='+tname+' id=tname1 ><input type=hidden value='+hdate+' id=hdate1 >'+
      					'<td><select class=browser-default stu_require name='+k+' id=s'+(i++)+'><option value="NIL">Your Section</option><option value=Completed>Completed</option><option value=Not-Completed>Not-Completed</option><option value=Clarification>Clarification</option></select></td>'+
      					'<td><input value=YourRemarks name='+k+' type=text id=txt'+(j++)+'   /><label for=test'+i+'></label></td>'+
				      	// sta1='+$('#s'+(i)+')'.val()' rem1='+$('#txt'+(j)+')'.val()+' onchange=status1(rem1,sta1)
				      	'<td><input name=r1 type=radio id=testt1 onClick=status1() /><label for=testt1>Check</label> </td>'+
				      	'</tr>'+
				      	
				      	'<tr><td style="text-align: left">'+PeriodNo2+'</td><td style="text-align: left"><input type=hidden value='+Subject2+' id=sub2 >'+Subject2+'</td><td style="text-align: left"> <input type=hidden value='+HomeWork2+' id=home2 >'+HomeWork2+'</td>'+
      					'<input type=hidden value='+tid+' id=tea1 ><input type=hidden value='+tname+' id=tname1 ><input type=hidden value='+hdate+' id=hdate1 >'+
				      	'<td><select class=browser-default stu_require name='+k+' id=s'+(i++)+' ><option value="NIL">Your Section</option><option value=Completed>Completed</option><option value=Not-Completed>Not-Completed</option><option value=Clarification>Clarification</option></select></td>'+
				      	'<td><input value=YourRemarks name='+k+' type=text id=txt'+(j++)+'   /><label for=test'+i+'></label></td>'+
				      	'<td><input name=r2 type=radio id=testt2 onClick=status2() /><label for=testt2>Check</label> </td>'+
				      	'</tr>'+
      					
				      	'<tr><td style="text-align: left">'+PeriodNo3+'</td><td style="text-align: left"><input type=hidden value='+Subject3+' id=sub1 >'+Subject3+'</td><td style="text-align: left"> <input type=hidden value='+HomeWork3+' id=home1 >'+HomeWork3+'</td>'+
      					'<input type=hidden value='+tid+' id=tea1 ><input type=hidden value='+tname+' id=tname1 ><input type=hidden value='+hdate+' id=hdate1 >'+
				      	'<td><select class=browser-default stu_require name='+k+' id=s'+(i++)+' ><option value="NIL">Your Section</option><option value=Completed>Completed</option><option value=Not-Completed>Not-Completed</option><option value=Clarification>Clarification</option></select></td>'+
				      	'<td><input value=YourRemarks name='+k+' type=text id=txt'+(j++)+'   /><label for=test'+i+'></label></td>'+
				      	'<td><input name=r3 type=radio id=testt3 onClick=status3() /><label for=testt3>Check</label> </td>'+
				      	'</tr>'+
      					
				      	'<tr><td style="text-align: left">'+PeriodNo4+'</td><td style="text-align: left"><input type=hidden value='+Subject4+' id=sub1 >'+Subject4+'</td><td style="text-align: left"> <input type=hidden value='+HomeWork4+' id=home1 >'+HomeWork4+'</td>'+
      					'<input type=hidden value='+tid+' id=tea1 ><input type=hidden value='+tname+' id=tname1 ><input type=hidden value='+hdate+' id=hdate1 >'+
				      	'<td><select class=browser-default stu_require name='+k+' id=s'+(i++)+' ><option value="NIL">Your Section</option><option value=Completed>Completed</option><option value=Not-Completed>Not-Completed</option><option value=Clarification>Clarification</option></select></td>'+
				      	'<td><input value=YourRemarks name='+k+' type=text id=txt'+(j++)+'   /><label for=test'+i+'></label></td>'+
				      	'<td><input name=r4 type=radio id=testt4 onClick=status4() /><label for=testt4>Check</label> </td>'+
				      	'</tr>'+
      					
				      	'<tr><td style="text-align: left">'+PeriodNo5+'</td><td style="text-align: left"><input type=hidden value='+Subject5+' id=sub1 >'+Subject5+'</td><td style="text-align: left"> <input type=hidden value='+HomeWork5+' id=home1 >'+HomeWork5+'</td>'+
      					'<input type=hidden value='+tid+' id=tea1 ><input type=hidden value='+tname+' id=tname1 ><input type=hidden value='+hdate+' id=hdate1 >'+
				      	'<td><select class=browser-default stu_require name='+k+' id=s'+(i++)+' ><option value="NIL">Your Section</option><option value=Completed>Completed</option><option value=Not-Completed>Not-Completed</option><option value=Clarification>Clarification</option></select></td>'+
				      	'<td><input value=YourRemarks name='+k+' type=text id=txt'+(j++)+'   /><label for=test'+i+'></label></td>'+
				      	'<td><input name=r5 type=radio id=testt5 onClick=status5() /><label for=testt5>Check</label> </td>'+
				      	'</tr>'+
      					
 						
				      	'<tr><td style="text-align: left">'+PeriodNo5+'</td><td style="text-align: left"><input type=hidden value='+eSubject+' id=sub1 >'+eSubject+'</td><td style="text-align: left"> <input type=hidden value='+eHomeWork+' id=home1 >'+eHomeWork+'</td>'+
      					'<input type=hidden value='+tid+' id=tea1 ><input type=hidden value='+tname+' id=tname1 ><input type=hidden value='+hdate+' id=hdate1 >'+
				      	'<td><select class=browser-default stu_require name='+k+' id=s'+(i++)+' ><option value="NIL">Your Section</option><option value=Completed>Completed</option><option value=Not-Completed>Not-Completed</option><option value=Clarification>Clarification</option></select></td>'+
				      	'<td><input value=YourRemarks name='+k+' type=text id=txt'+(j++)+'   /><label for=test'+i+'></label></td>'+
				      	'<td><input name=r6 type=radio id=testt6 onClick=status6() /><label for=testt6>Check</label> </td>'+
				      	'</tr>';
 			 			
      		
      					grop=parseInt(grop)+1;
     	            
     	 });   
   	
     	 $("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
     
     }
    
    function status1()
    {
    	var rem1=$("#txt1").val();
    	var status1=$("#s1").val();
    	$("#sta1").val(status1);
    	$("#rem1").val(rem1);
    	
    	////alert("remarks  "+rem1+"  status "+status1);
    }
    function status2()
    {
    	var rem1=$("#txt2").val();
    	var status1=$("#s2").val();
    	$("#sta2").val(status1);
    	$("#rem2").val(rem1);
    	////alert("remarks  "+rem1+"  status "+status1);
    }
    function status3()
    {
    	var rem1=$("#txt3").val();
    	var status1=$("#s3").val();
    	$("#sta3").val(status1);
    	$("#rem3").val(rem1);
    	////alert("remarks  "+rem1+"  status "+status1);
    }
    function status4()
    {
    	var rem1=$("#txt4").val();
    	var status1=$("#s4").val();
    	$("#sta4").val(status1);
    	$("#rem4").val(rem1);
    	////alert("remarks  "+rem1+"  status "+status1);
    }
    function status5()
    {
    	var rem1=$("#txt5").val();
    	var status1=$("#s5").val();
    	$("#sta5").val(status1);
    	$("#rem5").val(rem1);
    	////alert("remarks  "+rem1+"  status "+status1);
    }
    function status6()
    {
    	var rem1=$("#txt6").val();
    	var status1=$("#s6").val();
    	$("#sta6").val(status1);
    	$("#rem6").val(rem1);
    //	//alert("remarks  "+rem1+"  status "+status1);
    }



    function homeworkStatusInsert()
    {
    	var sid=$("#stuid").val(); 
    	var sclass1=$("#sclass").val(); 
		var sec=$("#sec").val(); 
		var tid=$('#tea1').val(); 
		var hdate=$('#hdate1').val(); 
		
		var sub1=$('#sub1').val();
		var status1=$('#sta1').val();
		var remark1=$('#rem1').val();
		
		var sub2=$('#sub2').val();
		var status2=$('#sta2').val();
		var remark2=$('#rem2').val();
				
		var sub3=$('#sub3').val();
		var status3=$('#sta3').val();
		var remark3=$('#rem3').val();
						
		var sub4=$('#sub4').val();
		var status4=$('#sta4').val();
		var remark4=$('#rem4').val();
		
		var sub5=$('#sub5').val();
		var status5=$('#sta5').val();
		var remark5=$('#rem5').val();
		
		var sub6=$('#sub6').val();
		var status6=$('#sta6').val();
		var remark6=$('#rem6').val();
		
		
		
    	/*  //alert("id  "+sid+"   class "+sclass+" sec  "+sec+"tid  "+tid+"  hdate "+hdate);
    	//alert("  subject1 "+sub1+" status1  "+status1+"  remark1  "+remark1);
    	//alert("  subject2 "+sub2+" status2  "+status2+"  remark2  "+remark2);
    	//alert("  subject3 "+sub3+" status3  "+status3+"  remark3  "+remark3);
    	//alert("  subject4 "+sub4+" status4  "+status4+"  remark4  "+remark4);
    	//alert("  subject5 "+sub5+" status5  "+status5+"  remark5  "+remark5);
    	//alert("  subject6 "+sub6+" status6  "+status6+"  remark6  "+remark6);  */
    	
    	
    	
    	/*  $.ajax({
 	        type: 'POST',
 	        url: ctxPath+'/app/Homework/Status1.do?',
 	        data: {"sclass":sclass},
 	        success: function(data) 
 	        {
 	       	 if(data=='success')
 	       	 {
 	       		Materialize.toast("Success",4000);	 
 	       	 }
 	       	 else 
 	       	 {
 	       		 Materialize.toast(data,4000);	 
 	       	 }  
 	        }
 		 }); */
    	
    	
    	
    	  $.ajax({
 	        type: 'POST',
 	       	url: ctxPath+'/app/Homework/Status1.do?',
 	        data: {"thedate":hdate,"sclass":sclass1,"section":sec,"teacher_id":tid,"student_id":sid,
 	        	"subject1":sub1,"status1":status1,"remark1":remark1,
 	        	"subject2":sub2,"status2":status2,"remark2":remark2,
 	        	"subject3":sub3,"status3":status3,"remark3":remark3,
 	        	"subject4":sub4,"status4":status4,"remark4":remark4,
 	        	"subject5":sub5,"status5":status5,"remark5":remark5,
 	        	"subject6":sub6,"status6":status6,"remark6":remark6 
 	        	
 	        	 },
 	        success: function(data) 
 	        {
 	       	 if(data=='success')
 	       	 {
 	       		Materialize.toast("Success",4000);	 
 	       	 }
 	       	 else 
 	       	 {
 	       		 Materialize.toast(data,4000);	 
 	       	 }  
 	        }
 		 }); 
    	
    	//$("#button1").prop("disabled", true); 
    }



    
    </script>
</body>

</html>