<html lang="en"><head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Student Admission Form</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
    <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<style>
	.input-field div.error, .input-field div.error2{
		position: relative;
		top: -1rem;
		left: 0rem;
		font-size: 0.8rem;
		color:#FF4081;
		-webkit-transform: translateY(0%);
		-ms-transform: translateY(0%);
		-o-transform: translateY(0%);
		transform: translateY(0%);
	}
	.inp2{
		float:left;
		margin-top:0px;
	}
	.input-field div.error2{
		top:-8px;
	}
	.input-field label.active{
		width:100%;
	}
	h4.header2.title{
		float:left;
		width:100%;
	}
	
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START HEADER -->
  <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Materialize</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button" data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA"></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- translation-button -->
                    <ul id="translation-dropdown" class="dropdown-content">
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
  </header>
  <!-- END HEADER -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START MAIN -->
  <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="#"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                       <%
                        	String uname=(String)session.getAttribute("userName2");
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= uname %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=dashboard/dashboard' class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                <%-- <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Student</a>
                </li> --%>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/Parent_Student' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Stu-Par List</a>
                        		    </li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_Main' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_List' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> ParentsList</a>
                        		    </li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/ParentsView' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                 	
               <%--  <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers</a>
                </li>
                
                
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers List</a>
                </li> --%>
                <li class="bold"><a href="../calender/app-calendar.html" class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Class_Main' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/AddActivity' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Activity_List' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Teachers </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                 		 <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Non-Teaching Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteaching' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teaching Staff</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachinglist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachingview' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach View</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                    
                                    <li><a href="">Teachers Attendance</a>
                                    </li>
                                    <li><a href="">Attendance Reports</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examlist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Accounts</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="table-basic.html">Fees Allocation</a>
                                    </li>
                                    <li><a href="table-data.html">Payment Details</a>
                                    </li>
                                    <li><a href="table-jsgrid.html">Teachers Salary</a>
                                    </li>
                                    <li><a href="table-editable.html">Other Expenses</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                       
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Library </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/LibraryMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library Details </a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/Libraryreqbook' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library Request Details </a>
                                    </li>
                                    
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/librarybook' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/libraryreqbooklist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library Request List</a>
                                    </li>
                                </ul>
                            </div>
                 	    </li>
                 	    
                 	    
                 	    
                         <li class="bold"><a href='<%=request.getContextPath()%>/app/book/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-editor-insert-comment"></i> Attendance </a>
                        </li>
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/book/loadpage?d=transport/transport_table' class="waves-effect waves-cyan"><i class="mdi-editor-insert-comment"></i> Transport </a>
                        </li>
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/Dormitory/loadpage?d=Dormitory/Dormitory_list' class="waves-effect waves-cyan"><i class="mdi-social-pages"></i> Dormitories</a>
                        </li>
                        <!-- <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-action-shopping-cart"></i> Notice Board</a>
                        </li> -->
                       <!--  <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Media Center</a>
                        </li> -->
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Notice Board </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/addevent' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/addnews' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/events_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/news_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Media Center </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Addmedia' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Media</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Addalbum' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Album</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Medialist' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Media List</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Albumlist' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Album List</a>
                        		    </li>
                   				</ul>
                     		</div>
                 	    </li>
                        
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=admission/Admission' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-action-account-circle"></i> Settings</a>
                            <div class="collapsible-body">
                                <ul>     
                                    <li><a href="user-profile-page.html">User Profile</a>
                                    </li>                                   
                                    <li><a href="user-login.html">Login</a>
                                    </li>                                        
                                    <li><a href="user-register.html">Register</a>
                                    </li>
                                    <li><a href="user-forgot-password.html">Forgot Password</a>
                                    </li>
                                    <li><a href="user-lock-screen.html">Lock Screen</a>
                                    </li>                                        
                                    <li><a href="user-session-timeout.html">Session Timeout</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
            </ul></li></ul></aside>
      <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
		
        <!--breadcrumbs end-->
		    <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Media-Add Media</h5>
               </div>
            </div>
          </div>
		
		 <div id="stu_list_all" class="col s12  cyan lighten-4">
		    <div class="container">
          <div class="section">
           <div class="col s8 m12 l9 right">
                      
					  <a class="waves-effect waves-light  btn">Cancel</a>
                    </div>
					 </div>
					  </div>
       
					<div class="col s12">
						<form class="formValidate" id="formValidate" method="get" action="">
							<div id="media-tab" class="col s12">
								<div class="row">
								
								
							

									<div class="input-field col s12">
										<label for="Media Title">Media Title</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="Media_Title" name="Media_Title" type="text" maxlength="20" class="textOnly med_require" data-error="Please enter your Book Title">
										<div class="error errorTxt1"></div>
									</div>
									
									
									
																	

																	
									<div class="input-field col s12">
										<label for="Media Description">Media Description</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="Media_Description" name="Media_Description" type="text" maxlength="20" class="textOnly med_require" data-error="Please enter Author name">
										<div class="error errorTxt2"></div>
									</div>																										
									

 
   <div class="row">
               <div class="input-field col m2">
			  
                     <p>
                     <input name="group1" type="radio" id="test1" value="Media_ImageURL" />
                      <label for="test1">Image URL:</label> 
                    
                  </p>
				 
					 </div>
					  <div class="input-field col m12">
					 
                      <input type="text" name="Media_ImageURL" id="Media_ImageURL">
					  </div>
   </div>
                   <div class="row">
               <div class="input-field col m2">
			   
                      <p>
                       <input name="group1" type="radio" id="test2" value="Media_YoutubeURL" /> 
                      <label for="test2">YOUTUBE URL:</label> 
                    
                  </p>
					 </div>
					  <div class="input-field col m12">
					 
                      <input type="text" name="Media_YoutubeURL" id="Media_YoutubeURL">
					  </div>
   </div>
                    <div class="row">
               <div class="input-field col m2">
			   
                      <p>
                      <input name="group1" type="radio" id="test2" value="Media_VimeeURL" /> 
                      <label for="test3">VIMEE URL:</label> 
                    
                  </p>
					 </div>
					  <div class="input-field col m12">
					 
                      <input type="text" name="Media_VimeeURL" id="Media_VimeeURL">
					  </div>
					  </br></br>
					  
					  
					  
					  <div class="col s12">
										<a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="mediaInsert()" name="med_Add" id="med_Add">Add Album</a>
									</div>
					  
   </div>
                 
				 

               
			  
						   				
									
									
									
											
									
							  
							
						
						
									</div>
					</div>
					</form>
					</div>
				
           
		
		
   
     </div>
   

  </div>

	
	</section>
	
	   </div>
    </div>
      <!-- END CONTENT -->

    
  <!-- END MAIN -->



  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://orectiq.com/">Orectiq</a></span>
        </div>
    </div>
  </footer>
  <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism
    <script type="text/javascript" src="js/prism/prism.js"></script>-->
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
	<script type="text/javascript" src="../../js/media-custom-script.js"></script>
     <script> var ctxPath = "<%=request.getContextPath() %>";</script>
     <script>
	function mediaInsert()
	{
		//alert("Welcome to book insert function");
		 var dt=new Date();
		 
		 var ur = document.getElementsByName("group1");
		 
		 
		    if (ur[0].checked == true) {
		   	 urltype=ur[0].value;
		   	 urlval=$("#Media_ImageURL").val();
		   	//alert("value ratio  "+urltype+" and URL  "+urlval);
		   	
		    } else if(ur[1].checked == true) {
		    	 urltype=ur[1].value;
			   	 urlval=$("#Media_YoutubeURL").val();
			   	//alert("value ratio  "+urltype+" and URL  "+urlval);
		    }
		    else if(ur[2].checked == true) {
		    	 urltype=ur[2].value;
			   	 urlval=$("#Media_VimeeURL").val();
			   	//alert("value ratio  "+urltype+" and URL  "+urlval);
		    }
		 
		    
		    
		    
		 $.ajax({
		        type: 'POST',
		        url: ctxPath+'/app/media/insertMedia.do?',
		        data: {"Media_Title":$("#Media_Title").val(),"Media_Description":$("#Media_Description").val(),
		        	   "urlType":urltype,"urlPath":urlval,
		        	   "isActive":'Y',"Inserted_By":$("#loguser1").val(),"Inserted_Date":dt,"Updated_By":$("#loguser1").val(),"Updated_Date":dt},
		        success: function(data) 
		        {
		       	 if(data=='success')
		       	 {
		       		Materialize.toast("Success",4000);	 
		       	 }
		       	 else 
		       	 {
		       		 Materialize.toast(data,4000);	 
		       	 }  
		        }
			 });
		
	}
	</script>
     
</body></html>