<!DOCTYPE html> 
<html lang="en"> 

<head> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<meta name="msapplication-tap-highlight" content="no"> 
<meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. "> 
<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,"> 
<title>Student List - Colegio - A Orectiq Product</title> 

<!-- Favicons--> 
<link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32"> 
<!-- Favicons--> 
<link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png"> 
<!-- For iPhone --> 
<meta name="msapplication-TileColor" content="#00bcd4"> 
<meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png"> 
<!-- For Windows Phone --> 


<!-- CORE CSS--> 

<link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<!-- Custome CSS-->    
<link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection"> 


     
    <title>Tiny, opensource, Bootstrap WYSIWYG rich text editor from MindMup</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
   
		
   
<script src="js/jquery.classyedit.js"></script>


<!-- INCLUDED PLUGIN CSS ON THIS PAGE --> 
<link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<style> 
.class_active{display:block!important;opacity:1!important;} 
.student_class_section{display:none;} 
.student_class_section_sbt{display:none;} 
.student_prof_pop { 
background: transparent none repeat scroll 0 0 !important; 
box-shadow: 0 0; 
color: #000000; 
padding: 0; 
} 
.student_prof_pop:hover{ 
box-shadow: 0 0; 
} 
.stu_list_pop_box h4.header{ 
color:#ffffff; 
} 
</style> 
</head> 

<body> 
<!-- Start Page Loading --> 
<div id="loader-wrapper"> 
<div id="loader"></div>        
<div class="loader-section section-left"></div> 
<div class="loader-section section-right"></div> 
</div> 
<!-- End Page Loading --> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START HEADER --> 
<header id="header" class="page-topbar"> 
<!-- start header nav--> 
<div class="navbar-fixed"> 
<nav class="navbar-color"> 
<div class="nav-wrapper"> 
<ul class="left">                      
<li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li> 
</ul> 
<div class="header-search-wrapper hide-on-med-and-down"> 
<i class="mdi-action-search"></i> 
<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/> 
</div> 
<ul class="right hide-on-med-and-down"> 
<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a> 
</li> 
<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a> 
</li> 
<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i> 

</a> 
</li>                        
<li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a> 
</li> 
</ul> 
<!-- notifications-dropdown --> 
<ul id="notifications-dropdown" class="dropdown-content"> 
<li> 
<h5>NOTIFICATIONS <span class="new badge">5</span></h5> 
</li> 
<li class="divider"></li> 
<li> 
<a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time> 
</li> 
<li> 
<a href="#!"><i class="mdi-action-stars"></i> Completed the task</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time> 
</li> 
<li> 
<a href="#!"><i class="mdi-action-settings"></i> Settings updated</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time> 
</li> 
<li> 
<a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time> 
</li> 
<li> 
<a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time> 
</li> 
</ul> 
</div> 
</nav> 
</div> 
<!-- end header nav--> 
</header> 
<!-- END HEADER --> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START MAIN --> 
<div id="main"> 
<!-- START WRAPPER --> 
<div class="wrapper"> 

<!-- START LEFT SIDEBAR NAV--> 
<aside id="left-sidebar-nav"> 
<ul id="slide-out" class="side-nav fixed leftside-navigation"> 
<li class="user-details cyan darken-2"> 
<div class="row"> 
<div class="col col s4 m4 l4"> 
<img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image"> 
</div> 
<div class="col col s8 m8 l8"> 
<ul id="profile-dropdown" class="dropdown-content"> 
<li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a> 
</li> 
<li><a href="#"><i class="mdi-action-settings"></i> Settings</a> 
</li> 
<li><a href="#"><i class="mdi-communication-live-help"></i> Help</a> 
</li> 
<li class="divider"></li> 
<li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a> 
</li> 
 <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        String sname=(String)request.getAttribute("sname");
                        System.out.println("Value passssssssssssssssssssssss  "+sid+" and "+sname);
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                     <li class="bold"><a href='<%=request.getContextPath()%>/app/all/schoolName?&sname=<%= sname %>&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                     							
                        </li> 															
                    
                   <!--  <li class="bold"><a href="javascript:void(0)" class="btn waves-effect waves-light right"  name="final" id="final"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li> -->	
                    
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/stuList?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/parent?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/parentView?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=calender/app-calendar' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/all/activityMain?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/all/teacherList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/all/NonTeacherList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach Staff</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/all/attendance?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <%-- <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/all/Examlist?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Marks </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/all/MarkSheet?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class wise Marks</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Finance</a>
                            <div class="collapsible-body">
                                <ul>
                                   <%--  <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_EChallan'>E-Challan</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeeDraft'>Fee Draft</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeesList'>Fee List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/all/FeeAllot?&admid=<%= sid %>' class="waves-effect waves-cyan">Mainpage</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                 	    
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> News Feed </a>
                			<div class="collapsible-body">
                                <ul>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/all/events_table?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/all/news_table?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                       
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=SMS/SmsOrEmail' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> SMS/Email</a>
                        </li>
</aside> 
<!-- END LEFT SIDEBAR NAV--> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START CONTENT --> 
<section id="content"> 

<!--breadcrumbs start--> 
<div id="breadcrumbs-wrapper"> 
<!-- Search for small screen --> 
<div class="header-search-wrapper grey hide-on-large-only"> 
<i class="mdi-action-search active"></i> 
<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"> 
</div> 
<div class="container"> 
<div class="row"> 
<div class="col s12 m12 l12"> 
<h5 class="breadcrumbs-title">Events</h5> 
<ol class="breadcrumbs"> 
<li><a href="index.html">Dashboard</a></li> 
<li class="active">Events</li> 
</ol> 
</div> 
</div> 
</div> 
</div> 
<!--breadcrumbs end--> 


<!-- 
<div class="container"> 
<textarea class="classy-editor"></textarea> -->

<!-- <div id="floatThead" class="cyan lighten-4"> --> 
 <div class="container">
 <form class="formValidate" id="formValidate" method="post">
<div class="row"> 
<h4> Add Events</h4>
 <div class="col s12 m6 l3"><p></p></div>
    <div class="col s12 m6 l3"><p></p></div>
    
	
    

</div> 
<div class="input-field col s12">
<label for="address2">Event Tittle</label>
<input id="event_tittle" name="event_tittle" maxlength="100" type="text">
</div>
<div class="input-field col s12">
<textarea id="event_desc" name="event_desc" class="materialize-textarea validate"></textarea>
<label for="event_desc"> Event Description</label>
</div>
 
<div class="input-field col s12">
<label for="address2">Event Place</label>
<input id="event_place" name="event_place" maxlength="100" type="text">
</div>

<div class="input-field col s12">
<a class="btn waves-effect waves-light" id="checkAll">For All</a>

<div>
  
<input class="first" id="option1" name="option1" type="checkbox" value="Student">
<label class="label1" for="Item 1">Student</label></br>
<input class="first" id="option2" name="option1" type="checkbox" value="Teacher">
<label class="label1" for="Item 2">Teacher</label></br>
<input class="first" id="option3" name="option1" type="checkbox" value="Non_Teaching_Staff">
<label class="label1" for="Item 3">Non Teaching Staff</label></br>


</div>
</div>
<br><br>
<div class="row">

 <div id="dob_date" class="input-field col s6 dob_date">
<label for="dob">Date *</label>
<input type="text" name="doe" id="doe" class="datepicker ad_date stu_require" data-error="Please enter your date of birth">
<div class="error errorTxt3"></div>
</div></div>
  <br>
  
  <pre id="output"></pre>

  <script src="../dist/js/datepicker.standalone.js" charset="utf-8"></script>

 

  
   <div class="col s12 m6 l3">
    	<!-- <button class="btn waves-effect waves-light " type="submit" name="action"> Save 
			<i class="mdi-content-send right"></i> 
		</button>  -->
		<a href="javascript:void(0)" class="btn waves-effect waves-light left" onClick="eventInsert()" name="stu" id="stu">Save</a>
	</div>

</section>

</form>
<div id="preselecting-tab" class="section"> 
<!--<h4 class="header">Preselecting a tab</h4>--> 

</div> 
<!--start container--> 


<!-- Floating Action Button --> 
<div class="fixed-action-btn" style="bottom: 50px; right: 19px;"> 
<a class="btn-floating btn-large"> 
<i class="mdi-action-stars"></i> 
</a> 
<ul> 
<li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li> 
<li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li> 
<li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li> 
<li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li> 
</ul> 
</div> 
<!-- Floating Action Button --> 
</div> 
</div> 
<!--end container--> 
</div> 
</section> 
<!-- END CONTENT --> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START RIGHT SIDEBAR NAV--> 
<aside id="right-sidebar-nav"> 
<ul id="chat-out" class="side-nav rightside-navigation"> 
<li class="li-hover"> 
<a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a> 
<div id="right-search" class="row"> 
 <div class="col s6"><h1>List Of Events</h1></div>

<form class="col s16"> 
<div class="input-field"> 
<i class="mdi-action-search prefix"></i> 
<input id="icon_prefix" type="text" class="validate"> 
<label for="icon_prefix">Search</label> 
</div> 
</form> 
</div> 
</li> 
<li class="li-hover"> 
<ul class="chat-collapsible" data-collapsible="expandable"> 
<li> 
<div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div> 
<div class="collapsible-body recent-activity"> 
<div class="recent-activity-list chat-out-list row"> 
<div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i> 
</div> 
<div class="col s9 recent-activity-list-text"> 
<a href="#">just now</a> 
<p>Jim Doe Purchased new equipments for zonal office.</p> 
</div> 
</div> 
<div class="recent-activity-list chat-out-list row"> 
<div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i> 
</div> 
<div class="col s9 recent-activity-list-text"> 
<a href="#">Yesterday</a> 
<p>Your Next flight for USA will be on 15th August 2015.</p> 
</div> 
</div> 
<div class="recent-activity-list chat-out-list row"> 
<div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i> 
</div> 
<div class="col s9 recent-activity-list-text"> 
<a href="#">5 Days Ago</a> 
<p>Natalya Parker Send you a voice mail for next conference.</p> 
</div> 
</div> 
<div class="recent-activity-list chat-out-list row"> 
<div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i> 
</div> 
<div class="col s9 recent-activity-list-text"> 
<a href="#">Last Week</a> 
<p>Jessy Jay open a new store at S.G Road.</p> 
</div> 
</div> 
<div class="recent-activity-list chat-out-list row"> 
<div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i> 
</div> 
<div class="col s9 recent-activity-list-text"> 
<a href="#">5 Days Ago</a> 
<p>Natalya Parker Send you a voice mail for next conference.</p> 
</div> 
</div> 
</div> 
</li> 
<li> 
<div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div> 
<div class="collapsible-body sales-repoart"> 
<div class="sales-repoart-list  chat-out-list row"> 
<div class="col s8">Target Salse</div> 
<div class="col s4"><span id="sales-line-1"></span> 
</div> 
</div> 
<div class="sales-repoart-list chat-out-list row"> 
<div class="col s8">Payment Due</div> 
<div class="col s4"><span id="sales-bar-1"></span> 
</div> 
</div> 
<div class="sales-repoart-list chat-out-list row"> 
<div class="col s8">Total Delivery</div> 
<div class="col s4"><span id="sales-line-2"></span> 
</div> 
</div> 
<div class="sales-repoart-list chat-out-list row"> 
<div class="col s8">Total Progress</div> 
<div class="col s4"><span id="sales-bar-2"></span> 
</div> 
</div> 
</div> 
</li> 
<li> 
<div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div> 
<div class="collapsible-body favorite-associates"> 
<div class="favorite-associate-list chat-out-list row"> 
<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> 
</div> 
<div class="col s8"> 
<p>Eileen Sideways</p> 
<p class="place">Los Angeles, CA</p> 
</div> 
</div> 
<div class="favorite-associate-list chat-out-list row"> 
<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> 
</div> 
<div class="col s8"> 
<p>Zaham Sindil</p> 
<p class="place">San Francisco, CA</p> 
</div> 
</div> 
<div class="favorite-associate-list chat-out-list row"> 
<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image"> 
</div> 
<div class="col s8"> 
<p>Renov Leongal</p> 
<p class="place">Cebu City, Philippines</p> 
</div> 
</div> 
<div class="favorite-associate-list chat-out-list row"> 
<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> 
</div> 
<div class="col s8"> 
<p>Weno Carasbong</p> 
<p>Tokyo, Japan</p> 
</div> 
</div> 
<div class="favorite-associate-list chat-out-list row"> 
<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image"> 
</div> 
<div class="col s8"> 
<p>Nusja Nawancali</p> 
<p class="place">Bangkok, Thailand</p> 
</div> 
</div> 
</div> 
</li> 
</ul> 
</li> 
</ul> 
</aside> 
<!-- LEFT RIGHT SIDEBAR NAV--> 

</div> 
</div> 
<!-- END WRAPPER --> 

</div> 
<!-- END MAIN --> 
<div id="modal4" class="modal modal-fixed-footer green white-text stu_list_pop_box"> 
<div class="modal-content"> 
<div id="bordered-table"> 

<h4 class="header left">Student Name</h4> 
<h4 class="header right">Academic Year</h4> 
<hr style="clear:both;"/> 

<div class="row"> 

<div class="col s12 m4 l3"> 
<img title="Avatar" src="../../resources/images/avatar.jpg"> 
</div> 

<div class="col s12 m8 l9"> 
<table class="bordered"> 
<th class="stu_details center" data-field="id">Student Details</th> 
<tr> 
<td>Class:</td> 
</tr> 
<tr> 
<td>Roll Number:</td> 
</tr> 
</table> 
</div> 
</div> 

<div class="col s12 m4 l3"> 
<table class="bordered"> 
<tr> 
<td>Birth Date:</td> 
</tr> 
<tr> 
<td>Gender:</td> 
</tr> 
<tr> 
<td>Nationality:</td> 
</tr> 
<tr> 
<td>Religion:</td> 
</tr> 
<tr> 
<td>Community:</td> 
</tr> 
<tr> 
<td>Place of Birth:</td> 
</tr> 
<tr> 
<td>Mother Tongue:</td> 
</tr> 
<tr> 
<td>Date of Joining:</td> 
</tr> 
<tr> 
<td>Permanent Address:</td> 
<tr> 
<td>Communication Address:</td> 
</tr> 
<tr> 
<td>Phone Number:</td> 
</tr> 
<th class="stu_details center" data-field="id">Previous Schools</th> 
<tr> 
<td>School Name:</td> 
</tr> 
<tr> 
<td>Years:</td> 
</tr> 
<tr> 
<td>Standard:</td> 
</tr> 
<th class="stu_details center" data-field="id">Parents Details</th> 
<tr> 
<td>Father Name:</td> 
</tr> 
<tr> 
<td>Mother Name:</td> 
</tr> 
<tr> 
<td>Father Occupation:</td> 
</tr> 
<tr> 
<td>Father Office Address:</td> 
<tr> 
<tr> 
<td>Office Phone Number:</td> 
</tr> 
<td>Mother Occupation:</td> 
</tr> 
<tr> 
<td>Mother office Address</td> 
</tr> 
<tr> 
<td>Office Phone Number:</td> 
</tr> 
<th class="stu_details center" data-field="id">Guardian Details</th> 
<tr> 
<td>Guardian Name:</td> 
</tr> 
<tr> 
<td>Guardian Relation:</td> 
</tr> 
<tr> 
<td>Guardian Address:</td> 
<tr> 
<td>Guardian Occupation:</td> 
</tr> 
<tr> 
<td>Guardian Office address:</td> 
</tr> 
<tr> 
<td>Office Phone Number:</td> 
</tr> 
</tr> 
<th class="stu_details center" data-field="id">Medical Details</th> 
<tr> 
<td>Blood Group:</td> 
</tr> 
<tr> 
<td>Height:</td> 
</tr> 
<tr> 
<td>Weight:</td> 
</tr> 
<tr> 
<td>Allergy:</td> 
</tr> 
<tr> 
<td>Diseases:</td> 
</tr> 
<tr> 
<td>Special Child:</td> 
</tr> 
<tr> 
<td>Physically Challenged:</td> 
</tr> 
<tr> 
<td>Medical Condition:</td> 
</tr> 
<th class="stu_details center" data-field="id">Transport Detials</th> 
<tr> 
<td>Transport Name:</td> 
</tr> 
<tr> 
<td>Boarding Point:</td> 
</tr> 
<th class="stu_details center" data-field="id">Hostel Detials</th> 
<tr> 
<td>Hostel Name:</td> 
</tr> 
<tr> 
<td>Room Number:</td> 
</tr> 
</table> 
</div> 
</div> 
</div> 
</div> 
</div> 


<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START FOOTER --> 
<footer class="page-footer"> 
<div class="footer-copyright"> 
<div class="container"> 
<span>Copyright ɠ2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span> 
</div> 
</div> 
</footer> 
<!-- END FOOTER --> 



<!-- ================================================ 
Scripts 
================================================ --> 

<!-- jQuery Library --> 
<script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
<!--materialize js--> 
<script type="text/javascript" src="../../js/materialize.min.js"></script> 
<!--prism--> 
<script type="text/javascript" src="../../js/plugins/prism/prism.js"></script> 
<!--scrollbar--> 
<script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- data-tables --> 
<script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script> 
<!-- chartist --> 
<script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   

<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="../../js/plugins.min.js"></script> 
<!--custom-script.js - Add your own theme custom JS--> 
<script type="text/javascript" src="../../js/custom-script.js"></script> 
<script type="text/javascript"> 

/*Show entries on click hide*/ 

$(document).ready(function(){ 
$(".dropdown-content.select-dropdown li").on( "click", function() { 
var that = this; 
setTimeout(function(){ 
if($(that).parent().hasClass('active')){ 
$(that).parent().removeClass('active'); 
$(that).parent().hide(); 
} 
},100); 
}); 

/*** STUDENT LIST INNER DROP DOWN ***/ 

$('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){ 
event.preventDefault(); 
$(this).parent().find('ul').addClass("class_active"); 
}); 

$('html').click(function(e) { 
var a = e.target; 
if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0) 
{ 
$('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
} 
}); 

$('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){ 
event.preventDefault(); 
$('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
}); 

/*** DELETE "ENTRIES" TEXT ***/ 

$('.dataTables_length label').contents().eq(2).replaceWith(''); 
$('.dataTables_length label').contents().eq(4).replaceWith(''); 
$('.dataTables_length label').contents().eq(6).replaceWith(''); 

/*** STUDENT PROFILE POP UP ***/ 

$('#student_prof_pop_01').click(function(event){ 
event.preventDefault(); 
}); 

}); 
$(document).ready(function() {
// Below code is used to remove all check property if,
// User select/unselect options with class first options.
$(".first").click(function() {
$("#checkAll").attr("data-type", "uncheck");
});
// Below code is used to remove all check property if,
// User select/unselect options with name=option2 options.
$("input[name=option2]").click(function() {
$("#selectall").prop("checked", false);
});
/////////////////////////////////////////////////////////////
// JS for Check/Uncheck all CheckBoxes by Button //
/////////////////////////////////////////////////////////////
$("#checkAll").attr("data-type", "check");
$("#checkAll").click(function() {
if ($("#checkAll").attr("data-type") === "check") {
$(".first").prop("checked", true);
$("#checkAll").attr("data-type", "uncheck");
} else {
$(".first").prop("checked", false);
$("#checkAll").attr("data-type", "check");
}
})
/////////////////////////////////////////////////////////////
// JS for Check/Uncheck all CheckBoxes by Checkbox //
/////////////////////////////////////////////////////////////
$("#selectall").click(function() {
$(".second").prop("checked", $("#selectall").prop("checked"))
})
});

$('.datepicker').pickadate({ 
	selectMonths: true, // Creates a dropdown to control month 
	selectYears: 15 // Creates a dropdown of 15 years to control year 
	}); 

	
</script> 
<script> var ctxPath = "<%=request.getContextPath() %>";</script>

   <script>
function eventInsert()
	{
		//alert("event Insert entered111111111111111");
		var dt=new Date();
		
		 var stuval,techval,nonval;
		 if(option1.checked==true)
			 {
			 	stuval=$("#option1").val();
			 }
		 	else
			 {
				stuval="NIL"; 
			 }
		 if(option2.checked==true)
		 {
		 	techval=$("#option2").val();
		 }
	 	else
		 {
			techval="NIL"; 
		 }
		 if(option3.checked==true)
		 {
		 	nonval=$("#option3").val();
		 }
	 	else
		 {
			nonval="NIL"; 
		 }
		 //alert("value 1  "+stuval+" value 2  "+techval+"  value 3  "+nonval);
		
		$.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/noticeboard/insertEvent.do?',
	        data: {"event_tittle":$("#event_tittle").val(),"event_desc":$("#event_desc").val(),"event_place":$("#event_place").val(),
	        	   "student":stuval,"teacher":techval,"non_teach_staff":nonval,"doe":$("#doe").val(),
	        	   "isActive":'Y',"inserted_By":$("#loguser1").val(),
		        	"inserted_Date":dt,"updated_By":$("#loguser1").val(),"updated_Date":dt	   
	        },
	        success: function(data) 
	        {
		if(data=='success')
		 {
			Materialize.toast("Success",4000);	
		 }
		else
		 {
			Materialize.toast(data,4000);	
		 }  
	        }
		 });
	}

   
   </script>

</body> 

</html>

