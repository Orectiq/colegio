<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Teachers Main Page - Colegio - A Orectiq Product</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
    <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<style>
  .class_active{display:block!important;opacity:1!important;}
  .teacher_class{display:none;}
  .teacher_class_section{display:none;}
  .teacher_subject{display:none;}
  .student_class_section_sbt{display:none;}
</style>
<style type="text/css">
    body
    {
        font-family: arial;
    }

    th,td
    {
        margin: 0;
        text-align: center;
        border-collapse: collapse;
        outline: 1px solid #e3e3e3;
    }

    td
    {
        padding: 5px 10px;
    }

    th
    {
        background: #666;
        color: white;
        padding: 5px 10px;
    }

    td:hover
    {
        cursor: pointer;
        background: #666;
        color: white;
    }
	
	
	
	@import "compass/css3";

.table-editable {
  position: relative;
  
  .glyphicon {
    font-size: 20px;
  }
}

.table-remove {
  color: #700;
  cursor: pointer;
  
  &:hover {
    color: #f00;
  }
}

.table-up, .table-down {
  color: #007;
  cursor: pointer;
  
  &:hover {
    color: #00f;
  }
}

.table-add {
  color: #070;
  cursor: pointer;
  position: absolute;
  top: 8px;
  right: 0;
  
  &:hover {
    color: #0b0;
  }
}
    </style>

</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                              <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        String sname=(String)request.getAttribute("sname");
                        System.out.println("Value passssssssssssssssssssssss  "+sid+" and "+sname);
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                     <li class="bold"><a href='<%=request.getContextPath()%>/app/all/schoolName?&sname=<%= sname %>&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                     							
                        </li> 															
                    
                   <!--  <li class="bold"><a href="javascript:void(0)" class="btn waves-effect waves-light right"  name="final" id="final"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li> -->	
                    
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/stuList?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/parent?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/parentView?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=calender/app-calendar' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/all/activityMain?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/all/teacherList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/all/NonTeacherList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach Staff</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/all/attendance?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <%-- <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/all/Examlist?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Marks </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/all/MarkSheet?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class wise Marks</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Finance</a>
                            <div class="collapsible-body">
                                <ul>
                                   <%--  <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_EChallan'>E-Challan</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeeDraft'>Fee Draft</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeesList'>Fee List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/all/FeeAllot?&admid=<%= sid %>' class="waves-effect waves-cyan">Mainpage</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                 	    
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> News Feed </a>
                			<div class="collapsible-body">
                                <ul>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/all/events_table?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/all/news_table?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                       
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=SMS/SmsOrEmail' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> SMS/Email</a>
                        </li>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Class Name</h5>
                <ol class="breadcrumbs">
                    <li><a href="../../index.html">Dashboard</a></li>
                    <li class="active">Class</li>
					<li class="active">Class Name</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

        
          <!-- Form Start -->
         <!--  <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4 z-depth-6 std_cls_sec_selection">
              <div class="card-panel">
                <!<h4 class="header2">Form Advance</h4>
                <div class="row">
                  <form class="col s12">
                    <div class="row">
                      <div class="input-field col s12 student_class">
                        <select>
                          <option value="" disabled selected>Choose your Class</option>
                          <option value="1">1st Standard</option>
                          <option value="2">2nd Standard</option>
                          <option value="3">3rd Standard</option>
                        </select>
                        <label>Select Your Class</label>
                      </div>         
                      <div class="input-field col s12 student_class_section">
                        <select>
                          <option value="" disabled selected>Choose your Section</option>
                          <option value="1">Section 1</option>
                          <option value="2">Section 2</option>
                          <option value="3">Section 3</option>
                        </select>
                        <label>Select Your Section</label>
                      </div>
                      <div class="input-field col s12 student_class_section_sbt">
                        <button class="btn waves-effect waves-light right submit" type="submit" name="action">Submit
                          <i class="mdi-content-send right"></i>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>-->
		  

		  <div class="container">
          <div class="section">
		 
					
		  
             <div class="row right">
                    <div class="col m6">
                      <a href='<%=request.getContextPath()%>/app/all/addActivity?&admid=<%= sid %>' class="btn waves-effect waves-light"  name="search" id="Search">AddActivity</a> 
                    </div>
                </div> 
					<br><br>
	  
	  <div class="row">
                  <div class="input-field col s6">
                     <label for="SClass1"></label>
										<select class="browser-default stu_require" id="SClass1" name="SClass1" data-error="Please select your Class" onClick="getSection()">
											<option value="NIL">Please select your Class</option>
											
										</select>
                  </div>
                
             
               
                  <div class="input-field col s6">
                   <label for="Section1"></label>
										<select class="browser-default stu_require" id="Section1" name="Section1" data-error="Please select your Section" onChange="showList()">
											<option value="NIL">Please select your Section</option>
											
										</select>
                  </div>
                </div>
                
			<div class="row">
                  <div class="input-field col s6">
                  		<label for="lname">Today</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
						<input id="today" name="today" type="text" maxlength="50" readonly>
                  </div>
                  
                  <div class="input-field col s6">
                  
                  </div>
            </div>
            
                  
	  <div class="row">
	  <br>
        <div class="col  m4">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Teachers</span>
			  <div class="card-action">
              <a href="#">Class Teacher</a><br>
              <a href="#">Associated Teachers List</a>
            </div>
             
            </div>
            
          </div>
        </div>
     
         <div class="col  m4">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Attendance</span>
			  <div class="card-action">
              <a href="#">Total</a><br>
              <a href="#">No Of Present</a>
            </div>
             
            </div>
            
          </div>
        </div>
      
        <div class="col  m4">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">MarkSheets/Leader Board</span><br>
			 
			  <div class="card-action">
              <a href="#">Class Topper</a><br>
              <a href="#">MarkObtained/Total</a>
            </div>
             
            </div>
            
          </div>
        </div>
      </div>
	  
	  
	  
	  <div class="row">
        <div class="col  m4">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Time Table</span>
			  <div class="card-action">
              <a href="#">Current Period</a><br>
              <a href="#">Next Period</a>
            </div>
             
            </div>
            
          </div>
        </div>
     
        <div class="col  m4">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Achievement</span>
			  <div class="card-action">
              <a href="#">Activity Name</a><br>
              <a href="#">Brief Description</a>
            </div>
             
            </div>
            
          </div>
        </div>
     
         <div class="col  m4">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Sports</span>
			  <div class="card-action">
              <a href="#">Activity Name</a><br>
              <a href="#">Brief Description</a>
            </div>
             
            </div>
            
          </div>
        </div>
      </div>
	  
	  
	  
               
              <div id="submit-button" class="section">
                  <div class="row">
                    <div class="col s12 m8 l9 right">
                      <button class="btn waves-effect waves-light right" type="submit" name="action">Send
                        <i class="mdi-content-send right"></i>
                      </button>
                    </div>
                  </div>
                </div>
				
				
				<div class="container">
 
  
  <div id="table" class="table-editable">
    <span class="table-add glyphicon glyphicon-plus"></span>
    <table class="table">
      <tr>
        <th>Time</th>
        <th>Monday</th>
        <th>Tuesday</th>
        <th>Wednesday</th>
        <th>Thrusday</th>
        <th>Friday</th>
        <th>Saturday</th>
        
      </tr>
      <tr>
        <th contenteditable="true">10:00 - 11:00</th>
        <td contenteditable="true">Physics-1</td>
		<td contenteditable="true">English</td>
        <td contenteditable="true">Tamil</td>
		<td contenteditable="true">Chemestry-1</td>
        <td contenteditable="true">Alzebra</td>
		<td contenteditable="true">Alzebra</td>
				
      </tr>
	  <tr>
        <th contenteditable="true">11:00 - 12:00</th>
        <td contenteditable="true">Physics-1</td>
		<td contenteditable="true">English</td>
        <td contenteditable="true">Tamil</td>
		<td contenteditable="true">Chemestry-1</td>
        <td contenteditable="true">Alzebra</td>
		<td contenteditable="true">Alzebra</td>
				
      </tr>
	  
	  <tr>
        <th contenteditable="true">12:00 - 01:00</th>
        <td contenteditable="true">Physics-1</td>
		<td contenteditable="true">English</td>
        <td contenteditable="true">Tamil</td>
		<td contenteditable="true">Chemestry-1</td>
        <td contenteditable="true">Alzebra</td>
		<td contenteditable="true">Alzebra</td>
				
      </tr>
	  
	  <tr>
        <th contenteditable="true">02:00 - 03:00</th>
        <td contenteditable="true">Physics-1</td>
		<td contenteditable="true">English</td>
        <td contenteditable="true">Tamil</td>
		<td contenteditable="true">Chemestry-1</td>
        <td contenteditable="true">Alzebra</td>
		<td contenteditable="true">Alzebra</td>
				
      </tr>
	  
	  <tr>
        <th contenteditable="true">04:00 - 05:00</th>
        <td contenteditable="true">Physics-1</td>
		<td contenteditable="true">English</td>
        <td contenteditable="true">Tamil</td>
		<td contenteditable="true">Chemestry-1</td>
        <td contenteditable="true">Alzebra</td>
		<td contenteditable="true">Alzebra</td>
				
      </tr>
      <!-- This is our clonable table line -->
      <tr class="hide">
        <td contenteditable="true">Untitled</td>
        <td contenteditable="true">undefined</td>
        <td>
          <span class="table-remove glyphicon glyphicon-remove"></span>
        </td>
        <td>
          <span class="table-up glyphicon glyphicon-arrow-up"></span>
          <span class="table-down glyphicon glyphicon-arrow-down"></span>
        </td>
      </tr>
    </table>
  </div>
  
  
</div>
				

<div ="row">
 <div class="col s12 m8 l9 right">
                      <a class="waves-effect waves-light  btn">Edit</a>
                      <a class="waves-effect waves-light  btn">Save</a>
                    </div>
					</div>
					<br>
				
				<!--start container-->
        
          <!-- Form End -->
          <!-- Floating Action Button -->
            <!-- <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                <a class="btn-floating btn-large">
                  <i class="mdi-action-stars"></i>
                </a>
                <ul>
                  <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                  <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                  <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                  <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
                </ul>
            </div> -->
            <!-- Floating Action Button -->
        </div>
        <!--end container-->
      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START RIGHT SIDEBAR NAV-->
      <aside id="right-sidebar-nav">
        <ul id="chat-out" class="side-nav rightside-navigation">
            <li class="li-hover">
            <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
            <div id="right-search" class="row">
                <form class="col s12">
                    <div class="input-field">
                        <i class="mdi-action-search prefix"></i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Search</label>
                    </div>
                </form>
            </div>
            </li>
            <li class="li-hover">
                <ul class="chat-collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                    <div class="collapsible-body recent-activity">
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">just now</a>
                                <p>Jim Doe Purchased new equipments for zonal office.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Yesterday</a>
                                <p>Your Next flight for USA will be on 15th August 2015.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Last Week</a>
                                <p>Jessy Jay open a new store at S.G Road.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                    <div class="collapsible-body sales-repoart">
                        <div class="sales-repoart-list  chat-out-list row">
                            <div class="col s8">Target Salse</div>
                            <div class="col s4"><span id="sales-line-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Payment Due</div>
                            <div class="col s4"><span id="sales-bar-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Delivery</div>
                            <div class="col s4"><span id="sales-line-2"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Progress</div>
                            <div class="col s4"><span id="sales-bar-2"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                    <div class="collapsible-body favorite-associates">
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Eileen Sideways</p>
                                <p class="place">Los Angeles, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Zaham Sindil</p>
                                <p class="place">San Francisco, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Renov Leongal</p>
                                <p class="place">Cebu City, Philippines</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Weno Carasbong</p>
                                <p>Tokyo, Japan</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Nusja Nawancali</p>
                                <p class="place">Bangkok, Thailand</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </li>
        </ul>
      </aside>
      <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->



  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright � 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
    </div>
  </footer>
  <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism
    <script type="text/javascript" src="js/prism/prism.js"></script>-->
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    <script type="text/javascript" src="../../js/custom-script.js"></script>
   <!-- <script type="text/javascript">
      $(document).ready(function(event){

        if($(window).width()>1024){
          $('section#content').css('height', ($(window).height()) - ($('#breadcrumbs-wrapper .container').height()) - 36);
        }

        $('.student_class .select-dropdown').click(function(event){
            event.preventDefault();
            $(this).parent().find('ul').addClass("class_active");
        });

        $('html, .student_class ul.select-dropdown li').click(function(e) {
            var a = e.target;
              $('.student_class ul.select-dropdown').removeClass("class_active"); 
              if ($(a).parents('.student_class ul.select-dropdown').length === 0)
              {
                $('.student_class ul.select-dropdown').removeClass("class_active"); 
              }
        });

        $('.student_class ul.select-dropdown li').click(function(event){
            event.preventDefault();
             if($('.student_class ul.select-dropdown li').hasClass('active')){
              $('.student_class_section').show();
             }
        });

        $('.student_class_section .select-dropdown').click(function(event){
            event.preventDefault();
            $(this).parent().find('ul').addClass("class_active");
        });

        $('html, .student_class_section ul.select-dropdown li').click(function(e) {
            var a = e.target;
              $('.student_class_section ul.select-dropdown').removeClass("class_active"); 
              if ($(a).parents('.student_class_section ul.select-dropdown').length === 0)
              {
                $('.student_class_section ul.select-dropdown').removeClass("class_active"); 
              }
        });

        $('.student_class_section ul.select-dropdown li').click(function(event){
            event.preventDefault();
             if($('.student_class_section ul.select-dropdown li').hasClass('active')){
              $('.student_class_section_sbt').show();
             }
        });

      });
    </script>-->
    
    <script>
    	retrieve1();
    	
    	//*****   Start   Class and Section Code   ******// 
   	 retrieve();
   	 function retrieve()
   	 {
   	 	  
   	 	   $.ajax({
   	 			  type: "GET",
   	 			  url: ctxPath+'/app/subject/getSClass.do?',
   	 			  dataType: 'json',
   	 			}).done(function( responseJson )
   	 					{	
   	 						loadDataIntoClass(responseJson);
   	 				});
   	 }
   	 function loadDataIntoClass(responseJson)
   	 {
   	 	var tblHtml = "";
   	 	var sclass;
   	 	 var sel = $("#SClass1").empty();
   	 	 $.each(responseJson.sectionServiceVOList, function(index, val)
   	 	 {
   	 		sclass=val.sclass;
   	 		sel.append('<option value="' + sclass + '">' + sclass + '</option>');
   	 	 }); 
   	 }
   	 
   	 function getSection()
   	 {
   		 var sclass=$("#SClass1").val();
   		   $.ajax({
   				  type: "GET",
   				  url: ctxPath+'/app/subject/getSection.do?',
   				  data: {"sclass":sclass},
   				  dataType: 'json',
   				}).done(function( responseJson )
   						{	
   							loadDataIntoSection(responseJson);
   					});
   	}
   	function loadDataIntoSection(responseJson)
   	{
   		var tblHtml = "";
   		var sec;
   		 var sel = $("#Section1").empty();
   		 $.each(responseJson.sectionServiceVOList, function(index, val)
   		 {
   			sec=val.section;
   			sel.append('<option value="' + sec + '">' + sec + '</option>');
   		 }); 
   	}

   	//*****   End   Class and Section Code   ******//
    	
    	
    	function retrieve1()
    	{
    		
    		var dt=new Date();
    		$('#today').val(dt);
    	}
    	function showList()
    	{
    		 var sclass=$('#SClass1').val();
    		 var sec=$('#Section1').val();
				//alert("class  "+sclass+" sec  "+sec);
				$.ajax({
    				  type: "GET",
    				  url: ctxPath+'/app/student/totalStudent.do?',
    				  data: {"sclass":sclass,"section":sec},
    				  dataType: 'json',
    				}).done(function( responseJson )
    						{	 
    							//alert(responseJson.object);
    							/* //alert("before  "+responseJson);
    						    loadDataIntoTable(responseJson);
    						   //alert("after"); */
    						   
    				
    					});	    		
				//console.dir($ajax.responseJSON);
    	}
    	
    	 function loadDataIntoTable(responseJson)
    	  {
    		 //alert("enter table  ");
    	  	 var tblHtml = "";
    	  	 $.each(responseJson.studentServiceVOList, function(index, val)
    	  	{
    	  		 //alert("enter each");
    	  		//alert(val.total);	 
    	  	});
    	  }
    	  	
    
    </script>
    
    
</body>

</html>