<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>lIBRARY - Colegio - A Orectiq Product</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        String sname=(String)request.getAttribute("sname");
                        System.out.println("Value passssssssssssssssssssssss  "+sid+" and "+sname);
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                     <li class="bold"><a href='<%=request.getContextPath()%>/app/all/schoolName?&sname=<%= sname %>&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                     							
                        </li> 															
                    
                   <!--  <li class="bold"><a href="javascript:void(0)" class="btn waves-effect waves-light right"  name="final" id="final"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li> -->	
                    
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/stuList?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/parent?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/parentView?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=calender/app-calendar' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/all/activityMain?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/all/teacherList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/all/NonTeacherList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach Staff</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/all/attendance?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <%-- <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/all/Examlist?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Marks </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/all/MarkSheet?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class wise Marks</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Finance</a>
                            <div class="collapsible-body">
                                <ul>
                                   <%--  <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_EChallan'>E-Challan</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeeDraft'>Fee Draft</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeesList'>Fee List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/all/FeeAllot?&admid=<%= sid %>' class="waves-effect waves-cyan">Mainpage</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                 	    
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> News Feed </a>
                			<div class="collapsible-body">
                                <ul>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/all/events_table?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/all/news_table?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                       
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=SMS/SmsOrEmail' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> SMS/Email</a>
                        </li>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
                    <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Marks Entry</h5>
               </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

    <div class="container">
		<!--<div id="floatThead" class="cyan lighten-4">-->
	
 <!--Preselecting a tab-->
          <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
                  <div class="col s12">
                    <ul class="tabs tab-demo-active z-depth-1 cyan">
                      
						<li class="tab col s6"><a class="white-text waves-effect waves-light active" href="#"></a>
						</li>					  
						<!-- <li class="tab col s6"><a class="white-text waves-effect waves-light active" href="#">Class</a>
						</li> -->
				   </ul>
                  </div>
                  
                 
                
                  <div class="col s12">
                    <div id="stu_list_all" class="col s12  cyan lighten-4">
                     		<!--DataTables example-->
				            <div id="#student-datatables_1">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">

				               <form  id="slick-login" method="post">
				              
				              
				              <%--  <div id="submit-button" class="section">
                  					<div class="row right">
                    					<div class="col s12 m12 l12">
                      							<a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/AddActivity' class="btn waves-effect waves-light"  name="search" id="Search">Add Activity</a> 
                    					</div>
                  					</div>
                				</div> --%>
                				
                				
<div class="col m6  "> 
<div class="input-field col m6" > 
<select class="browser-default stu_require" id="ddlView" name="ddlView" data-error="Please select your Class" onClick="getSection()">
										    <option value="" disabled selected>Choose your Class</option> 
											
										</select>
<!-- <label>Select Class</label>  -->
</div> 
</div> 
<div class="col m6  "> 
<div class="input-field col m6" > 
									<select class="browser-default stu_require" id="ddlView1" name="ddlView1" data-error="Please select your Section">
											<option value="NIL">Please select your Section</option>
											
										</select>
<!-- <label>Select Subject</label>  -->
</div> 
</div> 
<div class="col m6  "> 
<div class="input-field col m6" > 

<select class="browser-default stu_require" id="ddlView2" name="ddlView2" data-error="Please select your Exam Type" onChange="selectCheckBox1()">
											<option value="NIL">Please select your Exam Type</option>
										</select>
<!-- <label>Select Type</label>  -->
</div> 
</div> 



<!-- <div class="col s12 m8 l9 right">
                      <a class="waves-effect waves-light  btn" name="goto"  onClick="selectCheckBox()" value="Validate">Exam List</a>
                    </div> -->

								 
				                 <table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0">

				                    <thead>
				                        <tr>
				                            <th>S.NO</th>
				                            <th>Exam Id</th>
				                            <th>Exam Name</th>
				                            <th> Subject </th>
				                            <th>Date</th>
				                            <th> Subject </th>
				                            <th>Date</th>
				                            <th> Subject </th>
				                            <th>Date</th>
				                            <th> Subject </th>
				                            <th>Date</th>
				                            <th> Subject </th>
				                            <th>Date</th>
				                            <th>Class</th>
				                            <th>Operations</th>
				                        </tr>
				                    </thead>
				                 
				                    <tfoot>
				                        
				                    </tfoot>
				                 
				                    <tbody>

				                    </tbody>
				                        
				                        	
				                    </tbody>
				                  </table>
				                  <input id="examid" name="examid" type="hidden" size="45" tabindex="5" disabled  />
				                  
				                  </form>
				                </div>
				              </div>
				            </div>  
							</div>  
				            <br>
				            <div class="divider"></div> 
			   

			  
            </div>
            
          </div>
        </div>
		</div>
        </div>
		
        <!--end container-->
					
  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
    <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });

        });
		
		
		
	function selectCheckBox() 
{ 
  var e = document.getElementById("ddlView"); 
var optionSelIndex = e.options[e.selectedIndex].value; 
var optionSelectedText = e.options[e.selectedIndex].text; 
if (optionSelIndex == 0) { 
////alert("Please select a Class"); 
} 


var e = document.getElementById("ddlView1"); 
var optionSelIndex = e.options[e.selectedIndex].value; 
var optionSelectedText = e.options[e.selectedIndex].text; 
if (optionSelIndex == 0) { 
////alert("Please select a Section"); 
} 


var e = document.getElementById("ddlView2"); 
var optionSelIndex = e.options[e.selectedIndex].value; 
var optionSelectedText = e.options[e.selectedIndex].text; 
if (optionSelIndex == 0) { 
////alert("Please select a Exam Type"); 
} 


}

    </script>
    
    
    <script>
    //retrieve();  ddlView
    
    //*****   Start   Class and Section Code   ******// 
	 retrieve();
	 function retrieve()
	 {
	 	  
	 	   $.ajax({
	 			  type: "GET",
	 			  url: ctxPath+'/app/subject/getSClass.do?',
	 			  dataType: 'json',
	 			}).done(function( responseJson )
	 					{	
	 						loadDataIntoClass(responseJson);
	 				});
	 	   //ddlView2
	 	  $.ajax({
 			  type: "GET",
 			  url: ctxPath+'/app/exam/getExamType.do?',
 			  dataType: 'json',
 			}).done(function( responseJson )
 					{	
 						loadDataIntoExamType(responseJson);
 				});
	 }
	 function loadDataIntoExamType(responseJson)
	 {
	 	var tblHtml = "";
	 	var etype;
	 	 var sel = $("#ddlView2").empty();
	 	 $.each(responseJson.examtypeServiceVOList, function(index, val)
	 	 {
	 		etype=val.exam_type;
	 		sel.append('<option value="' + etype + '">' + etype + '</option>');
	 	 }); 
	 }
	 
	 function loadDataIntoClass(responseJson)
	 {
	 	var tblHtml = "";
	 	var sclass;
	 	 var sel = $("#ddlView").empty();
	 	 $.each(responseJson.sectionServiceVOList, function(index, val)
	 	 {
	 		sclass=val.sclass;
	 		sel.append('<option value="' + sclass + '">' + sclass + '</option>');
	 	 }); 
	 }
	 
	 function getSection()
	 {
		 var sclass=$("#ddlView").val();
		   $.ajax({
				  type: "GET",
				  url: ctxPath+'/app/subject/getSection.do?',
				  data: {"sclass":sclass},
				  dataType: 'json',
				}).done(function( responseJson )
						{	
							loadDataIntoSection(responseJson);
					});
	}
	function loadDataIntoSection(responseJson)
	{
		var tblHtml = "";
		var sec;
		 var sel = $("#ddlView1").empty();
		 $.each(responseJson.sectionServiceVOList, function(index, val)
		 {
			sec=val.section;
			sel.append('<option value="' + sec + '">' + sec + '</option>');
		 }); 
	}

	//*****   End   Class and Section Code   ******//
    
    
    
    function selectCheckBox1()
      {
    	var sclass=$('#ddlView').val();
		var sec=$('#ddlView1').val();
		var etype=$('#ddlView2').val();
		//////alert("class   "+sclass+" sec  "+sec+"  etype  "+etype);
      	$.ajax({
      			  type: "GET",
      			  url: ctxPath+'/app/exam/getExam.do?',
      			  data: {"E_Class":sclass,"Section":sec,"Exam_Type":etype},
      			  dataType: 'json',
      			}).done(function( responseJson )
      					{	
       						loadDataIntoTable(responseJson);
      				});
      }

    function loadDataIntoTable(responseJson)
      {
    	//////alert("enter table ");
    	var tblHtml = "";
      	 $.each(responseJson.examServiceVOList, function(index, val)
      	{
      		//////alert("enter for each ");
      		var Exam_Id=val.exam_Id;
      		var Exam_Name=val.exam_Name;
      		var Etype=val.exam_Type;
      		var Sclass=val.e_Class;
      		var sec=val.section;
      		
      		
      		var sub1=val.subject1;
      		var Date1=val.e_Date1;
      		var sub2=val.subject2;
      		var Date2=val.e_Date2;
      		var sub3=val.subject3;
      		var Date3=val.e_Date3;
      		var sub4=val.subject4;
      		var Date4=val.e_Date4;
      		var sub5=val.subject5;
      		var Date5=val.e_Date5;
      		
      		$('#examid').val(Exam_Id);
      		
			//////alert("Exam type  "+Etype);

     tblHtml += '<tr><td style="text-align: left" id='+Exam_Id+'onClick="getExamListAll(id)">'+(index+1)+'</a></td>'+ 
     '<td style="text-align: left" value='+Exam_Id+' ondblclick="markEntry(\'' +Exam_Id+ '\',\'' +Exam_Name+ '\',\'' +Sclass+ '\',\'' +Etype+'\',\'' +sec+ '\')">'+Exam_Id+'</td>'+ 
    '<td style="text-align: left" value='+Exam_Name+' ondblclick="getExamListAll(this)">'+Exam_Name+'</td>'+
    '<td style="text-align: left">'+sub1+'</td><td style="text-align: left">'+Date1+'</td>'+
    '<td style="text-align: left">'+sub2+'</td><td style="text-align: left">'+Date2+'</td>'+
    '<td style="text-align: left">'+sub3+'</td><td style="text-align: left">'+Date3+'</td>'+
    '<td style="text-align: left">'+sub4+'</td><td style="text-align: left">'+Date4+'</td>'+
    '<td style="text-align: left">'+sub5+'</td><td style="text-align: left">'+Date5+'</td>'+
    '<td style="text-align: left">'+Sclass+'</td> <td><a class="btn-floating blue btn tooltipped" data-position="top" data-delay="0" data-tooltip="Add Exam" onClick="examEdit(id,name)" name="examid" id='+Exam_Id+'><i class="large mdi-editor-border-color"></i></a></tr>';


      	            
      	 });   
    	
      	 $("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
      
      }

    
    function markEntry(val1,val2,val3,val4,val5)
	{

		   //////alert("Student Edit List entered111111111111111  value1   "+val1+" and val2   "+val2+" and val3   "+val3+" and val4   "+val4+" and val5  "+val5);
		   
		   document.getElementById('slick-login').action = ctxPath+'/app/student/markEntry?&obj1='+val1+'&obj2='+val2+'&obj3='+val3+'&obj4='+val4+'&obj5='+val5+'';
	  	   document.getElementById('slick-login').submit();
	}	  
    
    </script>
</body>

</html>