-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: orectiqcolegio.cosbet91s3qp.ap-south-1.rds.amazonaws.com    Database: colegio_devp
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `absence_request`
--

DROP TABLE IF EXISTS `absence_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `absence_request` (
  `theDate` varchar(100) DEFAULT NULL,
  `Teacher_Id` varchar(45) DEFAULT NULL,
  `Teacher_Name` varchar(45) DEFAULT NULL,
  `Start_Date` varchar(50) DEFAULT NULL,
  `End_Date` varchar(50) DEFAULT NULL,
  `Absence_Name` varchar(45) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `Comments` varchar(50) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `Inserted_By` varchar(10) DEFAULT NULL,
  `Inserted_Date` varchar(100) DEFAULT NULL,
  `Updated_By` varchar(10) DEFAULT NULL,
  `Updated_Date` varchar(100) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `absence_request`
--

LOCK TABLES `absence_request` WRITE;
/*!40000 ALTER TABLE `absence_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `absence_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `absence_student_request`
--

DROP TABLE IF EXISTS `absence_student_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `absence_student_request` (
  `thedate` varchar(100) DEFAULT NULL,
  `student_id` varchar(45) DEFAULT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `sclass` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  `start_date` varchar(100) DEFAULT NULL,
  `end_date` varchar(100) DEFAULT NULL,
  `absence_name` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `comments` varchar(50) DEFAULT NULL,
  `lapply` int(5) DEFAULT NULL,
  `approved_by` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `absence_student_request`
--

LOCK TABLES `absence_student_request` WRITE;
/*!40000 ALTER TABLE `absence_student_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `absence_student_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assign_approval`
--

DROP TABLE IF EXISTS `assign_approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assign_approval` (
  `assign_date` varchar(40) DEFAULT NULL,
  `teacher_id` varchar(45) DEFAULT NULL,
  `tname` varchar(45) DEFAULT NULL,
  `ttype` varchar(45) DEFAULT NULL,
  `student_id` varchar(45) DEFAULT NULL,
  `student_name` varchar(45) NOT NULL,
  `sclass` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `mark` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assign_approval`
--

LOCK TABLES `assign_approval` WRITE;
/*!40000 ALTER TABLE `assign_approval` DISABLE KEYS */;
/*!40000 ALTER TABLE `assign_approval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignmentadd`
--

DROP TABLE IF EXISTS `assignmentadd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignmentadd` (
  `Sclass` varchar(50) DEFAULT NULL,
  `Subject` varchar(50) DEFAULT NULL,
  `Section` varchar(50) DEFAULT NULL,
  `Title` varchar(200) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Due_Date` varchar(50) DEFAULT NULL,
  `Mark` varchar(100) DEFAULT NULL,
  `HDate` varchar(50) DEFAULT NULL,
  `isActive` varchar(10) DEFAULT NULL,
  `Inserted_By` varchar(50) DEFAULT NULL,
  `Inserted_Date` varchar(50) DEFAULT NULL,
  `Updated_By` varchar(50) DEFAULT NULL,
  `Updated_Date` varchar(50) DEFAULT NULL,
  `Tid` varchar(45) DEFAULT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `ttype` varchar(45) DEFAULT NULL,
  `assignid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignmentadd`
--

LOCK TABLES `assignmentadd` WRITE;
/*!40000 ALTER TABLE `assignmentadd` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignmentadd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance` (
  `adate` date NOT NULL,
  `aclass` varchar(45) DEFAULT NULL,
  `asection` varchar(45) DEFAULT NULL,
  `sid` varchar(1000) DEFAULT NULL,
  `fname` varchar(10000) DEFAULT NULL,
  `lname` varchar(10000) DEFAULT NULL,
  `att_status` varchar(45) DEFAULT NULL,
  `att_time` varchar(45) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `Inserted_By` varchar(45) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance`
--

LOCK TABLES `attendance` WRITE;
/*!40000 ALTER TABLE `attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendance_day`
--

DROP TABLE IF EXISTS `attendance_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_day` (
  `aclass` varchar(45) DEFAULT NULL,
  `asection` varchar(45) DEFAULT NULL,
  `sid` varchar(45) DEFAULT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `day1` varchar(45) DEFAULT NULL,
  `day2` varchar(45) DEFAULT NULL,
  `day3` varchar(45) DEFAULT NULL,
  `day4` varchar(45) DEFAULT NULL,
  `day5` varchar(45) DEFAULT NULL,
  `day6` varchar(45) DEFAULT NULL,
  `day7` varchar(45) DEFAULT NULL,
  `day8` varchar(45) DEFAULT NULL,
  `day9` varchar(45) DEFAULT NULL,
  `day10` varchar(45) DEFAULT NULL,
  `day11` varchar(45) DEFAULT NULL,
  `day12` varchar(45) DEFAULT NULL,
  `day13` varchar(45) DEFAULT NULL,
  `day14` varchar(45) DEFAULT NULL,
  `day15` varchar(45) DEFAULT NULL,
  `day16` varchar(45) DEFAULT NULL,
  `day17` varchar(45) DEFAULT NULL,
  `day18` varchar(45) DEFAULT NULL,
  `day19` varchar(45) DEFAULT NULL,
  `day20` varchar(45) DEFAULT NULL,
  `day21` varchar(45) DEFAULT NULL,
  `day22` varchar(45) DEFAULT NULL,
  `day23` varchar(45) DEFAULT NULL,
  `day24` varchar(45) DEFAULT NULL,
  `day25` varchar(45) DEFAULT NULL,
  `day26` varchar(45) DEFAULT NULL,
  `day27` varchar(45) DEFAULT NULL,
  `day28` varchar(45) DEFAULT NULL,
  `day29` varchar(45) DEFAULT NULL,
  `day30` varchar(45) DEFAULT NULL,
  `day31` varchar(45) DEFAULT NULL,
  `totalP` bigint(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance_day`
--

LOCK TABLES `attendance_day` WRITE;
/*!40000 ALTER TABLE `attendance_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendance_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `attendance_day1`
--

DROP TABLE IF EXISTS `attendance_day1`;
/*!50001 DROP VIEW IF EXISTS `attendance_day1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attendance_day1` AS SELECT 
 1 AS `month`,
 1 AS `aclass`,
 1 AS `asection`,
 1 AS `sid`,
 1 AS `fname`,
 1 AS `day1`,
 1 AS `day2`,
 1 AS `day3`,
 1 AS `day4`,
 1 AS `day5`,
 1 AS `day6`,
 1 AS `day7`,
 1 AS `day8`,
 1 AS `day9`,
 1 AS `day10`,
 1 AS `day11`,
 1 AS `day12`,
 1 AS `day13`,
 1 AS `day14`,
 1 AS `day15`,
 1 AS `day16`,
 1 AS `day17`,
 1 AS `day18`,
 1 AS `day19`,
 1 AS `day20`,
 1 AS `day21`,
 1 AS `day22`,
 1 AS `day23`,
 1 AS `day24`,
 1 AS `day25`,
 1 AS `day26`,
 1 AS `day27`,
 1 AS `day28`,
 1 AS `day29`,
 1 AS `day30`,
 1 AS `day31`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `attendance_day2`
--

DROP TABLE IF EXISTS `attendance_day2`;
/*!50001 DROP VIEW IF EXISTS `attendance_day2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attendance_day2` AS SELECT 
 1 AS `month`,
 1 AS `tid`,
 1 AS `tname`,
 1 AS `day1`,
 1 AS `day2`,
 1 AS `day3`,
 1 AS `day4`,
 1 AS `day5`,
 1 AS `day6`,
 1 AS `day7`,
 1 AS `day8`,
 1 AS `day9`,
 1 AS `day10`,
 1 AS `day11`,
 1 AS `day12`,
 1 AS `day13`,
 1 AS `day14`,
 1 AS `day15`,
 1 AS `day16`,
 1 AS `day17`,
 1 AS `day18`,
 1 AS `day19`,
 1 AS `day20`,
 1 AS `day21`,
 1 AS `day22`,
 1 AS `day23`,
 1 AS `day24`,
 1 AS `day25`,
 1 AS `day26`,
 1 AS `day27`,
 1 AS `day28`,
 1 AS `day29`,
 1 AS `day30`,
 1 AS `day31`,
 1 AS `totalP`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `attendance_dayStudentsTest`
--

DROP TABLE IF EXISTS `attendance_dayStudentsTest`;
/*!50001 DROP VIEW IF EXISTS `attendance_dayStudentsTest`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attendance_dayStudentsTest` AS SELECT 
 1 AS `month`,
 1 AS `aclass`,
 1 AS `asection`,
 1 AS `sid`,
 1 AS `fname`,
 1 AS `mondate`,
 1 AS `monyear`,
 1 AS `day1`,
 1 AS `day2`,
 1 AS `day3`,
 1 AS `day4`,
 1 AS `day5`,
 1 AS `day6`,
 1 AS `day7`,
 1 AS `day8`,
 1 AS `day9`,
 1 AS `day10`,
 1 AS `day11`,
 1 AS `day12`,
 1 AS `day13`,
 1 AS `day14`,
 1 AS `day15`,
 1 AS `day16`,
 1 AS `day17`,
 1 AS `day18`,
 1 AS `day19`,
 1 AS `day20`,
 1 AS `day21`,
 1 AS `day22`,
 1 AS `day23`,
 1 AS `day24`,
 1 AS `day25`,
 1 AS `day26`,
 1 AS `day27`,
 1 AS `day28`,
 1 AS `day29`,
 1 AS `day30`,
 1 AS `day31`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `attendance_dayTeacher`
--

DROP TABLE IF EXISTS `attendance_dayTeacher`;
/*!50001 DROP VIEW IF EXISTS `attendance_dayTeacher`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attendance_dayTeacher` AS SELECT 
 1 AS `month`,
 1 AS `tid`,
 1 AS `tname`,
 1 AS `day1`,
 1 AS `day2`,
 1 AS `day3`,
 1 AS `day4`,
 1 AS `day5`,
 1 AS `day6`,
 1 AS `day7`,
 1 AS `day8`,
 1 AS `day9`,
 1 AS `day10`,
 1 AS `day11`,
 1 AS `day12`,
 1 AS `day13`,
 1 AS `day14`,
 1 AS `day15`,
 1 AS `day16`,
 1 AS `day17`,
 1 AS `day18`,
 1 AS `day19`,
 1 AS `day20`,
 1 AS `day21`,
 1 AS `day22`,
 1 AS `day23`,
 1 AS `day24`,
 1 AS `day25`,
 1 AS `day26`,
 1 AS `day27`,
 1 AS `day28`,
 1 AS `day29`,
 1 AS `day30`,
 1 AS `day31`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `attendance_dayTeacher_Test`
--

DROP TABLE IF EXISTS `attendance_dayTeacher_Test`;
/*!50001 DROP VIEW IF EXISTS `attendance_dayTeacher_Test`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attendance_dayTeacher_Test` AS SELECT 
 1 AS `month`,
 1 AS `tid`,
 1 AS `tname`,
 1 AS `mondate`,
 1 AS `monyear`,
 1 AS `day1`,
 1 AS `day2`,
 1 AS `day3`,
 1 AS `day4`,
 1 AS `day5`,
 1 AS `day6`,
 1 AS `day7`,
 1 AS `day8`,
 1 AS `day9`,
 1 AS `day10`,
 1 AS `day11`,
 1 AS `day12`,
 1 AS `day13`,
 1 AS `day14`,
 1 AS `day15`,
 1 AS `day16`,
 1 AS `day17`,
 1 AS `day18`,
 1 AS `day19`,
 1 AS `day20`,
 1 AS `day21`,
 1 AS `day22`,
 1 AS `day23`,
 1 AS `day24`,
 1 AS `day25`,
 1 AS `day26`,
 1 AS `day27`,
 1 AS `day28`,
 1 AS `day29`,
 1 AS `day30`,
 1 AS `day31`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `attendance_dayTeacher_test`
--

DROP TABLE IF EXISTS `attendance_dayTeacher_test`;
/*!50001 DROP VIEW IF EXISTS `attendance_dayTeacher_test`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attendance_dayTeacher_test` AS SELECT 
 1 AS `month`,
 1 AS `month(adate)`,
 1 AS `year(adate)`,
 1 AS `tid`,
 1 AS `tname`,
 1 AS `day1`,
 1 AS `day2`,
 1 AS `day3`,
 1 AS `day4`,
 1 AS `day5`,
 1 AS `day6`,
 1 AS `day7`,
 1 AS `day8`,
 1 AS `day9`,
 1 AS `day10`,
 1 AS `day11`,
 1 AS `day12`,
 1 AS `day13`,
 1 AS `day14`,
 1 AS `day15`,
 1 AS `day16`,
 1 AS `day17`,
 1 AS `day18`,
 1 AS `day19`,
 1 AS `day20`,
 1 AS `day21`,
 1 AS `day22`,
 1 AS `day23`,
 1 AS `day24`,
 1 AS `day25`,
 1 AS `day26`,
 1 AS `day27`,
 1 AS `day28`,
 1 AS `day29`,
 1 AS `day30`,
 1 AS `day31`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `attendance_dayTeacher_tt`
--

DROP TABLE IF EXISTS `attendance_dayTeacher_tt`;
/*!50001 DROP VIEW IF EXISTS `attendance_dayTeacher_tt`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attendance_dayTeacher_tt` AS SELECT 
 1 AS `month`,
 1 AS `tid`,
 1 AS `tname`,
 1 AS `month(adate)`,
 1 AS `year(adate)`,
 1 AS `day1`,
 1 AS `day2`,
 1 AS `day3`,
 1 AS `day4`,
 1 AS `day5`,
 1 AS `day6`,
 1 AS `day7`,
 1 AS `day8`,
 1 AS `day9`,
 1 AS `day10`,
 1 AS `day11`,
 1 AS `day12`,
 1 AS `day13`,
 1 AS `day14`,
 1 AS `day15`,
 1 AS `day16`,
 1 AS `day17`,
 1 AS `day18`,
 1 AS `day19`,
 1 AS `day20`,
 1 AS `day21`,
 1 AS `day22`,
 1 AS `day23`,
 1 AS `day24`,
 1 AS `day25`,
 1 AS `day26`,
 1 AS `day27`,
 1 AS `day28`,
 1 AS `day29`,
 1 AS `day30`,
 1 AS `day31`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `attendance_dayteacher`
--

DROP TABLE IF EXISTS `attendance_dayteacher`;
/*!50001 DROP VIEW IF EXISTS `attendance_dayteacher`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attendance_dayteacher` AS SELECT 
 1 AS `month`,
 1 AS `tid`,
 1 AS `tname`,
 1 AS `day1`,
 1 AS `day2`,
 1 AS `day3`,
 1 AS `day4`,
 1 AS `day5`,
 1 AS `day6`,
 1 AS `day7`,
 1 AS `day8`,
 1 AS `day9`,
 1 AS `day10`,
 1 AS `day11`,
 1 AS `day12`,
 1 AS `day13`,
 1 AS `day14`,
 1 AS `day15`,
 1 AS `day16`,
 1 AS `day17`,
 1 AS `day18`,
 1 AS `day19`,
 1 AS `day20`,
 1 AS `day21`,
 1 AS `day22`,
 1 AS `day23`,
 1 AS `day24`,
 1 AS `day25`,
 1 AS `day26`,
 1 AS `day27`,
 1 AS `day28`,
 1 AS `day29`,
 1 AS `day30`,
 1 AS `day31`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `attendance_staff`
--

DROP TABLE IF EXISTS `attendance_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_staff` (
  `adate` varchar(200) DEFAULT NULL,
  `checkin` varchar(200) DEFAULT NULL,
  `checkout` varchar(200) DEFAULT NULL,
  `tid` varchar(45) DEFAULT NULL,
  `tname` varchar(45) DEFAULT NULL,
  `att_status` varchar(45) DEFAULT NULL,
  `ttype` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance_staff`
--

LOCK TABLES `attendance_staff` WRITE;
/*!40000 ALTER TABLE `attendance_staff` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendance_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendance_teacher`
--

DROP TABLE IF EXISTS `attendance_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_teacher` (
  `adate` varchar(200) DEFAULT NULL,
  `checkin` varchar(200) DEFAULT NULL,
  `checkout` varchar(200) DEFAULT NULL,
  `tid` varchar(45) DEFAULT NULL,
  `tname` varchar(45) DEFAULT NULL,
  `att_status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance_teacher`
--

LOCK TABLES `attendance_teacher` WRITE;
/*!40000 ALTER TABLE `attendance_teacher` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendance_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendence_master`
--

DROP TABLE IF EXISTS `attendence_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendence_master` (
  `student_id` int(11) NOT NULL,
  `attendance_code` int(11) NOT NULL,
  `for_the_day` date DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `seqno` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`seqno`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `attendence_master_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students_master` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendence_master`
--

LOCK TABLES `attendence_master` WRITE;
/*!40000 ALTER TABLE `attendence_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendence_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendence_meta`
--

DROP TABLE IF EXISTS `attendence_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendence_meta` (
  `attendance_code` int(11) NOT NULL,
  `description` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`attendance_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendence_meta`
--

LOCK TABLES `attendence_meta` WRITE;
/*!40000 ALTER TABLE `attendence_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendence_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48315 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_master`
--

DROP TABLE IF EXISTS `class_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_master` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_standard` int(11) DEFAULT NULL,
  `class_section` varchar(2) DEFAULT NULL,
  `academic_year` varchar(12) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_master`
--

LOCK TABLES `class_master` WRITE;
/*!40000 ALTER TABLE `class_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `class_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classmaster`
--

DROP TABLE IF EXISTS `classmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classmaster` (
  `sclass` varchar(50) NOT NULL,
  PRIMARY KEY (`sclass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classmaster`
--

LOCK TABLES `classmaster` WRITE;
/*!40000 ALTER TABLE `classmaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `classmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cocurricular_actvities`
--

DROP TABLE IF EXISTS `cocurricular_actvities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cocurricular_actvities` (
  `activity_code` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`activity_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cocurricular_actvities`
--

LOCK TABLES `cocurricular_actvities` WRITE;
/*!40000 ALTER TABLE `cocurricular_actvities` DISABLE KEYS */;
/*!40000 ALTER TABLE `cocurricular_actvities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colegio_devp.home_work_status`
--

DROP TABLE IF EXISTS `colegio_devp.home_work_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colegio_devp.home_work_status` (
  `thedate` date DEFAULT NULL,
  `Hdate` date DEFAULT NULL,
  `sclass` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  `teacher_name` varchar(45) DEFAULT NULL,
  `teacher_id` varchar(45) DEFAULT NULL,
  `student_id` varchar(45) DEFAULT NULL,
  `student_name` varchar(45) DEFAULT NULL,
  `subject1` varchar(45) DEFAULT NULL,
  `status1` varchar(45) DEFAULT NULL,
  `remark1` varchar(45) DEFAULT NULL,
  `subject2` varchar(45) DEFAULT NULL,
  `status2` varchar(45) DEFAULT NULL,
  `remark2` varchar(45) DEFAULT NULL,
  `subject3` varchar(45) DEFAULT NULL,
  `status3` varchar(45) DEFAULT NULL,
  `remark3` varchar(45) DEFAULT NULL,
  `subject4` varchar(45) DEFAULT NULL,
  `status4` varchar(45) DEFAULT NULL,
  `remark4` varchar(45) DEFAULT NULL,
  `subject5` varchar(45) DEFAULT NULL,
  `status5` varchar(45) DEFAULT NULL,
  `remark5` varchar(45) DEFAULT NULL,
  `subject6` varchar(45) DEFAULT NULL,
  `status6` varchar(45) DEFAULT NULL,
  `remark6` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colegio_devp.home_work_status`
--

LOCK TABLES `colegio_devp.home_work_status` WRITE;
/*!40000 ALTER TABLE `colegio_devp.home_work_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `colegio_devp.home_work_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `colu`
--

DROP TABLE IF EXISTS `colu`;
/*!50001 DROP VIEW IF EXISTS `colu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `colu` AS SELECT 
 1 AS `cname`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `counter_table`
--

DROP TABLE IF EXISTS `counter_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counter_table` (
  `Cnt_Mod_Id` int(11) NOT NULL,
  `Cnt_Prifix` varchar(45) DEFAULT NULL,
  `Cnt_Count` int(11) DEFAULT NULL,
  `Cnt_Num` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Cnt_Mod_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `counter_table`
--

LOCK TABLES `counter_table` WRITE;
/*!40000 ALTER TABLE `counter_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `counter_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sclass` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dormitory`
--

DROP TABLE IF EXISTS `dormitory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dormitory` (
  `Dormitory_name` varchar(45) NOT NULL,
  `Location` varchar(45) DEFAULT NULL,
  `fee` varchar(45) DEFAULT NULL,
  `cgender` varchar(10) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `No_of_beds` int(10) DEFAULT NULL,
  `Warden_Photo` varchar(45) DEFAULT NULL,
  `name` varchar(25) DEFAULT NULL,
  `age` int(100) DEFAULT NULL,
  `Contact_Number` varchar(20) DEFAULT NULL,
  `Alt_Contact_Number` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `DeputyWarden_Photo` varchar(45) DEFAULT NULL,
  `name1` varchar(25) DEFAULT NULL,
  `age1` int(100) DEFAULT NULL,
  `Contact_Number1` varchar(20) DEFAULT NULL,
  `Alt_Contact_Number1` varchar(20) DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `Inserted_By` varchar(10) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(10) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL,
  `allot_beds` int(100) DEFAULT NULL,
  `remain_beds` int(100) DEFAULT NULL,
  PRIMARY KEY (`Dormitory_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dormitory`
--

LOCK TABLES `dormitory` WRITE;
/*!40000 ALTER TABLE `dormitory` DISABLE KEYS */;
/*!40000 ALTER TABLE `dormitory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dummy`
--

DROP TABLE IF EXISTS `dummy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dummy` (
  `adate` date DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dummy`
--

LOCK TABLES `dummy` WRITE;
/*!40000 ALTER TABLE `dummy` DISABLE KEYS */;
/*!40000 ALTER TABLE `dummy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dummy2`
--

DROP TABLE IF EXISTS `dummy2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dummy2` (
  `subject` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dummy2`
--

LOCK TABLES `dummy2` WRITE;
/*!40000 ALTER TABLE `dummy2` DISABLE KEYS */;
/*!40000 ALTER TABLE `dummy2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `echallan`
--

DROP TABLE IF EXISTS `echallan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `echallan` (
  `challan_id` varchar(20) DEFAULT NULL,
  `theDate` varchar(45) DEFAULT NULL,
  `fee_name` varchar(45) DEFAULT NULL,
  `acc_year` varchar(1000) DEFAULT NULL,
  `Fee_Break1` varchar(45) DEFAULT NULL,
  `amount1` varchar(45) DEFAULT NULL,
  `Fee_Break2` varchar(45) DEFAULT NULL,
  `amount2` varchar(45) DEFAULT NULL,
  `Fee_Break3` varchar(45) DEFAULT NULL,
  `amount3` varchar(45) NOT NULL,
  `total` varchar(45) DEFAULT NULL,
  `SClass` varchar(45) DEFAULT NULL,
  `Section` varchar(45) DEFAULT NULL,
  `datepicker` varchar(45) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `Inserted_By` varchar(10) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(10) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL,
  `sendMessage` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `echallan`
--

LOCK TABLES `echallan` WRITE;
/*!40000 ALTER TABLE `echallan` DISABLE KEYS */;
/*!40000 ALTER TABLE `echallan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_schedule`
--

DROP TABLE IF EXISTS `exam_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_schedule` (
  `Exam_Id` varchar(40) NOT NULL,
  `Exam_Name` varchar(40) DEFAULT NULL,
  `Exam_Type` varchar(40) DEFAULT NULL,
  `E_Class` varchar(40) DEFAULT NULL,
  `Section` varchar(40) DEFAULT NULL,
  `Subject1` varchar(40) DEFAULT NULL,
  `E_Date1` date DEFAULT NULL,
  `E_Time1` varchar(40) DEFAULT NULL,
  `Subject2` varchar(40) DEFAULT NULL,
  `E_Date2` date DEFAULT NULL,
  `E_Time2` varchar(40) DEFAULT NULL,
  `Subject3` varchar(40) DEFAULT NULL,
  `E_Date3` date DEFAULT NULL,
  `E_Time3` varchar(40) DEFAULT NULL,
  `Subject4` varchar(40) DEFAULT NULL,
  `E_Date4` date DEFAULT NULL,
  `E_Time4` varchar(40) DEFAULT NULL,
  `Subject5` varchar(40) DEFAULT NULL,
  `E_Date5` date DEFAULT NULL,
  `E_Time5` varchar(40) DEFAULT NULL,
  `is_Active` varchar(1) DEFAULT NULL,
  `Inserted_By` varchar(40) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(40) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL,
  PRIMARY KEY (`Exam_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_schedule`
--

LOCK TABLES `exam_schedule` WRITE;
/*!40000 ALTER TABLE `exam_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_schedule_new`
--

DROP TABLE IF EXISTS `exam_schedule_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_schedule_new` (
  `exam_id` varchar(40) DEFAULT NULL,
  `exam_name` varchar(40) DEFAULT NULL,
  `exam_type` varchar(40) DEFAULT NULL,
  `e_class` varchar(40) DEFAULT NULL,
  `section` varchar(40) DEFAULT NULL,
  `subject` varchar(40) DEFAULT NULL,
  `e_date` varchar(40) DEFAULT NULL,
  `e_time` varchar(40) DEFAULT NULL,
  `e_mark` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_schedule_new`
--

LOCK TABLES `exam_schedule_new` WRITE;
/*!40000 ALTER TABLE `exam_schedule_new` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_schedule_new` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_type_master`
--

DROP TABLE IF EXISTS `exam_type_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_type_master` (
  `exam_type` varchar(100) NOT NULL,
  PRIMARY KEY (`exam_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_type_master`
--

LOCK TABLES `exam_type_master` WRITE;
/*!40000 ALTER TABLE `exam_type_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_type_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_list`
--

DROP TABLE IF EXISTS `fee_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fee_list` (
  `fees_code` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(25) DEFAULT NULL,
  `student_id` int(11) NOT NULL,
  `academic_year` varchar(12) NOT NULL,
  PRIMARY KEY (`fees_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_list`
--

LOCK TABLES `fee_list` WRITE;
/*!40000 ALTER TABLE `fee_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_master`
--

DROP TABLE IF EXISTS `fee_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fee_master` (
  `student_id` int(11) NOT NULL,
  `academic_year` varchar(12) NOT NULL,
  `class_id` int(11) NOT NULL,
  `fees_code` int(11) NOT NULL,
  `fees_amount` int(11) DEFAULT NULL,
  `is_Paid` varchar(1) NOT NULL,
  `seqno` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`seqno`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `fee_master_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students_master` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_master`
--

LOCK TABLES `fee_master` WRITE;
/*!40000 ALTER TABLE `fee_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_transaction`
--

DROP TABLE IF EXISTS `fee_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fee_transaction` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `challan_no` int(11) DEFAULT NULL,
  `fees_code` int(11) NOT NULL,
  `paid_amount` int(11) NOT NULL,
  `payment_method` varchar(15) NOT NULL,
  `payment_date` date NOT NULL,
  `is_Due` varchar(1) NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `student_id` (`student_id`),
  KEY `fees_code` (`fees_code`),
  CONSTRAINT `fee_transaction_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students_master` (`student_id`),
  CONSTRAINT `fee_transaction_ibfk_2` FOREIGN KEY (`fees_code`) REFERENCES `fee_list` (`fees_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_transaction`
--

LOCK TABLES `fee_transaction` WRITE;
/*!40000 ALTER TABLE `fee_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_create`
--

DROP TABLE IF EXISTS `group_create`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_create` (
  `group_name` varchar(100) DEFAULT NULL,
  `group_mem` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_create`
--

LOCK TABLES `group_create` WRITE;
/*!40000 ALTER TABLE `group_create` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_create` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_work`
--

DROP TABLE IF EXISTS `home_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home_work` (
  `theDate` date DEFAULT NULL,
  `sClass1` varchar(45) DEFAULT NULL,
  `Section1` varchar(45) DEFAULT NULL,
  `Teacher_Name` varchar(45) DEFAULT NULL,
  `Teacher_Id` varchar(45) DEFAULT NULL,
  `ttype` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `Subject` varchar(45) DEFAULT NULL,
  `HomeWork` varchar(300) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `Inserted_By` varchar(10) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(10) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_work`
--

LOCK TABLES `home_work` WRITE;
/*!40000 ALTER TABLE `home_work` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_work` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_work_status`
--

DROP TABLE IF EXISTS `home_work_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home_work_status` (
  `theDate` date DEFAULT NULL,
  `student_id` varchar(45) DEFAULT NULL,
  `sClass1` varchar(45) DEFAULT NULL,
  `Section1` varchar(45) DEFAULT NULL,
  `Teacher_Name` varchar(45) DEFAULT NULL,
  `Teacher_Id` varchar(45) DEFAULT NULL,
  `ttype` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `Subject` varchar(45) DEFAULT NULL,
  `HomeWork` varchar(300) DEFAULT NULL,
  `status` varchar(300) DEFAULT NULL,
  `remark` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_work_status`
--

LOCK TABLES `home_work_status` WRITE;
/*!40000 ALTER TABLE `home_work_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_work_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hostel_allot`
--

DROP TABLE IF EXISTS `hostel_allot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hostel_allot` (
  `dormitory_name` varchar(40) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `student_id` varchar(45) DEFAULT NULL,
  `student_name` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `room_number` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hostel_allot`
--

LOCK TABLES `hostel_allot` WRITE;
/*!40000 ALTER TABLE `hostel_allot` DISABLE KEYS */;
/*!40000 ALTER TABLE `hostel_allot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `house_system`
--

DROP TABLE IF EXISTS `house_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `house_system` (
  `house_id` int(11) NOT NULL AUTO_INCREMENT,
  `house_name` varchar(15) NOT NULL,
  `is_Active` varchar(1) NOT NULL,
  `color_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`house_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `house_system`
--

LOCK TABLES `house_system` WRITE;
/*!40000 ALTER TABLE `house_system` DISABLE KEYS */;
/*!40000 ALTER TABLE `house_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jpatable`
--

DROP TABLE IF EXISTS `jpatable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jpatable` (
  `id` int(11) NOT NULL,
  `sclass` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jpatable`
--

LOCK TABLES `jpatable` WRITE;
/*!40000 ALTER TABLE `jpatable` DISABLE KEYS */;
/*!40000 ALTER TABLE `jpatable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `librarian_master`
--

DROP TABLE IF EXISTS `librarian_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `librarian_master` (
  `lib_id` varchar(40) NOT NULL,
  `lib_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`lib_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `librarian_master`
--

LOCK TABLES `librarian_master` WRITE;
/*!40000 ALTER TABLE `librarian_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `librarian_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_addbook`
--

DROP TABLE IF EXISTS `library_addbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_addbook` (
  `Book_ID` varchar(15) NOT NULL,
  `Book_Title` varchar(45) DEFAULT NULL,
  `Author` varchar(40) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `SClass` varchar(40) DEFAULT NULL,
  `Subject` varchar(45) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  `total_books` int(10) DEFAULT NULL,
  `req_books` int(10) DEFAULT NULL,
  `avail_books` int(10) DEFAULT NULL,
  `request` int(10) DEFAULT NULL,
  `isActive` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`Book_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_addbook`
--

LOCK TABLES `library_addbook` WRITE;
/*!40000 ALTER TABLE `library_addbook` DISABLE KEYS */;
/*!40000 ALTER TABLE `library_addbook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_books`
--

DROP TABLE IF EXISTS `library_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_books` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `author` varchar(30) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `purchased_date` date DEFAULT NULL,
  `is_Available` varchar(1) NOT NULL,
  `is_Lent` varchar(1) NOT NULL,
  `isDamaged` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_books`
--

LOCK TABLES `library_books` WRITE;
/*!40000 ALTER TABLE `library_books` DISABLE KEYS */;
/*!40000 ALTER TABLE `library_books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_master`
--

DROP TABLE IF EXISTS `library_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_master` (
  `lending_id` int(11) NOT NULL AUTO_INCREMENT,
  `lender_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `lending_date` date DEFAULT NULL,
  `returned_date` date DEFAULT NULL,
  `is_Retured` varchar(1) NOT NULL,
  `is_Damaged` varchar(1) NOT NULL,
  PRIMARY KEY (`lending_id`),
  KEY `lender_id` (`lender_id`),
  KEY `book_id` (`book_id`),
  CONSTRAINT `library_master_ibfk_1` FOREIGN KEY (`lender_id`) REFERENCES `library_member` (`lender_id`),
  CONSTRAINT `library_master_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `library_books` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_master`
--

LOCK TABLES `library_master` WRITE;
/*!40000 ALTER TABLE `library_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `library_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_member`
--

DROP TABLE IF EXISTS `library_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_member` (
  `lender_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `active_date` date DEFAULT NULL,
  `active_lends` int(11) DEFAULT NULL,
  `is_Due` varchar(1) NOT NULL,
  `inactive_date` date DEFAULT NULL,
  `is_Active` varchar(1) NOT NULL,
  PRIMARY KEY (`lender_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `library_member_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students_master` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_member`
--

LOCK TABLES `library_member` WRITE;
/*!40000 ALTER TABLE `library_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `library_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_requestbook`
--

DROP TABLE IF EXISTS `library_requestbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_requestbook` (
  `breq_id` varchar(40) NOT NULL,
  `book_id` varchar(45) DEFAULT NULL,
  `book_title` varchar(45) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `student_id` varchar(40) DEFAULT NULL,
  `student_name` varchar(40) DEFAULT NULL,
  `sclass` varchar(40) DEFAULT NULL,
  `sec` varchar(40) DEFAULT NULL,
  `req_date` date DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `fineamt` int(10) DEFAULT NULL,
  `approved_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`breq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_requestbook`
--

LOCK TABLES `library_requestbook` WRITE;
/*!40000 ALTER TABLE `library_requestbook` DISABLE KEYS */;
/*!40000 ALTER TABLE `library_requestbook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_master`
--

DROP TABLE IF EXISTS `login_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_master` (
  `uname` varchar(45) NOT NULL,
  `lpass` varchar(45) DEFAULT NULL,
  `ltype` varchar(45) DEFAULT NULL,
  `sid` varchar(45) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `sname` varchar(200) DEFAULT NULL,
  `ayear` varchar(45) DEFAULT NULL,
  `slogo` varchar(200) DEFAULT NULL,
  `sheader` varchar(45) DEFAULT NULL,
  `sfooter` varchar(45) DEFAULT NULL,
  `pass_change` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`uname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_master`
--

LOCK TABLES `login_master` WRITE;
/*!40000 ALTER TABLE `login_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_master`
--

DROP TABLE IF EXISTS `mail_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_master` (
  `mail_from` varchar(40) NOT NULL,
  `mail_to` varchar(45) DEFAULT NULL,
  `subj` varchar(45) DEFAULT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `message` mediumtext,
  `attach` varchar(45) DEFAULT NULL,
  `mdate` varchar(50) DEFAULT NULL,
  `mtime` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_master`
--

LOCK TABLES `mail_master` WRITE;
/*!40000 ALTER TABLE `mail_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_master_backup`
--

DROP TABLE IF EXISTS `mail_master_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_master_backup` (
  `mail_from` varchar(40) NOT NULL,
  `mail_to` varchar(45) DEFAULT NULL,
  `subj` varchar(45) DEFAULT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `message` varchar(100) DEFAULT NULL,
  `attach` varchar(45) DEFAULT NULL,
  `mdate` varchar(50) DEFAULT NULL,
  `mtime` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_master_backup`
--

LOCK TABLES `mail_master_backup` WRITE;
/*!40000 ALTER TABLE `mail_master_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_master_backup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailsetting`
--

DROP TABLE IF EXISTS `mailsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailsetting` (
  `tem_var` varchar(100) NOT NULL,
  `tem_message` varchar(500) DEFAULT NULL,
  `temp_id` varchar(45) DEFAULT NULL,
  `temp_head` varchar(45) DEFAULT NULL,
  `temp_footer` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`tem_var`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailsetting`
--

LOCK TABLES `mailsetting` WRITE;
/*!40000 ALTER TABLE `mailsetting` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailsetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `markentryalldump`
--

DROP TABLE IF EXISTS `markentryalldump`;
/*!50001 DROP VIEW IF EXISTS `markentryalldump`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `markentryalldump` AS SELECT 
 1 AS `exam_id`,
 1 AS `sclass`,
 1 AS `ssec`,
 1 AS `student_id`,
 1 AS `student_name`,
 1 AS `Tamil`,
 1 AS `English`,
 1 AS `Maths`,
 1 AS `Science`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `markentryalldumphead`
--

DROP TABLE IF EXISTS `markentryalldumphead`;
/*!50001 DROP VIEW IF EXISTS `markentryalldumphead`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `markentryalldumphead` AS SELECT 
 1 AS `student_id`,
 1 AS `student_name`,
 1 AS `Tamil`,
 1 AS `English`,
 1 AS `Maths`,
 1 AS `Science`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `markentrydump`
--

DROP TABLE IF EXISTS `markentrydump`;
/*!50001 DROP VIEW IF EXISTS `markentrydump`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `markentrydump` AS SELECT 
 1 AS `student_id`,
 1 AS `tamil`,
 1 AS `english`,
 1 AS `maths`,
 1 AS `total`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `markentrydump11`
--

DROP TABLE IF EXISTS `markentrydump11`;
/*!50001 DROP VIEW IF EXISTS `markentrydump11`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `markentrydump11` AS SELECT 
 1 AS `exam_id`,
 1 AS `student_id`,
 1 AS `student_name`,
 1 AS `sclass`,
 1 AS `ssec`,
 1 AS `Social`,
 1 AS `Tamil`,
 1 AS `Maths`,
 1 AS `Science`,
 1 AS `total`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `marks_entry`
--

DROP TABLE IF EXISTS `marks_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marks_entry` (
  `exam_id` varchar(45) DEFAULT NULL,
  `exam_name` varchar(45) DEFAULT NULL,
  `exam_type` varchar(45) DEFAULT NULL,
  `sclass` varchar(45) DEFAULT NULL,
  `ssec` varchar(45) DEFAULT NULL,
  `student_id` varchar(45) DEFAULT NULL,
  `student_name` varchar(45) DEFAULT NULL,
  `tamil` int(5) DEFAULT NULL,
  `english` int(5) DEFAULT NULL,
  `maths` int(5) DEFAULT NULL,
  `science` int(5) DEFAULT NULL,
  `sscience` int(5) DEFAULT NULL,
  `total` int(5) DEFAULT NULL,
  `average` int(5) DEFAULT NULL,
  `result` varchar(45) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `Inserted_By` varchar(45) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marks_entry`
--

LOCK TABLES `marks_entry` WRITE;
/*!40000 ALTER TABLE `marks_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `marks_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marks_entry_all`
--

DROP TABLE IF EXISTS `marks_entry_all`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marks_entry_all` (
  `exam_id` varchar(45) DEFAULT NULL,
  `exam_name` varchar(45) DEFAULT NULL,
  `exam_type` varchar(45) DEFAULT NULL,
  `sclass` varchar(45) DEFAULT NULL,
  `ssec` varchar(45) DEFAULT NULL,
  `student_id` varchar(45) DEFAULT NULL,
  `student_name` varchar(45) DEFAULT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `mark` int(5) DEFAULT NULL,
  `IsActive` varchar(5) DEFAULT NULL,
  `Inserted_By` varchar(45) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marks_entry_all`
--

LOCK TABLES `marks_entry_all` WRITE;
/*!40000 ALTER TABLE `marks_entry_all` DISABLE KEYS */;
/*!40000 ALTER TABLE `marks_entry_all` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marks_total`
--

DROP TABLE IF EXISTS `marks_total`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marks_total` (
  `exam_id` varchar(45) DEFAULT NULL,
  `student_id` varchar(45) DEFAULT NULL,
  `student_name` varchar(45) DEFAULT NULL,
  `sclass` varchar(45) DEFAULT NULL,
  `ssec` varchar(45) DEFAULT NULL,
  `total` int(5) DEFAULT NULL,
  `Average` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marks_total`
--

LOCK TABLES `marks_total` WRITE;
/*!40000 ALTER TABLE `marks_total` DISABLE KEYS */;
/*!40000 ALTER TABLE `marks_total` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mastertable`
--

DROP TABLE IF EXISTS `mastertable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mastertable` (
  `role` varchar(50) DEFAULT NULL,
  `desg` varchar(50) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `uname` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `schoolname` varchar(45) DEFAULT NULL,
  `branch` varchar(45) DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `ayear` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `validdate` date DEFAULT NULL,
  `key` varchar(200) DEFAULT NULL,
  `pack` varchar(100) DEFAULT NULL,
  `admid` varchar(45) DEFAULT NULL,
  `sheader` varchar(200) DEFAULT NULL,
  `sfooter` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mastertable`
--

LOCK TABLES `mastertable` WRITE;
/*!40000 ALTER TABLE `mastertable` DISABLE KEYS */;
/*!40000 ALTER TABLE `mastertable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_addalbum`
--

DROP TABLE IF EXISTS `media_addalbum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_addalbum` (
  `Album_Title` varchar(15) NOT NULL,
  `Album_Description` varchar(50) DEFAULT NULL,
  `Album_Image` varchar(20) DEFAULT NULL,
  `Share_Student` varchar(40) NOT NULL,
  `Share_Teacher` varchar(40) NOT NULL,
  `Share_NonStaff` varchar(40) NOT NULL,
  `isActive` varchar(5) DEFAULT NULL,
  `Inserted_By` varchar(45) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL,
  PRIMARY KEY (`Album_Title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_addalbum`
--

LOCK TABLES `media_addalbum` WRITE;
/*!40000 ALTER TABLE `media_addalbum` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_addalbum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_addmedia`
--

DROP TABLE IF EXISTS `media_addmedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_addmedia` (
  `Media_Title` varchar(15) NOT NULL,
  `Media_Description` varchar(50) DEFAULT NULL,
  `urlType` varchar(50) DEFAULT NULL,
  `urlPath` varchar(200) DEFAULT NULL,
  `isActive` varchar(5) DEFAULT NULL,
  `Inserted_By` varchar(45) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL,
  PRIMARY KEY (`Media_Title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_addmedia`
--

LOCK TABLES `media_addmedia` WRITE;
/*!40000 ALTER TABLE `media_addmedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_addmedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_setting`
--

DROP TABLE IF EXISTS `menu_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_setting` (
  `sno` varchar(20) NOT NULL,
  `main_menu` varchar(200) DEFAULT NULL,
  `sub_menu` varchar(200) DEFAULT NULL,
  `menu_item1` varchar(200) DEFAULT NULL,
  `menu_item2` varchar(200) DEFAULT NULL,
  `menu_item3` varchar(200) DEFAULT NULL,
  `mtype` varchar(45) DEFAULT NULL,
  `pack` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_setting`
--

LOCK TABLES `menu_setting` WRITE;
/*!40000 ALTER TABLE `menu_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_type_code` int(11) NOT NULL,
  `message` varchar(5000) DEFAULT NULL,
  `sent_by` varchar(20) DEFAULT NULL,
  `sent_to` int(11) DEFAULT NULL,
  `sent_on` date DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  KEY `message_type_code` (`message_type_code`),
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`message_type_code`) REFERENCES `messaging_type` (`message_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaging_type`
--

DROP TABLE IF EXISTS `messaging_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaging_type` (
  `message_type_code` int(11) NOT NULL AUTO_INCREMENT,
  `type_description` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`message_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaging_type`
--

LOCK TABLES `messaging_type` WRITE;
/*!40000 ALTER TABLE `messaging_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaging_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nboard_event`
--

DROP TABLE IF EXISTS `nboard_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nboard_event` (
  `event_tittle` varchar(45) DEFAULT NULL,
  `event_desc` varchar(45) DEFAULT NULL,
  `event_place` varchar(45) DEFAULT NULL,
  `n_student` varchar(45) DEFAULT NULL,
  `n_parent` varchar(45) DEFAULT NULL,
  `n_teacher` varchar(45) DEFAULT NULL,
  `n_non_teach_staff` varchar(45) DEFAULT NULL,
  `doe` date DEFAULT NULL,
  `isActive` varchar(45) DEFAULT NULL,
  `Inserted_By` varchar(45) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL,
  `toe` varchar(100) DEFAULT NULL,
  `eventsid` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nboard_event`
--

LOCK TABLES `nboard_event` WRITE;
/*!40000 ALTER TABLE `nboard_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `nboard_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nboard_news`
--

DROP TABLE IF EXISTS `nboard_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nboard_news` (
  `news_tittle` varchar(45) DEFAULT NULL,
  `news_context` varchar(45) DEFAULT NULL,
  `n_student` varchar(45) DEFAULT NULL,
  `n_parent` varchar(45) DEFAULT NULL,
  `n_teacher` varchar(45) DEFAULT NULL,
  `n_non_teach_staff` varchar(45) DEFAULT NULL,
  `dop` date DEFAULT NULL,
  `isActive` varchar(45) DEFAULT NULL,
  `Inserted_By` varchar(45) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL,
  `top` varchar(45) DEFAULT NULL,
  `newsid` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nboard_news`
--

LOCK TABLES `nboard_news` WRITE;
/*!40000 ALTER TABLE `nboard_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `nboard_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `new_mark1`
--

DROP TABLE IF EXISTS `new_mark1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `new_mark1` (
  `exam_id` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `student_id` varchar(50) DEFAULT NULL,
  `student_name` varchar(50) DEFAULT NULL,
  `sclass` varchar(50) DEFAULT NULL,
  `ssec` varchar(50) DEFAULT NULL,
  `Social` varchar(50) DEFAULT NULL,
  `Tamil` varchar(50) DEFAULT NULL,
  `Maths` varchar(50) DEFAULT NULL,
  `Science` varchar(50) DEFAULT NULL,
  `total` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `new_mark1`
--

LOCK TABLES `new_mark1` WRITE;
/*!40000 ALTER TABLE `new_mark1` DISABLE KEYS */;
/*!40000 ALTER TABLE `new_mark1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nonteachingstaff`
--

DROP TABLE IF EXISTS `nonteachingstaff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nonteachingstaff` (
  `Staff_ID` varchar(45) NOT NULL,
  `Department` varchar(45) DEFAULT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Age` varchar(3) DEFAULT NULL,
  `DOB` varchar(45) DEFAULT NULL,
  `Gender` varchar(16) DEFAULT NULL,
  `Phone_Number` varchar(20) DEFAULT NULL,
  `Contact_Number` varchar(20) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `DOJ` varchar(20) DEFAULT NULL,
  `Work_Area` varchar(100) DEFAULT NULL,
  `Current_Address` varchar(200) DEFAULT NULL,
  `Permanent_Address` varchar(200) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `Inserted_By` varchar(45) DEFAULT NULL,
  `Inserted_Date` varchar(45) DEFAULT NULL,
  `Updated_By` varchar(45) DEFAULT NULL,
  `Updated_Date` varchar(45) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Staff_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nonteachingstaff`
--

LOCK TABLES `nonteachingstaff` WRITE;
/*!40000 ALTER TABLE `nonteachingstaff` DISABLE KEYS */;
/*!40000 ALTER TABLE `nonteachingstaff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parent_master`
--

DROP TABLE IF EXISTS `parent_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parent_master` (
  `parent_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_of` int(11) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `middlename` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` char(1) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `contact_number` bigint(20) NOT NULL,
  `std_code` int(11) NOT NULL,
  `mobile_number` bigint(20) NOT NULL,
  `alt_contact_number` bigint(20) DEFAULT NULL,
  `present_address` varchar(120) NOT NULL,
  `perm_address` varchar(120) NOT NULL,
  `email_id` varchar(30) NOT NULL,
  `alt_email_id` varchar(30) DEFAULT NULL,
  `is_Active` varchar(1) NOT NULL,
  `inserted_by` varchar(10) NOT NULL,
  `inserted_date` date NOT NULL,
  `updated_by` varchar(10) NOT NULL,
  `updated_date` date NOT NULL,
  PRIMARY KEY (`parent_id`),
  KEY `parent_of` (`parent_of`),
  CONSTRAINT `parent_master_ibfk_1` FOREIGN KEY (`parent_of`) REFERENCES `students_master` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parent_master`
--

LOCK TABLES `parent_master` WRITE;
/*!40000 ALTER TABLE `parent_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `parent_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parentmaster`
--

DROP TABLE IF EXISTS `parentmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parentmaster` (
  `parent_id` varchar(40) NOT NULL,
  `prt_fname` varchar(45) DEFAULT NULL,
  `prt_father_dob` varchar(50) DEFAULT NULL,
  `prt_occupa` varchar(45) DEFAULT NULL,
  `prt_design` varchar(45) DEFAULT NULL,
  `prt_off_add` varchar(200) DEFAULT NULL,
  `prt_off_phn` varchar(20) DEFAULT NULL,
  `prt_mname` varchar(45) DEFAULT NULL,
  `prt_mother_dob` varchar(50) DEFAULT NULL,
  `mother_occupa` varchar(45) DEFAULT NULL,
  `mother_design` varchar(45) DEFAULT NULL,
  `mother_off_add` varchar(200) DEFAULT NULL,
  `mother_off_phn` varchar(20) DEFAULT NULL,
  `prt_mobile` varchar(20) DEFAULT NULL,
  `prt_email` varchar(100) DEFAULT NULL,
  `prt_photo` varchar(100) DEFAULT NULL,
  `prt_gname` varchar(45) DEFAULT NULL,
  `prt_grd_occupa` varchar(50) DEFAULT NULL,
  `prt_grd_design` varchar(45) DEFAULT NULL,
  `prt_grd_off_add` varchar(200) DEFAULT NULL,
  `prt_grd_home_add` varchar(200) DEFAULT NULL,
  `prt_grd_mobile` varchar(20) DEFAULT NULL,
  `prt_grd_email` varchar(100) DEFAULT NULL,
  `prt_grd_relation` varchar(45) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `inserted_by` varchar(10) DEFAULT NULL,
  `inserted_date` varchar(50) DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `updated_date` varchar(50) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parentmaster`
--

LOCK TABLES `parentmaster` WRITE;
/*!40000 ALTER TABLE `parentmaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `parentmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requestbook_count`
--

DROP TABLE IF EXISTS `requestbook_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requestbook_count` (
  `book_title` varchar(100) DEFAULT NULL,
  `count` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requestbook_count`
--

LOCK TABLES `requestbook_count` WRITE;
/*!40000 ALTER TABLE `requestbook_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `requestbook_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sample`
--

DROP TABLE IF EXISTS `sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sample` (
  `eid` varchar(40) NOT NULL,
  `dob` date DEFAULT NULL,
  `doj` date DEFAULT NULL,
  PRIMARY KEY (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sample`
--

LOCK TABLES `sample` WRITE;
/*!40000 ALTER TABLE `sample` DISABLE KEYS */;
/*!40000 ALTER TABLE `sample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section_master`
--

DROP TABLE IF EXISTS `section_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section_master` (
  `sclass` varchar(50) NOT NULL,
  `section` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section_master`
--

LOCK TABLES `section_master` WRITE;
/*!40000 ALTER TABLE `section_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `section_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settingadmin`
--

DROP TABLE IF EXISTS `settingadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settingadmin` (
  `sname` varchar(200) DEFAULT NULL,
  `logo_disp` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `language_allowed` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settingadmin`
--

LOCK TABLES `settingadmin` WRITE;
/*!40000 ALTER TABLE `settingadmin` DISABLE KEYS */;
/*!40000 ALTER TABLE `settingadmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4121 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_activity`
--

DROP TABLE IF EXISTS `student_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_activity` (
  `ADate` date DEFAULT NULL,
  `tid` varchar(50) DEFAULT NULL,
  `tname` varchar(50) DEFAULT NULL,
  `ttype` varchar(50) DEFAULT NULL,
  `aclass` varchar(50) DEFAULT NULL,
  `asec` varchar(50) DEFAULT NULL,
  `Place` varchar(50) DEFAULT NULL,
  `Brief_Description` varchar(50) DEFAULT NULL,
  `Description1` varchar(50) DEFAULT NULL,
  `Description2` varchar(50) DEFAULT NULL,
  `Students_Involved` varchar(50) DEFAULT NULL,
  `Upload_Photo` varchar(50) DEFAULT NULL,
  `Upload_Document` varchar(50) DEFAULT NULL,
  `widget` varchar(50) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `Inserted_By` varchar(50) DEFAULT NULL,
  `Inserted_Date` date DEFAULT NULL,
  `Updated_By` varchar(50) DEFAULT NULL,
  `Updated_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_activity`
--

LOCK TABLES `student_activity` WRITE;
/*!40000 ALTER TABLE `student_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_class`
--

DROP TABLE IF EXISTS `student_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_class` (
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `roll_no` int(11) DEFAULT NULL,
  `academic_year` varchar(12) NOT NULL,
  `seqno` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`seqno`),
  KEY `student_id` (`student_id`),
  KEY `class_id` (`class_id`),
  CONSTRAINT `student_class_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students_master` (`student_id`),
  CONSTRAINT `student_class_ibfk_2` FOREIGN KEY (`class_id`) REFERENCES `class_master` (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_class`
--

LOCK TABLES `student_class` WRITE;
/*!40000 ALTER TABLE `student_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_cocurricular`
--

DROP TABLE IF EXISTS `student_cocurricular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_cocurricular` (
  `seqno` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `activity_code` int(11) NOT NULL,
  `prize` varchar(20) DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  `house_id` int(11) DEFAULT NULL,
  `prize_date` date DEFAULT NULL,
  PRIMARY KEY (`seqno`),
  KEY `student_id` (`student_id`),
  KEY `activity_code` (`activity_code`),
  KEY `house_id` (`house_id`),
  CONSTRAINT `student_cocurricular_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students_master` (`student_id`),
  CONSTRAINT `student_cocurricular_ibfk_2` FOREIGN KEY (`activity_code`) REFERENCES `cocurricular_actvities` (`activity_code`),
  CONSTRAINT `student_cocurricular_ibfk_3` FOREIGN KEY (`house_id`) REFERENCES `house_system` (`house_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_cocurricular`
--

LOCK TABLES `student_cocurricular` WRITE;
/*!40000 ALTER TABLE `student_cocurricular` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_cocurricular` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_medical`
--

DROP TABLE IF EXISTS `student_medical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_medical` (
  `student_id` varchar(50) NOT NULL,
  `blood_group` varchar(10) DEFAULT NULL,
  `height` varchar(50) DEFAULT NULL,
  `weights` varchar(50) DEFAULT NULL,
  `medical_condition` varchar(50) DEFAULT NULL,
  `allergy` varchar(5) DEFAULT NULL,
  `allergy_type` varchar(50) NOT NULL,
  `is_Physically_Challenged` varchar(5) NOT NULL,
  `phy_cha_type` varchar(50) NOT NULL,
  `is_Special_Child` varchar(5) DEFAULT NULL,
  `specialChild_type` varchar(50) NOT NULL,
  `is_disease` varchar(5) DEFAULT NULL,
  `disease_type` varchar(50) DEFAULT NULL,
  `isActive` varchar(45) DEFAULT NULL,
  `inserted_by` varchar(45) DEFAULT NULL,
  `inserted_date` date DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_medical`
--

LOCK TABLES `student_medical` WRITE;
/*!40000 ALTER TABLE `student_medical` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_medical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_master`
--

DROP TABLE IF EXISTS `students_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_master` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_number` varchar(20) DEFAULT NULL,
  `admission_number` varchar(25) DEFAULT NULL,
  `firstname` varchar(20) NOT NULL,
  `middlename` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) NOT NULL,
  `date_of_birth` varchar(100) NOT NULL,
  `gender` varchar(12) NOT NULL,
  `joining_date` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `birthplace` varchar(20) DEFAULT NULL,
  `nationality` varchar(20) NOT NULL,
  `religion` varchar(20) DEFAULT NULL,
  `caste` varchar(20) DEFAULT NULL,
  `community` varchar(5) DEFAULT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `contact_number` bigint(20) DEFAULT NULL,
  `std_code` int(11) NOT NULL,
  `mobile_number` bigint(20) DEFAULT NULL,
  `alt_contact_number` bigint(20) DEFAULT NULL,
  `present_address` varchar(120) NOT NULL,
  `perm_address` varchar(120) NOT NULL,
  `email_id` varchar(30) NOT NULL,
  `alt_email_id` varchar(30) DEFAULT NULL,
  `is_hostellite` varchar(1) NOT NULL,
  `hostel_room` varchar(10) DEFAULT NULL,
  `hostel_block` varchar(15) DEFAULT NULL,
  `is_active` varchar(1) NOT NULL,
  `inserted_by` varchar(10) NOT NULL,
  `inserted_date` date NOT NULL,
  `updated_by` varchar(10) NOT NULL,
  `updated_date` date NOT NULL,
  PRIMARY KEY (`student_id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `students_master_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `parent_master` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_master`
--

LOCK TABLES `students_master` WRITE;
/*!40000 ALTER TABLE `students_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentsmaster`
--

DROP TABLE IF EXISTS `studentsmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentsmaster` (
  `student_id` varchar(40) NOT NULL,
  `fname` varchar(40) NOT NULL,
  `lname` varchar(40) NOT NULL,
  `sclass` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  `photo` varchar(200) NOT NULL,
  `dob` date NOT NULL,
  `bplace` varchar(45) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  `mtongue` varchar(45) DEFAULT NULL,
  `cgender` varchar(20) DEFAULT NULL,
  `religion` varchar(45) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `cemail` varchar(45) DEFAULT NULL,
  `sch_transport` varchar(45) DEFAULT NULL,
  `transport_board` varchar(45) DEFAULT NULL,
  `hostel` varchar(5) DEFAULT NULL,
  `hostel_allot` varchar(45) DEFAULT NULL,
  `Hostel_name` varchar(45) DEFAULT NULL,
  `Hostel_Room_no` varchar(50) DEFAULT NULL,
  `pre_school1` varchar(100) DEFAULT NULL,
  `pre_qual1` varchar(100) DEFAULT NULL,
  `prd_from1` varchar(50) DEFAULT NULL,
  `prd_to1` varchar(50) DEFAULT NULL,
  `pre_school2` varchar(100) DEFAULT NULL,
  `pre_qual2` varchar(100) DEFAULT NULL,
  `prd_from2` varchar(50) DEFAULT NULL,
  `prd_to2` varchar(50) DEFAULT NULL,
  `pre_school3` varchar(100) DEFAULT NULL,
  `pre_qual3` varchar(100) DEFAULT NULL,
  `prd_from3` varchar(50) DEFAULT NULL,
  `prd_to3` varchar(50) DEFAULT NULL,
  `parent_id` varchar(20) DEFAULT NULL,
  `adm_id` varchar(45) DEFAULT NULL,
  `student_roll` varchar(45) DEFAULT NULL,
  `Join_date` date DEFAULT NULL,
  `School_Name` varchar(200) DEFAULT NULL,
  `Academic_year` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentsmaster`
--

LOCK TABLES `studentsmaster` WRITE;
/*!40000 ALTER TABLE `studentsmaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `studentsmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studymaterial`
--

DROP TABLE IF EXISTS `studymaterial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studymaterial` (
  `stud_mat_id` int(11) NOT NULL,
  `stud_mat_tittle` varchar(45) DEFAULT NULL,
  `stud_mat_author` varchar(45) DEFAULT NULL,
  `study_class` varchar(45) DEFAULT NULL,
  `study_section` varchar(45) DEFAULT NULL,
  `study_subject` varchar(45) DEFAULT NULL,
  `study_chapter` varchar(45) DEFAULT NULL,
  `stud_mat_file` varchar(100) DEFAULT NULL,
  `file_path` varchar(200) DEFAULT NULL,
  `isActive` varchar(45) DEFAULT NULL,
  `inserted_By` varchar(45) DEFAULT NULL,
  `inserted_Date` date DEFAULT NULL,
  `updated_By` varchar(45) DEFAULT NULL,
  `updated_Date` date DEFAULT NULL,
  PRIMARY KEY (`stud_mat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studymaterial`
--

LOCK TABLES `studymaterial` WRITE;
/*!40000 ALTER TABLE `studymaterial` DISABLE KEYS */;
/*!40000 ALTER TABLE `studymaterial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_assign`
--

DROP TABLE IF EXISTS `subject_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject_assign` (
  `tid` varchar(45) DEFAULT NULL,
  `tname` varchar(45) DEFAULT NULL,
  `sclass` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  `ttype` varchar(45) DEFAULT NULL,
  `subject_handle` varchar(100) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_assign`
--

LOCK TABLES `subject_assign` WRITE;
/*!40000 ALTER TABLE `subject_assign` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_master`
--

DROP TABLE IF EXISTS `subject_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject_master` (
  `sclass` varchar(50) NOT NULL,
  `section` varchar(45) DEFAULT NULL,
  `subject` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_master`
--

LOCK TABLES `subject_master` WRITE;
/*!40000 ALTER TABLE `subject_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_master`
--

DROP TABLE IF EXISTS `teacher_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_master` (
  `Tid` varchar(40) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `dob` varchar(100) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  `cgender` varchar(45) DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `pin` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `cemail` varchar(45) DEFAULT NULL,
  `pan` varchar(45) DEFAULT NULL,
  `lallowed` int(5) DEFAULT NULL,
  `lapply` int(5) DEFAULT NULL,
  `lbalance` int(5) DEFAULT NULL,
  `tech_school1_name` varchar(45) DEFAULT NULL,
  `tech_per1` varchar(45) DEFAULT NULL,
  `ayear1` varchar(45) DEFAULT NULL,
  `tech_school2_name` varchar(45) DEFAULT NULL,
  `tech_per2` varchar(45) DEFAULT NULL,
  `ayear2` varchar(45) DEFAULT NULL,
  `tech_school3_name` varchar(45) DEFAULT NULL,
  `tech_per3` varchar(45) DEFAULT NULL,
  `ayear3` varchar(45) DEFAULT NULL,
  `tech_school4_name` varchar(45) DEFAULT NULL,
  `tech_per4` varchar(45) DEFAULT NULL,
  `ayear4` varchar(45) DEFAULT NULL,
  `tech_prev_emp1` varchar(45) DEFAULT NULL,
  `tech_prev_emp_desg1` varchar(45) DEFAULT NULL,
  `tech_year_of_exp1` varchar(45) DEFAULT NULL,
  `tech_prev_emp2` varchar(45) DEFAULT NULL,
  `tech_prev_emp_desg2` varchar(45) DEFAULT NULL,
  `tech_year_of_exp2` varchar(45) DEFAULT NULL,
  `tech_prev_emp3` varchar(45) DEFAULT NULL,
  `tech_prev_emp_desg3` varchar(45) DEFAULT NULL,
  `tech_year_of_exp3` varchar(45) DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `created_date` varchar(100) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `updated_date` varchar(100) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_master`
--

LOCK TABLES `teacher_master` WRITE;
/*!40000 ALTER TABLE `teacher_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terms`
--

DROP TABLE IF EXISTS `terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terms` (
  `terms` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terms`
--

LOCK TABLES `terms` WRITE;
/*!40000 ALTER TABLE `terms` DISABLE KEYS */;
/*!40000 ALTER TABLE `terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time_period`
--

DROP TABLE IF EXISTS `time_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_period` (
  `period` varchar(10) NOT NULL,
  `time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`period`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time_period`
--

LOCK TABLES `time_period` WRITE;
/*!40000 ALTER TABLE `time_period` DISABLE KEYS */;
/*!40000 ALTER TABLE `time_period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time_table`
--

DROP TABLE IF EXISTS `time_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_table` (
  `sclass` varchar(40) DEFAULT NULL,
  `section` varchar(40) DEFAULT NULL,
  `day` varchar(40) DEFAULT NULL,
  `time1` varchar(100) DEFAULT NULL,
  `time1_hr` varchar(100) DEFAULT NULL,
  `time2` varchar(100) DEFAULT NULL,
  `time2_hr` varchar(100) DEFAULT NULL,
  `time3` varchar(100) DEFAULT NULL,
  `time3_hr` varchar(100) DEFAULT NULL,
  `time4` varchar(100) DEFAULT NULL,
  `time4_hr` varchar(100) DEFAULT NULL,
  `time5` varchar(100) DEFAULT NULL,
  `time5_hr` varchar(45) DEFAULT NULL,
  `time6` varchar(45) DEFAULT NULL,
  `time6_hr` varchar(45) DEFAULT NULL,
  `time7` varchar(45) DEFAULT NULL,
  `time7_hr` varchar(45) DEFAULT NULL,
  `time8` varchar(45) DEFAULT NULL,
  `time8_hr` varchar(45) DEFAULT NULL,
  `time9` varchar(45) DEFAULT NULL,
  `time9_hr` varchar(45) DEFAULT NULL,
  `time10` varchar(45) DEFAULT NULL,
  `time10_hr` varchar(45) DEFAULT NULL,
  `time11` varchar(45) DEFAULT NULL,
  `time11_hr` varchar(45) DEFAULT NULL,
  `time12` varchar(45) DEFAULT NULL,
  `time12_hr` varchar(45) DEFAULT NULL,
  `time13` varchar(45) DEFAULT NULL,
  `time13_hr` varchar(45) DEFAULT NULL,
  `time14` varchar(45) DEFAULT NULL,
  `time14_hr` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time_table`
--

LOCK TABLES `time_table` WRITE;
/*!40000 ALTER TABLE `time_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `time_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transport`
--

DROP TABLE IF EXISTS `transport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transport` (
  `transport_name` varchar(25) NOT NULL,
  `transport_type` varchar(45) DEFAULT NULL,
  `registration_number` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `no_seats` int(11) DEFAULT NULL,
  `fee` int(11) DEFAULT NULL,
  `area_cover` varchar(200) DEFAULT NULL,
  `board_points` varchar(45) DEFAULT NULL,
  `dri_photo` varchar(45) DEFAULT NULL,
  `dri_name` varchar(45) CHARACTER SET big5 DEFAULT NULL,
  `dri_age` int(11) DEFAULT NULL,
  `dri_contact_number` varchar(40) DEFAULT NULL,
  `dri_alter_contact_number` varchar(40) DEFAULT NULL,
  `dlicense` varchar(60) DEFAULT NULL,
  `dri_address` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cond_photo` varchar(45) DEFAULT NULL,
  `cond_name` varchar(45) DEFAULT NULL,
  `cond_age` int(11) DEFAULT NULL,
  `cond_contact_number` varchar(40) DEFAULT NULL,
  `cond_alter_contact_number` varchar(40) DEFAULT NULL,
  `cond_address` varchar(45) DEFAULT NULL,
  `route_num` varchar(45) DEFAULT NULL,
  `uname` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`transport_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transport`
--

LOCK TABLES `transport` WRITE;
/*!40000 ALTER TABLE `transport` DISABLE KEYS */;
/*!40000 ALTER TABLE `transport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transport_attendance`
--

DROP TABLE IF EXISTS `transport_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transport_attendance` (
  `adate` varchar(50) DEFAULT NULL,
  `duser` varchar(100) DEFAULT NULL,
  `boarding` varchar(1000) DEFAULT NULL,
  `route_num` varchar(100) DEFAULT NULL,
  `student_id` varchar(1000) DEFAULT NULL,
  `student_name` varchar(1000) DEFAULT NULL,
  `sclass` varchar(100) DEFAULT NULL,
  `section` varchar(100) DEFAULT NULL,
  `attendance` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transport_attendance`
--

LOCK TABLES `transport_attendance` WRITE;
/*!40000 ALTER TABLE `transport_attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `transport_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login_details`
--

DROP TABLE IF EXISTS `user_login_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_login_details` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(45) NOT NULL,
  `Password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `username_2` (`UserName`),
  CONSTRAINT `user_login_details_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `parent_master` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login_details`
--

LOCK TABLES `user_login_details` WRITE;
/*!40000 ALTER TABLE `user_login_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_login_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `attendance_day1`
--

/*!50001 DROP VIEW IF EXISTS `attendance_day1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `attendance_day1` AS select monthname(`attendance`.`adate`) AS `month`,`attendance`.`aclass` AS `aclass`,`attendance`.`asection` AS `asection`,`attendance`.`sid` AS `sid`,`attendance`.`fname` AS `fname`,group_concat(if((dayofmonth(`attendance`.`adate`) = 1),`attendance`.`att_status`,NULL) separator ',') AS `day1`,group_concat(if((dayofmonth(`attendance`.`adate`) = 2),`attendance`.`att_status`,NULL) separator ',') AS `day2`,group_concat(if((dayofmonth(`attendance`.`adate`) = 3),`attendance`.`att_status`,NULL) separator ',') AS `day3`,group_concat(if((dayofmonth(`attendance`.`adate`) = 4),`attendance`.`att_status`,NULL) separator ',') AS `day4`,group_concat(if((dayofmonth(`attendance`.`adate`) = 5),`attendance`.`att_status`,NULL) separator ',') AS `day5`,group_concat(if((dayofmonth(`attendance`.`adate`) = 6),`attendance`.`att_status`,NULL) separator ',') AS `day6`,group_concat(if((dayofmonth(`attendance`.`adate`) = 7),`attendance`.`att_status`,NULL) separator ',') AS `day7`,group_concat(if((dayofmonth(`attendance`.`adate`) = 8),`attendance`.`att_status`,NULL) separator ',') AS `day8`,group_concat(if((dayofmonth(`attendance`.`adate`) = 9),`attendance`.`att_status`,NULL) separator ',') AS `day9`,group_concat(if((dayofmonth(`attendance`.`adate`) = 10),`attendance`.`att_status`,NULL) separator ',') AS `day10`,group_concat(if((dayofmonth(`attendance`.`adate`) = 11),`attendance`.`att_status`,NULL) separator ',') AS `day11`,group_concat(if((dayofmonth(`attendance`.`adate`) = 12),`attendance`.`att_status`,NULL) separator ',') AS `day12`,group_concat(if((dayofmonth(`attendance`.`adate`) = 13),`attendance`.`att_status`,NULL) separator ',') AS `day13`,group_concat(if((dayofmonth(`attendance`.`adate`) = 14),`attendance`.`att_status`,NULL) separator ',') AS `day14`,group_concat(if((dayofmonth(`attendance`.`adate`) = 15),`attendance`.`att_status`,NULL) separator ',') AS `day15`,group_concat(if((dayofmonth(`attendance`.`adate`) = 16),`attendance`.`att_status`,NULL) separator ',') AS `day16`,group_concat(if((dayofmonth(`attendance`.`adate`) = 17),`attendance`.`att_status`,NULL) separator ',') AS `day17`,group_concat(if((dayofmonth(`attendance`.`adate`) = 18),`attendance`.`att_status`,NULL) separator ',') AS `day18`,group_concat(if((dayofmonth(`attendance`.`adate`) = 19),`attendance`.`att_status`,NULL) separator ',') AS `day19`,group_concat(if((dayofmonth(`attendance`.`adate`) = 20),`attendance`.`att_status`,NULL) separator ',') AS `day20`,group_concat(if((dayofmonth(`attendance`.`adate`) = 21),`attendance`.`att_status`,NULL) separator ',') AS `day21`,group_concat(if((dayofmonth(`attendance`.`adate`) = 22),`attendance`.`att_status`,NULL) separator ',') AS `day22`,group_concat(if((dayofmonth(`attendance`.`adate`) = 23),`attendance`.`att_status`,NULL) separator ',') AS `day23`,group_concat(if((dayofmonth(`attendance`.`adate`) = 24),`attendance`.`att_status`,NULL) separator ',') AS `day24`,group_concat(if((dayofmonth(`attendance`.`adate`) = 25),`attendance`.`att_status`,NULL) separator ',') AS `day25`,group_concat(if((dayofmonth(`attendance`.`adate`) = 26),`attendance`.`att_status`,NULL) separator ',') AS `day26`,group_concat(if((dayofmonth(`attendance`.`adate`) = 27),`attendance`.`att_status`,NULL) separator ',') AS `day27`,group_concat(if((dayofmonth(`attendance`.`adate`) = 28),`attendance`.`att_status`,NULL) separator ',') AS `day28`,group_concat(if((dayofmonth(`attendance`.`adate`) = 29),`attendance`.`att_status`,NULL) separator ',') AS `day29`,group_concat(if((dayofmonth(`attendance`.`adate`) = 30),`attendance`.`att_status`,NULL) separator ',') AS `day30`,group_concat(if((dayofmonth(`attendance`.`adate`) = 31),`attendance`.`att_status`,NULL) separator ',') AS `day31` from `attendance` group by rtrim(ltrim(`attendance`.`sid`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `attendance_day2`
--

/*!50001 DROP VIEW IF EXISTS `attendance_day2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `attendance_day2` AS select 1 AS `month`,1 AS `tid`,1 AS `tname`,1 AS `day1`,1 AS `day2`,1 AS `day3`,1 AS `day4`,1 AS `day5`,1 AS `day6`,1 AS `day7`,1 AS `day8`,1 AS `day9`,1 AS `day10`,1 AS `day11`,1 AS `day12`,1 AS `day13`,1 AS `day14`,1 AS `day15`,1 AS `day16`,1 AS `day17`,1 AS `day18`,1 AS `day19`,1 AS `day20`,1 AS `day21`,1 AS `day22`,1 AS `day23`,1 AS `day24`,1 AS `day25`,1 AS `day26`,1 AS `day27`,1 AS `day28`,1 AS `day29`,1 AS `day30`,1 AS `day31`,1 AS `totalP` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `attendance_dayStudentsTest`
--

/*!50001 DROP VIEW IF EXISTS `attendance_dayStudentsTest`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `attendance_dayStudentsTest` AS select monthname(`attendance`.`adate`) AS `month`,`attendance`.`aclass` AS `aclass`,`attendance`.`asection` AS `asection`,`attendance`.`sid` AS `sid`,`attendance`.`fname` AS `fname`,month(`attendance`.`adate`) AS `mondate`,year(`attendance`.`adate`) AS `monyear`,group_concat(if((dayofmonth(`attendance`.`adate`) = 1),`attendance`.`att_status`,NULL) separator ',') AS `day1`,group_concat(if((dayofmonth(`attendance`.`adate`) = 2),`attendance`.`att_status`,NULL) separator ',') AS `day2`,group_concat(if((dayofmonth(`attendance`.`adate`) = 3),`attendance`.`att_status`,NULL) separator ',') AS `day3`,group_concat(if((dayofmonth(`attendance`.`adate`) = 4),`attendance`.`att_status`,NULL) separator ',') AS `day4`,group_concat(if((dayofmonth(`attendance`.`adate`) = 5),`attendance`.`att_status`,NULL) separator ',') AS `day5`,group_concat(if((dayofmonth(`attendance`.`adate`) = 6),`attendance`.`att_status`,NULL) separator ',') AS `day6`,group_concat(if((dayofmonth(`attendance`.`adate`) = 7),`attendance`.`att_status`,NULL) separator ',') AS `day7`,group_concat(if((dayofmonth(`attendance`.`adate`) = 8),`attendance`.`att_status`,NULL) separator ',') AS `day8`,group_concat(if((dayofmonth(`attendance`.`adate`) = 9),`attendance`.`att_status`,NULL) separator ',') AS `day9`,group_concat(if((dayofmonth(`attendance`.`adate`) = 10),`attendance`.`att_status`,NULL) separator ',') AS `day10`,group_concat(if((dayofmonth(`attendance`.`adate`) = 11),`attendance`.`att_status`,NULL) separator ',') AS `day11`,group_concat(if((dayofmonth(`attendance`.`adate`) = 12),`attendance`.`att_status`,NULL) separator ',') AS `day12`,group_concat(if((dayofmonth(`attendance`.`adate`) = 13),`attendance`.`att_status`,NULL) separator ',') AS `day13`,group_concat(if((dayofmonth(`attendance`.`adate`) = 14),`attendance`.`att_status`,NULL) separator ',') AS `day14`,group_concat(if((dayofmonth(`attendance`.`adate`) = 15),`attendance`.`att_status`,NULL) separator ',') AS `day15`,group_concat(if((dayofmonth(`attendance`.`adate`) = 16),`attendance`.`att_status`,NULL) separator ',') AS `day16`,group_concat(if((dayofmonth(`attendance`.`adate`) = 17),`attendance`.`att_status`,NULL) separator ',') AS `day17`,group_concat(if((dayofmonth(`attendance`.`adate`) = 18),`attendance`.`att_status`,NULL) separator ',') AS `day18`,group_concat(if((dayofmonth(`attendance`.`adate`) = 19),`attendance`.`att_status`,NULL) separator ',') AS `day19`,group_concat(if((dayofmonth(`attendance`.`adate`) = 20),`attendance`.`att_status`,NULL) separator ',') AS `day20`,group_concat(if((dayofmonth(`attendance`.`adate`) = 21),`attendance`.`att_status`,NULL) separator ',') AS `day21`,group_concat(if((dayofmonth(`attendance`.`adate`) = 22),`attendance`.`att_status`,NULL) separator ',') AS `day22`,group_concat(if((dayofmonth(`attendance`.`adate`) = 23),`attendance`.`att_status`,NULL) separator ',') AS `day23`,group_concat(if((dayofmonth(`attendance`.`adate`) = 24),`attendance`.`att_status`,NULL) separator ',') AS `day24`,group_concat(if((dayofmonth(`attendance`.`adate`) = 25),`attendance`.`att_status`,NULL) separator ',') AS `day25`,group_concat(if((dayofmonth(`attendance`.`adate`) = 26),`attendance`.`att_status`,NULL) separator ',') AS `day26`,group_concat(if((dayofmonth(`attendance`.`adate`) = 27),`attendance`.`att_status`,NULL) separator ',') AS `day27`,group_concat(if((dayofmonth(`attendance`.`adate`) = 28),`attendance`.`att_status`,NULL) separator ',') AS `day28`,group_concat(if((dayofmonth(`attendance`.`adate`) = 29),`attendance`.`att_status`,NULL) separator ',') AS `day29`,group_concat(if((dayofmonth(`attendance`.`adate`) = 30),`attendance`.`att_status`,NULL) separator ',') AS `day30`,group_concat(if((dayofmonth(`attendance`.`adate`) = 31),`attendance`.`att_status`,NULL) separator ',') AS `day31` from `attendance` group by rtrim(ltrim(`attendance`.`sid`)),month(`attendance`.`adate`),year(`attendance`.`adate`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `attendance_dayTeacher`
--

/*!50001 DROP VIEW IF EXISTS `attendance_dayTeacher`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `attendance_dayTeacher` AS select monthname(`attendance_teacher`.`adate`) AS `month`,`attendance_teacher`.`tid` AS `tid`,`attendance_teacher`.`tname` AS `tname`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 1),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day1`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 2),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day2`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 3),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day3`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 4),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day4`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 5),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day5`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 6),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day6`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 7),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day7`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 8),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day8`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 9),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day9`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 10),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day10`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 11),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day11`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 12),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day12`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 13),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day13`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 14),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day14`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 15),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day15`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 16),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day16`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 17),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day17`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 18),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day18`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 19),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day19`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 20),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day20`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 21),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day21`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 22),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day22`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 23),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day23`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 24),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day24`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 25),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day25`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 26),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day26`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 27),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day27`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 28),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day28`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 29),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day29`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 30),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day30`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 31),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day31` from `attendance_teacher` group by rtrim(ltrim(`attendance_teacher`.`tid`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `attendance_dayTeacher_Test`
--

/*!50001 DROP VIEW IF EXISTS `attendance_dayTeacher_Test`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `attendance_dayTeacher_Test` AS select monthname(`attendance_teacher`.`adate`) AS `month`,`attendance_teacher`.`tid` AS `tid`,`attendance_teacher`.`tname` AS `tname`,month(`attendance_teacher`.`adate`) AS `mondate`,year(`attendance_teacher`.`adate`) AS `monyear`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 1),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day1`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 2),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day2`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 3),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day3`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 4),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day4`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 5),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day5`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 6),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day6`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 7),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day7`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 8),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day8`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 9),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day9`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 10),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day10`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 11),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day11`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 12),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day12`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 13),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day13`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 14),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day14`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 15),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day15`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 16),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day16`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 17),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day17`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 18),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day18`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 19),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day19`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 20),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day20`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 21),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day21`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 22),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day22`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 23),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day23`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 24),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day24`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 25),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day25`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 26),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day26`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 27),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day27`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 28),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day28`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 29),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day29`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 30),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day30`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 31),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day31` from `attendance_teacher` group by rtrim(ltrim(`attendance_teacher`.`tid`)),month(`attendance_teacher`.`adate`),year(`attendance_teacher`.`adate`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `attendance_dayTeacher_test`
--

/*!50001 DROP VIEW IF EXISTS `attendance_dayTeacher_test`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `attendance_dayTeacher_test` AS select monthname(`attendance_teacher`.`adate`) AS `month`,month(`attendance_teacher`.`adate`) AS `month(adate)`,year(`attendance_teacher`.`adate`) AS `year(adate)`,`attendance_teacher`.`tid` AS `tid`,`attendance_teacher`.`tname` AS `tname`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 1),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day1`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 2),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day2`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 3),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day3`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 4),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day4`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 5),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day5`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 6),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day6`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 7),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day7`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 8),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day8`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 9),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day9`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 10),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day10`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 11),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day11`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 12),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day12`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 13),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day13`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 14),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day14`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 15),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day15`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 16),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day16`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 17),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day17`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 18),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day18`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 19),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day19`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 20),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day20`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 21),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day21`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 22),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day22`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 23),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day23`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 24),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day24`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 25),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day25`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 26),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day26`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 27),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day27`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 28),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day28`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 29),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day29`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 30),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day30`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 31),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day31` from `attendance_teacher` group by rtrim(ltrim(`attendance_teacher`.`tid`)),month(`attendance_teacher`.`adate`),year(`attendance_teacher`.`adate`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `attendance_dayTeacher_tt`
--

/*!50001 DROP VIEW IF EXISTS `attendance_dayTeacher_tt`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `attendance_dayTeacher_tt` AS select monthname(`attendance_teacher`.`adate`) AS `month`,`attendance_teacher`.`tid` AS `tid`,`attendance_teacher`.`tname` AS `tname`,month(`attendance_teacher`.`adate`) AS `month(adate)`,year(`attendance_teacher`.`adate`) AS `year(adate)`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 1),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day1`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 2),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day2`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 3),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day3`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 4),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day4`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 5),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day5`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 6),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day6`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 7),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day7`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 8),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day8`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 9),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day9`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 10),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day10`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 11),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day11`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 12),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day12`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 13),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day13`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 14),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day14`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 15),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day15`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 16),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day16`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 17),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day17`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 18),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day18`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 19),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day19`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 20),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day20`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 21),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day21`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 22),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day22`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 23),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day23`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 24),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day24`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 25),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day25`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 26),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day26`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 27),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day27`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 28),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day28`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 29),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day29`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 30),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day30`,group_concat(if((dayofmonth(`attendance_teacher`.`adate`) = 31),`attendance_teacher`.`att_status`,NULL) separator ',') AS `day31` from `attendance_teacher` group by rtrim(ltrim(`attendance_teacher`.`tid`)),month(`attendance_teacher`.`adate`),year(`attendance_teacher`.`adate`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `attendance_dayteacher`
--

/*!50001 DROP VIEW IF EXISTS `attendance_dayteacher`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `attendance_dayteacher` AS select 1 AS `month`,1 AS `tid`,1 AS `tname`,1 AS `day1`,1 AS `day2`,1 AS `day3`,1 AS `day4`,1 AS `day5`,1 AS `day6`,1 AS `day7`,1 AS `day8`,1 AS `day9`,1 AS `day10`,1 AS `day11`,1 AS `day12`,1 AS `day13`,1 AS `day14`,1 AS `day15`,1 AS `day16`,1 AS `day17`,1 AS `day18`,1 AS `day19`,1 AS `day20`,1 AS `day21`,1 AS `day22`,1 AS `day23`,1 AS `day24`,1 AS `day25`,1 AS `day26`,1 AS `day27`,1 AS `day28`,1 AS `day29`,1 AS `day30`,1 AS `day31` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `colu`
--

/*!50001 DROP VIEW IF EXISTS `colu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `colu` AS select 1 AS `cname` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `markentryalldump`
--

/*!50001 DROP VIEW IF EXISTS `markentryalldump`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `markentryalldump` AS select 1 AS `exam_id`,1 AS `sclass`,1 AS `ssec`,1 AS `student_id`,1 AS `student_name`,1 AS `Tamil`,1 AS `English`,1 AS `Maths`,1 AS `Science` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `markentryalldumphead`
--

/*!50001 DROP VIEW IF EXISTS `markentryalldumphead`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `markentryalldumphead` AS select 1 AS `student_id`,1 AS `student_name`,1 AS `Tamil`,1 AS `English`,1 AS `Maths`,1 AS `Science` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `markentrydump`
--

/*!50001 DROP VIEW IF EXISTS `markentrydump`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `markentrydump` AS select 1 AS `student_id`,1 AS `tamil`,1 AS `english`,1 AS `maths`,1 AS `total` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `markentrydump11`
--

/*!50001 DROP VIEW IF EXISTS `markentrydump11`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `markentrydump11` AS select `t`.`exam_id` AS `exam_id`,`t`.`student_id` AS `student_id`,`t`.`student_name` AS `student_name`,`t`.`sclass` AS `sclass`,`t`.`ssec` AS `ssec`,max((case when (`t`.`subject` = 'Social') then `t`.`mark` else NULL end)) AS `Social`,max((case when (`t`.`subject` = 'Tamil') then `t`.`mark` else NULL end)) AS `Tamil`,max((case when (`t`.`subject` = 'Maths') then `t`.`mark` else NULL end)) AS `Maths`,max((case when (`t`.`subject` = 'Science') then `t`.`mark` else NULL end)) AS `Science`,sum(`t`.`mark`) AS `total` from `marks_entry_all` `t` where (`t`.`sclass` = '1st-Std') group by `t`.`student_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-06 15:13:30
