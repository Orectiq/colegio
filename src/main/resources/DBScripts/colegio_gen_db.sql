-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2017 at 09:07 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `colegio_gen_db`
--
CREATE DATABASE IF NOT EXISTS `colegio_gen_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `colegio_gen_db`;

-- --------------------------------------------------------

--
-- Table structure for table `school_info`
--

DROP TABLE IF EXISTS `school_info`;
CREATE TABLE `school_info` (
  `school_id` varchar(15) NOT NULL,
  `school_name` varchar(100) NOT NULL,
  `school_phone_primary` varchar(20) DEFAULT NULL,
  `school_address` varchar(100) DEFAULT NULL,
  `school_membership` int(11) NOT NULL,
  `school_phone_secondary` varchar(20) DEFAULT NULL,
  `school_email` varchar(50) DEFAULT NULL,
  `school_db` varchar(50) NOT NULL,
  `school_db_user` varchar(50) NOT NULL,
  `school_db_pass` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
