<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Register Page | Colegio - School Management System</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="../../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/layouts/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">

  <style type="text/css">
/*.Register-page {
      float: none!important;
      margin: auto!important;
      margin-let:0px;
      margin-right:0px;
    }*/
  .input-field div.error{
    position: relative;
    top: -1rem;
    left: 0rem;
    font-size: 0.8rem;
    color:#FF4081;
    -webkit-transform: translateY(0%);
    -ms-transform: translateY(0%);
    -o-transform: translateY(0%);
    transform: translateY(0%);
  }
  .input-field label.active{
      width:100%;
  }
  .left-alert input[type=text] + label:after, 
  .left-alert input[type=password] + label:after, 
  .left-alert input[type=email] + label:after, 
  .left-alert input[type=url] + label:after, 
  .left-alert input[type=time] + label:after,
  .left-alert input[type=date] + label:after, 
  .left-alert input[type=datetime-local] + label:after, 
  .left-alert input[type=tel] + label:after, 
  .left-alert input[type=number] + label:after, 
  .left-alert input[type=search] + label:after, 
  .left-alert textarea.materialize-textarea + label:after{
      left:0px;
  }
  .right-alert input[type=text] + label:after, 
  .right-alert input[type=password] + label:after, 
  .right-alert input[type=email] + label:after, 
  .right-alert input[type=url] + label:after, 
  .right-alert input[type=time] + label:after,
  .right-alert input[type=date] + label:after, 
  .right-alert input[type=datetime-local] + label:after, 
  .right-alert input[type=tel] + label:after, 
  .right-alert input[type=number] + label:after, 
  .right-alert input[type=search] + label:after, 
  .right-alert textarea.materialize-textarea + label:after{
      right:70px;
  }
  </style>
</head>

<body class="cyan reg_bg">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <div id="Register-page" class="row">
    <div class="col s12 m6 offset-m3 l4 offset-l4 z-depth-6">
      <div class="col s12 m12 l12">
        <div class="card-panel">
         <h4 class="header2">Register here</h4>
         <div class="row">
            <form class="formValidate" id="formValidate" method="g et" action="">
                <div class="row">
                    <div class="input-field col s12">
                        <label for="srole">Role *</label>
                        <input id="srole" name="srole" type="text" data-error=".errorTxt9">
                        <div class="errorTxt9"></div>
                    </div>
                    <div class="input-field col s12">
                        <label for="sname">School Name *</label>
                        <input id="sname" name="sname" type="text" data-error=".errorTxt7">
                        <div class="errorTxt7"></div>
                    </div>
                    
									<div class="col s12">
                  					<form action="#">
                   					 <div class="file-field input-field">
                     					 <div class="btn">
                       					 <span>Logo *</span>
                       					 <input type="file" id="logo" name="logo">
                     					 </div>
                     					 <div class="file-path-wrapper">
                     					   <input class="file-path validate" type="text">
                    					  </div>
                    					</div>
                 					 </form>
               						 </div>
                    <div class="input-field col s12">
                        <label for="ayear">Academic Year *</label>
                        <input id="ayear" name="ayear" type="text" data-error=".errorTxt9">
                        <div class="errorTxt9"></div>
                    </div>
                    <div class="input-field col s12">
                        <label for="uname">Username *</label>
                        <input id="uname" name="uname" type="text" data-error=".errorTxt1">
                        <div class="errorTxt1"></div>
                    </div>
                    <div class="input-field col s12">
                        <label for="password">Password *</label>
                        <input id="password" type="password" name="password" data-error=".errorTxt3">
                        <div class="errorTxt3"></div>
                    </div>
                    <div class="input-field col s12">
                        <label for="cpassword">Confirm Password *</label>
                        <input id="cpassword" type="password" name="cpassword" data-error=".errorTxt4">
                        <div class="errorTxt4"></div>
                    </div>
                    <div class="input-field col s12">
                        <label for="cemail">E-Mail *</label>
                        <input id="cemail" type="email" name="cemail" data-error=".errorTxt2">
                        <div class="errorTxt2"></div>
                    </div>
                    <div class="input-field col s12">
                        <label for="sphone">Phone No *</label>
                        <input id="sphone" type="text" name="sphone" data-error=".errorTxt2">
                        <div class="errorTxt2"></div>
                    </div>
                    <!--<div class="input-field col s12">
                        <label for="curl">URL *</label>
                        <input id="curl" type="url" name="curl" data-error=".errorTxt5">
                        <div class="errorTxt5"></div>
                    </div>-->
                    <div class="input-field col s12">
                        <textarea id="saddress" name="saddress" class="materialize-textarea validate" data-error=".errorTxt7"></textarea>
                        <label for="saddress">Address *</label>
                        <div class="errorTxt7"></div>
                    </div>
                    <div class="input-field col s12">
                        <label for="sbranch">Branch *</label>
                        <input id="sbranch" name="sbranch" type="text" data-error=".errorTxt9">
                        <div class="errorTxt9"></div>
                    </div>
                    <div class="col s12">
                        <label for="tnc_select">T&C *</label>
                        <p>
                            <input type="checkbox" class="checkbox" id="cagree" name="cagree" data-error=".errorTxt9" /> 
                            <label for="cagree">Please agree to our policy</label>
                         </p>
                              <div class="input-field">
                                  <div class="errorTxt6"></div>
                              </div>
                    </div>
                    <div class="input-field col s12">
                        <!--  <button class="btn waves-effect waves-light right submit" type="submit" name="action">Submit
                          <i class="mdi-content-send right"></i>
                        </button> -->
                        <a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="masterInsert()" name="final" id="final">Submit</a>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>



  <!-- ================================================
    Scripts
    ================================================ -->

     <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--angularjs-->
    <script type="text/javascript" src="../../js/plugins/angular.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism -->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>
    
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery-validation/additional-methods.min.js"></script>
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
    <script> var ctxPath = "<%=request.getContextPath() %>";</script>

    <script type="text/javascript">
    $("#formValidate").validate({
        rules: {
            uname: {
                required: true,
                minlength: 5
            },
            cemail: {
                required: true,
                //email:true
            },
            password: {
        required: true,
        minlength: 5
      },
      cpassword: {
        required: true,
        minlength: 5,
        equalTo: "#password"
      },
      curl: {
                required: true,
                url:true
            },
            crole:"required",
            ccomment: {
        required: true,
        minlength: 15
            },
            cgender:"required",
      cagree:"required",
        },
        //For custom messages
        messages: {
            uname:{
                required: "Enter a username",
                minlength: "Enter at least 5 characters"
            },
            curl: "Enter your website",
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
    </script>
    
    
    
    <script>
    referenceValue();
    function referenceValue()
	{
		alert("ref. value enter 1");
		 var dt=new Date();
		 
		 var d1=dt.getFullYear();
		 var d2=dt.getFullYear()+1;
		 var d3=d1+"-"+d2;
		 $("#ayear").val(d3)
		 
	}
    
    function masterInsert()
	{
		 alert("master Insert entered111111111111111");
		 var dt=new Date();
		 
		 $.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/user/insertMaster.do?',
	        //data: JSON.stringify(data),
	        data: {"role":$("#srole").val(),"schoolName":$("#sname").val(),"Logo":$("#logo").val(),"aYear":$("#ayear").val(),
	        	   "uname":$("#uname").val(),"pass":$("#password").val(),"rpass":$("#cpassword").val(),"email":$("#cemail").val(),
	        	   "phone":$("#sphone").val(),"address":$("#saddress").val(),"branch":$("#sbranch").val()},
	        success: function(data) 
	        {
	       	 if(data=='success')
	       	 {
	       		Materialize.toast("Success",4000);	 
	       	 }
	       	 else 
	       	 {
	       		 Materialize.toast(data,4000);	 
	       	 }  
	        }
		 });
	}
    
    </script>
    
    
</body>

</html>