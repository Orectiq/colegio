
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Student List - Colegio - A Orectiq Product</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
  .class_active{display:block!important;opacity:1!important;}
  .teacher_class{display:none;}
  .teacher_class_section{display:none;}
  .teacher_subject{display:none;}
  .student_class_section_sbt{display:none;}
  
  
  #tags{ 
float:left; 
border:1px solid #ccc; 
padding:5px; 
font-family:Arial; 
} 
#tags > span{ 
cursor:pointer; 
display:block; 
float:left; 
color:#fff; 
background:#789; 
padding:5px; 
padding-right:25px; 
margin:4px; 
} 
#tags > span:hover{ 
opacity:0.7; 
} 
#tags > span:after{ 
position:absolute; 
content:"Ã"; 
border:1px solid; 
padding:2px 5px; 
margin-left:3px; 
font-size:11px; 
} 
#tags > input{ 
background:#eee; 
border:0; 
margin:4px; 
padding:7px; 
width:auto; 
}

</style>



</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                     <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=admission/Admission' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_Main' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/ParentsView' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=calender/app-calendar' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Activity_List' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff List</a>
                                    </li>
                                      <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachinglist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachingview' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach Staff</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <%-- <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examlist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Marks </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=MarkSheet/Marks_ClassList' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class wise Marks</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Finance</a>
                            <div class="collapsible-body">
                                <ul>
                                   <%--  <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_EChallan'>E-Challan</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeeDraft'>Fee Draft</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeesList'>Fee List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/FeeAllotMainpage'>Mainpage</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                 	    
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> News Feed </a>
                			<div class="collapsible-body">
                                <ul>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/events_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/news_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                       
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=SMS/SmsOrEmail' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> SMS/Email</a>
                        </li>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
         
			
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">E-challan</h5>
                <ol class="breadcrumbs">
                    <li><a href="index.html">Accounting</a></li>
                    <li class="active">E-challan</li>
                </ol>
              </div>
            </div>
          </div>

        </div>
        <!--breadcrumbs end-->
		<div class="container">
		<form class="formValidate" id="slick-login" method="post">
		<!-- <div id="floatThead" class="cyan lighten-4"> -->

			<div class="input-field col m6">
				<label for="challan_id">Challan ID</label> <input
				id="challan_id" name="challan_id" type="text"
				maxlength="50" class="textOnly stu_require"
				data-error="Please enter your challan Id">
				<div class="error errorTxt1"></div>										
			</div>
 
			<div class="input-field col m12 ">	
				<input type="date" id="theDate">
			</div> 
		
		
		
  
  <div class="row">
  
	  
	  
	  
	  <div class="input-field col m6">
														<label for="fee_name">Fee Name</label> <input
															id="fee_name" name="fee_name" type="text" maxlength="50"
															class="textOnly stu_require"
															data-error="Please enter your feename">
														<div class="error errorTxt2"></div>
													</div>
	
	 <div class="input-field col m6">
														<label for="Academic_Year">Academic Year</label> <input
															id="Academic_Year" name="Academic_Year" type="text" maxlength="50"
															class="textOnly stu_require"
															data-error="Please enter your Academic_Year">
														<div class="error errorTxt3"></div>
													</div>
	    
					</div>
				

	
 <!--Preselecting a tab-->
          <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
               
                    <div id="stu_list_all" class="col s12  cyan lighten-4">
                     		<!--DataTables example-->
				            <div id="#student-datatables_1">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">
				                <div class="m12">
				                  <table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0">
				                    <thead>
				                        <tr>
				                            <th>S.NO</th>
				                            <th>Description</th> 
				                            <th>Amount</th>
				                        </tr>
				                    </thead>
				                 
				                    <tfoot>
				                        <tr>
				                            <th>S.NO</th>
				                             <th>Description</th>
				                            <th>Amount</th>
				                        </tr>
				                    </tfoot>
				                 
				                    <tbody>
				                        <tr>
				                        	<td>01</td>
				                           
				                            <td>
											  <div class="input-field col m12">
														<label for="Fee_Break1">Fee Break</label> <input
															id="Fee_Break1" name="Fee_Break1" type="text" maxlength="50"
															class="textOnly stu_require"
															data-error="Please enter your Fee_Break1">
														<div class="error errorTxt4"></div>
														
													</div>
													
										
										
											</td>
				                           
				                            <td>
											<div class="input-field col m12">
														<label for="amount1">Amount</label> <input
															id="amount1" name="amount1" type="text" maxlength="50"
															class="textOnly stu_require"
															data-error="Please enter your amount1">
														<div class="error errorTxt5"></div>
														
													</div>
   
    
											
											
											</td>
				                            
				                        </tr>
				                        <tr>
				                        	<td>02</td>
				                           
				                            <td>
											  <div class="input-field col m12">
														<label for="Fee_Break2">Fee Break</label> <input
															id="Fee_Break2" name="Fee_Break2" type="text" maxlength="50"
															class="textOnly stu_require"
															data-error="Please enter your Fee_Break2">
														<div class="error errorTxt6"></div>
														
													</div>
													
										
										
											</td>
				                           
				                            <td>
											<div class="input-field col m12">
														<label for="amount2">Amount</label> <input
															id="amount2" name="amount2" type="text" maxlength="50"
															class="textOnly stu_require"
															data-error="Please enter your amount2">
														<div class="error errorTxt7"></div>
														
													</div>
   
											</td>
				                            
				                        </tr>
				                        <tr>
				                        	<td>03</td>
				                           
				                            <td>
											  <div class="input-field col m12">
														<label for="Fee_Break3">Fee Break</label> <input
															id="Fee_Break3" name="Fee_Break3" type="text" maxlength="50"
															class="textOnly stu_require"
															data-error="Please enter your Fee_Break3">
														<div class="error errorTxt8"></div>
														
													</div>
													
										
										
											</td>
				                           
				                            <td>
											<div class="input-field col m12">
														<label for="amount3">Amount</label> <input
															id="amount3" name="amount3" type="text" maxlength="50"
															class="textOnly stu_require"
															data-error="Please enter your amount3">
														<div class="error errorTxt9"></div>
														
													</div>
   
											</td>
				                            
				                        </tr>
										
										 <tr>
				                        	<td>04</td>
				                           
				                            <td>
											 
										TOTAL
											</td>
				                           
				                            <td>
											<div class="input-field col m12">
														<label for="amount4">Total Amount</label> <input
															id="amount4" name="amount4" type="text" maxlength="50"
															class="textOnly stu_require"
															data-error="Please enter your amount4" onblur="tot()" onclick="tot()">
														<div class="error errorTxt10"></div>
														
													</div>
   
											</td>
				                            
				                        </tr>
				                        
				                    </tbody>
				                  </table>
								  
				                </div>
				              </div>
				            </div>  
				            <br>
				            <div class="divider"></div> 
				            <!--DataTables example Row grouping-->
                    </div>

                
              </div>
            </div>
          </div>
		  
		  
		  	<div class="row ">
	<div class="input-field col s6">
                    <label for="SClass1"></label>
										<select class="browser-default stu_require" id="SClass" name="SClass" data-error="Please select your Class">
											<option value="NIL">Please select your Class</option>
											<option value="1st-Std">1st-Std</option>
											<option value="2nd-Std">2nd-Std</option>
											<option value="3rd-Std">3rd-Std</option>
											<option value="4th-Std">4th-Std</option>
											<option value="5th-Std">5th-Std</option>
											<option value="6th-Std">6th-Std</option>
											<option value="7th-Std">7th-Std</option>
											<option value="8th-Std">8th-Std</option>
										</select>
										<div class="error errorTxt7"></div>
										
                  </div>
                
             
               
                  <div class="input-field col s6">
                    <label for="Section1"></label>
										<select class="browser-default stu_require" id="Section" name="Section" data-error="Please select your Section">
											<option value="NIL">Please select your Section</option>
											<option value="A-Section">A-Section</option>
											<option value="B-Section">B-Section</option>
											<option value="C-Section">C-Section</option>
											<option value="D-Section">D-Section</option>
											<option value="E-Section">E-Section</option>
											<option value="F-Section">F-Section</option>
											<option value="G-Section">G-Section</option>
											<option value="H-Section">H-Section</option>
										</select>
										<div class="error errorTxt8"></div>
						 </div>				
	</div>
		  

					 

 
 <div class="input-field col s12">
		<label for="datepicker">Due Date</label> <input
		id="duedate" name="duedate" type="date" maxlength="50"
		class="datepicker"
		data-error="Please enter your date">
		<div class="error errorTxt9"></div>												
		</div>
													

<br>
	  
<div class="col s12 m8 l9 center">
 <a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="echallanInsert()" name="echallan" id="echallan">Save</a>
 <!-- <a class="waves-effect waves-light  btn">Save</a> -->
  <!-- <a class="waves-effect waves-light  btn"></i>Send</a> -->
  <!-- <a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="echallanUpdate()" name="echallanup" id="echallanup">Send</a> -->
  <a class="waves-effect waves-light  btn"></i>Cancel</a>
</div>

</form>

        </div>
		
		
 

          <!-- Floating Action Button -->
           
            <!-- Floating Action Button -->
        </div>
		  
		  </div>
		  </div>
        <!--end container-->
</div>
      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START RIGHT SIDEBAR NAV-->
      <aside id="right-sidebar-nav">
        <ul id="chat-out" class="side-nav rightside-navigation">
            <li class="li-hover">
            <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
            <div id="right-search" class="row">
                <form class="col s12">
                    <div class="input-field">
                        <i class="mdi-action-search prefix"></i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Search</label>
                    </div>
                </form>
            </div>
            </li>
            <li class="li-hover">
                <ul class="chat-collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                    <div class="collapsible-body recent-activity">
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">just now</a>
                                <p>Jim Doe Purchased new equipments for zonal office.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Yesterday</a>
                                <p>Your Next flight for USA will be on 15th August 2015.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Last Week</a>
                                <p>Jessy Jay open a new store at S.G Road.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                    <div class="collapsible-body sales-repoart">
                        <div class="sales-repoart-list  chat-out-list row">
                            <div class="col s8">Target Salse</div>
                            <div class="col s4"><span id="sales-line-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Payment Due</div>
                            <div class="col s4"><span id="sales-bar-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Delivery</div>
                            <div class="col s4"><span id="sales-line-2"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Progress</div>
                            <div class="col s4"><span id="sales-bar-2"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                    <div class="collapsible-body favorite-associates">
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Eileen Sideways</p>
                                <p class="place">Los Angeles, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Zaham Sindil</p>
                                <p class="place">San Francisco, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Renov Leongal</p>
                                <p class="place">Cebu City, Philippines</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Weno Carasbong</p>
                                <p>Tokyo, Japan</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Nusja Nawancali</p>
                                <p class="place">Bangkok, Thailand</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </li>
        </ul>
      </aside>
      <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->
  <div id="modal4" class="modal modal-fixed-footer green white-text stu_list_pop_box">
            <div class="modal-content">
            <div id="bordered-table">
            
              <h4 class="header left">Student Name</h4>
              <h4 class="header right">Academic Year</h4>
              <hr style="clear:both;"/>
              
              <div class="row">
              
                <div class="col s12 m4 l3">
                  <img title="Avatar" src="../../resources/images/avatar.jpg">
                </div>
                
                <div class="col s12 m8 l9">
                  <table class="bordered">
                        <th class="stu_details center" data-field="id">Student Details</th>
                        <tr>
                        <td>Class:</td>
                      </tr>
                      <tr>
                        <td>Roll Number:</td>
                      </tr>
                </table>
                </div>
                </div>
                      
                      <div class="col s12 m4 l3">
                  <table class="bordered">
                        <tr>
                        <td>Birth Date:</td>
                      </tr>
                      <tr>
                        <td>Gender:</td>
                      </tr>
                      <tr>
                        <td>Nationality:</td>
                      </tr>
                      <tr>
                        <td>Religion:</td>
                      </tr>
                      <tr>
                        <td>Community:</td>
                      </tr>
                      <tr>
                        <td>Place of Birth:</td>
                      </tr>
                      <tr>
                        <td>Mother Tongue:</td>
                      </tr>
                      <tr>
                        <td>Date of Joining:</td>
                      </tr>
                      <tr>
                        <td>Permanent Address:</td>
                      <tr>
                        <td>Communication Address:</td>
                      </tr>
                      <tr>
                        <td>Phone Number:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Previous Schools</th>
                      <tr>
                        <td>School Name:</td>
                      </tr>
                      <tr>
                        <td>Years:</td>
                      </tr>
                      <tr>
                        <td>Standard:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Parents Details</th>
                      <tr>
                        <td>Father Name:</td>
                      </tr>
                      <tr>
                        <td>Mother Name:</td>
                      </tr>
                      <tr>
                        <td>Father Occupation:</td>
                      </tr>
                      <tr>
                        <td>Father Office Address:</td>
                      <tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                        <td>Mother Occupation:</td>
                      </tr>
                      <tr>
                        <td>Mother office Address</td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Guardian Details</th>
                      <tr>
                        <td>Guardian Name:</td>
                      </tr>
                      <tr>
                        <td>Guardian Relation:</td>
                      </tr>
                      <tr>
                        <td>Guardian Address:</td>
                      <tr>
                        <td>Guardian Occupation:</td>
                      </tr>
                      <tr>
                        <td>Guardian Office address:</td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                      </tr>
                      <th class="stu_details center" data-field="id">Medical Details</th>
                      <tr>
                        <td>Blood Group:</td>
                      </tr>
                      <tr>
                        <td>Height:</td>
                      </tr>
                      <tr>
                        <td>Weight:</td>
                      </tr>
                      <tr>
                        <td>Allergy:</td>
                      </tr>
                      <tr>
                        <td>Diseases:</td>
                      </tr>
                      <tr>
                        <td>Special Child:</td>
                      </tr>
                      <tr>
                        <td>Physically Challenged:</td>
                      </tr>
                      <tr>
                        <td>Medical Condition:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Transport Detials</th>
                      <tr>
                        <td>Transport Name:</td>
                      </tr>
                      <tr>
                        <td>Boarding Point:</td>
                        </tr>
                        <th class="stu_details center" data-field="id">Hostel Detials</th>
                      <tr>
                        <td>Hostel Name:</td>
                      </tr>
                      <tr>
                        <td>Room Number:</td>
                        </tr>
                  </table>
                </div>
            </div>
            </div>
            </div>
            </div>


  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright Â© 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
    <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    
    <script>
    referenceValue();
    function referenceValue()
	{
		//alert("ref. value enter 1");
		 var dt=new Date();
		 var d1=dt.getDate();
		 var d2=dt.getMonth();
		 var d3=dt.getFullYear();
		 var dd3=d3+1;
		 var d4=d3+"-0"+d2+"-"+d1;
		 var dd4=d3+"-"+dd3
		 $('#Academic_Year').val(dd4);
		 
		/*  $('#join').val(d4);
		$.ajax({
				  type: "GET",
				  url: ctxPath+'/app/Echallan/getChallanId.do?',
				  dataType: 'json',
				}).done(function( responseJson )
						{
					alert("value  "+responseJson.challan_id);
					
					$('#challan_id').val(responseJson.challan_id);
					//$('#student_id2').val(responseJson.challan_id);
				}); */
				
		 $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/student/getStuId.do?',
			  dataType: 'json',
			}).done(function( responseJson ){	
				alert("value  "+responseJson.student_id);
				$('#challan_id').val(responseJson.student_id);
				//$('#student_id2').val(responseJson.student_id);
			});
				
	}
    
    </script>
    
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });

        });
		
		var date = new Date();

var day = date.getDate();
var month = date.getMonth() + 1;
var year = date.getFullYear();

if (month < 10) month = "0" + month;
if (day < 10) day = "0" + day;

var today = year + "-" + month + "-" + day;


document.getElementById('theDate').value = today;




function echallanInsert()
	{
		// alert("parent Insert entered111111111111111");
		 var dt=new Date();
		 var challan_id=$("#challan_id").val(); //alert(challan_id);
		 var theDate=$("#theDate").val();//alert(theDate);
		 var fee_name=$("#fee_name").val();//alert(fee_name);
		 var Academic_Year=$("#Academic_Year").val();//alert(Academic_Year);
		 var Fee_Break1=$("#Fee_Break1").val(); //alert(Fee_Break1);
		 var amount1=$("#amount1").val();//alert(amount1);
		 var Fee_Break2=$("#Fee_Break2").val();//alert(Fee_Break2);
		 var amount2=$("#amount2").val();//alert(amount2);
		 var Fee_Break3=$("#Fee_Break3").val();//alert(Fee_Break3);
		 var amount3=$("#amount3").val();//alert(amount3);
		 var amount4=$("#amount4").val();//alert(amount4);
		 var SClass=$("#SClass").val();//alert(SClass);
		 var Section=$("#Section").val();//alert(Section);
		 var duedate=$("#duedate").val();//alert(duedate);
		 var loguser1=$("#loguser1").val();//alert(loguser1);
		 
		 
		 
		 $.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/Echallan/insertEchallan.do?',
	        data: {"challan_id":challan_id,
					"theDate":theDate,
					"fee_name":fee_name,
					"acc_year":Academic_Year,
					"Fee_Break1":Fee_Break1,
					"amount1":amount1,
					"Fee_Break2":Fee_Break2,
					"amount2":amount2,
					"Fee_Break3":Fee_Break3,
					"amount3":amount3,
					"total":amount4,
					"SClass":SClass,
					"Section":Section,
					"datepicker":duedate,					
					"isActive":"Y",
					"Inserted_By":loguser1,
					"Inserted_Date":dt, 
					"Updated_By":loguser1,
					"Updated_Date":dt,
					"sendMessage":"N" 
	        },
	        success: function(data) 
	        {
	       	 if(data=='success')
	       	 {
	       		Materialize.toast("Success",4000);	
	       		alert(data);
	       		showfeedraft();
	       	 }
	       	 else 
	       	 {
	       		 Materialize.toast(data,4000);	 
	       	 }  
	        }
		 });
		 
	}
	
	 function showfeedraft()
	{
		var sclass=$("#SClass").val();
		//var sec=$('#Section').val();
		
		//alert("Fees Draft List entered111111111111111  value 1  "+sclass);
		   
		   document.getElementById('slick-login').action = ctxPath+'/app/Echallan/getFeeDraftpage?&obj1='+sclass;
		   document.getElementById('slick-login').submit();
	} 

function tot()
{
	var amt1=parseInt($("#amount1").val());
	var amt2=parseInt($("#amount2").val());
	var amt3=parseInt($("#amount3").val());
	
	var tot=amt1+amt2+amt3;
	$("#amount4").val(tot);
	
	
}
		

    </script>
    
    
    
    
    
    
    
    
    
    
    
    
</body>

</html>