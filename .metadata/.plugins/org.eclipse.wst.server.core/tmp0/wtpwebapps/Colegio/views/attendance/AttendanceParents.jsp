<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Messages</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
<%
                        	String sid=(String)request.getAttribute("parid");
                    		System.out.println("Value passssssssssssssssssssssss 2222222222  "+sid);
                        
                        %>

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                          <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"  data-activates="profile-dropdown"><%= sid %><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Parents</p>
                    </div>
                </div>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/markParList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Marks</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/attendanceParList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Attendance</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/examParScheList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Exam Schedule</a>
                </li>
                
                 <li class="bold"><a href='<%=request.getContextPath()%>/app/student/feesParList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Fees Details</a>
                </li>
              
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a  class="waves-effect waves-cyan"><i class="mdi-device-now-widgets"></i> Library</a>
                        </li>
                        
                        <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-action-shopping-cart"></i> Notice Board</a>
                        </li>
                        
  </form>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
                    <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
               <center> <h5 class="breadcrumbs-title">Parent Login- Students Attendance</h5> </center>
               </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

        <!--start container-->
			             
       						                               		   		  
                        <div class="container">
                        
						<div class="row">	
						<div class="input-field col m1">
			<label for="stuid">Student Id</label>
				</div>
						
						 <div class="input-field col m5">				
			<!-- <input id="stuid" name="stuid" type="text" size="45" tabindex="5" disabled  /> -->
			<select class="browser-default stu_require" id="stuid" name="stuid" data-error="Please select your Students" >
											<option value="NIL">Please select your Students ID</option>
										</select>
</div>	
</div>	
			

			<div class="row">			
			<div class="input-field col m1">
			<label for="startdate">Start Date : </label>
				</div>		
          
             <div class="input-field col m5">		  
			<input
				id="start_Date" name="Due_Date" type="text"
				maxlength="10" class="datepicker"
				data-error="Please pick your Date">
				<input id="pid" name="pid" type="hidden" size="45" tabindex="5" disabled value=<%= sid %> />
             </div>	
					
                          <div class="input-field col m1">
			 <label for="enddate">End Date: </label>
			 </div>	
				

             <div class="input-field col m5">				
			<input
				id="end_Date" name="Due_Date" type="text"
				maxlength="10" class="datepicker"
				data-error="Please pick your Date">

										
			</div>									
					

            					
			</div>
			
			
	<h5 class="breadcrumbs-title">Type</h5>

    <div class="row"> 
<div class="col m4  "> 
<div class="input-field col m4" > 
<input type="radio" name="attendance" value="MORNING" id="test1"><label for="test1"> MORNING</label><br>
 
</div> 
</div> 

<div class="col m4"> 
<div class="input-field col m4" > 
<input type="radio" name="attendance" value="AFTERNOON" id="test2"><label for="test2"> AFTERNOON</label><br>
</div> 
</div> 

<div class="col m4"> 
<div class="input-field col m4" > 
<!-- <a href="javascript:void(0)" class="btn waves-effect waves-light right">Search</a> -->
<a class="waves-effect waves-light  btn" name="goto"  onClick="showAttendance()" value="Validate">Search</a>
</div> 
</div> 
 </div>
	
	</br>
	

</br>		
<div class="row">			
			<div class="input-field col m1">
			<label for="stuid">Student Id : </label>
				</div>

             <div class="input-field col m5">				
			<input id="stuid1" name="stuid1" type="text" size="45" tabindex="5" disabled  />
</div>	
			
			
			              <div class="input-field col m1">
			 <label for="att_type">Attendance Type: </label>
				</div>		

              <div class="input-field col m5">				
			<input id="att_type" name="att_type" type="text" size="45" tabindex="5" disabled  />

				</div>						
											
										
			</div>


						
</br>									


						
			<div class="row">			
			<div class="input-field col m1">
			<label for="Student_Name">Student Name : </label>
				</div>

             <div class="input-field col m5">				
			<input id="Student_Name" name="Student_Name" type="text" size="45" tabindex="5" disabled  />
</div>	
			
			
			              <div class="input-field col m1">
			 <label for="from_date">From Date: </label>
				</div>		

              <div class="input-field col m5">				
			<input id="from_date" name="from_date" type="text" size="45" tabindex="5" disabled  />

				</div>						
											
										
			</div>
			
			
			<div class="row">			
			<div class="input-field col m1">
			<label for="Sclass">Class: </label>
				</div>

             <div class="input-field col m5">				
			<input id="Sclass" name="Sclass" type="text" size="45" tabindex="5" disabled  />
</div>	
			
			
			              <div class="input-field col m1">
			 <label for="to_date">To Date: </label>
				</div>		

              <div class="input-field col m5">				
			<input id="to_date" name="to_date" type="text" size="45" tabindex="5" disabled  />

				</div>						
											
										
			</div>
			
			 
			 <div class="row">
			<div class="input-field col m1">
			<label for="Section">Section: </label>
             </div>		
			 
             <div class="input-field col m5">
            			
			<input id="Section" name="Section" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>
			 
			 
						</div>
	</br></br>
	 

  <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
                  <div class="col s12">
                   
                  </div>
				  
                 <div class="col s12"> 
<div id="stu_list_all" class="col s12  cyan lighten-4"> 
 
		 <div class="container">
          <div class="section">	
		  
<div class="row"> 
<!--DataTables example--> 
<div id="#student-datatables_1"> 
<!--<h4 class="header">DataTables example</h4>--> 
<div class="row"> 
<div class="col s12 m12 l12"> 
<table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0"> 
<thead> 
<tr> 
<th>S No</th>
<th>Date</th>
<th>Status</th> 



</tr> 
</thead> 


<tbody> 

</tbody> 
</table> 
</div> 
</div> 
</div> 
<br> 
<div class="divider"></div> 
          
</div>					
	
</div>
</div>	
	</div>					

 
	
	</div>
</div>					


 <div class="row">
			<!-- <div class="input-field col m2">
			<label for="Totalworkday">Total Working Days: </label>
             </div>		
			 
             <div class="input-field col m2">
            			
			<input id="Totalworkday" name="Totalworkday" type="text" size="45" tabindex="5" disabled  />
		</div>		 -->		
	</div>
	
	<div class="card-panel col m6">
	 <div class="row">
			<div class="input-field col m4">
			<label for="Number_of_presentday">No.Of Present Day: </label>
             </div>		
			 
             <div class="input-field col m4">
            			
			<input id="Number_of_presentday" name="Number_of_presentday" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>
	
	 <div class="row">
			<div class="input-field col m4">
			<label for="Number_of_absentday">No.Of Absent Day: </label>
             </div>		
			 
             <div class="input-field col m4">
            			
			<input id="Number_of_absentday" name="Number_of_absentday" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>
	
	 <div class="row">
			<div class="input-field col m4">
			<label for="Number_of_onduty">No.Of OnDuty: </label>
             </div>		
			 
             <div class="input-field col m4">
            			
			<input id="Number_of_onduty" name="Number_of_onduty" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>
	
	 <div class="row">
			<div class="input-field col m4">
			<label for="Number_of_permission">No.Of Permission: </label>
             </div>		
			 
             <div class="input-field col m4">
            			
			<input id="Number_of_permission" name="Number_of_permission" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>
	</div>
</section>
</div>

</div>
<!--start container-->
				     
       
			  
  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
     <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

		
		
	$('.datepicker').pickadate({ 
selectMonths: true, // Creates a dropdown to control month 
selectYears: 15 // Creates a dropdown of 15 years to control year 
}); 


		
		if ($("#test21:checked").length > 0) {
    $("#test24, #test27, #test30, #test33").prop('checked', true);
} else if ($("#DRequest-1:checked").length > 0) {
    $("#DRequest-2, #DRequest-3, #DRequest-4").prop('checked', true);
}

        }); 
		
    </script>
	
	<script>
	retrieve();



	function retrieve()
	  {
		var pid=$('#pid').val();
	  	$.ajax({
	  			  type: "GET",
				  url: ctxPath+'/app/student/getParentdataAll.do?',  					  
	  			  data: {"parent_id":pid},
	  			  dataType: 'json',
	  			}).done(function( responseJson )
	  					{	
	   						loadDataIntoLoop(responseJson);
	  				});
	  }
	  
	function loadDataIntoLoop(responseJson)
	{
		var tblHtml = "";
		var stuid;
		 var sel = $("#stuid").empty();
		 $.each(responseJson.studentServiceVOList, function(index, val)
		 {
			stuid=val.student_id;
			sel.append('<option value="' + stuid + '">' + stuid + '</option>');
		 }); 
		
	}
	
	function showAttendance()
	{	
		
		$('#stuid1').val("");
  		$('#att_type').val("");
  		$('#Student_Name').val("");
  		$('#from_date').val("");
  		$('#Sclass').val("");
  		$('#to_date').val("");
  		$('#Section').val("");
  		
  		$('#Number_of_presentday').val("");
  	  	$('#Number_of_absentday').val("");
  	  	$('#Number_of_onduty').val("");
  	  	$('#Number_of_permission').val("");
  		
		
		var fdate=$('#start_Date').val();
		var tdate=$('#end_Date').val();
		
		var ddate = new Date(fdate); 
		var dtate = new Date(tdate);
		
		var day = ddate.getDate(); 
		var month = ddate.getMonth() + 1; 
		var year = ddate.getFullYear(); 

		if (month < 10) month = "0" + month; 
		if (day < 10) day = "0" + day; 

		var frdate = year + "-" + month + "-" + day; 


		var day1 = dtate.getDate(); 
		var month1 = dtate.getMonth() + 1; 
		var year1 = dtate.getFullYear(); 

		if (month1 < 10) month1 = "0" + month1; 
		if (day1 < 10) day1 = "0" + day1; 

		var todate = year1 + "-" + month1 + "-" + day1; 
		

		
		
		
		 var status = document.getElementsByName("attendance");
		 var sta;
		 
		    if (status[0].checked == true) {
		   	 sta=status[0].value;
		    } else if(status[1].checked == true) {
		   	 sta=status[1].value;
		    }
		    var sid=$('#stuid').val();
		//alert("fdate  "+frdate+" tdate  "+todate+" sta  "+sta+"  studentid  "+sid);
		
		
		 $.ajax({
 			  type: "GET",
 			  url: ctxPath+'/app/student/getStuAttendance.do?',
 			  data: {"fdate":frdate,"tdate":todate,"atime":sta,"student_id":sid},
 			  dataType: 'json',
			}).done(function( responseJson )
					{	
						//alert("before "+responseJson.adate);
					    loadDataIntoTable(responseJson);
					    //alert("after "+responseJson.adate);
		
				});	
	}
	//attendanceServiceVOList
	function loadDataIntoTable(responseJson)
  {
	//alert("enter table ");
	var tblHtml = "";
	var fdate=$('#start_Date').val();
	var tdate=$('#end_Date').val();
	
	var ddate = new Date(fdate); 
	var dtate = new Date(tdate);
	var month1=ddate.getMonth() + 1; 
	var onduty=0,present=0,absent=0,permission=0;
	
	
  	 $.each(responseJson.attendanceServiceVOList, function(index, val)
  	{
  		//alert("enter for each ");
  		index++;
  		var adate=val.adate;
  		var sid=val.sid;
  		var attType=val.att_time;
  		var fname=val.fname;
  		var Sclass=val.aclass;
  		var sec=val.asection;
  		var att_status=val.att_status;
  		
  		
  	
  		$('#stuid1').val(sid);
  		$('#att_type').val(attType);
  		$('#Student_Name').val(fname);
  		$('#from_date').val(fdate);
  		$('#Sclass').val(Sclass);
  		$('#to_date').val(tdate);
  		$('#Section').val(sec);
  		
  		if(att_status=="Present")
  			{
  				present++;
  			}
  		if(att_status=="Absent")
			{
				absent++;
			}
  		if(att_status=="onDuty")
			{
				onduty++;
			}
  		if(att_status=="Permission")
			{
				permission++;
			}
		
		

  		 tblHtml += '<tr><td style="text-align: left">'+(index++)+'</a></td><td style="text-align: left">'+adate+'</td><td style="text-align: left">'+att_status+'</td></tr>';
	     
 	   /*  '<tr><td style="text-align: left">'+(index++)+'</a></td><td style="text-align: left">English</td><td style="text-align: left">'+eng+'</td></tr>'+
 	   '<tr><td style="text-align: left">'+(index++)+'</a></td><td style="text-align: left">Maths</td><td style="text-align: left">'+maths+'</td></tr>'+
 	  '<tr><td style="text-align: left">'+(index++)+'</a></td><td style="text-align: left">Science</td><td style="text-align: left">'+sci+'</td></tr>'+
 	 '<tr><td style="text-align: left">'+(index++)+'</a></td><td style="text-align: left">Social</td><td style="text-align: left">'+social+'</td></tr>'; */
  		
  	            
  	 });   
	
  	// alert("present  "+present+" absent  "+absent+" onduty  "+onduty+" permission  "+permission);
  	$('#Number_of_presentday').val(present);
  	$('#Number_of_absentday').val(absent);
  	$('#Number_of_onduty').val(onduty);
  	$('#Number_of_permission').val(permission);
  	 
  	 $("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
  
  }

	
  
   </script>


	
									
			
</body>

</html>
													