<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Messages</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
<%
                        	String sid=(String)request.getAttribute("stuid");
                    		System.out.println("Value passssssssssssssssssssssss 2222222222  "+sid);
                        
                        %>

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                       // String sid=(String)request.getAttribute("admid");
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                     <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=admission/Admission' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_Main' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/ParentsView' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=calender/app-calendar' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Activity_List' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff List</a>
                                    </li>
                                      <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachinglist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachingview' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach Staff</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <%-- <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examlist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Marks </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=MarkSheet/Marks_ClassList' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class wise Marks</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Finance</a>
                            <div class="collapsible-body">
                                <ul>
                                   <%--  <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_EChallan'>E-Challan</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeeDraft'>Fee Draft</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeesList'>Fee List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/FeeAllotMainpage'>Mainpage</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                 	    
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> News Feed </a>
                			<div class="collapsible-body">
                                <ul>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/events_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/news_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                       
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=SMS/SmsOrEmail' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> SMS/Email</a>
                        </li>
                        
  </form>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
                    <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
               <center> <h5 class="breadcrumbs-title">Admin Login-Students Attendance</h5> </center>
               </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

        <!--start container-->
			             
       						                               		   		  
                        <div class="container">



					<div class="row">
						<div class="input-field col m1">
							<label for="startdate">Date : </label>
						</div>

						<div class="input-field col m5">
							<input id="start_Date" name="Due_Date" type="text" maxlength="10"
								class="datepicker" data-error="Please pick your Date">
							<%-- <input id="sid" name="sid" type="hidden" size="45" tabindex="5" disabled value=<%= sid %> /> --%>
						</div>

						<!-- <div class="input-field col m1">
							<label for="enddate">End Date: </label>
						</div>


						<div class="input-field col m5">
							<input id="end_Date" name="Due_Date" type="text" maxlength="10"
								class="datepicker" data-error="Please pick your Date">
						</div> -->
					</div>

					
					<div class="row">
						<div class="input-field col m1">
							<label for="stuid">Class : </label> 
						</div>

						<div class="input-field col m5">
							 <label for="SClass1"></label>
										<select class="browser-default stu_require" id="sclass" name="sclass" data-error="Please select your Class">
											<option value="NIL">Please select your Class</option>
											<option value="1st-Std">1st-Std</option>
											<option value="2nd-Std">2nd-Std</option>
											<option value="3rd-Std">3rd-Std</option>
											<option value="4th-Std">4th-Std</option>
											<option value="5th-Std">5th-Std</option>
											<option value="6th-Std">6th-Std</option>
											<option value="7th-Std">7th-Std</option>
											<option value="8th-Std">8th-Std</option>
										</select>
						</div>

						<div class="input-field col m1">
							<label for="enddate">Section : </label>
						</div>


						<div class="input-field col m5">
							<select class="browser-default stu_require" id="sec" name="sec" data-error="Please select your Section" onChange="showList()">
											<option value="NIL">Please select your Section</option>
											<option value="A-Section">A-Section</option>
											<option value="B-Section">B-Section</option>
											<option value="C-Section">C-Section</option>
											<option value="D-Section">D-Section</option>
											<option value="E-Section">E-Section</option>
											<option value="F-Section">F-Section</option>
											<option value="G-Section">G-Section</option>
											<option value="H-Section">H-Section</option>
										</select>
						</div>
					</div>
					
					
					

<div class="row m6">
					<div class="input-field col m3"> 
						<a class="waves-effect waves-light  btn" name="goto" onClick="showAttendance()" value="Validate">Search</a> 
					</div>
</div>					 
 


	</br></br>
	 

  <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
                  <div class="col s12">
                   
                  </div>
				  
                 <div class="col s12"> 
<div id="stu_list_all" class="col s12  cyan lighten-4"> 
 
		 <div class="container">
          <div class="section">	
		  
<div class="row"> 
<!--DataTables example--> 
<div id="#student-datatables_1"> 
<!--<h4 class="header">DataTables example</h4>--> 
<div class="row"> 
<div class="col s12 m12 l12"> 
<table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0"> 
<thead> 
 <tr> 
<th>S No</th>
<th>Student Name</th>

</tr>  
</thead> 


<tbody> 

</tbody> 
</table> 
</div> 
</div> 
</div> 
<br> 
<div class="divider"></div> 
          
</div>					
	
</div>
</div>	
	</div>					

 
	
	</div>
</div>					


 <div class="row">
			 <div class="input-field col m2">
			<label for="Totalworkday">Total Students : </label>
             </div>		
			 
             <div class="input-field col m2">
            			
			<input id="Totalstu" name="Totalstu" type="text" size="45" tabindex="5" readonly  />
		</div>		 		
	</div>
	
	<div class="card-panel col m6">
	 <div class="row">
			<div class="input-field col m4">
			<label for="Number_of_presentday">No.Of Present Day: </label>
             </div>		
			 
             <div class="input-field col m4">
            			
			<input id="Number_of_presentday" name="Number_of_presentday" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>
	
	 <div class="row">
			<div class="input-field col m4">
			<label for="Number_of_absentday">No.Of Absent Day: </label>
             </div>		
			 
             <div class="input-field col m4">
            			
			<input id="Number_of_absentday" name="Number_of_absentday" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>
	
	 <div class="row">
			<div class="input-field col m4">
			<label for="Number_of_onduty">No.Of OnDuty: </label>
             </div>		
			 
             <div class="input-field col m4">
            			
			<input id="Number_of_onduty" name="Number_of_onduty" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>
	
	 <div class="row">
			<div class="input-field col m4">
			<label for="Number_of_permission">No.Of Permission: </label>
             </div>		
			 
             <div class="input-field col m4">
            			
			<input id="Number_of_permission" name="Number_of_permission" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>
	</div>
</section>
</div>

</div>
<!--start container-->
				     
       
			  
  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
     <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

		
		
	$('.datepicker').pickadate({ 
selectMonths: true, // Creates a dropdown to control month 
selectYears: 15 // Creates a dropdown of 15 years to control year 
}); 


		
		if ($("#test21:checked").length > 0) {
    $("#test24, #test27, #test30, #test33").prop('checked', true);
} else if ($("#DRequest-1:checked").length > 0) {
    $("#DRequest-2, #DRequest-3, #DRequest-4").prop('checked', true);
}

        }); 
		
    </script>
	
	<script>
	
	
	function showAttendance()
	{	
		var fdate=$('#start_Date').val();
		var sclass=$('#sclass').val();
		var sec=$('#sec').val();
		
		var ddate = new Date(fdate); 
		
		var day = ddate.getDate(); 
		var month = ddate.getMonth() + 1; 
		var year = ddate.getFullYear(); 

		if (month < 10) month = "0" + month; 
		if (day < 10) day = "0" + day; 

		var frdate = year + "-" + month + "-" + day; 
		
		var frdate1 = day + "-" + month + "-" + year; 

		tblHtml2='<th>S.No</th> <th> Student Name </th> <th>'+frdate1+'</th>';
		 $("#stu-db-data-table_1 thead").html(tblHtml2);
		
		$.ajax({
			  type: "GET",
			  url: ctxPath+'/app/student/totalStudent.do?',
			  data: {"aclass":sclass,"asec":sec},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						//alert("before total "+responseJson.sclass);
						loadTotalSTU(responseJson);
					    //alert("after total "+responseJson.section);
		
				});
		
		
		
/* 		 $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/student/getStuAttendanceByDate.do?',
			  data: {"fdate":frdate,"aclass":sclass,"asec":sec},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						loadDataIntoHead(responseJson);
				});	 
 */		
		 $.ajax({
 			  type: "GET",
 			  url: ctxPath+'/app/student/getStuAttendanceAll.do?',
 			  //data: {"fdate":frdate,"tdate":todate,"aclass":sclass,"asec":sec},
 			  data: {"fdate":frdate,"aclass":sclass,"asec":sec},
 			  dataType: 'json',
			}).done(function( responseJson )
					{	
						//alert("before "+responseJson.adate);
						//loadDataIntoHead(responseJson);
					    loadDataIntoTable(responseJson);
					    //alert("after "+responseJson.adate);
		
				});	
	}
	
	
	function loadTotalSTU(responseJson)
	  {
		//alert("total enter  ");
		var tot=0;
		 $.each(responseJson.studentServiceVOList, function(index, val)
				  	{
			 			tot++;
				  	});
		 //Totalstu
		 //alert("Total stu  "+tot);
		 $('#Totalstu').val(tot);
	  }
	
	/* function loadDataIntoHead(responseJson)
	  {
		var tblHtml1 = "";
		var tblHtml2 = "";
		tblHtml2='<th>S.No</th> <th> Student Name </th>';
		 $.each(responseJson.attendanceServiceVOList, function(index, val)
				  	{
			 			var dt=val.adate;
			 			tblHtml1 += '<th>'+val.adate+'</th> ';
				  	});
		 tblHtml2+=tblHtml1;
		 $("#stu-db-data-table_1 thead").html(tblHtml2);
		
	  } */
	
	function loadDataIntoTable(responseJson)
  {
	var tblHtml = "";
	var tblHtml2 = "";
	var tblHtml3 = "";
	var fdate=$('#start_Date').val();
	var tdate=$('#end_Date').val();
	
	var ddate = new Date(fdate); 
	var dtate = new Date(tdate);
	var month1=ddate.getMonth() + 1; 
	var onduty=0,present=0,absent=0,permission=0;
	
	var day = ddate.getDate(); 
	var month = ddate.getMonth() + 1; 
	var year = ddate.getFullYear(); 

	if (month < 10) month = "0" + month; 
	if (day < 10) day = "0" + day; 

	var frdate = year + "-" + month + "-" + day; 

	$.each(responseJson.attendanceServiceVOList, function(index, val)
  	{
  		index++;
		var fname=val.fname;
		var lname=val.lname;
		var name=fname+lname;
		var att_status=val.att_status;
		var dt=val.adate;

		
		if(att_status=="Present")
			{
				present++;
			}
		if(att_status=="Absent")
		{
			absent++;
		}
		if(att_status=="onDuty")
		{
			onduty++;
		}
		if(att_status=="Permission")
		{
			permission++;
		}
		
		tblHtml += '<tr>'+
  						'<td style="text-align: left">'+(index++)+'</a></td>'+
  						 '<td style="text-align: left">'+name+'</td>'+ 
  					    '<td style="text-align: left">'+att_status+'</td></tr>';
 	   
  	            
  	 });   
  	 
  	 //alert("present  "+present+" absent  "+absent+" onduty  "+onduty+" permission  "+permission);
  	 $('#Number_of_presentday').val(present);
  	$('#Number_of_absentday').val(absent);
  	$('#Number_of_onduty').val(onduty);
  	$('#Number_of_permission').val(permission);
  	
  	
  	$("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10); 
  
  }

	
  
   </script>


	
									
			
</body>

</html>
													