<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Teachers Form</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
    <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<style>
	.input-field div.error, .input-field div.error2{
		position: relative;
		top: -1rem;
		left: 0rem;
		font-size: 0.8rem;
		color:#FF4081;
		-webkit-transform: translateY(0%);
		-ms-transform: translateY(0%);
		-o-transform: translateY(0%);
		transform: translateY(0%);
	}
	.input-field div.error2{
		top:-8px;
	}
	.input-field label.active{
		width:100%;
	}
	h4.header2.title{
		float:left;
		width:100%;
	}
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->
<%
//                        	String uname=(String)session.getAttribute("userName2");
                        
                        %>
  <!-- START HEADER -->
  <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Materialize</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- translation-button -->
                    <ul id="translation-dropdown" class="dropdown-content">
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
  </header>
  <!-- END HEADER -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START MAIN -->
  <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                           <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                     <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=admission/Admission' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_Main' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/ParentsView' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=calender/app-calendar' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Activity_List' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff List</a>
                                    </li>
                                      <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachinglist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachingview' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach Staff</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <%-- <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examlist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Marks </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=MarkSheet/Marks_ClassList' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class wise Marks</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Finance</a>
                            <div class="collapsible-body">
                                <ul>
                                   <%--  <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_EChallan'>E-Challan</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeeDraft'>Fee Draft</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeesList'>Fee List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/FeeAllotMainpage'>Mainpage</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                 	    
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> News Feed </a>
                			<div class="collapsible-body">
                                <ul>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/events_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/news_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                       
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=SMS/SmsOrEmail' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> SMS/Email</a>
                        </li>
            </aside>
      <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
			<div class="container">
				<div class="row">
				  <div class="col s12 m12 l12">
					<h5 class="breadcrumbs-title">Teachers</h5>
					<ol class="breadcrumbs">
						<li><a href="../../index.html">Dashboard</a></li>
						<li class="active">Teachers</li>
					</ol>
				  </div>
				</div>
			</div>
        </div>
        <!--breadcrumbs end-->
        <div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="row">
					<div class="col s12">
						<ul class="tabs tab-demo z-depth-1">
						  <li id="tch-tab1" class="tab col s3"><a class="active" href="#teacher-detail-tab">Details</a>
						  </li>
						  <li id="tch-tab2" class="tab col s3"><a href="#teacher-qualifiaction-tab">Qualification</a>
						  </li>
						 <!--   <li id="tch-tab3" class="tab col s3"><a href="#print-tab">Print</a>
						  </li> -->
						</ul>
					</div>
					<div class="col s12">
						<form class="teacherValidate" id="teacherValidate" name="teacherValidate" method="post">
							<div id="teacher-detail-tab" class="col s12">
								<div class="row">
									<h4 class="header2 title">Teacher Details</h4>
									<div class="input-field col s12">
										<label for="fname">First Name *</label>
										<input id="tid" name="tid" type="hidden" maxlength="50">
										<input id="loguser1" name="loguser1" type="hidden" maxlength="10" value="<%= sid %>">
										<input id="fname" name="fname" type="text" maxlength="50" class="textOnly tch_dt_require" data-error="Please enter your first name">
										<div class="error errorTxt1"></div>
									</div>

									<div class="input-field col s12">
										<label for="lname">Last Name *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="lname" name="lname" type="text" maxlength="50" class="textOnly tch_dt_require" data-error="Please enter your last name">
										<div class="error errorTxt2"></div>
									</div>
									
									<div class="input-field col s12">
										<label for="uname">Username *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="uname" name="uname" type="text" maxlength="50" class="tch_dt_require" data-error="Please enter your user name">
										<div class="error errorTxt3"></div>
									</div>
									<div class="input-field col s12">
										<label for="pass">Password *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="pass" name="pass" type="text" maxlength="50" class="tch_dt_require" data-error="Please enter your Password">
										<div class="error errorTxt3"></div>
									</div>
									
									
									
									<div class="col s12">
                  					<form action="#">
										<div class="file-field input-field">
											 <div class="btn">
											 <span>Photo *</span>
											 <input type="file">
											 </div>
											 <div class="file-path-wrapper">
											   <input class="file-path validate" type="text" id="photo" name="photo">
											 </div>
                    					</div>
                 					 </form>
               						 </div>

									<div id="dob_date" class="input-field col s12 dob_date">
										<label for="dob">Date of birth *</label>
										<input type="text" name="dob" id="dob" class="datepicker ad_date tch_dt_require" data-error="Please enter your date of birth">
										<div class="error errorTxt4"></div>
									</div>

									<div class="col s12">
										<label for="genter_select">Nationality *</label>
										<p>
											<input name="nationality" type="radio" id="national_indian" class="tch_dt_require" data-error="Please select your nationality" value="Indian" />
											<label for="national_indian">Indian</label>
										</p>
										<p>
											<input name="nationality" type="radio" id="national_other" class="tch_dt_require" data-error="Please select your nationality" value="Others" />
											<label for="national_other">Other</label>
										</p>
										<div class="input-field">
											<div class="error errorTxt5"></div>
										</div>
									</div>

									<div class="col s12">
										<label for="genter_select">Gender *</label>
										<p>
											<input name="cgender" type="radio" id="gender_male" class="tch_dt_require" data-error="Please select your gender" value="Male"/>
											<label for="gender_male">Male</label>
										</p>
										<p>
											<input name="cgender" type="radio" id="gender_female" class="tch_dt_require" data-error="Please select your gender" value="FeMale" />
											<label for="gender_female">Female</label>
										</p>
										<div class="input-field">
											<div class="error errorTxt6"></div>
										</div>
									</div>

									<h4 class="header2 title">Residential Address</h4>

									<div class="input-field col s12">
										<label for="address1">Address Line 1 *</label>
										<input id="address1" name="address1" type="text" class="tch_dt_require" maxlength="100" data-error="Please enter your address">
										<div class="error errorTxt7"></div>
									</div>

									<div class="input-field col s12">
										<label for="address2">Address Line 2</label>
										<input id="address2" name="address2" maxlength="100" type="text">
									</div>

									<div class="input-field col s12">
										<label for="city">City *</label>
										<input id="city" name="city" type="text" maxlength="50" class="textOnly tch_dt_require" data-error="Please enter your city">
										<div class="error errorTxt8"></div>
									</div>

									<div class="col s12">
										<label for="State">State *</label>
										<select class="browser-default tch_dt_require" id="state" name="state" data-error="Please select your state">
											<option value="0">Please select your state</option>
											<option value="Tamilnadu">Tamil nadu</option>
											<option value="Kerala">Kerala</option>
											<option value="Punjab">punjab</option>
										</select>
										<div class="input-field">
											<div class="error errorTxt9"></div>
										</div>
									</div>

									<div class="input-field col s12">
										<label for="pin">Pin *</label>
										<input id="pin" name="pin" maxlength="6" class="numbersOnly tch_dt_require" type="text" data-error="Please enter your pin">
										<div class="error errorTxt10"></div>
									</div>
									
									<div class="input-field col s12">
										<label for="mobile">Mobile *</label>
										<input id="mobile" name="mobile" maxlength="10" class="numbersOnly tch_dt_require" type="text" data-error="Please enter your mobile munber">
										<div class="error errorTxt11"></div>
									</div>

									<div class="input-field col s12">
										<label for="phone">Phone</label>
										<input id="phone" maxlength="10" name="phone" class="numbersOnly" type="text">
									</div>

									<div class="input-field col s12">
										<label for="cemail">Email *</label>
										<input id="cemail" type="text" name="cemail" class="email tch_dt_require" maxlength="100" data-error="Please enter your email">
										<div class="error errorTxt12"></div>
									</div>
									
									<div class="input-field col s12">
										<label for="pan">Pan Number *</label>
										<input id="pan" type="text" name="pan" class="email tch_dt_require" maxlength="10" data-error="Please enter your Pan Number">
										<div class="error errorTxt13"></div>
									</div>

									<div class="col s12">
										<button class="btn waves-effect waves-light right" id="Next1" type="button" name="action">Next
										<i class="mdi-content-send right"></i>
										</button>
									</div>
								</div>
							</div>	
							<div id="teacher-qualifiaction-tab" class="col s12">
								<div class="row">
									<div class="qualification_bx">
									<h4 class="header2 title">Qualification Details</h4>
										<div class="qua_bx qf_b1">
											<div class="input-field col s12">
												<label for="tch_school_name">Name Of School / College/ University</label>
												<input id="tch_school_name" name="tch_school_name" type="text" maxlength="50" class="textOnly tch_qft_require" data-error="Please enter the name of school, college, university">
												<div class="input-field">
													<div class="error"></div>
												</div>
											</div>

											<div class="input-field col s12">
												<label for="tch_course_name"> Course / HSC / SSLC/ UG / PG </label>                                                                                                                                                                                                                                                                                                                                                                                                                             
												<input id="tch_course_name" name="tch_course_name" type="text" maxlength="50" class="textOnly tch_qft_require" data-error="Please enter your course name">
												<div class="input-field">
													<div class="error"></div>
												</div>
											</div>
											
											<div class="input-field col s12">
												<label for="tch_per"> Percentange </label>                                                                                                                                                                                                                                                                                                                                                                                                                             
												<input id="tch_per" name="tch_per" type="text" maxlength="50" class="textOnly tch_qft_require" data-error="Please enter your percentage">
												<div class="input-field">
													<div class="error"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="add_btn col s12 l12">
										<label for="pre_school">Do you want to add</label>
										<a id="add_tch_dtl" class="btn-floating btn-large waves-effect waves-light green accent-3"><i class="mdi-content-add"></i></a>
									</div>
									<div class="add_btn col s12 l12">
										<label for="pre_school">Do you want to Subtract</label>
										<a id="sub_tch_dtl" class="btn-floating btn-large waves-effect waves-light green accent-3"><i class="mdi-content-add"></i></a>
									</div>

									<div class="experience_bx">
									<h4 class="header2 title">Experience detail</h4>
										<div class="exp_bx ep_b1">
											<div class="input-field col s12">
												<label for="tch_prev_emp"> Previous Employer </label>
												<input id="tch_prev_emp" name="tch_prev_emp" type="text" maxlength="50" class="textOnly tch_exp_require" data-error="Please enter your guardian name">
												<div class="input-field">
													<div class="error"></div>
												</div>
											</div>
											
											<div class="input-field col s12">
												<label for="tch_prev_emp_desg">Designation</label>
												<input id="tch_prev_emp_desg" name="tch_prev_emp_desg" type="text" maxlength="50" class="textOnly tch_exp_require" data-error="Please enter your occupation">
												<div class="input-field">
													<div class="error"></div>
												</div>
											</div>
											
											<div class="input-field col s12">
												<label for="tch_year_of_exp">Year Of Experience</label>
												<input id="tch_year_of_exp" name="tch_year_of_exp" type="text" maxlength="50" class="textOnly tch_exp_require" data-error="Please enter your designation">
												<div class="input-field">
													<div class="error"></div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="add_btn col s12 l12">
										<label for="pre_school">Do you want to add</label>
										<a id="add_exp_dtl" class="btn-floating btn-large waves-effect waves-light green accent-3"><i class="mdi-content-add"></i></a>
                                    </div>
									<div class="add_btn col s12 l12">
										<label for="pre_school">Do you want to Subract</label>
										<a id="sub_exp_dtl" class="btn-floating btn-large waves-effect waves-light green accent-3"><i class="mdi-content-add"></i></a>
									</div>
									
									<div class="input-field col s12">
										<div class="col s6">
											<button class="btn waves-effect waves-light left" id="Prev2" type="button" name="action">Prev
											<i class="mdi-content-send rotate left"></i>
											</button>
										</div>
										
										<div class="col s6">
											<!--  <button class="btn waves-effect waves-light right" id="Next2" type="button" name="action">Submit
											<i class="mdi-content-send right"></i>
											</button> -->
											<a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="teacherInsert()" name="teacher" id="teacher">Submit</a>
										</div>
									</div>
								</div>
							</div>
							
							<!-- <div id="print-tab" class="col s12">
								<div class="col s6">
									<button class="btn waves-effect waves-light right" id="submit" type="submit" value="submit">Submit

									</button>
								</div>
							</div>  -->
							
						</form>
					</div>
					</div>
				</div>
            </div>
		</div>
    </section>
      <!-- END CONTENT -->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->



  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://geekslabs.com/">Orectiq</a></span>
        </div>
    </div>
  </footer>
  <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism
    <script type="text/javascript" src="js/prism/prism.js"></script>-->
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
	<script type="text/javascript" src="../../js/teacher-detail-custom.js"></script>
	
	<script> var ctxPath = "<%=request.getContextPath() %>";</script>
		
	
	<script>
	referenceValue();
	function teacherInsert()
	{
		 //alert("Teacher Insert entered111111111111111");
		 var dt=new Date();
		 

		 var nat = document.getElementsByName("nationality");
		    if (nat[0].checked == true) {
		   	 nation=nat[0].value;
		    } else if(nat[1].checked == true) {
		   	 nation=nat[1].value;
		    }
		    
		    var gender = document.getElementsByName("cgender");
		    if (gender[0].checked == true) {
		   	 xender=gender[0].value;
		    } else if(gender[1].checked == true) {
		   	 xender=gender[1].value;
		    }
		 
		 //alert($("#tid").val()+","+$("#fname").val()+","+$("#lname").val()+","+$("#uname").val()+","+$("#pass").val());
		    
		 $.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/teacher/teacherInsert.do?',
	        data: {"tid":$("#tid").val(),"fname":$("#fname").val(),"lname":$("#lname").val(),"uname":$("#uname").val(),"pass":$("#pass").val(),
	        	   "photo":$("#photo").val(),"dob":$("#dob").val(),"nationality":nation,"cgender":xender,
	        	   "address1":$("#address1").val(),"address2":$("#address2").val(),"city":$("#city").val(),"state":$("#state").val(),
	        	   "pin":$("#pin").val(),"mobile":$("#mobile").val(),"phone":$("#phone").val(),"cemail":$("#cemail").val(),
	        	   "pan":$("#pan").val(),"tch_school_name":$("#tch_school_name").val(),"tch_course_name":$("#tch_course_name").val(),"tch_per":$("#tch_per").val(),
	        	   "tch_prev_emp":$("#tch_prev_emp").val(),"tch_prev_emp_desg":$("#tch_prev_emp_desg").val(),"tch_year_of_exp":$("#tch_year_of_exp").val(),
	        	   "isActive":'Y',"created_by":$("#loguser1").val(),"created_date":dt,"updated_by":$("#loguser1").val(),"updated_date":dt},
	        success: function(data) 
	        {
	       	 if(data=='success')
	       	 {
	       		Materialize.toast("Success",4000);	 
	       	 }
	       	 else 
	       	 {
	       		 Materialize.toast(data,4000);	 
	       	 }  
	        }
		 });
	}
	
	function referenceValue()
	{
		//alert("ref. value enter 1  "+ctxPath);
		//alert("value 1  "+responseJson.Tid);
		$.ajax({
				  type: "GET",
				  url: ctxPath+'/app/teacher/getTeacherId.do?',
				  dataType: 'json',
				}).done(function( responseJson )
				{
					//alert("value 2  "+responseJson.tid);
					$('#tid').val(responseJson.tid);
				});
			
	}
	
	</script>
	
	
	
</body>

</html>