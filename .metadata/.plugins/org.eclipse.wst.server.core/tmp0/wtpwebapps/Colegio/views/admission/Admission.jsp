<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Student Admission Form</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
    <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<style>
	.input-field div.error, .input-field div.error2{
		position: relative;
		top: -1rem;
		left: 0rem;
		font-size: 0.8rem;
		color:#FF4081;
		-webkit-transform: translateY(0%);
		-ms-transform: translateY(0%);
		-o-transform: translateY(0%);
		transform: translateY(0%);
	}
	.inp2{
		float:left;
		margin-top:0px;
	}
	.input-field div.error2{
		top:-8px;
	}
	.input-field label.active{
		width:100%;
	}
	h4.header2.title{
		float:left;
		width:100%;
	}
	
	#School_Name { 
    border-style: none; 
    border-color: Transparent; 
    overflow: auto;    
    readonly: none;    
  }
  
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START HEADER -->
  <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Materialize</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
  </header>
  <!-- END HEADER -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->
<%
                       // 	String uname=(String)session.getAttribute("userName2");
                        
                        %>
  <!-- START MAIN -->
  <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        String sname=(String)request.getAttribute("sname"); 
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                     <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=admission/Admission' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_Main' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/ParentsView' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=calender/app-calendar' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Activity_List' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff List</a>
                                    </li>
                                      <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachinglist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachingview' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach Staff</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <%-- <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examlist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Marks </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=MarkSheet/Marks_ClassList' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class wise Marks</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Finance</a>
                            <div class="collapsible-body">
                                <ul>
                                   <%--  <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_EChallan'>E-Challan</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeeDraft'>Fee Draft</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeesList'>Fee List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/FeeAllotMainpage'>Mainpage</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                 	    
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> News Feed </a>
                			<div class="collapsible-body">
                                <ul>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/events_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/news_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                       
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=SMS/SmsOrEmail' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> SMS/Email</a>
                        </li>
            </aside>
      <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

						


      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
			<div class="container">
				<div class="row">
				  <div class="col s12 m12 l12">
					<h5 class="breadcrumbs-title">Admission</h5>
					<ol class="breadcrumbs">
						<li><a href="../../index.html">Dashboard</a></li>
						<li class="active">Admission</li>
					</ol>
				  </div>
				</div>
			</div>
        </div>
        <!--breadcrumbs end-->
        <div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="row">
					<div class="col s12">
						<ul class="tabs tab-demo z-depth-1">
						  <li id="ad-tab1" class="tab col s3"><a class="active" href="#student-tab">Student</a>
						  </li>
						  <li id="ad-tab2" class="tab col s3"><a href="#parent-tab">Parent</a>
						  </li>
						  <li id="ad-tab3" class="tab col s3"><a href="#student-medical-tab">Student Medical</a>
						  </li>
						  <li id="ad-tab4" class="tab col s3"><a href="#print-tab">Office Use</a>
						  </li>
						</ul>
					</div>
					<div class="col s12">
						<form class="formValidate" id="formValidate" method="post">
							<div id="student-tab" class="col s12">
								<div class="row">
								
								<h4 class="header2 title">Student Details</h4>
								
									<div class="input-field col s12">
										<label for="fname">First Name *</label>
										<input id="student_id" name="student_id" type="hidden" maxlength="10" >
										<input id="parent_id" name="parent_id" type="hidden" maxlength="10" >
										<input id="adm_id" name="adm_id" type="hidden" maxlength="10" >
										<input id="loguser1" name="loguser1" type="hidden" maxlength="10" value="<%= sid %>">
										<input id="student_roll" name="student_roll" type="hidden" maxlength="10" >
										
										<input id="fname" name="fname" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your first name">
										<div class="error errorTxt1"></div>
									</div>

									<div class="input-field col s12">
										<label for="lname">Last Name *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="lname" name="lname" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your last name">
										<div class="error errorTxt2"></div>
									</div>
									
									<div class="col s12">
                  					
                   					 <div class="file-field input-field">
                     					 <div class="btn">
                       					 <span>Photo *</span>
                       					 <input type="file" id="photo" name="photo">
                     					 </div>
                     					 <div class="file-path-wrapper">
                     					   <input class="file-path validate" type="text">
                    					  </div>
                    					</div>
                 					
               						 </div>

									<div id="dob_date" class="input-field col s12 dob_date">
										<label for="dob">Date of birth *</label>
										<input type="text" name="dob" id="dob" class="datepicker ad_date stu_require" data-error="Please enter your date of birth">
										<div class="error errorTxt3"></div>
									</div>
									
									<div class="input-field col s12">
										<label for="bplace">Birth Place *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="bplace" name="bplace" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter Birth Place">
										<div class="error errorTxt2"></div>
									</div>

									<div class="col s12">
										<label for="genter_select">Nationality *</label>
									<p>
										<input name="nationality" type="radio" id="national_indian" class="stu_require" data-error="Please select your nationality" value="Indian"/>
										<label for="national_indian">Indian</label>
									</p>
									<p>
										<input name="nationality" type="radio" id="national_other" class="stu_require" data-error="Please select your nationality" value="Other"/>
										<label for="national_other">Other</label>
									</p>
										<div class="input-field">
											<div class="error errorTxt4"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="mtongue">Mother Tongue *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="mtongue" name="mtongue" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your last name">
										<div class="error errorTxt2"></div>
									</div>

									<div class="col s12">
										<label for="genter_select">Gender *</label>
										<p>
											<input name="cgender" type="radio" id="gender_male" class="stu_require" data-error="Please select your gender" value="Male" />
											<label for="gender_male">Male</label>
										</p>
										<p>
											<input name="cgender" type="radio" id="gender_female" class="stu_require" data-error="Please select your gender" value="FeMale" />
											<label for="gender_female">Female</label>
										</p>
										<div class="input-field">
											<div class="error errorTxt5"></div>
										</div>
									</div>
									
									<div class="col s12">
										<label for="religion">Religion1111</label>
										<p>
											<input name="religion" type="radio" id="rel_hindu" class="stu_require" data-error="Please select your religion" value="Hindu" />
											<label for="rel_hindu">Hindu</label>
										</p>
										<p>
											<input name="religion" type="radio" id="rel_muslim" class="stu_require" data-error="Please select your religion" value="Muslim" />
											<label for="rel_muslim">Muslim</label>
										</p>
										<p>
											<input name="religion" type="radio" id="rel_chirst" class="stu_require" data-error="Please select your religion" value="Chirstian"/>
											<label for="rel_chirst">Chirstian</label>
										</p>
										<p>
											<input name="religion" type="radio" id="rel_others" value="Yes" data-error="Please select your religion" id="rel_others" class="stu_require" value="Others" />
											<label for="rel_others">Others</label>
										</p>
										<div class="input-field">
											<div class="error"></div>
										</div>
										<div class="col s12 religion_box">
											<label for="rel_others">Religion *</label>
											<input id="rel_other" name="rel_other" data-error="Please fill the field" type="text" maxlength="50" class="textOnly" readonly>
										</div>
										<div class="input-field inp2">
											<div class="error2"></div>
										</div>
									</div>

									<div class="col s12">
										<label for="category_select">Category *</label>
										<p>
											<input name="category" type="radio" id="cat_oc" class="stu_require" data-error="Please select your category" value="OC" />
											<label for="cat_oc">OC</label>
										</p>
										<p>
											<input name="category" type="radio" id="cat_bc" class="stu_require" data-error="Please select your category" value="BC" />
											<label for="cat_bc">BC</label>
										</p>
										<p>
											<input name="category" type="radio" id="cat_sc" class="stu_require" data-error="Please select your category" value="SC"/>
											<label for="cat_sc">SC</label>
										</p>
										<p>
											<input name="category" type="radio" id="cat_st" class="stu_require" data-error="Please select your category" value="ST"/>
											<label for="cat_st">ST</label>
										</p>
										<div class="input-field">
											<div class="error errorTxt6"></div>
										</div>
									</div>

									<h4 class="header2 title">Residential Address</h4>

									<div class="input-field col s12">
										<label for="address1">Address Line 1 *</label>
										<input id="address1" name="address1" type="text" class="stu_require" maxlength="100" data-error="Please enter your address">
										<div class="error errorTxt8"></div>
									</div>

									<div class="input-field col s12">
										<label for="address2">Address Line 2</label>
										<input id="address2" name="address2" maxlength="100" type="text">
									</div>

									<div class="input-field col s12">
										<label for="city">City *</label>
										<input id="city" name="city" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your city">
										<div class="error errorTxt9"></div>
									</div>

									<div class="col s12">
										<label for="State">State *</label>
										<select class="browser-default stu_require" id="state" name="state" data-error="Please select your state">
											<option value="0">Please select your state</option>
											<option value="Tamil nadu">Tamil nadu</option>
											<option value="Kerala">Kerala</option>
											<option value="punjab">punjab</option>
										</select>
										<div class="input-field">
											<div class="error errorTxt10"></div>
										</div>
									</div>

									<div class="input-field col s12">
										<label for="pin">Pin *</label>
										<input id="pin" name="pin" maxlength="6" class="numbersOnly stu_require" type="text" data-error="Please enter your pin">
										<div class="error errorTxt11"></div>
									</div>
									
									<div class="input-field col s12">
										<label for="mobile">Mobile *</label>
										<input id="mobile" name="mobile" maxlength="10" class="numbersOnly stu_require" type="text" data-error="Please enter your mobile munber">
										<div class="error errorTxt12"></div>
									</div>

									<div class="input-field col s12">
										<label for="phone">Phone</label>
										<input id="phone" maxlength="10" name="phone" class="numbersOnly" type="text">
									</div>

									<div class="input-field col s12">
										<label for="cemail">Email *</label>
										<input id="cemail" type="text" name="cemail" class="email stu_require" maxlength="100" data-error="Please enter your email">
										<div class="error errorTxt13"></div>
									</div>

									<div class="col s12">
										<label for="genter_select">Whether school transport required</label>
										<p>
											<input name="sch_transport" type="radio" id="sch_transport_yes" value="Yes" />
											<label for="sch_transport_yes">Yes</label>
										</p>
										<p>
											<input name="sch_transport" type="radio" id="sch_transport_no" value="No" />
											<label for="sch_transport_no">No</label>
										</p>
									</div>
									
									<h4 class="header2 title">Previous School</h4>
									

									<div class="input-field col s12">
										<textarea id="pre_school" name="pre_school" class="materialize-textarea validate"></textarea>
										<label for="pre_school"> Name of previous school / pre-school / Nursury / Cre'che</label>
									</div>
										<div class="col s12">
										<label for="genter_select">Period of Stay</label>
										</div>
									
									<div id="prd_from_date" class="input-field col s12">
										<label for="prd_from">From</label>
										<input type="date" name="prd_from" id="prd_from" class="datepicker ad_date">
									</div>
									<div id="prd_to_date" class="input-field col s12">
										<label for="prd_to">To</label>
										<input type="date" name="prd_to" id="prd_to" class="datepicker ad_date">
									</div>
									<!-- <div class="header2">
										<label for="pre_school">Add another school</label>
<a class="btn-floating btn-large waves-effect waves-light green accent-3"><i class="mdi-content-add"></i></a>
</div> -->
									<div class="col s12">
										   <button class="btn waves-effect waves-light right" id="Next1" type="button" name="action" id="stu" onClick="studentInsert()">Next....
										<i class="mdi-content-send right"></i>
										</button>   
										<!-- <a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="studentInsert()" name="stu" id="stu">Next</a>  -->
										<!--<a href="#" id="stu" name="stu" tabindex="7">Next</a>-->
									</div>

								</div>
							</div>	
							
							</form>
							
							<div id="parent-tab" class="col s12">
								<div class="row">
									<h4 class="header2 title">Parent Details</h4>
									<div class="input-field col s12">
										<label for="prt_fname">Father Name *</label>
										<input id="parent_id2" name="parent_id2" type="hidden" maxlength="10" >
										<input id="prt_fname" name="prt_fname" type="text" maxlength="50" class="textOnly prt_require" data-error="Please enter your first name">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>

									<div class="input-field col s12">
										<label for="prt_mname">Mother Name *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="prt_mname" name="prt_mname" type="text" maxlength="50" class="textOnly prt_require" data-error="Please enter your last name">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>

									<div id="dob_date" class="input-field col s12 dob_date">
										<label for="prt_father_dob">Father Date of birth</label>
										<input type="text" name="prt_father_dob" id="prt_father_dob" class="datepicker ad_date" data-error="Please enter father date of birth">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div id="dob_date" class="input-field col s12 dob_date">
										<label for="prt_mother_dob">Mother Date of birth</label>
										<input type="text" name="prt_mother_dob" id="prt_mother_dob" class="datepicker ad_date" data-error="Please enter mother date of birth">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_occupa">Occupation</label>
										<input id="prt_occupa" name="prt_occupa" type="text" maxlength="50" class="textOnly" data-error="Please enter your occupation">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_design">Designation</label>
										<input id="prt_design" name="prt_design" type="text" maxlength="50" class="textOnly" data-error="Please enter your designation">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_off_add">Office address</label>
										<input id="prt_off_add" name="prt_off_add" type="text" maxlength="50" class="" data-error="Please enter your office address">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_off_phn">Office phone</label>
										<input id="prt_off_phn" name="prt_off_phn" type="text" maxlength="50" class="numbersOnly" data-error="Please enter your office phone number">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_mobile">Mobile *</label>
										<input id="prt_mobile" name="prt_mobile" type="text" maxlength="50" class="prt_require numbersOnly" data-error="Please enter your mobile number">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_email">Email *</label>
										<input id="prt_email" name="prt_email" type="text" maxlength="50" class="prt_require" data-error="Please enter your email id">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<div class="file-field input-field">
											<div class="btn">
												<span>Photo</span>
												<input type="file" id="prt_photo" name="prt_photo">
											</div>
											<div class="file-path-wrapper">
												<input class="file-path validate" type="text">
											</div>
										</div>
									</div>
									
									<h4 class="header2 title">Guardian Details</h4>
									
									<div class="input-field col s12">
										<label for="prt_gname">Guardian Name *</label>
										<input id="prt_gname" name="prt_gname" type="text" maxlength="50" class="textOnly" data-error="Please enter your guardian name">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									<div class="input-field col s12">
										<label for="prt_grd_occupa">Occupation</label>
										<input id="prt_grd_occupa" name="prt_grd_occupa" type="text" maxlength="50" class="textOnly" data-error="Please enter your occupation">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_grd_design">Designation</label>
										<input id="prt_grd_design" name="prt_grd_design" type="text" maxlength="50" class="textOnly" data-error="Please enter your designation">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_grd_off_add">Office address</label>
										<input id="prt_grd_off_add" name="prt_grd_off_add" type="text" maxlength="50" class="" data-error="Please enter your office address">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_grd_home_add">home address</label>
										<input id="prt_grd_home_add" name="prt_grd_home_add" type="text" maxlength="50" class="textOnly" data-error="Please enter your home address">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_grd_mobile">Mobile</label>
										<input id="prt_grd_mobile" name="prt_grd_mobile" type="text" maxlength="50" class="numbersOnly" data-error="Please enter your phone number">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_grd_email">Email</label>
										<input id="prt_grd_email" name="prt_grd_email" type="text" maxlength="50" class="textOnly" data-error="Please enter your email id">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="prt_grd_relation">Relation</label>
										<input id="prt_grd_relation" name="prt_grd_relation" type="text" maxlength="50" class="textOnly" data-error="Please enter your relation">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>

									<div class="col s6">
										<button class="btn waves-effect waves-light left" id="Prev2" type="button" name="action">Prev
										<i class="mdi-content-send rotate left"></i>
										</button>
									</div>
									<div class="col s6">
										  <button class="btn waves-effect waves-light right" id="Next2" type="button" name="parent" id="parent" onClick="parentInsert()">Next....
										<i class="mdi-content-send right"></i>
										</button> 
										<!-- <a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="parentInsert()" name="parent" id="parent">Next</a> -->
									</div>

								</div>
							</div>
							<div id="student-medical-tab" class="col s12">
								<div class="row">
								
								<h4 class="header2 title">Medical History</h4>
								
									<div class="input-field col s12">
										<!-- <label for="stu_blood_grp">Blood group *</label> -->
										<input id="student_id2" name="student_id2" type="hidden" maxlength="10" >
										<!-- <input id="stu_blood_grp" name="stu_blood_grp" type="text" maxlength="50" class="med_require" data-error="Please enter your blood group"> -->
										Blood Group <select class="browser-default stu_require" id="stu_blood_grp" name="stu_blood_grp" data-error="Please select your Blood Group">
																		<option value="A1+">A1+</option>
																		<option value="A+">A+</option>
																		<option value="B+">B+</option>
										</select>
										
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>

									<div class="input-field col s12">
										<label for="stu_height">Height *</label>
										<input id="stu_height" name="stu_height" type="text" maxlength="50" class="numbersOnly med_require" data-error="Please enter your height">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="stu_weight">Weight *</label>
										<input id="stu_weight" name="stu_weight" type="text" maxlength="50" class="numbersOnly med_require" data-error="Please enter your weight">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="stu_med_cnd">Medical condition *</label>
										<input id="stu_med_cnd" name="stu_med_cnd" type="text" maxlength="50" class="textOnly med_require" data-error="Please enter your medical condition">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="col s12">
										<label for="allergy_select">Allergy</label>
										<p>
											<input name="allergy_select" value="Yes" type="radio" data-error="Please select" id="algy_yes" class="med_require" value="Yes" />
											<label for="algy_yes">Yes</label>
										</p>
										<p>
											<input name="allergy_select" value="No" type="radio" data-error="Please select" id="algy_no" class="med_require" value="No"/>
											<label for="algy_no">No</label>
										</p>
										<div class="input-field">
											<div class="error"></div>
										</div>
										<div class="col s12 allergy_box">
											<label for="type_alg">Enter the type of Allergy *</label>
											<input id="type_alg" name="type_alg" data-error="Please fill the field" type="text" maxlength="50" class="textOnly">
										</div>
										<div class="input-field inp2">
											<div class="error2"></div>
										</div>

									</div>
									
									<div class="col s12">
										<label for="phy_chg_select1">Physically challenge</label>
										<p>
											<input name="phy_chg_select" type="radio"  data-error="Please select" id="phy_chg_yes" class="med_require" value="Yes" />
											<label for="phy_chg_yes">Yes</label>
										</p>
										<p>
											<input name="phy_chg_select" type="radio"  data-error="Please select" id="phy_chg_no" class="med_require" value="No" />
											<label for="phy_chg_no">No</label>
										</p>
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="col s12">
										<label for="sp_child_select">Special child</label>
										<p>
											<input name="sp_child_select" type="radio" value="Yes" data-error="Please select" id="sp_child_yes" class="med_require" />
											<label for="sp_child_yes">Yes</label>
										</p>
										<p>
											<input name="sp_child_select" type="radio" value="No" data-error="Please select" id="sp_child_no" class="med_require" />
											<label for="sp_child_no">No</label>
										</p>
										<div class="input-field">
											<div class="error"></div>
										</div>
										<div class="col s12 special_child_box">
											<label for="type_sp_child">Enter special child Details *</label>
											<input id="type_sp_child" name="type_sp_child" data-error="Please fill the field" type="text" maxlength="50" class="textOnly">
										</div>
										<div class="input-field inp2">
											<div class="error2"></div>
										</div>
									</div>
									
									<div class="col s12">
										<label for="disease_select">Disease</label>
										<p>
											<input name="disease_select" type="radio" value="Yes" data-error="Please select" id="disease_yes" class="med_require" />
											<label for="disease_yes">Yes</label>
										</p>
										<p>
											<input name="disease_select" type="radio" value="No" data-error="Please select" id="disease_no" class="med_require" />
											<label for="disease_no">No</label>
										</p>
										<div class="input-field">
											<div class="error"></div>
										</div>
										<div class="input-field col s12 disease_box">
											<label for="type_disease">Enter Types of disease *</label>
											<input id="type_disease" name="type_disease" type="text" maxlength="50" class="textOnly" data-error="Please fill the field">
										</div>
										<div class="input-field inp2">
											<div class="error2"></div>
										</div>
									</div>
									
									<div class="col s6">
										<button class="btn waves-effect waves-light left" id="Prev3" type="button" name="action">Prev
										<i class="mdi-content-send rotate left"></i>
										</button>
									</div>
									<div class="col s6">
										<button class="btn waves-effect waves-light right" id="Next3" type="button" name="medical" id="medical" onClick="medicalInsert()">Next....
										<i class="mdi-content-send right"></i>
										</button> 
										<!-- <a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="medicalInsert()" name="medical" id="medical">Next</a> -->
									</div>
									
								</div>
							</div>
							<div id="print-tab" class="col s12">
								<div class="row">
									<h4 class="header2 title">Office Use</h4>
									
									<div class="input-field col s12">
										<label for="admn_no">Admission number *</label>
										<input id="adm_id2" name="adm_id2" type="text" maxlength="10" readonly>
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="stu_roll_no">Student Roll Number *</label>
										<input id="student_roll2" name="student_roll2" type="text" maxlength="10" readonly>
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div id="join_date" class="input-field col s12 dob_date">
										<label for="dob">Joining Date *</label>
										<input type="text" name="join" id="join" data-error="Please enter Date of Joining" readonly>
										<div class="error errorTxt3"></div>
									</div>
									
									<div class="input-field col s12">
										<label for="hostel_name">Hostel Name </label>
										<input id="hostel_name" name="hostel_name" type="text" maxlength="50" value="Later">
									</div>
									<div class="input-field col s12">
										<label for="hostel_no">Hostel Room Number </label>
										<input id="hostel_no" name="hostel_no" type="text" maxlength="50" value="Later" readonly>
									</div>
									<div class="input-field col s12">
										<label for="School_Name">School Name *</label> <br>
										<%-- <input id="School_Name" name="School_Name" type="text" maxlength="10" readonly value=<%= sname %>> --%>
										<textarea id="School_Name" name="School_Name" rows=1 cols=30 readonly><%= sname %></textarea>
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
                                    <div class="input-field col s12">
										<label for="Academic_year">Academic Year *</label>
										<input id="Academic_year" name="Academic_year" type="text" maxlength="10" readonly>
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
																		
									<div class="col s12">
										<label for="SClass1">Class *</label>
										<select class="browser-default stu_require" id="SClass1" name="SClass1" data-error="Please select your Class">
											<option value="NIL">Please select your Class</option>
											<option value="1st Std">1st Std</option>
											<option value="2nd Std">2nd Std</option>
											<option value="3rd Std">3rd Std</option>
											<option value="4th Std">4th Std</option>
											<option value="5th Std">5th Std</option>
											<option value="6th Std">6th Std</option>
											<option value="7th Std">7th Std</option>
											<option value="8th Std">8th Std</option>
										</select>
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="col s12">
										<label for="Section1">Section *</label>
										<select class="browser-default stu_require" id="Section1" name="Section1" data-error="Please select your Section">
											<option value="NIL">Please select your Section</option>
											<option value="A Section">A Section</option>
											<option value="B Section">B Section</option>
											<option value="C Section">C Section</option>
											<option value="D Section">D Section</option>
											<option value="E Section">E Section</option>
											<option value="F Section">F Section</option>
											<option value="G Section">G Section</option>
											<option value="H Section">H Section</option>
										</select>
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									
							  <div class="col s6">
										 <button class="btn waves-effect waves-light right" id="Next4" type="button" name="final" id="final" onClick="finalInsert()">Submit
										<i class="mdi-content-send right"></i>
										</button> 
										<!-- <a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="finalInsert()" name="final" id="final">Submit</a> -->
										
									</div>
							</div>
						
					</div>
					</div>
				</div>
            </div>
		</div>
    </section>
      <!-- END CONTENT -->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->



  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://orectiq.com/">Orectiq</a></span>
        </div>
    </div>
  </footer>
  <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism
    <script type="text/javascript" src="js/prism/prism.js"></script>-->
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
	<script type="text/javascript" src="../../js/admission-custom-script.js"></script>
	<script type="text/javascript" src="Admission.js"></script>
	<script> var ctxPath = "<%=request.getContextPath() %>";</script>
		
		
		<script>
		$(document).ready(function() {
			alert("hi");
			//
			$( "#religion" ).prop( "disabled", true );
			});
		</script>
		
	
	<script>

	
	$('#rel_hindu').click(function()
	{
		var cvalue=$("input[name='religion']:checked").val();
		$("#rel_other").val(cvalue);
	});
	$('#rel_muslim').click(function()
			{
				var cvalue=$("input[name='religion']:checked").val();
				$("#rel_other").val(cvalue);
			});
	$('#rel_chirst').click(function()
			{
				var cvalue=$("input[name='religion']:checked").val();
				$("#rel_other").val(cvalue);
			});
	$('#rel_others').click(function()
			{
				 $('#rel_other').val("");
				 $("#rel_other").prop("readonly", false);
			});
	
	referenceValue();
	
	function studentInsert()
	{
		
		 var nat = document.getElementsByName("nationality");
		    if (nat[0].checked == true) {
		   	 nation=nat[0].value;
		    } else if(nat[1].checked == true) {
		   	 nation=nat[1].value;
		    }
		    
		    var gender = document.getElementsByName("cgender");
		    if (gender[0].checked == true) {
		   	 xender=gender[0].value;
		    } else if(gender[1].checked == true) {
		   	 xender=gender[1].value;
		    }
		    
		    /* var reli = document.getElementsByName("religion");
		    if (reli[0].checked == true) {
		        relig=reli[0].value;
		    } else if (reli[1].checked == true) {
		   	 relig=reli[1].value;
		    }
		   	 else if (reli[2].checked == true) {
		       	 relig=reli[2].value;
		   	 }
		       	 else if (reli[3].checked == true) {
		           	 relig=reli[3].value; 
		    } */
		    
		    var caste = document.getElementsByName("category");
		    if (caste[0].checked == true) {
		        cate=caste[0].value;
		    } else if (caste[1].checked == true) {
		   	 cate=caste[1].value;
		    }
		   	 else if (caste[2].checked == true) {
		       	 cate=caste[2].value;
		   	 }
		       	 else if (caste[3].checked == true) {
		           	 cate=caste[3].value; 
		    }
		    var trans = document.getElementsByName("sch_transport");
		    if (trans[0].checked == true) {
		   	 transport=trans[0].value;
		    } else if(trans[1].checked == true) {
		   	 transport=trans[1].value;
		    }
		    alert("Insert insertStu2");
		 $.ajax({
			 
	        type: 'POST',
	        url: ctxPath+'/app/student/insertStu2.do?',
	        dataType: 'json',
	        //data: JSON.stringify(data),
	        data: {"student_id":$("#student_id").val(),"firstname":$("#fname").val(),"lastname":$("#lname").val(),"photo":$("#photo").val(),"sdob":$("#dob").val(),
				 "bplace":$("#bplace").val(),"nationality":nation,"mtongue":$("#mtongue").val(),"cgender":xender,
				 "religion":$("#rel_other").val(),"category":cate,"address1":$("#address1").val(),"address2":$("#address2").val(),
				 "state":$("#state").val(),"pin":$("#pin").val(),"mobile":$("#mobile").val(),"phone":$("#phone").val(),
				 "cemail":$("#cemail").val(),"sch_transport":transport,"pre_school":$("#pre_school").val(),"prd_from":$("#prd_from").val(),
				 "prd_to":$("#prd_to").val(),"parent_id":$("#parent_id").val(),"adm_id":$("#adm_id2").val(),"student_roll":$("#student_roll").val()},
				 
	        success: function(data) 
	        {
	        	 alert("Insert success");
	       	 if(data=='success')
	       	 {
	       		Materialize.toast("Success",4000);
	       	 }
	       	 else 
	       	 {
	       		 Materialize.toast(data,4000);	 
	       	 }  
	        }
		 });
	}
	
	function parentInsert()
	{
		//alert("parent Insert entered111111111111111");
		 var dt=new Date();
		 
		 $.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/student/insertParent.do?',
	        //data: JSON.stringify(data),
	        data: {"parent_id":$("#parent_id2").val(),"prt_fname":$("#prt_fname").val(),"prt_mname":$("#prt_mname").val(),"prt_father_dob":$("#prt_father_dob").val(),
	        	  "prt_mother_dob":$("#prt_mother_dob").val(),"prt_occupa":$("#prt_occupa").val(),"prt_design":$("#prt_design").val(),"prt_off_add":$("#prt_off_add").val(),
	        	  "prt_off_phn":$("#prt_off_phn").val(),"prt_mobile":$("#prt_mobile").val(),"prt_email":$("#prt_email").val(),"prt_photo":$("#prt_photo").val(),
	        	  "prt_gname":$("#prt_gname").val(),"prt_grd_occupa":$("#prt_grd_occupa").val(),"prt_grd_design":$("#prt_grd_design").val(),"prt_grd_off_add":$("#prt_grd_off_add").val(),
	        	  "prt_grd_home_add":$("#prt_grd_home_add").val(),"prt_grd_mobile":$("#prt_grd_mobile").val(),"prt_grd_email":$("#prt_grd_email").val(),"prt_grd_relation":$("#prt_grd_relation").val(),
	        	  "isActive":'Y',"inserted_by":$("#loguser1").val(),"inserted_date":dt,"updated_by":$("#loguser1").val(),"updated_date":dt},
	        success: function(data) 
	        {
	       	 if(data=='success')
	       	 {
	       		Materialize.toast("Success",4000);	 
	       	 }
	       	 else 
	       	 {
	       		 Materialize.toast(data,4000);	 
	       	 }  
	        }
		 });
	}
	
	function finalInsert()
	{
		// alert("final Insert entered111111111111111");
		 var dt=new Date();
		 //alert("school_name,aca,class and sec "+$("#School_Name").val()+" and "+$("#Academic_year").val()+" and "+$("#SClass1").val()+" and "+$("#Section1").val());
		 $.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/student/insertFinal.do?',
	        //data: JSON.stringify(data),
	        data: {"Join_date":dt,"Hostel_name":$("#hostel_name").val(),"Hostel_Room_no":$("#hostel_no").val(),
	        	   "School_Name":$("#School_Name").val(),"Academic_year":$("#Academic_year").val(),"SClass":$("#SClass1").val(),"Section":$("#Section1").val(),
	        	   "adm_id":$("#adm_id").val()},
	        success: function(data) 
	        {
	       	 if(data=='success')
	       	 {
	       		Materialize.toast("Success",4000);	 
	       	 }
	       	 else 
	       	 {
	       		 Materialize.toast(data,4000);	 
	       	 }  
	        }
		 });
	}
	function medicalInsert()
	{
		 //alert("medical Insert entered111111111111111");
		 
		 var phy = document.getElementsByName("phy_chg_select");
		    if (phy[0].checked == true) {
		   	 phychg=phy[0].value;
		    } else if(phy[1].checked == true) {
		    	phychg=phy[1].value;
		    }
		    
		    //Allergy
		    var aller = document.getElementsByName("allergy_select");
		    if (aller[0].checked == true) {
		   	 isaller1=aller[0].value;
		   	 aller_type=$("#type_alg").val();
		    } else if(aller[1].checked == true) {
		    	isaller1=aller[1].value;
		    	aller_type="NIL";
		    }
		 
		    //Special Child
		    var spchild = document.getElementsByName("sp_child_select");
		    if (spchild[0].checked == true) {
		   	 is_spchild=spchild[0].value;
		   	 sp_type=$("#type_sp_child").val();
		    } else if(spchild[1].checked == true) {
		    	is_spchild=spchild[1].value;
		    	sp_type="NIL";
		    }
		    
		  //Disease
		    var disease = document.getElementsByName("disease_select");
		    if (disease[0].checked == true) {
		   	 is_disease=disease[0].value;
		   	 disease_type=$("#type_disease").val();
		    } else if(disease[1].checked == true) {
		    	is_disease=disease[1].value;
		    	disease_type="NIL";
		    }
		    
		    
		 var dt=new Date();
		 
		 $.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/student/insertMedical.do?',
	        //data: JSON.stringify(data),
	        data: {"student_id":$("#student_id2").val(),"blood_group":$("#stu_blood_grp").val(),"height":$("#stu_height").val(),"weights":$("#stu_weight").val(),
	        	  "medical_condition":$("#stu_med_cnd").val(),"allergy":isaller1,"allergy_type":aller_type,"is_Physically_Challenged":phychg,
	        	  "is_Special_Child":is_spchild,"specialChild_type":sp_type,"is_disease":is_disease,"disease_type":disease_type,
	        	  "isActive":'Y',"inserted_by":$("#loguser1").val(),"inserted_date":dt,"updated_by":$("#loguser1").val(),"updated_date":dt},
	        success: function(data) 
	        {
	       	 if(data=='success')
	       	 {
	       		Materialize.toast("Success",4000);	 
	       	 }
	       	 else 
	       	 {
	       		 Materialize.toast(data,4000);	 
	       	 }  
	        }
		 });
	}
	
	
	function referenceValue()
	{
		//alert("ref. value enter 1");
		 var dt=new Date();
		 var d1=dt.getDate();
		 var d2=dt.getMonth();
		 var d3=dt.getFullYear();
		 var dd3=d3+1;
		 var d4=d3+"-0"+d2+"-"+d1;
		 var dd4=d3+"-"+dd3
		 $('#Academic_year').val(dd4);
		 
		 $('#join').val(d4);
		$.ajax({
				  type: "GET",
				  url: ctxPath+'/app/student/getStuId.do?',
				  dataType: 'json',
				}).done(function( responseJson ){	
					$('#student_id').val(responseJson.student_id);
					$('#student_id2').val(responseJson.student_id);
				});
		$.ajax({
			  type: "GET",
			  url: ctxPath+'/app/student/getParId.do?',
			  dataType: 'json',
			}).done(function( responseJson ){	
				$('#parent_id').val(responseJson.parent_id);
				$('#parent_id2').val(responseJson.parent_id);
			});

		$.ajax({
			  type: "GET",
			  url: ctxPath+'/app/student/getAdmId.do?',
			  dataType: 'json',
			}).done(function( responseJson ){	
				$('#adm_id').val(responseJson.adm_id);
				$('#adm_id2').val(responseJson.adm_id);
			});
		
		$.ajax({
			  type: "GET",
			  url: ctxPath+'/app/student/getAdmId1.do?',
			  dataType: 'json',
			}).done(function( responseJson ){	
				//$('#adm_id').val(responseJson.adm_id);
				$('#adm_id2').val(responseJson.adm_id);
			});

		$.ajax({
			  type: "GET",
			  url: ctxPath+'/app/student/getStuRoll.do?',
			  dataType: 'json',
			}).done(function( responseJson ){	
				$('#student_roll').val(responseJson.student_roll);
				$('#student_roll2').val(responseJson.student_roll);
			});

	} 
	
	
	</script>
</body>

</html>