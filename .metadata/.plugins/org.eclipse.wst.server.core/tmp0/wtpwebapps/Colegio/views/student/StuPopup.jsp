<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Student List - Colegio - A Orectiq Product</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
	body {
    background-color: rgb(113, 218, 113);
		}
		
		input {
    background-color: #71da71;
    border: 0px solid;
    height: 20px;
    width: 160px;
    color: white;
}



</style>
</head>

<body bgcolor="lightgreen" text="white">

<div id="modal4" class="modal modal-fixed-footer green white-text stu_list_pop_box">
            <div class="modal-content">
            <div id="bordered-table">
            <table border=0 id="Stu1">
            	
             <col width="800">
  			<col width="800">
		<thead>
            <tr>
            <td align="left">  <h1 class="header left">Student Name </h1> </td>
            <td>  <h1 class="header right">Academic Year</h1>  </td>
             </tr>
             </thead>
              
              <tbody>
              </tbody>
              
              
              </table>
              
              
              
              <div class="row">
              
              <table border=0>
              <tr>
              	<td>
                <div class="col s12 m4 l3">
                  <img title="Avatar" src="../../resources/images/avatar.jpg">
                </div>
                </td>
                <td>
                <div class="col s12 m8 l9">
                  <table id="Stu2">
                    <thead>   
                        <tr>
                        <td>Class:</td>
                         <td><h3>V Std </h3></td>
                      </tr>
                      <tr>
                        <td>Roll Number:</td>
                        <td><input id="roll" name="cl" type="text" size="45" tabindex="5" disabled  /></td>
                      </tr>
                      </thead>
                      
                      <tbody>
                      
                      </tbody>
                      
                </table>
                </div>
                </div>
                </td>
                
                <td width="500" align="right"><h2> Student Details </h2></td>
                
                </tr>
                </table>
                      
                      <div class="col s12 m4 l3">
                  <form  id="slick-login" method="post">
				                  <table border=1 id="stu-db-data-table_1" class="responsive-table display" cellspacing="0">
				                    <thead>
				                        <tr>
				                          <td>
				                            <th>Birth Date</th>
				                            <th>Gender</th>
				                            <th>Nationality</th>
				                            <th>Religion</th>
				                            <th>Community</th>
				                            <th>Place of Birth</th>
				                            <th>Mother Tongue</th>
				                            <th>Date of Joining</th>
				                            <th>Permanent Address</th>
				                            <th>Communication Address</th>
				                            <th>Phone Number</th>
				                           </td>
				                        </tr>
				                    </thead>
				                 
				                    <tbody>
				                        
				                      
				                    </tbody>
				                  </table>
				                  </form>
                </div>
            </div>
            </div>
            </div>
            </div>

            
             <!-- ================================================
    Scripts
    ================================================ -->
     <script type="text/javascript">
    	//alert("retrieve 1");
    	//retrieve();
    	
    </script>
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <!--<script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>-->
    <!--<script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>-->
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
     <script> var ctxPath = "<%=request.getContextPath() %>";</script>
     
    <script type="text/javascript">

        /*Show entries on click hide*/

        
        $(document).ready(function()
        		{
        	$("#roll").css("font-size",20 + "px");
        	retrieve();
           /* $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });*/

            /*** STUDENT LIST INNER DROP DOWN ***/

	       /*$('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });*/

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });

        });
    </script>
    
    
    <script type="text/javascript">
 //  alert("retrieve 2");
  function retrieve()
  {
	  //alert("retrieve 3");
	  var studid="STU00002";
  	$.ajax({
  			  type: "GET",
  			  url: ctxPath+'/app/student/getltrreferencedata.do?',
  			  data: {"student_id":studid},
  			  dataType: 'json',
  			}).done(function( responseJson )
  					{	
  						//alert("retrieve 4");
  						//alert("value 1 "+responseJson.student_id);
  					    loadDataIntoTable(responseJson);
  						
  				});
  }
  
  
  
  function loadDataIntoTable(responseJson)
  {
	  //alert("retrieve 5");
  	 var tblHtml = "";
  	 var tblHtml1="";
  	 var tblHtml2="";
  	//alert("List    "+responseJson.studentVOList.fname);
  	 //$.each(responseJson.studentVOList, function(index, val)
  //	{
  		//alert("fname  "+responseJson.fname+" lname  "+responseJson.lname);
  			 tblHtml += '<tr><td></td><td style="text-align: left">'+responseJson.dob+'</td><td style="text-align: left" >'+responseJson.cgender+'</td><td style="text-align: left">'+responseJson.nationality+'</td><td style="text-align: left">'+responseJson.religion+'</td><td style="text-align: left">'+responseJson.category+'</td>'+
  			 '<td style="text-align: left">'+responseJson.bplace+'</td><td style="text-align: left">'+responseJson.mtongue+'</td><td style="text-align: left">'+responseJson.Join_date+'</td><td style="text-align: left">'+responseJson.address1+'</td><td style="text-align: left">'+responseJson.address2+'</td><td style="text-align: left">'+responseJson.phone+'</td></tr>';
  			    //'<td style="text-align: center">'+responseJson.dob+'</td><td style="text-align: right">'+responseJson.bplace+'</td><td style="text-align: right">'+responseJson.nationality+'</td><td style="text-align: right">'+responseJson.mtongue+'</td><td style="text-align: right">'+val.cgender+'</td></tr>';
  	// });   
  			 tblHtml1 += '<tr><td style="text-align: left"><h1>'+responseJson.fname+'</h1></td><td style="text-align: left"><h1>2016</h1></td></tr>';
  			 tblHtml2 += '<tr><td style="text-align: left"><h1>V STD</h1></td><td style="text-align: left">'+responseJson.student_roll+'</td></tr>';
  	 
  			$('#roll').val(responseJson.student_roll);
  			 
  	 $("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
  	 $("#Stu1 tbody").html(tblHtml1);
  	//$("#Stu2 tbody").html(tblHtml2);
  }
  
  function getStudentsListAll(obj)
  {
	  modal.style.display = "block";
	 // $('#modal4').fadeOut("slow");
	  alert("click button");
  }
 
  
  </script>  
  
  <script>
  $(document).on('focus', '.addpopup', function() {
	  $('#modal4').popup('open');
	 });
</script>
            </body>

</html>