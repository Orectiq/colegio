<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Student Admission Form</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
    <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<style>
	.input-field div.error, .input-field div.error2{
		position: relative;
		top: -1rem;
		left: 0rem;
		font-size: 0.8rem;
		color:#FF4081;
		-webkit-transform: translateY(0%);
		-ms-transform: translateY(0%);
		-o-transform: translateY(0%);
		transform: translateY(0%);
	}
	.inp2{
		float:left;
		margin-top:0px;
	}
	.input-field div.error2{
		top:-8px;
	}
	.input-field label.active{
		width:100%;
	}
	h4.header2.title{
		float:left;
		width:100%;
	}
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START HEADER -->
  <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Materialize</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- translation-button -->
                    <ul id="translation-dropdown" class="dropdown-content">
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
  </header>
  <!-- END HEADER -->


<%
                        	String sid=(String)request.getAttribute("stuid");
							
                        
                        %>






  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START MAIN -->
  <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="#"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                        <%
                        	String uname=(String)session.getAttribute("userName2");
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= uname %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=dashboard/dashboard' class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                <%-- <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Student</a>
                </li> --%>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/Parent_Student' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Stu-Par List</a>
                        		    </li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_Main' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_List' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> ParentsList</a>
                        		    </li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/ParentsView' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                 	
               <%--  <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers</a>
                </li>
                
                
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers List</a>
                </li> --%>
                <li class="bold"><a href="../calender/app-calendar.html" class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Class_Main' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/AddActivity' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Activity_List' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Teachers </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                 		 <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Non-Teaching Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteaching' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teaching Staff</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachinglist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachingview' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach View</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                    
                                    <li><a href="">Teachers Attendance</a>
                                    </li>
                                    <li><a href="">Attendance Reports</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examlist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Accounts</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="table-basic.html">Fees Allocation</a>
                                    </li>
                                    <li><a href="table-data.html">Payment Details</a>
                                    </li>
                                    <li><a href="table-jsgrid.html">Teachers Salary</a>
                                    </li>
                                    <li><a href="table-editable.html">Other Expenses</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                       
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Library </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/LibraryMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library Details </a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/Libraryreqbook' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library Request Details </a>
                                    </li>
                                    
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/librarybook' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/libraryreqbooklist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library Request List</a>
                                    </li>
                                </ul>
                            </div>
                 	    </li>
                 	    
                 	    
                 	    
                         <li class="bold"><a href='<%=request.getContextPath()%>/app/book/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-editor-insert-comment"></i> Attendance </a>
                        </li>
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/book/loadpage?d=transport/transport_table' class="waves-effect waves-cyan"><i class="mdi-editor-insert-comment"></i> Transport </a>
                        </li>
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/Dormitory/loadpage?d=Dormitory/Dormitory_list' class="waves-effect waves-cyan"><i class="mdi-social-pages"></i> Dormitories</a>
                        </li>
                        <!-- <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-action-shopping-cart"></i> Notice Board</a>
                        </li> -->
                       <!--  <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Media Center</a>
                        </li> -->
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Notice Board </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/addevent' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/addnews' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/events_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/news_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Media Center </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Addmedia' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Media</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Addalbum' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Album</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Medialist' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Media List</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Albumlist' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Album List</a>
                        		    </li>
                   				</ul>
                     		</div>
                 	    </li>
                        
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=admission/Admission' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-action-account-circle"></i> Settings</a>
                            <div class="collapsible-body">
                                <ul>     
                                    <li><a href="user-profile-page.html">User Profile</a>
                                    </li>                                   
                                    <li><a href="user-login.html">Login</a>
                                    </li>                                        
                                    <li><a href="user-register.html">Register</a>
                                    </li>
                                    <li><a href="user-forgot-password.html">Forgot Password</a>
                                    </li>
                                    <li><a href="user-lock-screen.html">Lock Screen</a>
                                    </li>                                        
                                    <li><a href="user-session-timeout.html">Session Timeout</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
            </aside>
      <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
			<div class="container">
				<div class="row">
				  <div class="col m6">
					<h5 class="breadcrumbs-title">Student Name</h5>
					
				  </div>
				  <div class="col m6">
					<h5 class="breadcrumbs-title right">Academic Year</h5>
					
				  </div>
				</div>
			</div>
        </div>
        <!--breadcrumbs end-->
        <div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="row">
					
					<div class="col s12">
						<form class="formValidate" id="formValidate" method="post">
							<div id="student-tab" class="col s12">
								<div class="row">
								
								<h4 class="header2 title">Student Details</h4>
								
								
								 <div class="container">   
		<form class="col m6">
		<img class="materialboxed"  src="Photo.jpg">
		    <label for="Warden">Student</label>
								
									<div class="input-field col s12">
										<label for="fname">First Name * </label>
										<input id="student_id" name="student_id" type="text" maxlength="10" value=<%= sid %> >
										<input id="fname" name="fname" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your first name">
										<div class="error errorTxt1"></div>
									</div>

									<div class="input-field col s12">
										<label for="uname">Last Name *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="uname" name="lname" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your last name">
										<div class="error errorTxt2"></div>
									</div>
									
									<div class="col s12">
                  					<form action="#">
                   					 <div class="file-field input-field">
                     					 <div class="btn">
                       					 <span>Photo *</span>
                       					 <input type="file">
                     					 </div>
                     					 <div class="file-path-wrapper">
                     					   <input class="file-path validate" type="text">
                    					  </div>
                    					</div>
                 					 </form>
               						 </div>


									<div id="dob_date" class="input-field col s12 dob_date">
										<label for="dob">Date of birth *</label>
										<input type="text" name="dob" id="dob" class="datepicker ad_date stu_require" data-error="Please enter your date of birth">
										<div class="error errorTxt3"></div>
									</div>
                                     
									<div class="input-field col s12">
										<label for="bplace">Birth Place *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="bplace" name="bplace" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter Birth Place">
										<div class="error errorTxt2"></div>
									</div>
									
									
									
									<div class="input-field col s12">
										<label for="nationality">Nationality *</label>
										<input id="nationality" name="nationality" type="text" maxlength="50" class="textOnly med_require" data-error="Please enter your medical condition">
										<div class="input-field">
										
											<div class="error"></div>
											<div class="error errorTxt4"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="mtongue">Mother Tongue *</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
										<input id="mtongue" name="mtongue" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your mother tongue">
										<div class="error errorTxt2"></div>
									</div>
								
									<div class="input-field col s12">
										<label for="gender">Gender *</label>
										<input id="gender" name="gender" type="text" maxlength="50" class="textOnly med_require" data-error="Please enter your medical condition">
										<div class="input-field">
											<div class="error"></div>
											<div class="error errorTxt5"></div>
										</div>
									</div>
									
									

									
									<div class="input-field col s12">
										<label for="religion">Religion *</label>
										<input id="religion" name="religion" type="text" maxlength="50" class="textOnly med_require" data-error="Please enter your medical condition">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="category">Category *</label>
										<input id="category" name="category" type="text" maxlength="50" class="textOnly med_require" data-error="Please enter your category">
										<div class="input-field">
											<div class="error"></div>
											<div class="error errorTxt6"></div>
										</div>
									</div>
									
									<h4 class="header2 title">Residential Address</h4>

									<div class="input-field col s12">
										<label for="address1">Address Line 1 *</label>
										<input id="address1" name="address1" type="text" class="stu_require" maxlength="100" data-error="Please enter your address">
										<div class="error errorTxt8"></div>
									</div>

									<div class="input-field col s12">
										<label for="address2">Address Line 2</label>
										<input id="address2" name="address2" maxlength="100" type="text">
									</div>

									<div class="input-field col s12">
										<label for="city">City * </label>
										<input id="city" name="city" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your city">
										<div class="error errorTxt9"></div>
									</div>

                                      <div class="input-field col s12">
										<label for="state">State *</label>
										<input id="state" name="state" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your state">
										<div class="error errorTxt9"></div>
									</div>

									<div class="input-field col s12">
										<label for="pin">Pin *</label>
										<input id="pin" name="pin" maxlength="6" class="numbersOnly stu_require" type="text" data-error="Please enter your pin">
										<div class="error errorTxt11"></div>
									</div>
									
									<div class="input-field col s12">
										<label for="mobile">Mobile * </label>
										<input id="mobile" name="mobile" maxlength="10" class="numbersOnly stu_require" type="text" data-error="Please enter your mobile munber">
										<div class="error errorTxt12"></div>
									</div>

									<div class="input-field col s12">
										<label for="phone">Phone</label>
										<input id="phone" maxlength="10" name="phone" class="numbersOnly" type="text">
									</div>

									<div class="input-field col s12">
										<label for="cemail">Email *</label>
										<input id="cemail" type="text" name="cemail" class="email stu_require" maxlength="100" data-error="Please enter your email">
										<div class="error errorTxt13"></div>
									</div>
									
									<div class="input-field col s12">
										<label for="whether transport required">Whether School transport Required</label>
										<input id="whether transport required" type="text" name="whether transport required" class="whether transport required" maxlength="100" ">
										<div class="error errorTxt13"></div>
									</div>

									
									<h4 class="header2 title">Previous School</h4>
									

									<div class="input-field col s12">
										<textarea id="pre_school" name="pre_school" class="materialize-textarea validate"></textarea>
										<label for="pre_school"> Name of previous school / pre-school / Nursury / Cre'che </label>
									</div>
										
									
									<div class="col s12">
										<label for="genter_select">Period of Stay</label>
										</div>
									
									<div id="prd_from_date" class="input-field col s12">
										<label for="from">From </label>
										<input id="prd_from" name="from" type="text" maxlength="50">
									</div>
									
									<div id="prd_to_date" class="input-field col s12">
										<label for="to">To </label>
										<input id="prd_to" name="to" type="text" maxlength="50">
									</div>
								    
									<div class="input-field col s12">
										<label for="add another school">Add Another School </label>
										<input id="add another school" name="add another school" type="text" maxlength="50">
									</div>

									
									
									<h4 class="header2 title">Medical Details</h4>
								
									<div class="input-field col s12">
										<label for="stu_blood_grp">Blood group </label>
										<input id="stu_blood_grp" name="stu_blood_grp" type="text" maxlength="50" class="med_require" data-error="Please enter your blood group">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>

									<div class="input-field col s12">
										<label for="stu_height">Height </label>
										<input id="stu_height" name="stu_height" type="text" maxlength="50" class="numbersOnly med_require" data-error="Please enter your height">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="stu_weight">Weight </label>
										<input id="stu_weight" name="stu_weight" type="text" maxlength="50" class="numbersOnly med_require" data-error="Please enter your weight">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<div class="input-field col s12">
										<label for="allergy"> Allergy </label>
										<input id="allergy" name="allergy" type="text" maxlength="50">
									</div>
									
									
										<div class="input-field col s12">
										<label for="disease">Disease</label>
										<input id="disease" name="disease" type="text" maxlength="50">
									</div>
									
									<div class="input-field col s12">
										<label for="stu_med_cnd">Special Child </label>
										<input id="stu_med_cnd" name="stu_med_cnd" type="text" maxlength="50" class="textOnly med_require" data-error="Please enter your medical condition">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									
									<div class="input-field col s12">
										<label for="stu_med_cnd">Physically challenged</label>
										<input id="stu_med_cnd" name="stu_med_cnd" type="text" maxlength="50" class="textOnly med_require" data-error="Please enter your medical condition">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									
									
									<div class="input-field col s12">
										<label for="stu_med_cnd">Medical condition </label>
										<input id="stu_med_cnd" name="stu_med_cnd" type="text" maxlength="50" class="textOnly med_require" data-error="Please enter your medical condition">
										<div class="input-field">
											<div class="error"></div>
										</div>
									</div>
									
									<h4 class="header2 title">Transport Details</h4>
								

									<div class="input-field col s12">
										<label for="Transport Name">Transport Name</label>
										<input id="Transport Name" name="Transport Name" type="text" maxlength="50" >
										
									</div>
									
									
									<div class="input-field col s12">
										<label for="boarding point">Boarding point</label>
										<input id="boarding point" name="boarding point" type="text" maxlength="50" >
										
									</div>
                                    
									<h4 class="header2 title">Hostel Details</h4>
								

									<div class="input-field col s12">
										<label for="hostel_name">Hostel Name </label>
										<input id="hostel_name" name="hostel_name" type="text" maxlength="50">
									</div>
									<div class="input-field col s12">
										<label for="hostel_no">Hostel Room Number </label>
										<input id="hostel_no" name="hostel_no" type="text" maxlength="50">
									</div>

									
									
									

									
								</div>
							</div>
							
									
									<div class="col s12">
										<!-- <button class="btn waves-effect waves-light right" id="Next1" type="button" name="action">Save
										<i class=""></i>
										</button> -->
										
										<a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="stuEdit()" name="stuEdit" id="stuEdit">Submit</a>
									</div>
									
									
							 
							</div>
						</form>
					</div>
					</div>
				</div>
            </div>
		</div>
    </section>
      <!-- END CONTENT -->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->



  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright � 2016 <a class="grey-text text-lighten-4" href="http://www.orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://orectiq.com/">Orectiq</a></span>
        </div>
    </div>
  </footer>
  <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism
    <script type="text/javascript" src="js/prism/prism.js"></script>-->
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
	<script type="text/javascript" src="../../js/admission-custom-script.js"></script>
	
		<script> var ctxPath = "<%=request.getContextPath() %>";</script>
		
	
	<script>
	alert("enter script");
	referenceValue();
	
	
	
	
	
	function referenceValue()
	{
		
		var studid= $('#student_id').val();
		alert("ref. value enter 1  "+studid);
		 $.ajax({
 			  type: "GET",
 			  url: ctxPath+'/app/student/getltrreferencedataAll.do?',
 			  data: {"student_id":studid},
 			  dataType: 'json',
 			}).done(function( responseJson )
 					{	
 						//alert("retrieve 4");
 						//alert("value 1 "+responseJson.student_id);
 					    loadDataIntoForm(responseJson);
 					 
 				});
	} 
	function  loadDataIntoForm(responseJson)
	{
		 $('#fname').val(responseJson.fname);
		 $('#lname').val(responseJson.lname);
		 $('#photo').val(responseJson.photo);
		 $('#dob').val(responseJson.dob);
		 $('#bplace').val(responseJson.bplace);
		 $('#nationality').val(responseJson.nationality);
		 $('#mtongue').val(responseJson.mtongue);
		 $('#cgender').val(responseJson.cgender);
		 $('#religion').val(responseJson.religion);
		 $('#category').val(responseJson.category);
		 $('#address1').val(responseJson.address1);
		 $('#address2').val(responseJson.address2);
		 
	}
	
	function stuEdit()
	{
		 alert("Students Edit entered111111111111111");
		 var dt=new Date();
		 //alert("Join date and admid  "+$("#join").val()+" and "+$("#adm_id").val());
		 $.ajax({
	        type: 'POST',
	        url: ctxPath+'/app/student/stuEdit.do?',
	        //data: JSON.stringify(data),
	        data: {"fname":$("#fname").val(),"lname":$("#lname").val(),"photo":$("#photo").val(),"dob":$("#dob").val(),
				 "bplace":$("#bplace").val(),"nationality":$("#nationality").val(),"mtongue":$("#mtongue").val(),"cgender":$("#gender").val(),
				 "religion":$("#religion").val(),"category":$("#category").val(),"address1":$("#address1").val(),"address2":$("#address2").val(),"student_id":$("#student_id").val()},
	        //data: {"adm_id":$("#adm_id").val(),"Join_date":$("#join").val(),"Hostel_name":$("#hostel_name").val(),"Hostel_Room_no":$("#hostel_no").val()},
	        success: function(data) 
	        {
	       	 if(data=='success')
	       	 {
	       		Materialize.toast("Success",4000);	 
	       	 }
	       	 else 
	       	 {
	       		 Materialize.toast(data,4000);	 
	       	 }  
	        }
		 });
	}
	
	</script>
	
	
	
</body>

</html>