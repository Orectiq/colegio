
/*==========================================================
	NUMBERS ONLY FOR VALIDATION
============================================================*/

	$('.numbersOnly').keyup(function () {  
		this.value = this.value.replace(/[^0-9\.]/g,''); 
	});
	
/*==========================================================
	TEXT ONLY FOR VALIDATION
============================================================*/

	$('.textOnly').keyup(function () {  
		this.value = this.value.replace(/[^a-zA-Z]/g,''); 
	});
	
/*==========================================================
	EMAIL ONLY FOR VALIDATION
============================================================*/

	function ValidateEmail(email) {
		// Validate email format
		var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		return expr.test(email);
	};
	
/*==========================================================
	EMAIL ONLY FOR VALIDATION
============================================================*/

	/*==========================================================
		STUDENT TAB CHECK
	============================================================*/

	function library_tab() {
		
	////////// TEXT BOX //////////
				
		$(":text.lib_require").each(function(){
			if($(this).val() === "")
			{
				$(this).parent().find('.error').text($(this).attr('data-error'));
				$(this).parent().addClass('err_yes');
			}
			else{
				$(this).parent().find('.error').text('');
				$(this).parent().removeClass('err_yes');
			}
		});
	
	////////// RADIO BUTTON //////////	
	
		$(":radio").each(function(){
			if($(this).hasClass('lib_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text($(':radio[name=' + group + ']').attr('data-error'));
					$(':radio[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':radio[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});
		
	////////// CHECK BOX //////////	
	
		$(":checkbox").each(function(){
			if($(this).hasClass('lib_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text($(':checkbox[name=' + group + ']').attr('data-error'));
					$(':checkbox[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':checkbox[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});	
		
	////////// SELECT OPTION //////////	
	
		$("select").each(function(){
			if($(this).hasClass('lib_require'))
			{
				var group = $(this).attr('name');
				if ($('select[name='+ group +']').val() == 0) {
					$(this).parent().find('.error').text($(this).attr('data-error'));
					$(this).parent().addClass('err_yes');
				}
				else{
					$(this).parent().find('.error').text('');
					$(this).parent().removeClass('err_yes');
				}
			}
		});	
		
	////////// EMAIL //////////	
		
		if (!ValidateEmail($("#cemail").val())) {
            $('#cemail').parent().find('.error').text($('#cemail').attr('data-error'));
			$('#cemail').parent().addClass('err_yes');
            return false;
        }
        else {
			 $('#cemail').parent().find('.error').text('');
			 $('#cemail').parent().removeClass('err_yes');
        }
		
	};
	
	
	
	
	

/*==========================================================
	FORM VALIDATION
============================================================*/

	//////////////////// NEXT1 VALIDATION CHECKING ////////////////////	

	$('#Next1').click( function() { 
	
		library_tab();
		
		if ($('.err_yes').length > 0) { 
			return false;
		}
		else{
			$('#ad-tab2 a').click();
			return true;
		}
	
	});
	

////////script for parrent tab second + button///////
function parent_tab2() {
		////////// TEXT BOX //////////
				
		$(":text.validate").each(function(){
			if($(this).val() === "")
			{
				$(this).parent().find('.error').text($(this).attr('data-error'));
				$(this).parent().addClass('err_yes');
			}
			else{
					
				$(this).parent().find('.error').text('');
				$(this).parent().removeClass('err_yes');
			}
		});
		
		
		
		
		
		
		
	
	

	
	};
	
	 $("student_prof_pop_01").click(function(){
        alert("The paragraph was clicked.");
    });
	
	
 
