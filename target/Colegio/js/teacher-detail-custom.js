
/*==========================================================
	NUMBERS ONLY FOR VALIDATION
============================================================*/

	$('.numbersOnly').keyup(function () {  
		this.value = this.value.replace(/[^0-9\.]/g,''); 
	});
	
/*==========================================================
	TEXT ONLY FOR VALIDATION
============================================================*/

	$('.textOnly').keyup(function () {  
		this.value = this.value.replace(/[^a-zA-Z]/g,''); 
	});
	
/*==========================================================
	EMAIL ONLY FOR VALIDATION
============================================================*/

	function ValidateEmail(email) {
		// Validate email format
		var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		return expr.test(email);
	};
	
/*==========================================================
	EMAIL ONLY FOR VALIDATION
============================================================*/

	/*==========================================================
		TEACHER DETAIL TAB CHECK
	============================================================*/

	function teacher_detail_tab() {
		
	////////// TEXT BOX //////////
				
		$(":text.tch_dt_require").each(function(){
			if($(this).val() === "")
			{
				$(this).parent().find('.error').text($(this).attr('data-error'));
				$(this).parent().addClass('err_yes');
			}
			else{
				$(this).parent().find('.error').text('');
				$(this).parent().removeClass('err_yes');
			}
		});
	
	////////// RADIO BUTTON //////////	
	
		$(":radio").each(function(){
			if($(this).hasClass('tch_dt_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text($(':radio[name=' + group + ']').attr('data-error'));
					$(':radio[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':radio[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});
		
	////////// CHECK BOX //////////	
	
		$(":checkbox").each(function(){
			if($(this).hasClass('tch_dt_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text($(':checkbox[name=' + group + ']').attr('data-error'));
					$(':checkbox[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':checkbox[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});	
		
	////////// SELECT OPTION //////////	
	
		$("select").each(function(){
			if($(this).hasClass('tch_dt_require'))
			{
				var group = $(this).attr('name');
				if ($('select[name='+ group +']').val() == 0) {
					$(this).parent().find('.error').text($(this).attr('data-error'));
					$(this).parent().addClass('err_yes');
				}
				else{
					$(this).parent().find('.error').text('');
					$(this).parent().removeClass('err_yes');
				}
			}
		});	
		
	////////// EMAIL //////////	
		
		if (!ValidateEmail($("#cemail").val())) {
            $('#cemail').parent().find('.error').text($('#cemail').attr('data-error'));
			$('#cemail').parent().addClass('err_yes');
        }
        else {
			 $('#cemail').parent().find('.error').text('');
			 $('#cemail').parent().removeClass('err_yes');
        }
		
		//alert($('.err_yes').length);
	};
	
	/*==========================================================
		PARENT TAB CHECK
	============================================================*/
	
	function teacher_qualification_tab() {
		
	////////// TEXT BOX //////////
				
		$(":text.tch_qft_require").each(function(){
			if($(this).val() === "")
			{
				$(this).parent().find('.error').text($(this).attr('data-error'));
				$(this).parent().addClass('err_yes');
			}
			else{
					
				$(this).parent().find('.error').text('');
				$(this).parent().removeClass('err_yes');
			}
		});
			
	////////// RADIO BUTTON //////////	
	
		$(":radio").each(function(){
			if($(this).hasClass('tch_qft_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text($(':radio[name=' + group + ']').attr('data-error'));
					$(':radio[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':radio[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':radio[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});
		
	////////// CHECK BOX //////////	
	
		$(":checkbox").each(function(){
			if($(this).hasClass('tch_qft_require'))
			{
				var group = $(this).attr('name')
				if ($('[name=' + group + ']:checked').length == 0) {
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text($(':checkbox[name=' + group + ']').attr('data-error'));
					$(':checkbox[name=' + group + ']').parent().next('.input-field').addClass('err_yes');
				}
				else
				{
					$(':checkbox[name=' + group + ']').parent().next('.input-field').find('.error').text('');
					$(':checkbox[name=' + group + ']').parent().next('.input-field').removeClass('err_yes');
				}
			}
		});	
		
	////////// SELECT OPTION //////////	
	
		$("select").each(function(){
			if($(this).hasClass('tch_qft_require'))
			{
				var group = $(this).attr('name');
				if ($('select[name='+ group +']').val() == 0) {
					$(this).parent().find('.error').text($(this).attr('data-error'));
					$(this).parent().addClass('err_yes');
				}
				else{
					$(this).parent().find('.error').text('');
					$(this).parent().removeClass('err_yes');
				}
			}
		});

	};
	
/*==========================================================
	ONCLICK DATE LABEL ANIMATION
============================================================*/

	$( document ).ready(function() {
				
		$('#dob, #dob_root').click(function(){
			$(this).parent().find('label').addClass('active');
			if($('#dob').val() == ""){
				$(this).parent().find('label').removeClass('active');
				$('#dob.ad_date').css("border-color","#9e9e9e");
				$('#dob.ad_date').css("box-shadow", "none");
			}
		});
		
/*==========================================================
	ON CLICK VALIDATION 
============================================================*/

	////////// QUALIFICATION ADD CLICK //////////
	
	var count = 2;
	$('#add_tch_dtl').click(function(event){
		
		event.preventDefault();
		$(".qualification_bx .tch_qft_require").each(function(){
			if($(this).val() === "")
			{
				$(this).parent().find('.error').text($(this).attr('data-error'));
				$(this).parent().addClass('err_yes');
			}
			else{
					
				$(this).parent().find('.error').text('');
				$(this).parent().removeClass('err_yes');
			}
		});
		
		if($('.qualification_bx .err_yes').length <= 0){
			if(count<=3){
				$('.qualification_bx').append('<div class=\"qua_bx qf_b'+count+'\"><h4 class="header2 title">Qualification Details '+count+'</h4><div class="input-field col s12"><label for="tch_school_name'+count+'">Name Of School / College/ University</label><input id="tch_school_name'+count+'" name="tch_school_name'+count+'" type="text" maxlength="50" class="textOnly tch_qft_require" data-error="Please enter the name of school, college, university"><div class="input-field"><div class="error"></div></div></div><div class="input-field col s12"><label for="tch_course_name'+count+'"> Course / HSC / SSLC/ UG / PG </label><input id="tch_course_name'+count+'" name="tch_course_name'+count+'" type="text" maxlength="50" class="textOnly tch_qft_require" data-error="Please enter your course name"><div class="input-field"><div class="error"></div></div></div><div class="input-field col s12"><label for="tch_per'+count+'"> Percentange </label><input id="tch_per'+count+'" name="tch_per'+count+'" type="text" maxlength="50" class="textOnly tch_qft_require" data-error="Please enter your percentage"><div class="input-field"><div class="error"></div></div></div></div>');
				count++;
			}
		}
		
	});
	
	////////// QUALIFICATION SUBRACT CLICK //////////
	
	$('#sub_tch_dtl').click(function(event){
		event.preventDefault();
		if(count>2){
			$('.qualification_bx .qua_bx').last().remove();
			count--;
		}
	});
	
	////////// EXPERIENCE ADD CLICK //////////
	var exp_count = 2;
	$('#add_exp_dtl').click(function(event){
		event.preventDefault();
			$(".experience_bx .tch_exp_require").each(function(){
				if($(this).val() === "")
				{
					$(this).parent().find('.error').text($(this).attr('data-error'));
					$(this).parent().addClass('err_yes1');
				}
				else{
						
					$(this).parent().find('.error').text('');
					$(this).parent().removeClass('err_yes1');
				}
			});

			if($('.experience_bx .err_yes1').length <= 0){
				if(exp_count<=3){
					$('.experience_bx').append('<div class=\"exp_bx ep_b'+exp_count+'\"><h4 class="header2 title">Experience Detail '+exp_count+'</h4><div class="input-field col s12"><label for="tch_prev_emp'+exp_count+'"> Previous Employer </label><input id="tch_prev_emp'+exp_count+'" name="tch_prev_emp'+exp_count+'" type="text" maxlength="50" class="textOnly tch_exp_require" data-error="Please enter your guardian name"><div class="input-field"><div class="error"></div></div></div><div class="input-field col s12"><label for="tch_prev_emp_desg'+exp_count+'">Designation</label><input id="tch_prev_emp_desg'+exp_count+'" name="tch_prev_emp_desg'+exp_count+'" type="text" maxlength="50" class="textOnly tch_exp_require" data-error="Please enter your occupation"><div class="input-field"><div class="error"></div></div></div><div class="input-field col s12"><label for="tch_year_of_exp'+exp_count+'">Year Of Experience</label><input id="tch_year_of_exp'+exp_count+'" name="tch_year_of_exp'+exp_count+'" type="text" maxlength="50" class="textOnly tch_exp_require" data-error="Please enter your designation"><div class="input-field"><div class="error"></div></div></div></div>');
					exp_count++;
				}
			}
	});
	
	////////// EXPERIENCE SUBRACT CLICK //////////
	
	$('#sub_exp_dtl').click(function(event){
		event.preventDefault();
		if(exp_count>2){
			$('.experience_bx .exp_bx').last().remove();
			exp_count--;
		}
	});

	//////////////////// NEXT1 VALIDATION CHECKING ////////////////////	

	$('#Next1').click( function() { 
	
		teacher_detail_tab();
		
		if ($('.err_yes').length > 0) { 
			return false;
		}
		else{
			$('#tch-tab2 a').click();
			$("html, body").animate({ scrollTop: 0 }, 600);
			return true;
		}
	
	});
	
	//////////////////// NEXT2 VALIDATION CHECKING ////////////////////	
	
	$('#Next2').click( function() { 

		teacher_qualification_tab();
		
		if ($('.err_yes').length > 0) { 
			return false;
		}
		else{
			$('#tch-tab3 a').click();
			$("html, body").animate({ scrollTop: 0 }, 600);
			return true;
		}
	});
	
	$('#Prev2').click( function() { 
		$('#tch-tab1 a').click();
		$("html, body").animate({ scrollTop: 0 }, 600);
		return true;
	});
	
});

	
 



