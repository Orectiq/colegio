
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Student List - Colegio - A Orectiq Product</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="#"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                        <%
                        	String uname=(String)session.getAttribute("userName2");
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= uname %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=dashboard/dashboard' class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                <%-- <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Student</a>
                </li> --%>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/StudentMain' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=student/Parent_Student' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Stu-Par List</a>
                        		    </li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_Main' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/Parent_List' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> ParentsList</a>
                        		    </li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=parents/ParentsView' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                 	
               <%--  <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers</a>
                </li>
                
                
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers List</a>
                </li> --%>
                <li class="bold"><a href="../calender/app-calendar.html" class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Class_Main' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/AddActivity' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/Activity_List' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Teachers </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/Teachers' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=teachers/TeachersMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teachers List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                 		 <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Non-Teaching Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteaching' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teaching Staff</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachinglist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NonTeachingStaff/Nonteachingview' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach View</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                    
                                    <li><a href="">Teachers Attendance</a>
                                    </li>
                                    <li><a href="">Attendance Reports</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examlist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Accounts</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="table-basic.html">Fees Allocation</a>
                                    </li>
                                    <li><a href="table-data.html">Payment Details</a>
                                    </li>
                                    <li><a href="table-jsgrid.html">Teachers Salary</a>
                                    </li>
                                    <li><a href="table-editable.html">Other Expenses</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                       
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Library </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/LibraryMain' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library Details </a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/Libraryreqbook' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library Request Details </a>
                                    </li>
                                    
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/librarybook' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=library/libraryreqbooklist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Library Request List</a>
                                    </li>
                                </ul>
                            </div>
                 	    </li>
                 	    
                 	    
                 	    
                         <li class="bold"><a href='<%=request.getContextPath()%>/app/book/loadpage?d=attendance/Attendance' class="waves-effect waves-cyan"><i class="mdi-editor-insert-comment"></i> Attendance </a>
                        </li>
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/book/loadpage?d=transport/transport_table' class="waves-effect waves-cyan"><i class="mdi-editor-insert-comment"></i> Transport </a>
                        </li>
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/Dormitory/loadpage?d=Dormitory/Dormitory_list' class="waves-effect waves-cyan"><i class="mdi-social-pages"></i> Dormitories</a>
                        </li>
                        <!-- <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-action-shopping-cart"></i> Notice Board</a>
                        </li> -->
                       <!--  <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Media Center</a>
                        </li> -->
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Notice Board </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/addevent' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/addnews' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/events_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=NoticeBoard/news_table' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Media Center </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Addmedia' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Media</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Addalbum' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Album</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Medialist' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Media List</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Media/Albumlist' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Album List</a>
                        		    </li>
                   				</ul>
                     		</div>
                 	    </li>
                        
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=admission/Admission' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-action-account-circle"></i> Settings</a>
                            <div class="collapsible-body">
                                <ul>     
                                    <li><a href="user-profile-page.html">User Profile</a>
                                    </li>                                   
                                    <li><a href="user-login.html">Login</a>
                                    </li>                                        
                                    <li><a href="user-register.html">Register</a>
                                    </li>
                                    <li><a href="user-forgot-password.html">Forgot Password</a>
                                    </li>
                                    <li><a href="user-lock-screen.html">Lock Screen</a>
                                    </li>                                        
                                    <li><a href="user-session-timeout.html">Session Timeout</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Media</h5>
                <ol class="breadcrumbs">
                    <li><a href="index.html">Dashboard</a></li>
                    <li class="active">Media</li>
					<li class="active">Album List</li>
					
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
 	<div class="container">
		<!--<div id="floatThead" class="cyan lighten-4">-->
	
 <!--Preselecting a tab-->
          <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
                  <div class="col s12">
                    <ul class="tabs tab-demo-active z-depth-1 cyan">
                      
						<li class="tab col s6"><a class="white-text waves-effect waves-light active" href="#">Album</a>
						</li>					  
<!-- 						<li class="tab col s6"><a class="white-text waves-effect waves-light active" href="#">Class</a>
						</li> -->
				   </ul>
                  </div>
                  <div class="col s12">
                    <div id="stu_list_all" class="col s12  cyan lighten-4">
                     		<!--DataTables example-->
				            <div id="#student-datatables_1">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">
				                <div class="col s12 m12 l12">
				                
				                <form  id="slick-login" method="post">
				                
				                
				                				                 <table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0">

				                    <thead>
				                        <tr>
										  
				                           
				                            <th>S.NO</th>
				                            <th>Album Title</th>
				                            <th>Album Description</th>
				                            <th>Album Image</th>
				                            <th>Operations</th>
				                        </tr>
				                    </thead>
				                 
				                    <tfoot>
				                        <tr>
				                            <th>S.NO</th>
				                            <th>Album Title</th>
				                            <th>Album Description</th>
				                            <th>Album Image</th>
				                            <th>Operations</th>				                        </tr>
				                    </tfoot>
				                 
				                    <tbody>

				                    </tbody>
				                  </table>
				                  <input id="albumid" name="albumid" type="text" size="45" tabindex="5" disabled  />
				                  
				                  
				                  
				                  
				                  </form>
								  <div id="add-activity" class="section">
								  <div class="row">
									<div class="col s12 m8 l9 right">
									  
									</div>
								  </div>
								</div>
								  
				                </div>
				              </div>
				            </div>  
				            <br>
				            <div class="divider"></div> 
				            <!--DataTables example Row grouping-->
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
	
      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START RIGHT SIDEBAR NAV-->
      <aside id="right-sidebar-nav">
        <ul id="chat-out" class="side-nav rightside-navigation">
            <li class="li-hover">
            <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
            <div id="right-search" class="row">
                <form class="col s12">
                    <div class="input-field">
                        <i class="mdi-action-search prefix"></i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Search</label>
                    </div>
                </form>
            </div>
            </li>
            <li class="li-hover">
                <ul class="chat-collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                    <div class="collapsible-body recent-activity">
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">just now</a>
                                <p>Jim Doe Purchased new equipments for zonal office.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Yesterday</a>
                                <p>Your Next flight for USA will be on 15th August 2015.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Last Week</a>
                                <p>Jessy Jay open a new store at S.G Road.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                    <div class="collapsible-body sales-repoart">
                        <div class="sales-repoart-list  chat-out-list row">
                            <div class="col s8">Target Salse</div>
                            <div class="col s4"><span id="sales-line-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Payment Due</div>
                            <div class="col s4"><span id="sales-bar-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Delivery</div>
                            <div class="col s4"><span id="sales-line-2"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Progress</div>
                            <div class="col s4"><span id="sales-bar-2"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                    <div class="collapsible-body favorite-associates">
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Eileen Sideways</p>
                                <p class="place">Los Angeles, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Zaham Sindil</p>
                                <p class="place">San Francisco, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Renov Leongal</p>
                                <p class="place">Cebu City, Philippines</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Weno Carasbong</p>
                                <p>Tokyo, Japan</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Nusja Nawancali</p>
                                <p class="place">Bangkok, Thailand</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </li>
        </ul>
      </aside>
      <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
	 </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->
  <div id="modal4" class="modal modal-fixed-footer green white-text stu_list_pop_box">
            <div class="modal-content">
            <div id="bordered-table">
            
              <h4 class="header left">Student Name</h4>
              <h4 class="header right">Academic Year</h4>
              <hr style="clear:both;"/>
              
              <div class="row">
              
                <div class="col s12 m4 l3">
                  <img title="Avatar" src="../../resources/images/avatar.jpg">
                </div>
                
                <div class="col s12 m8 l9">
                  <table class="bordered">
                        <th class="stu_details center" data-field="id">Student Details</th>
                        <tr>
                        <td>Class:</td>
                      </tr>
                      <tr>
                        <td>Roll Number:</td>
                      </tr>
                </table>
                </div>
                </div>
                      
                      <div class="col s12 m4 l3">
                  <table class="bordered">
                        <tr>
                        <td>Birth Date:</td>
                      </tr>
                      <tr>
                        <td>Gender:</td>
                      </tr>
                      <tr>
                        <td>Nationality:</td>
                      </tr>
                      <tr>
                        <td>Religion:</td>
                      </tr>
                      <tr>
                        <td>Community:</td>
                      </tr>
                      <tr>
                        <td>Place of Birth:</td>
                      </tr>
                      <tr>
                        <td>Mother Tongue:</td>
                      </tr>
                      <tr>
                        <td>Date of Joining:</td>
                      </tr>
                      <tr>
                        <td>Permanent Address:</td>
                      <tr>
                        <td>Communication Address:</td>
                      </tr>
                      <tr>
                        <td>Phone Number:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Previous Schools</th>
                      <tr>
                        <td>School Name:</td>
                      </tr>
                      <tr>
                        <td>Years:</td>
                      </tr>
                      <tr>
                        <td>Standard:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Parents Details</th>
                      <tr>
                        <td>Father Name:</td>
                      </tr>
                      <tr>
                        <td>Mother Name:</td>
                      </tr>
                      <tr>
                        <td>Father Occupation:</td>
                      </tr>
                      <tr>
                        <td>Father Office Address:</td>
                      <tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                        <td>Mother Occupation:</td>
                      </tr>
                      <tr>
                        <td>Mother office Address</td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Guardian Details</th>
                      <tr>
                        <td>Guardian Name:</td>
                      </tr>
                      <tr>
                        <td>Guardian Relation:</td>
                      </tr>
                      <tr>
                        <td>Guardian Address:</td>
                      <tr>
                        <td>Guardian Occupation:</td>
                      </tr>
                      <tr>
                        <td>Guardian Office address:</td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                      </tr>
                      </tr>
                      <th class="stu_details center" data-field="id">Medical Details</th>
                      <tr>
                        <td>Blood Group:</td>
                      </tr>
                      <tr>
                        <td>Height:</td>
                      </tr>
                      <tr>
                        <td>Weight:</td>
                      </tr>
                      <tr>
                        <td>Allergy:</td>
                      </tr>
                      <tr>
                        <td>Diseases:</td>
                      </tr>
                      <tr>
                        <td>Special Child:</td>
                      </tr>
                      <tr>
                        <td>Physically Challenged:</td>
                      </tr>
                      <tr>
                        <td>Medical Condition:</td>
                      </tr>
                      <th class="stu_details center" data-field="id">Transport Detials</th>
                      <tr>
                        <td>Transport Name:</td>
                      </tr>
                      <tr>
                        <td>Boarding Point:</td>
                        </tr>
                        <th class="stu_details center" data-field="id">Hostel Detials</th>
                      <tr>
                        <td>Hostel Name:</td>
                      </tr>
                      <tr>
                        <td>Room Number:</td>
                        </tr>
                  </table>
                </div>
            </div>
            </div>
            </div>
            </div>


  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright ɠ2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
    <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });

        });
    </script>
    
    <script>
    
    retrieve();
    function retrieve()
      {
      	$.ajax({
      			  type: "GET",
      			  url: ctxPath+'/app/media/getAlbumData.do?',
      			  dataType: 'json',
      			}).done(function( responseJson )
      					{	
       						loadDataIntoTable(responseJson);
      				});
      }

    function loadDataIntoTable(responseJson)
      {
      	 var tblHtml = "";
      	 $.each(responseJson.albumServiceVOList, function(index, val)
      	{
      		var album_title=val.album_Title;
      		var album_description=val.album_Description;
      		var album_image="../../resources/images/media/"+val.album_Image;
      		//alert("Image  "+album_image);
      		
      		$('#albumid').val(album_title);
      		
      		tblHtml += '<tr><td style="text-align: left">'+(index+1)+'</td><td style="text-align: left">'+album_title+'</td><td style="text-align: left">'+album_description+'</td>'+
   		 '<td style="text-align: left"><img src='+album_image+' alt="" class="circle responsive-img valign profile-image" height="100" width="100"></td>'+
 			 '<td><a class="btn-floating blue btn tooltipped" data-position="top" data-delay="0" data-tooltip="Edit" onClick="libraryEdit(id,name)" name="techid" id='+album_title+'><i class="large mdi-editor-border-color"></i></a>'+
 			 '<a class="btn-floating red darken-1 btn tooltipped" data-position="top" data-delay="0" data-tooltip="Delete" onClick="libraryDelete(id)" name="techdel" id='+album_title+'><i class="large mdi-action-delete"></i></a></td> </tr>';// 		
      	            
      	 });    
    	
      	 $("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
      
      }


    </script>
    
</body>

</html>