<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Messages</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
<%
                        	String sid=(String)request.getAttribute("stuid");
                    		System.out.println("Value passssssssssssssssssssssss 2222222222  "+sid);
                        
                        %>
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"  data-activates="profile-dropdown"><%= sid %><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Students</p>
                    </div>
                </div>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/markList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Marks</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/attendanceList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Attendance</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/examScheList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Exam Schedule</a>
                </li>
                
                 <li class="bold"><a href='<%=request.getContextPath()%>/app/student/feesStuList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Fees Details</a>
                </li>
                
               <li class="bold"><a href='<%=request.getContextPath()%>/app/Homework/homeworkStuList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Home Work</a>
                </li>
                
                 <li class="bold"><a href='<%=request.getContextPath()%>/app/book/libStureqbooklist?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Book Request</a>
                </li>
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/absenceRequest?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Absence Request</a>
                </li>
                
                 <li class="bold"><a href='<%=request.getContextPath()%>/app/student/stuAssignment?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Assignment </a>
                </li>
                        
  </form>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
                    <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
               <center> <h5 class="breadcrumbs-title">Fees Details</h5> </center>
               </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

        <!--start container-->
			             
       						                               		   		  
                        <div class="container">
						
						

			<div class="row">			
			<div class="input-field col m1">
			<label for="challanid">Challan Id : </label>
				</div>		
          
             <div class="input-field col m5">		  
			<input id="challanid" name="challanid" type="text" size="45" tabindex="5" disabled  />
			 <input id="sid" name="sid" type="hidden" size="45" tabindex="5" disabled value=<%= sid %> />
			 <input id="std" name="std" type="hidden" size="45" tabindex="5" disabled />
             </div>	
					
                          <div class="input-field col m1">
			 <label for="Ename">Date: </label>
			 </div>	
				

             <div class="input-field col m5">				
			<input id="adate" name="adate" type="text" size="45" tabindex="5" disabled  />

										
			</div>									
					

            					
			</div>


						

</br>		
<div class="row">			
			<div class="input-field col m1">
			<label for="Academic_year">Academic Year : </label>
				</div>

             <div class="input-field col m5">				
			<input id="Academic_year" name="Academic_year" type="text" size="45" tabindex="5" disabled  />
</div>	
			
			
			              <div class="input-field col m1">
			 <label for="Fees_name">Fees Name: </label>
				</div>		

              <div class="input-field col m5">				
			<input id="Fees_name" name="Fees_name" type="text" size="45" tabindex="5" disabled  />

				</div>						
											
										
			</div>


						
</br>									


						
			<div class="row">			
			<div class="input-field col m1">
			<label for="ldate">Last Date : </label>
				</div>

             <div class="input-field col m5">				
			<input id="ldate" name="ldate" type="text" size="45" tabindex="5" disabled  />
</div>	
			
			
			              <div class="input-field col m1">
			 <label for="Sclass">Sclass: </label>
				</div>		

              <div class="input-field col m5">				
			<input id="Sclass" name="Sclass" type="text" size="45" tabindex="5" disabled  />

				</div>						
											
										
			</div>
						</div>
	</br></br>
	 

  <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
                  <div class="col s12">
                   
                  </div>
				  
                 <div class="col s12"> 
<div id="stu_list_all" class="col s12  cyan lighten-4"> 
 
		 <div class="container">
          <div class="section">	
		  <h5 class="breadcrumbs-title">Fees Details</h5> 
<div class="row"> 
<!--DataTables example--> 
<div id="#student-datatables_1"> 
<!--<h4 class="header">DataTables example</h4>--> 
<div class="row"> 
<div class="col s12 m12 l12"> 
<table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0"> 
<thead> 
<tr> 
<th>S NO.</th>
<th>fees Details</th>
<th>Amount</th> 



</tr> 
</thead> 


<tbody> 

</tbody> 
</table> 
</div> 
</div> 
</div> 
<br> 
<div class="divider"></div> 
          
</div>					
	
</div>
</div>	
	</div>					


	</div>
</div>					

  <div class="row right">
			<div class="input-field col m2">
			<label for="Total">Total: </label>
				</div>

            	<div class="input-field col m8">		
			<input id="Total" name="Total" type="text" size="45" tabindex="5" disabled  />
</div>		
	</div>


</section>

</div>
</div>
<!--start container-->
				     
       
			  
  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
    
    <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

		
		
	$('.datepicker').pickadate({ 
selectMonths: true, // Creates a dropdown to control month 
selectYears: 15 // Creates a dropdown of 15 years to control year 
}); 


		
		if ($("#test21:checked").length > 0) {
    $("#test24, #test27, #test30, #test33").prop('checked', true);
} else if ($("#DRequest-1:checked").length > 0) {
    $("#DRequest-2, #DRequest-3, #DRequest-4").prop('checked', true);
}

        }); 
		
    </script>
	
<script>
retrieve();

function retrieve()
{
	//getltrreferencedataAll
	var sid=$('#sid').val();
	////alert("enter retrieve "+sid);
  	$.ajax({
  			  type: "GET",
  			  url: ctxPath+'/app/student/getltrreferencedataAll.do?',
  			 data: {"student_id":sid},
  			  dataType: 'json',
  			}).done(function( responseJson )
  					{	
  						////alert("before load  "+responseJson.sclass);
  						//loadDataIntoTable(responseJson);
  						////alert("after load  "+responseJson.sclass);
  						$('#std').val(responseJson.sclass);
  						getstufees();
  				});
}

function getstufees()
  {
	var scla=$('#std').val();
	//var sid='1st-Std';
	////alert("enter retrieve "+scla);
  	$.ajax({
  			  type: "GET",
  			  url: ctxPath+'/app/Echallan/getFee.do?',
  			 data: {"sclass":scla},
  			  dataType: 'json',
  			}).done(function( responseJson )
  					{	
  						////alert("before load  "+responseJson.challan_id);
  						loadDataIntoTable(responseJson);
  						////alert("after load  "+responseJson.challan_id);
  				});
  }

function loadDataIntoTable(responseJson)
  {
	////alert("enter fee student list TABLE ");
  	 var tblHtml = "";
  	 $.each(responseJson.echallanServiceVOList, function(index, val1)
  	{
  		////alert("for each");
			index++;
      		var challan_id=val1.challan_id;
      		var theDate1=val1.theDate;
      		var fee_name=val1.fee_name;
      		var accyear=val1.acc_year;
      		var total=val1.total;
      		var sclass=val1.sclass;
      		var due=val1.datepicker;
      		var msg=val1.sendMessage;
      		
      		var fee1=val1.fee_Break1;
      		var amt1=val1.amount1;
      		var fee2=val1.fee_Break2;
      		var amt2=val1.amount2;
      		var fee3=val1.fee_Break3;
      		var amt3=val1.amount3;
      		var tot=val1.total;
      		
			
  		$('#challanid').val(challan_id);
  		$('#adate').val(theDate1);
  		$('#Academic_year').val(accyear);
  		$('#Fees_name').val(fee_name);
  		$('#ldate').val(due);
  		$('#Sclass').val(sclass);
  		$('#Total').val(tot);


  		 tblHtml += '<tr><td style="text-align: left">'+(index++)+'</a></td><td style="text-align: left">'+fee1+'</td><td style="text-align: left">'+amt1+'</td></tr>'+
	     
  	    '<tr><td style="text-align: left">'+(index++)+'</a></td><td style="text-align: left">'+fee2+'</td><td style="text-align: left">'+amt2+'</td></tr>'+
  	   '<tr><td style="text-align: left">'+(index++)+'</a></td><td style="text-align: left">'+fee3+'</td><td style="text-align: left">'+amt3+'</td></tr>';
  	      	            
  	      	 });   
  	    	
  	      	 $("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
  
  }

  
 </script>


	
									
			
</body>

</html>
													