<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Messages</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../resources/css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

<%
                        	String sid=(String)request.getAttribute("techId");
                    		System.out.println("Value passssssssssssssssssssssss 2222222222  "+sid);
                        
                        %>

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                          <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"  data-activates="profile-dropdown"><%= sid %><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Teacher</p>
                    </div>
                </div>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/homeworkList?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Homework</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/Homework/homeworkListStatus?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Homework Status</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/teacher/absenceRequest?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Absence Request</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/teacher/absenceStuApproval?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Students Leave Approval</a>
                        </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/assignment/assignment?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Assignment</a>
                        </li>        
                 <li class="bold"><a href='<%=request.getContextPath()%>/app/assignment/assignmentApproval?id=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Assignment Approval</a>
                        </li>       
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
                    <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Assignment Approval</h5>
               </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

        <!--start container-->
			             
       			<input id="techuname" name="techuname" type="hidden" size="45" tabindex="5" disabled value=<%= sid %> />		                               		   		  
    			<input id="tid" name="tid" type="text" size="45" tabindex="5" disabled />
    
	 <div class="row">
                
                  <div class="input-field col s6">
                    <label for="SClass1"></label>
										<select class="browser-default stu_require" id="SClass1" name="SClass1" data-error="Please select your Class" onChange="getSection()">
											<option value="NIL">Please select your Class</option>
											
										</select>
										
                  </div>
                
             
               
                  <div class="input-field col s6">
                    <label for="Section1"></label>
										<select class="browser-default stu_require" id="Section1" name="Section1" data-error="Please select your Section">
											<option value="NIL">Please select your Section</option>
											
										</select>
										
                  </div>
               
             
	  </div>
	  
	  <div class="row">
	  		<div class="input-field col s6">
	  		<label for="Start_Date"></label>Assignment Date<input
				id="adate" name="adate" type="text"
				maxlength="10" class="datepicker"
				data-error="Please pick your Date">
				<div class="error errorTxt3"></div>	
	  		</div>
	  
	  		<div class="input-field col s6">
	  			 <div id="submit-button" class="section">
                  <div class="row">
                    <div class="col s12 m12 l12">
                       <a href="javascript:void(0)" class="btn waves-effect waves-light" onClick="showList()" name="assign" id=""assign"">View Assignment</a> 
                    </div>
                  </div>
                </div>
	  		</div>
	  </div>
	
	<div class="row">
	  		<div class="input-field col s6">
	  		<label for="Start_Date"></label>Subject
	  		<input	id="sub" name="sub" type="text" maxlength="10" data-error="Please" disabled>
				<div class="error errorTxt3"></div>	
	  		</div>
	  
	  		<div class="input-field col s6">
	  			<label for="Start_Date"></label>Description
	  		<input	id="des" name="des" type="text" maxlength="10" data-error="Please" disabled>
				<div class="error errorTxt3"></div>	
	  		</div>
	  </div>
	  
	  <div id="submit-button" class="section">
                  <div class="row">
                    <div class="col s12 m12 l12">
                       <a href="javascript:void(0)" class="btn waves-effect waves-light" onClick="showApproval()" name="search" id="Search">Assignment Approval</a> 
                    </div>
                  </div>
                </div>

  








  <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
                  <div class="col s12">
                   
                  </div>
                 <div class="col s12"> 
<div id="stu_list_all" class="col s12  cyan lighten-4"> 
 
		 <div class="container">
          <div class="section">	
<div class="row"> 
<!--DataTables example--> 
<div id="#student-datatables_1"> 
<!--<h4 class="header">DataTables example</h4>--> 
<div class="row"> 
<div class="col s12 m12 l12"> 
<form  id="slick-login" method="post">

<table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0"> 
<thead> 
<tr> 
<th>S.NO</th>
<th>Student ID</th>
<th>Student Name</th> 
<th>Completed</th> 
<th>Pending</th> 
<th>Rework</th>
<th>Mark</th> 


</tr> 
</thead> 

<tfoot> 

</tfoot> 
<tbody> 

</tbody> 
</table> 
<input id="assid" name="assid" type="text" size="45" tabindex="5" disabled  />
</form>
</div> 
</div> 
</div> 
<br> 
<div class="divider"></div> 
        <center><a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="assignmentInsert()" name="stu" id="stu">Save Assignment</a></center>              
	</div>
</div>					
	
</div>
</div>	
	</div>					


	</div>
</div>					


</div>
</div>

</section>
</div>
</div>

<!--start container-->
				     
       
			  
  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright Â© 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
     <!-- <script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/jquery-ui.min.js"></script> 
    -->
  <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    
     <script type="text/javascript">
	
	

        /*Show entries on click hide*/

        $(document).ready(function(){
        	var d = new Date();
        	var month = d.getMonth() + 1;
        	var day = d.getDate();
        	var year = d.getYear();
        	var today = (day<10?'0':'')+ day + '/' +(month<10?'0':'')+ month + '/' + year;
        	$('#dt').val(d);
        	
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });                                                                               

        });
		
	/* $('.datepicker').pickadate({ 
selectMonths: true, // Creates a dropdown to control month 
selectYears: 15 // Creates a dropdown of 15 years to control year 
}); 
 */

function selectCheckBox() 
{ 
  var e = document.getElementById("ddlView"); 
var optionSelIndex = e.options[e.selectedIndex].value; 
var optionSelectedText = e.options[e.selectedIndex].text; 
if (optionSelIndex == 0) { 
//////alert("Please select a Class"); 
} 

var e = document.getElementById("ddlView1"); 
var optionSelIndex = e.options[e.selectedIndex].value; 
var optionSelectedText = e.options[e.selectedIndex].text; 
if (optionSelIndex == 0) { 
//////alert("Please select a Section"); 
} 
if ($("#test21:checked").length > 0) {
    $("#test25, #test29, #test33, #test37").prop('checked', true);
} else if ($("#DRequest-1:checked").length > 0) {
    $("#DRequest-2, #DRequest-3, #DRequest-4").prop('checked', true);
}
}




	</script>	
	
	 
 
<script>
	
retrieve();
function retrieve()
{
	var id=$('#techuname').val();
	//////alert("Teacher Name in retrieve  "+id);
	
	$.ajax({
			  type: "GET",
			  url: ctxPath+'/app/teacher/getId.do?',
			  data: {"uname":id},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						////////alert("before   "+responseJson.uname);
						loadDataIntoID(responseJson);
						////////alert("after   "+responseJson.uname);
				});
	
	$.ajax({
		  type: "GET",
		  url: ctxPath+'/app/subject/getSClass.do?',
		  dataType: 'json',
		}).done(function( responseJson )
				{	
					loadDataIntoClass(responseJson);
			});
}

function loadDataIntoClass(responseJson)
{
	var tblHtml = "";
	var sclass;
	 var sel = $("#SClass1").empty();
	 $.each(responseJson.sectionServiceVOList, function(index, val)
	 {
		sclass=val.sclass;
		sel.append('<option value="' + sclass + '">' + sclass + '</option>');
	 }); 
}

function getSection()
{
	 var sclass=$("#SClass1").val();
	   $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/subject/getSection.do?',
			  data: {"sclass":sclass},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
						loadDataIntoSection(responseJson);
				});
}
function loadDataIntoSection(responseJson)
{
	var tblHtml = "";
	var sec;
	 var sel = $("#Section1").empty();
	 $.each(responseJson.sectionServiceVOList, function(index, val)
	 {
		sec=val.section;
		sel.append('<option value="' + sec + '">' + sec + '</option>');
	 }); 
}



function loadDataIntoID(responseJson)
{
	   var id=responseJson.tid;
	   //////alert("tercher id  "+id);
	   $('#tid').val(id);
}

	
 
    
    
    function showList()
	{	
    	$('#sub').val("");
  		$('#des').val("");
    	
		var id= $('#tid').val();
		var sclass= $('#SClass1').val();
		var sec= $('#Section1').val();
		var hdate=$('#adate').val();
		var date = new Date(hdate); 

        var day = date.getDate(); 
        var month = date.getMonth() + 1; 
        var year = date.getFullYear(); 

        if (month < 10) month = "0" + month; 
        if (day < 10) day = "0" + day; 

        var adate = year + "-" + month + "-" + day; 
		
		
		//////alert("id  "+id+"  class "+sclass+"  sec  "+sec+"  ADate  "+adate);
		
		
		 $.ajax({
 			  type: "GET",
 			  url: ctxPath+'/app/assignment/getAssignmentTeacher.do?',
 			 data: {"Sclass":sclass,"Section":sec,"Tid":id,"HDate":adate},
 			  dataType: 'json',
			}).done(function( responseJson )
					{	
						//////alert("before "+responseJson.sclass+" and sec  "+responseJson.section);
					    loadDataIntoTable(responseJson);
				});	
	}
	
	
	function loadDataIntoTable(responseJson)
	  {
		 $.each(responseJson.assignmentServiceVOList, function(index, val)
				  	{
				  		
				 	  		//////alert("enter each  ");
				  		
				  		//var hdate=val.hdate;
				  		var subject=val.subject;
				  		var title=val.title;
				  		var desp=val.description;
				  		$('#sub').val(subject);
				  		$('#des').val(desp);
				  	});
		
	  }
		
	 function showApproval()
	  {
		 var sclass= $('#SClass1').val();
			var sec= $('#Section1').val();
			
		  $.ajax({
				  type: "GET",
				  url: ctxPath+'/app/student/getAttendance.do?',
				 data: {"sclass":sclass,"section":sec},
				  dataType: 'json',
			}).done(function( responseJson )
					{	
						//////alert("before "+responseJson.sclass+" and sec  "+responseJson.section);
					    loadDataIntoTable1(responseJson);
				});	
	  }
	 
	function loadDataIntoTable1(responseJson)
	  {
		//////alert("enter table  ");
	  	 var tblHtml = "";
	  	 var i=1;
	  	 var j=1;
	  	 var grop=1;
	  	 $.each(responseJson.studentServiceVOList, function(index, val)
	  	{
	  		//////alert("enter each  "+val.fname);
	  		
	  		var fname1=val.fname;
	  		var sid=val.student_id;
	  		//var j="test"+i;
	  		var k="group"+grop;
	  		//$('#stuid').val(sid);
			////////alert("value  "+i+" group  "+k);
	  		////////alert("in load student id  "+sid);
	  		////////alert("fname  "+fname);
	  			
	  			////////alert("before  "+i);
	  			 tblHtml += '<tr>'+
	  			
	  			'<td style="text-align: left">'+(index+1)+'</td><td style="text-align: left" id='+sid+' onClick="onClick="getStudentsListAll(id)">'+sid+'</a></td> <td style="text-align: left" id='+fname1+' onClick="onClick="getStudentsListAll(id)">'+fname1+'</a></td> '+ 
	  			 '<td><input name='+k+' type=radio id=test'+(i=parseInt(i)+1)+' value="Completed" onClick="one(name,id,value)"  /><label for=test'+i+'></label></td>'+  
				 '<td><input name='+k+' type="radio" id=test'+(i=parseInt(i)+1)+' value="Pending" onClick="one(name,id,value)" /><label for=test'+i+'></label></td>'+ 
				 '<td><input name='+k+' type="radio" id=test'+(i=parseInt(i)+1)+' value="Rework" onClick="one(name,id,value)" /><label for=test'+i+'></label></td>'+ 
				 '<td><input name='+k+' type="text" id=Mark'+(j++)+' onClick="two(id)" /><label for=Mark'+j+'></label></td></tr>';
				 
				// //////alert("after  "+i);
				//$("#test21, #test25, #test29, #test33, #test37").prop('checked', true);
				 //$("#radio_1").prop("checked", true)
	  			  
	  			
	  			//i=parseInt(i)+4;   
	  			grop=parseInt(grop)+1;
	  			
		 });   
	  	 $("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
	  	
	  	 
	  }
	var status,markid;
	 function one(obj,id,val)
	{
		////////alert("Result  "+obj+" ID "+id+" Value "+val);
		if(val=="Completed")
		{
			status=val;
			markid=id;
		}
		else
			{
				status="NIL";
			}
	}  
	 function two(id)
	 {
		// //////alert("enter Mark  "+id+" status "+status);
		 var mid='#'+id
		 if(status=="Completed")
			 {
			 
			 ////////alert("enter Completed Mark  "+mid);
		 		$(mid).val(100);
			 }
		 else
			 {
			 $(mid).val(0);
			 }
	 }
     
    function assignmentInsert()	
	{
		//////alert("enter nextload");
    	
		var id= $('#tid').val();
		var sclass= $('#SClass1').val();
		var sec= $('#Section1').val();
		var hdate=$('#adate').val();
		var date = new Date(hdate); 

        var day = date.getDate(); 
        var month = date.getMonth() + 1; 
        var year = date.getFullYear(); 

        if (month < 10) month = "0" + month; 
        if (day < 10) day = "0" + day; 

        var adate = year + "-" + month + "-" + day; 
        var sub=$('#sub').val();
        var desp=$('#des').val();
		
	 
		
		var MyRows = $('#stu-db-data-table_1').find('tbody').find('tr');
		//////alert("No of Rows  "+MyRows.length);
		
		for (var i = 0,j=1; i < MyRows.length; i++)
		{
			
			var sid = $(MyRows[i]).find('td:eq(1)').html();
			var fname = $(MyRows[i]).find('td:eq(2)').html();
		
			var value = $('input[name=group'+(i+1)+']:checked').val();
			var mark = $('#Mark'+(j++)).val();
				
			////alert("Student id  "+sid+"  SName  "+fname+" Status  "+value+" Mark "+mark);
			  $.ajax({
	 			  type: "POST",
	 			  url: ctxPath+'/app/assignment/insertAssignApproval.do?',
	 			 data: {"assign_date":adate ,"teacher_id":id,"student_id":sid,"student_name":fname,
	 				"sclass":sclass ,"section":sec,"subject":sub,"description":desp,
	 				"status":value ,"mark":mark,
					},
	 			  dataType: 'json',
	 			 success: function(data) 
	 	        {
	 	       	 if(data=='success')
	 	       	 {
	 	       		Materialize.toast("Success",4000);	 
	 	       	 }
	 	       	 else 
	 	       	 {
	 	       		 Materialize.toast(data,4000);	 
	 	       	 }  
	 	        }
	 		 }); 
		} 
	   
	}
 </script>


	
									
			
</body>

</html>
													