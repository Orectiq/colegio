<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Teacher List - Colegio - A Orectiq Product</title>
<!-- Global stylesheets -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900"
	rel="stylesheet" type="text/css">
<link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet"
	type="text/css">
<link href="../../assets/css/bootstrap.css" rel="stylesheet"
	type="text/css">
<link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
<link href="../../assets/css/components.css" rel="stylesheet"
	type="text/css">
<link href="../../assets/css/colors.css" rel="stylesheet"
	type="text/css">
<!-- /global stylesheets -->

<!-- Core JS files -->
<script type="text/javascript"
	src="../../assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="../../assets/js/core/app.js"></script>

<script type="text/javascript"
	src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/ui/nicescroll.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/pickers/anytime.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/pickers/pickadate/picker.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/pickers/pickadate/picker.time.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/pickers/pickadate/legacy.js"></script>
<script type="text/javascript"
	src="../../assets/js/pages/picker_date.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/pages/datatables_extension_buttons_init.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/ui/ripple.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/pages/datatables_extension_scroller.js"></script>
<script type="text/javascript"
	src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
<script type="text/javascript"
	src="../../assets/js/pages/form_bootstrap_select.js"></script>
<script type="text/javascript"
	src="../../assets/js/pages/layout_fixed_custom.js"></script>
	<script type="text/javascript"
	 src="../../assets/js/plugins/forms/editable/editable.min.js"></script>
<script type="text/javascript"
 src="../../assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript"
 src="../../assets/js/pages/form_editable.js"></script>
	

<!-- /theme JS files -->
  <style>
 	.class_active{display:block!important;opacity:1!important;}
  	.student_class_section{display:none;}
  	.student_class_section_sbt{display:none;}
	.student_prof_pop {
		background: transparent none repeat scroll 0 0 !important;
		box-shadow: 0 0;
		color: #000000;
		padding: 0;
	}
	.student_prof_pop:hover{
		box-shadow: 0 0;
	}
	.stu_list_pop_box h4.header{
		color:#ffffff;
	}
	
	input {
    background-color: #71da71;
    border: 0px;
    height: 20px;
    width: 160px;
    color: white;
}
</style>
</head>

<body>
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                           <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        String sname=(String)request.getAttribute("sname");
                        System.out.println("Value passssssssssssssssssssssss  "+sid+" and "+sname);
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                    
                     <li class="bold"><a href='<%=request.getContextPath()%>/app/all/schoolName?&sname=<%= sname %>&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Adminssion Form</a>
                     							
                        </li> 															
                    
                   <!--  <li class="bold"><a href="javascript:void(0)" class="btn waves-effect waves-light right"  name="final" id="final"><i class="mdi-image-image"></i> Adminssion Form</a>
                        </li> -->	
                    
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Students </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/stuList?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Students List</a>
                        			</li>
                   				</ul>
                     		</div>
                 	</li>
                    
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Parents </a>
                			<div class="collapsible-body">
                                <ul>
                        			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/parent?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents</a>
                        			</li>
                         			<li class="bold"><a href='<%=request.getContextPath()%>/app/all/parentView?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Parents View</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	</li>
                
                <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=calender/app-calendar' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Class </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/all/activityMain?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Activity List</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                        
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Staff </a>
                			<div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/all/teacherList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Teaching Staff List</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/all/NonTeacherList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Non-Teach Staff</a>
                                    </li>
                                </ul>
                            </div>
                 		</li>
                 		
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Attendance </a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href='<%=request.getContextPath()%>/app/all/attendance?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Students Attendance</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Exam </a>
                            <div class="collapsible-body">
                                <ul>
                                    <%-- <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Examid' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam Details</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=exam/Exam' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam ID List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/all/Examlist?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Exam List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Marks </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href='<%=request.getContextPath()%>/app/all/MarkSheet?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Class wise Marks</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Finance</a>
                            <div class="collapsible-body">
                                <ul>
                                   <%--  <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_EChallan'>E-Challan</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeeDraft'>Fee Draft</a>
                                    </li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Accounting/Accounting_FeesList'>Fee List</a>
                                    </li> --%>
                                    <li><a href='<%=request.getContextPath()%>/app/all/FeeAllot?&admid=<%= sid %>' class="waves-effect waves-cyan">Mainpage</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                 	    
                        
                        
                         <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> News Feed </a>
                			<div class="collapsible-body">
                                <ul>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/all/events_table?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Events List</a>
                        		    </li>
                        		    <li class="bold"><a href='<%=request.getContextPath()%>/app/all/news_table?&admid=<%= sid %>' class=" waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> News List</a>
                        		    </li>
                        		    
                   				</ul>
                     		</div>
                 	    </li>
                        
                       
                        
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=SMS/SmsOrEmail' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> SMS/Email</a>
                        </li>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Teacher List</h5>
                <ol class="breadcrumbs">
                    <li><a href="index.html">Dashboard</a></li>
                    <li class="active">Teacher List</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

        <!--start container-->
        <div class="container">
          <div class="section">
            
            

 <!--Preselecting a tab-->
          <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
                  <div class="col s12">
                    <ul class="tabs tab-demo-active z-depth-1 cyan">
                      <li class="tab col s3"><a class="white-text waves-effect waves-light active" href="#stu_list_all"></a>
                      </li>
                      <!-- <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#stu_list_Sec_1">Section 1</a>
                      </li>
                      <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#stu_list_Sec_2">Section 2</a>
                      </li> -->
                    </ul>
                  </div>
                  <div class="col s12">
                    <div id="stu_list_all" class="col s12  cyan lighten-4">
                     		<!--DataTables example-->
				            <div id="#student-datatables_1">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">
				              
				              
				              <form  id="slick-login" method="get">
				               <div id="submit-button" class="section">
                  					<div class="row right">
                    					<div class="col s12 m12 l12">
                      						<a href='<%=request.getContextPath()%>/app/all/teacherAdd?&admid=<%= sid %>' class="btn waves-effect waves-light"  name="search" id="Search">Add Teacher</a> 
                    					</div>
                    					
                  					</div>
                  					
                  					<div class="row right">
                  					<div class="col s12 m12 l12">
                      						<a href='<%=request.getContextPath()%>/app/all/teacherSubject?&admid=<%= sid %>' class="btn waves-effect waves-light"  name="search" id="Search">Teacher-Subject</a> 
                    					</div>
                    				</div>
                  					
                				</div>
				              
				              
				              
				                <div class="col s12 m12 l12">
				              
				                  <table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0">
				                    <thead>
				                  
				                        <tr>
				                            <th>Teacher ID</th>
				                            <th>Photo</th>
				                            <th>First Name</th>
				                            <th>Last Name</th>
				                            <th>Date of Birth</th>
				                            <th>Operations</th>
				                        </tr>
				                      
				                    </thead>
				                 
				                 
				                    <tfoot>
				                        <tr>
				                            <th>Teacher ID</th>
				                            <th>Photo</th>
				                            <th>First Name</th>
				                            <th>Last Name</th>
				                            <th>Date of Birth</th>
				                            <th>Operations</th>
				                        </tr>
				                    </tfoot>
				                 
				                    <tbody>
				                      
				                    </tbody>
				                  </table>
				                  
				                  <input id="stuid" name="stuid" type="text" size="45" tabindex="5" disabled  />
				                  </form>
				                  
				                </div>
				              </div>
				            </div>  
				            <br>
				            <div class="divider"></div> 
				            <!--DataTables example Row grouping-->
                    </div>
                    <div id="stu_list_Sec_1" class="col s12  cyan lighten-4">
                     	  <!--DataTables example-->
				            <div id="#student-datatables_2">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">
				                <div class="col s12 m12 l12">
				                 
				                </div>
				              </div>
				            </div> 
				            <br>
				            <div class="divider"></div> 
				            <!--DataTables example Row grouping--> 
                    </div>
                    <div id="stu_list_Sec_2" class="col s12  cyan lighten-4">
                      		 <!--DataTables example-->
				            <div id="#student-datatables_3">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">
				                <div class="col s12 m12 l12">
				                  
				                </div>
				              </div>
				            </div> 
				            <br>
				            <div class="divider"></div> 
				            <!--DataTables example Row grouping--> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          </div>
          <!-- Floating Action Button -->
            <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                <a class="btn-floating btn-large">
                  <i class="mdi-action-stars"></i>
                </a>
                <ul>
                  <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                  <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                  <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                  <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
                </ul>
            </div>
            <!-- Floating Action Button -->
        </div>
        <!--end container-->

      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START RIGHT SIDEBAR NAV-->
      <aside id="right-sidebar-nav">
        <ul id="chat-out" class="side-nav rightside-navigation">
            <li class="li-hover">
            <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
            <div id="right-search" class="row">
                <form class="col s12">
                    <div class="input-field">
                        <i class="mdi-action-search prefix"></i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Search</label>
                    </div>
                </form>
            </div>
            </li>
            <li class="li-hover">
                <ul class="chat-collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                    <div class="collapsible-body recent-activity">
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">just now</a>
                                <p>Jim Doe Purchased new equipments for zonal office.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Yesterday</a>
                                <p>Your Next flight for USA will be on 15th August 2015.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Last Week</a>
                                <p>Jessy Jay open a new store at S.G Road.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                    <div class="collapsible-body sales-repoart">
                        <div class="sales-repoart-list  chat-out-list row">
                            <div class="col s8">Target Salse</div>
                            <div class="col s4"><span id="sales-line-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Payment Due</div>
                            <div class="col s4"><span id="sales-bar-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Delivery</div>
                            <div class="col s4"><span id="sales-line-2"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Progress</div>
                            <div class="col s4"><span id="sales-bar-2"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                    <div class="collapsible-body favorite-associates">
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Eileen Sideways</p>
                                <p class="place">Los Angeles, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Zaham Sindil</p>
                                <p class="place">San Francisco, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Renov Leongal</p>
                                <p class="place">Cebu City, Philippines</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Weno Carasbong</p>
                                <p>Tokyo, Japan</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Nusja Nawancali</p>
                                <p class="place">Bangkok, Thailand</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </li>
        </ul>
      </aside>
      <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->
  <div id="modal3"  class="modal modal-fixed-footer green white-text stu_list_pop_box">
            <div class="modal-content">
            <div id="bordered-table">
            
              <h4 class="header left">Teacher Name : <input id="tname" name="tname" type="text" size="45" tabindex="5" disabled  /></h4>
              <h4 class="header right">Academic Year : 2016</h4>
              <hr style="clear:both;"/>
              
              <div class="row">
              
                <div class="col s12 m4 l3">
                  <img title="Avatar" src="../../resources/images/avatar.jpg">
                </div>
                
                <div class="col s12 m8 l9">
                  <table class="bordered">
                        <th class="stu_details center" data-field="id">Teacher Details</th>
                        
                      <!-- <tr>
                        <td>Class:</td>
                        <td><input id="cl" name="cl" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>  -->
                      
                      <tr>
                        <td>Employee ID</td>
                        <td><input id="roll" name="roll" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                </table>
                </div>
                </div>
                      
                      <div class="col s12 m4 l3">
                  <table class="bordered">
                        <tr>
                        <td>Birth Date:</td>
                        <td><input id="dob" name="dob" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Gender:</td>
                        <td><input id="gender" name="gender" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Nationality:</td>
                        <td><input id="nation" name="nation" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                     <!--  <tr>
                        <td>Religion:</td>
                        <td><input id="relig" name="relig" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr> 
                      <tr>
                        <td>Community:</td>
                        <td><input id="cate" name="cate" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Place of Birth:</td>
                        <td><input id="pbirth" name="pbirth" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Mother Tongue:</td>
                        <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Date of Joining:</td>
                         <td><input id="doj" name="doj" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>  -->
                      <tr>
                        <td>Permanent Address:</td>
                         <td><input id="addr1" name="addr1" type="text" size="45" tabindex="5" disabled  /> </td>
                      <tr>
                        <td>Communication Address:</td>
                         <td><input id="addr2" name="addr2" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Phone Number:</td>
                         <td><input id="sphone" name="sphone" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <th class="stu_details center" data-field="id">Previous Schools</th>
                     
                     <!--  <tr>
                        <td>School Name:</td>
                         <td><input id="schoolName" name="schoolName" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Years:</td>
                         <td><input id="Year" name="Year" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Standard:</td>
                        <td><input id="std" name="std" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <th class="stu_details center" data-field="id">Parents Details</th>
                      <tr>
                        <td>Father Name:</td>
                         <td><input id="parFName" name="parFName" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Mother Name:</td>
                         <td><input id="parMName" name="parMName" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Father Occupation:</td>
                         <td><input id="parOccu" name="parOccu" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Father Office Address:</td>
                         <td><input id="parFAddr1" name="parFAddr1" type="text" size="45" tabindex="5" disabled  /> </td>
                      <tr>
                      <tr>
                        <td>Office Phone Number:</td>
                         <td><input id="parOffPhone" name="parOffPhone" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                        <td>Mother Occupation:</td>
                         <td><input id="parMOccu" name="parMOccu" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Mother office Address</td>
                         <td><input id="parMAdd2" name="parMAdd2" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <th class="stu_details center" data-field="id">Guardian Details</th>
                      <tr>
                        <td>Guardian Name:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Guardian Relation:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Guardian Address:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      <tr>
                        <td>Guardian Occupation:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Guardian Office address:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Office Phone Number:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      </tr>
                      <th class="stu_details center" data-field="id">Medical Details</th>
                      <tr>
                        <td>Blood Group:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Height:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Weight:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Allergy:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Diseases:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Special Child:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Physically Challenged:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Medical Condition:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <th class="stu_details center" data-field="id">Transport Detials</th>
                      <tr>
                        <td>Transport Name:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Boarding Point:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                        </tr>
                        <th class="stu_details center" data-field="id">Hostel Detials</th>
                      <tr>
                        <td>Hostel Name:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                      </tr>
                      <tr>
                        <td>Room Number:</td>
                         <td><input id="mtongue" name="mtongue" type="text" size="45" tabindex="5" disabled  /> </td>
                        </tr>  -->
                  </table>
                </div>
            </div>
            </div>
            </div>
            </div>


  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->

</body>

</html>

    <!-- ================================================
    Scripts
    ================================================ -->
     <script type="text/javascript">
    	////alert("retrieve 1");
    	//retrieve();
    </script>
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="../../js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/data-tables/data-tables-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!-- custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
     <script> var ctxPath = "<%=request.getContextPath() %>";</script>
     <script type="text/javascript" src="Student.js"></script>
    <script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function()
        		{
        	$("#sname").css("font-size",20 + "px");
        	retrieve();
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });
			
			
			
			

        });
    </script>
    
    
    <script type="text/javascript">
 //  //alert("retrieve 2");
 
 function teacherDelete(obj)
	{
		// //alert("Student Delete entered111111111111111");
		 ////alert("Object  "+obj);
		 var dt=new Date();
	          $.ajax({
		        type: 'POST',
		        url: ctxPath+'/app/teacher/deleteTeacher.do?',
		        //data: JSON.stringify(data),
		        data: {"tid":obj},
		        success: function(data) 
		        {
		       	 	if(data=='success')
		       	 	{
			       		Materialize.toast("Success",4000);
			       	 	retrieve();
			       	 }
			       	 else 
			       	 {
			       		 Materialize.toast(data,4000);	 
			       	 }  
		        }
			 });
	         
	         
	}	  		
 	
 
 
 	function teacherEdit(obj,name)
		{

 		   ////alert("Student Edit List entered111111111111111  value   "+obj+" and name   "+name);
 		   
 		   document.getElementById('slick-login').action = ctxPath+'/app/teacher/editList';
		   document.getElementById('slick-login').submit();
		}	  		
 	
 	
  function retrieve()
  {
  	$.ajax({
  			  type: "GET",
  			  url: ctxPath+'/app/teacher/getltrreferencedata.do?',
  			  dataType: 'json',
  			}).done(function( responseJson )
  					{	
   						loadDataIntoTable(responseJson);
  				});
  }
  
  
  
  function loadDataIntoTable(responseJson)
  {
  	 var tblHtml = "";
  	 $.each(responseJson.teacherServiceVOList, function(index, val)
  	{
  		var fname=val.fname;
  		var tid=val.tid;
  		var sphoto="../../resources/images/teachers/"+val.photo;
  		//$('#stuid').val(sid);
  		////alert("in load teacher id  "+tid);
  		////alert("fname  "+fname);
  			 tblHtml += '<tr><td style="text-align: left" id='+tid+' onClick="getTeachersListAll(id)">'+val.tid+'</a></td><td style="text-align: left" value='+sphoto+' ondblclick="getStudentsListAll(this)"><img src='+sphoto+' alt="" class="circle responsive-img valign profile-image" height="100" width="100"></td><td style="text-align: left" value='+fname+' ondblclick="getTeachersListAll(this)">'+val.fname+'</td><td style="text-align: left">'+val.lname+'</td><td style="text-align: left">'+val.dob+'</td>'+
  			 '<td><a class="btn-floating blue btn tooltipped" data-position="top" data-delay="0" data-tooltip="Edit" onClick="teacherEdit(id,name)" name="techid" id='+tid+'><i class="large mdi-editor-border-color"></i></a>'+
  			 '<a class="btn-floating red darken-1 btn tooltipped" data-position="top" data-delay="0" data-tooltip="Delete" onClick="teacherDelete(id)" name="techdel" id='+tid+'><i class="large mdi-action-delete"></i></a></td> </tr>';// 
  	            //'<td style="text-align: center">'+responseJson.dob+'</td><td style="text-align: right">'+responseJson.bplace+'</td><td style="text-align: right">'+responseJson.nationality+'</td><td style="text-align: right">'+responseJson.mtongue+'</td><td style="text-align: right">'+val.cgender+'</td></tr>';
  	            
  	 });   
	
  	 $("#stu-db-data-table_1 tbody").html(tblHtml);//getRows(10);
  
  }
 
  function getTeachersListAll(obj)
  {
	  ////alert("Object  "+obj);
	 $.ajax({
  			  type: "GET",
  			  url: ctxPath+'/app/teacher/getltrreferencedataAll.do?',
  			  data: {"tid":obj},
  			  dataType: 'json',
  			}).done(function( responseJson )
  					{	
  					    loadDataIntoPopup(responseJson);
  				});
  }
 
  function loadDataIntoPopup(responseJson)
  {
	  ////alert("POPUP window   "+responseJson.fname);
	  $("#modal3").show();
	  $('#tname').val(responseJson.fname);
	  $('#roll').val(responseJson.tid);
	  $('#dob').val(responseJson.dob);
	  $('#gender').val(responseJson.cgender);
	  $('#nation').val(responseJson.nationality);
	  $('#addr1').val(responseJson.address1);
	  $('#addr2').val(responseJson.address2);
	  
	  
	 // $('#schoolName').val(responseJson.fname);
	 // $('#AYear').val(responseJson.fname);
	  /*$('#parFName').val(responseJson.fname);
	  $('#parMName').val(responseJson.fname);
	  $('#parFAddr1').val(responseJson.fname);
	  $('#parOffPhone').val(responseJson.fname);
	  $('#parMOccu').val(responseJson.fname);
	  $('#parMAdd2').val(responseJson.fname);*/
	  
  }
  
  </script>  
  

    
