<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Calendar | Colegio - A Orectiq Product</title>

  <!-- Favicons-->
  <link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="../../resources/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../resources/css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="../../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../js/plugins/fullcalendar/css/fullcalendar.min.css" type="text/css" rel="stylesheet" media="screen,projection">
</head>

<body>
  <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START HEADER -->
  <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Colegio</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- translation-button -->
                    <ul id="translation-dropdown" class="dropdown-content">
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
  </header>
  <!-- END HEADER -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

<%
                        	String tusername=(String)request.getAttribute("techId");
                    		//System.out.println("Value passssssssssssssssssssssss  "+uname1);
                        
                        %>

  <!-- START MAIN -->
  <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                            </li>
                            <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                            </li>
                            <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                          <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"  data-activates="profile-dropdown"><%= tusername %><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Teacher</p>
                    </div>
                </div>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/student/homeworkList?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Homework</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/Homework/homeworkListStatus?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Homework Status</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/teacher/absenceRequest?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Absence Request</a>
                </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/teacher/absenceStuApproval?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Students Leave Approval</a>
                        </li>
                <li class="bold"><a href='<%=request.getContextPath()%>/app/assignment/assignment?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Assignment</a>
                        </li>        
                 <li class="bold"><a href='<%=request.getContextPath()%>/app/assignment/assignmentApproval?id=<%= tusername %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Assignment Approval</a>
                        </li>       
                
               
            </aside>
      <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Teacher</h5>
                <ol class="breadcrumbs">
                    <li><a href="../../index.html">Dashboard</a></li>
                    <li class="active">Teacher</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        

        <!--start container-->
        <div class="container">
		
		<form class="col m6">
<div class="card-panel">		  
<div class="row">	

<div class="input-field col m3">
<img id="photo1" class="materialboxed" name="photo1" alt=""  width="235" height="235">
</div>


<div class="input-field col m1">
    <label for="Teacher_Id">Teacher Id</label>
</div>
	  
<div class="input-field col m8">
	<input id="Teacher_Id" name="Teacher_Id" type="text" size="45" tabindex="5" disabled  />
	<input id="uname" name="uname" type="hidden" size="45" tabindex="5" disabled value=<%= tusername %> />
</div>

<div class="input-field col m1">
    <label for="Name">Name</label>
</div>
	  
<div class="input-field col m3">
	<input id="Name" name="Name" type="text" size="45" tabindex="5" disabled  />
</div>

<div class="input-field col m1">
    <label for="Address">Address</label>
</div>
	  
<div class="input-field col m4">
	<input id="Address" name="Address" type="text" size="45" tabindex="5" disabled  />
</div>

<div class="input-field col m1">
    <label for="Mobile">Mobile</label>
</div>
	  
<div class="input-field col m3">
	<input id="Mobile" name="Mobile" type="text" size="45" tabindex="5" disabled  />
</div>  


<div class="input-field col m1">
    <label for="Email">Email</label>
</div>
	  
<div class="input-field col m4">
	<input id="Email" name="Email" type="text" size="45" tabindex="5" disabled  />
</div> 

</div> 



</form>
	
			
          <div class="section">
            <div class="divider"></div>
            <div id="full-calendar">              
              <div class="row">
               
                 <div class="col m6">
                  <div id='calendar'></div>
                </div>
				
				 <div class="col m4">
                  <div id='current-events'>  
<!-- <a class="waves-effect waves-light btn" onclick="loadDoc()"> Events</a>
<a class="waves-effect waves-light btn">News</a> -->

<div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">NEWS & EVENTS </span>
              
            </div>
            
          </div>
				  
				 <!--  <h4 class="header">Current Events</h4> -->
				   <div class="row">
   
      <!--  <ul class="collection">
    <li class="collection-item avatar">
      <i class="material-icons circle">Events</i>
      <span class="title">Title</span>
      <p><input id="news1" name="news1" type="text" size="5" tabindex="2" disabled  /> 
         <input id="news2" name="news2" type="text" size="5" tabindex="2" disabled  />
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">Events</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle">Events</i>
      <span class="title">Title</span>
      <p><input id="news3" name="news3" type="text" size="5" tabindex="2" disabled  /> 
         <input id="news4" name="news4" type="text" size="5" tabindex="2" disabled  />
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">Events</i></a>
    </li>

  </ul> -->
     <h4 class="header">News</h4>
         <ul class="collection">
    <li class="collection-item avatar">
     <!--  <i class="material-icons circle">Events</i> -->
      <span class="title">Title</span>
       <p><span id="blinker"> <input id="news5" name="news5" type="text" size="5" tabindex="2" style="color:red;font-family:Comic sans ms" readonly /> </span> 
       <p><span id="blinker1"> <input id="news6" name="news6" type="text" size="5" tabindex="2" style="color:red;font-family:Comic sans ms" readonly /> </span>
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">News</i></a>
    </li>
    <li class="collection-item avatar">
     <!--  <i class="material-icons circle">Events</i> -->
      <span class="title">Title</span>
      <p><input id="news7" name="news7" type="text" size="5" tabindex="2"  style="color:red;margin-left:30px;font-family:verdana" readonly  /> 
         <input id="news8" name="news8" type="text" size="5" tabindex="2"   />
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">News</i></a>
    </li>

  </ul>
  
  
  
  
	   
	   
	   
        </div>
      </div>
    </div>
        
        
        
        
              </div>
            </div>
            </div>
			
			

            <!-- Floating Action Button -->
            <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                <a class="btn-floating btn-large">
                  <i class="mdi-action-stars"></i>
                </a>
                <ul>
                  <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                  <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                  <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                  <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
                </ul>
            </div>
            <!-- Floating Action Button -->
        </div>
        <!--end container-->

      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START RIGHT SIDEBAR NAV-->
      <aside id="right-sidebar-nav">
        <ul id="chat-out" class="side-nav rightside-navigation">
            <li class="li-hover">
            <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
            <div id="right-search" class="row">
                <form class="col s12">
                    <div class="input-field">
                        <i class="mdi-action-search prefix"></i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Search</label>
                    </div>
                </form>
            </div>
            </li>
            <li class="li-hover">
                <ul class="chat-collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                    <div class="collapsible-body recent-activity">
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">just now</a>
                                <p>Jim Doe Purchased new equipments for zonal office.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Yesterday</a>
                                <p>Your Next flight for USA will be on 15th August 2015.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Last Week</a>
                                <p>Jessy Jay open a new store at S.G Road.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                    <div class="collapsible-body sales-repoart">
                        <div class="sales-repoart-list  chat-out-list row">
                            <div class="col s8">Target Salse</div>
                            <div class="col s4"><span id="sales-line-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Payment Due</div>
                            <div class="col s4"><span id="sales-bar-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Delivery</div>
                            <div class="col s4"><span id="sales-line-2"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Progress</div>
                            <div class="col s4"><span id="sales-bar-2"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                    <div class="collapsible-body favorite-associates">
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Eileen Sideways</p>
                                <p class="place">Los Angeles, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Zaham Sindil</p>
                                <p class="place">San Francisco, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Renov Leongal</p>
                                <p class="place">Cebu City, Philippines</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Weno Carasbong</p>
                                <p>Tokyo, Japan</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Nusja Nawancali</p>
                                <p class="place">Bangkok, Thailand</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </li>
        </ul>
      </aside>
      <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->



  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span>
        <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://orectiq.com.com/">Orectiq</a></span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="../../js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    
    <!-- chartist -->
    <script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   

    <!-- Calendar Script -->
    <script type="text/javascript" src="../../js/plugins/fullcalendar/lib/jquery-ui.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/fullcalendar/lib/moment.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/fullcalendar/js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/fullcalendar/fullcalendar-script.js"></script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../../js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../../js/custom-script.js"></script>
     <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    
    
    <script>
    retrieve();
    function retrieve()
    {
    	var id=$('#uname').val();
    	alert("teacher uname in retrieve  "+id);
 	     
 		 $.ajax({
 	  			  type: "GET",
 	  			  url: ctxPath+'/app/teacher/getId.do?',
 	  			  data: {"uname":id},
 	  			  dataType: 'json',
 	  			}).done(function( responseJson )
 	  					{	
 	  						alert("before   "+responseJson.mobile);
 	  						loadDataIntoID(responseJson);
 	  						//alert("after   "+responseJson.uname);
 	  				});
 		 
 		$.ajax({
 			  type: "GET",
 			  url: ctxPath+'/app/teacher/getId.do?',
 			  data: {"uname":id},
 			  dataType: 'json',
 			}).done(function( responseJson )
 					{	
 						//alert("before   "+responseJson.uname);
 						loadDataIntoID1(responseJson);
 						//alert("after   "+responseJson.uname);
 				});
 		 
 		 
   }
   
   
   
   function loadDataIntoID(responseJson)
   {
	   var id=responseJson.tid;
	   alert("tercher id  "+id);
	   loadID(id);
   }
   function loadDataIntoID1(responseJson)
   {
	   var id=responseJson.tid;
	   //alert("tercher id  "+id);
	   loadID1(id);
   }
    
   function loadID1(obj)
   {
 	  //alert("teacher id in retrieve  "+obj);
 	     //var id=$('#parid').val();
 	     //var id=obj;
 	   $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/AbsenceRequest/getLeaveData.do?',
			  data: {"Teacher_Id":obj},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
				        //alert("before id  "+responseJson.Teacher_Id);
					    loadDataIntoForm1(responseJson);
					   // alert("after id   "+responseJson.Teacher_Id);
				});
   }
   function loadDataIntoForm1(responseJson)
   {
	  // alert("Enter table  ");
   	$.each(responseJson.absenceRequestServiceVOList, function(index, val)
   	    	{
   				//alert("Enter each  ");
   	    		var sdate=val.start_Date;
   	    		var edate=val.end_Date;
   	    		var msg=sdate+" To "+edate+" Leave Granted ";
   	    		$('#news5').val(msg);
   	    	});
   	
   	 var blink_speed = 500;
   	    var t = setInterval(function () {
   	        var ele = document.getElementById('blinker');
   	        ele.style.visibility = (ele.style.visibility == 'hidden' ? '' : 'hidden');
   	    }, blink_speed);
   }
    
    
    function loadID(obj)
    {
  	  //alert("teacher id in retrieve  "+obj);
  	     //var id=$('#parid').val();
  	     //var id=obj;
  	   $.ajax({
			  type: "GET",
			  url: ctxPath+'/app/teacher/getltrreferencedataAll.do?',
			  data: {"Tid":obj},
			  dataType: 'json',
			}).done(function( responseJson )
					{	
				       // alert("before id  "+responseJson.tid);
					    loadDataIntoForm(responseJson);
					  //  alert("after id   "+responseJson.tid);
				});
    }
    
    
    
    function loadDataIntoForm(responseJson)
    {
    	
    	//alert("enter table");
  		 var tblHtml = "";
    	 $.each(responseJson.teacherServiceVOList, function(index, val1)
    	{
    		// alert("enter each");
    		var tid=val1.tid;
    		var fname=val1.fname;
    		var lname=val1.lname;
    		var tphoto="../../resources/images/teachers/"+val1.photo;
    		
    		var addr1=val1.address1;
    		var tmobile=val1.mobile;
    		var temail=val1.cemail;
    		var name=fname+lname;
    		
    		
    		$('#Teacher_Id').val(tid);	
    		$('#Name').val(name);
    		$('#Address').val(addr1);
    		$('#Mobile').val(tmobile);
    		$('#Email').val(temail);
    		
    		    		
    		$("#photo1").attr("src", tphoto);
  	
    	});
    
    }
    
    
    </script>
    
</body>

</html>