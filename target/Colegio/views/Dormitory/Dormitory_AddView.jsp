<html lang="en"><head> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<meta name="msapplication-tap-highlight" content="no"> 
<meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. "> 
<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,"> 
<title>Student Admission Form</title> 

<!-- Favicons--> 
<link rel="icon" href="../../resources/images/favicon/favicon-32x32.png" sizes="32x32"> 
<!-- Favicons--> 
<link rel="apple-touch-icon-precomposed" href="../../resources/images/favicon/apple-touch-icon-152x152.png"> 
<!-- For iPhone --> 
<meta name="msapplication-TileColor" content="#00bcd4"> 
<meta name="msapplication-TileImage" content="../../resources/images/favicon/mstile-144x144.png"> 
<!-- For Windows Phone --> 


<!-- CORE CSS--> 
<link href="../../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<link href="../../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<!-- Custome CSS-->    
<link href="../../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection"> 

<!-- INCLUDED PLUGIN CSS ON THIS PAGE --> 
<link href="../../js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<link href="../../js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<link href="../../js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection"> 
<style> 
.input-field div.error, .input-field div.error2{ 
position: relative; 
top: -1rem; 
left: 0rem; 
font-size: 0.8rem; 
color:#FF4081; 
-webkit-transform: translateY(0%); 
-ms-transform: translateY(0%); 
-o-transform: translateY(0%); 
transform: translateY(0%); 
} 
.inp2{ 
float:left; 
margin-top:0px; 
} 
.input-field div.error2{ 
top:-8px; 
} 
.input-field label.active{ 
width:100%; 
} 
h4.header2.title{ 
float:left; 
width:100%; 
} 

</style> 
</head> 

<body> 
<!-- Start Page Loading --> 
<div id="loader-wrapper"> 
<div id="loader"></div>        
<div class="loader-section section-left"></div> 
<div class="loader-section section-right"></div> 
</div> 
<!-- End Page Loading --> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START HEADER --> 
<header id="header" class="page-topbar"> 
<!-- start header nav--> 
<div class="navbar-fixed"> 
<nav class="navbar-color"> 
<div class="nav-wrapper"> 
<ul class="left">                      
<li><h1 class="logo-wrapper"><a href="../../index.html" class="brand-logo darken-1"><img src="../../resources/images/colegio.png" alt="Colegio"></a> <span class="logo-text">Materialize</span></h1></li> 
</ul> 
<div class="header-search-wrapper hide-on-med-and-down"> 
<i class="mdi-action-search"></i> 
<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"> 
</div> 
<ul class="right hide-on-med-and-down"> 
<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button" data-activates="translation-dropdown"><img src="../../resources/images/flag-icons/United-States.png" alt="USA"></a> 
</li> 
<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a> 
</li> 
<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i> 

</a> 
</li>                        
<li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a> 
</li> 
</ul> 
<!-- translation-button --> 
<ul id="translation-dropdown" class="dropdown-content"> 
</ul> 
<!-- notifications-dropdown --> 
<ul id="notifications-dropdown" class="dropdown-content"> 
<li> 
<h5>NOTIFICATIONS <span class="new badge">5</span></h5> 
</li> 
<li class="divider"></li> 
<li> 
<a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time> 
</li> 
<li> 
<a href="#!"><i class="mdi-action-stars"></i> Completed the task</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time> 
</li> 
<li> 
<a href="#!"><i class="mdi-action-settings"></i> Settings updated</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time> 
</li> 
<li> 
<a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time> 
</li> 
<li> 
<a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a> 
<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time> 
</li> 
</ul> 
</div> 
</nav> 
</div> 
<!-- end header nav--> 
</header> 
<!-- END HEADER --> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 
<%
                        //	String uname=(String)session.getAttribute("userName2");
                        
                        %>
<!-- START MAIN --> 
<div id="main"> 
<!-- START WRAPPER --> 
<div class="wrapper"> 

<!-- START LEFT SIDEBAR NAV--> 
<aside id="left-sidebar-nav"> 
<ul id="slide-out" class="side-nav fixed leftside-navigation"> 
<li class="user-details cyan darken-2"> 
<div class="row"> 
<div class="col col s4 m4 l4"> 
<img src="../../resources/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image"> 
</div> 
<div class="col col s8 m8 l8"> 
<ul id="profile-dropdown" class="dropdown-content"> 
<li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a> 
</li> 
<li><a href="#"><i class="mdi-action-settings"></i> Settings</a> 
</li> 
<li><a href="#"><i class="mdi-communication-live-help"></i> Help</a> 
</li> 
<li class="divider"></li> 
<li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a> 
</li> 
 <li><a href="http://localhost:8080/Colegio"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                         <%
                        String sid=(String)request.getAttribute("admid");
                        
                        %>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><label for="username" class="center-align"><font color=white size=5><%= sid %> </font></label><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
                
                
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
				        <li class="bold"><a href='<%=request.getContextPath()%>/app/all/DormitoryMain?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Hostel</a>
                        </li>
                        
                        
                      <li class="bold"><a href='<%=request.getContextPath()%>/app/all/TransportList?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Transport</a>
                        </li>
                        
                      <li class="bold"><a href='<%=request.getContextPath()%>/app/all/leaveapproval?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Leave Approval</a>
                        </li>
                        <li class="bold"><a href='<%=request.getContextPath()%>/app/all/StudyMaterialsAdd?&admid=<%= sid %>' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Study Materials</a>
                        </li>
                        
                        <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> HR</a>
                        </li>  
                       <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Notes</a>
                        </li>           
                            
<%--                         <li class="bold"><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Chart/PieChart' class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Pie Chart</a>
                        </li>
 --%>                                            
                     <li class="bold"><a class="waves-effect waves-cyan"><i class="mdi-image-image"></i> Analyties/Report</a>
                        </li>
                        
                          <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> Admin Setting </a>
                			<div class="collapsible-body">
                                <ul>
                                  
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=class/ClassSection' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> New Section</a></li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=subject/subjectlist' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> New Subject</a></li>
                                    <li><a href='<%=request.getContextPath()%>/app/user/loadpage?d=Setting/AdminSetting' class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Admin Setting</a></li>
                                </ul>
                            </div>
                 		</li>    
                        </aside> 
<!-- END LEFT SIDEBAR NAV--> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START CONTENT --> 
<section id="content"> 

<!--breadcrumbs start--> 
<div id="breadcrumbs-wrapper"> 
<!-- Search for small screen --> 
<div class="header-search-wrapper grey hide-on-large-only"> 
<i class="mdi-action-search active"></i> 
<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"> 
</div> 

<!--breadcrumbs end--> 
<div class="container"> 
<div class="row"> 
<div class="col s12 m12 l12"> 
<div class="row"> 
<div class="col s12"> 
<ul class="tabs tab-demo z-depth-1"> 
<li id="ad-tab1" class="tab col s3"><a class="active" href="#student-tab">Dormitory-Add Dormitory</a> 
</li> 

</ul> 
</div> 
<div class="col s12"> 
<form class="formValidate" id="formValidate" method="post"> 
<div id="student-tab" class="col s12"> 
<div class="row"> 

<h4 class="header2 title">Add Dormitory/View</h4> 

<div class="input-field col s12"> 
<label for="Dormitory_name">Dormitory Name</label>
<input id="loguser1" name="loguser1" type="hidden" maxlength="10" value="<%= sid %>">
<input id="Dormitory_name" name="Dormitory_name" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your Dormitory Name"> 
<div class="error errorTxt1"></div> 
</div> 

<div class="input-field col s12"> 
<label for="Location">Location</label>                                                                                                                                                                                                                                                                                                                                                                                                                             
<input id="Location" name="Location" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your Location"> 
<div class="error errorTxt2"></div> 
</div> 

<div class="input-field col s12"> 
<label for="Fee">Fee</label>                                                                                                                                                                                                                                                                                                                                                                                                                            
<input id="Fee" name="Fee" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter Fee"> 
<div class="error errorTxt3"></div> 
</div> 

<div class="col s12"> 
<label for="genter_select">Gender *</label> 
<p> 
<input name="cgender" type="radio" id="gender_male" class="stu_require" value="Male" data-error="Please select your gender"> 
<label for="gender_male">Male</label> 
</p> 
<p> 
<input name="cgender" type="radio" id="gender_female" class="stu_require" value="Female" data-error="Please select your gender"> 
<label for="gender_female">Female</label> 
</p> 
<div class="input-field"> 
<div class="error errorTxt4"></div> 
</div> 
</div> 

<div class="input-field col s12"> 
<label for="Description">Description</label>                                                                                                                                                                                                                                                                                                                                                                                                                            
<input id="Description" name="Description" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter Description"> 
<div class="error errorTxt5"></div> 
</div> 
<div class="input-field col s12"> 
<label for="No_OF_Beds">No OF Beds *</label> 
<input id="No_OF_Beds" name="mobile" maxlength="1" class="numbersOnly stu_require" type="text" data-error="Please enter your No OF Beds"> 
<div class="error errorTxt12"></div> 
</div> 

<h4 class="header2 title">Warden Details</h4> 

<div class="col m6">
		<!-- <img class="materialboxed"  src="Photo.jpg" id="WPhoto" name="WPhoto"> -->
		<label for="Warden"><h4>Warden</h4></label>
		
		<div class="file-field input-field">
                     					 <div class="btn">
                       					 <span>Photo *</span>
                       					 <input type="file" id="WPhoto" name="WPhoto">
                     					 </div>
                     					 <div class="file-path-wrapper">
                     					   <input class="file-path validate" type="text">
                    					  </div>
                    					</div>
</div>

<div class="input-field col s12"> 
<label for="Name">Name</label>                                                                                                                                                                                                                                                                                                                                                                                                                            
<input id="Name" name="Name" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your Name"> 
<div class="error errorTxt5"></div> 
</div> 

<div class="input-field col s12"> 
<label for="Age">Age *</label> 
<input id="Age" name="Age" maxlength="1" class="numbersOnly stu_require" type="text" data-error="Please enter your Age"> 
<div class="error errorTxt12"></div> 
</div>

<div class="input-field col s12"> 
<label for="Contact_Number">Contact Number *</label> 
<input id="Contact_Number" name="Contact_Number" maxlength="10" class="numbersOnly stu_require" type="text" data-error="Please enter your mobile Contact Number"> 
<div class="error errorTxt12"></div> 
</div>

<div class="input-field col s12"> 
<label for="ALt_Contact_Number">ALt Contact Number *</label> 
<input id="ALt_Contact_Number" name="ALt_Contact_Number" maxlength="10" class="numbersOnly stu_require" type="text" data-error="Please enter your ALternate Contact Number"> 
<div class="error errorTxt12"></div> 
</div>

<div class="input-field col s12"> 
<label for="Address">Address</label>                                                                                                                                                                                                                                                                                                                                                                                                                            
<input id="Address" name="Address" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter Address"> 
<div class="error errorTxt5"></div> 
</div>  

<div class="col m6">
		<!-- <img class="materialboxed"  src="Photo.jpg" id="Dphoto" name="Dphoto"> -->
		<label for="Warden"><h4>Deputy Warden</h4></label>
		
		<div class="file-field input-field">
                     					 <div class="btn">
                       					 <span>Photo *</span>
                       					 <input type="file" id="DPhoto" name="DPhoto">
                     					 </div>
                     					 <div class="file-path-wrapper">
                     					   <input class="file-path validate" type="text">
                    					  </div>
                    					</div>
</div>

<div class="input-field col s12"> 
<label for="Name1">Name</label>                                                                                                                                                                                                                                                                                                                                                                                                                            
<input id="Name1" name="Name1" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter your Name"> 
<div class="error errorTxt5"></div> 
</div> 

<div class="input-field col s12"> 
<label for="Age1">Age *</label> 
<input id="Age1" name="Age1" maxlength="1" class="numbersOnly stu_require" type="text" data-error="Please enter your Age"> 
<div class="error errorTxt12"></div> 
</div>

<div class="input-field col s12"> 
<label for="Contact_Number1">Contact Number *</label> 
<input id="Contact_Number1" name="Contact_Number" maxlength="1" class="numbersOnly stu_require" type="text" data-error="Please enter your  Contact Number"> 
<div class="error errorTxt12"></div> 
</div>

<div class="input-field col s12"> 
<label for="ALt_Contact_Number1">ALt_Contact_Number *</label> 
<input id="ALt_Contact_Number1" name="ALt_Contact_Number1" maxlength="1" class="numbersOnly stu_require" type="text" data-error="Please enter your ALternate Contact Number"> 
<div class="error errorTxt12"></div> 
</div>

<div class="input-field col s12"> 
<label for="Address1">Address</label>                                                                                                                                                                                                                                                                                                                                                                                                                            
<input id="Address1" name="Address1" type="text" maxlength="50" class="textOnly stu_require" data-error="Please enter Address"> 
<div class="error errorTxt5"></div> 
</div>  



<form> 
<div id="dynamicInput"> 

</div> 

</form> 

<div class="col s12"> 
<!-- <button class="btn waves-effect waves-light right" id="Next1" type="button" name="action">Submit
<i class="mdi-content-send right"></i> 
</button> --> 
<a href="javascript:void(0)" class="btn waves-effect waves-light right" onClick="DormitoryInsert()" name="add_dormitory" id="add_dormitory">SUBMIT</a>
</div> 

</div> 
</div></form> 



</div> 
</div> 
</div> 
</div> 

 <!--Preselecting a tab-->
          <div id="preselecting-tab" class="section">
            <!--<h4 class="header">Preselecting a tab</h4>-->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="row">
                  <div class="col s12">
                    <ul class="tabs tab-demo-active z-depth-1 cyan">
                      <li class="tab col s3"><a class="white-text waves-effect waves-light active" href="#stu_list_all">All</a>
                      </li>
                      <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#stu_list_Sec_1">Section 1</a>
                      </li>
                      <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#stu_list_Sec_2">Section 2</a>
                      </li>
                    </ul>
                  </div>
                  <div class="col s12">
                    <div id="stu_list_all" class="col s12  cyan lighten-4">
                     		<!--DataTables example-->
				            <div id="#student-datatables_1">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">
				                <div class="col s12 m12 l12">
				                  <table id="dor-db-data-table_1" class="responsive-table display" cellspacing="0">
				                    <thead>
				                        <tr>
				                        	<th>S.No</th>
				                            <th>Dormitory Name</th>
				                            <th>Location</th>
				                            <th>Warden Name</th>
				                            <th>Deputy Warden Name</th>
				                            <th>Operations</th>
				                        </tr>
				                    </thead>
				                 
				                    <tfoot>
				                        <tr>
				                        	<th>S.No</th>
				                            <th>Dormitory Name</th>
				                            <th>Location</th>
				                            <th>Warden Name</th>
				                            <th>Deputy Warden Name</th>
				                            <th>Operations</th>
				                        </tr>
				                    </tfoot>
				                 
				                    <tbody>
				                       
				                    </tbody>
				                  </table>
								  
				                </div>
				              </div>
				            </div>  
				            <br>
				            <div class="divider"></div> 
				            <!--DataTables example Row grouping-->
                    </div>
                    <div id="stu_list_Sec_1" class="col s12  cyan lighten-4">
                     	  <!--DataTables example-->
				            <div id="#student-datatables_2">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">
				                <div class="col s12 m12 l12">
				                  <table id="stu-db-data-table_2" class="responsive-table display" cellspacing="0">
				                    <thead>
				                        <tr>
				                            <th>Name</th>
				                            <th>Position</th>
				                            <th>Office</th>
				                            <th>Age</th>
				                            <th>Start date</th>
				                            <th>Salary</th>
				                        </tr>
				                    </thead>
				                 
				                    <tfoot>
				                        <tr>
				                            <th>Name</th>
				                            <th>Position</th>
				                            <th>Office</th>
				                            <th>Age</th>
				                            <th>Start date</th>
				                            <th>Salary</th>
				                        </tr>
				                    </tfoot>
				                 
				                    <tbody>
				                        <tr>
				                            <td>Tiger Nixon</td>
				                            <td>System Architect</td>
				                            <td>Edinburgh</td>
				                            <td>61</td>
				                            <td>2011/04/25</td>
				                            <td>$320,800</td>
				                        </tr>
				                        <tr>
				                            <td>Garrett Winters</td>
				                            <td>Accountant</td>
				                            <td>Tokyo</td>
				                            <td>63</td>
				                            <td>2011/07/25</td>
				                            <td>$170,750</td>
				                        </tr>
				                        <tr>
				                            <td>Sakura Yamamoto</td>
				                            <td>Support Engineer</td>
				                            <td>Tokyo</td>
				                            <td>37</td>
				                            <td>2009/08/19</td>
				                            <td>$139,575</td>
				                        </tr>
				                        <tr>
				                            <td>Thor Walton</td>
				                            <td>Developer</td>
				                            <td>New York</td>
				                            <td>61</td>
				                            <td>2013/08/11</td>
				                            <td>$98,540</td>
				                        </tr>
				                        <tr>
				                            <td>Finn Camacho</td>
				                            <td>Support Engineer</td>
				                            <td>San Francisco</td>
				                            <td>47</td>
				                            <td>2009/07/07</td>
				                            <td>$87,500</td>
				                        </tr>
				                        <tr>
				                            <td>Serge Baldwin</td>
				                            <td>Data Coordinator</td>
				                            <td>Singapore</td>
				                            <td>64</td>
				                            <td>2012/04/09</td>
				                            <td>$138,575</td>
				                        </tr>
				                        <tr>
				                            <td>Zenaida Frank</td>
				                            <td>Software Engineer</td>
				                            <td>New York</td>
				                            <td>63</td>
				                            <td>2010/01/04</td>
				                            <td>$125,250</td>
				                        </tr>
				                        <tr>
				                            <td>Zorita Serrano</td>
				                            <td>Software Engineer</td>
				                            <td>San Francisco</td>
				                            <td>56</td>
				                            <td>2012/06/01</td>
				                            <td>$115,000</td>
				                        </tr>
				                        <tr>
				                            <td>Jennifer Acosta</td>
				                            <td>Junior Javascript Developer</td>
				                            <td>Edinburgh</td>
				                            <td>43</td>
				                            <td>2013/02/01</td>
				                            <td>$75,650</td>
				                        </tr>
				                        <tr>
				                            <td>Cara Stevens</td>
				                            <td>Sales Assistant</td>
				                            <td>New York</td>
				                            <td>46</td>
				                            <td>2011/12/06</td>
				                            <td>$145,600</td>
				                        </tr>
				                        <tr>
				                            <td>Hermione Butler</td>
				                            <td>Regional Director</td>
				                            <td>London</td>
				                            <td>47</td>
				                            <td>2011/03/21</td>
				                            <td>$356,250</td>
				                        </tr>
				                        <tr>
				                            <td>Lael Greer</td>
				                            <td>Systems Administrator</td>
				                            <td>London</td>
				                            <td>21</td>
				                            <td>2009/02/27</td>
				                            <td>$103,500</td>
				                        </tr>
				                        <tr>
				                            <td>Jonas Alexander</td>
				                            <td>Developer</td>
				                            <td>San Francisco</td>
				                            <td>30</td>
				                            <td>2010/07/14</td>
				                            <td>$86,500</td>
				                        </tr>
				                        <tr>
				                            <td>Shad Decker</td>
				                            <td>Regional Director</td>
				                            <td>Edinburgh</td>
				                            <td>51</td>
				                            <td>2008/11/13</td>
				                            <td>$183,000</td>
				                        </tr>
				                        <tr>
				                            <td>Michael Bruce</td>
				                            <td>Javascript Developer</td>
				                            <td>Singapore</td>
				                            <td>29</td>
				                            <td>2011/06/27</td>
				                            <td>$183,000</td>
				                        </tr>
				                        <tr>
				                            <td>Donna Snider</td>
				                            <td>Customer Support</td>
				                            <td>New York</td>
				                            <td>27</td>
				                            <td>2011/01/25</td>
				                            <td>$112,000</td>
				                        </tr>
				                    </tbody>
				                  </table>
				                </div>
				              </div>
				            </div> 
				            <br>
				            <div class="divider"></div> 
				            <!--DataTables example Row grouping--> 
                    </div>
                    <div id="stu_list_Sec_2" class="col s12  cyan lighten-4">
                      		 <!--DataTables example-->
				            <div id="#student-datatables_3">
				              <!--<h4 class="header">DataTables example</h4>-->
				              <div class="row">
				                <div class="col s12 m12 l12">
				                  <table id="stu-db-data-table_3" class="responsive-table display" cellspacing="0">
				                    <thead>
				                        <tr>
				                            <th>Name</th>
				                            <th>Position</th>
				                            <th>Office</th>
				                            <th>Age</th>
				                            <th>Start date</th>
				                            <th>Salary</th>
				                        </tr>
				                    </thead>
				                 
				                    <tfoot>
				                        <tr>
				                            <th>Name</th>
				                            <th>Position</th>
				                            <th>Office</th>
				                            <th>Age</th>
				                            <th>Start date</th>
				                            <th>Salary</th>
				                        </tr>
				                    </tfoot>
				                 
				                    <tbody>
				                        <tr>
				                            <td>Tiger Nixon</td>
				                            <td>System Architect</td>
				                            <td>Edinburgh</td>
				                            <td>61</td>
				                            <td>2011/04/25</td>
				                            <td>$320,800</td>
				                        </tr>
				                        <tr>
				                            <td>Garrett Winters</td>
				                            <td>Accountant</td>
				                            <td>Tokyo</td>
				                            <td>63</td>
				                            <td>2011/07/25</td>
				                            <td>$170,750</td>
				                        </tr>
				                        <tr>
				                            <td>Sakura Yamamoto</td>
				                            <td>Support Engineer</td>
				                            <td>Tokyo</td>
				                            <td>37</td>
				                            <td>2009/08/19</td>
				                            <td>$139,575</td>
				                        </tr>
				                        <tr>
				                            <td>Thor Walton</td>
				                            <td>Developer</td>
				                            <td>New York</td>
				                            <td>61</td>
				                            <td>2013/08/11</td>
				                            <td>$98,540</td>
				                        </tr>
				                        <tr>
				                            <td>Finn Camacho</td>
				                            <td>Support Engineer</td>
				                            <td>San Francisco</td>
				                            <td>47</td>
				                            <td>2009/07/07</td>
				                            <td>$87,500</td>
				                        </tr>
				                        <tr>
				                            <td>Serge Baldwin</td>
				                            <td>Data Coordinator</td>
				                            <td>Singapore</td>
				                            <td>64</td>
				                            <td>2012/04/09</td>
				                            <td>$138,575</td>
				                        </tr>
				                        <tr>
				                            <td>Zenaida Frank</td>
				                            <td>Software Engineer</td>
				                            <td>New York</td>
				                            <td>63</td>
				                            <td>2010/01/04</td>
				                            <td>$125,250</td>
				                        </tr>
				                        <tr>
				                            <td>Zorita Serrano</td>
				                            <td>Software Engineer</td>
				                            <td>San Francisco</td>
				                            <td>56</td>
				                            <td>2012/06/01</td>
				                            <td>$115,000</td>
				                        </tr>
				                        <tr>
				                            <td>Jennifer Acosta</td>
				                            <td>Junior Javascript Developer</td>
				                            <td>Edinburgh</td>
				                            <td>43</td>
				                            <td>2013/02/01</td>
				                            <td>$75,650</td>
				                        </tr>
				                        <tr>
				                            <td>Cara Stevens</td>
				                            <td>Sales Assistant</td>
				                            <td>New York</td>
				                            <td>46</td>
				                            <td>2011/12/06</td>
				                            <td>$145,600</td>
				                        </tr>
				                        <tr>
				                            <td>Hermione Butler</td>
				                            <td>Regional Director</td>
				                            <td>London</td>
				                            <td>47</td>
				                            <td>2011/03/21</td>
				                            <td>$356,250</td>
				                        </tr>
				                        <tr>
				                            <td>Lael Greer</td>
				                            <td>Systems Administrator</td>
				                            <td>London</td>
				                            <td>21</td>
				                            <td>2009/02/27</td>
				                            <td>$103,500</td>
				                        </tr>
				                        <tr>
				                            <td>Jonas Alexander</td>
				                            <td>Developer</td>
				                            <td>San Francisco</td>
				                            <td>30</td>
				                            <td>2010/07/14</td>
				                            <td>$86,500</td>
				                        </tr>
				                        <tr>
				                            <td>Shad Decker</td>
				                            <td>Regional Director</td>
				                            <td>Edinburgh</td>
				                            <td>51</td>
				                            <td>2008/11/13</td>
				                            <td>$183,000</td>
				                        </tr>
				                        <tr>
				                            <td>Michael Bruce</td>
				                            <td>Javascript Developer</td>
				                            <td>Singapore</td>
				                            <td>29</td>
				                            <td>2011/06/27</td>
				                            <td>$183,000</td>
				                        </tr>
				                        <tr>
				                            <td>Donna Snider</td>
				                            <td>Customer Support</td>
				                            <td>New York</td>
				                            <td>27</td>
				                            <td>2011/01/25</td>
				                            <td>$112,000</td>
				                        </tr>
				                    </tbody>
				                  </table>
				                </div>
				              </div>
				            </div> 
				            <br>
				            <div class="divider"></div> 
				            <!--DataTables example Row grouping--> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

</div></section> 
<!-- END CONTENT --> 

</div> 
<!-- END WRAPPER --> 

</div> 
<!-- END MAIN --> 





<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START FOOTER --> 
<footer class="page-footer"> 
<div class="footer-copyright"> 
<div class="container"> 
<span>Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.orectiq.com" target="_blank">Orectiq</a> All rights reserved.</span> 
<span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://orectiq.com/">Orectiq</a></span> 
</div> 
</div> 
</footer> 
<!-- END FOOTER --> 



<!-- ================================================ 
Scripts 
================================================ --> 

<!-- jQuery Library --> 
<script type="text/javascript" src="../../js/plugins/jquery-1.11.2.min.js"></script>    
<!--materialize js--> 
<script type="text/javascript" src="../../js/materialize.min.js"></script> 
<!--prism 
<script type="text/javascript" src="js/prism/prism.js"></script>--> 
<!--scrollbar--> 
<script type="text/javascript" src="../../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- chartist --> 
<script type="text/javascript" src="../../js/plugins/chartist-js/chartist.min.js"></script>   

<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="../../js/plugins.min.js"></script> 
<!--custom-script.js - Add your own theme custom JS--> 
<script type="text/javascript" src="../../js/admission-custom-script.js"></script> 
<script type="text/javascript">

        /*Show entries on click hide*/

        $(document).ready(function(){
            $(".dropdown-content.select-dropdown li").on( "click", function() {
                var that = this;
                setTimeout(function(){
                if($(that).parent().hasClass('active')){
                        $(that).parent().removeClass('active');
                        $(that).parent().hide();
                }
                },100);
            });

            /*** STUDENT LIST INNER DROP DOWN ***/

	        $('#stu-db-data-table_1_length input.select-dropdown, #stu-db-data-table_2_length input.select-dropdown, #stu-db-data-table_3_length input.select-dropdown').click(function(event){
	            event.preventDefault();
	            $(this).parent().find('ul').addClass("class_active");
	        });

	        $('html').click(function(e) {
	            var a = e.target;
	              if ($(a).parents('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').length === 0)
	              {
	                $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	              }
        	});

	        $('#stu-db-data-table_1_length ul.select-dropdown li, #stu-db-data-table_2_length ul.select-dropdown li, #stu-db-data-table_3_length ul.select-dropdown li').click(function(event){
	        	event.preventDefault();
	            $('#stu-db-data-table_1_length ul.select-dropdown, #stu-db-data-table_2_length ul.select-dropdown, #stu-db-data-table_3_length ul.select-dropdown').removeClass("class_active"); 
	        });

			/*** DELETE "ENTRIES" TEXT ***/

			$('.dataTables_length label').contents().eq(2).replaceWith('');
			$('.dataTables_length label').contents().eq(4).replaceWith('');
			$('.dataTables_length label').contents().eq(6).replaceWith('');

			/*** STUDENT PROFILE POP UP ***/

			$('#student_prof_pop_01').click(function(event){
	            event.preventDefault();
	        });

        });
    </script>
    
     <script> var ctxPath = "<%=request.getContextPath()%>";</script>
	<script>
	retrieve();
function DormitoryInsert()
{
	////alert("Welcome to Dormitory function");
     var dt = new Date();
	
     var gender = document.getElementsByName("cgender");
	    if (gender[0].checked == true) {
	   	 xender=gender[0].value;
	    } else if(gender[1].checked == true) {
	   	 xender=gender[1].value;
	    }
     
     var allot=0;
     
	    
	 $.ajax({
       type: 'POST',
       url: ctxPath+'/app/Dormitory/insertDormitory.do?',
       data: {"Dormitory_name":$("#Dormitory_name").val(),"Location":$("#Location").val(),"fee":$("#Fee").val(),
    	   		"cgender":xender,"description":$("#Description").val(),"No_of_beds":$("#No_OF_Beds").val(),
    	   		"Warden_Photo":$("#WPhoto").val(),"name":$("#Name").val(),"age":$("#Age").val(),"Contact_Number":$("#Contact_Number").val(),
    	   		"Alt_Contact_Number":$("#ALt_Contact_Number").val(),"address":$("#Address").val(),
    	   		"DeputyWarden_Photo":$("#DPhoto").val(),"name1":$("#Name1").val(),"age1":$("#Age1").val(),"Contact_Number1":$("#Contact_Number1").val(),
    	   		"Alt_Contact_Number1":$("#ALt_Contact_Number1").val(),"address1":$("#Address1").val(),
    	   		"isActive":'Y',"Inserted_By":$("#loguser1").val(),"Inserted_Date":dt,"Updated_By":$("#loguser1").val(),"Updated_Date":dt,
    	   		"allot_beds":allot,"remain_beds":$("#No_OF_Beds").val()
       },
       success: function(data) 
       {
      	 if(data=='success')
      	 {
      		Materialize.toast("Success",4000);	 
      	 }
      	 else 
      	 {
      		 Materialize.toast(data,4000);	 
      	 }  
       }
	 });

	 
	 retrieve();
	}

function retrieve()
{
	////alert("enter retrieve");
	$.ajax({
			  type: "GET",
			  url: ctxPath+'/app/Dormitory/getltrreferencedata.do?',
			  dataType: 'json',
			}).done(function( responseJson )
					{	
 						loadDataIntoTable(responseJson);
				});
}



function loadDataIntoTable(responseJson)
{
	 var tblHtml = "";
	 $.each(responseJson.dormitoryServiceVOList, function(index, val)
	{
		var dname=val.dormitory_name;
		var loc=val.location;
		var wname=val.name;
		var dwname=val.name1;
		
		
		//$('#stuid').val(sid);
		////alert("in load dormitory name  "+dname);
		////alert("loc  "+loc);
		
		
			 tblHtml += '<tr><td style="text-align: left">'+(index+1)+'</a></td><td style="text-align: left" id='+dname+' onClick="getStudentsListAll(id)">'+dname+'</a></td><td style="text-align: left" value='+loc+' ondblclick="getStudentsListAll(this)">'+loc+'</td><td style="text-align: left">'+wname+'</td><td style="text-align: left">'+dwname+'</td>'+
			 '<td><a class="btn-floating blue btn tooltipped" data-position="top" data-delay="0" data-tooltip="Edit" onClick="studentEdit(id,name)" name="stuid" id='+dname+'><i class="large mdi-editor-border-color"></i></a>'+
			 '<a class="btn-floating red darken-1 btn tooltipped" data-position="top" data-delay="0" data-tooltip="Delete" onClick="studentDelete(id)" name="studel" id='+dname+'><i class="large mdi-action-delete"></i></a></td> </tr>';// 
	            //'<td style="text-align: center">'+responseJson.dob+'</td><td style="text-align: right">'+responseJson.bplace+'</td><td style="text-align: right">'+responseJson.nationality+'</td><td style="text-align: right">'+responseJson.mtongue+'</td><td style="text-align: right">'+val.cgender+'</td></tr>';
	            
	 });   
	
	 $("#dor-db-data-table_1 tbody").html(tblHtml);//getRows(10);

}

</script>


</body></html>