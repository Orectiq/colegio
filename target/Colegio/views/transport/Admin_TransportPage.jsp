<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Colegio - Admission Form</title>

	
	
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="../../assets/js/core/app.js"></script>

	<script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/ui/nicescroll.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/pickers/pickadate/legacy.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/picker_date.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="../../assets/js/pages/datatables_extension_buttons_init.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/ui/ripple.min.js"></script>
	<script type="text/javascript" src="../../assets/js/pages/datatables_extension_scroller.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
	<script type="text/javascript" src="../../assets/js/pages/form_bootstrap_select.js"></script>
	<script type="text/javascript" src="../../assets/js/pages/layout_fixed_custom.js"></script>
	
	<!-- /theme JS files -->
	
</head>


<%
                        	String tusername=(String)request.getAttribute("transId");
                    		//System.out.println("Value passssssssssssssssssssssss  "+uname1);
                        
                        %>

<body class="navbar-top">

	<!-- Main navbar -->
	<div class="navbar navbar-default navbar-fixed-top header-highlight">
	
	<!-- Main header navigation -->
	<script type="text/javascript" src="../../assets/js/custom/layout_header.js"></script>
	<!-- /main header navigation -->
		
	</div>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-fixed">
				<div class="sidebar-content">

					<!-- Main navigation -->
						 <script type="text/javascript" src="../../assets/js/custom/layout_left_navgation.js"></script>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					
				

					<!-- Form validation -->
					<div class="panel panel-flat">
						
						<div class="panel-body">
							
							<form class="form-horizontal form-validate-jquery" action="#">
								<fieldset class="content-group">
									<legend class="text-bold">Transport Ao Login</legend>

									<!-- Basic text input -->
									
									<div class="form-group">
								
									<div class="col-sm-2">
										<div class="thumbnail">
										<div class="thumb">
											<image id="photo1"  name='photo1' src="C:\Users\prave\Desktop\adddmin\1.png"></image>
											
										</div>
										</div>
										</div>
										
										<label class="control-label col-sm-1">Staff Id</label>
											<div class="col-sm-4">
												<input type="text" id="staffid" name="staffid"  placeholder="Eugene" class="form-control" disabled>
											</div>
											
										<div class="col-sm-5">
											<button type="submit" class="btn btn-primary">Show <i class="position"></i></button>								
											
										</div>
										
										<label class="control-label col-sm-1">AO Name</label>
											<div class="col-sm-4">
											<input id="name" name="name" placeholder="Eugene" class="form-control" disabled  />
											<!-- <input id="uname" name="uname" placeholder="Eugene" class="form-control" disabled value=<%= tusername %> /> -->
											
											</div>									
									
									
										<label class="control-label col-sm-1">Gender</label>
											<div class="col-sm-4">
												<input type="text" id="gender" name="gender"  placeholder="Eugene" class="form-control" disabled>
											</div>
											
										<label class="control-label col-sm-1">Phone </label>
											<div class="col-sm-4">
												<input type="text" id="phone" name="phone" placeholder="Eugene" class="form-control" disabled>
											</div>
										
										<label class="control-label col-sm-1">Email</label>
											<div class="col-sm-4">
												<input type="text" id="email" name="email" placeholder="Eugene" class="form-control" disabled>
											</div>
										<label class="control-label col-sm-1 col-sm-offset-2">Address</label>
											<div class="col-sm-9">
												<input type="text" id="addr" name="addr" placeholder="Eugene" class="form-control" disabled>
											</div>
										
										</div>
										
									<table id="stu-db-data-table_1" class="responsive-table display" cellspacing="0">							
									<thead>
									</thead>
										<tbody>

											<div class="row">
											<br>
											</div>

										</tbody>
									</table>
									
							</form>
						</div>
					</div>
					<!-- /form validation -->

					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2016. <a href="#">Colegio</a> by <a href="http://orectiq.com" target="_blank">Orectiq</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
  <script> var ctxPath = "<%=request.getContextPath() %>";</script>
    
    
   <script src="../../assets/js/ColegioCall/call.js"></script>	
	<script type="text/javascript" src="js/Admin_TransportPage.js"></script>	
</body>
</html>
